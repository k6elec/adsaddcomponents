classdef ADS_TQRES < ADScomponent
    % ADS_TQRES matlab representation for the ADS TQRES component
    % Triquint GaAs IC Resistor
    % TQRES [:Name] t1 t2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Resistance Value (smorr) Unit: Ohms
        R
        % Resistor  Width (smorr) Unit: m
        W
        % GaAs Substrate Height (s---r) Unit: m
        H
        % Resistor type (s---r) 
        Type
    end
    methods
        function obj = set.R(obj,val)
            obj = setParameter(obj,'R',val,0,'real');
        end
        function res = get.R(obj)
            res = getParameter(obj,'R');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.H(obj,val)
            obj = setParameter(obj,'H',val,0,'real');
        end
        function res = get.H(obj)
            res = getParameter(obj,'H');
        end
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'real');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
    end
end
