classdef ADS_TQFET < ADScomponent
    % ADS_TQFET matlab representation for the ADS TQFET component
    % Triquint GaAs FET
    % TQFET [:Name] td tg ts ...
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Gate  Width (smorr) Unit: m
        W
        % Number of Gate Fingers (s---r) 
        N
        % Fraction of Idss, Xids (smorr) 
        Xids
        % Drain to Source Voltage (smorr) Unit: V
        Vds
        % FET type (s---r) 
        Type
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Xids(obj,val)
            obj = setParameter(obj,'Xids',val,0,'real');
        end
        function res = get.Xids(obj)
            res = getParameter(obj,'Xids');
        end
        function obj = set.Vds(obj,val)
            obj = setParameter(obj,'Vds',val,0,'real');
        end
        function res = get.Vds(obj)
            res = getParameter(obj,'Vds');
        end
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'real');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
    end
end
