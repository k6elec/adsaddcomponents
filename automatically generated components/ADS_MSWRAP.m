classdef ADS_MSWRAP < ADScomponent
    % ADS_MSWRAP matlab representation for the ADS MSWRAP component
    % Microstrip ribbon ground strap
    % MSWRAP [:Name] t1 ...
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Line Width (smorr) Unit: m
        W
        % Thickness (smorr) Unit: m
        T
        % Conductivity per meter (smorr) Unit: S/m
        Cond
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.T(obj,val)
            obj = setParameter(obj,'T',val,0,'real');
        end
        function res = get.T(obj)
            res = getParameter(obj,'T');
        end
        function obj = set.Cond(obj,val)
            obj = setParameter(obj,'Cond',val,0,'real');
        end
        function res = get.Cond(obj)
            res = getParameter(obj,'Cond');
        end
    end
end
