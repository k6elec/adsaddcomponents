classdef ADS_Alter < ADSnodeless
    % ADS_Alter matlab representation for the ADS Alter component
    % Alter an instance parameter
    % Alter [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Name of parameter/variable to be altered (s---s) 
        Var
        % New value/instance for parameter/variable (s---d) 
        VarValue
        % Degree of annotation (s---i) 
        StatusLevel
    end
    methods
        function obj = set.Var(obj,val)
            obj = setParameter(obj,'Var',val,0,'string');
        end
        function res = get.Var(obj)
            res = getParameter(obj,'Var');
        end
        function obj = set.VarValue(obj,val)
            obj = setParameter(obj,'VarValue',val,0,'string');
        end
        function res = get.VarValue(obj)
            res = getParameter(obj,'VarValue');
        end
        function obj = set.StatusLevel(obj,val)
            obj = setParameter(obj,'StatusLevel',val,0,'integer');
        end
        function res = get.StatusLevel(obj)
            res = getParameter(obj,'StatusLevel');
        end
    end
end
