classdef ADS_PHD < ADScomponent
    % ADS_PHD matlab representation for the ADS PHD component
    % Symbolically Defined Device
    % PHD [:Name] p1 n1 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % current, i[port, vector]=f(_xv(),_xi(),_xv_...) (smo-r) 
        I
        % voltage, v[port, vector]=f(_xv(),_xi(),_xv_...) (smo-r) 
        V
        % charge, q[port, vector]=f(_xv(),_xi(),_xv_...) (smo-r) 
        Q
        % flux, q[port, vector]=f(_xv(),_xi(),_xv_...) (smo-r) 
        Fl
        % clock enable, ce[port] = sum(2^trigger number) (sm--i) 
        Ce
        % trigger event, trig[n] =  xcross() ... (smo-r) 
        Trig
        % Actual n'th frequency value (smo-r) 
        Freq
        % AC frequency equation for n'th port (sm--i) 
        Fss
        % Enable PHD equations (sm--b) 
        Enable
        % Expression evaluated to detect strange HB behavior (s---r) 
        StrangeHB
        % X parameter data file name with absolute path (s---s) 
        Xfile
        % Small signal conductance [i_port, eqn, v_port] (---rr) 
        di_dv
        % Small signal current gain [i_port, eqn, i_port] (---rr) 
        di_di
        % Small signal (voltage gain) [f_port, eqn, v_port] (---rr) 
        df_dv
        % Small signal (impedance) [f_port, eqn, i_port] (---rr) 
        df_di
    end
    methods
        function obj = set.I(obj,val)
            obj = setParameter(obj,'I',val,2,'real');
        end
        function res = get.I(obj)
            res = getParameter(obj,'I');
        end
        function obj = set.V(obj,val)
            obj = setParameter(obj,'V',val,2,'real');
        end
        function res = get.V(obj)
            res = getParameter(obj,'V');
        end
        function obj = set.Q(obj,val)
            obj = setParameter(obj,'Q',val,2,'real');
        end
        function res = get.Q(obj)
            res = getParameter(obj,'Q');
        end
        function obj = set.Fl(obj,val)
            obj = setParameter(obj,'Fl',val,2,'real');
        end
        function res = get.Fl(obj)
            res = getParameter(obj,'Fl');
        end
        function obj = set.Ce(obj,val)
            obj = setParameter(obj,'Ce',val,1,'integer');
        end
        function res = get.Ce(obj)
            res = getParameter(obj,'Ce');
        end
        function obj = set.Trig(obj,val)
            obj = setParameter(obj,'Trig',val,1,'real');
        end
        function res = get.Trig(obj)
            res = getParameter(obj,'Trig');
        end
        function obj = set.Freq(obj,val)
            obj = setParameter(obj,'Freq',val,1,'real');
        end
        function res = get.Freq(obj)
            res = getParameter(obj,'Freq');
        end
        function obj = set.Fss(obj,val)
            obj = setParameter(obj,'Fss',val,2,'integer');
        end
        function res = get.Fss(obj)
            res = getParameter(obj,'Fss');
        end
        function obj = set.Enable(obj,val)
            obj = setParameter(obj,'Enable',val,0,'boolean');
        end
        function res = get.Enable(obj)
            res = getParameter(obj,'Enable');
        end
        function obj = set.StrangeHB(obj,val)
            obj = setParameter(obj,'StrangeHB',val,0,'real');
        end
        function res = get.StrangeHB(obj)
            res = getParameter(obj,'StrangeHB');
        end
        function obj = set.Xfile(obj,val)
            obj = setParameter(obj,'Xfile',val,0,'string');
        end
        function res = get.Xfile(obj)
            res = getParameter(obj,'Xfile');
        end
        function obj = set.di_dv(obj,val)
            obj = setParameter(obj,'di_dv',val,3,'real');
        end
        function res = get.di_dv(obj)
            res = getParameter(obj,'di_dv');
        end
        function obj = set.di_di(obj,val)
            obj = setParameter(obj,'di_di',val,3,'real');
        end
        function res = get.di_di(obj)
            res = getParameter(obj,'di_di');
        end
        function obj = set.df_dv(obj,val)
            obj = setParameter(obj,'df_dv',val,3,'real');
        end
        function res = get.df_dv(obj)
            res = getParameter(obj,'df_dv');
        end
        function obj = set.df_di(obj,val)
            obj = setParameter(obj,'df_di',val,3,'real');
        end
        function res = get.df_di(obj)
            res = getParameter(obj,'df_di');
        end
    end
end
