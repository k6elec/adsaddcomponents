classdef ADS_MSOP < ADScomponent
    % ADS_MSOP matlab representation for the ADS MSOP component
    % Microstrip Symmetric Pair of Open Stubs
    % MSOP [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Width of Input Line (Smorr) Unit: m
        W1
        % Distance between centerlines of Input Line and Stub-Pair (Smorr) Unit: m
        D1
        % Width of Output Line (Smorr) Unit: m
        W2
        % Distance between centerlines of Output Line and of Stub-Pair (Smorr) Unit: m
        D2
        % Width of Stubs (Smorr) Unit: m
        Ws
        % Combined Length of Stubs (Smorr) Unit: m
        Ls
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W1(obj,val)
            obj = setParameter(obj,'W1',val,0,'real');
        end
        function res = get.W1(obj)
            res = getParameter(obj,'W1');
        end
        function obj = set.D1(obj,val)
            obj = setParameter(obj,'D1',val,0,'real');
        end
        function res = get.D1(obj)
            res = getParameter(obj,'D1');
        end
        function obj = set.W2(obj,val)
            obj = setParameter(obj,'W2',val,0,'real');
        end
        function res = get.W2(obj)
            res = getParameter(obj,'W2');
        end
        function obj = set.D2(obj,val)
            obj = setParameter(obj,'D2',val,0,'real');
        end
        function res = get.D2(obj)
            res = getParameter(obj,'D2');
        end
        function obj = set.Ws(obj,val)
            obj = setParameter(obj,'Ws',val,0,'real');
        end
        function res = get.Ws(obj)
            res = getParameter(obj,'Ws');
        end
        function obj = set.Ls(obj,val)
            obj = setParameter(obj,'Ls',val,0,'real');
        end
        function res = get.Ls(obj)
            res = getParameter(obj,'Ls');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end
