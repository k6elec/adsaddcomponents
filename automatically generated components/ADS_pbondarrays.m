classdef ADS_pbondarrays < ADScomponent
    % ADS_pbondarrays matlab representation for the ADS pbondarrays component
    % User-defined model
    % pbondarrays [:Name] n1 n2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % User device parameter (S--ri) Unit: unknown
        N
        % User device parameter (Smorr) Unit: unknown
        RW
        % User device parameter (Smorr) Unit: unknown
        COND
        % User device parameter (Smorr) Unit: unknown
        wdx
        % User device parameter (Smorr) Unit: unknown
        wdy
        % User device parameter (Smorr) Unit: unknown
        wn
        % User device parameter (Smorr) Unit: unknown
        wsp
        % User device parameter (Smorr) Unit: unknown
        wdz
        % User device parameter (Smorr) Unit: unknown
        wl
        % User device parameter (Sm-rs) Unit: unknown
        wseq
        % User device parameter (Smorr) Unit: unknown
        wsp1
        % User device parameter (Sm-rs) Unit: unknown
        wseq1
        % User device parameter (Smorr) Unit: unknown
        wsp2
        % User device parameter (Sm-rs) Unit: unknown
        wseq2
        % User device parameter (Smorr) Unit: unknown
        wsp3
        % User device parameter (Sm-rs) Unit: unknown
        wseq3
        % User device parameter (Smorr) Unit: unknown
        sx1
        % User device parameter (Smorr) Unit: unknown
        sx2
        % User device parameter (Smorr) Unit: unknown
        sx3
        % User device parameter (Smorr) Unit: unknown
        sx4
        % User device parameter (Smorr) Unit: unknown
        sx5
        % User device parameter (Smorr) Unit: unknown
        sx6
        % User device parameter (Smorr) Unit: unknown
        sy1
        % User device parameter (Smorr) Unit: unknown
        sy2
        % User device parameter (Smorr) Unit: unknown
        sy3
        % User device parameter (Smorr) Unit: unknown
        sy4
        % User device parameter (Smorr) Unit: unknown
        sy5
        % User device parameter (Smorr) Unit: unknown
        sy6
        % User device parameter (Smorr) Unit: unknown
        sz1
        % User device parameter (Smorr) Unit: unknown
        sz2
        % User device parameter (Smorr) Unit: unknown
        sz3
        % User device parameter (Smorr) Unit: unknown
        sz4
        % User device parameter (Smorr) Unit: unknown
        sz5
        % User device parameter (Smorr) Unit: unknown
        sz6
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.RW(obj,val)
            obj = setParameter(obj,'RW',val,0,'real');
        end
        function res = get.RW(obj)
            res = getParameter(obj,'RW');
        end
        function obj = set.COND(obj,val)
            obj = setParameter(obj,'COND',val,0,'real');
        end
        function res = get.COND(obj)
            res = getParameter(obj,'COND');
        end
        function obj = set.wdx(obj,val)
            obj = setParameter(obj,'wdx',val,0,'real');
        end
        function res = get.wdx(obj)
            res = getParameter(obj,'wdx');
        end
        function obj = set.wdy(obj,val)
            obj = setParameter(obj,'wdy',val,0,'real');
        end
        function res = get.wdy(obj)
            res = getParameter(obj,'wdy');
        end
        function obj = set.wn(obj,val)
            obj = setParameter(obj,'wn',val,0,'real');
        end
        function res = get.wn(obj)
            res = getParameter(obj,'wn');
        end
        function obj = set.wsp(obj,val)
            obj = setParameter(obj,'wsp',val,0,'real');
        end
        function res = get.wsp(obj)
            res = getParameter(obj,'wsp');
        end
        function obj = set.wdz(obj,val)
            obj = setParameter(obj,'wdz',val,0,'real');
        end
        function res = get.wdz(obj)
            res = getParameter(obj,'wdz');
        end
        function obj = set.wl(obj,val)
            obj = setParameter(obj,'wl',val,0,'real');
        end
        function res = get.wl(obj)
            res = getParameter(obj,'wl');
        end
        function obj = set.wseq(obj,val)
            obj = setParameter(obj,'wseq',val,0,'string');
        end
        function res = get.wseq(obj)
            res = getParameter(obj,'wseq');
        end
        function obj = set.wsp1(obj,val)
            obj = setParameter(obj,'wsp1',val,0,'real');
        end
        function res = get.wsp1(obj)
            res = getParameter(obj,'wsp1');
        end
        function obj = set.wseq1(obj,val)
            obj = setParameter(obj,'wseq1',val,0,'string');
        end
        function res = get.wseq1(obj)
            res = getParameter(obj,'wseq1');
        end
        function obj = set.wsp2(obj,val)
            obj = setParameter(obj,'wsp2',val,0,'real');
        end
        function res = get.wsp2(obj)
            res = getParameter(obj,'wsp2');
        end
        function obj = set.wseq2(obj,val)
            obj = setParameter(obj,'wseq2',val,0,'string');
        end
        function res = get.wseq2(obj)
            res = getParameter(obj,'wseq2');
        end
        function obj = set.wsp3(obj,val)
            obj = setParameter(obj,'wsp3',val,0,'real');
        end
        function res = get.wsp3(obj)
            res = getParameter(obj,'wsp3');
        end
        function obj = set.wseq3(obj,val)
            obj = setParameter(obj,'wseq3',val,0,'string');
        end
        function res = get.wseq3(obj)
            res = getParameter(obj,'wseq3');
        end
        function obj = set.sx1(obj,val)
            obj = setParameter(obj,'sx1',val,0,'real');
        end
        function res = get.sx1(obj)
            res = getParameter(obj,'sx1');
        end
        function obj = set.sx2(obj,val)
            obj = setParameter(obj,'sx2',val,0,'real');
        end
        function res = get.sx2(obj)
            res = getParameter(obj,'sx2');
        end
        function obj = set.sx3(obj,val)
            obj = setParameter(obj,'sx3',val,0,'real');
        end
        function res = get.sx3(obj)
            res = getParameter(obj,'sx3');
        end
        function obj = set.sx4(obj,val)
            obj = setParameter(obj,'sx4',val,0,'real');
        end
        function res = get.sx4(obj)
            res = getParameter(obj,'sx4');
        end
        function obj = set.sx5(obj,val)
            obj = setParameter(obj,'sx5',val,0,'real');
        end
        function res = get.sx5(obj)
            res = getParameter(obj,'sx5');
        end
        function obj = set.sx6(obj,val)
            obj = setParameter(obj,'sx6',val,0,'real');
        end
        function res = get.sx6(obj)
            res = getParameter(obj,'sx6');
        end
        function obj = set.sy1(obj,val)
            obj = setParameter(obj,'sy1',val,0,'real');
        end
        function res = get.sy1(obj)
            res = getParameter(obj,'sy1');
        end
        function obj = set.sy2(obj,val)
            obj = setParameter(obj,'sy2',val,0,'real');
        end
        function res = get.sy2(obj)
            res = getParameter(obj,'sy2');
        end
        function obj = set.sy3(obj,val)
            obj = setParameter(obj,'sy3',val,0,'real');
        end
        function res = get.sy3(obj)
            res = getParameter(obj,'sy3');
        end
        function obj = set.sy4(obj,val)
            obj = setParameter(obj,'sy4',val,0,'real');
        end
        function res = get.sy4(obj)
            res = getParameter(obj,'sy4');
        end
        function obj = set.sy5(obj,val)
            obj = setParameter(obj,'sy5',val,0,'real');
        end
        function res = get.sy5(obj)
            res = getParameter(obj,'sy5');
        end
        function obj = set.sy6(obj,val)
            obj = setParameter(obj,'sy6',val,0,'real');
        end
        function res = get.sy6(obj)
            res = getParameter(obj,'sy6');
        end
        function obj = set.sz1(obj,val)
            obj = setParameter(obj,'sz1',val,0,'real');
        end
        function res = get.sz1(obj)
            res = getParameter(obj,'sz1');
        end
        function obj = set.sz2(obj,val)
            obj = setParameter(obj,'sz2',val,0,'real');
        end
        function res = get.sz2(obj)
            res = getParameter(obj,'sz2');
        end
        function obj = set.sz3(obj,val)
            obj = setParameter(obj,'sz3',val,0,'real');
        end
        function res = get.sz3(obj)
            res = getParameter(obj,'sz3');
        end
        function obj = set.sz4(obj,val)
            obj = setParameter(obj,'sz4',val,0,'real');
        end
        function res = get.sz4(obj)
            res = getParameter(obj,'sz4');
        end
        function obj = set.sz5(obj,val)
            obj = setParameter(obj,'sz5',val,0,'real');
        end
        function res = get.sz5(obj)
            res = getParameter(obj,'sz5');
        end
        function obj = set.sz6(obj,val)
            obj = setParameter(obj,'sz6',val,0,'real');
        end
        function res = get.sz6(obj)
            res = getParameter(obj,'sz6');
        end
    end
end
