classdef ADS_PC_Gap < ADScomponent
    % ADS_PC_Gap matlab representation for the ADS PC_Gap component
    % User-defined model
    % PC_Gap [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % User device parameter (Sm-ri) Unit: unknown
        Layer
        % User device parameter (Smorr) Unit: unknown
        W
        % User device parameter (Smorr) Unit: unknown
        G
        % User device parameter (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.Layer(obj,val)
            obj = setParameter(obj,'Layer',val,0,'integer');
        end
        function res = get.Layer(obj)
            res = getParameter(obj,'Layer');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.G(obj,val)
            obj = setParameter(obj,'G',val,0,'real');
        end
        function res = get.G(obj)
            res = getParameter(obj,'G');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end
