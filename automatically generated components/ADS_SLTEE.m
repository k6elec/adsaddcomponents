classdef ADS_SLTEE < ADScomponent
    % ADS_SLTEE matlab representation for the ADS SLTEE component
    % Stripline tee discounity
    % SLTEE [:Name] n1 n2 n3
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Width 1 of stripline tee (Smorr) Unit: m
        W1
        % Width 1 of stripline tee (Smorr) Unit: m
        W2
        % Substrate label (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.W1(obj,val)
            obj = setParameter(obj,'W1',val,0,'real');
        end
        function res = get.W1(obj)
            res = getParameter(obj,'W1');
        end
        function obj = set.W2(obj,val)
            obj = setParameter(obj,'W2',val,0,'real');
        end
        function res = get.W2(obj)
            res = getParameter(obj,'W2');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end
