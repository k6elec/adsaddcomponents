classdef ADS_OscPort < ADScomponent
    % ADS_OscPort matlab representation for the ADS OscPort component
    % Oscillator Port
    % OscPort [:Name] p1 n1 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Initial guess at fundamental voltage (smorr) Unit: V
        V
        % Initial value for Z0 (smo-r) Unit: Ohms
        Z
        % Resistance for port 1 (smo-r) Unit: Ohms
        Z1
        % Resistance for port 2 (smo-r) Unit: Ohms
        Z2
        % Gain adjustment (smo-c) Unit: Ohms
        Polish
        % Gain adjustment (smo-c) Unit: V
        SS_Voltage
        % Number of octaves to search (smo-r) 
        NumOctaves
        % Number of steps per search octave (smo-r) 
        Steps
        % Fundmental number for oscillator (s---i) 
        FundIndex
        % Harmonic number for oscillator fundmental (sm--i) 
        Harm
        % Maximum arc-length step during loop gain search (s---r) 
        MaxLoopGainStep
        % Phase of oscport fundamental (smo-r) 
        phase
    end
    methods
        function obj = set.V(obj,val)
            obj = setParameter(obj,'V',val,0,'real');
        end
        function res = get.V(obj)
            res = getParameter(obj,'V');
        end
        function obj = set.Z(obj,val)
            obj = setParameter(obj,'Z',val,0,'real');
        end
        function res = get.Z(obj)
            res = getParameter(obj,'Z');
        end
        function obj = set.Z1(obj,val)
            obj = setParameter(obj,'Z1',val,0,'real');
        end
        function res = get.Z1(obj)
            res = getParameter(obj,'Z1');
        end
        function obj = set.Z2(obj,val)
            obj = setParameter(obj,'Z2',val,0,'real');
        end
        function res = get.Z2(obj)
            res = getParameter(obj,'Z2');
        end
        function obj = set.Polish(obj,val)
            obj = setParameter(obj,'Polish',val,0,'complex');
        end
        function res = get.Polish(obj)
            res = getParameter(obj,'Polish');
        end
        function obj = set.SS_Voltage(obj,val)
            obj = setParameter(obj,'SS_Voltage',val,0,'complex');
        end
        function res = get.SS_Voltage(obj)
            res = getParameter(obj,'SS_Voltage');
        end
        function obj = set.NumOctaves(obj,val)
            obj = setParameter(obj,'NumOctaves',val,0,'real');
        end
        function res = get.NumOctaves(obj)
            res = getParameter(obj,'NumOctaves');
        end
        function obj = set.Steps(obj,val)
            obj = setParameter(obj,'Steps',val,0,'real');
        end
        function res = get.Steps(obj)
            res = getParameter(obj,'Steps');
        end
        function obj = set.FundIndex(obj,val)
            obj = setParameter(obj,'FundIndex',val,0,'integer');
        end
        function res = get.FundIndex(obj)
            res = getParameter(obj,'FundIndex');
        end
        function obj = set.Harm(obj,val)
            obj = setParameter(obj,'Harm',val,0,'integer');
        end
        function res = get.Harm(obj)
            res = getParameter(obj,'Harm');
        end
        function obj = set.MaxLoopGainStep(obj,val)
            obj = setParameter(obj,'MaxLoopGainStep',val,0,'real');
        end
        function res = get.MaxLoopGainStep(obj)
            res = getParameter(obj,'MaxLoopGainStep');
        end
        function obj = set.phase(obj,val)
            obj = setParameter(obj,'phase',val,0,'real');
        end
        function res = get.phase(obj)
            res = getParameter(obj,'phase');
        end
    end
end
