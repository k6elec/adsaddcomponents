classdef ADS_TQSWH < ADScomponent
    % ADS_TQSWH matlab representation for the ADS TQSWH component
    % Triquint GaAs FET Switch
    % TQSWH [:Name] td tg ts ...
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Gate to Source Voltage (smorr) Unit: V
        Vgs
    end
    methods
        function obj = set.Vgs(obj,val)
            obj = setParameter(obj,'Vgs',val,0,'real');
        end
        function res = get.Vgs(obj)
            res = getParameter(obj,'Vgs');
        end
    end
end
