classdef ADS_Port < ADScomponent
    % ADS_Port matlab representation for the ADS Port component
    % Independent Power Source/Port
    % Port [:Name] p+ p-
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Reference impedance (smorc) Unit: Ohms
        Z
        % Port number (s--ri) 
        Num
        % Open circuit DC voltage (smorr) Unit: V
        Vdc
        % AC power (with optional phase) into matched load (smorc) Unit: W
        Pac
        % Large signal power (with optional phase) into matched load (smorc) Unit: W
        P
        % Small signal power (with optional phase) at given upper sideband (smo-c) Unit: W
        P_USB
        % Small signal power (with optional phase) at given lower sideband (smo-c) Unit: W
        P_LSB
        % Large signal powers (with optional phase) into matched load (smorc) Unit: W
        P_All
        % Single fundamental index for this source (sm-ri) 
        FundIndex
        % Linear, complex modulation (smorc) 
        Mod
        % Flag to enable/disable port noise (sm-rb) 
        Noise
        % Temperature of port in degrees Celsius (smorr) Unit: deg C
        Temp
        % Actual n'th frequency value (smo-r) Unit: Hz
        Freq
        % All Frequencies (smo-r) Unit: Hz
        Freq_All
        % Array of large signal harmonics (smo-c) Unit: V
        HarmList
        % Phase noise as a function of offset frequency (smo-r) Unit: dB
        PhaseNoise
        % Restrict phase noise to the USB/LSB of the carrier signal (s---b) 
        LimitPhaseNoiseToCarrier
        % Nominal temperature in degrees Celsisus (smorr) Unit: deg C
        Tnom
        % Linear temperature coefficient; per degree Celsius (smorr) Unit: 1/deg C
        TC1
        % Temperature coefficient; per degree Celsius squared (smorr) Unit: 1/(deg C)^2
        TC2
    end
    methods
        function obj = set.Z(obj,val)
            obj = setParameter(obj,'Z',val,0,'complex');
        end
        function res = get.Z(obj)
            res = getParameter(obj,'Z');
        end
        function obj = set.Num(obj,val)
            obj = setParameter(obj,'Num',val,0,'integer');
        end
        function res = get.Num(obj)
            res = getParameter(obj,'Num');
        end
        function obj = set.Vdc(obj,val)
            obj = setParameter(obj,'Vdc',val,0,'real');
        end
        function res = get.Vdc(obj)
            res = getParameter(obj,'Vdc');
        end
        function obj = set.Pac(obj,val)
            obj = setParameter(obj,'Pac',val,0,'complex');
        end
        function res = get.Pac(obj)
            res = getParameter(obj,'Pac');
        end
        function obj = set.P(obj,val)
            obj = setParameter(obj,'P',val,3,'complex');
        end
        function res = get.P(obj)
            res = getParameter(obj,'P');
        end
        function obj = set.P_USB(obj,val)
            obj = setParameter(obj,'P_USB',val,3,'complex');
        end
        function res = get.P_USB(obj)
            res = getParameter(obj,'P_USB');
        end
        function obj = set.P_LSB(obj,val)
            obj = setParameter(obj,'P_LSB',val,3,'complex');
        end
        function res = get.P_LSB(obj)
            res = getParameter(obj,'P_LSB');
        end
        function obj = set.P_All(obj,val)
            obj = setParameter(obj,'P_All',val,3,'complex');
        end
        function res = get.P_All(obj)
            res = getParameter(obj,'P_All');
        end
        function obj = set.FundIndex(obj,val)
            obj = setParameter(obj,'FundIndex',val,0,'integer');
        end
        function res = get.FundIndex(obj)
            res = getParameter(obj,'FundIndex');
        end
        function obj = set.Mod(obj,val)
            obj = setParameter(obj,'Mod',val,0,'complex');
        end
        function res = get.Mod(obj)
            res = getParameter(obj,'Mod');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Freq(obj,val)
            obj = setParameter(obj,'Freq',val,0,'real');
        end
        function res = get.Freq(obj)
            res = getParameter(obj,'Freq');
        end
        function obj = set.Freq_All(obj,val)
            obj = setParameter(obj,'Freq_All',val,0,'real');
        end
        function res = get.Freq_All(obj)
            res = getParameter(obj,'Freq_All');
        end
        function obj = set.HarmList(obj,val)
            obj = setParameter(obj,'HarmList',val,0,'complex');
        end
        function res = get.HarmList(obj)
            res = getParameter(obj,'HarmList');
        end
        function obj = set.PhaseNoise(obj,val)
            obj = setParameter(obj,'PhaseNoise',val,0,'real');
        end
        function res = get.PhaseNoise(obj)
            res = getParameter(obj,'PhaseNoise');
        end
        function obj = set.LimitPhaseNoiseToCarrier(obj,val)
            obj = setParameter(obj,'LimitPhaseNoiseToCarrier',val,0,'boolean');
        end
        function res = get.LimitPhaseNoiseToCarrier(obj)
            res = getParameter(obj,'LimitPhaseNoiseToCarrier');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.TC1(obj,val)
            obj = setParameter(obj,'TC1',val,0,'real');
        end
        function res = get.TC1(obj)
            res = getParameter(obj,'TC1');
        end
        function obj = set.TC2(obj,val)
            obj = setParameter(obj,'TC2',val,0,'real');
        end
        function res = get.TC2(obj)
            res = getParameter(obj,'TC2');
        end
    end
end
