classdef ADS_EE_BJT2_Model < ADSmodel
    % ADS_EE_BJT2_Model matlab representation for the ADS EE_BJT2_Model component
    % HP EEsof Bipolar Junction Transistor model
    % model ModelName EE_BJT2 ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Forward emission coefficient (smorr) 
        Nf
        % Forward early voltage (smorr) Unit: V
        Vaf
        % High current corner for forward beta (smorr) Unit: A
        Ikf
        % B-E leakage saturation current (smorr) Unit: A
        Ise
        % B-E leakage emission coef (smorr) 
        Ne
        % Reverse emission coefficient (smorr) 
        Nr
        % Reverse early voltage (smorr) Unit: V
        Var
        % High current corner for reverse beta (smorr) Unit: A
        Ikr
        % B-C leakage saturation current (smorr) Unit: A
        Isc
        % B-C leakage emission coef (smorr) 
        Nc
        % Zero bias base resistance (smorr) Unit: Ohms
        Rb
        % Emitter resistance (smorr) Unit: Ohms
        Re
        % Collector resistance (smorr) Unit: Ohms
        Rc
        % B-E zero-bias junction capacitance (smorr) Unit: F
        Cje
        % B-E built-in junction potential (smorr) Unit: V
        Vje
        % B-E junction exponent (smorr) 
        Mje
        % B-C zero-bias junction capacitance (smorr) Unit: F
        Cjc
        % B-C built-in junction potential (smorr) Unit: V
        Vjc
        % B-C junction exponent (smorr) 
        Mjc
        % Junction capacitor forward-bias threshold (smorr) 
        Fc
        % Ideal forward transit time (smorr) Unit: s
        Tf
        % Coefficient for bias dependence of `tf' (smorr) 
        Xtf
        % Voltage describing Vbc dependence of `tf' (smorr) Unit: V
        Vtf
        % High current parameter for effect on `tf' (smorr) Unit: A
        Itf
        % Ideal reverse transit time (smorr) Unit: s
        Tr
        % Forward saturation current for EE_BJT2 (smorr) Unit: A
        Isf
        % Reverse saturation current for EE_BJT2 (smorr) Unit: A
        Isr
        % Fwd base saturation current for EE_BJT2 (smorr) Unit: A
        Ibif
        % Rev base saturation current for EE_BJT2 (smorr) Unit: A
        Ibir
        % EE_BJT2 forward base ideality factor (smorr) 
        Nbf
        % EE_BJT2 reverse base ideality factor (smorr) 
        Nbr
        % EE_BJT2 extraction temperature (smorr) Unit: deg C
        Tamb
        % EE_BJT2 transistor type (s---r) 
        Type
        % Secured model parameters (s---b) 
        Secured
        % Substrate junction forward bias (warning) (s--rr) Unit: V
        wVsubfwd
        % Substrate junction reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvsub
        % Base-emitter reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvbe
        % Base-collector reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvbc
        % Base-collector forward bias (warning) (s--rr) Unit: V
        wVbcfwd
        % Maximum base current (warning) (s--rr) Unit: A
        wIbmax
        % Maximum collector current (warning) (s--rr) Unit: A
        wIcmax
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
        % Maximum base to emitter voltage (TSMC SOA warning) (smorr) Unit: V
        Vbe_max
        % Maximum base to collector voltage (TSMC SOA warning) (smorr) Unit: V
        Vbc_max
        % Maximum collector to emitter voltage (TSMC SOA warning) (smorr) Unit: V
        Vce_max
        % Maximum collector to substrate voltage (TSMC SOA warning) (smorr) Unit: V
        Vcs_max
    end
    methods
        function obj = set.Nf(obj,val)
            obj = setParameter(obj,'Nf',val,0,'real');
        end
        function res = get.Nf(obj)
            res = getParameter(obj,'Nf');
        end
        function obj = set.Vaf(obj,val)
            obj = setParameter(obj,'Vaf',val,0,'real');
        end
        function res = get.Vaf(obj)
            res = getParameter(obj,'Vaf');
        end
        function obj = set.Ikf(obj,val)
            obj = setParameter(obj,'Ikf',val,0,'real');
        end
        function res = get.Ikf(obj)
            res = getParameter(obj,'Ikf');
        end
        function obj = set.Ise(obj,val)
            obj = setParameter(obj,'Ise',val,0,'real');
        end
        function res = get.Ise(obj)
            res = getParameter(obj,'Ise');
        end
        function obj = set.Ne(obj,val)
            obj = setParameter(obj,'Ne',val,0,'real');
        end
        function res = get.Ne(obj)
            res = getParameter(obj,'Ne');
        end
        function obj = set.Nr(obj,val)
            obj = setParameter(obj,'Nr',val,0,'real');
        end
        function res = get.Nr(obj)
            res = getParameter(obj,'Nr');
        end
        function obj = set.Var(obj,val)
            obj = setParameter(obj,'Var',val,0,'real');
        end
        function res = get.Var(obj)
            res = getParameter(obj,'Var');
        end
        function obj = set.Ikr(obj,val)
            obj = setParameter(obj,'Ikr',val,0,'real');
        end
        function res = get.Ikr(obj)
            res = getParameter(obj,'Ikr');
        end
        function obj = set.Isc(obj,val)
            obj = setParameter(obj,'Isc',val,0,'real');
        end
        function res = get.Isc(obj)
            res = getParameter(obj,'Isc');
        end
        function obj = set.Nc(obj,val)
            obj = setParameter(obj,'Nc',val,0,'real');
        end
        function res = get.Nc(obj)
            res = getParameter(obj,'Nc');
        end
        function obj = set.Rb(obj,val)
            obj = setParameter(obj,'Rb',val,0,'real');
        end
        function res = get.Rb(obj)
            res = getParameter(obj,'Rb');
        end
        function obj = set.Re(obj,val)
            obj = setParameter(obj,'Re',val,0,'real');
        end
        function res = get.Re(obj)
            res = getParameter(obj,'Re');
        end
        function obj = set.Rc(obj,val)
            obj = setParameter(obj,'Rc',val,0,'real');
        end
        function res = get.Rc(obj)
            res = getParameter(obj,'Rc');
        end
        function obj = set.Cje(obj,val)
            obj = setParameter(obj,'Cje',val,0,'real');
        end
        function res = get.Cje(obj)
            res = getParameter(obj,'Cje');
        end
        function obj = set.Vje(obj,val)
            obj = setParameter(obj,'Vje',val,0,'real');
        end
        function res = get.Vje(obj)
            res = getParameter(obj,'Vje');
        end
        function obj = set.Mje(obj,val)
            obj = setParameter(obj,'Mje',val,0,'real');
        end
        function res = get.Mje(obj)
            res = getParameter(obj,'Mje');
        end
        function obj = set.Cjc(obj,val)
            obj = setParameter(obj,'Cjc',val,0,'real');
        end
        function res = get.Cjc(obj)
            res = getParameter(obj,'Cjc');
        end
        function obj = set.Vjc(obj,val)
            obj = setParameter(obj,'Vjc',val,0,'real');
        end
        function res = get.Vjc(obj)
            res = getParameter(obj,'Vjc');
        end
        function obj = set.Mjc(obj,val)
            obj = setParameter(obj,'Mjc',val,0,'real');
        end
        function res = get.Mjc(obj)
            res = getParameter(obj,'Mjc');
        end
        function obj = set.Fc(obj,val)
            obj = setParameter(obj,'Fc',val,0,'real');
        end
        function res = get.Fc(obj)
            res = getParameter(obj,'Fc');
        end
        function obj = set.Tf(obj,val)
            obj = setParameter(obj,'Tf',val,0,'real');
        end
        function res = get.Tf(obj)
            res = getParameter(obj,'Tf');
        end
        function obj = set.Xtf(obj,val)
            obj = setParameter(obj,'Xtf',val,0,'real');
        end
        function res = get.Xtf(obj)
            res = getParameter(obj,'Xtf');
        end
        function obj = set.Vtf(obj,val)
            obj = setParameter(obj,'Vtf',val,0,'real');
        end
        function res = get.Vtf(obj)
            res = getParameter(obj,'Vtf');
        end
        function obj = set.Itf(obj,val)
            obj = setParameter(obj,'Itf',val,0,'real');
        end
        function res = get.Itf(obj)
            res = getParameter(obj,'Itf');
        end
        function obj = set.Tr(obj,val)
            obj = setParameter(obj,'Tr',val,0,'real');
        end
        function res = get.Tr(obj)
            res = getParameter(obj,'Tr');
        end
        function obj = set.Isf(obj,val)
            obj = setParameter(obj,'Isf',val,0,'real');
        end
        function res = get.Isf(obj)
            res = getParameter(obj,'Isf');
        end
        function obj = set.Isr(obj,val)
            obj = setParameter(obj,'Isr',val,0,'real');
        end
        function res = get.Isr(obj)
            res = getParameter(obj,'Isr');
        end
        function obj = set.Ibif(obj,val)
            obj = setParameter(obj,'Ibif',val,0,'real');
        end
        function res = get.Ibif(obj)
            res = getParameter(obj,'Ibif');
        end
        function obj = set.Ibir(obj,val)
            obj = setParameter(obj,'Ibir',val,0,'real');
        end
        function res = get.Ibir(obj)
            res = getParameter(obj,'Ibir');
        end
        function obj = set.Nbf(obj,val)
            obj = setParameter(obj,'Nbf',val,0,'real');
        end
        function res = get.Nbf(obj)
            res = getParameter(obj,'Nbf');
        end
        function obj = set.Nbr(obj,val)
            obj = setParameter(obj,'Nbr',val,0,'real');
        end
        function res = get.Nbr(obj)
            res = getParameter(obj,'Nbr');
        end
        function obj = set.Tamb(obj,val)
            obj = setParameter(obj,'Tamb',val,0,'real');
        end
        function res = get.Tamb(obj)
            res = getParameter(obj,'Tamb');
        end
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'real');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
        function obj = set.wVsubfwd(obj,val)
            obj = setParameter(obj,'wVsubfwd',val,0,'real');
        end
        function res = get.wVsubfwd(obj)
            res = getParameter(obj,'wVsubfwd');
        end
        function obj = set.wBvsub(obj,val)
            obj = setParameter(obj,'wBvsub',val,0,'real');
        end
        function res = get.wBvsub(obj)
            res = getParameter(obj,'wBvsub');
        end
        function obj = set.wBvbe(obj,val)
            obj = setParameter(obj,'wBvbe',val,0,'real');
        end
        function res = get.wBvbe(obj)
            res = getParameter(obj,'wBvbe');
        end
        function obj = set.wBvbc(obj,val)
            obj = setParameter(obj,'wBvbc',val,0,'real');
        end
        function res = get.wBvbc(obj)
            res = getParameter(obj,'wBvbc');
        end
        function obj = set.wVbcfwd(obj,val)
            obj = setParameter(obj,'wVbcfwd',val,0,'real');
        end
        function res = get.wVbcfwd(obj)
            res = getParameter(obj,'wVbcfwd');
        end
        function obj = set.wIbmax(obj,val)
            obj = setParameter(obj,'wIbmax',val,0,'real');
        end
        function res = get.wIbmax(obj)
            res = getParameter(obj,'wIbmax');
        end
        function obj = set.wIcmax(obj,val)
            obj = setParameter(obj,'wIcmax',val,0,'real');
        end
        function res = get.wIcmax(obj)
            res = getParameter(obj,'wIcmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.Vbe_max(obj,val)
            obj = setParameter(obj,'Vbe_max',val,0,'real');
        end
        function res = get.Vbe_max(obj)
            res = getParameter(obj,'Vbe_max');
        end
        function obj = set.Vbc_max(obj,val)
            obj = setParameter(obj,'Vbc_max',val,0,'real');
        end
        function res = get.Vbc_max(obj)
            res = getParameter(obj,'Vbc_max');
        end
        function obj = set.Vce_max(obj,val)
            obj = setParameter(obj,'Vce_max',val,0,'real');
        end
        function res = get.Vce_max(obj)
            res = getParameter(obj,'Vce_max');
        end
        function obj = set.Vcs_max(obj,val)
            obj = setParameter(obj,'Vcs_max',val,0,'real');
        end
        function res = get.Vcs_max(obj)
            res = getParameter(obj,'Vcs_max');
        end
    end
end
