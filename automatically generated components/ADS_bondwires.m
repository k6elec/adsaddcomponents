classdef ADS_bondwires < ADScomponent
    % ADS_bondwires matlab representation for the ADS bondwires component
    % User-defined model
    % bondwires [:Name] n1 n2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % User device parameter (S--ri) Unit: unknown
        N
        % User device parameter (Smorr) Unit: unknown
        RW
        % User device parameter (Smorr) Unit: unknown
        COND
        % User device parameter (Sm-ri) Unit: unknown
        OPTION
        % User device parameter (Smorr) Unit: unknown
        x1
        % User device parameter (Smorr) Unit: unknown
        x2
        % User device parameter (Smorr) Unit: unknown
        x3
        % User device parameter (Smorr) Unit: unknown
        x4
        % User device parameter (Smorr) Unit: unknown
        x5
        % User device parameter (Smorr) Unit: unknown
        x6
        % User device parameter (Smorr) Unit: unknown
        y1
        % User device parameter (Smorr) Unit: unknown
        y2
        % User device parameter (Smorr) Unit: unknown
        y3
        % User device parameter (Smorr) Unit: unknown
        y4
        % User device parameter (Smorr) Unit: unknown
        y5
        % User device parameter (Smorr) Unit: unknown
        y6
        % User device parameter (Smorr) Unit: unknown
        z1
        % User device parameter (Smorr) Unit: unknown
        z2
        % User device parameter (Smorr) Unit: unknown
        z3
        % User device parameter (Smorr) Unit: unknown
        z4
        % User device parameter (Smorr) Unit: unknown
        z5
        % User device parameter (Smorr) Unit: unknown
        z6
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.RW(obj,val)
            obj = setParameter(obj,'RW',val,0,'real');
        end
        function res = get.RW(obj)
            res = getParameter(obj,'RW');
        end
        function obj = set.COND(obj,val)
            obj = setParameter(obj,'COND',val,0,'real');
        end
        function res = get.COND(obj)
            res = getParameter(obj,'COND');
        end
        function obj = set.OPTION(obj,val)
            obj = setParameter(obj,'OPTION',val,0,'integer');
        end
        function res = get.OPTION(obj)
            res = getParameter(obj,'OPTION');
        end
        function obj = set.x1(obj,val)
            obj = setParameter(obj,'x1',val,0,'real');
        end
        function res = get.x1(obj)
            res = getParameter(obj,'x1');
        end
        function obj = set.x2(obj,val)
            obj = setParameter(obj,'x2',val,0,'real');
        end
        function res = get.x2(obj)
            res = getParameter(obj,'x2');
        end
        function obj = set.x3(obj,val)
            obj = setParameter(obj,'x3',val,0,'real');
        end
        function res = get.x3(obj)
            res = getParameter(obj,'x3');
        end
        function obj = set.x4(obj,val)
            obj = setParameter(obj,'x4',val,0,'real');
        end
        function res = get.x4(obj)
            res = getParameter(obj,'x4');
        end
        function obj = set.x5(obj,val)
            obj = setParameter(obj,'x5',val,0,'real');
        end
        function res = get.x5(obj)
            res = getParameter(obj,'x5');
        end
        function obj = set.x6(obj,val)
            obj = setParameter(obj,'x6',val,0,'real');
        end
        function res = get.x6(obj)
            res = getParameter(obj,'x6');
        end
        function obj = set.y1(obj,val)
            obj = setParameter(obj,'y1',val,0,'real');
        end
        function res = get.y1(obj)
            res = getParameter(obj,'y1');
        end
        function obj = set.y2(obj,val)
            obj = setParameter(obj,'y2',val,0,'real');
        end
        function res = get.y2(obj)
            res = getParameter(obj,'y2');
        end
        function obj = set.y3(obj,val)
            obj = setParameter(obj,'y3',val,0,'real');
        end
        function res = get.y3(obj)
            res = getParameter(obj,'y3');
        end
        function obj = set.y4(obj,val)
            obj = setParameter(obj,'y4',val,0,'real');
        end
        function res = get.y4(obj)
            res = getParameter(obj,'y4');
        end
        function obj = set.y5(obj,val)
            obj = setParameter(obj,'y5',val,0,'real');
        end
        function res = get.y5(obj)
            res = getParameter(obj,'y5');
        end
        function obj = set.y6(obj,val)
            obj = setParameter(obj,'y6',val,0,'real');
        end
        function res = get.y6(obj)
            res = getParameter(obj,'y6');
        end
        function obj = set.z1(obj,val)
            obj = setParameter(obj,'z1',val,0,'real');
        end
        function res = get.z1(obj)
            res = getParameter(obj,'z1');
        end
        function obj = set.z2(obj,val)
            obj = setParameter(obj,'z2',val,0,'real');
        end
        function res = get.z2(obj)
            res = getParameter(obj,'z2');
        end
        function obj = set.z3(obj,val)
            obj = setParameter(obj,'z3',val,0,'real');
        end
        function res = get.z3(obj)
            res = getParameter(obj,'z3');
        end
        function obj = set.z4(obj,val)
            obj = setParameter(obj,'z4',val,0,'real');
        end
        function res = get.z4(obj)
            res = getParameter(obj,'z4');
        end
        function obj = set.z5(obj,val)
            obj = setParameter(obj,'z5',val,0,'real');
        end
        function res = get.z5(obj)
            res = getParameter(obj,'z5');
        end
        function obj = set.z6(obj,val)
            obj = setParameter(obj,'z6',val,0,'real');
        end
        function res = get.z6(obj)
            res = getParameter(obj,'z6');
        end
    end
end
