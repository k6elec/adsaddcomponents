classdef ADS_RX < ADScomponent
    % ADS_RX matlab representation for the ADS RX component
    % RX model for Channel Simulation.
    % RX [:Name] p ...
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % The Rx model (s--rs) 
        Model
    end
    methods
        function obj = set.Model(obj,val)
            obj = setParameter(obj,'Model',val,0,'string');
        end
        function res = get.Model(obj)
            res = getParameter(obj,'Model');
        end
    end
end
