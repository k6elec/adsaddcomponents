classdef ADS_TLIN4 < ADScomponent
    % ADS_TLIN4 matlab representation for the ADS TLIN4 component
    % Ideal 4-Terminal Transmission Line
    % TLIN4 [:Name] n1 n2 n3 n4
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Characteristic Impedance (Smorr) Unit: ohms
        Z
        % Electrical Length (Smorr) Unit: deg
        E
        % Reference Frequency for Electrical Length (Smorr) Unit: hz
        F
    end
    methods
        function obj = set.Z(obj,val)
            obj = setParameter(obj,'Z',val,0,'real');
        end
        function res = get.Z(obj)
            res = getParameter(obj,'Z');
        end
        function obj = set.E(obj,val)
            obj = setParameter(obj,'E',val,0,'real');
        end
        function res = get.E(obj)
            res = getParameter(obj,'E');
        end
        function obj = set.F(obj,val)
            obj = setParameter(obj,'F',val,0,'real');
        end
        function res = get.F(obj)
            res = getParameter(obj,'F');
        end
    end
end
