classdef ADS_ETAPER < ADScomponent
    % ADS_ETAPER matlab representation for the ADS ETAPER component
    % Ideal exponential tapered line
    % ETAPER [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Impedance of port 1 of exponential tapered line (Smorr) Unit: oh
        Z1
        % Impedance of port 2 of exponential tapered line (Smorr) Unit: oh
        Z2
        % Length of exponential tapered line (Smorr) Unit: m
        L
        % Relative velocity of exponential tapered line (Smorr) Unit: 1
        V
    end
    methods
        function obj = set.Z1(obj,val)
            obj = setParameter(obj,'Z1',val,0,'real');
        end
        function res = get.Z1(obj)
            res = getParameter(obj,'Z1');
        end
        function obj = set.Z2(obj,val)
            obj = setParameter(obj,'Z2',val,0,'real');
        end
        function res = get.Z2(obj)
            res = getParameter(obj,'Z2');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.V(obj,val)
            obj = setParameter(obj,'V',val,0,'real');
        end
        function res = get.V(obj)
            res = getParameter(obj,'V');
        end
    end
end
