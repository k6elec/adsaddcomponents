classdef ADS_ParamSweep < ADSnodeless
    % ADS_ParamSweep matlab representation for the ADS ParamSweep component
    % Sweeps other analyses
    % ParamSweep [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Analysis instance/path name to control (repeatable) (s---s) 
        SimInstanceName
        % Name of variable or parameter to be swept (s---s) 
        SweepVar
        % Stim instance/path name for sweep values (repeatable) (sm--d) 
        SweepPlan
        % Degree of annotation (s---i) 
        StatusLevel
        % Enable controllor (sm--b) 
        Enable
        % Restores nominal values if controlling optimization (s---b) 
        RestoreNomValues
    end
    methods
        function obj = set.SimInstanceName(obj,val)
            obj = setParameter(obj,'SimInstanceName',val,1,'string');
        end
        function res = get.SimInstanceName(obj)
            res = getParameter(obj,'SimInstanceName');
        end
        function obj = set.SweepVar(obj,val)
            obj = setParameter(obj,'SweepVar',val,0,'string');
        end
        function res = get.SweepVar(obj)
            res = getParameter(obj,'SweepVar');
        end
        function obj = set.SweepPlan(obj,val)
            obj = setParameter(obj,'SweepPlan',val,1,'string');
        end
        function res = get.SweepPlan(obj)
            res = getParameter(obj,'SweepPlan');
        end
        function obj = set.StatusLevel(obj,val)
            obj = setParameter(obj,'StatusLevel',val,0,'integer');
        end
        function res = get.StatusLevel(obj)
            res = getParameter(obj,'StatusLevel');
        end
        function obj = set.Enable(obj,val)
            obj = setParameter(obj,'Enable',val,0,'boolean');
        end
        function res = get.Enable(obj)
            res = getParameter(obj,'Enable');
        end
        function obj = set.RestoreNomValues(obj,val)
            obj = setParameter(obj,'RestoreNomValues',val,0,'boolean');
        end
        function res = get.RestoreNomValues(obj)
            res = getParameter(obj,'RestoreNomValues');
        end
    end
end
