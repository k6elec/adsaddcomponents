classdef ADS_PinDiode_Model < ADSmodel
    % ADS_PinDiode_Model matlab representation for the ADS PinDiode_Model component
    % P-I-N Diode model
    % model ModelName PinDiode ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Saturation current (smorr) Unit: A
        Is
        % Series resistance (smorr) Unit: Ohms
        Rs
        % I-region forward bias voltage drop (smorr) Unit: V
        Vi
        % Electron mobility (smorr) Unit: cm^2/(V*s)
        Un
        % I-region width (smorr) Unit: m
        Wi
        % I-region reverse bias resistance (smorr) Unit: Ohms
        Rr
        % P-I-N punchthrough capacitance (smorr) Unit: F
        Cmin
        % Ambipolar lifetime within I region (smorr) Unit: s
        Tau
        % Zero-bias junction capacitance (smorr) Unit: F
        Cjo
        % Junction potential (smorr) Unit: V
        Vj
        % Grading coefficient (smorr) 
        M
        % Forward-bias depletion capacitance threshold (smorr) 
        Fc
        % Flicker-noise coefficient (smorr) 
        Kf
        % Flicker-noise exponent (smorr) 
        Af
        % Explosion current (smorr) Unit: A/m^2
        Imax
        % Explosion current (smorr) Unit: A/m^2
        Imelt
        % flicker noise frequency exponent (smorr) 
        Ffe
        % Diode reverse breakdown voltage (warning) (s--rr) Unit: V
        wBv
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
        % Secured model parameters (s---b) 
        Secured
    end
    methods
        function obj = set.Is(obj,val)
            obj = setParameter(obj,'Is',val,0,'real');
        end
        function res = get.Is(obj)
            res = getParameter(obj,'Is');
        end
        function obj = set.Rs(obj,val)
            obj = setParameter(obj,'Rs',val,0,'real');
        end
        function res = get.Rs(obj)
            res = getParameter(obj,'Rs');
        end
        function obj = set.Vi(obj,val)
            obj = setParameter(obj,'Vi',val,0,'real');
        end
        function res = get.Vi(obj)
            res = getParameter(obj,'Vi');
        end
        function obj = set.Un(obj,val)
            obj = setParameter(obj,'Un',val,0,'real');
        end
        function res = get.Un(obj)
            res = getParameter(obj,'Un');
        end
        function obj = set.Wi(obj,val)
            obj = setParameter(obj,'Wi',val,0,'real');
        end
        function res = get.Wi(obj)
            res = getParameter(obj,'Wi');
        end
        function obj = set.Rr(obj,val)
            obj = setParameter(obj,'Rr',val,0,'real');
        end
        function res = get.Rr(obj)
            res = getParameter(obj,'Rr');
        end
        function obj = set.Cmin(obj,val)
            obj = setParameter(obj,'Cmin',val,0,'real');
        end
        function res = get.Cmin(obj)
            res = getParameter(obj,'Cmin');
        end
        function obj = set.Tau(obj,val)
            obj = setParameter(obj,'Tau',val,0,'real');
        end
        function res = get.Tau(obj)
            res = getParameter(obj,'Tau');
        end
        function obj = set.Cjo(obj,val)
            obj = setParameter(obj,'Cjo',val,0,'real');
        end
        function res = get.Cjo(obj)
            res = getParameter(obj,'Cjo');
        end
        function obj = set.Vj(obj,val)
            obj = setParameter(obj,'Vj',val,0,'real');
        end
        function res = get.Vj(obj)
            res = getParameter(obj,'Vj');
        end
        function obj = set.M(obj,val)
            obj = setParameter(obj,'M',val,0,'real');
        end
        function res = get.M(obj)
            res = getParameter(obj,'M');
        end
        function obj = set.Fc(obj,val)
            obj = setParameter(obj,'Fc',val,0,'real');
        end
        function res = get.Fc(obj)
            res = getParameter(obj,'Fc');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Imax(obj,val)
            obj = setParameter(obj,'Imax',val,0,'real');
        end
        function res = get.Imax(obj)
            res = getParameter(obj,'Imax');
        end
        function obj = set.Imelt(obj,val)
            obj = setParameter(obj,'Imelt',val,0,'real');
        end
        function res = get.Imelt(obj)
            res = getParameter(obj,'Imelt');
        end
        function obj = set.Ffe(obj,val)
            obj = setParameter(obj,'Ffe',val,0,'real');
        end
        function res = get.Ffe(obj)
            res = getParameter(obj,'Ffe');
        end
        function obj = set.wBv(obj,val)
            obj = setParameter(obj,'wBv',val,0,'real');
        end
        function res = get.wBv(obj)
            res = getParameter(obj,'wBv');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end
