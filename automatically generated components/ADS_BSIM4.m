classdef ADS_BSIM4 < ADScomponent
    % ADS_BSIM4 matlab representation for the ADS BSIM4 component
    % Berkeley Short Channel IGFET Model 4
    % ModelName [:Name] drain gate source body
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Channel length (smorr) Unit: m
        Length
        % Channel width (smorr) Unit: m
        Width
        % Number of fingers (smorr) 
        Nf
        % Minimize either D or S (sm-ri) 
        Min
        % Drain area (smorr) Unit: m^2
        Ad
        % Source area (smorr) Unit: m^2
        As
        % Drain perimeter (smorr) Unit: m
        Pd
        % Source perimeter (smorr) Unit: m
        Ps
        % Number of squares in drain (smorr) 
        Nrd
        % Number of squares in source (smorr) 
        Nrs
        % Body resistance (smorr) 
        Rbdb
        % Body resistance (smorr) 
        Rbsb
        % Body resistance (smorr) 
        Rbpb
        % Body resistance (smorr) 
        Rbps
        % Body resistance (smorr) 
        Rbpd
        % Transient NQS model selector (s--ri) 
        Trnqsmod
        % AC NQS model selector (s--ri) 
        Acnqsmod
        % Distributed body R model selector (s--ri) 
        Rbodymod
        % Gate resistance model selector (s--ri) 
        Rgatemod
        % Geometry dependent parasitics model selector (s--ri) 
        Geomod
        % S/D resistance and contact model selector (sm-ri) 
        Rgeomod
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Nonlinear spectral model on/off (s---i) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % Distance between OD edge to poly of one side (smorr) Unit: m
        Sa
        % Distance between OD edge to poly of the other side (smorr) Unit: m
        Sb
        % Distance between neighbour fingers (smorr) Unit: m
        Sd
        % Shift in body bias coefficent K1 (smorr) Unit: V^(1/2)
        Delk1
        % Shift in zero-bias threshold voltage Vth0 (smorr) Unit: V
        Delvt0
        % Mobility multiplier (smorr) 
        Mulu0
        % Shift in subthreshold swing factor Nfactor (smorr) 
        Delnfct
        % Distance to a single well edge (smorr) Unit: m
        Sc
        % Integral of the first distribution function for scattered well dopant (smorr) 
        Sca
        % Integral of the second distribution function for scattered well dopant (smorr) 
        Scb
        % Integral of the third distribution function for scattered well dopant (smorr) 
        Scc
        % Distance from gate contact center to device edge (smorr) Unit: m
        Xgw
        % Number of gate contacts (smorr) 
        Ngcon
        % Source contact resistance (smorr) 
        Rsc
        % Drain contact resistance (smorr) 
        Rdc
        % Device is initially off (---rb) 
        Off
        % Small signal drain source conductance (---rr) Unit: S
        Gds
        % Small signal Vgs to Ids transconductance (---rr) Unit: S
        Gm
        % Small signal Vbs to Ids transconductance (---rr) Unit: S
        Gmbs
        % Small signal Vgd to Ids transconductance (---rr) Unit: S
        Gmr
        % Small signal Vbd to Ids transconductance (---rr) Unit: S
        Gmbsr
        % Small signal bulk drain junction capacitance (---rr) Unit: F
        Capbd
        % Small signal bulk source junction capacitance (---rr) Unit: F
        Capbs
        % Small signal gate capacitance (---rr) Unit: F
        Cggb
        % Small signal gate drain capacitance (---rr) Unit: F
        Cgdb
        % Small signal gate source capacitance (---rr) Unit: F
        Cgsb
        % Small signal drain gate capacitance (---rr) Unit: F
        Cdgb
        % Small signal drain capacitance (---rr) Unit: F
        Cddb
        % Small signal drain source capacitance (---rr) Unit: F
        Cdsb
        % Small signal bulk gate capacitance (---rr) Unit: F
        Cbgb
        % Small signal bulk drain capacitance (---rr) Unit: F
        Cbdb
        % Small signal bulk source capacitance (---rr) Unit: F
        Cbsb
        % Small signal source gate capacitance (---rr) Unit: F
        Csgb
        % Small signal source drain capacitance (---rr) Unit: F
        Csdb
        % Small signal source capacitance (---rr) Unit: F
        Cssb
        % Gate charge (---rr) Unit: C
        Qg
        % Drain charge (---rr) Unit: C
        Qd
        % Bulk charge (---rr) Unit: C
        Qb
        % NMOS type BSIM4 MOSFET (---rb) 
        NMOS
        % PMOS type BSIM4 MOSFET (---rb) 
        PMOS
        % Qdef used in NQS model (---rr) Unit: C
        Qdef
        % Gtau used in NQS model (---rr) Unit: S
        Gtau
        % Gcrg (---rr) Unit: S
        Gcrg
    end
    methods
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.Width(obj,val)
            obj = setParameter(obj,'Width',val,0,'real');
        end
        function res = get.Width(obj)
            res = getParameter(obj,'Width');
        end
        function obj = set.Nf(obj,val)
            obj = setParameter(obj,'Nf',val,0,'real');
        end
        function res = get.Nf(obj)
            res = getParameter(obj,'Nf');
        end
        function obj = set.Min(obj,val)
            obj = setParameter(obj,'Min',val,0,'integer');
        end
        function res = get.Min(obj)
            res = getParameter(obj,'Min');
        end
        function obj = set.Ad(obj,val)
            obj = setParameter(obj,'Ad',val,0,'real');
        end
        function res = get.Ad(obj)
            res = getParameter(obj,'Ad');
        end
        function obj = set.As(obj,val)
            obj = setParameter(obj,'As',val,0,'real');
        end
        function res = get.As(obj)
            res = getParameter(obj,'As');
        end
        function obj = set.Pd(obj,val)
            obj = setParameter(obj,'Pd',val,0,'real');
        end
        function res = get.Pd(obj)
            res = getParameter(obj,'Pd');
        end
        function obj = set.Ps(obj,val)
            obj = setParameter(obj,'Ps',val,0,'real');
        end
        function res = get.Ps(obj)
            res = getParameter(obj,'Ps');
        end
        function obj = set.Nrd(obj,val)
            obj = setParameter(obj,'Nrd',val,0,'real');
        end
        function res = get.Nrd(obj)
            res = getParameter(obj,'Nrd');
        end
        function obj = set.Nrs(obj,val)
            obj = setParameter(obj,'Nrs',val,0,'real');
        end
        function res = get.Nrs(obj)
            res = getParameter(obj,'Nrs');
        end
        function obj = set.Rbdb(obj,val)
            obj = setParameter(obj,'Rbdb',val,0,'real');
        end
        function res = get.Rbdb(obj)
            res = getParameter(obj,'Rbdb');
        end
        function obj = set.Rbsb(obj,val)
            obj = setParameter(obj,'Rbsb',val,0,'real');
        end
        function res = get.Rbsb(obj)
            res = getParameter(obj,'Rbsb');
        end
        function obj = set.Rbpb(obj,val)
            obj = setParameter(obj,'Rbpb',val,0,'real');
        end
        function res = get.Rbpb(obj)
            res = getParameter(obj,'Rbpb');
        end
        function obj = set.Rbps(obj,val)
            obj = setParameter(obj,'Rbps',val,0,'real');
        end
        function res = get.Rbps(obj)
            res = getParameter(obj,'Rbps');
        end
        function obj = set.Rbpd(obj,val)
            obj = setParameter(obj,'Rbpd',val,0,'real');
        end
        function res = get.Rbpd(obj)
            res = getParameter(obj,'Rbpd');
        end
        function obj = set.Trnqsmod(obj,val)
            obj = setParameter(obj,'Trnqsmod',val,0,'integer');
        end
        function res = get.Trnqsmod(obj)
            res = getParameter(obj,'Trnqsmod');
        end
        function obj = set.Acnqsmod(obj,val)
            obj = setParameter(obj,'Acnqsmod',val,0,'integer');
        end
        function res = get.Acnqsmod(obj)
            res = getParameter(obj,'Acnqsmod');
        end
        function obj = set.Rbodymod(obj,val)
            obj = setParameter(obj,'Rbodymod',val,0,'integer');
        end
        function res = get.Rbodymod(obj)
            res = getParameter(obj,'Rbodymod');
        end
        function obj = set.Rgatemod(obj,val)
            obj = setParameter(obj,'Rgatemod',val,0,'integer');
        end
        function res = get.Rgatemod(obj)
            res = getParameter(obj,'Rgatemod');
        end
        function obj = set.Geomod(obj,val)
            obj = setParameter(obj,'Geomod',val,0,'integer');
        end
        function res = get.Geomod(obj)
            res = getParameter(obj,'Geomod');
        end
        function obj = set.Rgeomod(obj,val)
            obj = setParameter(obj,'Rgeomod',val,0,'integer');
        end
        function res = get.Rgeomod(obj)
            res = getParameter(obj,'Rgeomod');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Sa(obj,val)
            obj = setParameter(obj,'Sa',val,0,'real');
        end
        function res = get.Sa(obj)
            res = getParameter(obj,'Sa');
        end
        function obj = set.Sb(obj,val)
            obj = setParameter(obj,'Sb',val,0,'real');
        end
        function res = get.Sb(obj)
            res = getParameter(obj,'Sb');
        end
        function obj = set.Sd(obj,val)
            obj = setParameter(obj,'Sd',val,0,'real');
        end
        function res = get.Sd(obj)
            res = getParameter(obj,'Sd');
        end
        function obj = set.Delk1(obj,val)
            obj = setParameter(obj,'Delk1',val,0,'real');
        end
        function res = get.Delk1(obj)
            res = getParameter(obj,'Delk1');
        end
        function obj = set.Delvt0(obj,val)
            obj = setParameter(obj,'Delvt0',val,0,'real');
        end
        function res = get.Delvt0(obj)
            res = getParameter(obj,'Delvt0');
        end
        function obj = set.Mulu0(obj,val)
            obj = setParameter(obj,'Mulu0',val,0,'real');
        end
        function res = get.Mulu0(obj)
            res = getParameter(obj,'Mulu0');
        end
        function obj = set.Delnfct(obj,val)
            obj = setParameter(obj,'Delnfct',val,0,'real');
        end
        function res = get.Delnfct(obj)
            res = getParameter(obj,'Delnfct');
        end
        function obj = set.Sc(obj,val)
            obj = setParameter(obj,'Sc',val,0,'real');
        end
        function res = get.Sc(obj)
            res = getParameter(obj,'Sc');
        end
        function obj = set.Sca(obj,val)
            obj = setParameter(obj,'Sca',val,0,'real');
        end
        function res = get.Sca(obj)
            res = getParameter(obj,'Sca');
        end
        function obj = set.Scb(obj,val)
            obj = setParameter(obj,'Scb',val,0,'real');
        end
        function res = get.Scb(obj)
            res = getParameter(obj,'Scb');
        end
        function obj = set.Scc(obj,val)
            obj = setParameter(obj,'Scc',val,0,'real');
        end
        function res = get.Scc(obj)
            res = getParameter(obj,'Scc');
        end
        function obj = set.Xgw(obj,val)
            obj = setParameter(obj,'Xgw',val,0,'real');
        end
        function res = get.Xgw(obj)
            res = getParameter(obj,'Xgw');
        end
        function obj = set.Ngcon(obj,val)
            obj = setParameter(obj,'Ngcon',val,0,'real');
        end
        function res = get.Ngcon(obj)
            res = getParameter(obj,'Ngcon');
        end
        function obj = set.Rsc(obj,val)
            obj = setParameter(obj,'Rsc',val,0,'real');
        end
        function res = get.Rsc(obj)
            res = getParameter(obj,'Rsc');
        end
        function obj = set.Rdc(obj,val)
            obj = setParameter(obj,'Rdc',val,0,'real');
        end
        function res = get.Rdc(obj)
            res = getParameter(obj,'Rdc');
        end
        function obj = set.Off(obj,val)
            obj = setParameter(obj,'Off',val,0,'boolean');
        end
        function res = get.Off(obj)
            res = getParameter(obj,'Off');
        end
        function obj = set.Gds(obj,val)
            obj = setParameter(obj,'Gds',val,0,'real');
        end
        function res = get.Gds(obj)
            res = getParameter(obj,'Gds');
        end
        function obj = set.Gm(obj,val)
            obj = setParameter(obj,'Gm',val,0,'real');
        end
        function res = get.Gm(obj)
            res = getParameter(obj,'Gm');
        end
        function obj = set.Gmbs(obj,val)
            obj = setParameter(obj,'Gmbs',val,0,'real');
        end
        function res = get.Gmbs(obj)
            res = getParameter(obj,'Gmbs');
        end
        function obj = set.Gmr(obj,val)
            obj = setParameter(obj,'Gmr',val,0,'real');
        end
        function res = get.Gmr(obj)
            res = getParameter(obj,'Gmr');
        end
        function obj = set.Gmbsr(obj,val)
            obj = setParameter(obj,'Gmbsr',val,0,'real');
        end
        function res = get.Gmbsr(obj)
            res = getParameter(obj,'Gmbsr');
        end
        function obj = set.Capbd(obj,val)
            obj = setParameter(obj,'Capbd',val,0,'real');
        end
        function res = get.Capbd(obj)
            res = getParameter(obj,'Capbd');
        end
        function obj = set.Capbs(obj,val)
            obj = setParameter(obj,'Capbs',val,0,'real');
        end
        function res = get.Capbs(obj)
            res = getParameter(obj,'Capbs');
        end
        function obj = set.Cggb(obj,val)
            obj = setParameter(obj,'Cggb',val,0,'real');
        end
        function res = get.Cggb(obj)
            res = getParameter(obj,'Cggb');
        end
        function obj = set.Cgdb(obj,val)
            obj = setParameter(obj,'Cgdb',val,0,'real');
        end
        function res = get.Cgdb(obj)
            res = getParameter(obj,'Cgdb');
        end
        function obj = set.Cgsb(obj,val)
            obj = setParameter(obj,'Cgsb',val,0,'real');
        end
        function res = get.Cgsb(obj)
            res = getParameter(obj,'Cgsb');
        end
        function obj = set.Cdgb(obj,val)
            obj = setParameter(obj,'Cdgb',val,0,'real');
        end
        function res = get.Cdgb(obj)
            res = getParameter(obj,'Cdgb');
        end
        function obj = set.Cddb(obj,val)
            obj = setParameter(obj,'Cddb',val,0,'real');
        end
        function res = get.Cddb(obj)
            res = getParameter(obj,'Cddb');
        end
        function obj = set.Cdsb(obj,val)
            obj = setParameter(obj,'Cdsb',val,0,'real');
        end
        function res = get.Cdsb(obj)
            res = getParameter(obj,'Cdsb');
        end
        function obj = set.Cbgb(obj,val)
            obj = setParameter(obj,'Cbgb',val,0,'real');
        end
        function res = get.Cbgb(obj)
            res = getParameter(obj,'Cbgb');
        end
        function obj = set.Cbdb(obj,val)
            obj = setParameter(obj,'Cbdb',val,0,'real');
        end
        function res = get.Cbdb(obj)
            res = getParameter(obj,'Cbdb');
        end
        function obj = set.Cbsb(obj,val)
            obj = setParameter(obj,'Cbsb',val,0,'real');
        end
        function res = get.Cbsb(obj)
            res = getParameter(obj,'Cbsb');
        end
        function obj = set.Csgb(obj,val)
            obj = setParameter(obj,'Csgb',val,0,'real');
        end
        function res = get.Csgb(obj)
            res = getParameter(obj,'Csgb');
        end
        function obj = set.Csdb(obj,val)
            obj = setParameter(obj,'Csdb',val,0,'real');
        end
        function res = get.Csdb(obj)
            res = getParameter(obj,'Csdb');
        end
        function obj = set.Cssb(obj,val)
            obj = setParameter(obj,'Cssb',val,0,'real');
        end
        function res = get.Cssb(obj)
            res = getParameter(obj,'Cssb');
        end
        function obj = set.Qg(obj,val)
            obj = setParameter(obj,'Qg',val,0,'real');
        end
        function res = get.Qg(obj)
            res = getParameter(obj,'Qg');
        end
        function obj = set.Qd(obj,val)
            obj = setParameter(obj,'Qd',val,0,'real');
        end
        function res = get.Qd(obj)
            res = getParameter(obj,'Qd');
        end
        function obj = set.Qb(obj,val)
            obj = setParameter(obj,'Qb',val,0,'real');
        end
        function res = get.Qb(obj)
            res = getParameter(obj,'Qb');
        end
        function obj = set.NMOS(obj,val)
            obj = setParameter(obj,'NMOS',val,0,'boolean');
        end
        function res = get.NMOS(obj)
            res = getParameter(obj,'NMOS');
        end
        function obj = set.PMOS(obj,val)
            obj = setParameter(obj,'PMOS',val,0,'boolean');
        end
        function res = get.PMOS(obj)
            res = getParameter(obj,'PMOS');
        end
        function obj = set.Qdef(obj,val)
            obj = setParameter(obj,'Qdef',val,0,'real');
        end
        function res = get.Qdef(obj)
            res = getParameter(obj,'Qdef');
        end
        function obj = set.Gtau(obj,val)
            obj = setParameter(obj,'Gtau',val,0,'real');
        end
        function res = get.Gtau(obj)
            res = getParameter(obj,'Gtau');
        end
        function obj = set.Gcrg(obj,val)
            obj = setParameter(obj,'Gcrg',val,0,'real');
        end
        function res = get.Gcrg(obj)
            res = getParameter(obj,'Gcrg');
        end
    end
end
