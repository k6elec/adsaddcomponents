classdef ADS_DRC < ADScomponent
    % ADS_DRC matlab representation for the ADS DRC component
    % Ideal distributed RC line
    % DRC [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Impedance of distributed RC line (Smorr) Unit: oh/m
        R
        % Capacitance of distributed RC line (Smorr) Unit: f/m
        C
        % Length of distributed RC line (Smorr) Unit: m
        L
    end
    methods
        function obj = set.R(obj,val)
            obj = setParameter(obj,'R',val,0,'real');
        end
        function res = get.R(obj)
            res = getParameter(obj,'R');
        end
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,0,'real');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
    end
end
