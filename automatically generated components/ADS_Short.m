classdef ADS_Short < ADScomponent
    % ADS_Short matlab representation for the ADS Short component
    % Short Circuits
    % Short [:Name] sink source
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % 0 => short, >0 => DC block, <0 => DC feed (s--ri) 
        Mode
        % DC block capacitance (transient analysis only) (s---r) Unit: F
        C
        % DC feed inductance (transient analysis only) (s---r) Unit: H
        L
        % Current gain (s---r) 
        Gain
        % Send source current to rawfile (s--rb) 
        SaveCurrent
        % Maximum current (warning) (s---r) Unit: A
        wImax
    end
    methods
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,0,'real');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Gain(obj,val)
            obj = setParameter(obj,'Gain',val,0,'real');
        end
        function res = get.Gain(obj)
            res = getParameter(obj,'Gain');
        end
        function obj = set.SaveCurrent(obj,val)
            obj = setParameter(obj,'SaveCurrent',val,0,'boolean');
        end
        function res = get.SaveCurrent(obj)
            res = getParameter(obj,'SaveCurrent');
        end
        function obj = set.wImax(obj,val)
            obj = setParameter(obj,'wImax',val,0,'real');
        end
        function res = get.wImax(obj)
            res = getParameter(obj,'wImax');
        end
    end
end
