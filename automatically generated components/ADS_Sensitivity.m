classdef ADS_Sensitivity < ADSnodeless
    % ADS_Sensitivity matlab representation for the ADS Sensitivity component
    % Nominal sensitivity (using OptPak)
    % Sensitivity [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Type of sensitivity... (s---s) 
        SensType
        % Error function formulation: L1, L2, mm, neg_L2... (s---s) 
        ErrorForm
        % degree of annotation (s---i) 
        StatusLevel
        % Flag to send nominal analysis solutions to dataset (s---b) 
        SaveNominal
        % Flag to send analysis solutions to dataset (s---b) 
        SaveSolns
        % Send opt var values to dataset (s---b) 
        SaveOptimVars
        % Send optgoal values to dataset (s---b) 
        SaveGoals
        % Send current error function value to dataset (s---b) 
        SaveCurrentEF
        % Update dataset on each iteration (s---b) 
        UpdateDataset
        % Enable controller (sm--b) 
        Enable
        % Flag to indicate use of all optimization variables (s---b) 
        UseAllOptVars
        % Name of variable(s) to use in optimization (repeatable) (s---s) 
        OptVar
        % Flag to indicate use of all Goals (s---b) 
        UseAllGoals
        % Name of goal(s) to use in optimization (repeatable) (s---s) 
        GoalName
        % Recover Error Limit (s---i) 
        RecoverErrorLimit
        % Debug level (s---i) 
        DebugLevel
    end
    methods
        function obj = set.SensType(obj,val)
            obj = setParameter(obj,'SensType',val,0,'string');
        end
        function res = get.SensType(obj)
            res = getParameter(obj,'SensType');
        end
        function obj = set.ErrorForm(obj,val)
            obj = setParameter(obj,'ErrorForm',val,0,'string');
        end
        function res = get.ErrorForm(obj)
            res = getParameter(obj,'ErrorForm');
        end
        function obj = set.StatusLevel(obj,val)
            obj = setParameter(obj,'StatusLevel',val,0,'integer');
        end
        function res = get.StatusLevel(obj)
            res = getParameter(obj,'StatusLevel');
        end
        function obj = set.SaveNominal(obj,val)
            obj = setParameter(obj,'SaveNominal',val,0,'boolean');
        end
        function res = get.SaveNominal(obj)
            res = getParameter(obj,'SaveNominal');
        end
        function obj = set.SaveSolns(obj,val)
            obj = setParameter(obj,'SaveSolns',val,0,'boolean');
        end
        function res = get.SaveSolns(obj)
            res = getParameter(obj,'SaveSolns');
        end
        function obj = set.SaveOptimVars(obj,val)
            obj = setParameter(obj,'SaveOptimVars',val,0,'boolean');
        end
        function res = get.SaveOptimVars(obj)
            res = getParameter(obj,'SaveOptimVars');
        end
        function obj = set.SaveGoals(obj,val)
            obj = setParameter(obj,'SaveGoals',val,0,'boolean');
        end
        function res = get.SaveGoals(obj)
            res = getParameter(obj,'SaveGoals');
        end
        function obj = set.SaveCurrentEF(obj,val)
            obj = setParameter(obj,'SaveCurrentEF',val,0,'boolean');
        end
        function res = get.SaveCurrentEF(obj)
            res = getParameter(obj,'SaveCurrentEF');
        end
        function obj = set.UpdateDataset(obj,val)
            obj = setParameter(obj,'UpdateDataset',val,0,'boolean');
        end
        function res = get.UpdateDataset(obj)
            res = getParameter(obj,'UpdateDataset');
        end
        function obj = set.Enable(obj,val)
            obj = setParameter(obj,'Enable',val,0,'boolean');
        end
        function res = get.Enable(obj)
            res = getParameter(obj,'Enable');
        end
        function obj = set.UseAllOptVars(obj,val)
            obj = setParameter(obj,'UseAllOptVars',val,0,'boolean');
        end
        function res = get.UseAllOptVars(obj)
            res = getParameter(obj,'UseAllOptVars');
        end
        function obj = set.OptVar(obj,val)
            obj = setParameter(obj,'OptVar',val,1,'string');
        end
        function res = get.OptVar(obj)
            res = getParameter(obj,'OptVar');
        end
        function obj = set.UseAllGoals(obj,val)
            obj = setParameter(obj,'UseAllGoals',val,0,'boolean');
        end
        function res = get.UseAllGoals(obj)
            res = getParameter(obj,'UseAllGoals');
        end
        function obj = set.GoalName(obj,val)
            obj = setParameter(obj,'GoalName',val,1,'string');
        end
        function res = get.GoalName(obj)
            res = getParameter(obj,'GoalName');
        end
        function obj = set.RecoverErrorLimit(obj,val)
            obj = setParameter(obj,'RecoverErrorLimit',val,0,'integer');
        end
        function res = get.RecoverErrorLimit(obj)
            res = getParameter(obj,'RecoverErrorLimit');
        end
        function obj = set.DebugLevel(obj,val)
            obj = setParameter(obj,'DebugLevel',val,0,'integer');
        end
        function res = get.DebugLevel(obj)
            res = getParameter(obj,'DebugLevel');
        end
    end
end
