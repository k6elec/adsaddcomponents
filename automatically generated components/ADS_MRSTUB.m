classdef ADS_MRSTUB < ADScomponent
    % ADS_MRSTUB matlab representation for the ADS MRSTUB component
    % Microstrip Radial Stub
    % MRSTUB [:Name] n1
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % Width of Input Line (Smorr) Unit: m
        Wi
        % Length of Stub (Smorr) Unit: m
        L
        % Angle subtended by stub (Smorr) Unit: deg
        Angle
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.Wi(obj,val)
            obj = setParameter(obj,'Wi',val,0,'real');
        end
        function res = get.Wi(obj)
            res = getParameter(obj,'Wi');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Angle(obj,val)
            obj = setParameter(obj,'Angle',val,0,'real');
        end
        function res = get.Angle(obj)
            res = getParameter(obj,'Angle');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end
