classdef ADS_Budget < ADSnodeless
    % ADS_Budget matlab representation for the ADS Budget component
    % Budget Controller
    % Budget [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Module Name (s---s) 
        Module
        % Module Type (s---s) 
        Type
    end
    methods
        function obj = set.Module(obj,val)
            obj = setParameter(obj,'Module',val,0,'string');
        end
        function res = get.Module(obj)
            res = getParameter(obj,'Module');
        end
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'string');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
    end
end
