classdef ADS_Tran < ADSnodeless
    % ADS_Tran matlab representation for the ADS Tran component
    % transient analysis
    % Tran [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Final time in this analysis (s---r) 
        StopTime
        % Maximum time step in this analysis (s---r) 
        MaxTimeStep
        % Starting time to output time point results (s---r) 
        StartTime
        % Minimum time step in this analysis (s---r) 
        MinTimeStep
        % Time step control methods: 0=fixed 1=iter count 2=trunc error 3=GG dV/dt 4=GG dV/dt+polynomial fit (s---i) 
        TimeStepControl
        % Levels of subcircuits to output (sm-ri) 
        NestLevel
        % Degree of annotation (sm-ri) 
        StatusLevel
        % Variable or parameter to output (repeatable) (sm-rs) 
        OutVar
        % Levels of devices DC Operating Point Data to output (s---i) 
        DevOpPtLevel
        % Frequency of fundamentals (s---r) Unit: Hz
        Freq
        % Integration methods: 0=trapezoidal or 1=Gear (s---i) 
        IntegMethod
        % Maximum order for Gear integration (s---i) 
        MaxGearOrder
        % Coefficient for mixing trapezoidal and backward Euler (s---r) 
        Mu
        % Local truncation error overestimation factor (s---r) 
        TruncTol
        % Charge tolerance in LTE computation (s---i) 
        ChargeTol
        % Limit timestep to half of minimum transmission line delay (s---i) 
        LimitStepForTL
        % Shortest transmission line delay to simulate (s---r) 
        ShortTL_Delay
        % Maximum number of iterations per time step (s---i) 
        MaxIters
        % Maximum number of iterations at dc analysis before stepping source (s---i) 
        MaxItersDC
        % Final Gnode at which DC convergence is accepted (s---r) 
        GnodeFinal
        % Starting algorithm for DC convergence (s---i) 
        DefaultConvStart
        % Use user specified initial condition (s---i) 
        UseInitCond
        % Connect every node to ground with a big resistor at dc analysis (s---b) 
        LoadGminDC
        % Perform KCL check for convergence (s---b) 
        CheckKCL
        % Check only delta voltage for convergence (s---b) 
        CheckOnlyDeltaV
        % Check for strange behavior at every iteration (s---b) 
        OverloadAlert
        % Skip device evaluation if voltage changes are small between iterations (s---b) 
        DeviceBypass
        % Use approximate linear models instead of using full convolution (s---b) 
        ImpApprox
        % Maximum frequency for device evaluation (s--rr) Unit: Hz
        ImpMaxFreq
        % Sample spacing in frequency (s--rr) Unit: Hz
        ImpDeltaFreq
        % Maximum allowed points in impulse response (s---i) 
        ImpMaxPts
        % Relative impulse Response truncation factor (s---r) 
        ImpRelTrunc
        % Absolute impulse Response truncation factor (s---r) 
        ImpAbsTrunc
        % Interpolation order during the convolution (s---i) 
        ImpInterpOrder
        % Convolution mode (s---i) 
        ImpMode
        % Smoothing Window (s---i) 
        ImpWindow
        % Non-causal function impulse response order (s---i) 
        ImpNoncausalLength
        % Transient Model for coupled PCB lines (s---i) 
        PCB_LineModel
        % Output all internal time point results (s---i) 
        OutputAllPoints
        % Minimum capacitance to add to every node (s---r) 
        Cmin
        % Maximum combined order for Fourier analysis (s---i) 
        HB_MaxOrder
        % Maximum combined order for Fourier analysis (s---i) 
        MaxOrder
        % Maximum order of fundamenal for Fourier analysis (sm--i) 
        HB_Order
        % Maximum order of fundamenal for Fourier analysis (sm--i) 
        Order
        % File name for Fourier solution (sm-rs) 
        HB_OutFile
        % Generate an Harmonic Balance solution file (s---b) 
        HB_Sol
        % Apply Blackman window to Fourier analysis (s---i) 
        HB_Window
        % Save final Transient solution in DC solution file (s---b) 
        DC_Sol
        % OutputPlan Name (s---s) 
        OutputPlan
        % Noise bandwidth (s---r) 
        NoiseBandwidth
        % Noise scaling factor (s---r) 
        NoiseScale
        % Save output during cosimulation (s---b) 
        SaveCosimOutput
        % Relative voltage and current tolerances (s---r) 
        IV_RelTol
        % Enable steady state detection (s---i) 
        SteadyState
        % Earliest time to start steady state detection (s---r) Unit: s
        SteadyStateMinTime
        % Commensurate frequency for steady state (s---r) Unit: Hz
        CommensurateFreq
        % Turn Analysis output to Dataset on or off (s---i) 
        SaveToDataset
        % Enforce passivity (s---b) 
        ImpEnforcePassivity
        % Enforce strict passivity (s---b) 
        ImpEnforceStrictPassivity
        % Save spectrum and impulse response (s---b) 
        ImpSaveSpectrum
        % L2 norm reductioin tolerance for impulse thresholding (s---r) 
        ImpL2Reduction
        % Memory limit for impulse calculatioin (s---r) 
        ImpMemLimit
        % Cache impulse response (s---b) 
        ImpCache
        % Transient speedup [0-4] default=1 (s---i) 
        SpeedUp
        % Number of passes to make during impulse generation for large port count (default=0=auto) (s---i) 
        ImpPasses
        % HB Analysis Name for TAHB (sm--s) 
        HB_Analysis
    end
    methods
        function obj = set.StopTime(obj,val)
            obj = setParameter(obj,'StopTime',val,0,'real');
        end
        function res = get.StopTime(obj)
            res = getParameter(obj,'StopTime');
        end
        function obj = set.MaxTimeStep(obj,val)
            obj = setParameter(obj,'MaxTimeStep',val,0,'real');
        end
        function res = get.MaxTimeStep(obj)
            res = getParameter(obj,'MaxTimeStep');
        end
        function obj = set.StartTime(obj,val)
            obj = setParameter(obj,'StartTime',val,0,'real');
        end
        function res = get.StartTime(obj)
            res = getParameter(obj,'StartTime');
        end
        function obj = set.MinTimeStep(obj,val)
            obj = setParameter(obj,'MinTimeStep',val,0,'real');
        end
        function res = get.MinTimeStep(obj)
            res = getParameter(obj,'MinTimeStep');
        end
        function obj = set.TimeStepControl(obj,val)
            obj = setParameter(obj,'TimeStepControl',val,0,'integer');
        end
        function res = get.TimeStepControl(obj)
            res = getParameter(obj,'TimeStepControl');
        end
        function obj = set.NestLevel(obj,val)
            obj = setParameter(obj,'NestLevel',val,0,'integer');
        end
        function res = get.NestLevel(obj)
            res = getParameter(obj,'NestLevel');
        end
        function obj = set.StatusLevel(obj,val)
            obj = setParameter(obj,'StatusLevel',val,0,'integer');
        end
        function res = get.StatusLevel(obj)
            res = getParameter(obj,'StatusLevel');
        end
        function obj = set.OutVar(obj,val)
            obj = setParameter(obj,'OutVar',val,0,'string');
        end
        function res = get.OutVar(obj)
            res = getParameter(obj,'OutVar');
        end
        function obj = set.DevOpPtLevel(obj,val)
            obj = setParameter(obj,'DevOpPtLevel',val,0,'integer');
        end
        function res = get.DevOpPtLevel(obj)
            res = getParameter(obj,'DevOpPtLevel');
        end
        function obj = set.Freq(obj,val)
            obj = setParameter(obj,'Freq',val,1,'real');
        end
        function res = get.Freq(obj)
            res = getParameter(obj,'Freq');
        end
        function obj = set.IntegMethod(obj,val)
            obj = setParameter(obj,'IntegMethod',val,0,'integer');
        end
        function res = get.IntegMethod(obj)
            res = getParameter(obj,'IntegMethod');
        end
        function obj = set.MaxGearOrder(obj,val)
            obj = setParameter(obj,'MaxGearOrder',val,0,'integer');
        end
        function res = get.MaxGearOrder(obj)
            res = getParameter(obj,'MaxGearOrder');
        end
        function obj = set.Mu(obj,val)
            obj = setParameter(obj,'Mu',val,0,'real');
        end
        function res = get.Mu(obj)
            res = getParameter(obj,'Mu');
        end
        function obj = set.TruncTol(obj,val)
            obj = setParameter(obj,'TruncTol',val,0,'real');
        end
        function res = get.TruncTol(obj)
            res = getParameter(obj,'TruncTol');
        end
        function obj = set.ChargeTol(obj,val)
            obj = setParameter(obj,'ChargeTol',val,0,'integer');
        end
        function res = get.ChargeTol(obj)
            res = getParameter(obj,'ChargeTol');
        end
        function obj = set.LimitStepForTL(obj,val)
            obj = setParameter(obj,'LimitStepForTL',val,0,'integer');
        end
        function res = get.LimitStepForTL(obj)
            res = getParameter(obj,'LimitStepForTL');
        end
        function obj = set.ShortTL_Delay(obj,val)
            obj = setParameter(obj,'ShortTL_Delay',val,0,'real');
        end
        function res = get.ShortTL_Delay(obj)
            res = getParameter(obj,'ShortTL_Delay');
        end
        function obj = set.MaxIters(obj,val)
            obj = setParameter(obj,'MaxIters',val,0,'integer');
        end
        function res = get.MaxIters(obj)
            res = getParameter(obj,'MaxIters');
        end
        function obj = set.MaxItersDC(obj,val)
            obj = setParameter(obj,'MaxItersDC',val,0,'integer');
        end
        function res = get.MaxItersDC(obj)
            res = getParameter(obj,'MaxItersDC');
        end
        function obj = set.GnodeFinal(obj,val)
            obj = setParameter(obj,'GnodeFinal',val,0,'real');
        end
        function res = get.GnodeFinal(obj)
            res = getParameter(obj,'GnodeFinal');
        end
        function obj = set.DefaultConvStart(obj,val)
            obj = setParameter(obj,'DefaultConvStart',val,0,'integer');
        end
        function res = get.DefaultConvStart(obj)
            res = getParameter(obj,'DefaultConvStart');
        end
        function obj = set.UseInitCond(obj,val)
            obj = setParameter(obj,'UseInitCond',val,0,'integer');
        end
        function res = get.UseInitCond(obj)
            res = getParameter(obj,'UseInitCond');
        end
        function obj = set.LoadGminDC(obj,val)
            obj = setParameter(obj,'LoadGminDC',val,0,'boolean');
        end
        function res = get.LoadGminDC(obj)
            res = getParameter(obj,'LoadGminDC');
        end
        function obj = set.CheckKCL(obj,val)
            obj = setParameter(obj,'CheckKCL',val,0,'boolean');
        end
        function res = get.CheckKCL(obj)
            res = getParameter(obj,'CheckKCL');
        end
        function obj = set.CheckOnlyDeltaV(obj,val)
            obj = setParameter(obj,'CheckOnlyDeltaV',val,0,'boolean');
        end
        function res = get.CheckOnlyDeltaV(obj)
            res = getParameter(obj,'CheckOnlyDeltaV');
        end
        function obj = set.OverloadAlert(obj,val)
            obj = setParameter(obj,'OverloadAlert',val,0,'boolean');
        end
        function res = get.OverloadAlert(obj)
            res = getParameter(obj,'OverloadAlert');
        end
        function obj = set.DeviceBypass(obj,val)
            obj = setParameter(obj,'DeviceBypass',val,0,'boolean');
        end
        function res = get.DeviceBypass(obj)
            res = getParameter(obj,'DeviceBypass');
        end
        function obj = set.ImpApprox(obj,val)
            obj = setParameter(obj,'ImpApprox',val,0,'boolean');
        end
        function res = get.ImpApprox(obj)
            res = getParameter(obj,'ImpApprox');
        end
        function obj = set.ImpMaxFreq(obj,val)
            obj = setParameter(obj,'ImpMaxFreq',val,0,'real');
        end
        function res = get.ImpMaxFreq(obj)
            res = getParameter(obj,'ImpMaxFreq');
        end
        function obj = set.ImpDeltaFreq(obj,val)
            obj = setParameter(obj,'ImpDeltaFreq',val,0,'real');
        end
        function res = get.ImpDeltaFreq(obj)
            res = getParameter(obj,'ImpDeltaFreq');
        end
        function obj = set.ImpMaxPts(obj,val)
            obj = setParameter(obj,'ImpMaxPts',val,0,'integer');
        end
        function res = get.ImpMaxPts(obj)
            res = getParameter(obj,'ImpMaxPts');
        end
        function obj = set.ImpRelTrunc(obj,val)
            obj = setParameter(obj,'ImpRelTrunc',val,0,'real');
        end
        function res = get.ImpRelTrunc(obj)
            res = getParameter(obj,'ImpRelTrunc');
        end
        function obj = set.ImpAbsTrunc(obj,val)
            obj = setParameter(obj,'ImpAbsTrunc',val,0,'real');
        end
        function res = get.ImpAbsTrunc(obj)
            res = getParameter(obj,'ImpAbsTrunc');
        end
        function obj = set.ImpInterpOrder(obj,val)
            obj = setParameter(obj,'ImpInterpOrder',val,0,'integer');
        end
        function res = get.ImpInterpOrder(obj)
            res = getParameter(obj,'ImpInterpOrder');
        end
        function obj = set.ImpMode(obj,val)
            obj = setParameter(obj,'ImpMode',val,0,'integer');
        end
        function res = get.ImpMode(obj)
            res = getParameter(obj,'ImpMode');
        end
        function obj = set.ImpWindow(obj,val)
            obj = setParameter(obj,'ImpWindow',val,0,'integer');
        end
        function res = get.ImpWindow(obj)
            res = getParameter(obj,'ImpWindow');
        end
        function obj = set.ImpNoncausalLength(obj,val)
            obj = setParameter(obj,'ImpNoncausalLength',val,0,'integer');
        end
        function res = get.ImpNoncausalLength(obj)
            res = getParameter(obj,'ImpNoncausalLength');
        end
        function obj = set.PCB_LineModel(obj,val)
            obj = setParameter(obj,'PCB_LineModel',val,0,'integer');
        end
        function res = get.PCB_LineModel(obj)
            res = getParameter(obj,'PCB_LineModel');
        end
        function obj = set.OutputAllPoints(obj,val)
            obj = setParameter(obj,'OutputAllPoints',val,0,'integer');
        end
        function res = get.OutputAllPoints(obj)
            res = getParameter(obj,'OutputAllPoints');
        end
        function obj = set.Cmin(obj,val)
            obj = setParameter(obj,'Cmin',val,0,'real');
        end
        function res = get.Cmin(obj)
            res = getParameter(obj,'Cmin');
        end
        function obj = set.HB_MaxOrder(obj,val)
            obj = setParameter(obj,'HB_MaxOrder',val,0,'integer');
        end
        function res = get.HB_MaxOrder(obj)
            res = getParameter(obj,'HB_MaxOrder');
        end
        function obj = set.MaxOrder(obj,val)
            obj = setParameter(obj,'MaxOrder',val,0,'integer');
        end
        function res = get.MaxOrder(obj)
            res = getParameter(obj,'MaxOrder');
        end
        function obj = set.HB_Order(obj,val)
            obj = setParameter(obj,'HB_Order',val,1,'integer');
        end
        function res = get.HB_Order(obj)
            res = getParameter(obj,'HB_Order');
        end
        function obj = set.Order(obj,val)
            obj = setParameter(obj,'Order',val,1,'integer');
        end
        function res = get.Order(obj)
            res = getParameter(obj,'Order');
        end
        function obj = set.HB_OutFile(obj,val)
            obj = setParameter(obj,'HB_OutFile',val,0,'string');
        end
        function res = get.HB_OutFile(obj)
            res = getParameter(obj,'HB_OutFile');
        end
        function obj = set.HB_Sol(obj,val)
            obj = setParameter(obj,'HB_Sol',val,0,'boolean');
        end
        function res = get.HB_Sol(obj)
            res = getParameter(obj,'HB_Sol');
        end
        function obj = set.HB_Window(obj,val)
            obj = setParameter(obj,'HB_Window',val,0,'integer');
        end
        function res = get.HB_Window(obj)
            res = getParameter(obj,'HB_Window');
        end
        function obj = set.DC_Sol(obj,val)
            obj = setParameter(obj,'DC_Sol',val,0,'boolean');
        end
        function res = get.DC_Sol(obj)
            res = getParameter(obj,'DC_Sol');
        end
        function obj = set.OutputPlan(obj,val)
            obj = setParameter(obj,'OutputPlan',val,0,'string');
        end
        function res = get.OutputPlan(obj)
            res = getParameter(obj,'OutputPlan');
        end
        function obj = set.NoiseBandwidth(obj,val)
            obj = setParameter(obj,'NoiseBandwidth',val,0,'real');
        end
        function res = get.NoiseBandwidth(obj)
            res = getParameter(obj,'NoiseBandwidth');
        end
        function obj = set.NoiseScale(obj,val)
            obj = setParameter(obj,'NoiseScale',val,0,'real');
        end
        function res = get.NoiseScale(obj)
            res = getParameter(obj,'NoiseScale');
        end
        function obj = set.SaveCosimOutput(obj,val)
            obj = setParameter(obj,'SaveCosimOutput',val,0,'boolean');
        end
        function res = get.SaveCosimOutput(obj)
            res = getParameter(obj,'SaveCosimOutput');
        end
        function obj = set.IV_RelTol(obj,val)
            obj = setParameter(obj,'IV_RelTol',val,0,'real');
        end
        function res = get.IV_RelTol(obj)
            res = getParameter(obj,'IV_RelTol');
        end
        function obj = set.SteadyState(obj,val)
            obj = setParameter(obj,'SteadyState',val,0,'integer');
        end
        function res = get.SteadyState(obj)
            res = getParameter(obj,'SteadyState');
        end
        function obj = set.SteadyStateMinTime(obj,val)
            obj = setParameter(obj,'SteadyStateMinTime',val,0,'real');
        end
        function res = get.SteadyStateMinTime(obj)
            res = getParameter(obj,'SteadyStateMinTime');
        end
        function obj = set.CommensurateFreq(obj,val)
            obj = setParameter(obj,'CommensurateFreq',val,0,'real');
        end
        function res = get.CommensurateFreq(obj)
            res = getParameter(obj,'CommensurateFreq');
        end
        function obj = set.SaveToDataset(obj,val)
            obj = setParameter(obj,'SaveToDataset',val,0,'integer');
        end
        function res = get.SaveToDataset(obj)
            res = getParameter(obj,'SaveToDataset');
        end
        function obj = set.ImpEnforcePassivity(obj,val)
            obj = setParameter(obj,'ImpEnforcePassivity',val,0,'boolean');
        end
        function res = get.ImpEnforcePassivity(obj)
            res = getParameter(obj,'ImpEnforcePassivity');
        end
        function obj = set.ImpEnforceStrictPassivity(obj,val)
            obj = setParameter(obj,'ImpEnforceStrictPassivity',val,0,'boolean');
        end
        function res = get.ImpEnforceStrictPassivity(obj)
            res = getParameter(obj,'ImpEnforceStrictPassivity');
        end
        function obj = set.ImpSaveSpectrum(obj,val)
            obj = setParameter(obj,'ImpSaveSpectrum',val,0,'boolean');
        end
        function res = get.ImpSaveSpectrum(obj)
            res = getParameter(obj,'ImpSaveSpectrum');
        end
        function obj = set.ImpL2Reduction(obj,val)
            obj = setParameter(obj,'ImpL2Reduction',val,0,'real');
        end
        function res = get.ImpL2Reduction(obj)
            res = getParameter(obj,'ImpL2Reduction');
        end
        function obj = set.ImpMemLimit(obj,val)
            obj = setParameter(obj,'ImpMemLimit',val,0,'real');
        end
        function res = get.ImpMemLimit(obj)
            res = getParameter(obj,'ImpMemLimit');
        end
        function obj = set.ImpCache(obj,val)
            obj = setParameter(obj,'ImpCache',val,0,'boolean');
        end
        function res = get.ImpCache(obj)
            res = getParameter(obj,'ImpCache');
        end
        function obj = set.SpeedUp(obj,val)
            obj = setParameter(obj,'SpeedUp',val,0,'integer');
        end
        function res = get.SpeedUp(obj)
            res = getParameter(obj,'SpeedUp');
        end
        function obj = set.ImpPasses(obj,val)
            obj = setParameter(obj,'ImpPasses',val,0,'integer');
        end
        function res = get.ImpPasses(obj)
            res = getParameter(obj,'ImpPasses');
        end
        function obj = set.HB_Analysis(obj,val)
            obj = setParameter(obj,'HB_Analysis',val,0,'string');
        end
        function res = get.HB_Analysis(obj)
            res = getParameter(obj,'HB_Analysis');
        end
    end
end
