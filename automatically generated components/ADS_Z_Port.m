classdef ADS_Z_Port < ADScomponent
    % ADS_Z_Port matlab representation for the ADS Z_Port component
    % User Defined Linear Z Parameter N Port
    % Z_Port [:Name] p1 n1 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Z Parameter (smorc) Unit: Ohms
        Z
        % Controlling current (s---d) 
        C
        % Port is reciprocal (s---b) 
        Recip
        % SYM array pointer to all Z parameters (smo-c) 
        All
        % Temperature in degrees Celsius (smorr) Unit: deg C
        Temp
        % Non-causal function impulse response order (sm--i) 
        ImpNoncausalLength
        % Convolution mode (sm--i) 
        ImpMode
        % Maximum Frequency to which device is evaluated (smo-r) Unit: Hz
        ImpMaxFreq
        % Sample spacing in frequency (smo-r) Unit: Hz
        ImpDeltaFreq
        % maximum allowed impulse response order (sm--i) 
        ImpMaxOrder
        % Smoothing Window (sm--i) 
        ImpWindow
        % Relative impulse Response truncation factor (smo-r) 
        ImpRelTol
        % Absolute impulse Response truncation factor (smo-r) 
        ImpAbsTol
        % Minimum frequency of data file (smo-r) Unit: Hz
        MinFileFreq
        % Maximum frequency of data file (smo-r) Unit: Hz
        MaxFileFreq
        % Enforce passivity (s---b) 
        EnforcePassivity
        % Envelope fitting bandwidth override (Hz) (smo-r) Unit: Hz
        EnvFitBwHz
    end
    methods
        function obj = set.Z(obj,val)
            obj = setParameter(obj,'Z',val,2,'complex');
        end
        function res = get.Z(obj)
            res = getParameter(obj,'Z');
        end
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,1,'string');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.Recip(obj,val)
            obj = setParameter(obj,'Recip',val,0,'boolean');
        end
        function res = get.Recip(obj)
            res = getParameter(obj,'Recip');
        end
        function obj = set.All(obj,val)
            obj = setParameter(obj,'All',val,0,'complex');
        end
        function res = get.All(obj)
            res = getParameter(obj,'All');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.ImpNoncausalLength(obj,val)
            obj = setParameter(obj,'ImpNoncausalLength',val,0,'integer');
        end
        function res = get.ImpNoncausalLength(obj)
            res = getParameter(obj,'ImpNoncausalLength');
        end
        function obj = set.ImpMode(obj,val)
            obj = setParameter(obj,'ImpMode',val,0,'integer');
        end
        function res = get.ImpMode(obj)
            res = getParameter(obj,'ImpMode');
        end
        function obj = set.ImpMaxFreq(obj,val)
            obj = setParameter(obj,'ImpMaxFreq',val,0,'real');
        end
        function res = get.ImpMaxFreq(obj)
            res = getParameter(obj,'ImpMaxFreq');
        end
        function obj = set.ImpDeltaFreq(obj,val)
            obj = setParameter(obj,'ImpDeltaFreq',val,0,'real');
        end
        function res = get.ImpDeltaFreq(obj)
            res = getParameter(obj,'ImpDeltaFreq');
        end
        function obj = set.ImpMaxOrder(obj,val)
            obj = setParameter(obj,'ImpMaxOrder',val,0,'integer');
        end
        function res = get.ImpMaxOrder(obj)
            res = getParameter(obj,'ImpMaxOrder');
        end
        function obj = set.ImpWindow(obj,val)
            obj = setParameter(obj,'ImpWindow',val,0,'integer');
        end
        function res = get.ImpWindow(obj)
            res = getParameter(obj,'ImpWindow');
        end
        function obj = set.ImpRelTol(obj,val)
            obj = setParameter(obj,'ImpRelTol',val,0,'real');
        end
        function res = get.ImpRelTol(obj)
            res = getParameter(obj,'ImpRelTol');
        end
        function obj = set.ImpAbsTol(obj,val)
            obj = setParameter(obj,'ImpAbsTol',val,0,'real');
        end
        function res = get.ImpAbsTol(obj)
            res = getParameter(obj,'ImpAbsTol');
        end
        function obj = set.MinFileFreq(obj,val)
            obj = setParameter(obj,'MinFileFreq',val,0,'real');
        end
        function res = get.MinFileFreq(obj)
            res = getParameter(obj,'MinFileFreq');
        end
        function obj = set.MaxFileFreq(obj,val)
            obj = setParameter(obj,'MaxFileFreq',val,0,'real');
        end
        function res = get.MaxFileFreq(obj)
            res = getParameter(obj,'MaxFileFreq');
        end
        function obj = set.EnforcePassivity(obj,val)
            obj = setParameter(obj,'EnforcePassivity',val,0,'boolean');
        end
        function res = get.EnforcePassivity(obj)
            res = getParameter(obj,'EnforcePassivity');
        end
        function obj = set.EnvFitBwHz(obj,val)
            obj = setParameter(obj,'EnvFitBwHz',val,0,'real');
        end
        function res = get.EnvFitBwHz(obj)
            res = getParameter(obj,'EnvFitBwHz');
        end
    end
end
