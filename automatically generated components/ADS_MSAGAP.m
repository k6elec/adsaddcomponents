classdef ADS_MSAGAP < ADScomponent
    % ADS_MSAGAP matlab representation for the ADS MSAGAP component
    % Microstrip Asymmetric Gap
    % MSAGAP [:Name] t1 t2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Line Width at terminal 1 (smorr) Unit: m
        W1
        % Line Width at terminal 2 (smorr) Unit: m
        W2
        % Gap (smorr) Unit: m
        G
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.W1(obj,val)
            obj = setParameter(obj,'W1',val,0,'real');
        end
        function res = get.W1(obj)
            res = getParameter(obj,'W1');
        end
        function obj = set.W2(obj,val)
            obj = setParameter(obj,'W2',val,0,'real');
        end
        function res = get.W2(obj)
            res = getParameter(obj,'W2');
        end
        function obj = set.G(obj,val)
            obj = setParameter(obj,'G',val,0,'real');
        end
        function res = get.G(obj)
            res = getParameter(obj,'G');
        end
    end
end
