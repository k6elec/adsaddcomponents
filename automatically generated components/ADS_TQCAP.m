classdef ADS_TQCAP < ADScomponent
    % ADS_TQCAP matlab representation for the ADS TQCAP component
    % Triquint GaAs IC Capacitor
    % TQCAP [:Name] t1 t2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Capacitance Value (smorr) Unit: F
        C
        % GaAs Substrate Height (s---r) Unit: m
        H
    end
    methods
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,0,'real');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.H(obj,val)
            obj = setParameter(obj,'H',val,0,'real');
        end
        function res = get.H(obj)
            res = getParameter(obj,'H');
        end
    end
end
