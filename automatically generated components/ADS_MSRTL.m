classdef ADS_MSRTL < ADScomponent
    % ADS_MSRTL matlab representation for the ADS MSRTL component
    % Microstrip Radial Transmission Line
    % MSRTL [:Name] t1 ...
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Inner radius (smorr) Unit: m
        Ri
        % Outer radius (smorr) Unit: m
        Ro
        % Angle (smorr) Unit: deg
        Angle
        % Line Width (smorr) Unit: m
        W
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Ri(obj,val)
            obj = setParameter(obj,'Ri',val,0,'real');
        end
        function res = get.Ri(obj)
            res = getParameter(obj,'Ri');
        end
        function obj = set.Ro(obj,val)
            obj = setParameter(obj,'Ro',val,0,'real');
        end
        function res = get.Ro(obj)
            res = getParameter(obj,'Ro');
        end
        function obj = set.Angle(obj,val)
            obj = setParameter(obj,'Angle',val,0,'real');
        end
        function res = get.Angle(obj)
            res = getParameter(obj,'Angle');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
    end
end
