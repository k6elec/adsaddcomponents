classdef ADS_SLOBND < ADScomponent
    % ADS_SLOBND matlab representation for the ADS SLOBND component
    % Stripline Bend, Chamfered, 90 deg.
    % SLOBND [:Name] t1 t2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Line Width (smorr) Unit: m
        W
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
    end
end
