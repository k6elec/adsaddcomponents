classdef ADS_PC_Pad < ADScomponent
    % ADS_PC_Pad matlab representation for the ADS PC_Pad component
    % User-defined model
    % PC_Pad [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % User device parameter (Sm-ri) Unit: unknown
        Layer
        % User device parameter (Smorr) Unit: unknown
        DiamVia
        % User device parameter (Smorr) Unit: unknown
        DiamPad
        % User device parameter (smorr) Unit: unknown
        Angle
        % User device parameter (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.Layer(obj,val)
            obj = setParameter(obj,'Layer',val,0,'integer');
        end
        function res = get.Layer(obj)
            res = getParameter(obj,'Layer');
        end
        function obj = set.DiamVia(obj,val)
            obj = setParameter(obj,'DiamVia',val,0,'real');
        end
        function res = get.DiamVia(obj)
            res = getParameter(obj,'DiamVia');
        end
        function obj = set.DiamPad(obj,val)
            obj = setParameter(obj,'DiamPad',val,0,'real');
        end
        function res = get.DiamPad(obj)
            res = getParameter(obj,'DiamPad');
        end
        function obj = set.Angle(obj,val)
            obj = setParameter(obj,'Angle',val,0,'real');
        end
        function res = get.Angle(obj)
            res = getParameter(obj,'Angle');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end
