classdef ADS_GMSK_Lowpass < ADScomponent
    % ADS_GMSK_Lowpass matlab representation for the ADS GMSK_Lowpass component
    % Low Pass Gauss Filter
    % GMSK_Lowpass [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % 3 dB frequency of filter * bit duration (Smorr) 
        Alpha
        % bit duration (smorr) Unit: seconds
        Tau
        % Input impedance of filter (smorr) Unit: Ohms
        Z1
        % Output impedance of filter (smorr) Unit: Ohms
        Z2
        % symbol delay through filter (smorr) Unit: Seconds
        Delay
        % Non-causal function impulse response order (sm-ri) Unit: (unknown units)
        ImpNoncausalLength
        % Convolution mode (sm-ri) Unit: (unknown units)
        ImpMode
        % Maximum Frequency (smorr) Unit: (unknown units)
        ImpMaxFreq
        % Sample Frequency (smorr) Unit: (unknown units)
        ImpDeltaFreq
        % maximum allowed impulse response order (sm-ri) Unit: (unknown units)
        ImpMaxPts
        % Smoothing Window (sm-ri) Unit: (unknown units)
        ImpWindow
        % Relative impulse Response truncation factor (smorr) Unit: (unknown units)
        ImpRelTrunc
        % Relative impulse Response truncation factor (smorr) Unit: (unknown units)
        ImpAbsTrunc
    end
    methods
        function obj = set.Alpha(obj,val)
            obj = setParameter(obj,'Alpha',val,0,'real');
        end
        function res = get.Alpha(obj)
            res = getParameter(obj,'Alpha');
        end
        function obj = set.Tau(obj,val)
            obj = setParameter(obj,'Tau',val,0,'real');
        end
        function res = get.Tau(obj)
            res = getParameter(obj,'Tau');
        end
        function obj = set.Z1(obj,val)
            obj = setParameter(obj,'Z1',val,0,'real');
        end
        function res = get.Z1(obj)
            res = getParameter(obj,'Z1');
        end
        function obj = set.Z2(obj,val)
            obj = setParameter(obj,'Z2',val,0,'real');
        end
        function res = get.Z2(obj)
            res = getParameter(obj,'Z2');
        end
        function obj = set.Delay(obj,val)
            obj = setParameter(obj,'Delay',val,0,'real');
        end
        function res = get.Delay(obj)
            res = getParameter(obj,'Delay');
        end
        function obj = set.ImpNoncausalLength(obj,val)
            obj = setParameter(obj,'ImpNoncausalLength',val,0,'integer');
        end
        function res = get.ImpNoncausalLength(obj)
            res = getParameter(obj,'ImpNoncausalLength');
        end
        function obj = set.ImpMode(obj,val)
            obj = setParameter(obj,'ImpMode',val,0,'integer');
        end
        function res = get.ImpMode(obj)
            res = getParameter(obj,'ImpMode');
        end
        function obj = set.ImpMaxFreq(obj,val)
            obj = setParameter(obj,'ImpMaxFreq',val,0,'real');
        end
        function res = get.ImpMaxFreq(obj)
            res = getParameter(obj,'ImpMaxFreq');
        end
        function obj = set.ImpDeltaFreq(obj,val)
            obj = setParameter(obj,'ImpDeltaFreq',val,0,'real');
        end
        function res = get.ImpDeltaFreq(obj)
            res = getParameter(obj,'ImpDeltaFreq');
        end
        function obj = set.ImpMaxPts(obj,val)
            obj = setParameter(obj,'ImpMaxPts',val,0,'integer');
        end
        function res = get.ImpMaxPts(obj)
            res = getParameter(obj,'ImpMaxPts');
        end
        function obj = set.ImpWindow(obj,val)
            obj = setParameter(obj,'ImpWindow',val,0,'integer');
        end
        function res = get.ImpWindow(obj)
            res = getParameter(obj,'ImpWindow');
        end
        function obj = set.ImpRelTrunc(obj,val)
            obj = setParameter(obj,'ImpRelTrunc',val,0,'real');
        end
        function res = get.ImpRelTrunc(obj)
            res = getParameter(obj,'ImpRelTrunc');
        end
        function obj = set.ImpAbsTrunc(obj,val)
            obj = setParameter(obj,'ImpAbsTrunc',val,0,'real');
        end
        function res = get.ImpAbsTrunc(obj)
            res = getParameter(obj,'ImpAbsTrunc');
        end
    end
end
