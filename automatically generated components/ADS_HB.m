classdef ADS_HB < ADSnodeless
    % ADS_HB matlab representation for the ADS HB component
    % Steady-State Harmonic Balance Analysis
    % HB [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Node name of dynamic input for ABM characterization (s---s) 
        ABM_ActiveInputNode
        % Number of swept amplitude points to use for ABM (sm--i) 
        ABM_AmpPts
        % Additional delay that can be added to ABM model (smo-r) 
        ABM_Delay
        % Frequency compensation [0,1,2] => [None,Input,Output] (sm--i) 
        ABM_FreqComp
        % Number of SS frequency points to use for ABM (sm--i) 
        ABM_FreqPts
        % Node names of IQ baseband pairs (repeatable) (s---s) 
        ABM_IQ_Nodes
        % Maximum power for ABM characterization (smo-r) 
        ABM_MaxPower
        % Enable ABM mode during cosimulation (s--rb) 
        ABM_Mode
        % Number of points per decade in log noise characterization (s---i) 
        ABM_NoiseLogPtsPerDec
        % Flag to request the log sweep for noise characterization (s---b) 
        ABM_NoiseLogScale
        % Start frequency in the log sweep for noise characterization (s---r) 
        ABM_NoiseLogStartFreq
        % Flag to use the accelerated noise simulation (s---b) 
        ABM_NoiseSkipConvolution
        % Nominal power for ABM characterization (smo-r) 
        ABM_NomPower
        % Number of swept phase points to use for ABM (sm--i) 
        ABM_PhasePts
        % Re-use previous dataset if available (s---b) 
        ABM_ReUseData
        % Simulation time for initial verification of ABM model (smo-r) 
        ABM_VTime
        % Relative tolerance to use for switching to ABM model (smo-r) 
        ABM_VTol
        % AWHB window size (s---i) 
        AWHB_WindowSize
        % Maximum arc-length step for source-level continuation (s---r) 
        ArcLevelMaxStep
        % Maximum arc-length step (s---r) 
        ArcMaxStep
        % Maximum value for parameter during arclength continuation (s---r) 
        ArcMaxValue
        % Minimum value for parameter during arclength continuation (s---r) 
        ArcMinValue
        % RAM available to the simulator (s---r) 
        AvailableRAMsize
        % Bandwidth for spectral noise analysis (smorr) Unit: Hz
        BandwidthForNoise
        % Calculate "large-signal" S-params (s---b) 
        CalcS_Params
        % Newton solver, 0:Auto, 1:Advanced, 2:Basic (s---i) 
        ConvMode
        % Starting algorithm for DC convergence (s---i) 
        DefaultConvStart
        % Levels of DC Operating Point Data to output (s---i) 
        DevOpPtLevel
        % Perform XdB Gain Compression Analysis (s---b) 
        DoGainComp
        % Do not include frequencies from a band (s---r) 
        EliminateBand
        % Absolute truncation factor for Envelope fitting (s---r) 
        EnvAbsTrunc
        % Envelope relative bandwidth (default = .1) (s---r) 
        EnvBandwidth
        % Turn on/off the variable time step in CE: 0: Fixed, 1: Check LTE (s---i) 
        EnvCheckLTE
        % 0: Off, 1: Run 2 HB prior to CE to evaluate the snp fit accuracy (s---i) 
        EnvFitCompare
        % Disable cache for rational polynomial env fits. (s---b) 
        EnvFitDisableCache
        % 0: Off, 1: Dump vector fitting results into the dataset (s---i) 
        EnvFitDumpIntoDataset
        % Maximum number of poles to use for rational polynomial env fits. (s---i) 
        EnvFitMaxNumPoles
        % 0: Compat, 1: Vector Fit (Interp), 2: Vector Fit (Raw Sample Preference) (s---i) 
        EnvFitType
        % Numerical integration order for envelope (s---i) 
        EnvIntegOrder
        % Add noise sources during envelope simulation (s---b) 
        EnvNoise
        % Run noise at every time point (s---b) 
        EnvNoiseAtEveryTimePoint
        % 0: default, 1: enforce, 2: strict (pre and post) (s---i) 
        EnvPassivity
        % Relative truncation factor for Envelope fitting (s---r) 
        EnvRelTrunc
        % Don't do pole/zero fittting for baseband (s---b) 
        EnvSkipDC_Fit
        % Use poor fits instead of constant values (s---b) 
        EnvUsePoorFit
        % Enables warning of poor envelope fits (s---b) 
        EnvWarnPoorFit
        % Consider AM to FM conversion in oscillator phase noise analysis (s---b) 
        FM_Noise
        % Scale the number of points used to characterize the circuit (s---r) 
        FastEnvelopeAccu
        % 0: No debug, 1: Print out some debug files (s---i) 
        FastEnvelopeDebug
        % 0: Regular envelope, 1: Fast Envelope (No memory), 2: Fast Envelope (memory) (s---i) 
        FastEnvelopeLevel
        % 0: Off, 1: On (s---i) 
        FastEnvelopeSteadyState
        % Noise frequency for spectral noise analysis (smorr) Unit: Hz
        FreqForNoise
        % Frequency of fundamental (smorr) Unit: Hz
        Freq
        % Oversampling ratio for FFT (s---r) 
        FundOversample
        % Driving frequency at input port for XdBgc analysis (smorr) 
        GC_InputFreq
        % Input port for XdBgc analysis (s---i) 
        GC_InputPort
        % Input power tolerance for XdBgc analysis (s---r) 
        GC_InputPowerTol
        % Max input power (in dBm) for XdBgc analysis (s---r) 
        GC_MaxInputPower
        % Driving frequency at output port for XdBgc analysis (smorr) 
        GC_OutputFreq
        % Output port for XdBgc analysis (s---i) 
        GC_OutputPort
        % Output power tolerance for XdBgc analysis (s---r) 
        GC_OutputPowerTol
        % Specifies dB of compression to solve for (smorr) 
        GC_XdB
        % GMRES iterations before auto-restart (s---i) 
        GMRES_Restart
        % GMRES Packing Threshold (s---r) 
        GMRES_Thresh
        % Final Gnode at which DC convergence is accepted (s---r) 
        GnodeFinal
        % Guard threshold (s---r) 
        GuardThresh
        % Enable harmonic balance assisted harmonic balance: 0:OFF, 1:ON, 2:AUTO (sm-ri) 
        HBAHB_Enable
        % Continue sweep even after oscillation convergence error (s---b) 
        IgnoreOscErrors
        % Name of file to load initial HB solution from (sm-rs) 
        InFile
        % Include port noise in noise voltage and currents (s---b) 
        IncludePortNoise
        % HB initial guess in Auto/Advanced ConvMode: 0:AC, 1:zero (s---i) 
        InitGuess
        % Input frequency for desired SSB mixing term (smorr) Unit: Hz
        InputFreq
        % Input frequency for desired SSB mixing term (smorr) Unit: Hz
        InputFreqForMixer
        % Convergence ratio used in aborting GMRES (s---r) 
        KrylovConvRatio
        % Min. number of iterations to invoke loose tolerance (s---i) 
        KrylovLooseIters
        % Loose tolerance for Krylov loop (s---r) 
        KrylovLooseTol
        % Maximum number of GMRES iterations (s---i) 
        KrylovMaxIters
        % Krylov bandwidth threshold (s---r) 
        KrylovPackingThresh
        % Krylov preconditioner (0:AUTO, 1:DCP, 2:SCP, 3:BSP, 4:ITP) (s---i) 
        KrylovPrec
        % Krylov tol. for small-sig spectral / noise analysis (s---r) 
        KrylovSS_Tol
        % 0:GS_GMRES, 1:GSR_GMRES, 2:HA_GMRES, 3:GS_FGMRES, 4:TFQMR, 5:GS_GMRES_RPC (s---i) 
        KrylovSolver
        % GMRES tolerance (s---r) 
        KrylovTightTol
        % Use FlexBlock preconditioner for Krylov (s---b) 
        KrylovUseFlexPreconditioner
        % Use floats to store GMRES vectors (s---b) 
        KrylovUseGMRES_Float
        % Use Krylov spectral packing (s---b) 
        KrylovUsePacking
        % LSSP frequency at a given port (s----) 
        LSSP_FreqAtPort
        % Instance/path name for frequency sweep values for P2D files (repeatable) (s---d) 
        LSSP_FreqPlan
        % Instance/path name for power sweep values for P2D files (repeatable) (s---d) 
        LSSP_PowerPlan
        % Max number of tries in Auto ConvMode (s---i) 
        MaxAutoTries
        % Max number of iterations (s---i) 
        MaxIters
        % Maximum combined order to be considered (sm-ri) 
        MaxOrder
        % Maximum step shrinkage (s---r) 
        MaxShrinkage
        % Ratio of maximum to given number of steps (s---r) 
        MaxStepRatio
        % Merge small- and large-signal solutions (s---b) 
        MergeSS_Freqs
        % Merrit function for Nelder Mead, derivative free solver: 0:Linf, 1:L1, 2:L2 (s---i) 
        MerritFunc
        % Levels of subcircuits to output (s---i) 
        NestLevel
        % Instance/path name for noise sweep values (repeatable) (s---d) 
        NoiseFreqPlan
        % Input port for noise figure calculation (s---i) 
        NoiseInputPort
        % Nodename to compute noise voltage (repeatable) (s---s) 
        NoiseNode
        % Output port for noise figure calculation (s---i) 
        NoiseOutputPort
        % Noise Contribution Threshold (s---r) 
        NoiseThresh
        % Instance/path name for noisecon items (repeatable) (s---d) 
        Noisecon
        % Compute noisy two-port parameters: Sopt, Rn and NFmin (s---b) 
        NoisyTwoPort
        % Norm check type in the Advanced/Auto ConvMode Newton solver: 0:Linf, 1:L1, 2:L2 (s---i) 
        NormCheck
        % Number of fundamentals (---ri) 
        NumberOfFunds
        % Maximum order of fundamental to be considered (sm-ri) 
        Order
        % Oscillator phase noise algorithm (s---i) 
        OscNoiseMethod
        % Oscillator port used to break feedback loop (s---s) 
        OscPortName
        % File name for saved HB solution (sm-rs) 
        OutFile
        % Variable or parameter to output (repeatable) (s--rs) 
        OutVar
        % Output spectra at all computed steps when sweeping (s---b) 
        OutputAllSolns
        % Output top-level pin currents and voltages (s---b) 
        OutputBudgetIV
        % OutputPlan Name (s---s) 
        OutputPlan
        % Oversampling ratio for FFT (s---r) 
        Oversample
        % Name for P2D file (sm-rs) 
        P2D_FileName
        % Pack FFT in multi-tone analysis (s---b) 
        PackFFT
        % Specify noise frequency as offset from oscillation frequency (s---b) 
        PhaseNoise
        % BSP or SCP preconditioner input file (s---s) 
        PrecInFile
        % Krylov preconditioner matrix solver (0:AUTO, 1:General, 2:For large circuits) (s---i) 
        PrecMatrixSolver
        % BSP or SCP preconditioner output file (s---s) 
        PrecOutFile
        % RHS Thresh for activating BSP or SCP preconditioner (s---r) 
        PrecRhsThresh
        % Use dynamic waveform storage (s---b) 
        RecalculateWaveforms
        % Required norm reduction in the Advanced/Auto ConvMode Newton solver (s---r) 
        RedRatio
        % Reset initial oscillator envelope solution (s---b) 
        ResetOsc
        % Do not use last solution as initial guess (s--rb) 
        Restart
        % Small signal mixer frequency (smorr) Unit: Hz
        SS_Freq
        % Instance/path name for small-signal freq values (repeatable) (s---d) 
        SS_Plan
        % Small signal spectral threshold (s---r) 
        SS_Thresh
        % Samanskii constant (s---r) 
        SamanskiiConstant
        % Save output during cosimulation (s---b) 
        SaveCosimOutput
        % Save results to rawfile (s---b) 
        SaveResults
        % Turn Analysis output to Dataset on or off (s---b) 
        SaveToDataset
        % Sort Noise Contribution by: Value/1, Name/2 (default: 0/NoOutput) (s---i) 
        SortNoise
        % Include (yes) or Exclude (no) a specific spectral product (s---b) 
        SpectralProduct
        % Degree of annotation (s---i) 
        StatusLevel
        % Sweep variable offset (s---r) 
        SweepOffset
        % Instance/path name for sweep values (repeatable) (s---d) 
        SweepPlan
        % Name of variable or parameter to be swept (s---s) 
        SweepVar
        % Enable transient assisted harmonic balance: 0:OFF, 1:ON, 2:AUTO (sm-ri) 
        TAHB_Enable
        % Temperature for spectral noise analysis (smorr) Unit: deg C
        TempForNoise
        % Hard timeout for abandoning HB sweep point (s---r) 
        Timeout
        % Solve for all small signal mixer sidebands (requires more memory) (s---b) 
        UseAllSS_Freqs
        % Compress spectrum (s---b) 
        UseCompactFreqMap
        % Use deflation during continuation--slower, more robust (s---b) 
        UseDeflation
        % Use Gear's method for envelope integration (s---b) 
        UseGear
        % Read InFile as the HB initial guess (sm-rb) 
        UseInFile
        % Use initial AWHB stage before Krylov (s---b) 
        UseInitialAWHB
        % Linear solver (0:Direct, 1:Krylov, 2:Auto select) (s---i) 
        UseKrylov
        % Use optimal damping in the Advanced/Auto ConvMode Newton solver (s---b) 
        UseOptDamp
        % Write the HB solution to OutFile (sm-rb) 
        UseOutFile
        % Use harmonic relaxation, uses less memory, less robust (s---b) 
        UseRelaxation
        % VTB component (s---s) 
        VTB
        % Enable xtal osc HB->TAHB flow (sm-rb) 
        XtalOscStart
        % BSP or SCP preconditioner input file (s---s) 
        bspInFile
        % BSP or SCP preconditioner output file (s---s) 
        bspOutFile
        % RHS Thresh for activating BSP or SCP preconditioner (s---r) 
        bspRHS_Thresh
    end
    methods
        function obj = set.ABM_ActiveInputNode(obj,val)
            obj = setParameter(obj,'ABM_ActiveInputNode',val,0,'string');
        end
        function res = get.ABM_ActiveInputNode(obj)
            res = getParameter(obj,'ABM_ActiveInputNode');
        end
        function obj = set.ABM_AmpPts(obj,val)
            obj = setParameter(obj,'ABM_AmpPts',val,0,'integer');
        end
        function res = get.ABM_AmpPts(obj)
            res = getParameter(obj,'ABM_AmpPts');
        end
        function obj = set.ABM_Delay(obj,val)
            obj = setParameter(obj,'ABM_Delay',val,0,'real');
        end
        function res = get.ABM_Delay(obj)
            res = getParameter(obj,'ABM_Delay');
        end
        function obj = set.ABM_FreqComp(obj,val)
            obj = setParameter(obj,'ABM_FreqComp',val,0,'integer');
        end
        function res = get.ABM_FreqComp(obj)
            res = getParameter(obj,'ABM_FreqComp');
        end
        function obj = set.ABM_FreqPts(obj,val)
            obj = setParameter(obj,'ABM_FreqPts',val,0,'integer');
        end
        function res = get.ABM_FreqPts(obj)
            res = getParameter(obj,'ABM_FreqPts');
        end
        function obj = set.ABM_IQ_Nodes(obj,val)
            obj = setParameter(obj,'ABM_IQ_Nodes',val,1,'string');
        end
        function res = get.ABM_IQ_Nodes(obj)
            res = getParameter(obj,'ABM_IQ_Nodes');
        end
        function obj = set.ABM_MaxPower(obj,val)
            obj = setParameter(obj,'ABM_MaxPower',val,0,'real');
        end
        function res = get.ABM_MaxPower(obj)
            res = getParameter(obj,'ABM_MaxPower');
        end
        function obj = set.ABM_Mode(obj,val)
            obj = setParameter(obj,'ABM_Mode',val,0,'boolean');
        end
        function res = get.ABM_Mode(obj)
            res = getParameter(obj,'ABM_Mode');
        end
        function obj = set.ABM_NoiseLogPtsPerDec(obj,val)
            obj = setParameter(obj,'ABM_NoiseLogPtsPerDec',val,0,'integer');
        end
        function res = get.ABM_NoiseLogPtsPerDec(obj)
            res = getParameter(obj,'ABM_NoiseLogPtsPerDec');
        end
        function obj = set.ABM_NoiseLogScale(obj,val)
            obj = setParameter(obj,'ABM_NoiseLogScale',val,0,'boolean');
        end
        function res = get.ABM_NoiseLogScale(obj)
            res = getParameter(obj,'ABM_NoiseLogScale');
        end
        function obj = set.ABM_NoiseLogStartFreq(obj,val)
            obj = setParameter(obj,'ABM_NoiseLogStartFreq',val,0,'real');
        end
        function res = get.ABM_NoiseLogStartFreq(obj)
            res = getParameter(obj,'ABM_NoiseLogStartFreq');
        end
        function obj = set.ABM_NoiseSkipConvolution(obj,val)
            obj = setParameter(obj,'ABM_NoiseSkipConvolution',val,0,'boolean');
        end
        function res = get.ABM_NoiseSkipConvolution(obj)
            res = getParameter(obj,'ABM_NoiseSkipConvolution');
        end
        function obj = set.ABM_NomPower(obj,val)
            obj = setParameter(obj,'ABM_NomPower',val,0,'real');
        end
        function res = get.ABM_NomPower(obj)
            res = getParameter(obj,'ABM_NomPower');
        end
        function obj = set.ABM_PhasePts(obj,val)
            obj = setParameter(obj,'ABM_PhasePts',val,0,'integer');
        end
        function res = get.ABM_PhasePts(obj)
            res = getParameter(obj,'ABM_PhasePts');
        end
        function obj = set.ABM_ReUseData(obj,val)
            obj = setParameter(obj,'ABM_ReUseData',val,0,'boolean');
        end
        function res = get.ABM_ReUseData(obj)
            res = getParameter(obj,'ABM_ReUseData');
        end
        function obj = set.ABM_VTime(obj,val)
            obj = setParameter(obj,'ABM_VTime',val,0,'real');
        end
        function res = get.ABM_VTime(obj)
            res = getParameter(obj,'ABM_VTime');
        end
        function obj = set.ABM_VTol(obj,val)
            obj = setParameter(obj,'ABM_VTol',val,0,'real');
        end
        function res = get.ABM_VTol(obj)
            res = getParameter(obj,'ABM_VTol');
        end
        function obj = set.AWHB_WindowSize(obj,val)
            obj = setParameter(obj,'AWHB_WindowSize',val,0,'integer');
        end
        function res = get.AWHB_WindowSize(obj)
            res = getParameter(obj,'AWHB_WindowSize');
        end
        function obj = set.ArcLevelMaxStep(obj,val)
            obj = setParameter(obj,'ArcLevelMaxStep',val,0,'real');
        end
        function res = get.ArcLevelMaxStep(obj)
            res = getParameter(obj,'ArcLevelMaxStep');
        end
        function obj = set.ArcMaxStep(obj,val)
            obj = setParameter(obj,'ArcMaxStep',val,0,'real');
        end
        function res = get.ArcMaxStep(obj)
            res = getParameter(obj,'ArcMaxStep');
        end
        function obj = set.ArcMaxValue(obj,val)
            obj = setParameter(obj,'ArcMaxValue',val,0,'real');
        end
        function res = get.ArcMaxValue(obj)
            res = getParameter(obj,'ArcMaxValue');
        end
        function obj = set.ArcMinValue(obj,val)
            obj = setParameter(obj,'ArcMinValue',val,0,'real');
        end
        function res = get.ArcMinValue(obj)
            res = getParameter(obj,'ArcMinValue');
        end
        function obj = set.AvailableRAMsize(obj,val)
            obj = setParameter(obj,'AvailableRAMsize',val,0,'real');
        end
        function res = get.AvailableRAMsize(obj)
            res = getParameter(obj,'AvailableRAMsize');
        end
        function obj = set.BandwidthForNoise(obj,val)
            obj = setParameter(obj,'BandwidthForNoise',val,0,'real');
        end
        function res = get.BandwidthForNoise(obj)
            res = getParameter(obj,'BandwidthForNoise');
        end
        function obj = set.CalcS_Params(obj,val)
            obj = setParameter(obj,'CalcS_Params',val,0,'boolean');
        end
        function res = get.CalcS_Params(obj)
            res = getParameter(obj,'CalcS_Params');
        end
        function obj = set.ConvMode(obj,val)
            obj = setParameter(obj,'ConvMode',val,0,'integer');
        end
        function res = get.ConvMode(obj)
            res = getParameter(obj,'ConvMode');
        end
        function obj = set.DefaultConvStart(obj,val)
            obj = setParameter(obj,'DefaultConvStart',val,0,'integer');
        end
        function res = get.DefaultConvStart(obj)
            res = getParameter(obj,'DefaultConvStart');
        end
        function obj = set.DevOpPtLevel(obj,val)
            obj = setParameter(obj,'DevOpPtLevel',val,0,'integer');
        end
        function res = get.DevOpPtLevel(obj)
            res = getParameter(obj,'DevOpPtLevel');
        end
        function obj = set.DoGainComp(obj,val)
            obj = setParameter(obj,'DoGainComp',val,0,'boolean');
        end
        function res = get.DoGainComp(obj)
            res = getParameter(obj,'DoGainComp');
        end
        function obj = set.EliminateBand(obj,val)
            obj = setParameter(obj,'EliminateBand',val,1,'real');
        end
        function res = get.EliminateBand(obj)
            res = getParameter(obj,'EliminateBand');
        end
        function obj = set.EnvAbsTrunc(obj,val)
            obj = setParameter(obj,'EnvAbsTrunc',val,0,'real');
        end
        function res = get.EnvAbsTrunc(obj)
            res = getParameter(obj,'EnvAbsTrunc');
        end
        function obj = set.EnvBandwidth(obj,val)
            obj = setParameter(obj,'EnvBandwidth',val,0,'real');
        end
        function res = get.EnvBandwidth(obj)
            res = getParameter(obj,'EnvBandwidth');
        end
        function obj = set.EnvCheckLTE(obj,val)
            obj = setParameter(obj,'EnvCheckLTE',val,0,'integer');
        end
        function res = get.EnvCheckLTE(obj)
            res = getParameter(obj,'EnvCheckLTE');
        end
        function obj = set.EnvFitCompare(obj,val)
            obj = setParameter(obj,'EnvFitCompare',val,0,'integer');
        end
        function res = get.EnvFitCompare(obj)
            res = getParameter(obj,'EnvFitCompare');
        end
        function obj = set.EnvFitDisableCache(obj,val)
            obj = setParameter(obj,'EnvFitDisableCache',val,0,'boolean');
        end
        function res = get.EnvFitDisableCache(obj)
            res = getParameter(obj,'EnvFitDisableCache');
        end
        function obj = set.EnvFitDumpIntoDataset(obj,val)
            obj = setParameter(obj,'EnvFitDumpIntoDataset',val,0,'integer');
        end
        function res = get.EnvFitDumpIntoDataset(obj)
            res = getParameter(obj,'EnvFitDumpIntoDataset');
        end
        function obj = set.EnvFitMaxNumPoles(obj,val)
            obj = setParameter(obj,'EnvFitMaxNumPoles',val,0,'integer');
        end
        function res = get.EnvFitMaxNumPoles(obj)
            res = getParameter(obj,'EnvFitMaxNumPoles');
        end
        function obj = set.EnvFitType(obj,val)
            obj = setParameter(obj,'EnvFitType',val,0,'integer');
        end
        function res = get.EnvFitType(obj)
            res = getParameter(obj,'EnvFitType');
        end
        function obj = set.EnvIntegOrder(obj,val)
            obj = setParameter(obj,'EnvIntegOrder',val,0,'integer');
        end
        function res = get.EnvIntegOrder(obj)
            res = getParameter(obj,'EnvIntegOrder');
        end
        function obj = set.EnvNoise(obj,val)
            obj = setParameter(obj,'EnvNoise',val,0,'boolean');
        end
        function res = get.EnvNoise(obj)
            res = getParameter(obj,'EnvNoise');
        end
        function obj = set.EnvNoiseAtEveryTimePoint(obj,val)
            obj = setParameter(obj,'EnvNoiseAtEveryTimePoint',val,0,'boolean');
        end
        function res = get.EnvNoiseAtEveryTimePoint(obj)
            res = getParameter(obj,'EnvNoiseAtEveryTimePoint');
        end
        function obj = set.EnvPassivity(obj,val)
            obj = setParameter(obj,'EnvPassivity',val,0,'integer');
        end
        function res = get.EnvPassivity(obj)
            res = getParameter(obj,'EnvPassivity');
        end
        function obj = set.EnvRelTrunc(obj,val)
            obj = setParameter(obj,'EnvRelTrunc',val,0,'real');
        end
        function res = get.EnvRelTrunc(obj)
            res = getParameter(obj,'EnvRelTrunc');
        end
        function obj = set.EnvSkipDC_Fit(obj,val)
            obj = setParameter(obj,'EnvSkipDC_Fit',val,0,'boolean');
        end
        function res = get.EnvSkipDC_Fit(obj)
            res = getParameter(obj,'EnvSkipDC_Fit');
        end
        function obj = set.EnvUsePoorFit(obj,val)
            obj = setParameter(obj,'EnvUsePoorFit',val,0,'boolean');
        end
        function res = get.EnvUsePoorFit(obj)
            res = getParameter(obj,'EnvUsePoorFit');
        end
        function obj = set.EnvWarnPoorFit(obj,val)
            obj = setParameter(obj,'EnvWarnPoorFit',val,0,'boolean');
        end
        function res = get.EnvWarnPoorFit(obj)
            res = getParameter(obj,'EnvWarnPoorFit');
        end
        function obj = set.FM_Noise(obj,val)
            obj = setParameter(obj,'FM_Noise',val,0,'boolean');
        end
        function res = get.FM_Noise(obj)
            res = getParameter(obj,'FM_Noise');
        end
        function obj = set.FastEnvelopeAccu(obj,val)
            obj = setParameter(obj,'FastEnvelopeAccu',val,0,'real');
        end
        function res = get.FastEnvelopeAccu(obj)
            res = getParameter(obj,'FastEnvelopeAccu');
        end
        function obj = set.FastEnvelopeDebug(obj,val)
            obj = setParameter(obj,'FastEnvelopeDebug',val,0,'integer');
        end
        function res = get.FastEnvelopeDebug(obj)
            res = getParameter(obj,'FastEnvelopeDebug');
        end
        function obj = set.FastEnvelopeLevel(obj,val)
            obj = setParameter(obj,'FastEnvelopeLevel',val,0,'integer');
        end
        function res = get.FastEnvelopeLevel(obj)
            res = getParameter(obj,'FastEnvelopeLevel');
        end
        function obj = set.FastEnvelopeSteadyState(obj,val)
            obj = setParameter(obj,'FastEnvelopeSteadyState',val,0,'integer');
        end
        function res = get.FastEnvelopeSteadyState(obj)
            res = getParameter(obj,'FastEnvelopeSteadyState');
        end
        function obj = set.FreqForNoise(obj,val)
            obj = setParameter(obj,'FreqForNoise',val,0,'real');
        end
        function res = get.FreqForNoise(obj)
            res = getParameter(obj,'FreqForNoise');
        end
        function obj = set.Freq(obj,val)
            obj = setParameter(obj,'Freq',val,1,'real');
        end
        function res = get.Freq(obj)
            res = getParameter(obj,'Freq');
        end
        function obj = set.FundOversample(obj,val)
            obj = setParameter(obj,'FundOversample',val,1,'real');
        end
        function res = get.FundOversample(obj)
            res = getParameter(obj,'FundOversample');
        end
        function obj = set.GC_InputFreq(obj,val)
            obj = setParameter(obj,'GC_InputFreq',val,0,'real');
        end
        function res = get.GC_InputFreq(obj)
            res = getParameter(obj,'GC_InputFreq');
        end
        function obj = set.GC_InputPort(obj,val)
            obj = setParameter(obj,'GC_InputPort',val,0,'integer');
        end
        function res = get.GC_InputPort(obj)
            res = getParameter(obj,'GC_InputPort');
        end
        function obj = set.GC_InputPowerTol(obj,val)
            obj = setParameter(obj,'GC_InputPowerTol',val,0,'real');
        end
        function res = get.GC_InputPowerTol(obj)
            res = getParameter(obj,'GC_InputPowerTol');
        end
        function obj = set.GC_MaxInputPower(obj,val)
            obj = setParameter(obj,'GC_MaxInputPower',val,0,'real');
        end
        function res = get.GC_MaxInputPower(obj)
            res = getParameter(obj,'GC_MaxInputPower');
        end
        function obj = set.GC_OutputFreq(obj,val)
            obj = setParameter(obj,'GC_OutputFreq',val,0,'real');
        end
        function res = get.GC_OutputFreq(obj)
            res = getParameter(obj,'GC_OutputFreq');
        end
        function obj = set.GC_OutputPort(obj,val)
            obj = setParameter(obj,'GC_OutputPort',val,0,'integer');
        end
        function res = get.GC_OutputPort(obj)
            res = getParameter(obj,'GC_OutputPort');
        end
        function obj = set.GC_OutputPowerTol(obj,val)
            obj = setParameter(obj,'GC_OutputPowerTol',val,0,'real');
        end
        function res = get.GC_OutputPowerTol(obj)
            res = getParameter(obj,'GC_OutputPowerTol');
        end
        function obj = set.GC_XdB(obj,val)
            obj = setParameter(obj,'GC_XdB',val,0,'real');
        end
        function res = get.GC_XdB(obj)
            res = getParameter(obj,'GC_XdB');
        end
        function obj = set.GMRES_Restart(obj,val)
            obj = setParameter(obj,'GMRES_Restart',val,0,'integer');
        end
        function res = get.GMRES_Restart(obj)
            res = getParameter(obj,'GMRES_Restart');
        end
        function obj = set.GMRES_Thresh(obj,val)
            obj = setParameter(obj,'GMRES_Thresh',val,0,'real');
        end
        function res = get.GMRES_Thresh(obj)
            res = getParameter(obj,'GMRES_Thresh');
        end
        function obj = set.GnodeFinal(obj,val)
            obj = setParameter(obj,'GnodeFinal',val,0,'real');
        end
        function res = get.GnodeFinal(obj)
            res = getParameter(obj,'GnodeFinal');
        end
        function obj = set.GuardThresh(obj,val)
            obj = setParameter(obj,'GuardThresh',val,0,'real');
        end
        function res = get.GuardThresh(obj)
            res = getParameter(obj,'GuardThresh');
        end
        function obj = set.HBAHB_Enable(obj,val)
            obj = setParameter(obj,'HBAHB_Enable',val,0,'integer');
        end
        function res = get.HBAHB_Enable(obj)
            res = getParameter(obj,'HBAHB_Enable');
        end
        function obj = set.IgnoreOscErrors(obj,val)
            obj = setParameter(obj,'IgnoreOscErrors',val,0,'boolean');
        end
        function res = get.IgnoreOscErrors(obj)
            res = getParameter(obj,'IgnoreOscErrors');
        end
        function obj = set.InFile(obj,val)
            obj = setParameter(obj,'InFile',val,0,'string');
        end
        function res = get.InFile(obj)
            res = getParameter(obj,'InFile');
        end
        function obj = set.IncludePortNoise(obj,val)
            obj = setParameter(obj,'IncludePortNoise',val,0,'boolean');
        end
        function res = get.IncludePortNoise(obj)
            res = getParameter(obj,'IncludePortNoise');
        end
        function obj = set.InitGuess(obj,val)
            obj = setParameter(obj,'InitGuess',val,0,'integer');
        end
        function res = get.InitGuess(obj)
            res = getParameter(obj,'InitGuess');
        end
        function obj = set.InputFreq(obj,val)
            obj = setParameter(obj,'InputFreq',val,0,'real');
        end
        function res = get.InputFreq(obj)
            res = getParameter(obj,'InputFreq');
        end
        function obj = set.InputFreqForMixer(obj,val)
            obj = setParameter(obj,'InputFreqForMixer',val,0,'real');
        end
        function res = get.InputFreqForMixer(obj)
            res = getParameter(obj,'InputFreqForMixer');
        end
        function obj = set.KrylovConvRatio(obj,val)
            obj = setParameter(obj,'KrylovConvRatio',val,0,'real');
        end
        function res = get.KrylovConvRatio(obj)
            res = getParameter(obj,'KrylovConvRatio');
        end
        function obj = set.KrylovLooseIters(obj,val)
            obj = setParameter(obj,'KrylovLooseIters',val,0,'integer');
        end
        function res = get.KrylovLooseIters(obj)
            res = getParameter(obj,'KrylovLooseIters');
        end
        function obj = set.KrylovLooseTol(obj,val)
            obj = setParameter(obj,'KrylovLooseTol',val,0,'real');
        end
        function res = get.KrylovLooseTol(obj)
            res = getParameter(obj,'KrylovLooseTol');
        end
        function obj = set.KrylovMaxIters(obj,val)
            obj = setParameter(obj,'KrylovMaxIters',val,0,'integer');
        end
        function res = get.KrylovMaxIters(obj)
            res = getParameter(obj,'KrylovMaxIters');
        end
        function obj = set.KrylovPackingThresh(obj,val)
            obj = setParameter(obj,'KrylovPackingThresh',val,0,'real');
        end
        function res = get.KrylovPackingThresh(obj)
            res = getParameter(obj,'KrylovPackingThresh');
        end
        function obj = set.KrylovPrec(obj,val)
            obj = setParameter(obj,'KrylovPrec',val,0,'integer');
        end
        function res = get.KrylovPrec(obj)
            res = getParameter(obj,'KrylovPrec');
        end
        function obj = set.KrylovSS_Tol(obj,val)
            obj = setParameter(obj,'KrylovSS_Tol',val,0,'real');
        end
        function res = get.KrylovSS_Tol(obj)
            res = getParameter(obj,'KrylovSS_Tol');
        end
        function obj = set.KrylovSolver(obj,val)
            obj = setParameter(obj,'KrylovSolver',val,0,'integer');
        end
        function res = get.KrylovSolver(obj)
            res = getParameter(obj,'KrylovSolver');
        end
        function obj = set.KrylovTightTol(obj,val)
            obj = setParameter(obj,'KrylovTightTol',val,0,'real');
        end
        function res = get.KrylovTightTol(obj)
            res = getParameter(obj,'KrylovTightTol');
        end
        function obj = set.KrylovUseFlexPreconditioner(obj,val)
            obj = setParameter(obj,'KrylovUseFlexPreconditioner',val,0,'boolean');
        end
        function res = get.KrylovUseFlexPreconditioner(obj)
            res = getParameter(obj,'KrylovUseFlexPreconditioner');
        end
        function obj = set.KrylovUseGMRES_Float(obj,val)
            obj = setParameter(obj,'KrylovUseGMRES_Float',val,0,'boolean');
        end
        function res = get.KrylovUseGMRES_Float(obj)
            res = getParameter(obj,'KrylovUseGMRES_Float');
        end
        function obj = set.KrylovUsePacking(obj,val)
            obj = setParameter(obj,'KrylovUsePacking',val,0,'boolean');
        end
        function res = get.KrylovUsePacking(obj)
            res = getParameter(obj,'KrylovUsePacking');
        end
        function obj = set.LSSP_FreqAtPort(obj,val)
            obj = setParameter(obj,'LSSP_FreqAtPort',val,1,'string');
        end
        function res = get.LSSP_FreqAtPort(obj)
            res = getParameter(obj,'LSSP_FreqAtPort');
        end
        function obj = set.LSSP_FreqPlan(obj,val)
            obj = setParameter(obj,'LSSP_FreqPlan',val,1,'string');
        end
        function res = get.LSSP_FreqPlan(obj)
            res = getParameter(obj,'LSSP_FreqPlan');
        end
        function obj = set.LSSP_PowerPlan(obj,val)
            obj = setParameter(obj,'LSSP_PowerPlan',val,1,'string');
        end
        function res = get.LSSP_PowerPlan(obj)
            res = getParameter(obj,'LSSP_PowerPlan');
        end
        function obj = set.MaxAutoTries(obj,val)
            obj = setParameter(obj,'MaxAutoTries',val,0,'integer');
        end
        function res = get.MaxAutoTries(obj)
            res = getParameter(obj,'MaxAutoTries');
        end
        function obj = set.MaxIters(obj,val)
            obj = setParameter(obj,'MaxIters',val,0,'integer');
        end
        function res = get.MaxIters(obj)
            res = getParameter(obj,'MaxIters');
        end
        function obj = set.MaxOrder(obj,val)
            obj = setParameter(obj,'MaxOrder',val,0,'integer');
        end
        function res = get.MaxOrder(obj)
            res = getParameter(obj,'MaxOrder');
        end
        function obj = set.MaxShrinkage(obj,val)
            obj = setParameter(obj,'MaxShrinkage',val,0,'real');
        end
        function res = get.MaxShrinkage(obj)
            res = getParameter(obj,'MaxShrinkage');
        end
        function obj = set.MaxStepRatio(obj,val)
            obj = setParameter(obj,'MaxStepRatio',val,0,'real');
        end
        function res = get.MaxStepRatio(obj)
            res = getParameter(obj,'MaxStepRatio');
        end
        function obj = set.MergeSS_Freqs(obj,val)
            obj = setParameter(obj,'MergeSS_Freqs',val,0,'boolean');
        end
        function res = get.MergeSS_Freqs(obj)
            res = getParameter(obj,'MergeSS_Freqs');
        end
        function obj = set.MerritFunc(obj,val)
            obj = setParameter(obj,'MerritFunc',val,0,'integer');
        end
        function res = get.MerritFunc(obj)
            res = getParameter(obj,'MerritFunc');
        end
        function obj = set.NestLevel(obj,val)
            obj = setParameter(obj,'NestLevel',val,0,'integer');
        end
        function res = get.NestLevel(obj)
            res = getParameter(obj,'NestLevel');
        end
        function obj = set.NoiseFreqPlan(obj,val)
            obj = setParameter(obj,'NoiseFreqPlan',val,1,'string');
        end
        function res = get.NoiseFreqPlan(obj)
            res = getParameter(obj,'NoiseFreqPlan');
        end
        function obj = set.NoiseInputPort(obj,val)
            obj = setParameter(obj,'NoiseInputPort',val,0,'integer');
        end
        function res = get.NoiseInputPort(obj)
            res = getParameter(obj,'NoiseInputPort');
        end
        function obj = set.NoiseNode(obj,val)
            obj = setParameter(obj,'NoiseNode',val,0,'string');
        end
        function res = get.NoiseNode(obj)
            res = getParameter(obj,'NoiseNode');
        end
        function obj = set.NoiseOutputPort(obj,val)
            obj = setParameter(obj,'NoiseOutputPort',val,0,'integer');
        end
        function res = get.NoiseOutputPort(obj)
            res = getParameter(obj,'NoiseOutputPort');
        end
        function obj = set.NoiseThresh(obj,val)
            obj = setParameter(obj,'NoiseThresh',val,0,'real');
        end
        function res = get.NoiseThresh(obj)
            res = getParameter(obj,'NoiseThresh');
        end
        function obj = set.Noisecon(obj,val)
            obj = setParameter(obj,'Noisecon',val,1,'string');
        end
        function res = get.Noisecon(obj)
            res = getParameter(obj,'Noisecon');
        end
        function obj = set.NoisyTwoPort(obj,val)
            obj = setParameter(obj,'NoisyTwoPort',val,0,'boolean');
        end
        function res = get.NoisyTwoPort(obj)
            res = getParameter(obj,'NoisyTwoPort');
        end
        function obj = set.NormCheck(obj,val)
            obj = setParameter(obj,'NormCheck',val,0,'integer');
        end
        function res = get.NormCheck(obj)
            res = getParameter(obj,'NormCheck');
        end
        function obj = set.NumberOfFunds(obj,val)
            obj = setParameter(obj,'NumberOfFunds',val,0,'integer');
        end
        function res = get.NumberOfFunds(obj)
            res = getParameter(obj,'NumberOfFunds');
        end
        function obj = set.Order(obj,val)
            obj = setParameter(obj,'Order',val,1,'integer');
        end
        function res = get.Order(obj)
            res = getParameter(obj,'Order');
        end
        function obj = set.OscNoiseMethod(obj,val)
            obj = setParameter(obj,'OscNoiseMethod',val,0,'integer');
        end
        function res = get.OscNoiseMethod(obj)
            res = getParameter(obj,'OscNoiseMethod');
        end
        function obj = set.OscPortName(obj,val)
            obj = setParameter(obj,'OscPortName',val,0,'string');
        end
        function res = get.OscPortName(obj)
            res = getParameter(obj,'OscPortName');
        end
        function obj = set.OutFile(obj,val)
            obj = setParameter(obj,'OutFile',val,0,'string');
        end
        function res = get.OutFile(obj)
            res = getParameter(obj,'OutFile');
        end
        function obj = set.OutVar(obj,val)
            obj = setParameter(obj,'OutVar',val,0,'string');
        end
        function res = get.OutVar(obj)
            res = getParameter(obj,'OutVar');
        end
        function obj = set.OutputAllSolns(obj,val)
            obj = setParameter(obj,'OutputAllSolns',val,0,'boolean');
        end
        function res = get.OutputAllSolns(obj)
            res = getParameter(obj,'OutputAllSolns');
        end
        function obj = set.OutputBudgetIV(obj,val)
            obj = setParameter(obj,'OutputBudgetIV',val,0,'boolean');
        end
        function res = get.OutputBudgetIV(obj)
            res = getParameter(obj,'OutputBudgetIV');
        end
        function obj = set.OutputPlan(obj,val)
            obj = setParameter(obj,'OutputPlan',val,0,'string');
        end
        function res = get.OutputPlan(obj)
            res = getParameter(obj,'OutputPlan');
        end
        function obj = set.Oversample(obj,val)
            obj = setParameter(obj,'Oversample',val,1,'real');
        end
        function res = get.Oversample(obj)
            res = getParameter(obj,'Oversample');
        end
        function obj = set.P2D_FileName(obj,val)
            obj = setParameter(obj,'P2D_FileName',val,0,'string');
        end
        function res = get.P2D_FileName(obj)
            res = getParameter(obj,'P2D_FileName');
        end
        function obj = set.PackFFT(obj,val)
            obj = setParameter(obj,'PackFFT',val,0,'boolean');
        end
        function res = get.PackFFT(obj)
            res = getParameter(obj,'PackFFT');
        end
        function obj = set.PhaseNoise(obj,val)
            obj = setParameter(obj,'PhaseNoise',val,0,'boolean');
        end
        function res = get.PhaseNoise(obj)
            res = getParameter(obj,'PhaseNoise');
        end
        function obj = set.PrecInFile(obj,val)
            obj = setParameter(obj,'PrecInFile',val,0,'string');
        end
        function res = get.PrecInFile(obj)
            res = getParameter(obj,'PrecInFile');
        end
        function obj = set.PrecMatrixSolver(obj,val)
            obj = setParameter(obj,'PrecMatrixSolver',val,0,'integer');
        end
        function res = get.PrecMatrixSolver(obj)
            res = getParameter(obj,'PrecMatrixSolver');
        end
        function obj = set.PrecOutFile(obj,val)
            obj = setParameter(obj,'PrecOutFile',val,0,'string');
        end
        function res = get.PrecOutFile(obj)
            res = getParameter(obj,'PrecOutFile');
        end
        function obj = set.PrecRhsThresh(obj,val)
            obj = setParameter(obj,'PrecRhsThresh',val,0,'real');
        end
        function res = get.PrecRhsThresh(obj)
            res = getParameter(obj,'PrecRhsThresh');
        end
        function obj = set.RecalculateWaveforms(obj,val)
            obj = setParameter(obj,'RecalculateWaveforms',val,0,'boolean');
        end
        function res = get.RecalculateWaveforms(obj)
            res = getParameter(obj,'RecalculateWaveforms');
        end
        function obj = set.RedRatio(obj,val)
            obj = setParameter(obj,'RedRatio',val,0,'real');
        end
        function res = get.RedRatio(obj)
            res = getParameter(obj,'RedRatio');
        end
        function obj = set.ResetOsc(obj,val)
            obj = setParameter(obj,'ResetOsc',val,0,'boolean');
        end
        function res = get.ResetOsc(obj)
            res = getParameter(obj,'ResetOsc');
        end
        function obj = set.Restart(obj,val)
            obj = setParameter(obj,'Restart',val,0,'boolean');
        end
        function res = get.Restart(obj)
            res = getParameter(obj,'Restart');
        end
        function obj = set.SS_Freq(obj,val)
            obj = setParameter(obj,'SS_Freq',val,0,'real');
        end
        function res = get.SS_Freq(obj)
            res = getParameter(obj,'SS_Freq');
        end
        function obj = set.SS_Plan(obj,val)
            obj = setParameter(obj,'SS_Plan',val,1,'string');
        end
        function res = get.SS_Plan(obj)
            res = getParameter(obj,'SS_Plan');
        end
        function obj = set.SS_Thresh(obj,val)
            obj = setParameter(obj,'SS_Thresh',val,0,'real');
        end
        function res = get.SS_Thresh(obj)
            res = getParameter(obj,'SS_Thresh');
        end
        function obj = set.SamanskiiConstant(obj,val)
            obj = setParameter(obj,'SamanskiiConstant',val,0,'real');
        end
        function res = get.SamanskiiConstant(obj)
            res = getParameter(obj,'SamanskiiConstant');
        end
        function obj = set.SaveCosimOutput(obj,val)
            obj = setParameter(obj,'SaveCosimOutput',val,0,'boolean');
        end
        function res = get.SaveCosimOutput(obj)
            res = getParameter(obj,'SaveCosimOutput');
        end
        function obj = set.SaveResults(obj,val)
            obj = setParameter(obj,'SaveResults',val,0,'boolean');
        end
        function res = get.SaveResults(obj)
            res = getParameter(obj,'SaveResults');
        end
        function obj = set.SaveToDataset(obj,val)
            obj = setParameter(obj,'SaveToDataset',val,0,'boolean');
        end
        function res = get.SaveToDataset(obj)
            res = getParameter(obj,'SaveToDataset');
        end
        function obj = set.SortNoise(obj,val)
            obj = setParameter(obj,'SortNoise',val,0,'integer');
        end
        function res = get.SortNoise(obj)
            res = getParameter(obj,'SortNoise');
        end
        function obj = set.SpectralProduct(obj,val)
            obj = setParameter(obj,'SpectralProduct',val,1,'boolean');
        end
        function res = get.SpectralProduct(obj)
            res = getParameter(obj,'SpectralProduct');
        end
        function obj = set.StatusLevel(obj,val)
            obj = setParameter(obj,'StatusLevel',val,0,'integer');
        end
        function res = get.StatusLevel(obj)
            res = getParameter(obj,'StatusLevel');
        end
        function obj = set.SweepOffset(obj,val)
            obj = setParameter(obj,'SweepOffset',val,0,'real');
        end
        function res = get.SweepOffset(obj)
            res = getParameter(obj,'SweepOffset');
        end
        function obj = set.SweepPlan(obj,val)
            obj = setParameter(obj,'SweepPlan',val,1,'string');
        end
        function res = get.SweepPlan(obj)
            res = getParameter(obj,'SweepPlan');
        end
        function obj = set.SweepVar(obj,val)
            obj = setParameter(obj,'SweepVar',val,0,'string');
        end
        function res = get.SweepVar(obj)
            res = getParameter(obj,'SweepVar');
        end
        function obj = set.TAHB_Enable(obj,val)
            obj = setParameter(obj,'TAHB_Enable',val,0,'integer');
        end
        function res = get.TAHB_Enable(obj)
            res = getParameter(obj,'TAHB_Enable');
        end
        function obj = set.TempForNoise(obj,val)
            obj = setParameter(obj,'TempForNoise',val,0,'real');
        end
        function res = get.TempForNoise(obj)
            res = getParameter(obj,'TempForNoise');
        end
        function obj = set.Timeout(obj,val)
            obj = setParameter(obj,'Timeout',val,0,'real');
        end
        function res = get.Timeout(obj)
            res = getParameter(obj,'Timeout');
        end
        function obj = set.UseAllSS_Freqs(obj,val)
            obj = setParameter(obj,'UseAllSS_Freqs',val,0,'boolean');
        end
        function res = get.UseAllSS_Freqs(obj)
            res = getParameter(obj,'UseAllSS_Freqs');
        end
        function obj = set.UseCompactFreqMap(obj,val)
            obj = setParameter(obj,'UseCompactFreqMap',val,0,'boolean');
        end
        function res = get.UseCompactFreqMap(obj)
            res = getParameter(obj,'UseCompactFreqMap');
        end
        function obj = set.UseDeflation(obj,val)
            obj = setParameter(obj,'UseDeflation',val,0,'boolean');
        end
        function res = get.UseDeflation(obj)
            res = getParameter(obj,'UseDeflation');
        end
        function obj = set.UseGear(obj,val)
            obj = setParameter(obj,'UseGear',val,0,'boolean');
        end
        function res = get.UseGear(obj)
            res = getParameter(obj,'UseGear');
        end
        function obj = set.UseInFile(obj,val)
            obj = setParameter(obj,'UseInFile',val,0,'boolean');
        end
        function res = get.UseInFile(obj)
            res = getParameter(obj,'UseInFile');
        end
        function obj = set.UseInitialAWHB(obj,val)
            obj = setParameter(obj,'UseInitialAWHB',val,0,'boolean');
        end
        function res = get.UseInitialAWHB(obj)
            res = getParameter(obj,'UseInitialAWHB');
        end
        function obj = set.UseKrylov(obj,val)
            obj = setParameter(obj,'UseKrylov',val,0,'integer');
        end
        function res = get.UseKrylov(obj)
            res = getParameter(obj,'UseKrylov');
        end
        function obj = set.UseOptDamp(obj,val)
            obj = setParameter(obj,'UseOptDamp',val,0,'boolean');
        end
        function res = get.UseOptDamp(obj)
            res = getParameter(obj,'UseOptDamp');
        end
        function obj = set.UseOutFile(obj,val)
            obj = setParameter(obj,'UseOutFile',val,0,'boolean');
        end
        function res = get.UseOutFile(obj)
            res = getParameter(obj,'UseOutFile');
        end
        function obj = set.UseRelaxation(obj,val)
            obj = setParameter(obj,'UseRelaxation',val,0,'boolean');
        end
        function res = get.UseRelaxation(obj)
            res = getParameter(obj,'UseRelaxation');
        end
        function obj = set.VTB(obj,val)
            obj = setParameter(obj,'VTB',val,0,'string');
        end
        function res = get.VTB(obj)
            res = getParameter(obj,'VTB');
        end
        function obj = set.XtalOscStart(obj,val)
            obj = setParameter(obj,'XtalOscStart',val,0,'boolean');
        end
        function res = get.XtalOscStart(obj)
            res = getParameter(obj,'XtalOscStart');
        end
        function obj = set.bspInFile(obj,val)
            obj = setParameter(obj,'bspInFile',val,0,'string');
        end
        function res = get.bspInFile(obj)
            res = getParameter(obj,'bspInFile');
        end
        function obj = set.bspOutFile(obj,val)
            obj = setParameter(obj,'bspOutFile',val,0,'string');
        end
        function res = get.bspOutFile(obj)
            res = getParameter(obj,'bspOutFile');
        end
        function obj = set.bspRHS_Thresh(obj,val)
            obj = setParameter(obj,'bspRHS_Thresh',val,0,'real');
        end
        function res = get.bspRHS_Thresh(obj)
            res = getParameter(obj,'bspRHS_Thresh');
        end
    end
end
