classdef ADS_reluctance < ADScomponent
    % ADS_reluctance matlab representation for the ADS reluctance component
    % User Defined K Element N Port
    % reluctance [:Name] p1 n1 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % K element matrix entries (smorr) Unit: H^-1
        reluctance
        % All ports are shorted (s---b) 
        ShortAll
        % Couplings are ignored (s---b) 
        IgnoreCoupling
        % Reluctance data file name with absolute path (s---s) 
        file
    end
    methods
        function obj = set.reluctance(obj,val)
            obj = setParameter(obj,'reluctance',val,0,'real');
        end
        function res = get.reluctance(obj)
            res = getParameter(obj,'reluctance');
        end
        function obj = set.ShortAll(obj,val)
            obj = setParameter(obj,'ShortAll',val,0,'boolean');
        end
        function res = get.ShortAll(obj)
            res = getParameter(obj,'ShortAll');
        end
        function obj = set.IgnoreCoupling(obj,val)
            obj = setParameter(obj,'IgnoreCoupling',val,0,'boolean');
        end
        function res = get.IgnoreCoupling(obj)
            res = getParameter(obj,'IgnoreCoupling');
        end
        function obj = set.file(obj,val)
            obj = setParameter(obj,'file',val,0,'string');
        end
        function res = get.file(obj)
            res = getParameter(obj,'file');
        end
    end
end
