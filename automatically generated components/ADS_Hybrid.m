classdef ADS_Hybrid < ADScomponent
    % ADS_Hybrid matlab representation for the ADS Hybrid component
    % User Defined Linear Hybrid Parameter 2 Port
    % Hybrid [:Name] p1 n1 p2 n2
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Input impedance (smorc) Unit: Ohms
        H11
        % Reverse voltage gain (smorc) 
        H12
        % Forward current gain (smorc) 
        H21
        % Output conductance (smorc) Unit: S
        H22
        % Non-causal function impulse response order (sm--i) 
        ImpNoncausalLength
        % Convolution mode (sm--i) 
        ImpMode
        % Maximum Frequency to which device is evaluated (smo-r) Unit: Hz
        ImpMaxFreq
        % Sample spacing in frequency (smo-r) Unit: Hz
        ImpDeltaFreq
        % maximum allowed impulse response order (sm--i) 
        ImpMaxOrder
        % Smoothing Window (sm--i) 
        ImpWindow
        % Relative impulse Response truncation factor (smo-r) 
        ImpRelTol
        % Absolute impulse Response truncation factor (smo-r) 
        ImpAbsTol
    end
    methods
        function obj = set.H11(obj,val)
            obj = setParameter(obj,'H11',val,0,'complex');
        end
        function res = get.H11(obj)
            res = getParameter(obj,'H11');
        end
        function obj = set.H12(obj,val)
            obj = setParameter(obj,'H12',val,0,'complex');
        end
        function res = get.H12(obj)
            res = getParameter(obj,'H12');
        end
        function obj = set.H21(obj,val)
            obj = setParameter(obj,'H21',val,0,'complex');
        end
        function res = get.H21(obj)
            res = getParameter(obj,'H21');
        end
        function obj = set.H22(obj,val)
            obj = setParameter(obj,'H22',val,0,'complex');
        end
        function res = get.H22(obj)
            res = getParameter(obj,'H22');
        end
        function obj = set.ImpNoncausalLength(obj,val)
            obj = setParameter(obj,'ImpNoncausalLength',val,0,'integer');
        end
        function res = get.ImpNoncausalLength(obj)
            res = getParameter(obj,'ImpNoncausalLength');
        end
        function obj = set.ImpMode(obj,val)
            obj = setParameter(obj,'ImpMode',val,0,'integer');
        end
        function res = get.ImpMode(obj)
            res = getParameter(obj,'ImpMode');
        end
        function obj = set.ImpMaxFreq(obj,val)
            obj = setParameter(obj,'ImpMaxFreq',val,0,'real');
        end
        function res = get.ImpMaxFreq(obj)
            res = getParameter(obj,'ImpMaxFreq');
        end
        function obj = set.ImpDeltaFreq(obj,val)
            obj = setParameter(obj,'ImpDeltaFreq',val,0,'real');
        end
        function res = get.ImpDeltaFreq(obj)
            res = getParameter(obj,'ImpDeltaFreq');
        end
        function obj = set.ImpMaxOrder(obj,val)
            obj = setParameter(obj,'ImpMaxOrder',val,0,'integer');
        end
        function res = get.ImpMaxOrder(obj)
            res = getParameter(obj,'ImpMaxOrder');
        end
        function obj = set.ImpWindow(obj,val)
            obj = setParameter(obj,'ImpWindow',val,0,'integer');
        end
        function res = get.ImpWindow(obj)
            res = getParameter(obj,'ImpWindow');
        end
        function obj = set.ImpRelTol(obj,val)
            obj = setParameter(obj,'ImpRelTol',val,0,'real');
        end
        function res = get.ImpRelTol(obj)
            res = getParameter(obj,'ImpRelTol');
        end
        function obj = set.ImpAbsTol(obj,val)
            obj = setParameter(obj,'ImpAbsTol',val,0,'real');
        end
        function res = get.ImpAbsTol(obj)
            res = getParameter(obj,'ImpAbsTol');
        end
    end
end
