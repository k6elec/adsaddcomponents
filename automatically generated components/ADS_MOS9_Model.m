classdef ADS_MOS9_Model < ADSmodel
    % ADS_MOS9_Model matlab representation for the ADS MOS9_Model component
    % MOS MODEL 9 model
    % model ModelName MOS9 ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % NMOS type MOS9 (s---b) 
        NMOS
        % PMOS type MOS9 (s---b) 
        PMOS
        % Effective channel length of the reference transistor (smorr) Unit: m
        Ler
        % Effective channel width of the refence transistor (smorr) Unit: m
        Wer
        % Difference between the actual and the programmed poly-silicon gate length (smorr) Unit: m
        Lvar
        % Effective channel length reduction per side due to the lateral diffusion of the source/drain dopant ions (smorr) Unit: m
        Lap
        % Difference between the actual and the programmed field-oxide opening (smorr) Unit: m
        Wvar
        % Effective reduction of the channel width per side due to the lateral diffusion of the channel-stop dopant ions (smorr) Unit: m
        Wot
        % Temperature at which the parameters for the reference transistor have been determined (smorr) Unit: deg C
        Tr
        % Temperature at which the parameters for the reference transistor have been determined (smorr) Unit: deg C
        Tnom
        % Ambient device temperature (smorr) Unit: deg C
        Ta
        % Temperature rise above ambient (smorr) Unit: deg C
        Trise
        % Threshold voltage at zero back-bias for the reference transistor at the reference temperature (smorr) Unit: V
        Vtor
        % Coefficient of the temperature dependence of VTO (smorr) Unit: V/K
        Stvto
        % Coefficient of the length dependence of VTO (smorr) Unit: V*m
        Slvto
        % Second coefficient of the temperature dependence of VTO (smorr) Unit: V*m^2
        Sl2vto
        % Coefficient of the width dependence of VTO (smorr) Unit: V*m
        Swvto
        % Low-back-bias body factor for the reference transistor (smorr) Unit: V^(1/2)
        Kor
        % Coefficient of the length dependence of KO (smorr) Unit: m*V^(1/2)
        Slko
        % Coefficient of the width dependence of KO (smorr) Unit: m*V^(1/2)
        Swko
        % High-back-bias body factor for the reference transistor (smorr) Unit: V^(1/2)
        Kr
        % Coefficient of the lenght dependence of K (smorr) Unit: m*V^(1/2)
        Slk
        % Coefficient of the width dependence of K (smorr) Unit: m*V^(1/2)
        Swk
        % Surface potential at strong inversion for the reference transistor at the reference temperature (smorr) Unit: V
        Phibr
        % Transition voltage for the dual-k-factor model for the reference transistor (smorr) Unit: V
        Vsbxr
        % Coefficient of the length dependence of VSBX (smorr) Unit: V*m
        Slvsbx
        % Coefficient of the width dependence of VSBX (smorr) Unit: V*m
        Swvsbx
        % Gain factor for an infinite square transistor at the reference temperature (smorr) Unit: A/V^2
        Betsq
        % Exponent of the temperature dependence of the gain factor (smorr) 
        Etabet
        % Coefficient of the mobility reduction due to the gate-induced field for the reference transistor at the reference temperature (smorr) Unit: V^-1
        The1r
        % Coefficient of the temperature dependence of THE1 for the reference transistor (smorr) Unit: V^-1/K
        Stthe1r
        % Coefficient of the length dependence of THE1 for the reference transistor (smorr) Unit: m/V
        Slthe1r
        % Coefficient of the temperature dependence of the length dependence of THE1 (smorr) Unit: m/V/K
        Stlthe1
        % Coefficient of the width dependence of THE1 (smorr) Unit: m/V
        Swthe1
        % Coefficient of the mobility reduction due to the back-bias for the reference transistor at the reference temperature (smorr) Unit: V^(-1/2)
        The2r
        % Coefficient of the temperature dependence of THE2 for the reference transistor (smorr) Unit: V^(-1/2)/K
        Stthe2r
        % Coefficient of the length dependence of THE2 at the reference temperature (smorr) Unit: m/V^(1/2)
        Slthe2r
        % Coefficient of the temperature dependence of the length dependence (smorr) Unit: m/V^(1/2)/K
        Stlthe2
        % Coefficient of the width dependence of THE2 (smorr) Unit: m/V^(1/2)
        Swthe2
        % Coefficient of the mobility reduction due to the lateral field for the reference transistor at the reference temperature (smorr) Unit: V^-1
        The3r
        % Coefficient of the termperature dependence of THE3 for the reference transistor (smorr) Unit: V^-1/K
        Stthe3r
        % Coefficient of the length dependence of THE3 at the reference temperature (smorr) Unit: m/V
        Slthe3r
        % Coefficient of the temperature dependece of the length dependence of THE3 (smorr) Unit: m/V/K
        Stlthe3
        % Coefficient of the width dependence of THE3 (smorr) Unit: m/V
        Swthe3
        % Coefficient for the drain induced threshold shift for large gate drive for the reference transistor (smorr) Unit: V
        Gam1r
        % Coefficient of the length dependence of GAM1 (smorr) Unit: V*m
        Slgam1
        % Coefficient of the width dependence of GAM1 (smorr) Unit: V*m
        Swgam1
        % Exponent of the VDS dependence of GAM1 for the reference transistor (smorr) 
        Etadsr
        % Factor of the channel-length modulation for the reference transistor (smorr) 
        Alpr
        % Exponent of the length dependence of ALP (smorr) 
        Etaalp
        % Coefficient of the length dependence of ALP (smorr) Unit: m
        Slalp
        % Coefficient of the width dependence of ALP (smorr) Unit: m
        Swalp
        % Characteristic voltage of channel length modulation for the reference transistor (smorr) Unit: V
        Vpr
        % Coefficient of the drain induced threshold shift at zero gate drive for the reference transistor (smorr) 
        Gamoor
        % Coefficient of the length dependence of GAMOO (smorr) Unit: m^2
        Slgamoo
        % Exponent of the back-bias dependence of GAMO for the reference transistor (smorr) 
        Etagamr
        % Factor of the subthreshold slope for the reference transistor at the reference temperature (smorr) 
        Mor
        % Coefficient of the temperature dependence of MO (smorr) Unit: K^-1
        Stmo
        % Coefficient of the length dependence of MO (smorr) Unit: m^(1/2)
        Slmo
        % Exponent of the back-bias dependence of M for the reference transistor (smorr) 
        Etamr
        % Weak inversion correction factor for the reference transistor (smorr) 
        Zet1r
        % Exponent of the length dependence of ZET (smorr) 
        Etazet
        % Coefficient of the length dependence of ZET (smorr) Unit: m
        Slzet1
        % Limiting voltage of the VSB dependence of M and GAMO for the reference transistor (smorr) Unit: V
        Vsbtr
        % Coefficient of the length dependence of VSBT (smorr) Unit: V*m
        Slvsbt
        % Factor of the weak-avalanche current for the reference transistor at the reference temperature (smorr) 
        A1r
        % Coefficient of the temperature dependence of A1 (smorr) Unit: K^-1
        Sta1
        % Coefficient of the length dependence of A1 (smorr) Unit: m
        Sla1
        % Coefficient of the width dependence of A1 (smorr) Unit: m
        Swa1
        % Exponent of the weak-avalanche current for the reference transistor (smorr) Unit: V
        A2r
        % Coefficient of the length dependence of A2 (smorr) Unit: V*m
        Sla2
        % Coefficient of the width dependence of A2 (smorr) Unit: V*m
        Swa2
        % Factor of the D-S voltage above which weak-avalanch occurs, for the reference transistor (smorr) 
        A3r
        % Coefficient of the length dependence of A3 (smorr) Unit: m
        Sla3
        % Coefficient of the width dependence of A3 (smorr) Unit: m
        Swa3
        % Thickness of the oxide layer (smorr) Unit: m
        Tox
        % Gate overlap capacitance per unit channel width (smorr) Unit: F/m
        Col
        % Gate-to-channel capacitance (smorr) Unit: F
        Cox
        % G-D overlap capacitance (smorr) Unit: F
        Cgdo
        % G-S overlap capacitance (smorr) Unit: F
        Cgso
        % Coefficient of the thermal noise for the reference transistor (smorr) Unit: J
        Ntr
        % Coefficient of the flicker noise for the reference transistor (smorr) Unit: V^2
        Nfr
        % Type of model: 1:SINGLE_DEVICE 2:PROCESS_BASED (s---i) 
        Type
        % Voltage at which parameters have been determined (smorr) Unit: V
        Vr
        % Bottom saturation-current density due to electron-hole generation at V=Vr (smorr) Unit: A/m^2
        Jsgbr
        % Bottom saturation-current density due to diffusion  from back contact (smorr) Unit: A/m^2
        Jsdbr
        % Sidewall saturation-current density due to electron-hole generation at V=Vr (smorr) Unit: A/m
        Jsgsr
        % Sidewall saturation-current density due to diffusion  from back contact (smorr) Unit: A/m
        Jsdsr
        % Gate-edge saturation-current density due to electron-hole generation at V=Vr (smorr) Unit: A/m
        Jsggr
        % Gate-edge saturation-current density due to diffusion  from back contact (smorr) Unit: A/m
        Jsdgr
        % Bottom junction capacitance at V = Vr (smorr) Unit: F/m^2
        Cjbr
        % Sidewall junction capacitance at V = Vr (smorr) Unit: F/m
        Cjsr
        % Gate edge junction capacitance at V = Vr (smorr) Unit: F/m
        Cjgr
        % Diffusion voltage of the bottom junction at T = Tr (smorr) Unit: V
        Vdbr
        % Diffusion voltage of the sidewall junction at T = Tr (smorr) Unit: V
        Vdsr
        % Diffusion voltage of the gate edge junction at T = Tr (smorr) Unit: V
        Vdgr
        % Bottom-junction grading coefficient (smorr) 
        Pb
        % Sidewall-junction grading coefficient (smorr) 
        Ps
        % Gate-edge-junction grading coefficient (smorr) 
        Pg
        % Emission coefficient of the bottom forward current (smorr) 
        Nb
        % Emission coefficient of the sidewall forward current (smorr) 
        Ns
        % Emission coefficient of the gate edge forward current (smorr) 
        Ng
        % Characteristic drawn gate width, below which dogboning appears (smorr) Unit: m
        Wdog
        % Coefficient describing the width dependence of THE1 for W<Wdog (smorr) 
        Fthe1
        % Switch that selects either old or new flicker noise model (s---i) 
        Nfmod
        % First coefficient of the flicker noise for the reference transistor (Nfmod=1) (smorr) Unit: V^-1*m^-1
        Nfar
        % Second coefficient of the flicker noise for the reference transistor (Nfmod=1) (smorr) Unit: V^-1*m^-2
        Nfbr
        % Third coefficient of the flicker noise for the reference transistor (Nfmod=1) (smorr) Unit: V^-1
        Nfcr
        % Flag for The3 clipping (s---b) 
        The3Clipping
        % Secured model parameters (s---b) 
        Secured
        % Substrate junction forward bias (warning) (s--rr) Unit: V
        wVsubfwd
        % Substrate junction reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvsub
        % Gate oxide breakdown voltage (warning) (s--rr) Unit: V
        wBvg
        % Drain-source breakdown voltage (warning) (s--rr) Unit: V
        wBvds
        % Maximum drain-source current (warning) (s--rr) Unit: A
        wIdsmax
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
    end
    methods
        function obj = set.NMOS(obj,val)
            obj = setParameter(obj,'NMOS',val,0,'boolean');
        end
        function res = get.NMOS(obj)
            res = getParameter(obj,'NMOS');
        end
        function obj = set.PMOS(obj,val)
            obj = setParameter(obj,'PMOS',val,0,'boolean');
        end
        function res = get.PMOS(obj)
            res = getParameter(obj,'PMOS');
        end
        function obj = set.Ler(obj,val)
            obj = setParameter(obj,'Ler',val,0,'real');
        end
        function res = get.Ler(obj)
            res = getParameter(obj,'Ler');
        end
        function obj = set.Wer(obj,val)
            obj = setParameter(obj,'Wer',val,0,'real');
        end
        function res = get.Wer(obj)
            res = getParameter(obj,'Wer');
        end
        function obj = set.Lvar(obj,val)
            obj = setParameter(obj,'Lvar',val,0,'real');
        end
        function res = get.Lvar(obj)
            res = getParameter(obj,'Lvar');
        end
        function obj = set.Lap(obj,val)
            obj = setParameter(obj,'Lap',val,0,'real');
        end
        function res = get.Lap(obj)
            res = getParameter(obj,'Lap');
        end
        function obj = set.Wvar(obj,val)
            obj = setParameter(obj,'Wvar',val,0,'real');
        end
        function res = get.Wvar(obj)
            res = getParameter(obj,'Wvar');
        end
        function obj = set.Wot(obj,val)
            obj = setParameter(obj,'Wot',val,0,'real');
        end
        function res = get.Wot(obj)
            res = getParameter(obj,'Wot');
        end
        function obj = set.Tr(obj,val)
            obj = setParameter(obj,'Tr',val,0,'real');
        end
        function res = get.Tr(obj)
            res = getParameter(obj,'Tr');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Ta(obj,val)
            obj = setParameter(obj,'Ta',val,0,'real');
        end
        function res = get.Ta(obj)
            res = getParameter(obj,'Ta');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Vtor(obj,val)
            obj = setParameter(obj,'Vtor',val,0,'real');
        end
        function res = get.Vtor(obj)
            res = getParameter(obj,'Vtor');
        end
        function obj = set.Stvto(obj,val)
            obj = setParameter(obj,'Stvto',val,0,'real');
        end
        function res = get.Stvto(obj)
            res = getParameter(obj,'Stvto');
        end
        function obj = set.Slvto(obj,val)
            obj = setParameter(obj,'Slvto',val,0,'real');
        end
        function res = get.Slvto(obj)
            res = getParameter(obj,'Slvto');
        end
        function obj = set.Sl2vto(obj,val)
            obj = setParameter(obj,'Sl2vto',val,0,'real');
        end
        function res = get.Sl2vto(obj)
            res = getParameter(obj,'Sl2vto');
        end
        function obj = set.Swvto(obj,val)
            obj = setParameter(obj,'Swvto',val,0,'real');
        end
        function res = get.Swvto(obj)
            res = getParameter(obj,'Swvto');
        end
        function obj = set.Kor(obj,val)
            obj = setParameter(obj,'Kor',val,0,'real');
        end
        function res = get.Kor(obj)
            res = getParameter(obj,'Kor');
        end
        function obj = set.Slko(obj,val)
            obj = setParameter(obj,'Slko',val,0,'real');
        end
        function res = get.Slko(obj)
            res = getParameter(obj,'Slko');
        end
        function obj = set.Swko(obj,val)
            obj = setParameter(obj,'Swko',val,0,'real');
        end
        function res = get.Swko(obj)
            res = getParameter(obj,'Swko');
        end
        function obj = set.Kr(obj,val)
            obj = setParameter(obj,'Kr',val,0,'real');
        end
        function res = get.Kr(obj)
            res = getParameter(obj,'Kr');
        end
        function obj = set.Slk(obj,val)
            obj = setParameter(obj,'Slk',val,0,'real');
        end
        function res = get.Slk(obj)
            res = getParameter(obj,'Slk');
        end
        function obj = set.Swk(obj,val)
            obj = setParameter(obj,'Swk',val,0,'real');
        end
        function res = get.Swk(obj)
            res = getParameter(obj,'Swk');
        end
        function obj = set.Phibr(obj,val)
            obj = setParameter(obj,'Phibr',val,0,'real');
        end
        function res = get.Phibr(obj)
            res = getParameter(obj,'Phibr');
        end
        function obj = set.Vsbxr(obj,val)
            obj = setParameter(obj,'Vsbxr',val,0,'real');
        end
        function res = get.Vsbxr(obj)
            res = getParameter(obj,'Vsbxr');
        end
        function obj = set.Slvsbx(obj,val)
            obj = setParameter(obj,'Slvsbx',val,0,'real');
        end
        function res = get.Slvsbx(obj)
            res = getParameter(obj,'Slvsbx');
        end
        function obj = set.Swvsbx(obj,val)
            obj = setParameter(obj,'Swvsbx',val,0,'real');
        end
        function res = get.Swvsbx(obj)
            res = getParameter(obj,'Swvsbx');
        end
        function obj = set.Betsq(obj,val)
            obj = setParameter(obj,'Betsq',val,0,'real');
        end
        function res = get.Betsq(obj)
            res = getParameter(obj,'Betsq');
        end
        function obj = set.Etabet(obj,val)
            obj = setParameter(obj,'Etabet',val,0,'real');
        end
        function res = get.Etabet(obj)
            res = getParameter(obj,'Etabet');
        end
        function obj = set.The1r(obj,val)
            obj = setParameter(obj,'The1r',val,0,'real');
        end
        function res = get.The1r(obj)
            res = getParameter(obj,'The1r');
        end
        function obj = set.Stthe1r(obj,val)
            obj = setParameter(obj,'Stthe1r',val,0,'real');
        end
        function res = get.Stthe1r(obj)
            res = getParameter(obj,'Stthe1r');
        end
        function obj = set.Slthe1r(obj,val)
            obj = setParameter(obj,'Slthe1r',val,0,'real');
        end
        function res = get.Slthe1r(obj)
            res = getParameter(obj,'Slthe1r');
        end
        function obj = set.Stlthe1(obj,val)
            obj = setParameter(obj,'Stlthe1',val,0,'real');
        end
        function res = get.Stlthe1(obj)
            res = getParameter(obj,'Stlthe1');
        end
        function obj = set.Swthe1(obj,val)
            obj = setParameter(obj,'Swthe1',val,0,'real');
        end
        function res = get.Swthe1(obj)
            res = getParameter(obj,'Swthe1');
        end
        function obj = set.The2r(obj,val)
            obj = setParameter(obj,'The2r',val,0,'real');
        end
        function res = get.The2r(obj)
            res = getParameter(obj,'The2r');
        end
        function obj = set.Stthe2r(obj,val)
            obj = setParameter(obj,'Stthe2r',val,0,'real');
        end
        function res = get.Stthe2r(obj)
            res = getParameter(obj,'Stthe2r');
        end
        function obj = set.Slthe2r(obj,val)
            obj = setParameter(obj,'Slthe2r',val,0,'real');
        end
        function res = get.Slthe2r(obj)
            res = getParameter(obj,'Slthe2r');
        end
        function obj = set.Stlthe2(obj,val)
            obj = setParameter(obj,'Stlthe2',val,0,'real');
        end
        function res = get.Stlthe2(obj)
            res = getParameter(obj,'Stlthe2');
        end
        function obj = set.Swthe2(obj,val)
            obj = setParameter(obj,'Swthe2',val,0,'real');
        end
        function res = get.Swthe2(obj)
            res = getParameter(obj,'Swthe2');
        end
        function obj = set.The3r(obj,val)
            obj = setParameter(obj,'The3r',val,0,'real');
        end
        function res = get.The3r(obj)
            res = getParameter(obj,'The3r');
        end
        function obj = set.Stthe3r(obj,val)
            obj = setParameter(obj,'Stthe3r',val,0,'real');
        end
        function res = get.Stthe3r(obj)
            res = getParameter(obj,'Stthe3r');
        end
        function obj = set.Slthe3r(obj,val)
            obj = setParameter(obj,'Slthe3r',val,0,'real');
        end
        function res = get.Slthe3r(obj)
            res = getParameter(obj,'Slthe3r');
        end
        function obj = set.Stlthe3(obj,val)
            obj = setParameter(obj,'Stlthe3',val,0,'real');
        end
        function res = get.Stlthe3(obj)
            res = getParameter(obj,'Stlthe3');
        end
        function obj = set.Swthe3(obj,val)
            obj = setParameter(obj,'Swthe3',val,0,'real');
        end
        function res = get.Swthe3(obj)
            res = getParameter(obj,'Swthe3');
        end
        function obj = set.Gam1r(obj,val)
            obj = setParameter(obj,'Gam1r',val,0,'real');
        end
        function res = get.Gam1r(obj)
            res = getParameter(obj,'Gam1r');
        end
        function obj = set.Slgam1(obj,val)
            obj = setParameter(obj,'Slgam1',val,0,'real');
        end
        function res = get.Slgam1(obj)
            res = getParameter(obj,'Slgam1');
        end
        function obj = set.Swgam1(obj,val)
            obj = setParameter(obj,'Swgam1',val,0,'real');
        end
        function res = get.Swgam1(obj)
            res = getParameter(obj,'Swgam1');
        end
        function obj = set.Etadsr(obj,val)
            obj = setParameter(obj,'Etadsr',val,0,'real');
        end
        function res = get.Etadsr(obj)
            res = getParameter(obj,'Etadsr');
        end
        function obj = set.Alpr(obj,val)
            obj = setParameter(obj,'Alpr',val,0,'real');
        end
        function res = get.Alpr(obj)
            res = getParameter(obj,'Alpr');
        end
        function obj = set.Etaalp(obj,val)
            obj = setParameter(obj,'Etaalp',val,0,'real');
        end
        function res = get.Etaalp(obj)
            res = getParameter(obj,'Etaalp');
        end
        function obj = set.Slalp(obj,val)
            obj = setParameter(obj,'Slalp',val,0,'real');
        end
        function res = get.Slalp(obj)
            res = getParameter(obj,'Slalp');
        end
        function obj = set.Swalp(obj,val)
            obj = setParameter(obj,'Swalp',val,0,'real');
        end
        function res = get.Swalp(obj)
            res = getParameter(obj,'Swalp');
        end
        function obj = set.Vpr(obj,val)
            obj = setParameter(obj,'Vpr',val,0,'real');
        end
        function res = get.Vpr(obj)
            res = getParameter(obj,'Vpr');
        end
        function obj = set.Gamoor(obj,val)
            obj = setParameter(obj,'Gamoor',val,0,'real');
        end
        function res = get.Gamoor(obj)
            res = getParameter(obj,'Gamoor');
        end
        function obj = set.Slgamoo(obj,val)
            obj = setParameter(obj,'Slgamoo',val,0,'real');
        end
        function res = get.Slgamoo(obj)
            res = getParameter(obj,'Slgamoo');
        end
        function obj = set.Etagamr(obj,val)
            obj = setParameter(obj,'Etagamr',val,0,'real');
        end
        function res = get.Etagamr(obj)
            res = getParameter(obj,'Etagamr');
        end
        function obj = set.Mor(obj,val)
            obj = setParameter(obj,'Mor',val,0,'real');
        end
        function res = get.Mor(obj)
            res = getParameter(obj,'Mor');
        end
        function obj = set.Stmo(obj,val)
            obj = setParameter(obj,'Stmo',val,0,'real');
        end
        function res = get.Stmo(obj)
            res = getParameter(obj,'Stmo');
        end
        function obj = set.Slmo(obj,val)
            obj = setParameter(obj,'Slmo',val,0,'real');
        end
        function res = get.Slmo(obj)
            res = getParameter(obj,'Slmo');
        end
        function obj = set.Etamr(obj,val)
            obj = setParameter(obj,'Etamr',val,0,'real');
        end
        function res = get.Etamr(obj)
            res = getParameter(obj,'Etamr');
        end
        function obj = set.Zet1r(obj,val)
            obj = setParameter(obj,'Zet1r',val,0,'real');
        end
        function res = get.Zet1r(obj)
            res = getParameter(obj,'Zet1r');
        end
        function obj = set.Etazet(obj,val)
            obj = setParameter(obj,'Etazet',val,0,'real');
        end
        function res = get.Etazet(obj)
            res = getParameter(obj,'Etazet');
        end
        function obj = set.Slzet1(obj,val)
            obj = setParameter(obj,'Slzet1',val,0,'real');
        end
        function res = get.Slzet1(obj)
            res = getParameter(obj,'Slzet1');
        end
        function obj = set.Vsbtr(obj,val)
            obj = setParameter(obj,'Vsbtr',val,0,'real');
        end
        function res = get.Vsbtr(obj)
            res = getParameter(obj,'Vsbtr');
        end
        function obj = set.Slvsbt(obj,val)
            obj = setParameter(obj,'Slvsbt',val,0,'real');
        end
        function res = get.Slvsbt(obj)
            res = getParameter(obj,'Slvsbt');
        end
        function obj = set.A1r(obj,val)
            obj = setParameter(obj,'A1r',val,0,'real');
        end
        function res = get.A1r(obj)
            res = getParameter(obj,'A1r');
        end
        function obj = set.Sta1(obj,val)
            obj = setParameter(obj,'Sta1',val,0,'real');
        end
        function res = get.Sta1(obj)
            res = getParameter(obj,'Sta1');
        end
        function obj = set.Sla1(obj,val)
            obj = setParameter(obj,'Sla1',val,0,'real');
        end
        function res = get.Sla1(obj)
            res = getParameter(obj,'Sla1');
        end
        function obj = set.Swa1(obj,val)
            obj = setParameter(obj,'Swa1',val,0,'real');
        end
        function res = get.Swa1(obj)
            res = getParameter(obj,'Swa1');
        end
        function obj = set.A2r(obj,val)
            obj = setParameter(obj,'A2r',val,0,'real');
        end
        function res = get.A2r(obj)
            res = getParameter(obj,'A2r');
        end
        function obj = set.Sla2(obj,val)
            obj = setParameter(obj,'Sla2',val,0,'real');
        end
        function res = get.Sla2(obj)
            res = getParameter(obj,'Sla2');
        end
        function obj = set.Swa2(obj,val)
            obj = setParameter(obj,'Swa2',val,0,'real');
        end
        function res = get.Swa2(obj)
            res = getParameter(obj,'Swa2');
        end
        function obj = set.A3r(obj,val)
            obj = setParameter(obj,'A3r',val,0,'real');
        end
        function res = get.A3r(obj)
            res = getParameter(obj,'A3r');
        end
        function obj = set.Sla3(obj,val)
            obj = setParameter(obj,'Sla3',val,0,'real');
        end
        function res = get.Sla3(obj)
            res = getParameter(obj,'Sla3');
        end
        function obj = set.Swa3(obj,val)
            obj = setParameter(obj,'Swa3',val,0,'real');
        end
        function res = get.Swa3(obj)
            res = getParameter(obj,'Swa3');
        end
        function obj = set.Tox(obj,val)
            obj = setParameter(obj,'Tox',val,0,'real');
        end
        function res = get.Tox(obj)
            res = getParameter(obj,'Tox');
        end
        function obj = set.Col(obj,val)
            obj = setParameter(obj,'Col',val,0,'real');
        end
        function res = get.Col(obj)
            res = getParameter(obj,'Col');
        end
        function obj = set.Cox(obj,val)
            obj = setParameter(obj,'Cox',val,0,'real');
        end
        function res = get.Cox(obj)
            res = getParameter(obj,'Cox');
        end
        function obj = set.Cgdo(obj,val)
            obj = setParameter(obj,'Cgdo',val,0,'real');
        end
        function res = get.Cgdo(obj)
            res = getParameter(obj,'Cgdo');
        end
        function obj = set.Cgso(obj,val)
            obj = setParameter(obj,'Cgso',val,0,'real');
        end
        function res = get.Cgso(obj)
            res = getParameter(obj,'Cgso');
        end
        function obj = set.Ntr(obj,val)
            obj = setParameter(obj,'Ntr',val,0,'real');
        end
        function res = get.Ntr(obj)
            res = getParameter(obj,'Ntr');
        end
        function obj = set.Nfr(obj,val)
            obj = setParameter(obj,'Nfr',val,0,'real');
        end
        function res = get.Nfr(obj)
            res = getParameter(obj,'Nfr');
        end
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'integer');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
        function obj = set.Vr(obj,val)
            obj = setParameter(obj,'Vr',val,0,'real');
        end
        function res = get.Vr(obj)
            res = getParameter(obj,'Vr');
        end
        function obj = set.Jsgbr(obj,val)
            obj = setParameter(obj,'Jsgbr',val,0,'real');
        end
        function res = get.Jsgbr(obj)
            res = getParameter(obj,'Jsgbr');
        end
        function obj = set.Jsdbr(obj,val)
            obj = setParameter(obj,'Jsdbr',val,0,'real');
        end
        function res = get.Jsdbr(obj)
            res = getParameter(obj,'Jsdbr');
        end
        function obj = set.Jsgsr(obj,val)
            obj = setParameter(obj,'Jsgsr',val,0,'real');
        end
        function res = get.Jsgsr(obj)
            res = getParameter(obj,'Jsgsr');
        end
        function obj = set.Jsdsr(obj,val)
            obj = setParameter(obj,'Jsdsr',val,0,'real');
        end
        function res = get.Jsdsr(obj)
            res = getParameter(obj,'Jsdsr');
        end
        function obj = set.Jsggr(obj,val)
            obj = setParameter(obj,'Jsggr',val,0,'real');
        end
        function res = get.Jsggr(obj)
            res = getParameter(obj,'Jsggr');
        end
        function obj = set.Jsdgr(obj,val)
            obj = setParameter(obj,'Jsdgr',val,0,'real');
        end
        function res = get.Jsdgr(obj)
            res = getParameter(obj,'Jsdgr');
        end
        function obj = set.Cjbr(obj,val)
            obj = setParameter(obj,'Cjbr',val,0,'real');
        end
        function res = get.Cjbr(obj)
            res = getParameter(obj,'Cjbr');
        end
        function obj = set.Cjsr(obj,val)
            obj = setParameter(obj,'Cjsr',val,0,'real');
        end
        function res = get.Cjsr(obj)
            res = getParameter(obj,'Cjsr');
        end
        function obj = set.Cjgr(obj,val)
            obj = setParameter(obj,'Cjgr',val,0,'real');
        end
        function res = get.Cjgr(obj)
            res = getParameter(obj,'Cjgr');
        end
        function obj = set.Vdbr(obj,val)
            obj = setParameter(obj,'Vdbr',val,0,'real');
        end
        function res = get.Vdbr(obj)
            res = getParameter(obj,'Vdbr');
        end
        function obj = set.Vdsr(obj,val)
            obj = setParameter(obj,'Vdsr',val,0,'real');
        end
        function res = get.Vdsr(obj)
            res = getParameter(obj,'Vdsr');
        end
        function obj = set.Vdgr(obj,val)
            obj = setParameter(obj,'Vdgr',val,0,'real');
        end
        function res = get.Vdgr(obj)
            res = getParameter(obj,'Vdgr');
        end
        function obj = set.Pb(obj,val)
            obj = setParameter(obj,'Pb',val,0,'real');
        end
        function res = get.Pb(obj)
            res = getParameter(obj,'Pb');
        end
        function obj = set.Ps(obj,val)
            obj = setParameter(obj,'Ps',val,0,'real');
        end
        function res = get.Ps(obj)
            res = getParameter(obj,'Ps');
        end
        function obj = set.Pg(obj,val)
            obj = setParameter(obj,'Pg',val,0,'real');
        end
        function res = get.Pg(obj)
            res = getParameter(obj,'Pg');
        end
        function obj = set.Nb(obj,val)
            obj = setParameter(obj,'Nb',val,0,'real');
        end
        function res = get.Nb(obj)
            res = getParameter(obj,'Nb');
        end
        function obj = set.Ns(obj,val)
            obj = setParameter(obj,'Ns',val,0,'real');
        end
        function res = get.Ns(obj)
            res = getParameter(obj,'Ns');
        end
        function obj = set.Ng(obj,val)
            obj = setParameter(obj,'Ng',val,0,'real');
        end
        function res = get.Ng(obj)
            res = getParameter(obj,'Ng');
        end
        function obj = set.Wdog(obj,val)
            obj = setParameter(obj,'Wdog',val,0,'real');
        end
        function res = get.Wdog(obj)
            res = getParameter(obj,'Wdog');
        end
        function obj = set.Fthe1(obj,val)
            obj = setParameter(obj,'Fthe1',val,0,'real');
        end
        function res = get.Fthe1(obj)
            res = getParameter(obj,'Fthe1');
        end
        function obj = set.Nfmod(obj,val)
            obj = setParameter(obj,'Nfmod',val,0,'integer');
        end
        function res = get.Nfmod(obj)
            res = getParameter(obj,'Nfmod');
        end
        function obj = set.Nfar(obj,val)
            obj = setParameter(obj,'Nfar',val,0,'real');
        end
        function res = get.Nfar(obj)
            res = getParameter(obj,'Nfar');
        end
        function obj = set.Nfbr(obj,val)
            obj = setParameter(obj,'Nfbr',val,0,'real');
        end
        function res = get.Nfbr(obj)
            res = getParameter(obj,'Nfbr');
        end
        function obj = set.Nfcr(obj,val)
            obj = setParameter(obj,'Nfcr',val,0,'real');
        end
        function res = get.Nfcr(obj)
            res = getParameter(obj,'Nfcr');
        end
        function obj = set.The3Clipping(obj,val)
            obj = setParameter(obj,'The3Clipping',val,0,'boolean');
        end
        function res = get.The3Clipping(obj)
            res = getParameter(obj,'The3Clipping');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
        function obj = set.wVsubfwd(obj,val)
            obj = setParameter(obj,'wVsubfwd',val,0,'real');
        end
        function res = get.wVsubfwd(obj)
            res = getParameter(obj,'wVsubfwd');
        end
        function obj = set.wBvsub(obj,val)
            obj = setParameter(obj,'wBvsub',val,0,'real');
        end
        function res = get.wBvsub(obj)
            res = getParameter(obj,'wBvsub');
        end
        function obj = set.wBvg(obj,val)
            obj = setParameter(obj,'wBvg',val,0,'real');
        end
        function res = get.wBvg(obj)
            res = getParameter(obj,'wBvg');
        end
        function obj = set.wBvds(obj,val)
            obj = setParameter(obj,'wBvds',val,0,'real');
        end
        function res = get.wBvds(obj)
            res = getParameter(obj,'wBvds');
        end
        function obj = set.wIdsmax(obj,val)
            obj = setParameter(obj,'wIdsmax',val,0,'real');
        end
        function res = get.wIdsmax(obj)
            res = getParameter(obj,'wIdsmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
    end
end
