classdef ADS_S_Param < ADSnodeless
    % ADS_S_Param matlab representation for the ADS S_Param component
    % Small-Signal S-Parameter Analysis
    % S_Param [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Name of variable or parameter to be swept (sm-rs) 
        SweepVar
        % Stim instance/path name for sweep values (repeatable) (sm-rd) 
        SweepPlan
        % Frequency value if it's not being swept (smorr) Unit: Hz
        Freq
        % Levels of subcircuits to output (sm-ri) 
        NestLevel
        % Degree of annotation (sm-ri) 
        StatusLevel
        % Variable or parameter to output (repeatable) (sm-rs) 
        OutVar
        % Active port (repeatable) (sm-rd) 
        ActivePort
        % S-parameters output file name (sm-rs) 
        File
        % Calculate y parameters (sm-rb) 
        CalcY
        % Calculate z parameters (sm-rb) 
        CalcZ
        % Calculate s parameters (sm-rb) 
        CalcS
        % Calculate noise parameters (sm-rb) 
        CalcNoise
        % Bandwidth for noise analysis (smorr) Unit: Hz
        BandwidthForNoise
        % Temperature for linear noise analysis (smorr) Unit: deg C
        TempForNoise
        % Calculate group delays (s---b) 
        CalcGroupDelay
        % Use finite differences for sensitivities (s---b) 
        UseFiniteDiff
        % Enable AC frequency conversion (s---b) 
        FreqConversion
        % S-parameter frequency conversion input port (s---i) 
        FreqConversionPort
        % Frequency aperture used to calculate group delay (smorr) 
        GroupDelayAperture
        % Sort Noise Contribution by: Value/1, Name/2  (default: 0/NoOutput) (s---i) 
        SortNoise
        % Noise Contribution Threshold (s---r) 
        NoiseThresh
        % Levels of DC Operating Point Data to output (s---i) 
        DevOpPtLevel
        % OutputPlan Name (s---s) 
        OutputPlan
        % Input port for noise figure calculation (sm-ri) 
        NoiseInputPort
        % Output port for noise figure calculation (sm-ri) 
        NoiseOutputPort
        % Turn S-Parameter analysis output to Dataset on or off (s---i) 
        SaveToDataset
        % Enforces Passivity (s---b) 
        EnforcePassivity
        % Final Gnode at which DC convergence is accepted (s---r) 
        GnodeFinal
        % Starting algorithm for DC convergence (s---i) 
        DefaultConvStart
        % Save DC solution to dataset if it is available (s---i) 
        SaveDCToDataset
    end
    methods
        function obj = set.SweepVar(obj,val)
            obj = setParameter(obj,'SweepVar',val,0,'string');
        end
        function res = get.SweepVar(obj)
            res = getParameter(obj,'SweepVar');
        end
        function obj = set.SweepPlan(obj,val)
            obj = setParameter(obj,'SweepPlan',val,1,'string');
        end
        function res = get.SweepPlan(obj)
            res = getParameter(obj,'SweepPlan');
        end
        function obj = set.Freq(obj,val)
            obj = setParameter(obj,'Freq',val,0,'real');
        end
        function res = get.Freq(obj)
            res = getParameter(obj,'Freq');
        end
        function obj = set.NestLevel(obj,val)
            obj = setParameter(obj,'NestLevel',val,0,'integer');
        end
        function res = get.NestLevel(obj)
            res = getParameter(obj,'NestLevel');
        end
        function obj = set.StatusLevel(obj,val)
            obj = setParameter(obj,'StatusLevel',val,0,'integer');
        end
        function res = get.StatusLevel(obj)
            res = getParameter(obj,'StatusLevel');
        end
        function obj = set.OutVar(obj,val)
            obj = setParameter(obj,'OutVar',val,0,'string');
        end
        function res = get.OutVar(obj)
            res = getParameter(obj,'OutVar');
        end
        function obj = set.ActivePort(obj,val)
            obj = setParameter(obj,'ActivePort',val,0,'string');
        end
        function res = get.ActivePort(obj)
            res = getParameter(obj,'ActivePort');
        end
        function obj = set.File(obj,val)
            obj = setParameter(obj,'File',val,0,'string');
        end
        function res = get.File(obj)
            res = getParameter(obj,'File');
        end
        function obj = set.CalcY(obj,val)
            obj = setParameter(obj,'CalcY',val,0,'boolean');
        end
        function res = get.CalcY(obj)
            res = getParameter(obj,'CalcY');
        end
        function obj = set.CalcZ(obj,val)
            obj = setParameter(obj,'CalcZ',val,0,'boolean');
        end
        function res = get.CalcZ(obj)
            res = getParameter(obj,'CalcZ');
        end
        function obj = set.CalcS(obj,val)
            obj = setParameter(obj,'CalcS',val,0,'boolean');
        end
        function res = get.CalcS(obj)
            res = getParameter(obj,'CalcS');
        end
        function obj = set.CalcNoise(obj,val)
            obj = setParameter(obj,'CalcNoise',val,0,'boolean');
        end
        function res = get.CalcNoise(obj)
            res = getParameter(obj,'CalcNoise');
        end
        function obj = set.BandwidthForNoise(obj,val)
            obj = setParameter(obj,'BandwidthForNoise',val,0,'real');
        end
        function res = get.BandwidthForNoise(obj)
            res = getParameter(obj,'BandwidthForNoise');
        end
        function obj = set.TempForNoise(obj,val)
            obj = setParameter(obj,'TempForNoise',val,0,'real');
        end
        function res = get.TempForNoise(obj)
            res = getParameter(obj,'TempForNoise');
        end
        function obj = set.CalcGroupDelay(obj,val)
            obj = setParameter(obj,'CalcGroupDelay',val,0,'boolean');
        end
        function res = get.CalcGroupDelay(obj)
            res = getParameter(obj,'CalcGroupDelay');
        end
        function obj = set.UseFiniteDiff(obj,val)
            obj = setParameter(obj,'UseFiniteDiff',val,0,'boolean');
        end
        function res = get.UseFiniteDiff(obj)
            res = getParameter(obj,'UseFiniteDiff');
        end
        function obj = set.FreqConversion(obj,val)
            obj = setParameter(obj,'FreqConversion',val,0,'boolean');
        end
        function res = get.FreqConversion(obj)
            res = getParameter(obj,'FreqConversion');
        end
        function obj = set.FreqConversionPort(obj,val)
            obj = setParameter(obj,'FreqConversionPort',val,0,'integer');
        end
        function res = get.FreqConversionPort(obj)
            res = getParameter(obj,'FreqConversionPort');
        end
        function obj = set.GroupDelayAperture(obj,val)
            obj = setParameter(obj,'GroupDelayAperture',val,0,'real');
        end
        function res = get.GroupDelayAperture(obj)
            res = getParameter(obj,'GroupDelayAperture');
        end
        function obj = set.SortNoise(obj,val)
            obj = setParameter(obj,'SortNoise',val,0,'integer');
        end
        function res = get.SortNoise(obj)
            res = getParameter(obj,'SortNoise');
        end
        function obj = set.NoiseThresh(obj,val)
            obj = setParameter(obj,'NoiseThresh',val,0,'real');
        end
        function res = get.NoiseThresh(obj)
            res = getParameter(obj,'NoiseThresh');
        end
        function obj = set.DevOpPtLevel(obj,val)
            obj = setParameter(obj,'DevOpPtLevel',val,0,'integer');
        end
        function res = get.DevOpPtLevel(obj)
            res = getParameter(obj,'DevOpPtLevel');
        end
        function obj = set.OutputPlan(obj,val)
            obj = setParameter(obj,'OutputPlan',val,0,'string');
        end
        function res = get.OutputPlan(obj)
            res = getParameter(obj,'OutputPlan');
        end
        function obj = set.NoiseInputPort(obj,val)
            obj = setParameter(obj,'NoiseInputPort',val,0,'integer');
        end
        function res = get.NoiseInputPort(obj)
            res = getParameter(obj,'NoiseInputPort');
        end
        function obj = set.NoiseOutputPort(obj,val)
            obj = setParameter(obj,'NoiseOutputPort',val,0,'integer');
        end
        function res = get.NoiseOutputPort(obj)
            res = getParameter(obj,'NoiseOutputPort');
        end
        function obj = set.SaveToDataset(obj,val)
            obj = setParameter(obj,'SaveToDataset',val,0,'integer');
        end
        function res = get.SaveToDataset(obj)
            res = getParameter(obj,'SaveToDataset');
        end
        function obj = set.EnforcePassivity(obj,val)
            obj = setParameter(obj,'EnforcePassivity',val,0,'boolean');
        end
        function res = get.EnforcePassivity(obj)
            res = getParameter(obj,'EnforcePassivity');
        end
        function obj = set.GnodeFinal(obj,val)
            obj = setParameter(obj,'GnodeFinal',val,0,'real');
        end
        function res = get.GnodeFinal(obj)
            res = getParameter(obj,'GnodeFinal');
        end
        function obj = set.DefaultConvStart(obj,val)
            obj = setParameter(obj,'DefaultConvStart',val,0,'integer');
        end
        function res = get.DefaultConvStart(obj)
            res = getParameter(obj,'DefaultConvStart');
        end
        function obj = set.SaveDCToDataset(obj,val)
            obj = setParameter(obj,'SaveDCToDataset',val,0,'integer');
        end
        function res = get.SaveDCToDataset(obj)
            res = getParameter(obj,'SaveDCToDataset');
        end
    end
end
