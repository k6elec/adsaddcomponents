classdef ADS_I_Source < ADScomponent
    % ADS_I_Source matlab representation for the ADS I_Source component
    % Independent Current Source
    % I_Source [:Name] sink source
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % DC current (smorr) Unit: A
        Idc
        % AC current (Complex) (smorc) Unit: A
        Iac
        % Large signal current at given harmonic (smo-c) Unit: A
        I
        % Large signal current at all harmonics (smorc) 
        I_All
        % Small signal current at given upper sideband (smo-c) Unit: A
        I_USB
        % Small signal current at given lower sideband (smo-c) Unit: A
        I_LSB
        % Single fundamental index for this source (sm--i) 
        FundIndex
        % Noise current (smorr) Unit: A
        I_Noise
        % Phase noise as a function of offset frequency (smo-r) Unit: dB
        PhaseNoise
        % Restrict phase noise to the USB/LSB of the carrier signal (s---b) 
        LimitPhaseNoiseToCarrier
        % Transient current (smorr) Unit: A
        I_Tran
        % Actual n'th frequency value (smo-r) Unit: Hz
        Freq
        % Array of large signal harmonics (smo-c) Unit: A
        HarmList
        % Nominal temperature in degrees Celsisus (smorr) Unit: deg C
        Tnom
        % Linear temperature coefficient; per degree Celsius (smorr) Unit: 1/deg C
        TC1
        % Temperature coefficient; per degree Celsius squared (smorr) Unit: 1/(deg C)^2
        TC2
        % Source Type (e.g. V_DC for DC source) genereated by PDE. (s--rs) 
        Type
    end
    methods
        function obj = set.Idc(obj,val)
            obj = setParameter(obj,'Idc',val,0,'real');
        end
        function res = get.Idc(obj)
            res = getParameter(obj,'Idc');
        end
        function obj = set.Iac(obj,val)
            obj = setParameter(obj,'Iac',val,0,'complex');
        end
        function res = get.Iac(obj)
            res = getParameter(obj,'Iac');
        end
        function obj = set.I(obj,val)
            obj = setParameter(obj,'I',val,3,'complex');
        end
        function res = get.I(obj)
            res = getParameter(obj,'I');
        end
        function obj = set.I_All(obj,val)
            obj = setParameter(obj,'I_All',val,0,'complex');
        end
        function res = get.I_All(obj)
            res = getParameter(obj,'I_All');
        end
        function obj = set.I_USB(obj,val)
            obj = setParameter(obj,'I_USB',val,3,'complex');
        end
        function res = get.I_USB(obj)
            res = getParameter(obj,'I_USB');
        end
        function obj = set.I_LSB(obj,val)
            obj = setParameter(obj,'I_LSB',val,3,'complex');
        end
        function res = get.I_LSB(obj)
            res = getParameter(obj,'I_LSB');
        end
        function obj = set.FundIndex(obj,val)
            obj = setParameter(obj,'FundIndex',val,0,'integer');
        end
        function res = get.FundIndex(obj)
            res = getParameter(obj,'FundIndex');
        end
        function obj = set.I_Noise(obj,val)
            obj = setParameter(obj,'I_Noise',val,0,'real');
        end
        function res = get.I_Noise(obj)
            res = getParameter(obj,'I_Noise');
        end
        function obj = set.PhaseNoise(obj,val)
            obj = setParameter(obj,'PhaseNoise',val,0,'real');
        end
        function res = get.PhaseNoise(obj)
            res = getParameter(obj,'PhaseNoise');
        end
        function obj = set.LimitPhaseNoiseToCarrier(obj,val)
            obj = setParameter(obj,'LimitPhaseNoiseToCarrier',val,0,'boolean');
        end
        function res = get.LimitPhaseNoiseToCarrier(obj)
            res = getParameter(obj,'LimitPhaseNoiseToCarrier');
        end
        function obj = set.I_Tran(obj,val)
            obj = setParameter(obj,'I_Tran',val,0,'real');
        end
        function res = get.I_Tran(obj)
            res = getParameter(obj,'I_Tran');
        end
        function obj = set.Freq(obj,val)
            obj = setParameter(obj,'Freq',val,0,'real');
        end
        function res = get.Freq(obj)
            res = getParameter(obj,'Freq');
        end
        function obj = set.HarmList(obj,val)
            obj = setParameter(obj,'HarmList',val,0,'complex');
        end
        function res = get.HarmList(obj)
            res = getParameter(obj,'HarmList');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.TC1(obj,val)
            obj = setParameter(obj,'TC1',val,0,'real');
        end
        function res = get.TC1(obj)
            res = getParameter(obj,'TC1');
        end
        function obj = set.TC2(obj,val)
            obj = setParameter(obj,'TC2',val,0,'real');
        end
        function res = get.TC2(obj)
            res = getParameter(obj,'TC2');
        end
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'string');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
    end
end
