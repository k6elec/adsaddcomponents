classdef ADS_C < ADScomponent
    % ADS_C matlab representation for the ADS C component
    % Linear Two Terminal Capacitor
    % C [:Name] p n
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Capacitance (Smorr) Unit: F
        C
        % Temperature of capacitor in degrees Celsius (smorr) Unit: deg C
        Temp
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Nominal temperature of capacitor in degrees Celsius (smorr) Unit: deg C
        Tnom
        % Temperature coefficient; per degree Celsius (smorr) Unit: 1/deg C
        TC1
        % Temperature coefficient; per degree Celsius squared (smorr) Unit: 1/(deg C)^2
        TC2
        % Breakdown voltage (warning) (s--rr) Unit: V
        wBV
        % Number of capacitors in parallel (smorr) 
        M
        % Secured Devel parameters (s---b) 
        Secured
        % Initial condition for transient analysis (smorr) Unit: V
        InitCond
        % Maximum operating voltage (TSMC SOA warning) (smorr) Unit: V
        Bv_max
    end
    methods
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,0,'real');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.TC1(obj,val)
            obj = setParameter(obj,'TC1',val,0,'real');
        end
        function res = get.TC1(obj)
            res = getParameter(obj,'TC1');
        end
        function obj = set.TC2(obj,val)
            obj = setParameter(obj,'TC2',val,0,'real');
        end
        function res = get.TC2(obj)
            res = getParameter(obj,'TC2');
        end
        function obj = set.wBV(obj,val)
            obj = setParameter(obj,'wBV',val,0,'real');
        end
        function res = get.wBV(obj)
            res = getParameter(obj,'wBV');
        end
        function obj = set.M(obj,val)
            obj = setParameter(obj,'M',val,0,'real');
        end
        function res = get.M(obj)
            res = getParameter(obj,'M');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
        function obj = set.InitCond(obj,val)
            obj = setParameter(obj,'InitCond',val,0,'real');
        end
        function res = get.InitCond(obj)
            res = getParameter(obj,'InitCond');
        end
        function obj = set.Bv_max(obj,val)
            obj = setParameter(obj,'Bv_max',val,0,'real');
        end
        function res = get.Bv_max(obj)
            res = getParameter(obj,'Bv_max');
        end
    end
end
