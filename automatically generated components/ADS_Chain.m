classdef ADS_Chain < ADScomponent
    % ADS_Chain matlab representation for the ADS Chain component
    % User Defined Chain Parameter 2 Port
    % Chain [:Name] p1 n1 p2 n2
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Reverse voltage gain (V1/V2 with I2=0) (smorc) 
        A
        % Reverse transresistance (V1/I2 with V2=0) (smorc) Unit: Ohms
        B
        % Reverse transconductance (I1/V2 with I2=0) (smorc) Unit: S
        C
        % Reverse current gain (I1/I2 with V2=0) (smorc) 
        D
        % Non-causal function impulse response order (sm--i) 
        ImpNoncausalLength
        % Convolution mode (sm--i) 
        ImpMode
        % Maximum Frequency to which device is evaluated (smo-r) Unit: Hz
        ImpMaxFreq
        % Sample spacing in frequency (smo-r) Unit: Hz
        ImpDeltaFreq
        % maximum allowed impulse response order (sm--i) 
        ImpMaxOrder
        % Smoothing Window (sm--i) 
        ImpWindow
        % Relative impulse Response truncation factor (smo-r) 
        ImpRelTol
        % Absolute impulse Response truncation factor (smo-r) 
        ImpAbsTol
    end
    methods
        function obj = set.A(obj,val)
            obj = setParameter(obj,'A',val,0,'complex');
        end
        function res = get.A(obj)
            res = getParameter(obj,'A');
        end
        function obj = set.B(obj,val)
            obj = setParameter(obj,'B',val,0,'complex');
        end
        function res = get.B(obj)
            res = getParameter(obj,'B');
        end
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,0,'complex');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.D(obj,val)
            obj = setParameter(obj,'D',val,0,'complex');
        end
        function res = get.D(obj)
            res = getParameter(obj,'D');
        end
        function obj = set.ImpNoncausalLength(obj,val)
            obj = setParameter(obj,'ImpNoncausalLength',val,0,'integer');
        end
        function res = get.ImpNoncausalLength(obj)
            res = getParameter(obj,'ImpNoncausalLength');
        end
        function obj = set.ImpMode(obj,val)
            obj = setParameter(obj,'ImpMode',val,0,'integer');
        end
        function res = get.ImpMode(obj)
            res = getParameter(obj,'ImpMode');
        end
        function obj = set.ImpMaxFreq(obj,val)
            obj = setParameter(obj,'ImpMaxFreq',val,0,'real');
        end
        function res = get.ImpMaxFreq(obj)
            res = getParameter(obj,'ImpMaxFreq');
        end
        function obj = set.ImpDeltaFreq(obj,val)
            obj = setParameter(obj,'ImpDeltaFreq',val,0,'real');
        end
        function res = get.ImpDeltaFreq(obj)
            res = getParameter(obj,'ImpDeltaFreq');
        end
        function obj = set.ImpMaxOrder(obj,val)
            obj = setParameter(obj,'ImpMaxOrder',val,0,'integer');
        end
        function res = get.ImpMaxOrder(obj)
            res = getParameter(obj,'ImpMaxOrder');
        end
        function obj = set.ImpWindow(obj,val)
            obj = setParameter(obj,'ImpWindow',val,0,'integer');
        end
        function res = get.ImpWindow(obj)
            res = getParameter(obj,'ImpWindow');
        end
        function obj = set.ImpRelTol(obj,val)
            obj = setParameter(obj,'ImpRelTol',val,0,'real');
        end
        function res = get.ImpRelTol(obj)
            res = getParameter(obj,'ImpRelTol');
        end
        function obj = set.ImpAbsTol(obj,val)
            obj = setParameter(obj,'ImpAbsTol',val,0,'real');
        end
        function res = get.ImpAbsTol(obj)
            res = getParameter(obj,'ImpAbsTol');
        end
    end
end
