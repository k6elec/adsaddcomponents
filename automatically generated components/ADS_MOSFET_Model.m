classdef ADS_MOSFET_Model < ADSmodel
    % ADS_MOSFET_Model matlab representation for the ADS MOSFET_Model component
    % Metal Oxide Semiconductor Transistor model
    % model ModelName MOSFET ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Bulk charge effect coefficient (smorr) 
        A0
        % First non-saturation factor (smorr) Unit: V^-1
        A1
        % Second non-saturation factor (smorr) 
        A2
        % Spacing Between Diffusion Regions in POWMOS (smorr) Unit: m
        A_epi
        % Exponential coefficient for finite charge thickness (smorr) Unit: m/V
        Acde
        % Area Calculation Method (sm-ri) 
        Acm
        % AC non-quasi static model selector. (sm-ri) 
        Acnqsmod
        % Area of the drain diffusion (smorr) Unit: m^2
        Ad
        % Flicker (1/f) noise exponent (smorr) 
        Af
        % Gate bias coefficient of Abulk (smorr) Unit: V^-1
        Ags
        % Hot-electron-induced Rout degradation coefficient (smorr) 
        Ai0
        % Sensitivity of Ai to Vbs (smorr) Unit: V^-1
        Aib
        % First parameter of impact ionization current (smorr) Unit: m/V
        Alpha0
        % Substrate current parameter (smorr) Unit: V^-1
        Alpha1
        % Area of the source diffusion (smorr) Unit: m^2
        As
        % Temperature coefficient for saturation velocity (smorr) Unit: m/s
        At
        % Bulk charge effect coefficient for channel width (smorr) Unit: m
        B0
        % Bulk charge effect width offset (smorr) Unit: m
        B1
        % BSIM3 charge model (sm-ri) 
        B3qmod
        % Second parameter of impact ionization current (smorr) Unit: V
        Beta0
        % Exponential parameter of Rout degradation (smorr) Unit: V
        Bi0
        % Sensitivity of Bi to Vbs (smorr) 
        Bib
        % Bin unit selector (sm-ri) 
        Binunit
        % Use ACM algorithms when Acm=12 (sm-ri) 
        Calcacm
        % 0=no cap 1=meyer_ward 2=smooth 3=qmeyer; capacitance model selector of BSIM3 (sm-ri) 
        Capmod
        % Zero bias bulk-drain junction capacitance (smorr) Unit: F
        Cbd
        % Zero bias bulk-source junction capacitance (smorr) Unit: F
        Cbs
        % Drain/Source and channel coupling capacitance (smorr) Unit: F/m^2
        Cdsc
        % Body effect coefficient of Cdsc (smorr) Unit: F/(V*m^2)
        Cdscb
        % Drain bias sensitivity of Cdsc (smorr) Unit: F/(V*m^2)
        Cdscd
        % Fringing field capacitance (smorr) Unit: F/m
        Cf
        % G-B overlap capacitance per meter channel width (smorr) Unit: F/m
        Cgbo
        % Light doped drain-gate region overlap capacitance (smorr) Unit: F/m
        Cgdl
        % G-D overlap capacitance per meter channel  width (smorr) Unit: F/m
        Cgdo
        % Light doped source-gate region overlap capacitance (smorr) Unit: F/m
        Cgsl
        % G-S overlap capacitance per meter channel width (smorr) Unit: F/m
        Cgso
        % Capacitance due to interface charge (smorr) Unit: F/m^2
        Cit
        % Zero-bias bulk junction bottom capacitance (smorr) Unit: F/m^2
        Cj
        % Zero-bias bulk junction sidewall capacitance (smorr) Unit: F/m
        Cjsw
        % S/D (gate side) sidewall junction capacitance (smorr) Unit: F/m
        Cjswg
        % Coefficient for light doped region overlap capacitance (smorr) Unit: F/m
        Ckappa
        % Constant term for the short channel model (smorr) Unit: m
        Clc
        % Exponential term for the short channel model (smorr) 
        Cle
        % Cj linear temperature coefficient (smorr) Unit: 1/deg C
        Cta
        % Cjsw linear temperature coeffient (smorr) Unit: 1/deg C
        Ctp
        % Source drain junction length reduction (smorr) Unit: m
        Dell
        % Width effect on theshold voltage (smorr) 
        Delta
        % Shortening of channel (smorr) Unit: um
        Dl
        % Length offset fitting parameter from C-V (smorr) Unit: m
        Dlc
        % L depend. coeff. of the DIBL effect in Rout (smorr) 
        Drout
        % DIBL coefficient in subthreshold region (smorr) 
        Dsub
        % Delta oxide thickness (smorr) Unit: m
        Dtoxcv
        % First Coefficient of short-channel effect on Vth (smorr) 
        Dvt0
        % First Coefficient of narrow-width effect on Vth (smorr) 
        Dvt0w
        % Second Coefficient of short-channel effect on Vth (smorr) 
        Dvt1
        % Second Coefficient of narrow-width effect on Vth (smorr) Unit: m^-1
        Dvt1w
        % Body-bias Coefficient of short-channel effect on Vth (smorr) Unit: V^-1
        Dvt2
        % Body-bias Coefficient of narrow-width effect on Vth (smorr) Unit: V^-1
        Dvt2w
        % Narrowing of channel (smorr) Unit: um
        Dw
        % Coefficient of Weff's substrate body bias dependence (smorr) Unit: m/V^(1/2)
        Dwb
        % Width offset fitting parameter from C-V (smorr) Unit: m
        Dwc
        % Coefficient of Weff's gate dependence (smorr) Unit: m/V
        Dwg
        % Band gap (smorr) Unit: eV
        Eg
        % Non-quasi-static Elmore constant parameter (smorr) 
        Elm
        % Flicker (1/f) noise parameter (smorr) Unit: V/m
        Em
        % LEVEL3 Static feedback (smorr) 
        Eta
        % Zero-bias drain-induced barrier lowering coefficient (smorr) 
        Eta0
        % Coefficient for forward-bias depletion capacitance (smorr) 
        Fc
        % Flicker (1/f) noise frequency exponent (smorr) 
        Ffe
        % Flicker noise model selector (sm-ri) 
        Flkmod
        % Bulk threshold parameter (smorr) Unit: V^(1/2)
        Gamma
        % Body-effect coefficient near the interface (smorr) Unit: V^(1/2)
        Gamma1
        % Bulk threshold parameter deep in substrate (smorr) Unit: V^(1/2)
        Gamma2
        % Energy gap temperature coefficient alpha (smorr) Unit: V/deg C
        Gap1
        % Energy gap temperature coefficient beta (smorr) Unit: K
        Gap2
        % Drain noise parameter for Nlev=3 (smorr) 
        Gdsnoi
        % Epi Layer Height in POWMOS (smorr) Unit: m
        H_epi
        % Length of heavily doped diffusion (ACM=2,3 only) (smorr) Unit: m
        Hdif
        % 1=LEVEL1 2=LEVEL2 3=LEVEL3 4=BSIM1 5=BSIM2 6=NMOD 			8=BSIM3 9=POWMOS (s--ri) 
        Idsmod
        % Diode limiting current (smorr) Unit: A
        Ijth
        % Explosion current (smorr) Unit: A
        Imax
        % Explosion current (smorr) Unit: A
        Imelt
        % Bulk junction saturation current (smorr) Unit: A
        Is
        % Bulk junction saturation current per sq-meter (smorr) Unit: A/m^2
        Js
        % Sidewall junction reverse saturation current density (smorr) Unit: A/m^2
        Jsw
        % Body effect coefficient (smorr) Unit: V^(1/2)
        K1
        % Drain/source depletion charge sharing coefficient (smorr) 
        K2
        % Narrow width coefficient (smorr) 
        K3
        % Body effect coefficient of K3 (smorr) Unit: V^-1
        K3b
        % Saturation Field factor (smorr) 
        Kappa
        % Field correction factor gate drive dependence (smorr) 
        Kappag
        % Body-bias coefficient of the bulk charge effect (smorr) Unit: V^-1
        Keta
        % Flicker (1/f) noise coefficient (smorr) 
        Kf
        % Transconductance parameter (smorr) Unit: A/V^2
        Kp
        % Temperature coefficient for threshold voltage (smorr) Unit: V
        Kt1
        % Channel length sensitivity of kt1 (smorr) Unit: V*m
        Kt1l
        % Body-bias coeff. of the Vth temperature effect (smorr) 
        Kt2
        % Mobility degradation/enhancement coefficient for LOD (smorr) Unit: m
        Ku0
        % Saturation velocity degradation/enhancement coefficient for LOD (smorr) Unit: m
        Kvsat
        % Threshold degradation/enhancement parameter for LOD (smorr) Unit: V*m
        Kvth0
        % Channel length (smorr) Unit: m
        L
        % Length dependence of a0 (smorr) 
        La0
        % Length dependence of a1 (smorr) 
        La1
        % Length dependence of a2 (smorr) 
        La2
        % Length dependence of Acde (smorr) 
        Lacde
        % Length dependence of ags (smorr) 
        Lags
        % Length dependence of Ai0 (smorr) Unit: um
        Lai0
        % Length dependence of Aib (smorr) Unit: um/V
        Laib
        % Length dependence of alpha0 (smorr) 
        Lalpha0
        % Length dependence of Alpha1 (smorr) Unit: um/V
        Lalpha1
        % Channel length modulation parameter (smorr) Unit: V^-1
        Lambda
        % Length dependence of at (smorr) 
        Lat
        % Length dependence of b0 (smorr) 
        Lb0
        % Length dependence of b1 (smorr) 
        Lb1
        % Length dependence of bet (smorr) 
        Lbeta0
        % Length dependence of Bi0 (smorr) Unit: um*V
        Lbi0
        % Length dependence of Bib (smorr) Unit: um
        Lbib
        % Length dependence of cdsc (smorr) 
        Lcdsc
        % Length dependence of cdscb (smorr) 
        Lcdscb
        % Length dependence of cdscd (smorr) 
        Lcdscd
        % Length dependence of cf (smorr) 
        Lcf
        % Length dependence of cgdl (smorr) 
        Lcgdl
        % Length dependence of cgsl (smorr) 
        Lcgsl
        % Length dependence of cit (smorr) 
        Lcit
        % Length dependence of ckappa (smorr) 
        Lckappa
        % Length dependence of clc (smorr) 
        Lclc
        % Length dependence of cle (smorr) 
        Lcle
        % Lateral Diffusion (smorr) Unit: m
        Ld
        % Length dependence of delta (smorr) 
        Ldelta
        % Length of lightly doped diffusion adjacent to gate (ACM=1,2) (smorr) Unit: m
        Ldif
        % Length dependence of drout (smorr) 
        Ldrout
        % Length dependence of dsub (smorr) 
        Ldsub
        % Length dependence of dvt0 (smorr) 
        Ldvt0
        % Length dependence of dvt0w (smorr) 
        Ldvt0w
        % Length dependence of dvt1 (smorr) 
        Ldvt1
        % Length dependence of dvt1w (smorr) 
        Ldvt1w
        % Length dependence of dvt2 (smorr) 
        Ldvt2
        % Length dependence of dvt2w (smorr) 
        Ldvt2w
        % Length dependence of dwb (smorr) 
        Ldwb
        % Length dependence of dwg (smorr) 
        Ldwg
        % Intrinsic MOS Spacing in POWMOS (smorr) Unit: m
        Leff_dm
        % Length dependence of elm (smorr) 
        Lelm
        % Length dependence of barrier lowering coefficient (smorr) Unit: um
        Leta
        % Length dependence of sens. of Eta to Vds (smorr) Unit: um/V
        Letad
        % Length dependence of gamma1 (smorr) 
        Lgamma1
        % Length dependence of gamma2 (smorr) 
        Lgamma2
        % Gate-to-contact length of drain side (smorr) Unit: m
        Lgcd
        % Gate-to-contact length of source side (smorr) Unit: m
        Lgcs
        % Lint offset for noise calculation (smorr) Unit: m
        Lintnoi
        % Length dependence of body effect coefficient (smorr) Unit: um*V^(1/2)
        Lk1
        % Length dependence of charge sharing coefficient (smorr) Unit: um
        Lk2
        % Length dependence of k3 (smorr) 
        Lk3
        % Length dependence of k3b (smorr) 
        Lk3b
        % Length dependence of keta (smorr) 
        Lketa
        % Length dependence of kt1 (smorr) 
        Lkt1
        % Length dependence of kt1l (smorr) 
        Lkt1l
        % Length dependence of kt2 (smorr) 
        Lkt2
        % Length dependence of Ku0 (smorr) 
        Lku0
        % Length dependence of Kvth0 (smorr) 
        Lkvth0
        % Coefficient of length dependence for length offset (smorr) Unit: m
        Ll
        % Length reduction parameter for CV (smorr) Unit: m
        Llc
        % Power of length dependence of length offset (smorr) 
        Lln
        % Length parameter for U0 LOD effect (smorr) 
        Llodku0
        % Length parameter for Vth LOD effect (smorr) 
        Llodvth
        % Binning maximum length (smorr) Unit: m
        Lmax
        % Binning minimum length (smorr) Unit: m
        Lmin
        % Gate length shrink factor (smorr) 
        Lmlt
        % Length dependence of Moin (smorr) Unit: um*V^(1/2)
        Lmoin
        % Length dependence of Mu20 with the length (smorr) Unit: um
        Lmu20
        % Length dependence of Mu2b (smorr) Unit: um/V
        Lmu2b
        % Length dependence of Mu2g (smorr) Unit: um/V
        Lmu2g
        % Length dependence of Mu30 (smorr) Unit: um*cm^2/(V^2*s)
        Lmu30
        % Length dependence of Mu3b (smorr) Unit: um*cm^2/(V^3*s)
        Lmu3b
        % Length dependence of Mu3g (smorr) Unit: um*cm^2/(V^3*s)
        Lmu3g
        % Length dependence of Mu40 (smorr) Unit: um*cm^2/(V^3*s)
        Lmu40
        % Length dependence of Mu4b (smorr) Unit: um*cm^2/(V^4*s)
        Lmu4b
        % Length dependence of Mu4g (smorr) Unit: um*cm^2/(V^4*s)
        Lmu4g
        % Length dependence of Mus (smorr) Unit: um*cm^2/(V*s)
        Lmus
        % Length dependence of N0 (smorr) Unit: um
        Ln0
        % Length dependence of Nb (smorr) Unit: um/V
        Lnb
        % Length dependence of nch (smorr) 
        Lnch
        % Length dependence of Nd (smorr) Unit: um/V
        Lnd
        % Length dependence of nfactor (smorr) 
        Lnfactor
        % Length dependence of ngate (smorr) 
        Lngate
        % Length dependence of nlx (smorr) 
        Lnlx
        % Length dependence of Noff (smorr) Unit: um
        Lnoff
        % Length dependence of nsub (smorr) 
        Lnsub
        % Eta0 shift modification factor for stress effect (smorr) 
        Lodeta0
        % K2 shift modification factor for stress effect (smorr) 
        Lodk2
        % Length dependence of pclm (smorr) 
        Lpclm
        % Length dependence of pdiblc1 (smorr) 
        Lpdiblc1
        % Length dependence of pdiblc2 (smorr) 
        Lpdiblc2
        % Length dependence of pdiblcb (smorr) 
        Lpdiblcb
        % Length dependence of surface potencial (smorr) Unit: um*V
        Lphi
        % Length dependence of prt (smorr) 
        Lprt
        % Length dependence of prwb (smorr) 
        Lprwb
        % Length dependence of prwg (smorr) 
        Lprwg
        % Length dependence of pscbe1 (smorr) 
        Lpscbe1
        % Length dependence of pscbe2 (smorr) 
        Lpscbe2
        % Length dependence of pvag (smorr) 
        Lpvag
        % Length dependence of rdsw (smorr) 
        Lrdsw
        % Intrinsic MOS Spacing in POWMOS (smorr) Unit: m
        Lt
        % Length dependence of mobility degradation coefficient (smorr) Unit: um/V
        Lu0
        % Length dependence of velocity saturation coefficient (smorr) Unit: um^2/V
        Lu1
        % Length dependence of ua (smorr) 
        Lua
        % Length dependence of ua1 (smorr) 
        Lua1
        % Length dependence of ub (smorr) 
        Lub
        % Length dependence of Ub (smorr) Unit: um/V^2
        Lub0
        % Length dependence of ub1 (smorr) 
        Lub1
        % Length dependence of Ub (smorr) Unit: um/V^3
        Lubb
        % Length dependence of uc (smorr) 
        Luc
        % Length dependence of uc1 (smorr) 
        Luc1
        % Length dependence of ute (smorr) 
        Lute
        % Length dependence of vbm (smorr) 
        Lvbm
        % Length dependence of vbx (smorr) 
        Lvbx
        % Length dependence of flat-band voltage (smorr) Unit: um*V
        Lvfb
        % Length dependence of vfbcv (smorr) Unit: um*V
        Lvfbcv
        % Length dependence of Vghigh (smorr) Unit: um*V
        Lvghigh
        % Length dependence of Vglow (smorr) Unit: um*V
        Lvglow
        % Length dependence of vof0 (smorr) Unit: um*V
        Lvof0
        % Length dependence of vofb (smorr) Unit: um
        Lvofb
        % Length dependence of vofd (smorr) Unit: um
        Lvofd
        % Length dependence of voff (smorr) 
        Lvoff
        % Length dependence of Voffcv (smorr) Unit: um*V
        Lvoffcv
        % Length dependence of vsat (smorr) 
        Lvsat
        % Length dependence of vth0 (smorr) 
        Lvth0
        % Coefficient of width dependence for length offset (smorr) Unit: m
        Lw
        % Length dependence of w0 (smorr) 
        Lw0
        % Length reduction parameter for CV (smorr) Unit: m
        Lwc
        % Coefficient of length and width cross term length offset (smorr) Unit: m
        Lwl
        % Length reduction parameter for CV (smorr) Unit: m
        Lwlc
        % Power of width dependence of length offset (smorr) 
        Lwn
        % Length dependence of wr (smorr) 
        Lwr
        % Length dependence of sens. of Eta to Vbs (smorr) Unit: um/V
        Lx2e
        % Length dependence of Musb (smorr) Unit: um*cm^2/(V^2*s)
        Lx2ms
        % Length dependence of sens. of mobility to Vbs (smorr) Unit: um*cm^2/(V^2*s)
        Lx2mz
        % Length dependence of sens. of Ugs to Vbs (smorr) Unit: um/V^2
        Lx2u0
        % Length dependence of sens. of Uds to Vbs (smorr) Unit: um^2/V^2
        Lx2u1
        % Length dependence of Musd (smorr) Unit: um*cm^2/(V^2*s)
        Lx3ms
        % Length dependence of sens. of Uds to Vds (smorr) Unit: um^2/V^2
        Lx3u1
        % Length dependence of xj (smorr) 
        Lxj
        % Length dependence of xt (smorr) 
        Lxt
        % Minimum drain and source parasitic resistance (smorr) Unit: Ohms
        Minr
        % Bulk junction bottom grading coefficient (smorr) 
        Mj
        % Bulk junction sidewall grading coefficient (smorr) 
        Mjsw
        % S/D (gate side) sidewall junction grading coefficient (smorr) 
        Mjswg
        % Mobility model selector (sm-ri) 
        Mobmod
        % Coefficient for gate-bias dependent surface potential (smorr) Unit: V^(1/2)
        Moin
        % BSIM2 Surface mobility (smorr) Unit: cm^2/(V*s)
        Mu0
        % Empirical parameter in beta0 expression (smorr) 
        Mu20
        % Sensitivity of Mu2 to Vbs (smorr) Unit: V^-1
        Mu2b
        % Sensitivity of Mu2 to Vbs (smorr) Unit: V^-1
        Mu2g
        % Linear empirical parameter in beta0 expression (smorr) Unit: cm^2/(V^2*s)
        Mu30
        % Sensitivity of Mu3 to Vbs (smorr) Unit: cm^2/(V^3*s)
        Mu3b
        % Sensitivity of Mu3 to Vbs (smorr) Unit: cm^2/(V^3*s)
        Mu3g
        % Quadratic empirical parameter in beta0 expression (smorr) Unit: cm^2/(V^3*s)
        Mu40
        % Sensitivity of Mu4 to Vbs (smorr) Unit: cm^2/(V^4*s)
        Mu4b
        % Sensitivity of Mu4 to Vbs (smorr) Unit: cm^2/(V^4*s)
        Mu4g
        % Mobility at zero substrate bias and at Vds=Vdd (smorr) Unit: cm^2/(V*s)
        Mus
        % Bulk P-N emission coefficient (smorr) 
        N
        % Zero-bias subthreshold slope coefficient (smorr) 
        N0
        % NMOS type MOSFET (s--rb) 
        NMOS
        % Sens. of subthreshold slope to substrate bias (smorr) Unit: V^-1
        Nb
        % Channel doping concentration (smorr) Unit: cm^-3
        Nch
        % Sens. of subthreshold slope to substrate bias (smorr) Unit: V^-1
        Nd
        % Total channel charge coefficient (smorr) 
        Neff
        % Epi Layer Doping in POWMOS (smorr) Unit: cm^-3
        Nepi
        % Subthreshold Swing Factor (smorr) 
        Nfactor
        % Fast Surface state density (smorr) Unit: cm^-2
        Nfs
        % Gate doping concentration (smorr) Unit: cm^-3
        Ngate
        % Noise model level (sm-ri) 
        Nlev
        % Lateral non-uniform doping coefficient (smorr) Unit: m
        Nlx
        % C-V turn on/off parameter (smorr) Unit: V
        Noff
        % Noise parameter A (smorr) 
        Noia
        % Noise parameter B (smorr) 
        Noib
        % Noise parameter C (smorr) 
        Noic
        % Noise model selector (sm-ri) 
        Noimod
        % Non-quasi static model selector. (sm-ri) 
        Nqsmod
        % Number of squares of the drain diffusion (smorr) 
        Nrd
        % Number of squares of the source difussion (smorr) 
        Nrs
        % Surface state density (smorr) Unit: cm^-2
        Nss
        % Substrate doping (smorr) Unit: cm^-3
        Nsub
        % PMOS type MOSFET (s--rb) 
        PMOS
        % Cross-term dependence of a0 (smorr) 
        Pa0
        % Cross-term dependence of a1 (smorr) 
        Pa1
        % Cross-term dependence of a2 (smorr) 
        Pa2
        % Cross-term dependence of Acde (smorr) 
        Pacde
        % Cross-term dependence of ags (smorr) 
        Pags
        % Cross-term dependence of alpha0 (smorr) 
        Palpha0
        % Cross-term dependence of Alpha1 (smorr) 
        Palpha1
        % Model parameter checking selector (sm-ri) 
        Paramchk
        % Cross-term dependence of at (smorr) 
        Pat
        % Bulk junction potential (smorr) Unit: V
        Pb
        % Cross-term dependence of b0 (smorr) 
        Pb0
        % Cross-term dependence of b1 (smorr) 
        Pb1
        % Cross-term dependence of beta0 (smorr) 
        Pbeta0
        % Built in potential of source drain junction sidewal (smorr) Unit: V
        Pbsw
        % S/D (gate side) sidewall junction built in potential (smorr) Unit: V
        Pbswg
        % Cross-term dependence of cdsc (smorr) 
        Pcdsc
        % Cross-term dependence of cdscb (smorr) 
        Pcdscb
        % Cross-term dependence of cdscd (smorr) 
        Pcdscd
        % Cross-term dependence of cf (smorr) 
        Pcf
        % Cross-term dependence of cgdl (smorr) 
        Pcgdl
        % Cross-term dependence of cgsl (smorr) 
        Pcgsl
        % Cross-term dependence of cit (smorr) 
        Pcit
        % Cross-term dependence of ckappa (smorr) 
        Pckappa
        % Cross-term dependence of clc (smorr) 
        Pclc
        % Cross-term dependence of cle (smorr) 
        Pcle
        % Channel-Length_Modulation Effect Coefficient (smorr) 
        Pclm
        % Perimeter of the drain junction (smorr) Unit: m
        Pd
        % Cross-term dependence of delta (smorr) 
        Pdelta
        % Drain Induced Barrier Lowering Effect Coefficient 1 (smorr) 
        Pdiblc1
        % Drain Induced Barrier Lowering Effect Coefficient 2 (smorr) 
        Pdiblc2
        % Body-effect on Drain Induced Barrier Lowering (smorr) Unit: V^-1
        Pdiblcb
        % Cross-term dependence of drout (smorr) 
        Pdrout
        % Cross-term dependence of dsub (smorr) 
        Pdsub
        % Cross-term dependence of dvt0 (smorr) 
        Pdvt0
        % Cross-term dependence of dvt0w (smorr) 
        Pdvt0w
        % Cross-term dependence of dvt1 (smorr) 
        Pdvt1
        % Cross-term dependence of dvt1w (smorr) 
        Pdvt1w
        % Cross-term dependence of dvt2 (smorr) 
        Pdvt2
        % Cross-term dependence of dvt2w (smorr) 
        Pdvt2w
        % Cross-term dependence of dwb (smorr) 
        Pdwb
        % Cross-term dependence of dwg (smorr) 
        Pdwg
        % Cross-term dependence of elm (smorr) 
        Pelm
        % Cross-term dependence of eta0 (smorr) 
        Peta0
        % Cross-term dependence of etab (smorr) 
        Petab
        % Cross-term dependence of gamma1 (smorr) 
        Pgamma1
        % Cross-term dependence of gamma2 (smorr) 
        Pgamma2
        % LEVEL1-2-3 Surface Potencial (smorr) Unit: V
        Phi
        % Built_in Potencial Bet. Sub. & Epi Layer in POWMOS (smorr) Unit: V
        Phis_jfet
        % Cross-term dependence of k1 (smorr) 
        Pk1
        % Cross-term dependence of k2 (smorr) 
        Pk2
        % Cross-term dependence of k3 (smorr) 
        Pk3
        % Cross-term dependence of k3b (smorr) 
        Pk3b
        % Cross-term dependence of keta (smorr) 
        Pketa
        % Cross-term dependence of kt1 (smorr) 
        Pkt1
        % Cross-term dependence of kt1l (smorr) 
        Pkt1l
        % Cross-term dependence of kt2 (smorr) 
        Pkt2
        % Cross-term dependence of Ku0 (smorr) 
        Pku0
        % Cross-term dependence of Kvth0 (smorr) 
        Pkvth0
        % Cross-term dependence of Moin (smorr) 
        Pmoin
        % Cross-term dependence of nch (smorr) 
        Pnch
        % Cross-term dependence of nfactor (smorr) 
        Pnfactor
        % Cross-term dependence of ngate (smorr) 
        Pngate
        % Cross-term dependence of nlx (smorr) 
        Pnlx
        % Cross-term dependence of Noff (smorr) 
        Pnoff
        % Cross-term dependence of nsub (smorr) 
        Pnsub
        % Cross-term dependence of pclm (smorr) 
        Ppclm
        % Cross-term dependence of pdiblc1 (smorr) 
        Ppdiblc1
        % Cross-term dependence of pdiblc2 (smorr) 
        Ppdiblc2
        % Cross-term dependence of pdiblcb (smorr) 
        Ppdiblcb
        % Cross-term dependence of prt (smorr) 
        Pprt
        % Cross-term dependence of prwb (smorr) 
        Pprwb
        % Cross-term dependence of prwg (smorr) 
        Pprwg
        % Cross-term dependence of pscbe1 (smorr) 
        Ppscbe1
        % Cross-term dependence of pscbe2 (smorr) 
        Ppscbe2
        % Cross-term dependence of pvag (smorr) 
        Ppvag
        % Cross-term dependence of rdsw (smorr) 
        Prdsw
        % Temperature coefficient of parasitic resistance (smorr) Unit: Ohms*um
        Prt
        % Body effect on parasitic resistance (smorr) Unit: V^(-1/2)
        Prwb
        % Gate bias effect on parasitic resistance (smorr) Unit: V^-1
        Prwg
        % Perimeter of the source junction (smorr) Unit: m
        Ps
        % Substrate_Current_Body_Effect Coefficient. 1 (smorr) Unit: V/m
        Pscbe1
        % Substrate_Current_Body_Effect Coefficient. 2 (smorr) Unit: m/V
        Pscbe2
        % Vj linear temperature coefficient (smorr) Unit: 1/deg C
        Pta
        % Vjsw linear temperature coefficient (smorr) Unit: 1/deg C
        Ptp
        % Cross-term dependence of u0 (smorr) 
        Pu0
        % Cross-term dependence of ua (smorr) 
        Pua
        % Cross-term dependence of ua1 (smorr) 
        Pua1
        % Cross-term dependence of ub (smorr) 
        Pub
        % Cross-term dependence of ub1 (smorr) 
        Pub1
        % Cross-term dependence of uc (smorr) 
        Puc
        % Cross-term dependence of uc1 (smorr) 
        Puc1
        % Cross-term dependence of ute (smorr) 
        Pute
        % Gate voltage dependence of Rout coefficient (smorr) 
        Pvag
        % Cross-term dependence of vbm (smorr) 
        Pvbm
        % Cross-term dependence of vbx (smorr) 
        Pvbx
        % Cross-term dependence of vfb (smorr) 
        Pvfb
        % Cross-term dependence of vfbcv (smorr) 
        Pvfbcv
        % Cross-term dependence of voff (smorr) 
        Pvoff
        % Cross-term dependence of Voffcv (smorr) 
        Pvoffcv
        % Cross-term dependence of vsat (smorr) 
        Pvsat
        % Cross-term dependence of vth0 (smorr) 
        Pvth0
        % Cross-term dependence of w0 (smorr) 
        Pw0
        % Cross-term dependence of wr (smorr) 
        Pwr
        % Cross-term dependence of xj (smorr) 
        Pxj
        % Cross-term dependence of xt (smorr) 
        Pxt
        % Drain resistance (smorr) Unit: Ohms
        Rd
        % Additional drain resistance due to contact resistance (smorr) Unit: Ohms
        Rdc
        % Scalable drain resistance (smorr) Unit: Ohms*m
        Rdd
        % Drain-source shunt resistance (smorr) Unit: Ohms
        Rds
        % Parasitic resistance per unit width (smorr) Unit: Ohms*um
        Rdsw
        % Resistivity of Epi Layer in POWMOS (smorr) Unit: Ohms*m
        Resistivity_epi
        % Gate ohmic resistance (smorr) Unit: Ohms
        Rg
        % Source resistance (smorr) Unit: Ohms
        Rs
        % Additional source resistance due to contact resistance (smorr) Unit: Ohms
        Rsc
        % Drain and source diffusion sheet resistance (smorr) Unit: Ohms
        Rsh
        % Scalable source resistance (smorr) Unit: Ohms*m
        Rss
        % Reference distance between OD edge to poly of one side (smorr) Unit: m
        Sa0
        % Reference distance between OD edge to poly of the other side (smorr) Unit: m
        Sb0
        % Spacing between contacts (smorr) Unit: m
        Sc
        % Secured model parameters (s---b) 
        Secured
        % Eta0 shift factor related to stress effect on Vth (smorr) Unit: m
        Steta0
        % LOD stress effect model selector (s--ri) 
        Stimod
        % K2 shift factor related to stress effect on Vth (smorr) Unit: m
        Stk2
        % Temperature coefficient of Cj (smorr) Unit: K^-1
        Tcj
        % Temperature coefficient of Cjsw (smorr) Unit: K^-1
        Tcjsw
        % Temperature coefficient of Cjswg (smorr) Unit: K^-1
        Tcjswg
        % Mobility modulation (smorr) Unit: V^-1
        Theta
        % Temperature coefficient of Ku0 (smorr) 
        Tku0
        % Temperature equation selector (0/1/2) (sm-ri) 
        Tlev
        % Temperature equation selector for capacitance (0/1/2/3) (sm-ri) 
        Tlevc
        % Parameter measurement temperature (smorr) Unit: deg C
        Tnom
        % LEVEL1-2-3 Oxide thickness (smorr) Unit: m
        Tox
        % Tox value at which parameters are extracted (smorr) Unit: m
        Toxm
        % Temperature coefficient of Pb (smorr) Unit: V/K
        Tpb
        % Temperature coefficient of Pbsw (smorr) Unit: V/K
        Tpbsw
        % Temperature coefficient of Pbswg (smorr) Unit: V/K
        Tpbswg
        % Type of gate material: +1=op -1=same 0=Al (smorr) 
        Tpg
        % Rd linear temperature coefficient (smorr) Unit: 1/deg C
        Trd
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Rs linear temperature coefficient (smorr) Unit: 1/deg C
        Trs
        % Bulk P-N transit time (smorr) Unit: s
        Tt
        % Transverse field mobility degradation coefficient (smorr) Unit: V^-1
        U0
        % Zero-bias velocity saturation coefficient (smorr) Unit: um/V
        U1
        % First order mobility degradation coefficient (smorr) Unit: m/V
        Ua
        % Temperature coefficient of parameter ua (smorr) Unit: m/V
        Ua1
        % Transverse field mobility degradation coefficient (smorr) Unit: m/V
        Ua_dm
        % Second order mobility degradation coefficient (smorr) Unit: (m/V)^2
        Ub
        % Mobility reduction to vertical field at Vbs=0 (smorr) Unit: V^-2
        Ub0
        % Temperature coefficient of parameter ub (smorr) Unit: (m/V)^2
        Ub1
        % Transverse field mobility degradation coefficient (smorr) Unit: (m/V)^2
        Ub_dm
        % Sensitivity of the mobility reduction to Vbs (smorr) Unit: V^-3
        Ubb
        % Body bias mobility degradation coefficient (smorr) Unit: V^-1
        Uc
        % Temperature coefficient of parameter uc (smorr) Unit: V^-1
        Uc1
        % Critical Field for mobility degradation (smorr) Unit: V/cm
        Ucrit
        % Critical Field exponent in mobility degradation (smorr) 
        Uexp
        % LEVEL1-2-3 Surface mobility (smorr) Unit: cm^2/(V*s)
        Uo
        % Depletion Mode MOS Surface mobility (smorr) Unit: cm^2/(V*s)
        Uo_dm
        % Mobility temperature exponent (smorr) 
        Ute
        % Mobility temperature constant (smorr) 
        Uto
        % Measurement bulk bias range (smorr) Unit: V
        Vbb
        % Maximum bulk to drain voltage (TSMC SOA warning) (smorr) Unit: V
        Vbd_max
        % Maximum bulk to substrate voltage (TSMC SOA warning) (smorr) Unit: V
        Vbs_max
        % Vth transition body Voltage (smorr) Unit: V
        Vbx
        % Measurement drain bias range (smorr) Unit: V
        Vdd
        % Maximum drain to source voltage (TSMC SOA warning) (smorr) Unit: V
        Vds_max
        % Minimum voltage (smorr) 
        Vdsmin
        % Parameter for model version (s---r) 
        Version
        % flat-band voltage (smorr) Unit: V
        Vfb
        % flat-band voltage of depletion mode MOS (smorr) Unit: V
        Vfb_dm
        % Flat-band voltage parameter for capmod=0 only (smorr) Unit: V
        Vfbcv
        % Select Vfb for Capmod=0 (sm-ri) 
        Vfbflag
        % Maximum gate to drain voltage (TSMC SOA warning) (smorr) Unit: V
        Vgd_max
        % Measurement gate bias range (smorr) Unit: V
        Vgg
        % Upper bound for the transition region (smorr) Unit: V
        Vghigh
        % Lower bound for the transition region (smorr) Unit: V
        Vglow
        % Maximum gate to source voltage (TSMC SOA warning) (smorr) Unit: V
        Vgs_max
        % Maximum drift velocity of carriers (smorr) Unit: m/s
        Vmax
        % Threshold voltage offset in the subthreshold region (smorr) Unit: V
        Vof0
        % Sensitivity of vof to Vbs (smorr) 
        Vofb
        % Sensitivity of vof to Vds (smorr) 
        Vofd
        % Offset Voltage in subthreshold region (smorr) Unit: V
        Voff
        % C-V lateral shift parameter (smorr) Unit: V
        Voffcv
        % Saturation velocity at Temp=Tnom (smorr) Unit: m/s
        Vsat
        % Zero-bias threshold voltage (smorr) Unit: V
        Vth0
        % Depletion Mode MOSFET Threshold Voltage (smorr) Unit: V
        Vth_dm
        % Zero-bias threshold voltage (smorr) Unit: V
        Vto
        % Channel width (smorr) Unit: m
        W
        % Narrow width parameter (smorr) Unit: m
        W0
        % Width dependence of a0 (smorr) 
        Wa0
        % Width dependence of a1 (smorr) 
        Wa1
        % Width dependence of a2 (smorr) 
        Wa2
        % Width dependence of Acde (smorr) 
        Wacde
        % Width dependence of ags (smorr) 
        Wags
        % Width dependence of Ai0 (smorr) Unit: um
        Wai0
        % Width dependence of Aib (smorr) Unit: um/V
        Waib
        % Width dependence of alpha0 (smorr) 
        Walpha0
        % Width dependence of Alpha1 (smorr) Unit: um/V
        Walpha1
        % Width dependence of at (smorr) 
        Wat
        % Width dependence of b0 (smorr) 
        Wb0
        % Width dependence of b1 (smorr) 
        Wb1
        % Width dependence of bet (smorr) 
        Wbeta0
        % Width dependence of Bi0 (smorr) Unit: um*V
        Wbi0
        % Width dependence of Bib (smorr) Unit: um
        Wbib
        % Width dependence of cdsc (smorr) 
        Wcdsc
        % Width dependence of cdscb (smorr) 
        Wcdscb
        % Width dependence of cdscd (smorr) 
        Wcdscd
        % Width dependence of cf (smorr) 
        Wcf
        % Width dependence of cgdl (smorr) 
        Wcgdl
        % Width dependence of cgsl (smorr) 
        Wcgsl
        % Width dependence of cit (smorr) 
        Wcit
        % Width dependence of ckappa (smorr) 
        Wckappa
        % Width dependence of clc (smorr) 
        Wclc
        % Width dependence of cle (smorr) 
        Wcle
        % Width dependence of delta (smorr) 
        Wdelta
        % Source drain junction default width (smorr) Unit: m
        Wdf
        % Width dependence of drout (smorr) 
        Wdrout
        % Width dependence of dsub (smorr) 
        Wdsub
        % Width dependence of dvt0 (smorr) 
        Wdvt0
        % Width dependence of dvt0w (smorr) 
        Wdvt0w
        % Width dependence of dvt1 (smorr) 
        Wdvt1
        % Width dependence of dvt1w (smorr) 
        Wdvt1w
        % Width dependence of dvt2 (smorr) 
        Wdvt2
        % Width dependence of dvt2w (smorr) 
        Wdvt2w
        % Width dependence of dwb (smorr) 
        Wdwb
        % Width dependence of dwg (smorr) 
        Wdwg
        % Width dependence of elm (smorr) 
        Welm
        % Width dependence of barrier lowering coefficient (smorr) Unit: um
        Weta
        % Width dependence of sens. of Eta to Vds (smorr) Unit: um/V
        Wetad
        % Width dependence of gamma1 (smorr) 
        Wgamma1
        % Width dependence of gamma2 (smorr) 
        Wgamma2
        % Width dependence of body effect coefficient (smorr) Unit: um*V^(1/2)
        Wk1
        % Width dependence of charge sharing coefficient (smorr) Unit: um
        Wk2
        % Width dependence of k3 (smorr) 
        Wk3
        % Width dependence of k3b (smorr) 
        Wk3b
        % Width dependence of keta (smorr) 
        Wketa
        % Width dependence of kt1 (smorr) 
        Wkt1
        % Width dependence of kt1l (smorr) 
        Wkt1l
        % Width dependence of kt2 (smorr) 
        Wkt2
        % Width dependence of Ku0 (smorr) 
        Wku0
        % Width dependence of Kvth0 (smorr) 
        Wkvth0
        % Coefficient of length dependence for width offset (smorr) Unit: m
        Wl
        % Width reduction parameter for CV (smorr) Unit: m
        Wlc
        % Power of length dependence of width offset (smorr) 
        Wln
        % Length parameter for stress effect (smorr) Unit: m
        Wlod
        % Width parameter for U0 LOD effect (smorr) 
        Wlodku0
        % Width parameter for Vth LOD effect (smorr) 
        Wlodvth
        % Binning maximum width (smorr) Unit: m
        Wmax
        % Binning minimum width (smorr) Unit: m
        Wmin
        % Gate width shrink factor (smorr) 
        Wmlt
        % Width dependence of Moin (smorr) Unit: um*V^(1/2)
        Wmoin
        % Width dependence of Mu20 (smorr) Unit: um
        Wmu20
        % Width dependence of Mu2b (smorr) Unit: um/V
        Wmu2b
        % Width dependence of Mu2g (smorr) Unit: um/V
        Wmu2g
        % Width dependence of Mu30 (smorr) Unit: um*cm^2/(V^2*s)
        Wmu30
        % Width dependence of Mu3b (smorr) Unit: um*cm^2/(V^3*s)
        Wmu3b
        % Width dependence of Mu3g (smorr) Unit: um*cm^2/(V^3*s)
        Wmu3g
        % Width dependence of Mu40 (smorr) Unit: um*cm^2/(V^3*s)
        Wmu40
        % Width dependence of Mu4b (smorr) Unit: um*cm^2/(V^4*s)
        Wmu4b
        % Width dependence of Mu4g (smorr) Unit: um*cm^2/(V^4*s)
        Wmu4g
        % Width dependence of Mus (smorr) Unit: um*cm^2/(V*s)
        Wmus
        % Width dependence of N0 (smorr) Unit: um
        Wn0
        % Width dependence of Nb (smorr) Unit: um/V
        Wnb
        % Width dependence of nch (smorr) 
        Wnch
        % Width dependence of Nd (smorr) Unit: um/V
        Wnd
        % Width dependence of nfactor (smorr) 
        Wnfactor
        % Width dependence of ngate (smorr) 
        Wngate
        % Width dependence of nlx (smorr) 
        Wnlx
        % Width dependence of Noff (smorr) Unit: um
        Wnoff
        % Width dependence of nsub (smorr) 
        Wnsub
        % Width dependence of pclm (smorr) 
        Wpclm
        % Width dependence of pdiblc1 (smorr) 
        Wpdiblc1
        % Width dependence of pdiblc2 (smorr) 
        Wpdiblc2
        % Width dependence of pdiblcb (smorr) 
        Wpdiblcb
        % Width dependence of surface potencial (smorr) Unit: um*V
        Wphi
        % Width dependence of prt (smorr) 
        Wprt
        % Width dependence of prwb (smorr) 
        Wprwb
        % Width dependence of prwg (smorr) 
        Wprwg
        % Width dependence of pscbe1 (smorr) 
        Wpscbe1
        % Width dependence of pscbe2 (smorr) 
        Wpscbe2
        % Width dependence of pvag (smorr) 
        Wpvag
        % Width dependence of Rds (smorr) 
        Wr
        % Width dependence of rdsw (smorr) 
        Wrdsw
        % Width dependence of mobility degradation coefficient (smorr) Unit: um/V
        Wu0
        % Width dependence of velocity saturation coefficient (smorr) Unit: um^2/V
        Wu1
        % Width dependence of ua (smorr) 
        Wua
        % Width dependence of ua1 (smorr) 
        Wua1
        % Width dependence of ub (smorr) 
        Wub
        % Width dependence of Ub (smorr) Unit: um/V^2
        Wub0
        % Width dependence of ub1 (smorr) 
        Wub1
        % Width dependence of Ub (smorr) Unit: um/V^3
        Wubb
        % Width dependence of uc (smorr) 
        Wuc
        % Width dependence of uc1 (smorr) 
        Wuc1
        % Width dependence of ute (smorr) 
        Wute
        % Width dependence of vbm (smorr) 
        Wvbm
        % Width dependence of vbx (smorr) 
        Wvbx
        % Width dependence of flat-band voltage (smorr) Unit: um*V
        Wvfb
        % Width dependence of vfbcv (smorr) Unit: um*V
        Wvfbcv
        % Width dependence of Vghigh (smorr) Unit: um*V
        Wvghigh
        % Width dependence of Vglow (smorr) Unit: um*V
        Wvglow
        % Width dependence of vof0 (smorr) Unit: um*V
        Wvof0
        % Width dependence of vofb (smorr) Unit: um
        Wvofb
        % Width dependence of vofd (smorr) Unit: um
        Wvofd
        % Width dependence of voff (smorr) 
        Wvoff
        % Width dependence of Voffcv (smorr) Unit: um*V
        Wvoffcv
        % Width dependence of vsat (smorr) 
        Wvsat
        % Width dependence of vth0 (smorr) 
        Wvth0
        % Coefficient of width dependence for width offset (smorr) Unit: m
        Ww
        % Width dependence of w0 (smorr) 
        Ww0
        % Width reduction parameter for CV (smorr) Unit: m
        Wwc
        % Coefficient of length and width cross term width offset (smorr) Unit: m
        Wwl
        % Width reduction parameter for CV (smorr) Unit: m
        Wwlc
        % Power of width dependence of width offset (smorr) 
        Wwn
        % Width dependence of wr (smorr) 
        Wwr
        % Width dependence of sens. of Eta to Vbs (smorr) Unit: um/V
        Wx2e
        % Width dependence of Musb (smorr) Unit: um*cm^2/(V^2*s)
        Wx2ms
        % Width dependence of sens. of mobility to Vbs (smorr) Unit: um*cm^2/(V^2*s)
        Wx2mz
        % Width dependence of sens. of Ugs to Vbs (smorr) Unit: um/V^2
        Wx2u0
        % Width dependence of sens. of Uds to Vbs (smorr) Unit: um^2/V^2
        Wx2u1
        % Width dependence of Musd (smorr) Unit: um*cm^2/(V^2*s)
        Wx3ms
        % Width dependence of sens. of Uds to Vds (smorr) Unit: um^2/V^2
        Wx3u1
        % Width dependence of xj (smorr) 
        Wxj
        % Width dependence of xt (smorr) 
        Wxt
        % Sens. of barrier lowering cf. to substrate bias (smorr) Unit: V^-1
        X2e
        % Sens. of mobility to substrate bias at Vds=Vdd (smorr) Unit: cm^2/(V^2*s)
        X2ms
        % Sensitivity of mobility to substrate bias (smorr) Unit: cm^2/(V^2*s)
        X2mz
        % Sens. of Ugs to substrate bias (smorr) Unit: V^-2
        X2u0
        % Sens. of velocity saturation cf. to substrate bias (smorr) Unit: um/V^2
        X2u1
        % Sens. of barrier lowering cf. to drain bias (smorr) Unit: V^-1
        X3e
        % Sens. of mobility to drain bias at Vds=Vdd (smorr) Unit: cm^2/(V^2*s)
        X3ms
        % Sens. of velocity saturation cf. to drain bias (smorr) Unit: um/V^2
        X3u1
        % Metallurgical Junction Depth (smorr) Unit: m
        Xj
        % Metallurgical Junction Depth for Power MOS (smorr) Unit: m
        Xj_pow
        % Accounts for masking and etching effects (smorr) Unit: m
        Xl
        % Subthreshold fitting model parameter for NMOD (smorr) 
        Xmu
        % Coefficient of channel charge share (smorr) 
        Xpart
        % Doping depth (smorr) Unit: m
        Xt
        % Junction current temperature exponent (smorr) 
        Xti
        % Accounts for masking and etching effects (smorr) Unit: m
        Xw
        % Mobility modulation with substrate bias parameter (smorr) 
        Zeta
        % Drain-source breakdown voltage (warning) (smorr) Unit: V
        wBvds
        % Gate oxide breakdown voltage (warning) (smorr) Unit: V
        wBvg
        % Substrate junction reverse breakdown voltage (warning) (smorr) Unit: V
        wBvsub
        % Maximum drain-source current (warning) (smorr) Unit: A
        wIdsmax
        % Maximum power dissipation (warning) (smorr) Unit: W
        wPmax
        % Substrate junction forward bias (warning) (smorr) Unit: V
        wVsubfwd
    end
    methods
        function obj = set.A0(obj,val)
            obj = setParameter(obj,'A0',val,0,'real');
        end
        function res = get.A0(obj)
            res = getParameter(obj,'A0');
        end
        function obj = set.A1(obj,val)
            obj = setParameter(obj,'A1',val,0,'real');
        end
        function res = get.A1(obj)
            res = getParameter(obj,'A1');
        end
        function obj = set.A2(obj,val)
            obj = setParameter(obj,'A2',val,0,'real');
        end
        function res = get.A2(obj)
            res = getParameter(obj,'A2');
        end
        function obj = set.A_epi(obj,val)
            obj = setParameter(obj,'A_epi',val,0,'real');
        end
        function res = get.A_epi(obj)
            res = getParameter(obj,'A_epi');
        end
        function obj = set.Acde(obj,val)
            obj = setParameter(obj,'Acde',val,0,'real');
        end
        function res = get.Acde(obj)
            res = getParameter(obj,'Acde');
        end
        function obj = set.Acm(obj,val)
            obj = setParameter(obj,'Acm',val,0,'integer');
        end
        function res = get.Acm(obj)
            res = getParameter(obj,'Acm');
        end
        function obj = set.Acnqsmod(obj,val)
            obj = setParameter(obj,'Acnqsmod',val,0,'integer');
        end
        function res = get.Acnqsmod(obj)
            res = getParameter(obj,'Acnqsmod');
        end
        function obj = set.Ad(obj,val)
            obj = setParameter(obj,'Ad',val,0,'real');
        end
        function res = get.Ad(obj)
            res = getParameter(obj,'Ad');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Ags(obj,val)
            obj = setParameter(obj,'Ags',val,0,'real');
        end
        function res = get.Ags(obj)
            res = getParameter(obj,'Ags');
        end
        function obj = set.Ai0(obj,val)
            obj = setParameter(obj,'Ai0',val,0,'real');
        end
        function res = get.Ai0(obj)
            res = getParameter(obj,'Ai0');
        end
        function obj = set.Aib(obj,val)
            obj = setParameter(obj,'Aib',val,0,'real');
        end
        function res = get.Aib(obj)
            res = getParameter(obj,'Aib');
        end
        function obj = set.Alpha0(obj,val)
            obj = setParameter(obj,'Alpha0',val,0,'real');
        end
        function res = get.Alpha0(obj)
            res = getParameter(obj,'Alpha0');
        end
        function obj = set.Alpha1(obj,val)
            obj = setParameter(obj,'Alpha1',val,0,'real');
        end
        function res = get.Alpha1(obj)
            res = getParameter(obj,'Alpha1');
        end
        function obj = set.As(obj,val)
            obj = setParameter(obj,'As',val,0,'real');
        end
        function res = get.As(obj)
            res = getParameter(obj,'As');
        end
        function obj = set.At(obj,val)
            obj = setParameter(obj,'At',val,0,'real');
        end
        function res = get.At(obj)
            res = getParameter(obj,'At');
        end
        function obj = set.B0(obj,val)
            obj = setParameter(obj,'B0',val,0,'real');
        end
        function res = get.B0(obj)
            res = getParameter(obj,'B0');
        end
        function obj = set.B1(obj,val)
            obj = setParameter(obj,'B1',val,0,'real');
        end
        function res = get.B1(obj)
            res = getParameter(obj,'B1');
        end
        function obj = set.B3qmod(obj,val)
            obj = setParameter(obj,'B3qmod',val,0,'integer');
        end
        function res = get.B3qmod(obj)
            res = getParameter(obj,'B3qmod');
        end
        function obj = set.Beta0(obj,val)
            obj = setParameter(obj,'Beta0',val,0,'real');
        end
        function res = get.Beta0(obj)
            res = getParameter(obj,'Beta0');
        end
        function obj = set.Bi0(obj,val)
            obj = setParameter(obj,'Bi0',val,0,'real');
        end
        function res = get.Bi0(obj)
            res = getParameter(obj,'Bi0');
        end
        function obj = set.Bib(obj,val)
            obj = setParameter(obj,'Bib',val,0,'real');
        end
        function res = get.Bib(obj)
            res = getParameter(obj,'Bib');
        end
        function obj = set.Binunit(obj,val)
            obj = setParameter(obj,'Binunit',val,0,'integer');
        end
        function res = get.Binunit(obj)
            res = getParameter(obj,'Binunit');
        end
        function obj = set.Calcacm(obj,val)
            obj = setParameter(obj,'Calcacm',val,0,'integer');
        end
        function res = get.Calcacm(obj)
            res = getParameter(obj,'Calcacm');
        end
        function obj = set.Capmod(obj,val)
            obj = setParameter(obj,'Capmod',val,0,'integer');
        end
        function res = get.Capmod(obj)
            res = getParameter(obj,'Capmod');
        end
        function obj = set.Cbd(obj,val)
            obj = setParameter(obj,'Cbd',val,0,'real');
        end
        function res = get.Cbd(obj)
            res = getParameter(obj,'Cbd');
        end
        function obj = set.Cbs(obj,val)
            obj = setParameter(obj,'Cbs',val,0,'real');
        end
        function res = get.Cbs(obj)
            res = getParameter(obj,'Cbs');
        end
        function obj = set.Cdsc(obj,val)
            obj = setParameter(obj,'Cdsc',val,0,'real');
        end
        function res = get.Cdsc(obj)
            res = getParameter(obj,'Cdsc');
        end
        function obj = set.Cdscb(obj,val)
            obj = setParameter(obj,'Cdscb',val,0,'real');
        end
        function res = get.Cdscb(obj)
            res = getParameter(obj,'Cdscb');
        end
        function obj = set.Cdscd(obj,val)
            obj = setParameter(obj,'Cdscd',val,0,'real');
        end
        function res = get.Cdscd(obj)
            res = getParameter(obj,'Cdscd');
        end
        function obj = set.Cf(obj,val)
            obj = setParameter(obj,'Cf',val,0,'real');
        end
        function res = get.Cf(obj)
            res = getParameter(obj,'Cf');
        end
        function obj = set.Cgbo(obj,val)
            obj = setParameter(obj,'Cgbo',val,0,'real');
        end
        function res = get.Cgbo(obj)
            res = getParameter(obj,'Cgbo');
        end
        function obj = set.Cgdl(obj,val)
            obj = setParameter(obj,'Cgdl',val,0,'real');
        end
        function res = get.Cgdl(obj)
            res = getParameter(obj,'Cgdl');
        end
        function obj = set.Cgdo(obj,val)
            obj = setParameter(obj,'Cgdo',val,0,'real');
        end
        function res = get.Cgdo(obj)
            res = getParameter(obj,'Cgdo');
        end
        function obj = set.Cgsl(obj,val)
            obj = setParameter(obj,'Cgsl',val,0,'real');
        end
        function res = get.Cgsl(obj)
            res = getParameter(obj,'Cgsl');
        end
        function obj = set.Cgso(obj,val)
            obj = setParameter(obj,'Cgso',val,0,'real');
        end
        function res = get.Cgso(obj)
            res = getParameter(obj,'Cgso');
        end
        function obj = set.Cit(obj,val)
            obj = setParameter(obj,'Cit',val,0,'real');
        end
        function res = get.Cit(obj)
            res = getParameter(obj,'Cit');
        end
        function obj = set.Cj(obj,val)
            obj = setParameter(obj,'Cj',val,0,'real');
        end
        function res = get.Cj(obj)
            res = getParameter(obj,'Cj');
        end
        function obj = set.Cjsw(obj,val)
            obj = setParameter(obj,'Cjsw',val,0,'real');
        end
        function res = get.Cjsw(obj)
            res = getParameter(obj,'Cjsw');
        end
        function obj = set.Cjswg(obj,val)
            obj = setParameter(obj,'Cjswg',val,0,'real');
        end
        function res = get.Cjswg(obj)
            res = getParameter(obj,'Cjswg');
        end
        function obj = set.Ckappa(obj,val)
            obj = setParameter(obj,'Ckappa',val,0,'real');
        end
        function res = get.Ckappa(obj)
            res = getParameter(obj,'Ckappa');
        end
        function obj = set.Clc(obj,val)
            obj = setParameter(obj,'Clc',val,0,'real');
        end
        function res = get.Clc(obj)
            res = getParameter(obj,'Clc');
        end
        function obj = set.Cle(obj,val)
            obj = setParameter(obj,'Cle',val,0,'real');
        end
        function res = get.Cle(obj)
            res = getParameter(obj,'Cle');
        end
        function obj = set.Cta(obj,val)
            obj = setParameter(obj,'Cta',val,0,'real');
        end
        function res = get.Cta(obj)
            res = getParameter(obj,'Cta');
        end
        function obj = set.Ctp(obj,val)
            obj = setParameter(obj,'Ctp',val,0,'real');
        end
        function res = get.Ctp(obj)
            res = getParameter(obj,'Ctp');
        end
        function obj = set.Dell(obj,val)
            obj = setParameter(obj,'Dell',val,0,'real');
        end
        function res = get.Dell(obj)
            res = getParameter(obj,'Dell');
        end
        function obj = set.Delta(obj,val)
            obj = setParameter(obj,'Delta',val,0,'real');
        end
        function res = get.Delta(obj)
            res = getParameter(obj,'Delta');
        end
        function obj = set.Dl(obj,val)
            obj = setParameter(obj,'Dl',val,0,'real');
        end
        function res = get.Dl(obj)
            res = getParameter(obj,'Dl');
        end
        function obj = set.Dlc(obj,val)
            obj = setParameter(obj,'Dlc',val,0,'real');
        end
        function res = get.Dlc(obj)
            res = getParameter(obj,'Dlc');
        end
        function obj = set.Drout(obj,val)
            obj = setParameter(obj,'Drout',val,0,'real');
        end
        function res = get.Drout(obj)
            res = getParameter(obj,'Drout');
        end
        function obj = set.Dsub(obj,val)
            obj = setParameter(obj,'Dsub',val,0,'real');
        end
        function res = get.Dsub(obj)
            res = getParameter(obj,'Dsub');
        end
        function obj = set.Dtoxcv(obj,val)
            obj = setParameter(obj,'Dtoxcv',val,0,'real');
        end
        function res = get.Dtoxcv(obj)
            res = getParameter(obj,'Dtoxcv');
        end
        function obj = set.Dvt0(obj,val)
            obj = setParameter(obj,'Dvt0',val,0,'real');
        end
        function res = get.Dvt0(obj)
            res = getParameter(obj,'Dvt0');
        end
        function obj = set.Dvt0w(obj,val)
            obj = setParameter(obj,'Dvt0w',val,0,'real');
        end
        function res = get.Dvt0w(obj)
            res = getParameter(obj,'Dvt0w');
        end
        function obj = set.Dvt1(obj,val)
            obj = setParameter(obj,'Dvt1',val,0,'real');
        end
        function res = get.Dvt1(obj)
            res = getParameter(obj,'Dvt1');
        end
        function obj = set.Dvt1w(obj,val)
            obj = setParameter(obj,'Dvt1w',val,0,'real');
        end
        function res = get.Dvt1w(obj)
            res = getParameter(obj,'Dvt1w');
        end
        function obj = set.Dvt2(obj,val)
            obj = setParameter(obj,'Dvt2',val,0,'real');
        end
        function res = get.Dvt2(obj)
            res = getParameter(obj,'Dvt2');
        end
        function obj = set.Dvt2w(obj,val)
            obj = setParameter(obj,'Dvt2w',val,0,'real');
        end
        function res = get.Dvt2w(obj)
            res = getParameter(obj,'Dvt2w');
        end
        function obj = set.Dw(obj,val)
            obj = setParameter(obj,'Dw',val,0,'real');
        end
        function res = get.Dw(obj)
            res = getParameter(obj,'Dw');
        end
        function obj = set.Dwb(obj,val)
            obj = setParameter(obj,'Dwb',val,0,'real');
        end
        function res = get.Dwb(obj)
            res = getParameter(obj,'Dwb');
        end
        function obj = set.Dwc(obj,val)
            obj = setParameter(obj,'Dwc',val,0,'real');
        end
        function res = get.Dwc(obj)
            res = getParameter(obj,'Dwc');
        end
        function obj = set.Dwg(obj,val)
            obj = setParameter(obj,'Dwg',val,0,'real');
        end
        function res = get.Dwg(obj)
            res = getParameter(obj,'Dwg');
        end
        function obj = set.Eg(obj,val)
            obj = setParameter(obj,'Eg',val,0,'real');
        end
        function res = get.Eg(obj)
            res = getParameter(obj,'Eg');
        end
        function obj = set.Elm(obj,val)
            obj = setParameter(obj,'Elm',val,0,'real');
        end
        function res = get.Elm(obj)
            res = getParameter(obj,'Elm');
        end
        function obj = set.Em(obj,val)
            obj = setParameter(obj,'Em',val,0,'real');
        end
        function res = get.Em(obj)
            res = getParameter(obj,'Em');
        end
        function obj = set.Eta(obj,val)
            obj = setParameter(obj,'Eta',val,0,'real');
        end
        function res = get.Eta(obj)
            res = getParameter(obj,'Eta');
        end
        function obj = set.Eta0(obj,val)
            obj = setParameter(obj,'Eta0',val,0,'real');
        end
        function res = get.Eta0(obj)
            res = getParameter(obj,'Eta0');
        end
        function obj = set.Fc(obj,val)
            obj = setParameter(obj,'Fc',val,0,'real');
        end
        function res = get.Fc(obj)
            res = getParameter(obj,'Fc');
        end
        function obj = set.Ffe(obj,val)
            obj = setParameter(obj,'Ffe',val,0,'real');
        end
        function res = get.Ffe(obj)
            res = getParameter(obj,'Ffe');
        end
        function obj = set.Flkmod(obj,val)
            obj = setParameter(obj,'Flkmod',val,0,'integer');
        end
        function res = get.Flkmod(obj)
            res = getParameter(obj,'Flkmod');
        end
        function obj = set.Gamma(obj,val)
            obj = setParameter(obj,'Gamma',val,0,'real');
        end
        function res = get.Gamma(obj)
            res = getParameter(obj,'Gamma');
        end
        function obj = set.Gamma1(obj,val)
            obj = setParameter(obj,'Gamma1',val,0,'real');
        end
        function res = get.Gamma1(obj)
            res = getParameter(obj,'Gamma1');
        end
        function obj = set.Gamma2(obj,val)
            obj = setParameter(obj,'Gamma2',val,0,'real');
        end
        function res = get.Gamma2(obj)
            res = getParameter(obj,'Gamma2');
        end
        function obj = set.Gap1(obj,val)
            obj = setParameter(obj,'Gap1',val,0,'real');
        end
        function res = get.Gap1(obj)
            res = getParameter(obj,'Gap1');
        end
        function obj = set.Gap2(obj,val)
            obj = setParameter(obj,'Gap2',val,0,'real');
        end
        function res = get.Gap2(obj)
            res = getParameter(obj,'Gap2');
        end
        function obj = set.Gdsnoi(obj,val)
            obj = setParameter(obj,'Gdsnoi',val,0,'real');
        end
        function res = get.Gdsnoi(obj)
            res = getParameter(obj,'Gdsnoi');
        end
        function obj = set.H_epi(obj,val)
            obj = setParameter(obj,'H_epi',val,0,'real');
        end
        function res = get.H_epi(obj)
            res = getParameter(obj,'H_epi');
        end
        function obj = set.Hdif(obj,val)
            obj = setParameter(obj,'Hdif',val,0,'real');
        end
        function res = get.Hdif(obj)
            res = getParameter(obj,'Hdif');
        end
        function obj = set.Idsmod(obj,val)
            obj = setParameter(obj,'Idsmod',val,0,'integer');
        end
        function res = get.Idsmod(obj)
            res = getParameter(obj,'Idsmod');
        end
        function obj = set.Ijth(obj,val)
            obj = setParameter(obj,'Ijth',val,0,'real');
        end
        function res = get.Ijth(obj)
            res = getParameter(obj,'Ijth');
        end
        function obj = set.Imax(obj,val)
            obj = setParameter(obj,'Imax',val,0,'real');
        end
        function res = get.Imax(obj)
            res = getParameter(obj,'Imax');
        end
        function obj = set.Imelt(obj,val)
            obj = setParameter(obj,'Imelt',val,0,'real');
        end
        function res = get.Imelt(obj)
            res = getParameter(obj,'Imelt');
        end
        function obj = set.Is(obj,val)
            obj = setParameter(obj,'Is',val,0,'real');
        end
        function res = get.Is(obj)
            res = getParameter(obj,'Is');
        end
        function obj = set.Js(obj,val)
            obj = setParameter(obj,'Js',val,0,'real');
        end
        function res = get.Js(obj)
            res = getParameter(obj,'Js');
        end
        function obj = set.Jsw(obj,val)
            obj = setParameter(obj,'Jsw',val,0,'real');
        end
        function res = get.Jsw(obj)
            res = getParameter(obj,'Jsw');
        end
        function obj = set.K1(obj,val)
            obj = setParameter(obj,'K1',val,0,'real');
        end
        function res = get.K1(obj)
            res = getParameter(obj,'K1');
        end
        function obj = set.K2(obj,val)
            obj = setParameter(obj,'K2',val,0,'real');
        end
        function res = get.K2(obj)
            res = getParameter(obj,'K2');
        end
        function obj = set.K3(obj,val)
            obj = setParameter(obj,'K3',val,0,'real');
        end
        function res = get.K3(obj)
            res = getParameter(obj,'K3');
        end
        function obj = set.K3b(obj,val)
            obj = setParameter(obj,'K3b',val,0,'real');
        end
        function res = get.K3b(obj)
            res = getParameter(obj,'K3b');
        end
        function obj = set.Kappa(obj,val)
            obj = setParameter(obj,'Kappa',val,0,'real');
        end
        function res = get.Kappa(obj)
            res = getParameter(obj,'Kappa');
        end
        function obj = set.Kappag(obj,val)
            obj = setParameter(obj,'Kappag',val,0,'real');
        end
        function res = get.Kappag(obj)
            res = getParameter(obj,'Kappag');
        end
        function obj = set.Keta(obj,val)
            obj = setParameter(obj,'Keta',val,0,'real');
        end
        function res = get.Keta(obj)
            res = getParameter(obj,'Keta');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Kp(obj,val)
            obj = setParameter(obj,'Kp',val,0,'real');
        end
        function res = get.Kp(obj)
            res = getParameter(obj,'Kp');
        end
        function obj = set.Kt1(obj,val)
            obj = setParameter(obj,'Kt1',val,0,'real');
        end
        function res = get.Kt1(obj)
            res = getParameter(obj,'Kt1');
        end
        function obj = set.Kt1l(obj,val)
            obj = setParameter(obj,'Kt1l',val,0,'real');
        end
        function res = get.Kt1l(obj)
            res = getParameter(obj,'Kt1l');
        end
        function obj = set.Kt2(obj,val)
            obj = setParameter(obj,'Kt2',val,0,'real');
        end
        function res = get.Kt2(obj)
            res = getParameter(obj,'Kt2');
        end
        function obj = set.Ku0(obj,val)
            obj = setParameter(obj,'Ku0',val,0,'real');
        end
        function res = get.Ku0(obj)
            res = getParameter(obj,'Ku0');
        end
        function obj = set.Kvsat(obj,val)
            obj = setParameter(obj,'Kvsat',val,0,'real');
        end
        function res = get.Kvsat(obj)
            res = getParameter(obj,'Kvsat');
        end
        function obj = set.Kvth0(obj,val)
            obj = setParameter(obj,'Kvth0',val,0,'real');
        end
        function res = get.Kvth0(obj)
            res = getParameter(obj,'Kvth0');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.La0(obj,val)
            obj = setParameter(obj,'La0',val,0,'real');
        end
        function res = get.La0(obj)
            res = getParameter(obj,'La0');
        end
        function obj = set.La1(obj,val)
            obj = setParameter(obj,'La1',val,0,'real');
        end
        function res = get.La1(obj)
            res = getParameter(obj,'La1');
        end
        function obj = set.La2(obj,val)
            obj = setParameter(obj,'La2',val,0,'real');
        end
        function res = get.La2(obj)
            res = getParameter(obj,'La2');
        end
        function obj = set.Lacde(obj,val)
            obj = setParameter(obj,'Lacde',val,0,'real');
        end
        function res = get.Lacde(obj)
            res = getParameter(obj,'Lacde');
        end
        function obj = set.Lags(obj,val)
            obj = setParameter(obj,'Lags',val,0,'real');
        end
        function res = get.Lags(obj)
            res = getParameter(obj,'Lags');
        end
        function obj = set.Lai0(obj,val)
            obj = setParameter(obj,'Lai0',val,0,'real');
        end
        function res = get.Lai0(obj)
            res = getParameter(obj,'Lai0');
        end
        function obj = set.Laib(obj,val)
            obj = setParameter(obj,'Laib',val,0,'real');
        end
        function res = get.Laib(obj)
            res = getParameter(obj,'Laib');
        end
        function obj = set.Lalpha0(obj,val)
            obj = setParameter(obj,'Lalpha0',val,0,'real');
        end
        function res = get.Lalpha0(obj)
            res = getParameter(obj,'Lalpha0');
        end
        function obj = set.Lalpha1(obj,val)
            obj = setParameter(obj,'Lalpha1',val,0,'real');
        end
        function res = get.Lalpha1(obj)
            res = getParameter(obj,'Lalpha1');
        end
        function obj = set.Lambda(obj,val)
            obj = setParameter(obj,'Lambda',val,0,'real');
        end
        function res = get.Lambda(obj)
            res = getParameter(obj,'Lambda');
        end
        function obj = set.Lat(obj,val)
            obj = setParameter(obj,'Lat',val,0,'real');
        end
        function res = get.Lat(obj)
            res = getParameter(obj,'Lat');
        end
        function obj = set.Lb0(obj,val)
            obj = setParameter(obj,'Lb0',val,0,'real');
        end
        function res = get.Lb0(obj)
            res = getParameter(obj,'Lb0');
        end
        function obj = set.Lb1(obj,val)
            obj = setParameter(obj,'Lb1',val,0,'real');
        end
        function res = get.Lb1(obj)
            res = getParameter(obj,'Lb1');
        end
        function obj = set.Lbeta0(obj,val)
            obj = setParameter(obj,'Lbeta0',val,0,'real');
        end
        function res = get.Lbeta0(obj)
            res = getParameter(obj,'Lbeta0');
        end
        function obj = set.Lbi0(obj,val)
            obj = setParameter(obj,'Lbi0',val,0,'real');
        end
        function res = get.Lbi0(obj)
            res = getParameter(obj,'Lbi0');
        end
        function obj = set.Lbib(obj,val)
            obj = setParameter(obj,'Lbib',val,0,'real');
        end
        function res = get.Lbib(obj)
            res = getParameter(obj,'Lbib');
        end
        function obj = set.Lcdsc(obj,val)
            obj = setParameter(obj,'Lcdsc',val,0,'real');
        end
        function res = get.Lcdsc(obj)
            res = getParameter(obj,'Lcdsc');
        end
        function obj = set.Lcdscb(obj,val)
            obj = setParameter(obj,'Lcdscb',val,0,'real');
        end
        function res = get.Lcdscb(obj)
            res = getParameter(obj,'Lcdscb');
        end
        function obj = set.Lcdscd(obj,val)
            obj = setParameter(obj,'Lcdscd',val,0,'real');
        end
        function res = get.Lcdscd(obj)
            res = getParameter(obj,'Lcdscd');
        end
        function obj = set.Lcf(obj,val)
            obj = setParameter(obj,'Lcf',val,0,'real');
        end
        function res = get.Lcf(obj)
            res = getParameter(obj,'Lcf');
        end
        function obj = set.Lcgdl(obj,val)
            obj = setParameter(obj,'Lcgdl',val,0,'real');
        end
        function res = get.Lcgdl(obj)
            res = getParameter(obj,'Lcgdl');
        end
        function obj = set.Lcgsl(obj,val)
            obj = setParameter(obj,'Lcgsl',val,0,'real');
        end
        function res = get.Lcgsl(obj)
            res = getParameter(obj,'Lcgsl');
        end
        function obj = set.Lcit(obj,val)
            obj = setParameter(obj,'Lcit',val,0,'real');
        end
        function res = get.Lcit(obj)
            res = getParameter(obj,'Lcit');
        end
        function obj = set.Lckappa(obj,val)
            obj = setParameter(obj,'Lckappa',val,0,'real');
        end
        function res = get.Lckappa(obj)
            res = getParameter(obj,'Lckappa');
        end
        function obj = set.Lclc(obj,val)
            obj = setParameter(obj,'Lclc',val,0,'real');
        end
        function res = get.Lclc(obj)
            res = getParameter(obj,'Lclc');
        end
        function obj = set.Lcle(obj,val)
            obj = setParameter(obj,'Lcle',val,0,'real');
        end
        function res = get.Lcle(obj)
            res = getParameter(obj,'Lcle');
        end
        function obj = set.Ld(obj,val)
            obj = setParameter(obj,'Ld',val,0,'real');
        end
        function res = get.Ld(obj)
            res = getParameter(obj,'Ld');
        end
        function obj = set.Ldelta(obj,val)
            obj = setParameter(obj,'Ldelta',val,0,'real');
        end
        function res = get.Ldelta(obj)
            res = getParameter(obj,'Ldelta');
        end
        function obj = set.Ldif(obj,val)
            obj = setParameter(obj,'Ldif',val,0,'real');
        end
        function res = get.Ldif(obj)
            res = getParameter(obj,'Ldif');
        end
        function obj = set.Ldrout(obj,val)
            obj = setParameter(obj,'Ldrout',val,0,'real');
        end
        function res = get.Ldrout(obj)
            res = getParameter(obj,'Ldrout');
        end
        function obj = set.Ldsub(obj,val)
            obj = setParameter(obj,'Ldsub',val,0,'real');
        end
        function res = get.Ldsub(obj)
            res = getParameter(obj,'Ldsub');
        end
        function obj = set.Ldvt0(obj,val)
            obj = setParameter(obj,'Ldvt0',val,0,'real');
        end
        function res = get.Ldvt0(obj)
            res = getParameter(obj,'Ldvt0');
        end
        function obj = set.Ldvt0w(obj,val)
            obj = setParameter(obj,'Ldvt0w',val,0,'real');
        end
        function res = get.Ldvt0w(obj)
            res = getParameter(obj,'Ldvt0w');
        end
        function obj = set.Ldvt1(obj,val)
            obj = setParameter(obj,'Ldvt1',val,0,'real');
        end
        function res = get.Ldvt1(obj)
            res = getParameter(obj,'Ldvt1');
        end
        function obj = set.Ldvt1w(obj,val)
            obj = setParameter(obj,'Ldvt1w',val,0,'real');
        end
        function res = get.Ldvt1w(obj)
            res = getParameter(obj,'Ldvt1w');
        end
        function obj = set.Ldvt2(obj,val)
            obj = setParameter(obj,'Ldvt2',val,0,'real');
        end
        function res = get.Ldvt2(obj)
            res = getParameter(obj,'Ldvt2');
        end
        function obj = set.Ldvt2w(obj,val)
            obj = setParameter(obj,'Ldvt2w',val,0,'real');
        end
        function res = get.Ldvt2w(obj)
            res = getParameter(obj,'Ldvt2w');
        end
        function obj = set.Ldwb(obj,val)
            obj = setParameter(obj,'Ldwb',val,0,'real');
        end
        function res = get.Ldwb(obj)
            res = getParameter(obj,'Ldwb');
        end
        function obj = set.Ldwg(obj,val)
            obj = setParameter(obj,'Ldwg',val,0,'real');
        end
        function res = get.Ldwg(obj)
            res = getParameter(obj,'Ldwg');
        end
        function obj = set.Leff_dm(obj,val)
            obj = setParameter(obj,'Leff_dm',val,0,'real');
        end
        function res = get.Leff_dm(obj)
            res = getParameter(obj,'Leff_dm');
        end
        function obj = set.Lelm(obj,val)
            obj = setParameter(obj,'Lelm',val,0,'real');
        end
        function res = get.Lelm(obj)
            res = getParameter(obj,'Lelm');
        end
        function obj = set.Leta(obj,val)
            obj = setParameter(obj,'Leta',val,0,'real');
        end
        function res = get.Leta(obj)
            res = getParameter(obj,'Leta');
        end
        function obj = set.Letad(obj,val)
            obj = setParameter(obj,'Letad',val,0,'real');
        end
        function res = get.Letad(obj)
            res = getParameter(obj,'Letad');
        end
        function obj = set.Lgamma1(obj,val)
            obj = setParameter(obj,'Lgamma1',val,0,'real');
        end
        function res = get.Lgamma1(obj)
            res = getParameter(obj,'Lgamma1');
        end
        function obj = set.Lgamma2(obj,val)
            obj = setParameter(obj,'Lgamma2',val,0,'real');
        end
        function res = get.Lgamma2(obj)
            res = getParameter(obj,'Lgamma2');
        end
        function obj = set.Lgcd(obj,val)
            obj = setParameter(obj,'Lgcd',val,0,'real');
        end
        function res = get.Lgcd(obj)
            res = getParameter(obj,'Lgcd');
        end
        function obj = set.Lgcs(obj,val)
            obj = setParameter(obj,'Lgcs',val,0,'real');
        end
        function res = get.Lgcs(obj)
            res = getParameter(obj,'Lgcs');
        end
        function obj = set.Lintnoi(obj,val)
            obj = setParameter(obj,'Lintnoi',val,0,'real');
        end
        function res = get.Lintnoi(obj)
            res = getParameter(obj,'Lintnoi');
        end
        function obj = set.Lk1(obj,val)
            obj = setParameter(obj,'Lk1',val,0,'real');
        end
        function res = get.Lk1(obj)
            res = getParameter(obj,'Lk1');
        end
        function obj = set.Lk2(obj,val)
            obj = setParameter(obj,'Lk2',val,0,'real');
        end
        function res = get.Lk2(obj)
            res = getParameter(obj,'Lk2');
        end
        function obj = set.Lk3(obj,val)
            obj = setParameter(obj,'Lk3',val,0,'real');
        end
        function res = get.Lk3(obj)
            res = getParameter(obj,'Lk3');
        end
        function obj = set.Lk3b(obj,val)
            obj = setParameter(obj,'Lk3b',val,0,'real');
        end
        function res = get.Lk3b(obj)
            res = getParameter(obj,'Lk3b');
        end
        function obj = set.Lketa(obj,val)
            obj = setParameter(obj,'Lketa',val,0,'real');
        end
        function res = get.Lketa(obj)
            res = getParameter(obj,'Lketa');
        end
        function obj = set.Lkt1(obj,val)
            obj = setParameter(obj,'Lkt1',val,0,'real');
        end
        function res = get.Lkt1(obj)
            res = getParameter(obj,'Lkt1');
        end
        function obj = set.Lkt1l(obj,val)
            obj = setParameter(obj,'Lkt1l',val,0,'real');
        end
        function res = get.Lkt1l(obj)
            res = getParameter(obj,'Lkt1l');
        end
        function obj = set.Lkt2(obj,val)
            obj = setParameter(obj,'Lkt2',val,0,'real');
        end
        function res = get.Lkt2(obj)
            res = getParameter(obj,'Lkt2');
        end
        function obj = set.Lku0(obj,val)
            obj = setParameter(obj,'Lku0',val,0,'real');
        end
        function res = get.Lku0(obj)
            res = getParameter(obj,'Lku0');
        end
        function obj = set.Lkvth0(obj,val)
            obj = setParameter(obj,'Lkvth0',val,0,'real');
        end
        function res = get.Lkvth0(obj)
            res = getParameter(obj,'Lkvth0');
        end
        function obj = set.Ll(obj,val)
            obj = setParameter(obj,'Ll',val,0,'real');
        end
        function res = get.Ll(obj)
            res = getParameter(obj,'Ll');
        end
        function obj = set.Llc(obj,val)
            obj = setParameter(obj,'Llc',val,0,'real');
        end
        function res = get.Llc(obj)
            res = getParameter(obj,'Llc');
        end
        function obj = set.Lln(obj,val)
            obj = setParameter(obj,'Lln',val,0,'real');
        end
        function res = get.Lln(obj)
            res = getParameter(obj,'Lln');
        end
        function obj = set.Llodku0(obj,val)
            obj = setParameter(obj,'Llodku0',val,0,'real');
        end
        function res = get.Llodku0(obj)
            res = getParameter(obj,'Llodku0');
        end
        function obj = set.Llodvth(obj,val)
            obj = setParameter(obj,'Llodvth',val,0,'real');
        end
        function res = get.Llodvth(obj)
            res = getParameter(obj,'Llodvth');
        end
        function obj = set.Lmax(obj,val)
            obj = setParameter(obj,'Lmax',val,0,'real');
        end
        function res = get.Lmax(obj)
            res = getParameter(obj,'Lmax');
        end
        function obj = set.Lmin(obj,val)
            obj = setParameter(obj,'Lmin',val,0,'real');
        end
        function res = get.Lmin(obj)
            res = getParameter(obj,'Lmin');
        end
        function obj = set.Lmlt(obj,val)
            obj = setParameter(obj,'Lmlt',val,0,'real');
        end
        function res = get.Lmlt(obj)
            res = getParameter(obj,'Lmlt');
        end
        function obj = set.Lmoin(obj,val)
            obj = setParameter(obj,'Lmoin',val,0,'real');
        end
        function res = get.Lmoin(obj)
            res = getParameter(obj,'Lmoin');
        end
        function obj = set.Lmu20(obj,val)
            obj = setParameter(obj,'Lmu20',val,0,'real');
        end
        function res = get.Lmu20(obj)
            res = getParameter(obj,'Lmu20');
        end
        function obj = set.Lmu2b(obj,val)
            obj = setParameter(obj,'Lmu2b',val,0,'real');
        end
        function res = get.Lmu2b(obj)
            res = getParameter(obj,'Lmu2b');
        end
        function obj = set.Lmu2g(obj,val)
            obj = setParameter(obj,'Lmu2g',val,0,'real');
        end
        function res = get.Lmu2g(obj)
            res = getParameter(obj,'Lmu2g');
        end
        function obj = set.Lmu30(obj,val)
            obj = setParameter(obj,'Lmu30',val,0,'real');
        end
        function res = get.Lmu30(obj)
            res = getParameter(obj,'Lmu30');
        end
        function obj = set.Lmu3b(obj,val)
            obj = setParameter(obj,'Lmu3b',val,0,'real');
        end
        function res = get.Lmu3b(obj)
            res = getParameter(obj,'Lmu3b');
        end
        function obj = set.Lmu3g(obj,val)
            obj = setParameter(obj,'Lmu3g',val,0,'real');
        end
        function res = get.Lmu3g(obj)
            res = getParameter(obj,'Lmu3g');
        end
        function obj = set.Lmu40(obj,val)
            obj = setParameter(obj,'Lmu40',val,0,'real');
        end
        function res = get.Lmu40(obj)
            res = getParameter(obj,'Lmu40');
        end
        function obj = set.Lmu4b(obj,val)
            obj = setParameter(obj,'Lmu4b',val,0,'real');
        end
        function res = get.Lmu4b(obj)
            res = getParameter(obj,'Lmu4b');
        end
        function obj = set.Lmu4g(obj,val)
            obj = setParameter(obj,'Lmu4g',val,0,'real');
        end
        function res = get.Lmu4g(obj)
            res = getParameter(obj,'Lmu4g');
        end
        function obj = set.Lmus(obj,val)
            obj = setParameter(obj,'Lmus',val,0,'real');
        end
        function res = get.Lmus(obj)
            res = getParameter(obj,'Lmus');
        end
        function obj = set.Ln0(obj,val)
            obj = setParameter(obj,'Ln0',val,0,'real');
        end
        function res = get.Ln0(obj)
            res = getParameter(obj,'Ln0');
        end
        function obj = set.Lnb(obj,val)
            obj = setParameter(obj,'Lnb',val,0,'real');
        end
        function res = get.Lnb(obj)
            res = getParameter(obj,'Lnb');
        end
        function obj = set.Lnch(obj,val)
            obj = setParameter(obj,'Lnch',val,0,'real');
        end
        function res = get.Lnch(obj)
            res = getParameter(obj,'Lnch');
        end
        function obj = set.Lnd(obj,val)
            obj = setParameter(obj,'Lnd',val,0,'real');
        end
        function res = get.Lnd(obj)
            res = getParameter(obj,'Lnd');
        end
        function obj = set.Lnfactor(obj,val)
            obj = setParameter(obj,'Lnfactor',val,0,'real');
        end
        function res = get.Lnfactor(obj)
            res = getParameter(obj,'Lnfactor');
        end
        function obj = set.Lngate(obj,val)
            obj = setParameter(obj,'Lngate',val,0,'real');
        end
        function res = get.Lngate(obj)
            res = getParameter(obj,'Lngate');
        end
        function obj = set.Lnlx(obj,val)
            obj = setParameter(obj,'Lnlx',val,0,'real');
        end
        function res = get.Lnlx(obj)
            res = getParameter(obj,'Lnlx');
        end
        function obj = set.Lnoff(obj,val)
            obj = setParameter(obj,'Lnoff',val,0,'real');
        end
        function res = get.Lnoff(obj)
            res = getParameter(obj,'Lnoff');
        end
        function obj = set.Lnsub(obj,val)
            obj = setParameter(obj,'Lnsub',val,0,'real');
        end
        function res = get.Lnsub(obj)
            res = getParameter(obj,'Lnsub');
        end
        function obj = set.Lodeta0(obj,val)
            obj = setParameter(obj,'Lodeta0',val,0,'real');
        end
        function res = get.Lodeta0(obj)
            res = getParameter(obj,'Lodeta0');
        end
        function obj = set.Lodk2(obj,val)
            obj = setParameter(obj,'Lodk2',val,0,'real');
        end
        function res = get.Lodk2(obj)
            res = getParameter(obj,'Lodk2');
        end
        function obj = set.Lpclm(obj,val)
            obj = setParameter(obj,'Lpclm',val,0,'real');
        end
        function res = get.Lpclm(obj)
            res = getParameter(obj,'Lpclm');
        end
        function obj = set.Lpdiblc1(obj,val)
            obj = setParameter(obj,'Lpdiblc1',val,0,'real');
        end
        function res = get.Lpdiblc1(obj)
            res = getParameter(obj,'Lpdiblc1');
        end
        function obj = set.Lpdiblc2(obj,val)
            obj = setParameter(obj,'Lpdiblc2',val,0,'real');
        end
        function res = get.Lpdiblc2(obj)
            res = getParameter(obj,'Lpdiblc2');
        end
        function obj = set.Lpdiblcb(obj,val)
            obj = setParameter(obj,'Lpdiblcb',val,0,'real');
        end
        function res = get.Lpdiblcb(obj)
            res = getParameter(obj,'Lpdiblcb');
        end
        function obj = set.Lphi(obj,val)
            obj = setParameter(obj,'Lphi',val,0,'real');
        end
        function res = get.Lphi(obj)
            res = getParameter(obj,'Lphi');
        end
        function obj = set.Lprt(obj,val)
            obj = setParameter(obj,'Lprt',val,0,'real');
        end
        function res = get.Lprt(obj)
            res = getParameter(obj,'Lprt');
        end
        function obj = set.Lprwb(obj,val)
            obj = setParameter(obj,'Lprwb',val,0,'real');
        end
        function res = get.Lprwb(obj)
            res = getParameter(obj,'Lprwb');
        end
        function obj = set.Lprwg(obj,val)
            obj = setParameter(obj,'Lprwg',val,0,'real');
        end
        function res = get.Lprwg(obj)
            res = getParameter(obj,'Lprwg');
        end
        function obj = set.Lpscbe1(obj,val)
            obj = setParameter(obj,'Lpscbe1',val,0,'real');
        end
        function res = get.Lpscbe1(obj)
            res = getParameter(obj,'Lpscbe1');
        end
        function obj = set.Lpscbe2(obj,val)
            obj = setParameter(obj,'Lpscbe2',val,0,'real');
        end
        function res = get.Lpscbe2(obj)
            res = getParameter(obj,'Lpscbe2');
        end
        function obj = set.Lpvag(obj,val)
            obj = setParameter(obj,'Lpvag',val,0,'real');
        end
        function res = get.Lpvag(obj)
            res = getParameter(obj,'Lpvag');
        end
        function obj = set.Lrdsw(obj,val)
            obj = setParameter(obj,'Lrdsw',val,0,'real');
        end
        function res = get.Lrdsw(obj)
            res = getParameter(obj,'Lrdsw');
        end
        function obj = set.Lt(obj,val)
            obj = setParameter(obj,'Lt',val,0,'real');
        end
        function res = get.Lt(obj)
            res = getParameter(obj,'Lt');
        end
        function obj = set.Lu0(obj,val)
            obj = setParameter(obj,'Lu0',val,0,'real');
        end
        function res = get.Lu0(obj)
            res = getParameter(obj,'Lu0');
        end
        function obj = set.Lu1(obj,val)
            obj = setParameter(obj,'Lu1',val,0,'real');
        end
        function res = get.Lu1(obj)
            res = getParameter(obj,'Lu1');
        end
        function obj = set.Lua(obj,val)
            obj = setParameter(obj,'Lua',val,0,'real');
        end
        function res = get.Lua(obj)
            res = getParameter(obj,'Lua');
        end
        function obj = set.Lua1(obj,val)
            obj = setParameter(obj,'Lua1',val,0,'real');
        end
        function res = get.Lua1(obj)
            res = getParameter(obj,'Lua1');
        end
        function obj = set.Lub(obj,val)
            obj = setParameter(obj,'Lub',val,0,'real');
        end
        function res = get.Lub(obj)
            res = getParameter(obj,'Lub');
        end
        function obj = set.Lub0(obj,val)
            obj = setParameter(obj,'Lub0',val,0,'real');
        end
        function res = get.Lub0(obj)
            res = getParameter(obj,'Lub0');
        end
        function obj = set.Lub1(obj,val)
            obj = setParameter(obj,'Lub1',val,0,'real');
        end
        function res = get.Lub1(obj)
            res = getParameter(obj,'Lub1');
        end
        function obj = set.Lubb(obj,val)
            obj = setParameter(obj,'Lubb',val,0,'real');
        end
        function res = get.Lubb(obj)
            res = getParameter(obj,'Lubb');
        end
        function obj = set.Luc(obj,val)
            obj = setParameter(obj,'Luc',val,0,'real');
        end
        function res = get.Luc(obj)
            res = getParameter(obj,'Luc');
        end
        function obj = set.Luc1(obj,val)
            obj = setParameter(obj,'Luc1',val,0,'real');
        end
        function res = get.Luc1(obj)
            res = getParameter(obj,'Luc1');
        end
        function obj = set.Lute(obj,val)
            obj = setParameter(obj,'Lute',val,0,'real');
        end
        function res = get.Lute(obj)
            res = getParameter(obj,'Lute');
        end
        function obj = set.Lvbm(obj,val)
            obj = setParameter(obj,'Lvbm',val,0,'real');
        end
        function res = get.Lvbm(obj)
            res = getParameter(obj,'Lvbm');
        end
        function obj = set.Lvbx(obj,val)
            obj = setParameter(obj,'Lvbx',val,0,'real');
        end
        function res = get.Lvbx(obj)
            res = getParameter(obj,'Lvbx');
        end
        function obj = set.Lvfb(obj,val)
            obj = setParameter(obj,'Lvfb',val,0,'real');
        end
        function res = get.Lvfb(obj)
            res = getParameter(obj,'Lvfb');
        end
        function obj = set.Lvfbcv(obj,val)
            obj = setParameter(obj,'Lvfbcv',val,0,'real');
        end
        function res = get.Lvfbcv(obj)
            res = getParameter(obj,'Lvfbcv');
        end
        function obj = set.Lvghigh(obj,val)
            obj = setParameter(obj,'Lvghigh',val,0,'real');
        end
        function res = get.Lvghigh(obj)
            res = getParameter(obj,'Lvghigh');
        end
        function obj = set.Lvglow(obj,val)
            obj = setParameter(obj,'Lvglow',val,0,'real');
        end
        function res = get.Lvglow(obj)
            res = getParameter(obj,'Lvglow');
        end
        function obj = set.Lvof0(obj,val)
            obj = setParameter(obj,'Lvof0',val,0,'real');
        end
        function res = get.Lvof0(obj)
            res = getParameter(obj,'Lvof0');
        end
        function obj = set.Lvofb(obj,val)
            obj = setParameter(obj,'Lvofb',val,0,'real');
        end
        function res = get.Lvofb(obj)
            res = getParameter(obj,'Lvofb');
        end
        function obj = set.Lvofd(obj,val)
            obj = setParameter(obj,'Lvofd',val,0,'real');
        end
        function res = get.Lvofd(obj)
            res = getParameter(obj,'Lvofd');
        end
        function obj = set.Lvoff(obj,val)
            obj = setParameter(obj,'Lvoff',val,0,'real');
        end
        function res = get.Lvoff(obj)
            res = getParameter(obj,'Lvoff');
        end
        function obj = set.Lvoffcv(obj,val)
            obj = setParameter(obj,'Lvoffcv',val,0,'real');
        end
        function res = get.Lvoffcv(obj)
            res = getParameter(obj,'Lvoffcv');
        end
        function obj = set.Lvsat(obj,val)
            obj = setParameter(obj,'Lvsat',val,0,'real');
        end
        function res = get.Lvsat(obj)
            res = getParameter(obj,'Lvsat');
        end
        function obj = set.Lvth0(obj,val)
            obj = setParameter(obj,'Lvth0',val,0,'real');
        end
        function res = get.Lvth0(obj)
            res = getParameter(obj,'Lvth0');
        end
        function obj = set.Lw(obj,val)
            obj = setParameter(obj,'Lw',val,0,'real');
        end
        function res = get.Lw(obj)
            res = getParameter(obj,'Lw');
        end
        function obj = set.Lw0(obj,val)
            obj = setParameter(obj,'Lw0',val,0,'real');
        end
        function res = get.Lw0(obj)
            res = getParameter(obj,'Lw0');
        end
        function obj = set.Lwc(obj,val)
            obj = setParameter(obj,'Lwc',val,0,'real');
        end
        function res = get.Lwc(obj)
            res = getParameter(obj,'Lwc');
        end
        function obj = set.Lwl(obj,val)
            obj = setParameter(obj,'Lwl',val,0,'real');
        end
        function res = get.Lwl(obj)
            res = getParameter(obj,'Lwl');
        end
        function obj = set.Lwlc(obj,val)
            obj = setParameter(obj,'Lwlc',val,0,'real');
        end
        function res = get.Lwlc(obj)
            res = getParameter(obj,'Lwlc');
        end
        function obj = set.Lwn(obj,val)
            obj = setParameter(obj,'Lwn',val,0,'real');
        end
        function res = get.Lwn(obj)
            res = getParameter(obj,'Lwn');
        end
        function obj = set.Lwr(obj,val)
            obj = setParameter(obj,'Lwr',val,0,'real');
        end
        function res = get.Lwr(obj)
            res = getParameter(obj,'Lwr');
        end
        function obj = set.Lx2e(obj,val)
            obj = setParameter(obj,'Lx2e',val,0,'real');
        end
        function res = get.Lx2e(obj)
            res = getParameter(obj,'Lx2e');
        end
        function obj = set.Lx2ms(obj,val)
            obj = setParameter(obj,'Lx2ms',val,0,'real');
        end
        function res = get.Lx2ms(obj)
            res = getParameter(obj,'Lx2ms');
        end
        function obj = set.Lx2mz(obj,val)
            obj = setParameter(obj,'Lx2mz',val,0,'real');
        end
        function res = get.Lx2mz(obj)
            res = getParameter(obj,'Lx2mz');
        end
        function obj = set.Lx2u0(obj,val)
            obj = setParameter(obj,'Lx2u0',val,0,'real');
        end
        function res = get.Lx2u0(obj)
            res = getParameter(obj,'Lx2u0');
        end
        function obj = set.Lx2u1(obj,val)
            obj = setParameter(obj,'Lx2u1',val,0,'real');
        end
        function res = get.Lx2u1(obj)
            res = getParameter(obj,'Lx2u1');
        end
        function obj = set.Lx3ms(obj,val)
            obj = setParameter(obj,'Lx3ms',val,0,'real');
        end
        function res = get.Lx3ms(obj)
            res = getParameter(obj,'Lx3ms');
        end
        function obj = set.Lx3u1(obj,val)
            obj = setParameter(obj,'Lx3u1',val,0,'real');
        end
        function res = get.Lx3u1(obj)
            res = getParameter(obj,'Lx3u1');
        end
        function obj = set.Lxj(obj,val)
            obj = setParameter(obj,'Lxj',val,0,'real');
        end
        function res = get.Lxj(obj)
            res = getParameter(obj,'Lxj');
        end
        function obj = set.Lxt(obj,val)
            obj = setParameter(obj,'Lxt',val,0,'real');
        end
        function res = get.Lxt(obj)
            res = getParameter(obj,'Lxt');
        end
        function obj = set.Minr(obj,val)
            obj = setParameter(obj,'Minr',val,0,'real');
        end
        function res = get.Minr(obj)
            res = getParameter(obj,'Minr');
        end
        function obj = set.Mj(obj,val)
            obj = setParameter(obj,'Mj',val,0,'real');
        end
        function res = get.Mj(obj)
            res = getParameter(obj,'Mj');
        end
        function obj = set.Mjsw(obj,val)
            obj = setParameter(obj,'Mjsw',val,0,'real');
        end
        function res = get.Mjsw(obj)
            res = getParameter(obj,'Mjsw');
        end
        function obj = set.Mjswg(obj,val)
            obj = setParameter(obj,'Mjswg',val,0,'real');
        end
        function res = get.Mjswg(obj)
            res = getParameter(obj,'Mjswg');
        end
        function obj = set.Mobmod(obj,val)
            obj = setParameter(obj,'Mobmod',val,0,'integer');
        end
        function res = get.Mobmod(obj)
            res = getParameter(obj,'Mobmod');
        end
        function obj = set.Moin(obj,val)
            obj = setParameter(obj,'Moin',val,0,'real');
        end
        function res = get.Moin(obj)
            res = getParameter(obj,'Moin');
        end
        function obj = set.Mu0(obj,val)
            obj = setParameter(obj,'Mu0',val,0,'real');
        end
        function res = get.Mu0(obj)
            res = getParameter(obj,'Mu0');
        end
        function obj = set.Mu20(obj,val)
            obj = setParameter(obj,'Mu20',val,0,'real');
        end
        function res = get.Mu20(obj)
            res = getParameter(obj,'Mu20');
        end
        function obj = set.Mu2b(obj,val)
            obj = setParameter(obj,'Mu2b',val,0,'real');
        end
        function res = get.Mu2b(obj)
            res = getParameter(obj,'Mu2b');
        end
        function obj = set.Mu2g(obj,val)
            obj = setParameter(obj,'Mu2g',val,0,'real');
        end
        function res = get.Mu2g(obj)
            res = getParameter(obj,'Mu2g');
        end
        function obj = set.Mu30(obj,val)
            obj = setParameter(obj,'Mu30',val,0,'real');
        end
        function res = get.Mu30(obj)
            res = getParameter(obj,'Mu30');
        end
        function obj = set.Mu3b(obj,val)
            obj = setParameter(obj,'Mu3b',val,0,'real');
        end
        function res = get.Mu3b(obj)
            res = getParameter(obj,'Mu3b');
        end
        function obj = set.Mu3g(obj,val)
            obj = setParameter(obj,'Mu3g',val,0,'real');
        end
        function res = get.Mu3g(obj)
            res = getParameter(obj,'Mu3g');
        end
        function obj = set.Mu40(obj,val)
            obj = setParameter(obj,'Mu40',val,0,'real');
        end
        function res = get.Mu40(obj)
            res = getParameter(obj,'Mu40');
        end
        function obj = set.Mu4b(obj,val)
            obj = setParameter(obj,'Mu4b',val,0,'real');
        end
        function res = get.Mu4b(obj)
            res = getParameter(obj,'Mu4b');
        end
        function obj = set.Mu4g(obj,val)
            obj = setParameter(obj,'Mu4g',val,0,'real');
        end
        function res = get.Mu4g(obj)
            res = getParameter(obj,'Mu4g');
        end
        function obj = set.Mus(obj,val)
            obj = setParameter(obj,'Mus',val,0,'real');
        end
        function res = get.Mus(obj)
            res = getParameter(obj,'Mus');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.N0(obj,val)
            obj = setParameter(obj,'N0',val,0,'real');
        end
        function res = get.N0(obj)
            res = getParameter(obj,'N0');
        end
        function obj = set.NMOS(obj,val)
            obj = setParameter(obj,'NMOS',val,0,'boolean');
        end
        function res = get.NMOS(obj)
            res = getParameter(obj,'NMOS');
        end
        function obj = set.Nb(obj,val)
            obj = setParameter(obj,'Nb',val,0,'real');
        end
        function res = get.Nb(obj)
            res = getParameter(obj,'Nb');
        end
        function obj = set.Nch(obj,val)
            obj = setParameter(obj,'Nch',val,0,'real');
        end
        function res = get.Nch(obj)
            res = getParameter(obj,'Nch');
        end
        function obj = set.Nd(obj,val)
            obj = setParameter(obj,'Nd',val,0,'real');
        end
        function res = get.Nd(obj)
            res = getParameter(obj,'Nd');
        end
        function obj = set.Neff(obj,val)
            obj = setParameter(obj,'Neff',val,0,'real');
        end
        function res = get.Neff(obj)
            res = getParameter(obj,'Neff');
        end
        function obj = set.Nepi(obj,val)
            obj = setParameter(obj,'Nepi',val,0,'real');
        end
        function res = get.Nepi(obj)
            res = getParameter(obj,'Nepi');
        end
        function obj = set.Nfactor(obj,val)
            obj = setParameter(obj,'Nfactor',val,0,'real');
        end
        function res = get.Nfactor(obj)
            res = getParameter(obj,'Nfactor');
        end
        function obj = set.Nfs(obj,val)
            obj = setParameter(obj,'Nfs',val,0,'real');
        end
        function res = get.Nfs(obj)
            res = getParameter(obj,'Nfs');
        end
        function obj = set.Ngate(obj,val)
            obj = setParameter(obj,'Ngate',val,0,'real');
        end
        function res = get.Ngate(obj)
            res = getParameter(obj,'Ngate');
        end
        function obj = set.Nlev(obj,val)
            obj = setParameter(obj,'Nlev',val,0,'integer');
        end
        function res = get.Nlev(obj)
            res = getParameter(obj,'Nlev');
        end
        function obj = set.Nlx(obj,val)
            obj = setParameter(obj,'Nlx',val,0,'real');
        end
        function res = get.Nlx(obj)
            res = getParameter(obj,'Nlx');
        end
        function obj = set.Noff(obj,val)
            obj = setParameter(obj,'Noff',val,0,'real');
        end
        function res = get.Noff(obj)
            res = getParameter(obj,'Noff');
        end
        function obj = set.Noia(obj,val)
            obj = setParameter(obj,'Noia',val,0,'real');
        end
        function res = get.Noia(obj)
            res = getParameter(obj,'Noia');
        end
        function obj = set.Noib(obj,val)
            obj = setParameter(obj,'Noib',val,0,'real');
        end
        function res = get.Noib(obj)
            res = getParameter(obj,'Noib');
        end
        function obj = set.Noic(obj,val)
            obj = setParameter(obj,'Noic',val,0,'real');
        end
        function res = get.Noic(obj)
            res = getParameter(obj,'Noic');
        end
        function obj = set.Noimod(obj,val)
            obj = setParameter(obj,'Noimod',val,0,'integer');
        end
        function res = get.Noimod(obj)
            res = getParameter(obj,'Noimod');
        end
        function obj = set.Nqsmod(obj,val)
            obj = setParameter(obj,'Nqsmod',val,0,'integer');
        end
        function res = get.Nqsmod(obj)
            res = getParameter(obj,'Nqsmod');
        end
        function obj = set.Nrd(obj,val)
            obj = setParameter(obj,'Nrd',val,0,'real');
        end
        function res = get.Nrd(obj)
            res = getParameter(obj,'Nrd');
        end
        function obj = set.Nrs(obj,val)
            obj = setParameter(obj,'Nrs',val,0,'real');
        end
        function res = get.Nrs(obj)
            res = getParameter(obj,'Nrs');
        end
        function obj = set.Nss(obj,val)
            obj = setParameter(obj,'Nss',val,0,'real');
        end
        function res = get.Nss(obj)
            res = getParameter(obj,'Nss');
        end
        function obj = set.Nsub(obj,val)
            obj = setParameter(obj,'Nsub',val,0,'real');
        end
        function res = get.Nsub(obj)
            res = getParameter(obj,'Nsub');
        end
        function obj = set.PMOS(obj,val)
            obj = setParameter(obj,'PMOS',val,0,'boolean');
        end
        function res = get.PMOS(obj)
            res = getParameter(obj,'PMOS');
        end
        function obj = set.Pa0(obj,val)
            obj = setParameter(obj,'Pa0',val,0,'real');
        end
        function res = get.Pa0(obj)
            res = getParameter(obj,'Pa0');
        end
        function obj = set.Pa1(obj,val)
            obj = setParameter(obj,'Pa1',val,0,'real');
        end
        function res = get.Pa1(obj)
            res = getParameter(obj,'Pa1');
        end
        function obj = set.Pa2(obj,val)
            obj = setParameter(obj,'Pa2',val,0,'real');
        end
        function res = get.Pa2(obj)
            res = getParameter(obj,'Pa2');
        end
        function obj = set.Pacde(obj,val)
            obj = setParameter(obj,'Pacde',val,0,'real');
        end
        function res = get.Pacde(obj)
            res = getParameter(obj,'Pacde');
        end
        function obj = set.Pags(obj,val)
            obj = setParameter(obj,'Pags',val,0,'real');
        end
        function res = get.Pags(obj)
            res = getParameter(obj,'Pags');
        end
        function obj = set.Palpha0(obj,val)
            obj = setParameter(obj,'Palpha0',val,0,'real');
        end
        function res = get.Palpha0(obj)
            res = getParameter(obj,'Palpha0');
        end
        function obj = set.Palpha1(obj,val)
            obj = setParameter(obj,'Palpha1',val,0,'real');
        end
        function res = get.Palpha1(obj)
            res = getParameter(obj,'Palpha1');
        end
        function obj = set.Paramchk(obj,val)
            obj = setParameter(obj,'Paramchk',val,0,'integer');
        end
        function res = get.Paramchk(obj)
            res = getParameter(obj,'Paramchk');
        end
        function obj = set.Pat(obj,val)
            obj = setParameter(obj,'Pat',val,0,'real');
        end
        function res = get.Pat(obj)
            res = getParameter(obj,'Pat');
        end
        function obj = set.Pb(obj,val)
            obj = setParameter(obj,'Pb',val,0,'real');
        end
        function res = get.Pb(obj)
            res = getParameter(obj,'Pb');
        end
        function obj = set.Pb0(obj,val)
            obj = setParameter(obj,'Pb0',val,0,'real');
        end
        function res = get.Pb0(obj)
            res = getParameter(obj,'Pb0');
        end
        function obj = set.Pb1(obj,val)
            obj = setParameter(obj,'Pb1',val,0,'real');
        end
        function res = get.Pb1(obj)
            res = getParameter(obj,'Pb1');
        end
        function obj = set.Pbeta0(obj,val)
            obj = setParameter(obj,'Pbeta0',val,0,'real');
        end
        function res = get.Pbeta0(obj)
            res = getParameter(obj,'Pbeta0');
        end
        function obj = set.Pbsw(obj,val)
            obj = setParameter(obj,'Pbsw',val,0,'real');
        end
        function res = get.Pbsw(obj)
            res = getParameter(obj,'Pbsw');
        end
        function obj = set.Pbswg(obj,val)
            obj = setParameter(obj,'Pbswg',val,0,'real');
        end
        function res = get.Pbswg(obj)
            res = getParameter(obj,'Pbswg');
        end
        function obj = set.Pcdsc(obj,val)
            obj = setParameter(obj,'Pcdsc',val,0,'real');
        end
        function res = get.Pcdsc(obj)
            res = getParameter(obj,'Pcdsc');
        end
        function obj = set.Pcdscb(obj,val)
            obj = setParameter(obj,'Pcdscb',val,0,'real');
        end
        function res = get.Pcdscb(obj)
            res = getParameter(obj,'Pcdscb');
        end
        function obj = set.Pcdscd(obj,val)
            obj = setParameter(obj,'Pcdscd',val,0,'real');
        end
        function res = get.Pcdscd(obj)
            res = getParameter(obj,'Pcdscd');
        end
        function obj = set.Pcf(obj,val)
            obj = setParameter(obj,'Pcf',val,0,'real');
        end
        function res = get.Pcf(obj)
            res = getParameter(obj,'Pcf');
        end
        function obj = set.Pcgdl(obj,val)
            obj = setParameter(obj,'Pcgdl',val,0,'real');
        end
        function res = get.Pcgdl(obj)
            res = getParameter(obj,'Pcgdl');
        end
        function obj = set.Pcgsl(obj,val)
            obj = setParameter(obj,'Pcgsl',val,0,'real');
        end
        function res = get.Pcgsl(obj)
            res = getParameter(obj,'Pcgsl');
        end
        function obj = set.Pcit(obj,val)
            obj = setParameter(obj,'Pcit',val,0,'real');
        end
        function res = get.Pcit(obj)
            res = getParameter(obj,'Pcit');
        end
        function obj = set.Pckappa(obj,val)
            obj = setParameter(obj,'Pckappa',val,0,'real');
        end
        function res = get.Pckappa(obj)
            res = getParameter(obj,'Pckappa');
        end
        function obj = set.Pclc(obj,val)
            obj = setParameter(obj,'Pclc',val,0,'real');
        end
        function res = get.Pclc(obj)
            res = getParameter(obj,'Pclc');
        end
        function obj = set.Pcle(obj,val)
            obj = setParameter(obj,'Pcle',val,0,'real');
        end
        function res = get.Pcle(obj)
            res = getParameter(obj,'Pcle');
        end
        function obj = set.Pclm(obj,val)
            obj = setParameter(obj,'Pclm',val,0,'real');
        end
        function res = get.Pclm(obj)
            res = getParameter(obj,'Pclm');
        end
        function obj = set.Pd(obj,val)
            obj = setParameter(obj,'Pd',val,0,'real');
        end
        function res = get.Pd(obj)
            res = getParameter(obj,'Pd');
        end
        function obj = set.Pdelta(obj,val)
            obj = setParameter(obj,'Pdelta',val,0,'real');
        end
        function res = get.Pdelta(obj)
            res = getParameter(obj,'Pdelta');
        end
        function obj = set.Pdiblc1(obj,val)
            obj = setParameter(obj,'Pdiblc1',val,0,'real');
        end
        function res = get.Pdiblc1(obj)
            res = getParameter(obj,'Pdiblc1');
        end
        function obj = set.Pdiblc2(obj,val)
            obj = setParameter(obj,'Pdiblc2',val,0,'real');
        end
        function res = get.Pdiblc2(obj)
            res = getParameter(obj,'Pdiblc2');
        end
        function obj = set.Pdiblcb(obj,val)
            obj = setParameter(obj,'Pdiblcb',val,0,'real');
        end
        function res = get.Pdiblcb(obj)
            res = getParameter(obj,'Pdiblcb');
        end
        function obj = set.Pdrout(obj,val)
            obj = setParameter(obj,'Pdrout',val,0,'real');
        end
        function res = get.Pdrout(obj)
            res = getParameter(obj,'Pdrout');
        end
        function obj = set.Pdsub(obj,val)
            obj = setParameter(obj,'Pdsub',val,0,'real');
        end
        function res = get.Pdsub(obj)
            res = getParameter(obj,'Pdsub');
        end
        function obj = set.Pdvt0(obj,val)
            obj = setParameter(obj,'Pdvt0',val,0,'real');
        end
        function res = get.Pdvt0(obj)
            res = getParameter(obj,'Pdvt0');
        end
        function obj = set.Pdvt0w(obj,val)
            obj = setParameter(obj,'Pdvt0w',val,0,'real');
        end
        function res = get.Pdvt0w(obj)
            res = getParameter(obj,'Pdvt0w');
        end
        function obj = set.Pdvt1(obj,val)
            obj = setParameter(obj,'Pdvt1',val,0,'real');
        end
        function res = get.Pdvt1(obj)
            res = getParameter(obj,'Pdvt1');
        end
        function obj = set.Pdvt1w(obj,val)
            obj = setParameter(obj,'Pdvt1w',val,0,'real');
        end
        function res = get.Pdvt1w(obj)
            res = getParameter(obj,'Pdvt1w');
        end
        function obj = set.Pdvt2(obj,val)
            obj = setParameter(obj,'Pdvt2',val,0,'real');
        end
        function res = get.Pdvt2(obj)
            res = getParameter(obj,'Pdvt2');
        end
        function obj = set.Pdvt2w(obj,val)
            obj = setParameter(obj,'Pdvt2w',val,0,'real');
        end
        function res = get.Pdvt2w(obj)
            res = getParameter(obj,'Pdvt2w');
        end
        function obj = set.Pdwb(obj,val)
            obj = setParameter(obj,'Pdwb',val,0,'real');
        end
        function res = get.Pdwb(obj)
            res = getParameter(obj,'Pdwb');
        end
        function obj = set.Pdwg(obj,val)
            obj = setParameter(obj,'Pdwg',val,0,'real');
        end
        function res = get.Pdwg(obj)
            res = getParameter(obj,'Pdwg');
        end
        function obj = set.Pelm(obj,val)
            obj = setParameter(obj,'Pelm',val,0,'real');
        end
        function res = get.Pelm(obj)
            res = getParameter(obj,'Pelm');
        end
        function obj = set.Peta0(obj,val)
            obj = setParameter(obj,'Peta0',val,0,'real');
        end
        function res = get.Peta0(obj)
            res = getParameter(obj,'Peta0');
        end
        function obj = set.Petab(obj,val)
            obj = setParameter(obj,'Petab',val,0,'real');
        end
        function res = get.Petab(obj)
            res = getParameter(obj,'Petab');
        end
        function obj = set.Pgamma1(obj,val)
            obj = setParameter(obj,'Pgamma1',val,0,'real');
        end
        function res = get.Pgamma1(obj)
            res = getParameter(obj,'Pgamma1');
        end
        function obj = set.Pgamma2(obj,val)
            obj = setParameter(obj,'Pgamma2',val,0,'real');
        end
        function res = get.Pgamma2(obj)
            res = getParameter(obj,'Pgamma2');
        end
        function obj = set.Phi(obj,val)
            obj = setParameter(obj,'Phi',val,0,'real');
        end
        function res = get.Phi(obj)
            res = getParameter(obj,'Phi');
        end
        function obj = set.Phis_jfet(obj,val)
            obj = setParameter(obj,'Phis_jfet',val,0,'real');
        end
        function res = get.Phis_jfet(obj)
            res = getParameter(obj,'Phis_jfet');
        end
        function obj = set.Pk1(obj,val)
            obj = setParameter(obj,'Pk1',val,0,'real');
        end
        function res = get.Pk1(obj)
            res = getParameter(obj,'Pk1');
        end
        function obj = set.Pk2(obj,val)
            obj = setParameter(obj,'Pk2',val,0,'real');
        end
        function res = get.Pk2(obj)
            res = getParameter(obj,'Pk2');
        end
        function obj = set.Pk3(obj,val)
            obj = setParameter(obj,'Pk3',val,0,'real');
        end
        function res = get.Pk3(obj)
            res = getParameter(obj,'Pk3');
        end
        function obj = set.Pk3b(obj,val)
            obj = setParameter(obj,'Pk3b',val,0,'real');
        end
        function res = get.Pk3b(obj)
            res = getParameter(obj,'Pk3b');
        end
        function obj = set.Pketa(obj,val)
            obj = setParameter(obj,'Pketa',val,0,'real');
        end
        function res = get.Pketa(obj)
            res = getParameter(obj,'Pketa');
        end
        function obj = set.Pkt1(obj,val)
            obj = setParameter(obj,'Pkt1',val,0,'real');
        end
        function res = get.Pkt1(obj)
            res = getParameter(obj,'Pkt1');
        end
        function obj = set.Pkt1l(obj,val)
            obj = setParameter(obj,'Pkt1l',val,0,'real');
        end
        function res = get.Pkt1l(obj)
            res = getParameter(obj,'Pkt1l');
        end
        function obj = set.Pkt2(obj,val)
            obj = setParameter(obj,'Pkt2',val,0,'real');
        end
        function res = get.Pkt2(obj)
            res = getParameter(obj,'Pkt2');
        end
        function obj = set.Pku0(obj,val)
            obj = setParameter(obj,'Pku0',val,0,'real');
        end
        function res = get.Pku0(obj)
            res = getParameter(obj,'Pku0');
        end
        function obj = set.Pkvth0(obj,val)
            obj = setParameter(obj,'Pkvth0',val,0,'real');
        end
        function res = get.Pkvth0(obj)
            res = getParameter(obj,'Pkvth0');
        end
        function obj = set.Pmoin(obj,val)
            obj = setParameter(obj,'Pmoin',val,0,'real');
        end
        function res = get.Pmoin(obj)
            res = getParameter(obj,'Pmoin');
        end
        function obj = set.Pnch(obj,val)
            obj = setParameter(obj,'Pnch',val,0,'real');
        end
        function res = get.Pnch(obj)
            res = getParameter(obj,'Pnch');
        end
        function obj = set.Pnfactor(obj,val)
            obj = setParameter(obj,'Pnfactor',val,0,'real');
        end
        function res = get.Pnfactor(obj)
            res = getParameter(obj,'Pnfactor');
        end
        function obj = set.Pngate(obj,val)
            obj = setParameter(obj,'Pngate',val,0,'real');
        end
        function res = get.Pngate(obj)
            res = getParameter(obj,'Pngate');
        end
        function obj = set.Pnlx(obj,val)
            obj = setParameter(obj,'Pnlx',val,0,'real');
        end
        function res = get.Pnlx(obj)
            res = getParameter(obj,'Pnlx');
        end
        function obj = set.Pnoff(obj,val)
            obj = setParameter(obj,'Pnoff',val,0,'real');
        end
        function res = get.Pnoff(obj)
            res = getParameter(obj,'Pnoff');
        end
        function obj = set.Pnsub(obj,val)
            obj = setParameter(obj,'Pnsub',val,0,'real');
        end
        function res = get.Pnsub(obj)
            res = getParameter(obj,'Pnsub');
        end
        function obj = set.Ppclm(obj,val)
            obj = setParameter(obj,'Ppclm',val,0,'real');
        end
        function res = get.Ppclm(obj)
            res = getParameter(obj,'Ppclm');
        end
        function obj = set.Ppdiblc1(obj,val)
            obj = setParameter(obj,'Ppdiblc1',val,0,'real');
        end
        function res = get.Ppdiblc1(obj)
            res = getParameter(obj,'Ppdiblc1');
        end
        function obj = set.Ppdiblc2(obj,val)
            obj = setParameter(obj,'Ppdiblc2',val,0,'real');
        end
        function res = get.Ppdiblc2(obj)
            res = getParameter(obj,'Ppdiblc2');
        end
        function obj = set.Ppdiblcb(obj,val)
            obj = setParameter(obj,'Ppdiblcb',val,0,'real');
        end
        function res = get.Ppdiblcb(obj)
            res = getParameter(obj,'Ppdiblcb');
        end
        function obj = set.Pprt(obj,val)
            obj = setParameter(obj,'Pprt',val,0,'real');
        end
        function res = get.Pprt(obj)
            res = getParameter(obj,'Pprt');
        end
        function obj = set.Pprwb(obj,val)
            obj = setParameter(obj,'Pprwb',val,0,'real');
        end
        function res = get.Pprwb(obj)
            res = getParameter(obj,'Pprwb');
        end
        function obj = set.Pprwg(obj,val)
            obj = setParameter(obj,'Pprwg',val,0,'real');
        end
        function res = get.Pprwg(obj)
            res = getParameter(obj,'Pprwg');
        end
        function obj = set.Ppscbe1(obj,val)
            obj = setParameter(obj,'Ppscbe1',val,0,'real');
        end
        function res = get.Ppscbe1(obj)
            res = getParameter(obj,'Ppscbe1');
        end
        function obj = set.Ppscbe2(obj,val)
            obj = setParameter(obj,'Ppscbe2',val,0,'real');
        end
        function res = get.Ppscbe2(obj)
            res = getParameter(obj,'Ppscbe2');
        end
        function obj = set.Ppvag(obj,val)
            obj = setParameter(obj,'Ppvag',val,0,'real');
        end
        function res = get.Ppvag(obj)
            res = getParameter(obj,'Ppvag');
        end
        function obj = set.Prdsw(obj,val)
            obj = setParameter(obj,'Prdsw',val,0,'real');
        end
        function res = get.Prdsw(obj)
            res = getParameter(obj,'Prdsw');
        end
        function obj = set.Prt(obj,val)
            obj = setParameter(obj,'Prt',val,0,'real');
        end
        function res = get.Prt(obj)
            res = getParameter(obj,'Prt');
        end
        function obj = set.Prwb(obj,val)
            obj = setParameter(obj,'Prwb',val,0,'real');
        end
        function res = get.Prwb(obj)
            res = getParameter(obj,'Prwb');
        end
        function obj = set.Prwg(obj,val)
            obj = setParameter(obj,'Prwg',val,0,'real');
        end
        function res = get.Prwg(obj)
            res = getParameter(obj,'Prwg');
        end
        function obj = set.Ps(obj,val)
            obj = setParameter(obj,'Ps',val,0,'real');
        end
        function res = get.Ps(obj)
            res = getParameter(obj,'Ps');
        end
        function obj = set.Pscbe1(obj,val)
            obj = setParameter(obj,'Pscbe1',val,0,'real');
        end
        function res = get.Pscbe1(obj)
            res = getParameter(obj,'Pscbe1');
        end
        function obj = set.Pscbe2(obj,val)
            obj = setParameter(obj,'Pscbe2',val,0,'real');
        end
        function res = get.Pscbe2(obj)
            res = getParameter(obj,'Pscbe2');
        end
        function obj = set.Pta(obj,val)
            obj = setParameter(obj,'Pta',val,0,'real');
        end
        function res = get.Pta(obj)
            res = getParameter(obj,'Pta');
        end
        function obj = set.Ptp(obj,val)
            obj = setParameter(obj,'Ptp',val,0,'real');
        end
        function res = get.Ptp(obj)
            res = getParameter(obj,'Ptp');
        end
        function obj = set.Pu0(obj,val)
            obj = setParameter(obj,'Pu0',val,0,'real');
        end
        function res = get.Pu0(obj)
            res = getParameter(obj,'Pu0');
        end
        function obj = set.Pua(obj,val)
            obj = setParameter(obj,'Pua',val,0,'real');
        end
        function res = get.Pua(obj)
            res = getParameter(obj,'Pua');
        end
        function obj = set.Pua1(obj,val)
            obj = setParameter(obj,'Pua1',val,0,'real');
        end
        function res = get.Pua1(obj)
            res = getParameter(obj,'Pua1');
        end
        function obj = set.Pub(obj,val)
            obj = setParameter(obj,'Pub',val,0,'real');
        end
        function res = get.Pub(obj)
            res = getParameter(obj,'Pub');
        end
        function obj = set.Pub1(obj,val)
            obj = setParameter(obj,'Pub1',val,0,'real');
        end
        function res = get.Pub1(obj)
            res = getParameter(obj,'Pub1');
        end
        function obj = set.Puc(obj,val)
            obj = setParameter(obj,'Puc',val,0,'real');
        end
        function res = get.Puc(obj)
            res = getParameter(obj,'Puc');
        end
        function obj = set.Puc1(obj,val)
            obj = setParameter(obj,'Puc1',val,0,'real');
        end
        function res = get.Puc1(obj)
            res = getParameter(obj,'Puc1');
        end
        function obj = set.Pute(obj,val)
            obj = setParameter(obj,'Pute',val,0,'real');
        end
        function res = get.Pute(obj)
            res = getParameter(obj,'Pute');
        end
        function obj = set.Pvag(obj,val)
            obj = setParameter(obj,'Pvag',val,0,'real');
        end
        function res = get.Pvag(obj)
            res = getParameter(obj,'Pvag');
        end
        function obj = set.Pvbm(obj,val)
            obj = setParameter(obj,'Pvbm',val,0,'real');
        end
        function res = get.Pvbm(obj)
            res = getParameter(obj,'Pvbm');
        end
        function obj = set.Pvbx(obj,val)
            obj = setParameter(obj,'Pvbx',val,0,'real');
        end
        function res = get.Pvbx(obj)
            res = getParameter(obj,'Pvbx');
        end
        function obj = set.Pvfb(obj,val)
            obj = setParameter(obj,'Pvfb',val,0,'real');
        end
        function res = get.Pvfb(obj)
            res = getParameter(obj,'Pvfb');
        end
        function obj = set.Pvfbcv(obj,val)
            obj = setParameter(obj,'Pvfbcv',val,0,'real');
        end
        function res = get.Pvfbcv(obj)
            res = getParameter(obj,'Pvfbcv');
        end
        function obj = set.Pvoff(obj,val)
            obj = setParameter(obj,'Pvoff',val,0,'real');
        end
        function res = get.Pvoff(obj)
            res = getParameter(obj,'Pvoff');
        end
        function obj = set.Pvoffcv(obj,val)
            obj = setParameter(obj,'Pvoffcv',val,0,'real');
        end
        function res = get.Pvoffcv(obj)
            res = getParameter(obj,'Pvoffcv');
        end
        function obj = set.Pvsat(obj,val)
            obj = setParameter(obj,'Pvsat',val,0,'real');
        end
        function res = get.Pvsat(obj)
            res = getParameter(obj,'Pvsat');
        end
        function obj = set.Pvth0(obj,val)
            obj = setParameter(obj,'Pvth0',val,0,'real');
        end
        function res = get.Pvth0(obj)
            res = getParameter(obj,'Pvth0');
        end
        function obj = set.Pw0(obj,val)
            obj = setParameter(obj,'Pw0',val,0,'real');
        end
        function res = get.Pw0(obj)
            res = getParameter(obj,'Pw0');
        end
        function obj = set.Pwr(obj,val)
            obj = setParameter(obj,'Pwr',val,0,'real');
        end
        function res = get.Pwr(obj)
            res = getParameter(obj,'Pwr');
        end
        function obj = set.Pxj(obj,val)
            obj = setParameter(obj,'Pxj',val,0,'real');
        end
        function res = get.Pxj(obj)
            res = getParameter(obj,'Pxj');
        end
        function obj = set.Pxt(obj,val)
            obj = setParameter(obj,'Pxt',val,0,'real');
        end
        function res = get.Pxt(obj)
            res = getParameter(obj,'Pxt');
        end
        function obj = set.Rd(obj,val)
            obj = setParameter(obj,'Rd',val,0,'real');
        end
        function res = get.Rd(obj)
            res = getParameter(obj,'Rd');
        end
        function obj = set.Rdc(obj,val)
            obj = setParameter(obj,'Rdc',val,0,'real');
        end
        function res = get.Rdc(obj)
            res = getParameter(obj,'Rdc');
        end
        function obj = set.Rdd(obj,val)
            obj = setParameter(obj,'Rdd',val,0,'real');
        end
        function res = get.Rdd(obj)
            res = getParameter(obj,'Rdd');
        end
        function obj = set.Rds(obj,val)
            obj = setParameter(obj,'Rds',val,0,'real');
        end
        function res = get.Rds(obj)
            res = getParameter(obj,'Rds');
        end
        function obj = set.Rdsw(obj,val)
            obj = setParameter(obj,'Rdsw',val,0,'real');
        end
        function res = get.Rdsw(obj)
            res = getParameter(obj,'Rdsw');
        end
        function obj = set.Resistivity_epi(obj,val)
            obj = setParameter(obj,'Resistivity_epi',val,0,'real');
        end
        function res = get.Resistivity_epi(obj)
            res = getParameter(obj,'Resistivity_epi');
        end
        function obj = set.Rg(obj,val)
            obj = setParameter(obj,'Rg',val,0,'real');
        end
        function res = get.Rg(obj)
            res = getParameter(obj,'Rg');
        end
        function obj = set.Rs(obj,val)
            obj = setParameter(obj,'Rs',val,0,'real');
        end
        function res = get.Rs(obj)
            res = getParameter(obj,'Rs');
        end
        function obj = set.Rsc(obj,val)
            obj = setParameter(obj,'Rsc',val,0,'real');
        end
        function res = get.Rsc(obj)
            res = getParameter(obj,'Rsc');
        end
        function obj = set.Rsh(obj,val)
            obj = setParameter(obj,'Rsh',val,0,'real');
        end
        function res = get.Rsh(obj)
            res = getParameter(obj,'Rsh');
        end
        function obj = set.Rss(obj,val)
            obj = setParameter(obj,'Rss',val,0,'real');
        end
        function res = get.Rss(obj)
            res = getParameter(obj,'Rss');
        end
        function obj = set.Sa0(obj,val)
            obj = setParameter(obj,'Sa0',val,0,'real');
        end
        function res = get.Sa0(obj)
            res = getParameter(obj,'Sa0');
        end
        function obj = set.Sb0(obj,val)
            obj = setParameter(obj,'Sb0',val,0,'real');
        end
        function res = get.Sb0(obj)
            res = getParameter(obj,'Sb0');
        end
        function obj = set.Sc(obj,val)
            obj = setParameter(obj,'Sc',val,0,'real');
        end
        function res = get.Sc(obj)
            res = getParameter(obj,'Sc');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
        function obj = set.Steta0(obj,val)
            obj = setParameter(obj,'Steta0',val,0,'real');
        end
        function res = get.Steta0(obj)
            res = getParameter(obj,'Steta0');
        end
        function obj = set.Stimod(obj,val)
            obj = setParameter(obj,'Stimod',val,0,'integer');
        end
        function res = get.Stimod(obj)
            res = getParameter(obj,'Stimod');
        end
        function obj = set.Stk2(obj,val)
            obj = setParameter(obj,'Stk2',val,0,'real');
        end
        function res = get.Stk2(obj)
            res = getParameter(obj,'Stk2');
        end
        function obj = set.Tcj(obj,val)
            obj = setParameter(obj,'Tcj',val,0,'real');
        end
        function res = get.Tcj(obj)
            res = getParameter(obj,'Tcj');
        end
        function obj = set.Tcjsw(obj,val)
            obj = setParameter(obj,'Tcjsw',val,0,'real');
        end
        function res = get.Tcjsw(obj)
            res = getParameter(obj,'Tcjsw');
        end
        function obj = set.Tcjswg(obj,val)
            obj = setParameter(obj,'Tcjswg',val,0,'real');
        end
        function res = get.Tcjswg(obj)
            res = getParameter(obj,'Tcjswg');
        end
        function obj = set.Theta(obj,val)
            obj = setParameter(obj,'Theta',val,0,'real');
        end
        function res = get.Theta(obj)
            res = getParameter(obj,'Theta');
        end
        function obj = set.Tku0(obj,val)
            obj = setParameter(obj,'Tku0',val,0,'real');
        end
        function res = get.Tku0(obj)
            res = getParameter(obj,'Tku0');
        end
        function obj = set.Tlev(obj,val)
            obj = setParameter(obj,'Tlev',val,0,'integer');
        end
        function res = get.Tlev(obj)
            res = getParameter(obj,'Tlev');
        end
        function obj = set.Tlevc(obj,val)
            obj = setParameter(obj,'Tlevc',val,0,'integer');
        end
        function res = get.Tlevc(obj)
            res = getParameter(obj,'Tlevc');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Tox(obj,val)
            obj = setParameter(obj,'Tox',val,0,'real');
        end
        function res = get.Tox(obj)
            res = getParameter(obj,'Tox');
        end
        function obj = set.Toxm(obj,val)
            obj = setParameter(obj,'Toxm',val,0,'real');
        end
        function res = get.Toxm(obj)
            res = getParameter(obj,'Toxm');
        end
        function obj = set.Tpb(obj,val)
            obj = setParameter(obj,'Tpb',val,0,'real');
        end
        function res = get.Tpb(obj)
            res = getParameter(obj,'Tpb');
        end
        function obj = set.Tpbsw(obj,val)
            obj = setParameter(obj,'Tpbsw',val,0,'real');
        end
        function res = get.Tpbsw(obj)
            res = getParameter(obj,'Tpbsw');
        end
        function obj = set.Tpbswg(obj,val)
            obj = setParameter(obj,'Tpbswg',val,0,'real');
        end
        function res = get.Tpbswg(obj)
            res = getParameter(obj,'Tpbswg');
        end
        function obj = set.Tpg(obj,val)
            obj = setParameter(obj,'Tpg',val,0,'real');
        end
        function res = get.Tpg(obj)
            res = getParameter(obj,'Tpg');
        end
        function obj = set.Trd(obj,val)
            obj = setParameter(obj,'Trd',val,0,'real');
        end
        function res = get.Trd(obj)
            res = getParameter(obj,'Trd');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Trs(obj,val)
            obj = setParameter(obj,'Trs',val,0,'real');
        end
        function res = get.Trs(obj)
            res = getParameter(obj,'Trs');
        end
        function obj = set.Tt(obj,val)
            obj = setParameter(obj,'Tt',val,0,'real');
        end
        function res = get.Tt(obj)
            res = getParameter(obj,'Tt');
        end
        function obj = set.U0(obj,val)
            obj = setParameter(obj,'U0',val,0,'real');
        end
        function res = get.U0(obj)
            res = getParameter(obj,'U0');
        end
        function obj = set.U1(obj,val)
            obj = setParameter(obj,'U1',val,0,'real');
        end
        function res = get.U1(obj)
            res = getParameter(obj,'U1');
        end
        function obj = set.Ua(obj,val)
            obj = setParameter(obj,'Ua',val,0,'real');
        end
        function res = get.Ua(obj)
            res = getParameter(obj,'Ua');
        end
        function obj = set.Ua1(obj,val)
            obj = setParameter(obj,'Ua1',val,0,'real');
        end
        function res = get.Ua1(obj)
            res = getParameter(obj,'Ua1');
        end
        function obj = set.Ua_dm(obj,val)
            obj = setParameter(obj,'Ua_dm',val,0,'real');
        end
        function res = get.Ua_dm(obj)
            res = getParameter(obj,'Ua_dm');
        end
        function obj = set.Ub(obj,val)
            obj = setParameter(obj,'Ub',val,0,'real');
        end
        function res = get.Ub(obj)
            res = getParameter(obj,'Ub');
        end
        function obj = set.Ub0(obj,val)
            obj = setParameter(obj,'Ub0',val,0,'real');
        end
        function res = get.Ub0(obj)
            res = getParameter(obj,'Ub0');
        end
        function obj = set.Ub1(obj,val)
            obj = setParameter(obj,'Ub1',val,0,'real');
        end
        function res = get.Ub1(obj)
            res = getParameter(obj,'Ub1');
        end
        function obj = set.Ub_dm(obj,val)
            obj = setParameter(obj,'Ub_dm',val,0,'real');
        end
        function res = get.Ub_dm(obj)
            res = getParameter(obj,'Ub_dm');
        end
        function obj = set.Ubb(obj,val)
            obj = setParameter(obj,'Ubb',val,0,'real');
        end
        function res = get.Ubb(obj)
            res = getParameter(obj,'Ubb');
        end
        function obj = set.Uc(obj,val)
            obj = setParameter(obj,'Uc',val,0,'real');
        end
        function res = get.Uc(obj)
            res = getParameter(obj,'Uc');
        end
        function obj = set.Uc1(obj,val)
            obj = setParameter(obj,'Uc1',val,0,'real');
        end
        function res = get.Uc1(obj)
            res = getParameter(obj,'Uc1');
        end
        function obj = set.Ucrit(obj,val)
            obj = setParameter(obj,'Ucrit',val,0,'real');
        end
        function res = get.Ucrit(obj)
            res = getParameter(obj,'Ucrit');
        end
        function obj = set.Uexp(obj,val)
            obj = setParameter(obj,'Uexp',val,0,'real');
        end
        function res = get.Uexp(obj)
            res = getParameter(obj,'Uexp');
        end
        function obj = set.Uo(obj,val)
            obj = setParameter(obj,'Uo',val,0,'real');
        end
        function res = get.Uo(obj)
            res = getParameter(obj,'Uo');
        end
        function obj = set.Uo_dm(obj,val)
            obj = setParameter(obj,'Uo_dm',val,0,'real');
        end
        function res = get.Uo_dm(obj)
            res = getParameter(obj,'Uo_dm');
        end
        function obj = set.Ute(obj,val)
            obj = setParameter(obj,'Ute',val,0,'real');
        end
        function res = get.Ute(obj)
            res = getParameter(obj,'Ute');
        end
        function obj = set.Uto(obj,val)
            obj = setParameter(obj,'Uto',val,0,'real');
        end
        function res = get.Uto(obj)
            res = getParameter(obj,'Uto');
        end
        function obj = set.Vbb(obj,val)
            obj = setParameter(obj,'Vbb',val,0,'real');
        end
        function res = get.Vbb(obj)
            res = getParameter(obj,'Vbb');
        end
        function obj = set.Vbd_max(obj,val)
            obj = setParameter(obj,'Vbd_max',val,0,'real');
        end
        function res = get.Vbd_max(obj)
            res = getParameter(obj,'Vbd_max');
        end
        function obj = set.Vbs_max(obj,val)
            obj = setParameter(obj,'Vbs_max',val,0,'real');
        end
        function res = get.Vbs_max(obj)
            res = getParameter(obj,'Vbs_max');
        end
        function obj = set.Vbx(obj,val)
            obj = setParameter(obj,'Vbx',val,0,'real');
        end
        function res = get.Vbx(obj)
            res = getParameter(obj,'Vbx');
        end
        function obj = set.Vdd(obj,val)
            obj = setParameter(obj,'Vdd',val,0,'real');
        end
        function res = get.Vdd(obj)
            res = getParameter(obj,'Vdd');
        end
        function obj = set.Vds_max(obj,val)
            obj = setParameter(obj,'Vds_max',val,0,'real');
        end
        function res = get.Vds_max(obj)
            res = getParameter(obj,'Vds_max');
        end
        function obj = set.Vdsmin(obj,val)
            obj = setParameter(obj,'Vdsmin',val,0,'real');
        end
        function res = get.Vdsmin(obj)
            res = getParameter(obj,'Vdsmin');
        end
        function obj = set.Version(obj,val)
            obj = setParameter(obj,'Version',val,0,'real');
        end
        function res = get.Version(obj)
            res = getParameter(obj,'Version');
        end
        function obj = set.Vfb(obj,val)
            obj = setParameter(obj,'Vfb',val,0,'real');
        end
        function res = get.Vfb(obj)
            res = getParameter(obj,'Vfb');
        end
        function obj = set.Vfb_dm(obj,val)
            obj = setParameter(obj,'Vfb_dm',val,0,'real');
        end
        function res = get.Vfb_dm(obj)
            res = getParameter(obj,'Vfb_dm');
        end
        function obj = set.Vfbcv(obj,val)
            obj = setParameter(obj,'Vfbcv',val,0,'real');
        end
        function res = get.Vfbcv(obj)
            res = getParameter(obj,'Vfbcv');
        end
        function obj = set.Vfbflag(obj,val)
            obj = setParameter(obj,'Vfbflag',val,0,'integer');
        end
        function res = get.Vfbflag(obj)
            res = getParameter(obj,'Vfbflag');
        end
        function obj = set.Vgd_max(obj,val)
            obj = setParameter(obj,'Vgd_max',val,0,'real');
        end
        function res = get.Vgd_max(obj)
            res = getParameter(obj,'Vgd_max');
        end
        function obj = set.Vgg(obj,val)
            obj = setParameter(obj,'Vgg',val,0,'real');
        end
        function res = get.Vgg(obj)
            res = getParameter(obj,'Vgg');
        end
        function obj = set.Vghigh(obj,val)
            obj = setParameter(obj,'Vghigh',val,0,'real');
        end
        function res = get.Vghigh(obj)
            res = getParameter(obj,'Vghigh');
        end
        function obj = set.Vglow(obj,val)
            obj = setParameter(obj,'Vglow',val,0,'real');
        end
        function res = get.Vglow(obj)
            res = getParameter(obj,'Vglow');
        end
        function obj = set.Vgs_max(obj,val)
            obj = setParameter(obj,'Vgs_max',val,0,'real');
        end
        function res = get.Vgs_max(obj)
            res = getParameter(obj,'Vgs_max');
        end
        function obj = set.Vmax(obj,val)
            obj = setParameter(obj,'Vmax',val,0,'real');
        end
        function res = get.Vmax(obj)
            res = getParameter(obj,'Vmax');
        end
        function obj = set.Vof0(obj,val)
            obj = setParameter(obj,'Vof0',val,0,'real');
        end
        function res = get.Vof0(obj)
            res = getParameter(obj,'Vof0');
        end
        function obj = set.Vofb(obj,val)
            obj = setParameter(obj,'Vofb',val,0,'real');
        end
        function res = get.Vofb(obj)
            res = getParameter(obj,'Vofb');
        end
        function obj = set.Vofd(obj,val)
            obj = setParameter(obj,'Vofd',val,0,'real');
        end
        function res = get.Vofd(obj)
            res = getParameter(obj,'Vofd');
        end
        function obj = set.Voff(obj,val)
            obj = setParameter(obj,'Voff',val,0,'real');
        end
        function res = get.Voff(obj)
            res = getParameter(obj,'Voff');
        end
        function obj = set.Voffcv(obj,val)
            obj = setParameter(obj,'Voffcv',val,0,'real');
        end
        function res = get.Voffcv(obj)
            res = getParameter(obj,'Voffcv');
        end
        function obj = set.Vsat(obj,val)
            obj = setParameter(obj,'Vsat',val,0,'real');
        end
        function res = get.Vsat(obj)
            res = getParameter(obj,'Vsat');
        end
        function obj = set.Vth0(obj,val)
            obj = setParameter(obj,'Vth0',val,0,'real');
        end
        function res = get.Vth0(obj)
            res = getParameter(obj,'Vth0');
        end
        function obj = set.Vth_dm(obj,val)
            obj = setParameter(obj,'Vth_dm',val,0,'real');
        end
        function res = get.Vth_dm(obj)
            res = getParameter(obj,'Vth_dm');
        end
        function obj = set.Vto(obj,val)
            obj = setParameter(obj,'Vto',val,0,'real');
        end
        function res = get.Vto(obj)
            res = getParameter(obj,'Vto');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.W0(obj,val)
            obj = setParameter(obj,'W0',val,0,'real');
        end
        function res = get.W0(obj)
            res = getParameter(obj,'W0');
        end
        function obj = set.Wa0(obj,val)
            obj = setParameter(obj,'Wa0',val,0,'real');
        end
        function res = get.Wa0(obj)
            res = getParameter(obj,'Wa0');
        end
        function obj = set.Wa1(obj,val)
            obj = setParameter(obj,'Wa1',val,0,'real');
        end
        function res = get.Wa1(obj)
            res = getParameter(obj,'Wa1');
        end
        function obj = set.Wa2(obj,val)
            obj = setParameter(obj,'Wa2',val,0,'real');
        end
        function res = get.Wa2(obj)
            res = getParameter(obj,'Wa2');
        end
        function obj = set.Wacde(obj,val)
            obj = setParameter(obj,'Wacde',val,0,'real');
        end
        function res = get.Wacde(obj)
            res = getParameter(obj,'Wacde');
        end
        function obj = set.Wags(obj,val)
            obj = setParameter(obj,'Wags',val,0,'real');
        end
        function res = get.Wags(obj)
            res = getParameter(obj,'Wags');
        end
        function obj = set.Wai0(obj,val)
            obj = setParameter(obj,'Wai0',val,0,'real');
        end
        function res = get.Wai0(obj)
            res = getParameter(obj,'Wai0');
        end
        function obj = set.Waib(obj,val)
            obj = setParameter(obj,'Waib',val,0,'real');
        end
        function res = get.Waib(obj)
            res = getParameter(obj,'Waib');
        end
        function obj = set.Walpha0(obj,val)
            obj = setParameter(obj,'Walpha0',val,0,'real');
        end
        function res = get.Walpha0(obj)
            res = getParameter(obj,'Walpha0');
        end
        function obj = set.Walpha1(obj,val)
            obj = setParameter(obj,'Walpha1',val,0,'real');
        end
        function res = get.Walpha1(obj)
            res = getParameter(obj,'Walpha1');
        end
        function obj = set.Wat(obj,val)
            obj = setParameter(obj,'Wat',val,0,'real');
        end
        function res = get.Wat(obj)
            res = getParameter(obj,'Wat');
        end
        function obj = set.Wb0(obj,val)
            obj = setParameter(obj,'Wb0',val,0,'real');
        end
        function res = get.Wb0(obj)
            res = getParameter(obj,'Wb0');
        end
        function obj = set.Wb1(obj,val)
            obj = setParameter(obj,'Wb1',val,0,'real');
        end
        function res = get.Wb1(obj)
            res = getParameter(obj,'Wb1');
        end
        function obj = set.Wbeta0(obj,val)
            obj = setParameter(obj,'Wbeta0',val,0,'real');
        end
        function res = get.Wbeta0(obj)
            res = getParameter(obj,'Wbeta0');
        end
        function obj = set.Wbi0(obj,val)
            obj = setParameter(obj,'Wbi0',val,0,'real');
        end
        function res = get.Wbi0(obj)
            res = getParameter(obj,'Wbi0');
        end
        function obj = set.Wbib(obj,val)
            obj = setParameter(obj,'Wbib',val,0,'real');
        end
        function res = get.Wbib(obj)
            res = getParameter(obj,'Wbib');
        end
        function obj = set.Wcdsc(obj,val)
            obj = setParameter(obj,'Wcdsc',val,0,'real');
        end
        function res = get.Wcdsc(obj)
            res = getParameter(obj,'Wcdsc');
        end
        function obj = set.Wcdscb(obj,val)
            obj = setParameter(obj,'Wcdscb',val,0,'real');
        end
        function res = get.Wcdscb(obj)
            res = getParameter(obj,'Wcdscb');
        end
        function obj = set.Wcdscd(obj,val)
            obj = setParameter(obj,'Wcdscd',val,0,'real');
        end
        function res = get.Wcdscd(obj)
            res = getParameter(obj,'Wcdscd');
        end
        function obj = set.Wcf(obj,val)
            obj = setParameter(obj,'Wcf',val,0,'real');
        end
        function res = get.Wcf(obj)
            res = getParameter(obj,'Wcf');
        end
        function obj = set.Wcgdl(obj,val)
            obj = setParameter(obj,'Wcgdl',val,0,'real');
        end
        function res = get.Wcgdl(obj)
            res = getParameter(obj,'Wcgdl');
        end
        function obj = set.Wcgsl(obj,val)
            obj = setParameter(obj,'Wcgsl',val,0,'real');
        end
        function res = get.Wcgsl(obj)
            res = getParameter(obj,'Wcgsl');
        end
        function obj = set.Wcit(obj,val)
            obj = setParameter(obj,'Wcit',val,0,'real');
        end
        function res = get.Wcit(obj)
            res = getParameter(obj,'Wcit');
        end
        function obj = set.Wckappa(obj,val)
            obj = setParameter(obj,'Wckappa',val,0,'real');
        end
        function res = get.Wckappa(obj)
            res = getParameter(obj,'Wckappa');
        end
        function obj = set.Wclc(obj,val)
            obj = setParameter(obj,'Wclc',val,0,'real');
        end
        function res = get.Wclc(obj)
            res = getParameter(obj,'Wclc');
        end
        function obj = set.Wcle(obj,val)
            obj = setParameter(obj,'Wcle',val,0,'real');
        end
        function res = get.Wcle(obj)
            res = getParameter(obj,'Wcle');
        end
        function obj = set.Wdelta(obj,val)
            obj = setParameter(obj,'Wdelta',val,0,'real');
        end
        function res = get.Wdelta(obj)
            res = getParameter(obj,'Wdelta');
        end
        function obj = set.Wdf(obj,val)
            obj = setParameter(obj,'Wdf',val,0,'real');
        end
        function res = get.Wdf(obj)
            res = getParameter(obj,'Wdf');
        end
        function obj = set.Wdrout(obj,val)
            obj = setParameter(obj,'Wdrout',val,0,'real');
        end
        function res = get.Wdrout(obj)
            res = getParameter(obj,'Wdrout');
        end
        function obj = set.Wdsub(obj,val)
            obj = setParameter(obj,'Wdsub',val,0,'real');
        end
        function res = get.Wdsub(obj)
            res = getParameter(obj,'Wdsub');
        end
        function obj = set.Wdvt0(obj,val)
            obj = setParameter(obj,'Wdvt0',val,0,'real');
        end
        function res = get.Wdvt0(obj)
            res = getParameter(obj,'Wdvt0');
        end
        function obj = set.Wdvt0w(obj,val)
            obj = setParameter(obj,'Wdvt0w',val,0,'real');
        end
        function res = get.Wdvt0w(obj)
            res = getParameter(obj,'Wdvt0w');
        end
        function obj = set.Wdvt1(obj,val)
            obj = setParameter(obj,'Wdvt1',val,0,'real');
        end
        function res = get.Wdvt1(obj)
            res = getParameter(obj,'Wdvt1');
        end
        function obj = set.Wdvt1w(obj,val)
            obj = setParameter(obj,'Wdvt1w',val,0,'real');
        end
        function res = get.Wdvt1w(obj)
            res = getParameter(obj,'Wdvt1w');
        end
        function obj = set.Wdvt2(obj,val)
            obj = setParameter(obj,'Wdvt2',val,0,'real');
        end
        function res = get.Wdvt2(obj)
            res = getParameter(obj,'Wdvt2');
        end
        function obj = set.Wdvt2w(obj,val)
            obj = setParameter(obj,'Wdvt2w',val,0,'real');
        end
        function res = get.Wdvt2w(obj)
            res = getParameter(obj,'Wdvt2w');
        end
        function obj = set.Wdwb(obj,val)
            obj = setParameter(obj,'Wdwb',val,0,'real');
        end
        function res = get.Wdwb(obj)
            res = getParameter(obj,'Wdwb');
        end
        function obj = set.Wdwg(obj,val)
            obj = setParameter(obj,'Wdwg',val,0,'real');
        end
        function res = get.Wdwg(obj)
            res = getParameter(obj,'Wdwg');
        end
        function obj = set.Welm(obj,val)
            obj = setParameter(obj,'Welm',val,0,'real');
        end
        function res = get.Welm(obj)
            res = getParameter(obj,'Welm');
        end
        function obj = set.Weta(obj,val)
            obj = setParameter(obj,'Weta',val,0,'real');
        end
        function res = get.Weta(obj)
            res = getParameter(obj,'Weta');
        end
        function obj = set.Wetad(obj,val)
            obj = setParameter(obj,'Wetad',val,0,'real');
        end
        function res = get.Wetad(obj)
            res = getParameter(obj,'Wetad');
        end
        function obj = set.Wgamma1(obj,val)
            obj = setParameter(obj,'Wgamma1',val,0,'real');
        end
        function res = get.Wgamma1(obj)
            res = getParameter(obj,'Wgamma1');
        end
        function obj = set.Wgamma2(obj,val)
            obj = setParameter(obj,'Wgamma2',val,0,'real');
        end
        function res = get.Wgamma2(obj)
            res = getParameter(obj,'Wgamma2');
        end
        function obj = set.Wk1(obj,val)
            obj = setParameter(obj,'Wk1',val,0,'real');
        end
        function res = get.Wk1(obj)
            res = getParameter(obj,'Wk1');
        end
        function obj = set.Wk2(obj,val)
            obj = setParameter(obj,'Wk2',val,0,'real');
        end
        function res = get.Wk2(obj)
            res = getParameter(obj,'Wk2');
        end
        function obj = set.Wk3(obj,val)
            obj = setParameter(obj,'Wk3',val,0,'real');
        end
        function res = get.Wk3(obj)
            res = getParameter(obj,'Wk3');
        end
        function obj = set.Wk3b(obj,val)
            obj = setParameter(obj,'Wk3b',val,0,'real');
        end
        function res = get.Wk3b(obj)
            res = getParameter(obj,'Wk3b');
        end
        function obj = set.Wketa(obj,val)
            obj = setParameter(obj,'Wketa',val,0,'real');
        end
        function res = get.Wketa(obj)
            res = getParameter(obj,'Wketa');
        end
        function obj = set.Wkt1(obj,val)
            obj = setParameter(obj,'Wkt1',val,0,'real');
        end
        function res = get.Wkt1(obj)
            res = getParameter(obj,'Wkt1');
        end
        function obj = set.Wkt1l(obj,val)
            obj = setParameter(obj,'Wkt1l',val,0,'real');
        end
        function res = get.Wkt1l(obj)
            res = getParameter(obj,'Wkt1l');
        end
        function obj = set.Wkt2(obj,val)
            obj = setParameter(obj,'Wkt2',val,0,'real');
        end
        function res = get.Wkt2(obj)
            res = getParameter(obj,'Wkt2');
        end
        function obj = set.Wku0(obj,val)
            obj = setParameter(obj,'Wku0',val,0,'real');
        end
        function res = get.Wku0(obj)
            res = getParameter(obj,'Wku0');
        end
        function obj = set.Wkvth0(obj,val)
            obj = setParameter(obj,'Wkvth0',val,0,'real');
        end
        function res = get.Wkvth0(obj)
            res = getParameter(obj,'Wkvth0');
        end
        function obj = set.Wl(obj,val)
            obj = setParameter(obj,'Wl',val,0,'real');
        end
        function res = get.Wl(obj)
            res = getParameter(obj,'Wl');
        end
        function obj = set.Wlc(obj,val)
            obj = setParameter(obj,'Wlc',val,0,'real');
        end
        function res = get.Wlc(obj)
            res = getParameter(obj,'Wlc');
        end
        function obj = set.Wln(obj,val)
            obj = setParameter(obj,'Wln',val,0,'real');
        end
        function res = get.Wln(obj)
            res = getParameter(obj,'Wln');
        end
        function obj = set.Wlod(obj,val)
            obj = setParameter(obj,'Wlod',val,0,'real');
        end
        function res = get.Wlod(obj)
            res = getParameter(obj,'Wlod');
        end
        function obj = set.Wlodku0(obj,val)
            obj = setParameter(obj,'Wlodku0',val,0,'real');
        end
        function res = get.Wlodku0(obj)
            res = getParameter(obj,'Wlodku0');
        end
        function obj = set.Wlodvth(obj,val)
            obj = setParameter(obj,'Wlodvth',val,0,'real');
        end
        function res = get.Wlodvth(obj)
            res = getParameter(obj,'Wlodvth');
        end
        function obj = set.Wmax(obj,val)
            obj = setParameter(obj,'Wmax',val,0,'real');
        end
        function res = get.Wmax(obj)
            res = getParameter(obj,'Wmax');
        end
        function obj = set.Wmin(obj,val)
            obj = setParameter(obj,'Wmin',val,0,'real');
        end
        function res = get.Wmin(obj)
            res = getParameter(obj,'Wmin');
        end
        function obj = set.Wmlt(obj,val)
            obj = setParameter(obj,'Wmlt',val,0,'real');
        end
        function res = get.Wmlt(obj)
            res = getParameter(obj,'Wmlt');
        end
        function obj = set.Wmoin(obj,val)
            obj = setParameter(obj,'Wmoin',val,0,'real');
        end
        function res = get.Wmoin(obj)
            res = getParameter(obj,'Wmoin');
        end
        function obj = set.Wmu20(obj,val)
            obj = setParameter(obj,'Wmu20',val,0,'real');
        end
        function res = get.Wmu20(obj)
            res = getParameter(obj,'Wmu20');
        end
        function obj = set.Wmu2b(obj,val)
            obj = setParameter(obj,'Wmu2b',val,0,'real');
        end
        function res = get.Wmu2b(obj)
            res = getParameter(obj,'Wmu2b');
        end
        function obj = set.Wmu2g(obj,val)
            obj = setParameter(obj,'Wmu2g',val,0,'real');
        end
        function res = get.Wmu2g(obj)
            res = getParameter(obj,'Wmu2g');
        end
        function obj = set.Wmu30(obj,val)
            obj = setParameter(obj,'Wmu30',val,0,'real');
        end
        function res = get.Wmu30(obj)
            res = getParameter(obj,'Wmu30');
        end
        function obj = set.Wmu3b(obj,val)
            obj = setParameter(obj,'Wmu3b',val,0,'real');
        end
        function res = get.Wmu3b(obj)
            res = getParameter(obj,'Wmu3b');
        end
        function obj = set.Wmu3g(obj,val)
            obj = setParameter(obj,'Wmu3g',val,0,'real');
        end
        function res = get.Wmu3g(obj)
            res = getParameter(obj,'Wmu3g');
        end
        function obj = set.Wmu40(obj,val)
            obj = setParameter(obj,'Wmu40',val,0,'real');
        end
        function res = get.Wmu40(obj)
            res = getParameter(obj,'Wmu40');
        end
        function obj = set.Wmu4b(obj,val)
            obj = setParameter(obj,'Wmu4b',val,0,'real');
        end
        function res = get.Wmu4b(obj)
            res = getParameter(obj,'Wmu4b');
        end
        function obj = set.Wmu4g(obj,val)
            obj = setParameter(obj,'Wmu4g',val,0,'real');
        end
        function res = get.Wmu4g(obj)
            res = getParameter(obj,'Wmu4g');
        end
        function obj = set.Wmus(obj,val)
            obj = setParameter(obj,'Wmus',val,0,'real');
        end
        function res = get.Wmus(obj)
            res = getParameter(obj,'Wmus');
        end
        function obj = set.Wn0(obj,val)
            obj = setParameter(obj,'Wn0',val,0,'real');
        end
        function res = get.Wn0(obj)
            res = getParameter(obj,'Wn0');
        end
        function obj = set.Wnb(obj,val)
            obj = setParameter(obj,'Wnb',val,0,'real');
        end
        function res = get.Wnb(obj)
            res = getParameter(obj,'Wnb');
        end
        function obj = set.Wnch(obj,val)
            obj = setParameter(obj,'Wnch',val,0,'real');
        end
        function res = get.Wnch(obj)
            res = getParameter(obj,'Wnch');
        end
        function obj = set.Wnd(obj,val)
            obj = setParameter(obj,'Wnd',val,0,'real');
        end
        function res = get.Wnd(obj)
            res = getParameter(obj,'Wnd');
        end
        function obj = set.Wnfactor(obj,val)
            obj = setParameter(obj,'Wnfactor',val,0,'real');
        end
        function res = get.Wnfactor(obj)
            res = getParameter(obj,'Wnfactor');
        end
        function obj = set.Wngate(obj,val)
            obj = setParameter(obj,'Wngate',val,0,'real');
        end
        function res = get.Wngate(obj)
            res = getParameter(obj,'Wngate');
        end
        function obj = set.Wnlx(obj,val)
            obj = setParameter(obj,'Wnlx',val,0,'real');
        end
        function res = get.Wnlx(obj)
            res = getParameter(obj,'Wnlx');
        end
        function obj = set.Wnoff(obj,val)
            obj = setParameter(obj,'Wnoff',val,0,'real');
        end
        function res = get.Wnoff(obj)
            res = getParameter(obj,'Wnoff');
        end
        function obj = set.Wnsub(obj,val)
            obj = setParameter(obj,'Wnsub',val,0,'real');
        end
        function res = get.Wnsub(obj)
            res = getParameter(obj,'Wnsub');
        end
        function obj = set.Wpclm(obj,val)
            obj = setParameter(obj,'Wpclm',val,0,'real');
        end
        function res = get.Wpclm(obj)
            res = getParameter(obj,'Wpclm');
        end
        function obj = set.Wpdiblc1(obj,val)
            obj = setParameter(obj,'Wpdiblc1',val,0,'real');
        end
        function res = get.Wpdiblc1(obj)
            res = getParameter(obj,'Wpdiblc1');
        end
        function obj = set.Wpdiblc2(obj,val)
            obj = setParameter(obj,'Wpdiblc2',val,0,'real');
        end
        function res = get.Wpdiblc2(obj)
            res = getParameter(obj,'Wpdiblc2');
        end
        function obj = set.Wpdiblcb(obj,val)
            obj = setParameter(obj,'Wpdiblcb',val,0,'real');
        end
        function res = get.Wpdiblcb(obj)
            res = getParameter(obj,'Wpdiblcb');
        end
        function obj = set.Wphi(obj,val)
            obj = setParameter(obj,'Wphi',val,0,'real');
        end
        function res = get.Wphi(obj)
            res = getParameter(obj,'Wphi');
        end
        function obj = set.Wprt(obj,val)
            obj = setParameter(obj,'Wprt',val,0,'real');
        end
        function res = get.Wprt(obj)
            res = getParameter(obj,'Wprt');
        end
        function obj = set.Wprwb(obj,val)
            obj = setParameter(obj,'Wprwb',val,0,'real');
        end
        function res = get.Wprwb(obj)
            res = getParameter(obj,'Wprwb');
        end
        function obj = set.Wprwg(obj,val)
            obj = setParameter(obj,'Wprwg',val,0,'real');
        end
        function res = get.Wprwg(obj)
            res = getParameter(obj,'Wprwg');
        end
        function obj = set.Wpscbe1(obj,val)
            obj = setParameter(obj,'Wpscbe1',val,0,'real');
        end
        function res = get.Wpscbe1(obj)
            res = getParameter(obj,'Wpscbe1');
        end
        function obj = set.Wpscbe2(obj,val)
            obj = setParameter(obj,'Wpscbe2',val,0,'real');
        end
        function res = get.Wpscbe2(obj)
            res = getParameter(obj,'Wpscbe2');
        end
        function obj = set.Wpvag(obj,val)
            obj = setParameter(obj,'Wpvag',val,0,'real');
        end
        function res = get.Wpvag(obj)
            res = getParameter(obj,'Wpvag');
        end
        function obj = set.Wr(obj,val)
            obj = setParameter(obj,'Wr',val,0,'real');
        end
        function res = get.Wr(obj)
            res = getParameter(obj,'Wr');
        end
        function obj = set.Wrdsw(obj,val)
            obj = setParameter(obj,'Wrdsw',val,0,'real');
        end
        function res = get.Wrdsw(obj)
            res = getParameter(obj,'Wrdsw');
        end
        function obj = set.Wu0(obj,val)
            obj = setParameter(obj,'Wu0',val,0,'real');
        end
        function res = get.Wu0(obj)
            res = getParameter(obj,'Wu0');
        end
        function obj = set.Wu1(obj,val)
            obj = setParameter(obj,'Wu1',val,0,'real');
        end
        function res = get.Wu1(obj)
            res = getParameter(obj,'Wu1');
        end
        function obj = set.Wua(obj,val)
            obj = setParameter(obj,'Wua',val,0,'real');
        end
        function res = get.Wua(obj)
            res = getParameter(obj,'Wua');
        end
        function obj = set.Wua1(obj,val)
            obj = setParameter(obj,'Wua1',val,0,'real');
        end
        function res = get.Wua1(obj)
            res = getParameter(obj,'Wua1');
        end
        function obj = set.Wub(obj,val)
            obj = setParameter(obj,'Wub',val,0,'real');
        end
        function res = get.Wub(obj)
            res = getParameter(obj,'Wub');
        end
        function obj = set.Wub0(obj,val)
            obj = setParameter(obj,'Wub0',val,0,'real');
        end
        function res = get.Wub0(obj)
            res = getParameter(obj,'Wub0');
        end
        function obj = set.Wub1(obj,val)
            obj = setParameter(obj,'Wub1',val,0,'real');
        end
        function res = get.Wub1(obj)
            res = getParameter(obj,'Wub1');
        end
        function obj = set.Wubb(obj,val)
            obj = setParameter(obj,'Wubb',val,0,'real');
        end
        function res = get.Wubb(obj)
            res = getParameter(obj,'Wubb');
        end
        function obj = set.Wuc(obj,val)
            obj = setParameter(obj,'Wuc',val,0,'real');
        end
        function res = get.Wuc(obj)
            res = getParameter(obj,'Wuc');
        end
        function obj = set.Wuc1(obj,val)
            obj = setParameter(obj,'Wuc1',val,0,'real');
        end
        function res = get.Wuc1(obj)
            res = getParameter(obj,'Wuc1');
        end
        function obj = set.Wute(obj,val)
            obj = setParameter(obj,'Wute',val,0,'real');
        end
        function res = get.Wute(obj)
            res = getParameter(obj,'Wute');
        end
        function obj = set.Wvbm(obj,val)
            obj = setParameter(obj,'Wvbm',val,0,'real');
        end
        function res = get.Wvbm(obj)
            res = getParameter(obj,'Wvbm');
        end
        function obj = set.Wvbx(obj,val)
            obj = setParameter(obj,'Wvbx',val,0,'real');
        end
        function res = get.Wvbx(obj)
            res = getParameter(obj,'Wvbx');
        end
        function obj = set.Wvfb(obj,val)
            obj = setParameter(obj,'Wvfb',val,0,'real');
        end
        function res = get.Wvfb(obj)
            res = getParameter(obj,'Wvfb');
        end
        function obj = set.Wvfbcv(obj,val)
            obj = setParameter(obj,'Wvfbcv',val,0,'real');
        end
        function res = get.Wvfbcv(obj)
            res = getParameter(obj,'Wvfbcv');
        end
        function obj = set.Wvghigh(obj,val)
            obj = setParameter(obj,'Wvghigh',val,0,'real');
        end
        function res = get.Wvghigh(obj)
            res = getParameter(obj,'Wvghigh');
        end
        function obj = set.Wvglow(obj,val)
            obj = setParameter(obj,'Wvglow',val,0,'real');
        end
        function res = get.Wvglow(obj)
            res = getParameter(obj,'Wvglow');
        end
        function obj = set.Wvof0(obj,val)
            obj = setParameter(obj,'Wvof0',val,0,'real');
        end
        function res = get.Wvof0(obj)
            res = getParameter(obj,'Wvof0');
        end
        function obj = set.Wvofb(obj,val)
            obj = setParameter(obj,'Wvofb',val,0,'real');
        end
        function res = get.Wvofb(obj)
            res = getParameter(obj,'Wvofb');
        end
        function obj = set.Wvofd(obj,val)
            obj = setParameter(obj,'Wvofd',val,0,'real');
        end
        function res = get.Wvofd(obj)
            res = getParameter(obj,'Wvofd');
        end
        function obj = set.Wvoff(obj,val)
            obj = setParameter(obj,'Wvoff',val,0,'real');
        end
        function res = get.Wvoff(obj)
            res = getParameter(obj,'Wvoff');
        end
        function obj = set.Wvoffcv(obj,val)
            obj = setParameter(obj,'Wvoffcv',val,0,'real');
        end
        function res = get.Wvoffcv(obj)
            res = getParameter(obj,'Wvoffcv');
        end
        function obj = set.Wvsat(obj,val)
            obj = setParameter(obj,'Wvsat',val,0,'real');
        end
        function res = get.Wvsat(obj)
            res = getParameter(obj,'Wvsat');
        end
        function obj = set.Wvth0(obj,val)
            obj = setParameter(obj,'Wvth0',val,0,'real');
        end
        function res = get.Wvth0(obj)
            res = getParameter(obj,'Wvth0');
        end
        function obj = set.Ww(obj,val)
            obj = setParameter(obj,'Ww',val,0,'real');
        end
        function res = get.Ww(obj)
            res = getParameter(obj,'Ww');
        end
        function obj = set.Ww0(obj,val)
            obj = setParameter(obj,'Ww0',val,0,'real');
        end
        function res = get.Ww0(obj)
            res = getParameter(obj,'Ww0');
        end
        function obj = set.Wwc(obj,val)
            obj = setParameter(obj,'Wwc',val,0,'real');
        end
        function res = get.Wwc(obj)
            res = getParameter(obj,'Wwc');
        end
        function obj = set.Wwl(obj,val)
            obj = setParameter(obj,'Wwl',val,0,'real');
        end
        function res = get.Wwl(obj)
            res = getParameter(obj,'Wwl');
        end
        function obj = set.Wwlc(obj,val)
            obj = setParameter(obj,'Wwlc',val,0,'real');
        end
        function res = get.Wwlc(obj)
            res = getParameter(obj,'Wwlc');
        end
        function obj = set.Wwn(obj,val)
            obj = setParameter(obj,'Wwn',val,0,'real');
        end
        function res = get.Wwn(obj)
            res = getParameter(obj,'Wwn');
        end
        function obj = set.Wwr(obj,val)
            obj = setParameter(obj,'Wwr',val,0,'real');
        end
        function res = get.Wwr(obj)
            res = getParameter(obj,'Wwr');
        end
        function obj = set.Wx2e(obj,val)
            obj = setParameter(obj,'Wx2e',val,0,'real');
        end
        function res = get.Wx2e(obj)
            res = getParameter(obj,'Wx2e');
        end
        function obj = set.Wx2ms(obj,val)
            obj = setParameter(obj,'Wx2ms',val,0,'real');
        end
        function res = get.Wx2ms(obj)
            res = getParameter(obj,'Wx2ms');
        end
        function obj = set.Wx2mz(obj,val)
            obj = setParameter(obj,'Wx2mz',val,0,'real');
        end
        function res = get.Wx2mz(obj)
            res = getParameter(obj,'Wx2mz');
        end
        function obj = set.Wx2u0(obj,val)
            obj = setParameter(obj,'Wx2u0',val,0,'real');
        end
        function res = get.Wx2u0(obj)
            res = getParameter(obj,'Wx2u0');
        end
        function obj = set.Wx2u1(obj,val)
            obj = setParameter(obj,'Wx2u1',val,0,'real');
        end
        function res = get.Wx2u1(obj)
            res = getParameter(obj,'Wx2u1');
        end
        function obj = set.Wx3ms(obj,val)
            obj = setParameter(obj,'Wx3ms',val,0,'real');
        end
        function res = get.Wx3ms(obj)
            res = getParameter(obj,'Wx3ms');
        end
        function obj = set.Wx3u1(obj,val)
            obj = setParameter(obj,'Wx3u1',val,0,'real');
        end
        function res = get.Wx3u1(obj)
            res = getParameter(obj,'Wx3u1');
        end
        function obj = set.Wxj(obj,val)
            obj = setParameter(obj,'Wxj',val,0,'real');
        end
        function res = get.Wxj(obj)
            res = getParameter(obj,'Wxj');
        end
        function obj = set.Wxt(obj,val)
            obj = setParameter(obj,'Wxt',val,0,'real');
        end
        function res = get.Wxt(obj)
            res = getParameter(obj,'Wxt');
        end
        function obj = set.X2e(obj,val)
            obj = setParameter(obj,'X2e',val,0,'real');
        end
        function res = get.X2e(obj)
            res = getParameter(obj,'X2e');
        end
        function obj = set.X2ms(obj,val)
            obj = setParameter(obj,'X2ms',val,0,'real');
        end
        function res = get.X2ms(obj)
            res = getParameter(obj,'X2ms');
        end
        function obj = set.X2mz(obj,val)
            obj = setParameter(obj,'X2mz',val,0,'real');
        end
        function res = get.X2mz(obj)
            res = getParameter(obj,'X2mz');
        end
        function obj = set.X2u0(obj,val)
            obj = setParameter(obj,'X2u0',val,0,'real');
        end
        function res = get.X2u0(obj)
            res = getParameter(obj,'X2u0');
        end
        function obj = set.X2u1(obj,val)
            obj = setParameter(obj,'X2u1',val,0,'real');
        end
        function res = get.X2u1(obj)
            res = getParameter(obj,'X2u1');
        end
        function obj = set.X3e(obj,val)
            obj = setParameter(obj,'X3e',val,0,'real');
        end
        function res = get.X3e(obj)
            res = getParameter(obj,'X3e');
        end
        function obj = set.X3ms(obj,val)
            obj = setParameter(obj,'X3ms',val,0,'real');
        end
        function res = get.X3ms(obj)
            res = getParameter(obj,'X3ms');
        end
        function obj = set.X3u1(obj,val)
            obj = setParameter(obj,'X3u1',val,0,'real');
        end
        function res = get.X3u1(obj)
            res = getParameter(obj,'X3u1');
        end
        function obj = set.Xj(obj,val)
            obj = setParameter(obj,'Xj',val,0,'real');
        end
        function res = get.Xj(obj)
            res = getParameter(obj,'Xj');
        end
        function obj = set.Xj_pow(obj,val)
            obj = setParameter(obj,'Xj_pow',val,0,'real');
        end
        function res = get.Xj_pow(obj)
            res = getParameter(obj,'Xj_pow');
        end
        function obj = set.Xl(obj,val)
            obj = setParameter(obj,'Xl',val,0,'real');
        end
        function res = get.Xl(obj)
            res = getParameter(obj,'Xl');
        end
        function obj = set.Xmu(obj,val)
            obj = setParameter(obj,'Xmu',val,0,'real');
        end
        function res = get.Xmu(obj)
            res = getParameter(obj,'Xmu');
        end
        function obj = set.Xpart(obj,val)
            obj = setParameter(obj,'Xpart',val,0,'real');
        end
        function res = get.Xpart(obj)
            res = getParameter(obj,'Xpart');
        end
        function obj = set.Xt(obj,val)
            obj = setParameter(obj,'Xt',val,0,'real');
        end
        function res = get.Xt(obj)
            res = getParameter(obj,'Xt');
        end
        function obj = set.Xti(obj,val)
            obj = setParameter(obj,'Xti',val,0,'real');
        end
        function res = get.Xti(obj)
            res = getParameter(obj,'Xti');
        end
        function obj = set.Xw(obj,val)
            obj = setParameter(obj,'Xw',val,0,'real');
        end
        function res = get.Xw(obj)
            res = getParameter(obj,'Xw');
        end
        function obj = set.Zeta(obj,val)
            obj = setParameter(obj,'Zeta',val,0,'real');
        end
        function res = get.Zeta(obj)
            res = getParameter(obj,'Zeta');
        end
        function obj = set.wBvds(obj,val)
            obj = setParameter(obj,'wBvds',val,0,'real');
        end
        function res = get.wBvds(obj)
            res = getParameter(obj,'wBvds');
        end
        function obj = set.wBvg(obj,val)
            obj = setParameter(obj,'wBvg',val,0,'real');
        end
        function res = get.wBvg(obj)
            res = getParameter(obj,'wBvg');
        end
        function obj = set.wBvsub(obj,val)
            obj = setParameter(obj,'wBvsub',val,0,'real');
        end
        function res = get.wBvsub(obj)
            res = getParameter(obj,'wBvsub');
        end
        function obj = set.wIdsmax(obj,val)
            obj = setParameter(obj,'wIdsmax',val,0,'real');
        end
        function res = get.wIdsmax(obj)
            res = getParameter(obj,'wIdsmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.wVsubfwd(obj,val)
            obj = setParameter(obj,'wVsubfwd',val,0,'real');
        end
        function res = get.wVsubfwd(obj)
            res = getParameter(obj,'wVsubfwd');
        end
    end
end
