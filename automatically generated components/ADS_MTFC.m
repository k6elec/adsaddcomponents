classdef ADS_MTFC < ADScomponent
    % ADS_MTFC matlab representation for the ADS MTFC component
    % Microstrip Thin Film Capacitor
    % MTFC [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Dielectric Width Common to both metal plates (Smorr) Unit: m
        W
        % Dielectric Length common to both metal plates (Smorr) Unit: m
        L
        % Capacitance per unit area (Smorr) Unit: pf
        CPUA
        % Thickness of the Capacitor dielectric (Smorr) Unit: m
        T
        % Sheet Resistance of the Top Metal Plate (Smorr) Unit: ohms
        RsT
        % Sheet Resistance of the Bottom Metal Plate (Smorr) Unit: ohms
        RsB
        % Thickness of the Top Metal Plate (Smorr) Unit: m
        TT
        % Thickness of the Bottom Metal Plate (Smorr) Unit: m
        TB
        % Bottom Conductor Overlap (Smorr) Unit: m
        COB
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.CPUA(obj,val)
            obj = setParameter(obj,'CPUA',val,0,'real');
        end
        function res = get.CPUA(obj)
            res = getParameter(obj,'CPUA');
        end
        function obj = set.T(obj,val)
            obj = setParameter(obj,'T',val,0,'real');
        end
        function res = get.T(obj)
            res = getParameter(obj,'T');
        end
        function obj = set.RsT(obj,val)
            obj = setParameter(obj,'RsT',val,0,'real');
        end
        function res = get.RsT(obj)
            res = getParameter(obj,'RsT');
        end
        function obj = set.RsB(obj,val)
            obj = setParameter(obj,'RsB',val,0,'real');
        end
        function res = get.RsB(obj)
            res = getParameter(obj,'RsB');
        end
        function obj = set.TT(obj,val)
            obj = setParameter(obj,'TT',val,0,'real');
        end
        function res = get.TT(obj)
            res = getParameter(obj,'TT');
        end
        function obj = set.TB(obj,val)
            obj = setParameter(obj,'TB',val,0,'real');
        end
        function res = get.TB(obj)
            res = getParameter(obj,'TB');
        end
        function obj = set.COB(obj,val)
            obj = setParameter(obj,'COB',val,0,'real');
        end
        function res = get.COB(obj)
            res = getParameter(obj,'COB');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end
