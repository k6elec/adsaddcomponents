classdef ADS_InitCond < ADSnodeless
    % ADS_InitCond matlab representation for the ADS InitCond component
    % Set initial condition for a transient analysis
    % InitCond [:Name]
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Node name (s---s) 
        NodeName
        % initial node voltage for transient analysis (smorr) Unit: V
        V
        % Connection resistance (smorr) Unit: Ohms
        R
        % Device comes from spice netlist (s---i) 
        Hspice
        % File to read name value pairs from (s---s) 
        File
    end
    methods
        function obj = set.NodeName(obj,val)
            obj = setParameter(obj,'NodeName',val,1,'string');
        end
        function res = get.NodeName(obj)
            res = getParameter(obj,'NodeName');
        end
        function obj = set.V(obj,val)
            obj = setParameter(obj,'V',val,1,'real');
        end
        function res = get.V(obj)
            res = getParameter(obj,'V');
        end
        function obj = set.R(obj,val)
            obj = setParameter(obj,'R',val,1,'real');
        end
        function res = get.R(obj)
            res = getParameter(obj,'R');
        end
        function obj = set.Hspice(obj,val)
            obj = setParameter(obj,'Hspice',val,1,'integer');
        end
        function res = get.Hspice(obj)
            res = getParameter(obj,'Hspice');
        end
        function obj = set.File(obj,val)
            obj = setParameter(obj,'File',val,0,'string');
        end
        function res = get.File(obj)
            res = getParameter(obj,'File');
        end
    end
end
