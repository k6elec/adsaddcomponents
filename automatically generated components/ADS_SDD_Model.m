classdef ADS_SDD_Model < ADScomponent
    % ADS_SDD_Model matlab representation for the ADS SDD_Model component
    % Symbolically Defined Device with Model
    % ModelName [:Name] p1 n1 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Enable SDD equations (sm--b) 
        Enable
    end
    methods
        function obj = set.Enable(obj,val)
            obj = setParameter(obj,'Enable',val,0,'boolean');
        end
        function res = get.Enable(obj)
            res = getParameter(obj,'Enable');
        end
    end
end
