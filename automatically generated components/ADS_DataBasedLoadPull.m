classdef ADS_DataBasedLoadPull < ADSnodeless
    % ADS_DataBasedLoadPull matlab representation for the ADS DataBasedLoadPull component
    % Data-Based Load-Pull Analysis
    % DataBasedLoadPull [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Load-Pull File Type (s---s) 
        FileType
        % Load-Pull File Names (s---s) 
        FileName
        % Frequency Name in load-pull file (s--rs) 
        FreqVarName
        % Independent Variable Names (s---s) 
        IndepVarName
        % Independent Variable Values (sm--s) 
        IndepVarValue
        % Gamma Names (s---s) 
        GammaName
        % Gamma Types (s---s) 
        GammaType
        % Gamma Domains (s---s) 
        GammaDomain
        % Gamma Harm Indexes (s---i) 
        GammaHarmIndex
        % Gamma Value Types (s---s) 
        GammaValue
        % Performance Parameter Name (s---s) 
        PrfmParamName
        % Performance Parameter Type (s---s) 
        PrfmParamType
        % Performance Parameter Output Flag (s---b) 
        PrfmParamOutput
        % Performance Parameter AutoPlot Flag (s---b) 
        PrfmParamAutoPlot
        % Interpolation mode: "linear"/"spline"/"cubic" (sm-rs) 
        InterpMode
        % Interpolation domain: "ri"/"ma"/ (sm-rs) 
        InterpDom
        % Extrapolation mode: "constant"/"interpMode" (sm-rs) 
        ExtrapMode
        % Output plan name (s---s) 
        OutputPlan
        % Frequency value if it's not being swept (smorr) Unit: Hz
        Freq
        % Name of variable or parameter to be swept (sm-rs) 
        SweepVar
        % Stim instance/path name for sweep values (repeatable) (sm-rd) 
        SweepPlan
        % Calculate reflection coefficients (S-parameters) (s--rb) 
        CalcGamma
        % Calculate admittance (s--rb) 
        CalcAdmittance
        % Calculate impedance (s--rb) 
        CalcImpedance
        % Degree of annotation (sm-ri) 
        StatusLevel
        % Allow nonlinear devices in the matching network (s--rb) 
        AllowNonlinearDevices
        % Internal port number (s--ri) 
        TermNum
        % The methodology for the simulator to access the file data: Auto, RawFile, GriddedDataset (s--rs) 
        FileDataAccessMode
        % The measurement tolerance (s--rr) 
        MeasurementTolerance
    end
    methods
        function obj = set.FileType(obj,val)
            obj = setParameter(obj,'FileType',val,0,'string');
        end
        function res = get.FileType(obj)
            res = getParameter(obj,'FileType');
        end
        function obj = set.FileName(obj,val)
            obj = setParameter(obj,'FileName',val,1,'string');
        end
        function res = get.FileName(obj)
            res = getParameter(obj,'FileName');
        end
        function obj = set.FreqVarName(obj,val)
            obj = setParameter(obj,'FreqVarName',val,0,'string');
        end
        function res = get.FreqVarName(obj)
            res = getParameter(obj,'FreqVarName');
        end
        function obj = set.IndepVarName(obj,val)
            obj = setParameter(obj,'IndepVarName',val,1,'string');
        end
        function res = get.IndepVarName(obj)
            res = getParameter(obj,'IndepVarName');
        end
        function obj = set.IndepVarValue(obj,val)
            obj = setParameter(obj,'IndepVarValue',val,1,'string');
        end
        function res = get.IndepVarValue(obj)
            res = getParameter(obj,'IndepVarValue');
        end
        function obj = set.GammaName(obj,val)
            obj = setParameter(obj,'GammaName',val,1,'string');
        end
        function res = get.GammaName(obj)
            res = getParameter(obj,'GammaName');
        end
        function obj = set.GammaType(obj,val)
            obj = setParameter(obj,'GammaType',val,1,'string');
        end
        function res = get.GammaType(obj)
            res = getParameter(obj,'GammaType');
        end
        function obj = set.GammaDomain(obj,val)
            obj = setParameter(obj,'GammaDomain',val,1,'string');
        end
        function res = get.GammaDomain(obj)
            res = getParameter(obj,'GammaDomain');
        end
        function obj = set.GammaHarmIndex(obj,val)
            obj = setParameter(obj,'GammaHarmIndex',val,1,'integer');
        end
        function res = get.GammaHarmIndex(obj)
            res = getParameter(obj,'GammaHarmIndex');
        end
        function obj = set.GammaValue(obj,val)
            obj = setParameter(obj,'GammaValue',val,1,'string');
        end
        function res = get.GammaValue(obj)
            res = getParameter(obj,'GammaValue');
        end
        function obj = set.PrfmParamName(obj,val)
            obj = setParameter(obj,'PrfmParamName',val,1,'string');
        end
        function res = get.PrfmParamName(obj)
            res = getParameter(obj,'PrfmParamName');
        end
        function obj = set.PrfmParamType(obj,val)
            obj = setParameter(obj,'PrfmParamType',val,1,'string');
        end
        function res = get.PrfmParamType(obj)
            res = getParameter(obj,'PrfmParamType');
        end
        function obj = set.PrfmParamOutput(obj,val)
            obj = setParameter(obj,'PrfmParamOutput',val,1,'boolean');
        end
        function res = get.PrfmParamOutput(obj)
            res = getParameter(obj,'PrfmParamOutput');
        end
        function obj = set.PrfmParamAutoPlot(obj,val)
            obj = setParameter(obj,'PrfmParamAutoPlot',val,1,'boolean');
        end
        function res = get.PrfmParamAutoPlot(obj)
            res = getParameter(obj,'PrfmParamAutoPlot');
        end
        function obj = set.InterpMode(obj,val)
            obj = setParameter(obj,'InterpMode',val,0,'string');
        end
        function res = get.InterpMode(obj)
            res = getParameter(obj,'InterpMode');
        end
        function obj = set.InterpDom(obj,val)
            obj = setParameter(obj,'InterpDom',val,0,'string');
        end
        function res = get.InterpDom(obj)
            res = getParameter(obj,'InterpDom');
        end
        function obj = set.ExtrapMode(obj,val)
            obj = setParameter(obj,'ExtrapMode',val,0,'string');
        end
        function res = get.ExtrapMode(obj)
            res = getParameter(obj,'ExtrapMode');
        end
        function obj = set.OutputPlan(obj,val)
            obj = setParameter(obj,'OutputPlan',val,1,'string');
        end
        function res = get.OutputPlan(obj)
            res = getParameter(obj,'OutputPlan');
        end
        function obj = set.Freq(obj,val)
            obj = setParameter(obj,'Freq',val,0,'real');
        end
        function res = get.Freq(obj)
            res = getParameter(obj,'Freq');
        end
        function obj = set.SweepVar(obj,val)
            obj = setParameter(obj,'SweepVar',val,0,'string');
        end
        function res = get.SweepVar(obj)
            res = getParameter(obj,'SweepVar');
        end
        function obj = set.SweepPlan(obj,val)
            obj = setParameter(obj,'SweepPlan',val,1,'string');
        end
        function res = get.SweepPlan(obj)
            res = getParameter(obj,'SweepPlan');
        end
        function obj = set.CalcGamma(obj,val)
            obj = setParameter(obj,'CalcGamma',val,0,'boolean');
        end
        function res = get.CalcGamma(obj)
            res = getParameter(obj,'CalcGamma');
        end
        function obj = set.CalcAdmittance(obj,val)
            obj = setParameter(obj,'CalcAdmittance',val,0,'boolean');
        end
        function res = get.CalcAdmittance(obj)
            res = getParameter(obj,'CalcAdmittance');
        end
        function obj = set.CalcImpedance(obj,val)
            obj = setParameter(obj,'CalcImpedance',val,0,'boolean');
        end
        function res = get.CalcImpedance(obj)
            res = getParameter(obj,'CalcImpedance');
        end
        function obj = set.StatusLevel(obj,val)
            obj = setParameter(obj,'StatusLevel',val,0,'integer');
        end
        function res = get.StatusLevel(obj)
            res = getParameter(obj,'StatusLevel');
        end
        function obj = set.AllowNonlinearDevices(obj,val)
            obj = setParameter(obj,'AllowNonlinearDevices',val,0,'boolean');
        end
        function res = get.AllowNonlinearDevices(obj)
            res = getParameter(obj,'AllowNonlinearDevices');
        end
        function obj = set.TermNum(obj,val)
            obj = setParameter(obj,'TermNum',val,0,'integer');
        end
        function res = get.TermNum(obj)
            res = getParameter(obj,'TermNum');
        end
        function obj = set.FileDataAccessMode(obj,val)
            obj = setParameter(obj,'FileDataAccessMode',val,0,'string');
        end
        function res = get.FileDataAccessMode(obj)
            res = getParameter(obj,'FileDataAccessMode');
        end
        function obj = set.MeasurementTolerance(obj,val)
            obj = setParameter(obj,'MeasurementTolerance',val,0,'real');
        end
        function res = get.MeasurementTolerance(obj)
            res = getParameter(obj,'MeasurementTolerance');
        end
    end
end
