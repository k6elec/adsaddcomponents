classdef ADS_Juncap_Model < ADSmodel
    % ADS_Juncap_Model matlab representation for the ADS Juncap_Model component
    % Junction Diode Philips Model model
    % model ModelName Juncap ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Type of model: 1:SINGLE_DEVICE 2:PROCESS_BASED (s---i) 
        Type
        % Reversed polarity yes/no (no: first node is Anode) (s--rb) 
        Reversed
        % Temperature at which the parameters for the reference transistor have been determined (smorr) Unit: deg C
        Tr
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Voltage at which parameters have been determined (smorr) Unit: V
        Vr
        % Bottom saturation-current density due to electron-hole generation at V=Vr (smorr) Unit: A/m^2
        Jsgbr
        % Bottom saturation-current density due to diffusion  from back contact (smorr) Unit: A/m^2
        Jsdbr
        % Sidewall saturation-current density due to electron-hole generation at V=Vr (smorr) Unit: A/m
        Jsgsr
        % Sidewall saturation-current density due to diffusion  from back contact (smorr) Unit: A/m
        Jsdsr
        % Gate-edge saturation-current density due to electron-hole generation at V=Vr (smorr) Unit: A/m
        Jsggr
        % Gate-edge saturation-current density due to diffusion  from back contact (smorr) Unit: A/m
        Jsdgr
        % Bottom junction capacitance at V = Vr (smorr) Unit: F/m^2
        Cjbr
        % Sidewall junction capacitance at V = Vr (smorr) Unit: F/m
        Cjsr
        % Gate edge junction capacitance at V = Vr (smorr) Unit: F/m
        Cjgr
        % Diffusion voltage of the bottom junction at T = Tr (smorr) Unit: V
        Vdbr
        % Diffusion voltage of the sidewall junction at T = Tr (smorr) Unit: V
        Vdsr
        % Diffusion voltage of the gate edge junction at T = Tr (smorr) Unit: V
        Vdgr
        % Bottom-junction grading coefficient (smorr) 
        Pb
        % Sidewall-junction grading coefficient (smorr) 
        Ps
        % Gate-edge-junction grading coefficient (smorr) 
        Pg
        % Emission coefficient of the bottom forward current (smorr) 
        Nb
        % Emission coefficient of the sidewall forward current (smorr) 
        Ns
        % Emission coefficient of the gate edge forward current (smorr) 
        Ng
        % P-N junction parallel conductance (smorr) Unit: S
        Gmin
        % Explosion current (smorr) Unit: A
        Imax
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
        % Secured model parameters (s---b) 
        Secured
    end
    methods
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'integer');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
        function obj = set.Reversed(obj,val)
            obj = setParameter(obj,'Reversed',val,0,'boolean');
        end
        function res = get.Reversed(obj)
            res = getParameter(obj,'Reversed');
        end
        function obj = set.Tr(obj,val)
            obj = setParameter(obj,'Tr',val,0,'real');
        end
        function res = get.Tr(obj)
            res = getParameter(obj,'Tr');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Vr(obj,val)
            obj = setParameter(obj,'Vr',val,0,'real');
        end
        function res = get.Vr(obj)
            res = getParameter(obj,'Vr');
        end
        function obj = set.Jsgbr(obj,val)
            obj = setParameter(obj,'Jsgbr',val,0,'real');
        end
        function res = get.Jsgbr(obj)
            res = getParameter(obj,'Jsgbr');
        end
        function obj = set.Jsdbr(obj,val)
            obj = setParameter(obj,'Jsdbr',val,0,'real');
        end
        function res = get.Jsdbr(obj)
            res = getParameter(obj,'Jsdbr');
        end
        function obj = set.Jsgsr(obj,val)
            obj = setParameter(obj,'Jsgsr',val,0,'real');
        end
        function res = get.Jsgsr(obj)
            res = getParameter(obj,'Jsgsr');
        end
        function obj = set.Jsdsr(obj,val)
            obj = setParameter(obj,'Jsdsr',val,0,'real');
        end
        function res = get.Jsdsr(obj)
            res = getParameter(obj,'Jsdsr');
        end
        function obj = set.Jsggr(obj,val)
            obj = setParameter(obj,'Jsggr',val,0,'real');
        end
        function res = get.Jsggr(obj)
            res = getParameter(obj,'Jsggr');
        end
        function obj = set.Jsdgr(obj,val)
            obj = setParameter(obj,'Jsdgr',val,0,'real');
        end
        function res = get.Jsdgr(obj)
            res = getParameter(obj,'Jsdgr');
        end
        function obj = set.Cjbr(obj,val)
            obj = setParameter(obj,'Cjbr',val,0,'real');
        end
        function res = get.Cjbr(obj)
            res = getParameter(obj,'Cjbr');
        end
        function obj = set.Cjsr(obj,val)
            obj = setParameter(obj,'Cjsr',val,0,'real');
        end
        function res = get.Cjsr(obj)
            res = getParameter(obj,'Cjsr');
        end
        function obj = set.Cjgr(obj,val)
            obj = setParameter(obj,'Cjgr',val,0,'real');
        end
        function res = get.Cjgr(obj)
            res = getParameter(obj,'Cjgr');
        end
        function obj = set.Vdbr(obj,val)
            obj = setParameter(obj,'Vdbr',val,0,'real');
        end
        function res = get.Vdbr(obj)
            res = getParameter(obj,'Vdbr');
        end
        function obj = set.Vdsr(obj,val)
            obj = setParameter(obj,'Vdsr',val,0,'real');
        end
        function res = get.Vdsr(obj)
            res = getParameter(obj,'Vdsr');
        end
        function obj = set.Vdgr(obj,val)
            obj = setParameter(obj,'Vdgr',val,0,'real');
        end
        function res = get.Vdgr(obj)
            res = getParameter(obj,'Vdgr');
        end
        function obj = set.Pb(obj,val)
            obj = setParameter(obj,'Pb',val,0,'real');
        end
        function res = get.Pb(obj)
            res = getParameter(obj,'Pb');
        end
        function obj = set.Ps(obj,val)
            obj = setParameter(obj,'Ps',val,0,'real');
        end
        function res = get.Ps(obj)
            res = getParameter(obj,'Ps');
        end
        function obj = set.Pg(obj,val)
            obj = setParameter(obj,'Pg',val,0,'real');
        end
        function res = get.Pg(obj)
            res = getParameter(obj,'Pg');
        end
        function obj = set.Nb(obj,val)
            obj = setParameter(obj,'Nb',val,0,'real');
        end
        function res = get.Nb(obj)
            res = getParameter(obj,'Nb');
        end
        function obj = set.Ns(obj,val)
            obj = setParameter(obj,'Ns',val,0,'real');
        end
        function res = get.Ns(obj)
            res = getParameter(obj,'Ns');
        end
        function obj = set.Ng(obj,val)
            obj = setParameter(obj,'Ng',val,0,'real');
        end
        function res = get.Ng(obj)
            res = getParameter(obj,'Ng');
        end
        function obj = set.Gmin(obj,val)
            obj = setParameter(obj,'Gmin',val,0,'real');
        end
        function res = get.Gmin(obj)
            res = getParameter(obj,'Gmin');
        end
        function obj = set.Imax(obj,val)
            obj = setParameter(obj,'Imax',val,0,'real');
        end
        function res = get.Imax(obj)
            res = getParameter(obj,'Imax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end
