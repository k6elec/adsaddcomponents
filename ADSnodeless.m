classdef ADSnodeless < ADSnetlistComponent
methods (Access=protected)
    function res = getStartOfNetlistStatement(obj)
        res = class(obj);
        res = string(res(5:end));
        if strlength(obj.NetlistComponentName)~=0
            res = strjoin([res,":",obj.NetlistComponentName],'');
        else
            error('You need to assign a NetlistComponentName');
        end
    end
end
end