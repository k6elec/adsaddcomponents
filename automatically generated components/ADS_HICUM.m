classdef ADS_HICUM < ADScomponent
    % ADS_HICUM matlab representation for the ADS HICUM component
    % HICUM Bipolar Junction Transistor version 2.1
    % ModelName [:Name] collector base emitter substrate ...
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Nonlinear spectral model on/off (s---i) 
        Mode
        % NPN bipolar transistor (s---b) 
        NPN
        % PNP bipolar transistor (s---b) 
        PNP
        % Noise generation on/off (s---b) 
        Noise
        % Include selfheating effects on/off (s---b) 
        Selfheating
        % Small Signal Transconductance dIt_dVbi (---rr) Unit: S
        dIt_dVbi
        % Small Signal Transconductance dIt_dVci (---rr) Unit: S
        dIt_dVci
        % Small Signal Transconductance dIt_dVei (---rr) Unit: S
        dIt_dVei
        % Small Signal Transconductance Sfb (---rr) Unit: S
        Sfb
        % Small Signal Transconductance Sfc (---rr) Unit: S
        Sfc
        % Small Signal Transconductance Grbi (---rr) Unit: S
        Grbi
        % Small Signal Transconductance Gbet (---rr) Unit: S
        Gbet
        % Small Signal Transconductance Gbcx (---rr) Unit: S
        Gbcx
        % Small Signal Transconductance Gbep (---rr) Unit: S
        Gbep
        % Small Signal Transconductance Gsc (---rr) Unit: S
        Gsc
        % Small Signal Transconductance Gbici (---rr) Unit: S
        Gbici
        % Small Signal Base Collector Capacitance Cbici (---rr) Unit: F
        Cbici
        % Small Signal Transconductance Gbiei (---rr) Unit: S
        Gbiei
        % Small Signal Base Emmiter Capacitance Cbiei (---rr) Unit: F
        Cbiei
        % Small Signal Substrate Collector Capacitance Cjs (---rr) Unit: F
        Cjs
        % Small Signal Base Emmiter Capacitance Cbep (---rr) Unit: F
        Cbep
        % Small Signal Base Collector Capacitance C2bcx (---rr) Unit: F
        C2bcx
        % Small Signal Base Collector Capacitance C1bcx (---rr) Unit: F
        C1bcx
    end
    methods
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.NPN(obj,val)
            obj = setParameter(obj,'NPN',val,0,'boolean');
        end
        function res = get.NPN(obj)
            res = getParameter(obj,'NPN');
        end
        function obj = set.PNP(obj,val)
            obj = setParameter(obj,'PNP',val,0,'boolean');
        end
        function res = get.PNP(obj)
            res = getParameter(obj,'PNP');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Selfheating(obj,val)
            obj = setParameter(obj,'Selfheating',val,0,'boolean');
        end
        function res = get.Selfheating(obj)
            res = getParameter(obj,'Selfheating');
        end
        function obj = set.dIt_dVbi(obj,val)
            obj = setParameter(obj,'dIt_dVbi',val,0,'real');
        end
        function res = get.dIt_dVbi(obj)
            res = getParameter(obj,'dIt_dVbi');
        end
        function obj = set.dIt_dVci(obj,val)
            obj = setParameter(obj,'dIt_dVci',val,0,'real');
        end
        function res = get.dIt_dVci(obj)
            res = getParameter(obj,'dIt_dVci');
        end
        function obj = set.dIt_dVei(obj,val)
            obj = setParameter(obj,'dIt_dVei',val,0,'real');
        end
        function res = get.dIt_dVei(obj)
            res = getParameter(obj,'dIt_dVei');
        end
        function obj = set.Sfb(obj,val)
            obj = setParameter(obj,'Sfb',val,0,'real');
        end
        function res = get.Sfb(obj)
            res = getParameter(obj,'Sfb');
        end
        function obj = set.Sfc(obj,val)
            obj = setParameter(obj,'Sfc',val,0,'real');
        end
        function res = get.Sfc(obj)
            res = getParameter(obj,'Sfc');
        end
        function obj = set.Grbi(obj,val)
            obj = setParameter(obj,'Grbi',val,0,'real');
        end
        function res = get.Grbi(obj)
            res = getParameter(obj,'Grbi');
        end
        function obj = set.Gbet(obj,val)
            obj = setParameter(obj,'Gbet',val,0,'real');
        end
        function res = get.Gbet(obj)
            res = getParameter(obj,'Gbet');
        end
        function obj = set.Gbcx(obj,val)
            obj = setParameter(obj,'Gbcx',val,0,'real');
        end
        function res = get.Gbcx(obj)
            res = getParameter(obj,'Gbcx');
        end
        function obj = set.Gbep(obj,val)
            obj = setParameter(obj,'Gbep',val,0,'real');
        end
        function res = get.Gbep(obj)
            res = getParameter(obj,'Gbep');
        end
        function obj = set.Gsc(obj,val)
            obj = setParameter(obj,'Gsc',val,0,'real');
        end
        function res = get.Gsc(obj)
            res = getParameter(obj,'Gsc');
        end
        function obj = set.Gbici(obj,val)
            obj = setParameter(obj,'Gbici',val,0,'real');
        end
        function res = get.Gbici(obj)
            res = getParameter(obj,'Gbici');
        end
        function obj = set.Cbici(obj,val)
            obj = setParameter(obj,'Cbici',val,0,'real');
        end
        function res = get.Cbici(obj)
            res = getParameter(obj,'Cbici');
        end
        function obj = set.Gbiei(obj,val)
            obj = setParameter(obj,'Gbiei',val,0,'real');
        end
        function res = get.Gbiei(obj)
            res = getParameter(obj,'Gbiei');
        end
        function obj = set.Cbiei(obj,val)
            obj = setParameter(obj,'Cbiei',val,0,'real');
        end
        function res = get.Cbiei(obj)
            res = getParameter(obj,'Cbiei');
        end
        function obj = set.Cjs(obj,val)
            obj = setParameter(obj,'Cjs',val,0,'real');
        end
        function res = get.Cjs(obj)
            res = getParameter(obj,'Cjs');
        end
        function obj = set.Cbep(obj,val)
            obj = setParameter(obj,'Cbep',val,0,'real');
        end
        function res = get.Cbep(obj)
            res = getParameter(obj,'Cbep');
        end
        function obj = set.C2bcx(obj,val)
            obj = setParameter(obj,'C2bcx',val,0,'real');
        end
        function res = get.C2bcx(obj)
            res = getParameter(obj,'C2bcx');
        end
        function obj = set.C1bcx(obj,val)
            obj = setParameter(obj,'C1bcx',val,0,'real');
        end
        function res = get.C1bcx(obj)
            res = getParameter(obj,'C1bcx');
        end
    end
end
