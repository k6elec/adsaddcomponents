classdef ADS_Multipath < ADScomponent
    % ADS_Multipath matlab representation for the ADS Multipath component
    % Multipath Model
    % Multipath [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Two or three path model (Sm-ri) 
        Model
        % Notch frequency (smorr) Unit: Hz
        Freq0
        % Relative notch depth (Smorr) Unit: dB
        NotchDepth
        % Loss term (smorr) Unit: dB
        LossTerm
        % Time delay relative to the main signal (smorr) Unit: sec
        Tau
        % Input impedance of filter (smorr) Unit: Ohms
        Z1
        % Output impedance of filter (smorr) Unit: Ohms
        Z2
    end
    methods
        function obj = set.Model(obj,val)
            obj = setParameter(obj,'Model',val,0,'integer');
        end
        function res = get.Model(obj)
            res = getParameter(obj,'Model');
        end
        function obj = set.Freq0(obj,val)
            obj = setParameter(obj,'Freq0',val,0,'real');
        end
        function res = get.Freq0(obj)
            res = getParameter(obj,'Freq0');
        end
        function obj = set.NotchDepth(obj,val)
            obj = setParameter(obj,'NotchDepth',val,0,'real');
        end
        function res = get.NotchDepth(obj)
            res = getParameter(obj,'NotchDepth');
        end
        function obj = set.LossTerm(obj,val)
            obj = setParameter(obj,'LossTerm',val,0,'real');
        end
        function res = get.LossTerm(obj)
            res = getParameter(obj,'LossTerm');
        end
        function obj = set.Tau(obj,val)
            obj = setParameter(obj,'Tau',val,0,'real');
        end
        function res = get.Tau(obj)
            res = getParameter(obj,'Tau');
        end
        function obj = set.Z1(obj,val)
            obj = setParameter(obj,'Z1',val,0,'real');
        end
        function res = get.Z1(obj)
            res = getParameter(obj,'Z1');
        end
        function obj = set.Z2(obj,val)
            obj = setParameter(obj,'Z2',val,0,'real');
        end
        function res = get.Z2(obj)
            res = getParameter(obj,'Z2');
        end
    end
end
