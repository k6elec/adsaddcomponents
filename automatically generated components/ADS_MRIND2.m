classdef ADS_MRIND2 < ADScomponent
    % ADS_MRIND2 matlab representation for the ADS MRIND2 component
    % Microstrip Rectangular Inductor (No Bridge)
    % MRIND2 [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Number of Turns (need not be an integer) (Smorr) Unit: unknown
        N
        % Length of Second Outermost Segment (Smorr) Unit: m
        L1
        % Length of Outermost Segment (Smorr) Unit: m
        L2
        % Conductor Width (Smorr) Unit: m
        W
        % Conductor Spacing (Smorr) Unit: m
        S
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.L1(obj,val)
            obj = setParameter(obj,'L1',val,0,'real');
        end
        function res = get.L1(obj)
            res = getParameter(obj,'L1');
        end
        function obj = set.L2(obj,val)
            obj = setParameter(obj,'L2',val,0,'real');
        end
        function res = get.L2(obj)
            res = getParameter(obj,'L2');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end
