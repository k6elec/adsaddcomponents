classdef ADS_coupling < ADSnodeless
    % ADS_coupling matlab representation for the ADS coupling component
    % User-defined model
    % coupling [:Name]
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % User device parameter (S--ri) Unit: unknown
        N
        % User device parameter (S--rs) Unit: unknown
        Coupled
        % User device parameter (Smorr) Unit: unknown
        S
        % RLGC File Name (sm-rs) Unit: unknown
        RLGC_File
        % Flag to reuse RLGC matricies (sm-ri) Unit: unknown
        ReuseRLGC
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Coupled(obj,val)
            obj = setParameter(obj,'Coupled',val,0,'string');
        end
        function res = get.Coupled(obj)
            res = getParameter(obj,'Coupled');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.RLGC_File(obj,val)
            obj = setParameter(obj,'RLGC_File',val,0,'string');
        end
        function res = get.RLGC_File(obj)
            res = getParameter(obj,'RLGC_File');
        end
        function obj = set.ReuseRLGC(obj,val)
            obj = setParameter(obj,'ReuseRLGC',val,0,'integer');
        end
        function res = get.ReuseRLGC(obj)
            res = getParameter(obj,'ReuseRLGC');
        end
    end
end
