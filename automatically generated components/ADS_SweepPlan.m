classdef ADS_SweepPlan < ADSnodeless
    % ADS_SweepPlan matlab representation for the ADS SweepPlan component
    % SweepPlan Definition
    % SweepPlan [:Name]
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % start value for stimulus sweep (smorr) 
        Start
        % stop value for stimulus sweep (smorr) 
        Stop
        % center value for stimulus sweep (smorr) 
        Center
        % span for stimulus sweep (smorr) 
        Span
        % step for linear stimulus sweep (smorr) 
        Step
        % points in a linear stimulus sweep (sm-ri) 
        Lin
        % points per decade in a logarithmic stimulus sweep (sm-ri) 
        Dec
        % points in a logarithmic stimulus sweep (sm-ri) 
        Log
        % SweepPlan instance/path name to append to sweep (s---d) 
        SweepPlan
        % sweep in reverse direction (s---b) 
        Reverse
        % sort in ascending value (Sort=no works only for compound sweep plan) (s---b) 
        Sort
        % add point to SweepPlan (repeatable) (s---r) 
        Pt
        % absolute independent with other sweep plans (Parent sweep plan has no control for it) (s---b) 
        AbsoluteIndep
    end
    methods
        function obj = set.Start(obj,val)
            obj = setParameter(obj,'Start',val,0,'real');
        end
        function res = get.Start(obj)
            res = getParameter(obj,'Start');
        end
        function obj = set.Stop(obj,val)
            obj = setParameter(obj,'Stop',val,0,'real');
        end
        function res = get.Stop(obj)
            res = getParameter(obj,'Stop');
        end
        function obj = set.Center(obj,val)
            obj = setParameter(obj,'Center',val,0,'real');
        end
        function res = get.Center(obj)
            res = getParameter(obj,'Center');
        end
        function obj = set.Span(obj,val)
            obj = setParameter(obj,'Span',val,0,'real');
        end
        function res = get.Span(obj)
            res = getParameter(obj,'Span');
        end
        function obj = set.Step(obj,val)
            obj = setParameter(obj,'Step',val,0,'real');
        end
        function res = get.Step(obj)
            res = getParameter(obj,'Step');
        end
        function obj = set.Lin(obj,val)
            obj = setParameter(obj,'Lin',val,0,'integer');
        end
        function res = get.Lin(obj)
            res = getParameter(obj,'Lin');
        end
        function obj = set.Dec(obj,val)
            obj = setParameter(obj,'Dec',val,0,'integer');
        end
        function res = get.Dec(obj)
            res = getParameter(obj,'Dec');
        end
        function obj = set.Log(obj,val)
            obj = setParameter(obj,'Log',val,0,'integer');
        end
        function res = get.Log(obj)
            res = getParameter(obj,'Log');
        end
        function obj = set.SweepPlan(obj,val)
            obj = setParameter(obj,'SweepPlan',val,1,'string');
        end
        function res = get.SweepPlan(obj)
            res = getParameter(obj,'SweepPlan');
        end
        function obj = set.Reverse(obj,val)
            obj = setParameter(obj,'Reverse',val,0,'boolean');
        end
        function res = get.Reverse(obj)
            res = getParameter(obj,'Reverse');
        end
        function obj = set.Sort(obj,val)
            obj = setParameter(obj,'Sort',val,0,'boolean');
        end
        function res = get.Sort(obj)
            res = getParameter(obj,'Sort');
        end
        function obj = set.Pt(obj,val)
            obj = setParameter(obj,'Pt',val,0,'real');
        end
        function res = get.Pt(obj)
            res = getParameter(obj,'Pt');
        end
        function obj = set.AbsoluteIndep(obj,val)
            obj = setParameter(obj,'AbsoluteIndep',val,0,'boolean');
        end
        function res = get.AbsoluteIndep(obj)
            res = getParameter(obj,'AbsoluteIndep');
        end
    end
end
