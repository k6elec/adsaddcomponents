classdef ADS_SP_Model_Model < ADSmodel
    % ADS_SP_Model_Model matlab representation for the ADS SP_Model_Model component
    % Frequency dependent tabular data model
    % model ModelName SP_Model ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Number of signal lines (s--ri) 
        N
        % Name of the file containing tabular frequency dependent SP matrices (sm-rs) 
        Xfile
        % Frequency dependent tabular matrix data (smorr) 
        Xdata
        % Type of sweep in Xdata/Xfile: 0 - linear, 1,2,3 - log, 4 - points (sm-ri) 
        XfreqSweep
        % Frequency of the first data set in the Xdata/Xfile (XfreqSweep=0,1,2,3) (smorr) Unit: Hz
        XfreqStart
        % Frequency of the last data set in the Xdata/Xfile (XfreqSweep=0,1) (smorr) Unit: Hz
        XfreqStop
        % Number of frequency points per decade in Xdata/Xfile (XfreqSweep=2) (sm-ri) 
        XfreqsPerDec
        % Number of frequency points per octave in Xdata/Xfile (XfreqSweep=3) (sm-ri) 
        XfreqsPerOct
        % Interpolation mode: "linear"/"spline"/"cubic" (sm-rs) 
        XinterpMode
        % Matrix type: `symmetric', `hermitian', or `nonsymmetric' (sm-rs) 
        Matrix
        % Data type: `real', `cartesian', or `polar' (sm-rs) 
        ValType
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Xfile(obj,val)
            obj = setParameter(obj,'Xfile',val,0,'string');
        end
        function res = get.Xfile(obj)
            res = getParameter(obj,'Xfile');
        end
        function obj = set.Xdata(obj,val)
            obj = setParameter(obj,'Xdata',val,0,'real');
        end
        function res = get.Xdata(obj)
            res = getParameter(obj,'Xdata');
        end
        function obj = set.XfreqSweep(obj,val)
            obj = setParameter(obj,'XfreqSweep',val,0,'integer');
        end
        function res = get.XfreqSweep(obj)
            res = getParameter(obj,'XfreqSweep');
        end
        function obj = set.XfreqStart(obj,val)
            obj = setParameter(obj,'XfreqStart',val,0,'real');
        end
        function res = get.XfreqStart(obj)
            res = getParameter(obj,'XfreqStart');
        end
        function obj = set.XfreqStop(obj,val)
            obj = setParameter(obj,'XfreqStop',val,0,'real');
        end
        function res = get.XfreqStop(obj)
            res = getParameter(obj,'XfreqStop');
        end
        function obj = set.XfreqsPerDec(obj,val)
            obj = setParameter(obj,'XfreqsPerDec',val,0,'integer');
        end
        function res = get.XfreqsPerDec(obj)
            res = getParameter(obj,'XfreqsPerDec');
        end
        function obj = set.XfreqsPerOct(obj,val)
            obj = setParameter(obj,'XfreqsPerOct',val,0,'integer');
        end
        function res = get.XfreqsPerOct(obj)
            res = getParameter(obj,'XfreqsPerOct');
        end
        function obj = set.XinterpMode(obj,val)
            obj = setParameter(obj,'XinterpMode',val,0,'string');
        end
        function res = get.XinterpMode(obj)
            res = getParameter(obj,'XinterpMode');
        end
        function obj = set.Matrix(obj,val)
            obj = setParameter(obj,'Matrix',val,0,'string');
        end
        function res = get.Matrix(obj)
            res = getParameter(obj,'Matrix');
        end
        function obj = set.ValType(obj,val)
            obj = setParameter(obj,'ValType',val,0,'string');
        end
        function res = get.ValType(obj)
            res = getParameter(obj,'ValType');
        end
    end
end
