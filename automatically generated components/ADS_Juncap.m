classdef ADS_Juncap < ADScomponent
    % ADS_Juncap matlab representation for the ADS Juncap component
    % Junction Diode Philips Model
    % ModelName [:Name] anode cathode
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Diffusion area (smorr) Unit: m^2
        Ab
        % Length of the sidewall of the diffusion area  which is not under the gate. (smorr) Unit: m
        Ls
        % Length of the sidewall of the diffusion area  which is under the gate. (smorr) Unit: m
        Lg
        % DC operating region, 0=off, 1=on (s---i) 
        Region
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Nonlinear spectral model on/off (s---b) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % Small signal conductance (---rr) Unit: S
        Gd
        % Small signal capacitance (---rr) Unit: F
        Cd
    end
    methods
        function obj = set.Ab(obj,val)
            obj = setParameter(obj,'Ab',val,0,'real');
        end
        function res = get.Ab(obj)
            res = getParameter(obj,'Ab');
        end
        function obj = set.Ls(obj,val)
            obj = setParameter(obj,'Ls',val,0,'real');
        end
        function res = get.Ls(obj)
            res = getParameter(obj,'Ls');
        end
        function obj = set.Lg(obj,val)
            obj = setParameter(obj,'Lg',val,0,'real');
        end
        function res = get.Lg(obj)
            res = getParameter(obj,'Lg');
        end
        function obj = set.Region(obj,val)
            obj = setParameter(obj,'Region',val,0,'integer');
        end
        function res = get.Region(obj)
            res = getParameter(obj,'Region');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'boolean');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Gd(obj,val)
            obj = setParameter(obj,'Gd',val,0,'real');
        end
        function res = get.Gd(obj)
            res = getParameter(obj,'Gd');
        end
        function obj = set.Cd(obj,val)
            obj = setParameter(obj,'Cd',val,0,'real');
        end
        function res = get.Cd(obj)
            res = getParameter(obj,'Cd');
        end
    end
end
