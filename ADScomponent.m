classdef ADScomponent < ADSnetlistComponent
    properties
        Nodes (1,:) string = string.empty;
    end
    properties (Access=protected,Abstract)
        NumberOfNodes (1,1) double {mustBeInteger}
    end
    methods
        function obj = set.Nodes(obj,val)
            obj.Nodes = val;
            if length(obj.Nodes)~=obj.NumberOfNodes%#ok
                error('Incorrect amount of nodes');
            end
        end
    end
    methods (Access=protected)
        function res = getStartOfNetlistStatement(obj)
            res = class(obj);
            res = string(res(5:end));
            if strlength(obj.NetlistComponentName)~=0
                res = strjoin([res,":",obj.NetlistComponentName],'');
            else
                error('You need to assign a ComponentName to the component');
            end
            if ~isempty(obj.Nodes)
                res = strjoin([res obj.Nodes]);
            else
                error('You need to assign nodes to the component');
            end
        end
    end
end

