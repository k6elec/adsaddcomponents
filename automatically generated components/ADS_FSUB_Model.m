classdef ADS_FSUB_Model < ADSmodel
    % ADS_FSUB_Model matlab representation for the ADS FSUB_Model component
    % Finline Substrate Parameter Definition model
    % model ModelName FSUB ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Relative dielectric constant (smorr) 
        Er
        % Conductor conductivity (smorr) Unit: S/m
        Cond
        % Finline enclosure inside width (smorr) 
        Fa
        % Finline enclosure inside height (smorr) 
        Fb
        % Finline dielectric width (smorr) 
        Fdw
        % Secured Substrate parameters (s---b) 
        Secured
    end
    methods
        function obj = set.Er(obj,val)
            obj = setParameter(obj,'Er',val,0,'real');
        end
        function res = get.Er(obj)
            res = getParameter(obj,'Er');
        end
        function obj = set.Cond(obj,val)
            obj = setParameter(obj,'Cond',val,0,'real');
        end
        function res = get.Cond(obj)
            res = getParameter(obj,'Cond');
        end
        function obj = set.Fa(obj,val)
            obj = setParameter(obj,'Fa',val,0,'real');
        end
        function res = get.Fa(obj)
            res = getParameter(obj,'Fa');
        end
        function obj = set.Fb(obj,val)
            obj = setParameter(obj,'Fb',val,0,'real');
        end
        function res = get.Fb(obj)
            res = getParameter(obj,'Fb');
        end
        function obj = set.Fdw(obj,val)
            obj = setParameter(obj,'Fdw',val,0,'real');
        end
        function res = get.Fdw(obj)
            res = getParameter(obj,'Fdw');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end
