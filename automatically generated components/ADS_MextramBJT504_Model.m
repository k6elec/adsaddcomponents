classdef ADS_MextramBJT504_Model < ADSmodel
    % ADS_MextramBJT504_Model matlab representation for the ADS MextramBJT504_Model component
    % Mextram Bipolar Junction Transistor level 504 model
    % model ModelName MextramBJT504 ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Model level (should be set to 504; not used) (s---r) 
        Level
        % Reference temperature (smorr) Unit: deg C
        Tref
        % Difference between the device and ambient temperature (smorr) Unit: deg C
        Dta
        % Flag for the extended modeling of the reverse current gain (s---b) 
        Exmod
        % Flag for high-frequency effects in transient (s---b) 
        Exphi
        % Flag for extended modelling of avalanche currents (s---b) 
        Exavl
        % Collector-emitter saturation current (smorr) Unit: A
        Is
        % Collector-emitter high injection knee current (smorr) Unit: A
        Ik
        % Reverse Early voltage (smorr) Unit: V
        Ver
        % Forward Early voltage (smorr) Unit: V
        Vef
        % Ideal forward current gain (smorr) 
        Bf
        % Saturation current of the non-ideal forward base current (smorr) Unit: A
        Ibf
        % Non-ideality factor of the forward base current (smorr) 
        Mlf
        % Part of the ideal base current that belongs to the sidewall (smorr) 
        Xibi
        % Ideal reverse current gain (smorr) 
        Bri
        % Saturation current of the non-ideal reverse base current (smorr) Unit: A
        Ibr
        % Cross-over voltage of the non-ideal reverse base current (smorr) Unit: V
        Vlr
        % Part of Iex, Qtex, Qex and Isub that depends on Vbc1 instead of Vb1c2 (smorr) 
        Xext
        % Epilayer thickness used in weak-avalanche model (smorr) Unit: m
        Wavl
        % Voltage determining curvature of avalanche current (smorr) Unit: V
        Vavl
        % Current spreading factor of avalanche model (when EXAVL=1) (smorr) 
        Sfh
        % Emitter resistance (smorr) Unit: Ohms
        Re
        % Constant part of the base resistance (smorr) Unit: Ohms
        Rbc
        % Zero-bias value of the variable part of the base resistance (smorr) Unit: Ohms
        Rbv
        % Constant part of the collector resistance (smorr) Unit: Ohms
        Rcc
        % Resistance of the un-modulated epilayer (smorr) Unit: Ohms
        Rcv
        % Space charge resistance of the epilayer (smorr) Unit: Ohms
        Scrcv
        % Critital current for velocity saturation in the epilayer (smorr) Unit: A
        Ihc
        % Smoothness parameter for the onset of quasi-saturation (smorr) 
        Axi
        % Zero-bias emitter-base depletion capacitance (smorr) Unit: F
        Cje
        % Emitter-base diffusion voltage (smorr) Unit: V
        Vde
        % Emitter-base grading coefficient (smorr) 
        Pe
        % Fraction of the emitter-base depletion capacitance that belongs to the sidewall (smorr) 
        Xcje
        % Fixed capacitance between external base and emitter nodes (smorr) Unit: F
        Cbeo
        % Zero-bias collector-base depletion capacitance (smorr) Unit: F
        Cjc
        % Collector-base diffusion voltage (smorr) Unit: V
        Vdc
        % Collector-base grading coefficient (smorr) 
        Pc
        % Constant part of Cjc (smorr) 
        Xp
        % Coefficient for the current modulation of the collector-base capacitance (smorr) 
        Mc
        % Fraction of the collector-base depletion capacitance under the emitter (smorr) 
        Xcjc
        % Fixed capacitance between external base and collector nodes (smorr) Unit: F
        Cbco
        % Non-ideality factor of the emitter stored charge (smorr) 
        Mtau
        % Minimum transit time of stored emitter charge (smorr) Unit: s
        Taue
        % Transit time of stored base charge (smorr) Unit: s
        Taub
        % Transit time of stored epilayer charge (smorr) Unit: s
        Tepi
        % Transit time of reverse extrinsic stored base charge (smorr) Unit: s
        Taur
        % Bandgap difference over the base (smorr) Unit: eV
        Deg
        % Pre-factor of the recombination part of Ib1 (smorr) 
        Xrec
        % Temperature coefficient of the zero-bias base charge (smorr) 
        Aqbo
        % Temperature coefficient of the resistivity of the emitter (smorr) 
        Ae
        % Temperature coefficient of the resistivity of the base (smorr) 
        Ab
        % Temperature coefficient of the resistivity of the epilayer (smorr) 
        Aepi
        % Temperature coefficient of the resistivity of the extrinsic base (smorr) 
        Aex
        % Temperature coefficient of the resistivity of the buried layer (smorr) 
        Ac
        % Band-gap voltage difference of the forward current gain (smorr) Unit: V
        dVgbf
        % Band-gap voltage difference of the reverse current gain (smorr) Unit: V
        dVgbr
        % Band-gap voltage of the base (smorr) Unit: V
        Vgb
        % Band-gap voltage of the collector (smorr) Unit: V
        Vgc
        % Band-gap voltage recombination emitter-base junction (smorr) Unit: V
        Vgj
        % Band-gap voltage difference of emitter stored charge (smorr) Unit: V
        dVgte
        % Exponent of the flicker-noise (smorr) 
        Af
        % Flicker-noise coefficient of the ideal base current (smorr) 
        Kf
        % Flicker-noise coefficient of the non-ideal base current (smorr) 
        Kfn
        % Base-substrate saturation current (smorr) Unit: A
        Iss
        % Base-substrate high injection knee current (smorr) Unit: A
        Iks
        % Zero-bias collector-substrate depletion capacitance (smorr) Unit: F
        Cjs
        % Collector-substrate diffusion voltage (smorr) Unit: V
        Vds
        % Collector-substrate grading coefficient (smorr) 
        Ps
        % Band-gap voltage of the substrate (smorr) Unit: V
        Vgs
        % For a closed buried layer As=Ac and for an open buried layer As=Aepi (smorr) 
        As
        % Thermal resistance (smorr) 
        Rth
        % Thermal capacitance (smorr) 
        Cth
        % NPN bipolar transistor (s---b) 
        NPN
        % PNP bipolar transistor (s---b) 
        PNP
        % Substrate junction forward bias (warning) (s--rr) Unit: V
        wVsubfwd
        % Substrate junction reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvsub
        % Base-emitter reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvbe
        % Base-collector reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvbc
        % Base-collector forward bias (warning) (s--rr) Unit: V
        wVbcfwd
        % Maximum base current (warning) (s--rr) Unit: A/m^2
        wIbmax
        % Maximum collector current (warning) (s--rr) Unit: A/m^2
        wIcmax
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
    end
    methods
        function obj = set.Level(obj,val)
            obj = setParameter(obj,'Level',val,0,'real');
        end
        function res = get.Level(obj)
            res = getParameter(obj,'Level');
        end
        function obj = set.Tref(obj,val)
            obj = setParameter(obj,'Tref',val,0,'real');
        end
        function res = get.Tref(obj)
            res = getParameter(obj,'Tref');
        end
        function obj = set.Dta(obj,val)
            obj = setParameter(obj,'Dta',val,0,'real');
        end
        function res = get.Dta(obj)
            res = getParameter(obj,'Dta');
        end
        function obj = set.Exmod(obj,val)
            obj = setParameter(obj,'Exmod',val,0,'boolean');
        end
        function res = get.Exmod(obj)
            res = getParameter(obj,'Exmod');
        end
        function obj = set.Exphi(obj,val)
            obj = setParameter(obj,'Exphi',val,0,'boolean');
        end
        function res = get.Exphi(obj)
            res = getParameter(obj,'Exphi');
        end
        function obj = set.Exavl(obj,val)
            obj = setParameter(obj,'Exavl',val,0,'boolean');
        end
        function res = get.Exavl(obj)
            res = getParameter(obj,'Exavl');
        end
        function obj = set.Is(obj,val)
            obj = setParameter(obj,'Is',val,0,'real');
        end
        function res = get.Is(obj)
            res = getParameter(obj,'Is');
        end
        function obj = set.Ik(obj,val)
            obj = setParameter(obj,'Ik',val,0,'real');
        end
        function res = get.Ik(obj)
            res = getParameter(obj,'Ik');
        end
        function obj = set.Ver(obj,val)
            obj = setParameter(obj,'Ver',val,0,'real');
        end
        function res = get.Ver(obj)
            res = getParameter(obj,'Ver');
        end
        function obj = set.Vef(obj,val)
            obj = setParameter(obj,'Vef',val,0,'real');
        end
        function res = get.Vef(obj)
            res = getParameter(obj,'Vef');
        end
        function obj = set.Bf(obj,val)
            obj = setParameter(obj,'Bf',val,0,'real');
        end
        function res = get.Bf(obj)
            res = getParameter(obj,'Bf');
        end
        function obj = set.Ibf(obj,val)
            obj = setParameter(obj,'Ibf',val,0,'real');
        end
        function res = get.Ibf(obj)
            res = getParameter(obj,'Ibf');
        end
        function obj = set.Mlf(obj,val)
            obj = setParameter(obj,'Mlf',val,0,'real');
        end
        function res = get.Mlf(obj)
            res = getParameter(obj,'Mlf');
        end
        function obj = set.Xibi(obj,val)
            obj = setParameter(obj,'Xibi',val,0,'real');
        end
        function res = get.Xibi(obj)
            res = getParameter(obj,'Xibi');
        end
        function obj = set.Bri(obj,val)
            obj = setParameter(obj,'Bri',val,0,'real');
        end
        function res = get.Bri(obj)
            res = getParameter(obj,'Bri');
        end
        function obj = set.Ibr(obj,val)
            obj = setParameter(obj,'Ibr',val,0,'real');
        end
        function res = get.Ibr(obj)
            res = getParameter(obj,'Ibr');
        end
        function obj = set.Vlr(obj,val)
            obj = setParameter(obj,'Vlr',val,0,'real');
        end
        function res = get.Vlr(obj)
            res = getParameter(obj,'Vlr');
        end
        function obj = set.Xext(obj,val)
            obj = setParameter(obj,'Xext',val,0,'real');
        end
        function res = get.Xext(obj)
            res = getParameter(obj,'Xext');
        end
        function obj = set.Wavl(obj,val)
            obj = setParameter(obj,'Wavl',val,0,'real');
        end
        function res = get.Wavl(obj)
            res = getParameter(obj,'Wavl');
        end
        function obj = set.Vavl(obj,val)
            obj = setParameter(obj,'Vavl',val,0,'real');
        end
        function res = get.Vavl(obj)
            res = getParameter(obj,'Vavl');
        end
        function obj = set.Sfh(obj,val)
            obj = setParameter(obj,'Sfh',val,0,'real');
        end
        function res = get.Sfh(obj)
            res = getParameter(obj,'Sfh');
        end
        function obj = set.Re(obj,val)
            obj = setParameter(obj,'Re',val,0,'real');
        end
        function res = get.Re(obj)
            res = getParameter(obj,'Re');
        end
        function obj = set.Rbc(obj,val)
            obj = setParameter(obj,'Rbc',val,0,'real');
        end
        function res = get.Rbc(obj)
            res = getParameter(obj,'Rbc');
        end
        function obj = set.Rbv(obj,val)
            obj = setParameter(obj,'Rbv',val,0,'real');
        end
        function res = get.Rbv(obj)
            res = getParameter(obj,'Rbv');
        end
        function obj = set.Rcc(obj,val)
            obj = setParameter(obj,'Rcc',val,0,'real');
        end
        function res = get.Rcc(obj)
            res = getParameter(obj,'Rcc');
        end
        function obj = set.Rcv(obj,val)
            obj = setParameter(obj,'Rcv',val,0,'real');
        end
        function res = get.Rcv(obj)
            res = getParameter(obj,'Rcv');
        end
        function obj = set.Scrcv(obj,val)
            obj = setParameter(obj,'Scrcv',val,0,'real');
        end
        function res = get.Scrcv(obj)
            res = getParameter(obj,'Scrcv');
        end
        function obj = set.Ihc(obj,val)
            obj = setParameter(obj,'Ihc',val,0,'real');
        end
        function res = get.Ihc(obj)
            res = getParameter(obj,'Ihc');
        end
        function obj = set.Axi(obj,val)
            obj = setParameter(obj,'Axi',val,0,'real');
        end
        function res = get.Axi(obj)
            res = getParameter(obj,'Axi');
        end
        function obj = set.Cje(obj,val)
            obj = setParameter(obj,'Cje',val,0,'real');
        end
        function res = get.Cje(obj)
            res = getParameter(obj,'Cje');
        end
        function obj = set.Vde(obj,val)
            obj = setParameter(obj,'Vde',val,0,'real');
        end
        function res = get.Vde(obj)
            res = getParameter(obj,'Vde');
        end
        function obj = set.Pe(obj,val)
            obj = setParameter(obj,'Pe',val,0,'real');
        end
        function res = get.Pe(obj)
            res = getParameter(obj,'Pe');
        end
        function obj = set.Xcje(obj,val)
            obj = setParameter(obj,'Xcje',val,0,'real');
        end
        function res = get.Xcje(obj)
            res = getParameter(obj,'Xcje');
        end
        function obj = set.Cbeo(obj,val)
            obj = setParameter(obj,'Cbeo',val,0,'real');
        end
        function res = get.Cbeo(obj)
            res = getParameter(obj,'Cbeo');
        end
        function obj = set.Cjc(obj,val)
            obj = setParameter(obj,'Cjc',val,0,'real');
        end
        function res = get.Cjc(obj)
            res = getParameter(obj,'Cjc');
        end
        function obj = set.Vdc(obj,val)
            obj = setParameter(obj,'Vdc',val,0,'real');
        end
        function res = get.Vdc(obj)
            res = getParameter(obj,'Vdc');
        end
        function obj = set.Pc(obj,val)
            obj = setParameter(obj,'Pc',val,0,'real');
        end
        function res = get.Pc(obj)
            res = getParameter(obj,'Pc');
        end
        function obj = set.Xp(obj,val)
            obj = setParameter(obj,'Xp',val,0,'real');
        end
        function res = get.Xp(obj)
            res = getParameter(obj,'Xp');
        end
        function obj = set.Mc(obj,val)
            obj = setParameter(obj,'Mc',val,0,'real');
        end
        function res = get.Mc(obj)
            res = getParameter(obj,'Mc');
        end
        function obj = set.Xcjc(obj,val)
            obj = setParameter(obj,'Xcjc',val,0,'real');
        end
        function res = get.Xcjc(obj)
            res = getParameter(obj,'Xcjc');
        end
        function obj = set.Cbco(obj,val)
            obj = setParameter(obj,'Cbco',val,0,'real');
        end
        function res = get.Cbco(obj)
            res = getParameter(obj,'Cbco');
        end
        function obj = set.Mtau(obj,val)
            obj = setParameter(obj,'Mtau',val,0,'real');
        end
        function res = get.Mtau(obj)
            res = getParameter(obj,'Mtau');
        end
        function obj = set.Taue(obj,val)
            obj = setParameter(obj,'Taue',val,0,'real');
        end
        function res = get.Taue(obj)
            res = getParameter(obj,'Taue');
        end
        function obj = set.Taub(obj,val)
            obj = setParameter(obj,'Taub',val,0,'real');
        end
        function res = get.Taub(obj)
            res = getParameter(obj,'Taub');
        end
        function obj = set.Tepi(obj,val)
            obj = setParameter(obj,'Tepi',val,0,'real');
        end
        function res = get.Tepi(obj)
            res = getParameter(obj,'Tepi');
        end
        function obj = set.Taur(obj,val)
            obj = setParameter(obj,'Taur',val,0,'real');
        end
        function res = get.Taur(obj)
            res = getParameter(obj,'Taur');
        end
        function obj = set.Deg(obj,val)
            obj = setParameter(obj,'Deg',val,0,'real');
        end
        function res = get.Deg(obj)
            res = getParameter(obj,'Deg');
        end
        function obj = set.Xrec(obj,val)
            obj = setParameter(obj,'Xrec',val,0,'real');
        end
        function res = get.Xrec(obj)
            res = getParameter(obj,'Xrec');
        end
        function obj = set.Aqbo(obj,val)
            obj = setParameter(obj,'Aqbo',val,0,'real');
        end
        function res = get.Aqbo(obj)
            res = getParameter(obj,'Aqbo');
        end
        function obj = set.Ae(obj,val)
            obj = setParameter(obj,'Ae',val,0,'real');
        end
        function res = get.Ae(obj)
            res = getParameter(obj,'Ae');
        end
        function obj = set.Ab(obj,val)
            obj = setParameter(obj,'Ab',val,0,'real');
        end
        function res = get.Ab(obj)
            res = getParameter(obj,'Ab');
        end
        function obj = set.Aepi(obj,val)
            obj = setParameter(obj,'Aepi',val,0,'real');
        end
        function res = get.Aepi(obj)
            res = getParameter(obj,'Aepi');
        end
        function obj = set.Aex(obj,val)
            obj = setParameter(obj,'Aex',val,0,'real');
        end
        function res = get.Aex(obj)
            res = getParameter(obj,'Aex');
        end
        function obj = set.Ac(obj,val)
            obj = setParameter(obj,'Ac',val,0,'real');
        end
        function res = get.Ac(obj)
            res = getParameter(obj,'Ac');
        end
        function obj = set.dVgbf(obj,val)
            obj = setParameter(obj,'dVgbf',val,0,'real');
        end
        function res = get.dVgbf(obj)
            res = getParameter(obj,'dVgbf');
        end
        function obj = set.dVgbr(obj,val)
            obj = setParameter(obj,'dVgbr',val,0,'real');
        end
        function res = get.dVgbr(obj)
            res = getParameter(obj,'dVgbr');
        end
        function obj = set.Vgb(obj,val)
            obj = setParameter(obj,'Vgb',val,0,'real');
        end
        function res = get.Vgb(obj)
            res = getParameter(obj,'Vgb');
        end
        function obj = set.Vgc(obj,val)
            obj = setParameter(obj,'Vgc',val,0,'real');
        end
        function res = get.Vgc(obj)
            res = getParameter(obj,'Vgc');
        end
        function obj = set.Vgj(obj,val)
            obj = setParameter(obj,'Vgj',val,0,'real');
        end
        function res = get.Vgj(obj)
            res = getParameter(obj,'Vgj');
        end
        function obj = set.dVgte(obj,val)
            obj = setParameter(obj,'dVgte',val,0,'real');
        end
        function res = get.dVgte(obj)
            res = getParameter(obj,'dVgte');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Kfn(obj,val)
            obj = setParameter(obj,'Kfn',val,0,'real');
        end
        function res = get.Kfn(obj)
            res = getParameter(obj,'Kfn');
        end
        function obj = set.Iss(obj,val)
            obj = setParameter(obj,'Iss',val,0,'real');
        end
        function res = get.Iss(obj)
            res = getParameter(obj,'Iss');
        end
        function obj = set.Iks(obj,val)
            obj = setParameter(obj,'Iks',val,0,'real');
        end
        function res = get.Iks(obj)
            res = getParameter(obj,'Iks');
        end
        function obj = set.Cjs(obj,val)
            obj = setParameter(obj,'Cjs',val,0,'real');
        end
        function res = get.Cjs(obj)
            res = getParameter(obj,'Cjs');
        end
        function obj = set.Vds(obj,val)
            obj = setParameter(obj,'Vds',val,0,'real');
        end
        function res = get.Vds(obj)
            res = getParameter(obj,'Vds');
        end
        function obj = set.Ps(obj,val)
            obj = setParameter(obj,'Ps',val,0,'real');
        end
        function res = get.Ps(obj)
            res = getParameter(obj,'Ps');
        end
        function obj = set.Vgs(obj,val)
            obj = setParameter(obj,'Vgs',val,0,'real');
        end
        function res = get.Vgs(obj)
            res = getParameter(obj,'Vgs');
        end
        function obj = set.As(obj,val)
            obj = setParameter(obj,'As',val,0,'real');
        end
        function res = get.As(obj)
            res = getParameter(obj,'As');
        end
        function obj = set.Rth(obj,val)
            obj = setParameter(obj,'Rth',val,0,'real');
        end
        function res = get.Rth(obj)
            res = getParameter(obj,'Rth');
        end
        function obj = set.Cth(obj,val)
            obj = setParameter(obj,'Cth',val,0,'real');
        end
        function res = get.Cth(obj)
            res = getParameter(obj,'Cth');
        end
        function obj = set.NPN(obj,val)
            obj = setParameter(obj,'NPN',val,0,'boolean');
        end
        function res = get.NPN(obj)
            res = getParameter(obj,'NPN');
        end
        function obj = set.PNP(obj,val)
            obj = setParameter(obj,'PNP',val,0,'boolean');
        end
        function res = get.PNP(obj)
            res = getParameter(obj,'PNP');
        end
        function obj = set.wVsubfwd(obj,val)
            obj = setParameter(obj,'wVsubfwd',val,0,'real');
        end
        function res = get.wVsubfwd(obj)
            res = getParameter(obj,'wVsubfwd');
        end
        function obj = set.wBvsub(obj,val)
            obj = setParameter(obj,'wBvsub',val,0,'real');
        end
        function res = get.wBvsub(obj)
            res = getParameter(obj,'wBvsub');
        end
        function obj = set.wBvbe(obj,val)
            obj = setParameter(obj,'wBvbe',val,0,'real');
        end
        function res = get.wBvbe(obj)
            res = getParameter(obj,'wBvbe');
        end
        function obj = set.wBvbc(obj,val)
            obj = setParameter(obj,'wBvbc',val,0,'real');
        end
        function res = get.wBvbc(obj)
            res = getParameter(obj,'wBvbc');
        end
        function obj = set.wVbcfwd(obj,val)
            obj = setParameter(obj,'wVbcfwd',val,0,'real');
        end
        function res = get.wVbcfwd(obj)
            res = getParameter(obj,'wVbcfwd');
        end
        function obj = set.wIbmax(obj,val)
            obj = setParameter(obj,'wIbmax',val,0,'real');
        end
        function res = get.wIbmax(obj)
            res = getParameter(obj,'wIbmax');
        end
        function obj = set.wIcmax(obj,val)
            obj = setParameter(obj,'wIcmax',val,0,'real');
        end
        function res = get.wIcmax(obj)
            res = getParameter(obj,'wIcmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
    end
end
