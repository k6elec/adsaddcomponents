classdef ADS_OutputPlan < ADSnodeless
    % ADS_OutputPlan matlab representation for the ADS OutputPlan component
    % Output Definition
    % OutputPlan [:Name]
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Output/Inhibit indictes whether it is an Output or an Inhibit OuputPlan (s--rs) 
        Type
        % Normal/Global/SuperGlobal indicates the scope of the output (s--rs) 
        Scope
        % Voltage nodes Regular Expression (s--rs) 
        NodeRegExpr
        % Voltage node name (s--rs) 
        NodeName
        % Variable names Regular Expression (s--rs) 
        EquationRegExpr
        % Variable name (s--rs) 
        EquationName
        % Node NestLevel (s--ri) 
        NodeNestLevel
        % Should NestLevel for node voltages be used (s--rb) 
        UseNodeNestLevel
        % Variable NestLevel (s--ri) 
        EquationNestLevel
        % Should NestLevel for variable be used (s--rb) 
        UseEquationNestLevel
        % Current Regular Expression (s--rs) 
        CurrentRegExpr
        % Current name (s--rs) 
        CurrentName
        % Current NestLevel (s--ri) 
        CurrentNestLevel
        % Should NestLevel for currents be used (s--rb) 
        UseCurrentNestLevel
        % Device Current name (s--rs) 
        DeviceCurrentName
        % Device Current Regular Expression (s--rs) 
        DeviceCurrentRegExpr
        % Device type for device current output (s--rs) 
        DeviceCurrentDeviceType
        % Current NestLevel (s--ri) 
        DeviceCurrentNestLevel
        % Should NestLevel for currents be used (s--rb) 
        UseDeviceCurrentNestLevel
        % Overrides inhibit plan (s--rb) 
        OverrideInhibitRule
        % Overrides Gemini's default built-in rule (s--rb) 
        UseBuiltinRule
        % Saved Variable name (s--rs) 
        SavedEquationName
        % Saved Variable NestLevel (s--ri) 
        SavedEquationNestLevel
        % Should NestLevel for saved variable be used (s--rb) 
        UseSavedEquationNestLevel
        % Attachment/UnAttachment indictes whether it is an attachment/unattachment option (s--rs) 
        AttachmentType
        % Variable names Regular Expression for Attachment (s--rs) 
        AttachEquationRegExp
        % Variable name for Attachment (s--rs) 
        AttachedEquationName
        % Variable NestLevel for Attachment (s--ri) 
        AttachedEquationNestLevel
        % Should NestLevel for variable be used for Attachment (s--rb) 
        UseAttachedEquationNestLevel
        % Overrides UnAttachment plan (s--rb) 
        OverrideUnAttachmentRule
        % Use Gemini's default built-in rule for attachment (s--rb) 
        UseAttachmentBuiltinRule
        % the symmetrical syntax for device current (s--rb) 
        DeviceCurrentSymSyntax
        % Device Voltage Regular Expression (s--rs) 
        DeviceVoltageRegExpr
        % Device Voltage name (s--rs) 
        DeviceVoltageName
        % Should NestLevel for pin voltages be used (s--rb) 
        UseDeviceVoltageNestLevel
        % Voltage NestLevel (s--ri) 
        DeviceVoltageNestLevel
        % Device type for device voltage output (s--rs) 
        DeviceVoltageDeviceType
        % the symmetrical syntax for device voltage (s--rb) 
        DeviceVoltageSymSyntax
    end
    methods
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'string');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
        function obj = set.Scope(obj,val)
            obj = setParameter(obj,'Scope',val,0,'string');
        end
        function res = get.Scope(obj)
            res = getParameter(obj,'Scope');
        end
        function obj = set.NodeRegExpr(obj,val)
            obj = setParameter(obj,'NodeRegExpr',val,0,'string');
        end
        function res = get.NodeRegExpr(obj)
            res = getParameter(obj,'NodeRegExpr');
        end
        function obj = set.NodeName(obj,val)
            obj = setParameter(obj,'NodeName',val,0,'string');
        end
        function res = get.NodeName(obj)
            res = getParameter(obj,'NodeName');
        end
        function obj = set.EquationRegExpr(obj,val)
            obj = setParameter(obj,'EquationRegExpr',val,0,'string');
        end
        function res = get.EquationRegExpr(obj)
            res = getParameter(obj,'EquationRegExpr');
        end
        function obj = set.EquationName(obj,val)
            obj = setParameter(obj,'EquationName',val,0,'string');
        end
        function res = get.EquationName(obj)
            res = getParameter(obj,'EquationName');
        end
        function obj = set.NodeNestLevel(obj,val)
            obj = setParameter(obj,'NodeNestLevel',val,0,'integer');
        end
        function res = get.NodeNestLevel(obj)
            res = getParameter(obj,'NodeNestLevel');
        end
        function obj = set.UseNodeNestLevel(obj,val)
            obj = setParameter(obj,'UseNodeNestLevel',val,0,'boolean');
        end
        function res = get.UseNodeNestLevel(obj)
            res = getParameter(obj,'UseNodeNestLevel');
        end
        function obj = set.EquationNestLevel(obj,val)
            obj = setParameter(obj,'EquationNestLevel',val,0,'integer');
        end
        function res = get.EquationNestLevel(obj)
            res = getParameter(obj,'EquationNestLevel');
        end
        function obj = set.UseEquationNestLevel(obj,val)
            obj = setParameter(obj,'UseEquationNestLevel',val,0,'boolean');
        end
        function res = get.UseEquationNestLevel(obj)
            res = getParameter(obj,'UseEquationNestLevel');
        end
        function obj = set.CurrentRegExpr(obj,val)
            obj = setParameter(obj,'CurrentRegExpr',val,0,'string');
        end
        function res = get.CurrentRegExpr(obj)
            res = getParameter(obj,'CurrentRegExpr');
        end
        function obj = set.CurrentName(obj,val)
            obj = setParameter(obj,'CurrentName',val,0,'string');
        end
        function res = get.CurrentName(obj)
            res = getParameter(obj,'CurrentName');
        end
        function obj = set.CurrentNestLevel(obj,val)
            obj = setParameter(obj,'CurrentNestLevel',val,0,'integer');
        end
        function res = get.CurrentNestLevel(obj)
            res = getParameter(obj,'CurrentNestLevel');
        end
        function obj = set.UseCurrentNestLevel(obj,val)
            obj = setParameter(obj,'UseCurrentNestLevel',val,0,'boolean');
        end
        function res = get.UseCurrentNestLevel(obj)
            res = getParameter(obj,'UseCurrentNestLevel');
        end
        function obj = set.DeviceCurrentName(obj,val)
            obj = setParameter(obj,'DeviceCurrentName',val,0,'string');
        end
        function res = get.DeviceCurrentName(obj)
            res = getParameter(obj,'DeviceCurrentName');
        end
        function obj = set.DeviceCurrentRegExpr(obj,val)
            obj = setParameter(obj,'DeviceCurrentRegExpr',val,0,'string');
        end
        function res = get.DeviceCurrentRegExpr(obj)
            res = getParameter(obj,'DeviceCurrentRegExpr');
        end
        function obj = set.DeviceCurrentDeviceType(obj,val)
            obj = setParameter(obj,'DeviceCurrentDeviceType',val,0,'string');
        end
        function res = get.DeviceCurrentDeviceType(obj)
            res = getParameter(obj,'DeviceCurrentDeviceType');
        end
        function obj = set.DeviceCurrentNestLevel(obj,val)
            obj = setParameter(obj,'DeviceCurrentNestLevel',val,0,'integer');
        end
        function res = get.DeviceCurrentNestLevel(obj)
            res = getParameter(obj,'DeviceCurrentNestLevel');
        end
        function obj = set.UseDeviceCurrentNestLevel(obj,val)
            obj = setParameter(obj,'UseDeviceCurrentNestLevel',val,0,'boolean');
        end
        function res = get.UseDeviceCurrentNestLevel(obj)
            res = getParameter(obj,'UseDeviceCurrentNestLevel');
        end
        function obj = set.OverrideInhibitRule(obj,val)
            obj = setParameter(obj,'OverrideInhibitRule',val,0,'boolean');
        end
        function res = get.OverrideInhibitRule(obj)
            res = getParameter(obj,'OverrideInhibitRule');
        end
        function obj = set.UseBuiltinRule(obj,val)
            obj = setParameter(obj,'UseBuiltinRule',val,0,'boolean');
        end
        function res = get.UseBuiltinRule(obj)
            res = getParameter(obj,'UseBuiltinRule');
        end
        function obj = set.SavedEquationName(obj,val)
            obj = setParameter(obj,'SavedEquationName',val,0,'string');
        end
        function res = get.SavedEquationName(obj)
            res = getParameter(obj,'SavedEquationName');
        end
        function obj = set.SavedEquationNestLevel(obj,val)
            obj = setParameter(obj,'SavedEquationNestLevel',val,0,'integer');
        end
        function res = get.SavedEquationNestLevel(obj)
            res = getParameter(obj,'SavedEquationNestLevel');
        end
        function obj = set.UseSavedEquationNestLevel(obj,val)
            obj = setParameter(obj,'UseSavedEquationNestLevel',val,0,'boolean');
        end
        function res = get.UseSavedEquationNestLevel(obj)
            res = getParameter(obj,'UseSavedEquationNestLevel');
        end
        function obj = set.AttachmentType(obj,val)
            obj = setParameter(obj,'AttachmentType',val,0,'string');
        end
        function res = get.AttachmentType(obj)
            res = getParameter(obj,'AttachmentType');
        end
        function obj = set.AttachEquationRegExp(obj,val)
            obj = setParameter(obj,'AttachEquationRegExp',val,0,'string');
        end
        function res = get.AttachEquationRegExp(obj)
            res = getParameter(obj,'AttachEquationRegExp');
        end
        function obj = set.AttachedEquationName(obj,val)
            obj = setParameter(obj,'AttachedEquationName',val,0,'string');
        end
        function res = get.AttachedEquationName(obj)
            res = getParameter(obj,'AttachedEquationName');
        end
        function obj = set.AttachedEquationNestLevel(obj,val)
            obj = setParameter(obj,'AttachedEquationNestLevel',val,0,'integer');
        end
        function res = get.AttachedEquationNestLevel(obj)
            res = getParameter(obj,'AttachedEquationNestLevel');
        end
        function obj = set.UseAttachedEquationNestLevel(obj,val)
            obj = setParameter(obj,'UseAttachedEquationNestLevel',val,0,'boolean');
        end
        function res = get.UseAttachedEquationNestLevel(obj)
            res = getParameter(obj,'UseAttachedEquationNestLevel');
        end
        function obj = set.OverrideUnAttachmentRule(obj,val)
            obj = setParameter(obj,'OverrideUnAttachmentRule',val,0,'boolean');
        end
        function res = get.OverrideUnAttachmentRule(obj)
            res = getParameter(obj,'OverrideUnAttachmentRule');
        end
        function obj = set.UseAttachmentBuiltinRule(obj,val)
            obj = setParameter(obj,'UseAttachmentBuiltinRule',val,0,'boolean');
        end
        function res = get.UseAttachmentBuiltinRule(obj)
            res = getParameter(obj,'UseAttachmentBuiltinRule');
        end
        function obj = set.DeviceCurrentSymSyntax(obj,val)
            obj = setParameter(obj,'DeviceCurrentSymSyntax',val,0,'boolean');
        end
        function res = get.DeviceCurrentSymSyntax(obj)
            res = getParameter(obj,'DeviceCurrentSymSyntax');
        end
        function obj = set.DeviceVoltageRegExpr(obj,val)
            obj = setParameter(obj,'DeviceVoltageRegExpr',val,0,'string');
        end
        function res = get.DeviceVoltageRegExpr(obj)
            res = getParameter(obj,'DeviceVoltageRegExpr');
        end
        function obj = set.DeviceVoltageName(obj,val)
            obj = setParameter(obj,'DeviceVoltageName',val,0,'string');
        end
        function res = get.DeviceVoltageName(obj)
            res = getParameter(obj,'DeviceVoltageName');
        end
        function obj = set.UseDeviceVoltageNestLevel(obj,val)
            obj = setParameter(obj,'UseDeviceVoltageNestLevel',val,0,'boolean');
        end
        function res = get.UseDeviceVoltageNestLevel(obj)
            res = getParameter(obj,'UseDeviceVoltageNestLevel');
        end
        function obj = set.DeviceVoltageNestLevel(obj,val)
            obj = setParameter(obj,'DeviceVoltageNestLevel',val,0,'integer');
        end
        function res = get.DeviceVoltageNestLevel(obj)
            res = getParameter(obj,'DeviceVoltageNestLevel');
        end
        function obj = set.DeviceVoltageDeviceType(obj,val)
            obj = setParameter(obj,'DeviceVoltageDeviceType',val,0,'string');
        end
        function res = get.DeviceVoltageDeviceType(obj)
            res = getParameter(obj,'DeviceVoltageDeviceType');
        end
        function obj = set.DeviceVoltageSymSyntax(obj,val)
            obj = setParameter(obj,'DeviceVoltageSymSyntax',val,0,'boolean');
        end
        function res = get.DeviceVoltageSymSyntax(obj)
            res = getParameter(obj,'DeviceVoltageSymSyntax');
        end
    end
end
