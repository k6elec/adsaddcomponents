classdef ADS_MOS11_Model < ADSmodel
    % ADS_MOS11_Model matlab representation for the ADS MOS11_Model component
    % MOS MODEL 11 model
    % model ModelName MOS11 ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % NMOS type MOS11 (s---b) 
        NMOS
        % PMOS type MOS11 (s---b) 
        PMOS
        % Philips Level Name (1101/11010/11011) (s---i) 
        Level
        % Difference between the actual and the programmed poly-silicon gate length (smorr) Unit: m
        Lvar
        % Effective channel length reduction per side due to the lateral diffusion of the source/drain dopant ions (smorr) Unit: m
        Lap
        % Difference between the actual and the programmed field-oxide opening (smorr) Unit: m
        Wvar
        % Effective reduction of the channel width per side due to the lateral diffusion of the channel-stop dopant ions (smorr) Unit: m
        Wot
        % Temperature at which the parameters for the reference transistor have been determined (smorr) Unit: deg C
        Tr
        % Temperature at which the parameters for the reference transistor have been determined (smorr) Unit: deg C
        Tnom
        % Temperature offset of the device with respect to Ta (smorr) Unit: K
        Dta
        % Flat-band voltage for the reference transistor at the reference temperature (smorr) Unit: V
        Vfb
        % Coefficient of the temperature dependence of VFB (smorr) Unit: V/K
        Stvfb
        % Low-back-bias body factor for the reference transistor (smorr) Unit: V^(1/2)
        Kor
        % Coefficient of the length dependence of KO (smorr) Unit: m*V^(1/2)
        Slko
        % Second coefficient of the length dependence of KO (smorr) Unit: m*V^(1/2)
        Sl2ko
        % Coefficient of the width dependence of KO (smorr) Unit: m*V^(1/2)
        Swko
        % Inverse of body-effect factor of the poly-silicon gate (smorr) Unit: V^(-1/2)
        Kpinv
        % Surface potential at the onset of strong inversion at the reference temperature (smorr) Unit: V
        Phibr
        % Coefficient of the temperature dependence of PHIB (smorr) Unit: V/K
        Stphib
        % Coefficient of the length dependence of PHIB (smorr) Unit: V*m
        Slphib
        % Second coefficient of the length dependence of PHIB (smorr) Unit: V*m^2
        Sl2phib
        % Coefficient of the width dependence of PHIB (smorr) Unit: V*m
        Swphib
        % Gain factor for an infinite square transistor at the reference temperature (smorr) Unit: A/V^2
        Betsq
        % Exponent of the temperature dependence of the gain factor (smorr) 
        Etabetr
        % Coefficient of the length dependence of ETABET (smorr) Unit: m
        Sletabet
        % Relative mobility decrease due to first lateral profile (smorr) 
        Fbet1
        % Characteristic length of first lateral profile (smorr) Unit: m
        Lp1
        % Relative mobility decrease due to second lateral profile (smorr) 
        Fbet2
        % Characteristic length of second lateral profile (smorr) Unit: m
        Lp2
        % Coefficient of the mobility reduction due to surface roughness scattering for the reference transistor at the reference temperature (smorr) Unit: V^-1
        Thesrr
        % Exponent of the temperature dependence of THESR for the reference transistor (smorr) 
        Etasr
        % Coefficient of the width dependence of THESR (smorr) Unit: m
        Swthesr
        % Coefficient of the mobility reduction due to phonon scattering for the reference transistor at the reference temperature (smorr) Unit: V^-1
        Thephr
        % Exponent of the temperature dependence of THESR for the reference transistor (smorr) 
        Etaph
        % Coefficient of the width dependence of THEPH (smorr) Unit: m
        Swtheph
        % Effective field parameter for dependence on depletion/inversion charge for the reference transistor (smorr) 
        Etamobr
        % Coefficient of the temperature dependence of ETAMOB (smorr) Unit: K^-1
        Stetamob
        % Coefficient of the width dependence of ETAMOB (smorr) Unit: m
        Swetamob
        % Exponent of the field dependence of the mobility model minus 1 at the reference temperature (smorr) 
        Nu
        % Exponent of the temperature dependence of NU (smorr) 
        Nuexp
        % Coefficient of the series resistance for the reference transistor at the reference temperature (smorr) Unit: V^-1
        Therr
        % Exponent of the temperature dependence of ETA (smorr) 
        Etar
        % Coefficient of the width dependence of THER (smorr) Unit: m
        Swther
        % Numerator of the gate voltage dependent part of series resistance for the reference transistor (smorr) Unit: V
        Ther1
        % Denominator of the gate voltage dependent part of series resistance for the reference transistor (smorr) Unit: V
        Ther2
        % Velocity saturation parameter due to optical/acoustic phonon scattering  for the reference transistor at the reference temperature (smorr) Unit: V^-1
        Thesatr
        % Exponent of the temperature dependence of THESAT (smorr) 
        Etasat
        % Coefficient of the length dependence of THESAT (smorr) 
        Slthesat
        % Exponent of the length dependence of THESAT (smorr) 
        Thesatexp
        % Coefficient of the width dependence of THESAT (smorr) Unit: m
        Swthesat
        % Coefficient on self-heating for the reference transistor at the reference temperature (smorr) Unit: V^-3
        Thethr
        % Exponent of the length dependence of THETH (smorr) 
        Thethexp
        % Coefficient of the width dependence of THETH (smorr) Unit: m
        Swtheth
        % Drain-induced barrier-lowering parameter for the reference transistor (smorr) Unit: V^(-1/2)
        Sdiblo
        % Exponent of the length dependence of SDIBLO (smorr) 
        Sdiblexp
        % Parameter fr short-channel subthreshold slope for the reference transistor (smorr) 
        Mor
        % Exponent of the length dependence of MO (smorr) 
        Moexp
        % Static feedback parameter for the reference transistor (smorr) Unit: V^(-1/2)
        Ssfr
        % Coefficient of the length dependence of SSF (smorr) Unit: m
        Slssf
        % Coefficient of the width dependence of SSF (smorr) Unit: m
        Swssf
        % Factor of the channel-length modulation for the reference transistor (smorr) 
        Alpr
        % Coefficient of the length dependence of ALP (smorr) 
        Slalp
        % Exponent of the length dependence of ALP (smorr) 
        Alpexp
        % Coefficient of the width dependence of SSF (smorr) Unit: m
        Swalp
        % Characteristic voltage of channel length modulation (smorr) Unit: V
        Vp
        % Minimum effective channel length in technology, used for calculation of smoothing factor m (smorr) Unit: m
        Lmin
        % Smoothing factor for the actual transistor (smorr) 
        Mexp
        % Factor of the weak-avalanche current for the reference transistor at the reference temperature (smorr) 
        A1r
        % Coefficient of the temperature dependence of A1 (smorr) Unit: K^-1
        Sta1
        % Coefficient of the length dependence of A1 (smorr) Unit: m
        Sla1
        % oefficient of the width dependence of A1 (smorr) Unit: m
        Swa1
        % Exponent of the weak-avalanche current for the reference transistor at the reference temperature (smorr) Unit: V
        A2r
        % Coefficient of the length dependence of A2 (smorr) Unit: V*m
        Sla2
        % Coefficient of the width dependence of A2 (smorr) Unit: V*m
        Swa2
        % Factor of the drain-source voltage above which weak-avalanche occurs, for the reference transistor (smorr) 
        A3r
        % Coefficient of the length dependence of A3 (smorr) Unit: m
        Sla3
        % Coefficient of the width dependence of A3 (smorr) Unit: m
        Swa3
        % Gain factor for intrinsic gate tunelling current in inversion for the reference transistor (smorr) Unit: A/V^2
        Iginvr
        % Probability factor for intrinsic gate tunelling current in inversion (smorr) Unit: V
        Binv
        % Gain factor for intrinsic gate tunelling current in accumulation for the reference transistor (smorr) Unit: A/V^2
        Igaccr
        % Probability factor for intrinsic gate tunelling current in accumulation (smorr) Unit: V
        Bacc
        % Flat-band voltage for the source/drain overlap extension (smorr) Unit: V
        Vfbov
        % Body-effect factor for the source/drain overlap extension (smorr) Unit: V^(1/2)
        Kov
        % Gain factor for source/drain overlap gate tunelling current for the reference transistor (smorr) Unit: A/V^2
        Igovr
        % Thickness of the gate oxide layer (smorr) Unit: m
        Tox
        % Gate overlap capacitance for a channel width of 1 um (smorr) Unit: F
        Col
        % Gate-to-channel capacitance (smorr) Unit: F
        Cox
        % G-D overlap capacitance (smorr) Unit: F
        Cgdo
        % G-S overlap capacitance (smorr) Unit: F
        Cgso
        % Flag for in/exclusion of induced gate thermal noise (smorr) 
        Gatenoise
        % Coefficient of the thermal noise at the actual temperature (smorr) Unit: J
        Nt
        % First coefficient of the flicker noise for the reference transistor (smorr) Unit: V^-1*m^-1
        Nfar
        % Second coefficient of the flicker noise for the reference transistor (smorr) Unit: V^-1*m^-2
        Nfbr
        % Second coefficient of the flicker noise for the reference transistor (smorr) Unit: V^-1*m^-2
        Nfcr
        % Coefficient for the geometry independent part of KO (smorr) Unit: V^(1/2)
        Poko
        % Coefficient for the length dependence of KO (smorr) Unit: V^(1/2)
        Plko
        % Coefficient for the width dependence of KO (smorr) Unit: V^(1/2)
        Pwko
        % Coefficient for the length times width dependence of KO (smorr) Unit: V^(1/2)
        Plwko
        % Coefficient for the geometry independent part of PHIB (smorr) Unit: V
        Pophib
        % Coefficient for the length dependence of PHIB (smorr) Unit: V
        Plphib
        % Coefficient for the width dependence of PHIB (smorr) Unit: V
        Pwphib
        % Coefficient for the length times width dependence of PHIB (smorr) Unit: V
        Plwphib
        % Coefficient for the geometry independent part of BET (smorr) Unit: A/V^2
        Pobet
        % Coefficient for the length dependence of BET (smorr) Unit: A/V^2
        Plbet
        % Coefficient for the width dependence of BET (smorr) Unit: A/V^2
        Pwbet
        % Coefficient for the width over length dependence of BET (smorr) Unit: A/V^2
        Plwbet
        % Coefficient for the geometry independent part of THES (smorr) Unit: V^-1
        Pothesr
        % Coefficient for the length dependence of THES (smorr) Unit: V^-1
        Plthesr
        % Coefficient for the width dependence of THES (smorr) Unit: V^-1
        Pwthesr
        % Coefficient for the length times width dependence of THES (smorr) Unit: V^-1
        Plwthesr
        % Coefficient for the geometry independent part of THEPH (smorr) Unit: V^-1
        Potheph
        % Coefficient for the length dependence of THEPH (smorr) Unit: V^-1
        Pltheph
        % Coefficient for the width dependence of THEPH (smorr) Unit: V^-1
        Pwtheph
        % Coefficient for the length times width dependence of THEPH (smorr) Unit: V^-1
        Plwtheph
        % Coefficient for the geometry independent part of ETAMOB (smorr) 
        Poetamob
        % Coefficient for the length dependence of ETAMOB (smorr) 
        Pletamob
        % Coefficient for the width dependence of ETAMOB (smorr) 
        Pwetamob
        % Coefficient for the length times width dependence of ETAMOB (smorr) 
        Plwetamob
        % Coefficient for the geometry independent part of THER (smorr) Unit: V^-1
        Pother
        % Coefficient for the length dependence of THER (smorr) Unit: V^-1
        Plther
        % Coefficient for the width dependence of THER (smorr) Unit: V^-1
        Pwther
        % Coefficient for the length times width dependence of THER (smorr) Unit: V^-1
        Plwther
        % Coefficient for the geometry independent part of THESAT (smorr) Unit: V^-1
        Pothesat
        % Coefficient for the length dependence of THESAT (smorr) Unit: V^-1
        Plthesat
        % Coefficient for the width dependence of THESAT (smorr) Unit: V^-1
        Pwthesat
        % Coefficient for the length times width dependence of THESAT (smorr) Unit: V^-1
        Plwthesat
        % Coefficient for the geometry independent part of THETH (smorr) Unit: V^-3
        Potheth
        % Coefficient for the length dependence of THETH (smorr) Unit: V^-3
        Pltheth
        % Coefficient for the width dependence of THETH (smorr) Unit: V^-3
        Pwtheth
        % Coefficient for the length times width dependence of THETH (smorr) Unit: V^-3
        Plwtheth
        % Coefficient for the geometry independent part of SDIBL (smorr) Unit: V^(-1/2)
        Posdibl
        % Coefficient for the length dependence of SDIBL (smorr) Unit: V^(-1/2)
        Plsdibl
        % Coefficient for the width dependence of SDIBL (smorr) Unit: V^(-1/2)
        Pwsdibl
        % Coefficient for the length times width dependence of SDIBL (smorr) Unit: V^(-1/2)
        Plwsdibl
        % Coefficient for the geometry independent part of MO (smorr) 
        Pomo
        % Coefficient for the length dependence of MO (smorr) 
        Plmo
        % Coefficient for the width dependence of MO (smorr) 
        Pwmo
        % Coefficient for the length times width dependence of MO (smorr) 
        Plwmo
        % Coefficient for the geometry independent part of SSF (smorr) Unit: V^(-1/2)
        Possf
        % Coefficient for the length dependence of SSF (smorr) Unit: V^(-1/2)
        Plssf
        % Coefficient for the width dependence of SSF (smorr) Unit: V^(-1/2)
        Pwssf
        % Coefficient for the length times width dependence of SSF (smorr) Unit: V^(-1/2)
        Plwssf
        % Coefficient for the geometry independent part of ALP (smorr) 
        Poalp
        % Coefficient for the length dependence of ALP (smorr) 
        Plalp
        % Coefficient for the width dependence of ALP (smorr) 
        Pwalp
        % Coefficient for the length times width dependence of ALP (smorr) 
        Plwalp
        % Coefficient for the geometry independent part of 1/m (smorr) 
        Pomexp
        % Coefficient for the length dependence of 1/m (smorr) 
        Plmexp
        % Coefficient for the width dependence of 1/m (smorr) 
        Pwmexp
        % Coefficient for the length times width dependence of 1/m (smorr) 
        Plwmexp
        % Coefficient for the geometry independent part of A1 (smorr) 
        Poa1
        % Coefficient for the length dependence of A1 (smorr) 
        Pla1
        % Coefficient for the width dependence of A1 (smorr) 
        Pwa1
        % Coefficient for the length times width dependence of A1 (smorr) 
        Plwa1
        % Coefficient for the geometry independent part of A2 (smorr) Unit: V
        Poa2
        % Coefficient for the length dependence of A2 (smorr) Unit: V
        Pla2
        % Coefficient for the width dependence of A2 (smorr) Unit: V
        Pwa2
        % Coefficient for the length times width dependence of A2 (smorr) Unit: V
        Plwa2
        % Coefficient for the geometry independent part of A3 (smorr) 
        Poa3
        % Coefficient for the length dependence of A3 (smorr) 
        Pla3
        % Coefficient for the width dependence of A3 (smorr) 
        Pwa3
        % Coefficient for the length times width dependence of A3 (smorr) 
        Plwa3
        % Coefficient for the geometry independent part of IGINV (smorr) Unit: A/V
        Poiginv
        % Coefficient for the length dependence of IGINV (smorr) 
        Pliginv
        % Coefficient for the width dependence of IGINV (smorr) 
        Pwiginv
        % Coefficient for the length times width dependence of IGINV (smorr) 
        Plwiginv
        % Coefficient for the geometry independent part of BINV (smorr) Unit: V
        Pobinv
        % Coefficient for the length dependence of BINV (smorr) Unit: V
        Plbinv
        % Coefficient for the width dependence of BINV (smorr) Unit: V
        Pwbinv
        % Coefficient for the length times width dependence of BINV (smorr) Unit: V
        Plwbinv
        % Coefficient for the geometry independent part of IGACC (smorr) Unit: A/V^2
        Poigacc
        % Coefficient for the length dependence of IGACC (smorr) Unit: A/V^2
        Pligacc
        % Coefficient for the width dependence of IGACC (smorr) Unit: A/V^2
        Pwigacc
        % Coefficient for the length times width dependence of IGACC (smorr) Unit: A/V^2
        Plwigacc
        % Coefficient for the geometry independent part of BACC (smorr) Unit: V
        Pobacc
        % Coefficient for the length dependence of BACC (smorr) Unit: V
        Plbacc
        % Coefficient for the width dependence of BACC (smorr) Unit: V
        Pwbacc
        % Coefficient for the length times width dependence of BACC (smorr) Unit: V
        Plwbacc
        % Coefficient for the geometry independent part of IGOV (smorr) Unit: A/V^2
        Poigov
        % Coefficient for the length dependence of IGOV (smorr) Unit: A/V^2
        Pligov
        % Coefficient for the width dependence of IGOV (smorr) Unit: A/V^2
        Pwigov
        % Coefficient for the width over length dependence of IGOV (smorr) Unit: A/V^2
        Plwigov
        % Coefficient for the geometry independent part of COX (smorr) Unit: F
        Pocox
        % Coefficient for the length dependence of COX (smorr) Unit: F
        Plcox
        % Coefficient for the width dependence of COX (smorr) Unit: F
        Pwcox
        % Coefficient for the width over length dependence COX (smorr) Unit: F
        Plwcox
        % Coefficient for the geometry independent part of CGDO (smorr) Unit: F
        Pocgdo
        % Coefficient for the length dependence of CGDO (smorr) Unit: F
        Plcgdo
        % Coefficient for the width dependence of CGDO (smorr) Unit: F
        Pwcgdo
        % Coefficient for the width over length dependence CGDO (smorr) Unit: F
        Plwcgdo
        % Coefficient for the geometry independent part of CGSO (smorr) Unit: F
        Pocgso
        % Coefficient for the length dependence of CGSO (smorr) Unit: F
        Plcgso
        % Coefficient for the width dependence of CGSO (smorr) Unit: F
        Pwcgso
        % Coefficient for the width over length dependence CGSO (smorr) Unit: F
        Plwcgso
        % Coefficient for the geometry independent part of NFA (smorr) Unit: V^-1*m^-1
        Ponfa
        % Coefficient for the length dependence of NFA (smorr) Unit: V^-1*m^-1
        Plnfa
        % Coefficient for the width dependence of NFA (smorr) Unit: V^-1*m^-1
        Pwnfa
        % Coefficient for the length times width dependence of NFA (smorr) Unit: V^-1*m^-1
        Plwnfa
        % Coefficient for the geometry independent part of NFB (smorr) Unit: V^-1*m^-2
        Ponfb
        % Coefficient for the length dependence of NFB (smorr) Unit: V^-1*m^-2
        Plnfb
        % Coefficient for the width dependence of NFB (smorr) Unit: V^-1*m^-2
        Pwnfb
        % Coefficient for the length times width dependence of NFB (smorr) Unit: V^-1*m^-2
        Plwnfb
        % Coefficient for the geometry independent part of NFC (smorr) Unit: V^-1
        Ponfc
        % Coefficient for the length dependence of NFC (smorr) Unit: V^-1
        Plnfc
        % Coefficient for the width dependence of NFC (smorr) Unit: V^-1
        Pwnfc
        % Coefficient for the length times width dependence of NFC (smorr) Unit: V^-1
        Plwnfc
        % Coefficient for the geometry independent part of STVFB (smorr) Unit: V/K
        Potvfb
        % Coefficient for the length dependence of STVFB (smorr) Unit: V/K
        Pltvfb
        % Coefficient for the width dependence of STVFB (smorr) Unit: V/K
        Pwtvfb
        % Coefficient for the length times width dependence of STVFB (smorr) Unit: V/K
        Plwtvfb
        % Coefficient for the geometry independent part of STPHIB (smorr) Unit: V/K
        Potphib
        % Coefficient for the length dependence of STPHIB (smorr) Unit: V/K
        Pltphib
        % Coefficient for the width dependence of STPHIB (smorr) Unit: V/K
        Pwtphib
        % Coefficient for the length times width dependence of STPHIB (smorr) Unit: V/K
        Plwtphib
        % Coefficient for the geometry independent part of ETABET (smorr) 
        Potetabet
        % Coefficient for the length dependence of ETABET (smorr) 
        Pltetabet
        % Coefficient for the width dependence of ETABET (smorr) 
        Pwtetabet
        % Coefficient for the length times width dependence of ETABET (smorr) 
        Plwtetabet
        % Coefficient for the geometry independent part of ETASR (smorr) 
        Potetasr
        % Coefficient for the length dependence of ETASR (smorr) 
        Pltetasr
        % Coefficient for the width dependence of ETASR (smorr) 
        Pwtetasr
        % Coefficient for the length times width dependence of ETASR (smorr) 
        Plwtetasr
        % Coefficient for the geometry independent part of ETAPH (smorr) 
        Potetaph
        % Coefficient for the length dependence of ETAPH (smorr) 
        Pltetaph
        % Coefficient for the width dependence of ETAPH (smorr) 
        Pwtetaph
        % Coefficient for the length times width dependence of ETAPH (smorr) 
        Plwtetaph
        % Coefficient for the geometry independent part of ETAMOB (smorr) Unit: K^-1
        Potetamob
        % Coefficient for the length dependence of ETAMOB (smorr) Unit: K^-1
        Pltetamob
        % Coefficient for the width dependence of ETAMOB (smorr) Unit: K^-1
        Pwtetamob
        % Coefficient for the length times width dependence of ETAMOB (smorr) Unit: K^-1
        Plwtetamob
        % Coefficient for the geometry independent part of NUEXP (smorr) 
        Potnuexp
        % Coefficient for the length dependence of NUEXP (smorr) 
        Pltnuexp
        % Coefficient for the width dependence of NUEXP (smorr) 
        Pwtnuexp
        % Coefficient for the length times width dependence of NUEXP (smorr) 
        Plwtnuexp
        % Coefficient for the geometry independent part of ETAR (smorr) 
        Potetar
        % Coefficient for the length dependence of ETAR (smorr) 
        Pltetar
        % Coefficient for the width dependence of ETAR (smorr) 
        Pwtetar
        % Coefficient for the length times width dependence of ETAR (smorr) 
        Plwtetar
        % Coefficient for the geometry independent part of ETASAT (smorr) 
        Potetasat
        % Coefficient for the length dependence of ETASAT (smorr) 
        Pltetasat
        % Coefficient for the width dependence of ETASAT (smorr) 
        Pwtetasat
        % Coefficient for the length times width dependence of ETASAT (smorr) 
        Plwtetasat
        % Coefficient for the geometry independent part of STA1 (smorr) Unit: K^-1
        Pota1
        % Coefficient for the length dependence of STA1 (smorr) Unit: K^-1
        Plta1
        % Coefficient for the width dependence of STA1 (smorr) Unit: K^-1
        Pwta1
        % Coefficient for the length times width dependence of STA1 (smorr) Unit: K^-1
        Plwta1
        % Secured model parameters (s---b) 
        Secured
        % Substrate junction forward bias (warning) (s--rr) Unit: V
        wVsubfwd
        % Substrate junction reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvsub
        % Gate oxide breakdown voltage (warning) (s--rr) Unit: V
        wBvg
        % Drain-source breakdown voltage (warning) (s--rr) Unit: V
        wBvds
        % Maximum drain-source current (warning) (s--rr) Unit: A
        wIdsmax
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
    end
    methods
        function obj = set.NMOS(obj,val)
            obj = setParameter(obj,'NMOS',val,0,'boolean');
        end
        function res = get.NMOS(obj)
            res = getParameter(obj,'NMOS');
        end
        function obj = set.PMOS(obj,val)
            obj = setParameter(obj,'PMOS',val,0,'boolean');
        end
        function res = get.PMOS(obj)
            res = getParameter(obj,'PMOS');
        end
        function obj = set.Level(obj,val)
            obj = setParameter(obj,'Level',val,0,'integer');
        end
        function res = get.Level(obj)
            res = getParameter(obj,'Level');
        end
        function obj = set.Lvar(obj,val)
            obj = setParameter(obj,'Lvar',val,0,'real');
        end
        function res = get.Lvar(obj)
            res = getParameter(obj,'Lvar');
        end
        function obj = set.Lap(obj,val)
            obj = setParameter(obj,'Lap',val,0,'real');
        end
        function res = get.Lap(obj)
            res = getParameter(obj,'Lap');
        end
        function obj = set.Wvar(obj,val)
            obj = setParameter(obj,'Wvar',val,0,'real');
        end
        function res = get.Wvar(obj)
            res = getParameter(obj,'Wvar');
        end
        function obj = set.Wot(obj,val)
            obj = setParameter(obj,'Wot',val,0,'real');
        end
        function res = get.Wot(obj)
            res = getParameter(obj,'Wot');
        end
        function obj = set.Tr(obj,val)
            obj = setParameter(obj,'Tr',val,0,'real');
        end
        function res = get.Tr(obj)
            res = getParameter(obj,'Tr');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Dta(obj,val)
            obj = setParameter(obj,'Dta',val,0,'real');
        end
        function res = get.Dta(obj)
            res = getParameter(obj,'Dta');
        end
        function obj = set.Vfb(obj,val)
            obj = setParameter(obj,'Vfb',val,0,'real');
        end
        function res = get.Vfb(obj)
            res = getParameter(obj,'Vfb');
        end
        function obj = set.Stvfb(obj,val)
            obj = setParameter(obj,'Stvfb',val,0,'real');
        end
        function res = get.Stvfb(obj)
            res = getParameter(obj,'Stvfb');
        end
        function obj = set.Kor(obj,val)
            obj = setParameter(obj,'Kor',val,0,'real');
        end
        function res = get.Kor(obj)
            res = getParameter(obj,'Kor');
        end
        function obj = set.Slko(obj,val)
            obj = setParameter(obj,'Slko',val,0,'real');
        end
        function res = get.Slko(obj)
            res = getParameter(obj,'Slko');
        end
        function obj = set.Sl2ko(obj,val)
            obj = setParameter(obj,'Sl2ko',val,0,'real');
        end
        function res = get.Sl2ko(obj)
            res = getParameter(obj,'Sl2ko');
        end
        function obj = set.Swko(obj,val)
            obj = setParameter(obj,'Swko',val,0,'real');
        end
        function res = get.Swko(obj)
            res = getParameter(obj,'Swko');
        end
        function obj = set.Kpinv(obj,val)
            obj = setParameter(obj,'Kpinv',val,0,'real');
        end
        function res = get.Kpinv(obj)
            res = getParameter(obj,'Kpinv');
        end
        function obj = set.Phibr(obj,val)
            obj = setParameter(obj,'Phibr',val,0,'real');
        end
        function res = get.Phibr(obj)
            res = getParameter(obj,'Phibr');
        end
        function obj = set.Stphib(obj,val)
            obj = setParameter(obj,'Stphib',val,0,'real');
        end
        function res = get.Stphib(obj)
            res = getParameter(obj,'Stphib');
        end
        function obj = set.Slphib(obj,val)
            obj = setParameter(obj,'Slphib',val,0,'real');
        end
        function res = get.Slphib(obj)
            res = getParameter(obj,'Slphib');
        end
        function obj = set.Sl2phib(obj,val)
            obj = setParameter(obj,'Sl2phib',val,0,'real');
        end
        function res = get.Sl2phib(obj)
            res = getParameter(obj,'Sl2phib');
        end
        function obj = set.Swphib(obj,val)
            obj = setParameter(obj,'Swphib',val,0,'real');
        end
        function res = get.Swphib(obj)
            res = getParameter(obj,'Swphib');
        end
        function obj = set.Betsq(obj,val)
            obj = setParameter(obj,'Betsq',val,0,'real');
        end
        function res = get.Betsq(obj)
            res = getParameter(obj,'Betsq');
        end
        function obj = set.Etabetr(obj,val)
            obj = setParameter(obj,'Etabetr',val,0,'real');
        end
        function res = get.Etabetr(obj)
            res = getParameter(obj,'Etabetr');
        end
        function obj = set.Sletabet(obj,val)
            obj = setParameter(obj,'Sletabet',val,0,'real');
        end
        function res = get.Sletabet(obj)
            res = getParameter(obj,'Sletabet');
        end
        function obj = set.Fbet1(obj,val)
            obj = setParameter(obj,'Fbet1',val,0,'real');
        end
        function res = get.Fbet1(obj)
            res = getParameter(obj,'Fbet1');
        end
        function obj = set.Lp1(obj,val)
            obj = setParameter(obj,'Lp1',val,0,'real');
        end
        function res = get.Lp1(obj)
            res = getParameter(obj,'Lp1');
        end
        function obj = set.Fbet2(obj,val)
            obj = setParameter(obj,'Fbet2',val,0,'real');
        end
        function res = get.Fbet2(obj)
            res = getParameter(obj,'Fbet2');
        end
        function obj = set.Lp2(obj,val)
            obj = setParameter(obj,'Lp2',val,0,'real');
        end
        function res = get.Lp2(obj)
            res = getParameter(obj,'Lp2');
        end
        function obj = set.Thesrr(obj,val)
            obj = setParameter(obj,'Thesrr',val,0,'real');
        end
        function res = get.Thesrr(obj)
            res = getParameter(obj,'Thesrr');
        end
        function obj = set.Etasr(obj,val)
            obj = setParameter(obj,'Etasr',val,0,'real');
        end
        function res = get.Etasr(obj)
            res = getParameter(obj,'Etasr');
        end
        function obj = set.Swthesr(obj,val)
            obj = setParameter(obj,'Swthesr',val,0,'real');
        end
        function res = get.Swthesr(obj)
            res = getParameter(obj,'Swthesr');
        end
        function obj = set.Thephr(obj,val)
            obj = setParameter(obj,'Thephr',val,0,'real');
        end
        function res = get.Thephr(obj)
            res = getParameter(obj,'Thephr');
        end
        function obj = set.Etaph(obj,val)
            obj = setParameter(obj,'Etaph',val,0,'real');
        end
        function res = get.Etaph(obj)
            res = getParameter(obj,'Etaph');
        end
        function obj = set.Swtheph(obj,val)
            obj = setParameter(obj,'Swtheph',val,0,'real');
        end
        function res = get.Swtheph(obj)
            res = getParameter(obj,'Swtheph');
        end
        function obj = set.Etamobr(obj,val)
            obj = setParameter(obj,'Etamobr',val,0,'real');
        end
        function res = get.Etamobr(obj)
            res = getParameter(obj,'Etamobr');
        end
        function obj = set.Stetamob(obj,val)
            obj = setParameter(obj,'Stetamob',val,0,'real');
        end
        function res = get.Stetamob(obj)
            res = getParameter(obj,'Stetamob');
        end
        function obj = set.Swetamob(obj,val)
            obj = setParameter(obj,'Swetamob',val,0,'real');
        end
        function res = get.Swetamob(obj)
            res = getParameter(obj,'Swetamob');
        end
        function obj = set.Nu(obj,val)
            obj = setParameter(obj,'Nu',val,0,'real');
        end
        function res = get.Nu(obj)
            res = getParameter(obj,'Nu');
        end
        function obj = set.Nuexp(obj,val)
            obj = setParameter(obj,'Nuexp',val,0,'real');
        end
        function res = get.Nuexp(obj)
            res = getParameter(obj,'Nuexp');
        end
        function obj = set.Therr(obj,val)
            obj = setParameter(obj,'Therr',val,0,'real');
        end
        function res = get.Therr(obj)
            res = getParameter(obj,'Therr');
        end
        function obj = set.Etar(obj,val)
            obj = setParameter(obj,'Etar',val,0,'real');
        end
        function res = get.Etar(obj)
            res = getParameter(obj,'Etar');
        end
        function obj = set.Swther(obj,val)
            obj = setParameter(obj,'Swther',val,0,'real');
        end
        function res = get.Swther(obj)
            res = getParameter(obj,'Swther');
        end
        function obj = set.Ther1(obj,val)
            obj = setParameter(obj,'Ther1',val,0,'real');
        end
        function res = get.Ther1(obj)
            res = getParameter(obj,'Ther1');
        end
        function obj = set.Ther2(obj,val)
            obj = setParameter(obj,'Ther2',val,0,'real');
        end
        function res = get.Ther2(obj)
            res = getParameter(obj,'Ther2');
        end
        function obj = set.Thesatr(obj,val)
            obj = setParameter(obj,'Thesatr',val,0,'real');
        end
        function res = get.Thesatr(obj)
            res = getParameter(obj,'Thesatr');
        end
        function obj = set.Etasat(obj,val)
            obj = setParameter(obj,'Etasat',val,0,'real');
        end
        function res = get.Etasat(obj)
            res = getParameter(obj,'Etasat');
        end
        function obj = set.Slthesat(obj,val)
            obj = setParameter(obj,'Slthesat',val,0,'real');
        end
        function res = get.Slthesat(obj)
            res = getParameter(obj,'Slthesat');
        end
        function obj = set.Thesatexp(obj,val)
            obj = setParameter(obj,'Thesatexp',val,0,'real');
        end
        function res = get.Thesatexp(obj)
            res = getParameter(obj,'Thesatexp');
        end
        function obj = set.Swthesat(obj,val)
            obj = setParameter(obj,'Swthesat',val,0,'real');
        end
        function res = get.Swthesat(obj)
            res = getParameter(obj,'Swthesat');
        end
        function obj = set.Thethr(obj,val)
            obj = setParameter(obj,'Thethr',val,0,'real');
        end
        function res = get.Thethr(obj)
            res = getParameter(obj,'Thethr');
        end
        function obj = set.Thethexp(obj,val)
            obj = setParameter(obj,'Thethexp',val,0,'real');
        end
        function res = get.Thethexp(obj)
            res = getParameter(obj,'Thethexp');
        end
        function obj = set.Swtheth(obj,val)
            obj = setParameter(obj,'Swtheth',val,0,'real');
        end
        function res = get.Swtheth(obj)
            res = getParameter(obj,'Swtheth');
        end
        function obj = set.Sdiblo(obj,val)
            obj = setParameter(obj,'Sdiblo',val,0,'real');
        end
        function res = get.Sdiblo(obj)
            res = getParameter(obj,'Sdiblo');
        end
        function obj = set.Sdiblexp(obj,val)
            obj = setParameter(obj,'Sdiblexp',val,0,'real');
        end
        function res = get.Sdiblexp(obj)
            res = getParameter(obj,'Sdiblexp');
        end
        function obj = set.Mor(obj,val)
            obj = setParameter(obj,'Mor',val,0,'real');
        end
        function res = get.Mor(obj)
            res = getParameter(obj,'Mor');
        end
        function obj = set.Moexp(obj,val)
            obj = setParameter(obj,'Moexp',val,0,'real');
        end
        function res = get.Moexp(obj)
            res = getParameter(obj,'Moexp');
        end
        function obj = set.Ssfr(obj,val)
            obj = setParameter(obj,'Ssfr',val,0,'real');
        end
        function res = get.Ssfr(obj)
            res = getParameter(obj,'Ssfr');
        end
        function obj = set.Slssf(obj,val)
            obj = setParameter(obj,'Slssf',val,0,'real');
        end
        function res = get.Slssf(obj)
            res = getParameter(obj,'Slssf');
        end
        function obj = set.Swssf(obj,val)
            obj = setParameter(obj,'Swssf',val,0,'real');
        end
        function res = get.Swssf(obj)
            res = getParameter(obj,'Swssf');
        end
        function obj = set.Alpr(obj,val)
            obj = setParameter(obj,'Alpr',val,0,'real');
        end
        function res = get.Alpr(obj)
            res = getParameter(obj,'Alpr');
        end
        function obj = set.Slalp(obj,val)
            obj = setParameter(obj,'Slalp',val,0,'real');
        end
        function res = get.Slalp(obj)
            res = getParameter(obj,'Slalp');
        end
        function obj = set.Alpexp(obj,val)
            obj = setParameter(obj,'Alpexp',val,0,'real');
        end
        function res = get.Alpexp(obj)
            res = getParameter(obj,'Alpexp');
        end
        function obj = set.Swalp(obj,val)
            obj = setParameter(obj,'Swalp',val,0,'real');
        end
        function res = get.Swalp(obj)
            res = getParameter(obj,'Swalp');
        end
        function obj = set.Vp(obj,val)
            obj = setParameter(obj,'Vp',val,0,'real');
        end
        function res = get.Vp(obj)
            res = getParameter(obj,'Vp');
        end
        function obj = set.Lmin(obj,val)
            obj = setParameter(obj,'Lmin',val,0,'real');
        end
        function res = get.Lmin(obj)
            res = getParameter(obj,'Lmin');
        end
        function obj = set.Mexp(obj,val)
            obj = setParameter(obj,'Mexp',val,0,'real');
        end
        function res = get.Mexp(obj)
            res = getParameter(obj,'Mexp');
        end
        function obj = set.A1r(obj,val)
            obj = setParameter(obj,'A1r',val,0,'real');
        end
        function res = get.A1r(obj)
            res = getParameter(obj,'A1r');
        end
        function obj = set.Sta1(obj,val)
            obj = setParameter(obj,'Sta1',val,0,'real');
        end
        function res = get.Sta1(obj)
            res = getParameter(obj,'Sta1');
        end
        function obj = set.Sla1(obj,val)
            obj = setParameter(obj,'Sla1',val,0,'real');
        end
        function res = get.Sla1(obj)
            res = getParameter(obj,'Sla1');
        end
        function obj = set.Swa1(obj,val)
            obj = setParameter(obj,'Swa1',val,0,'real');
        end
        function res = get.Swa1(obj)
            res = getParameter(obj,'Swa1');
        end
        function obj = set.A2r(obj,val)
            obj = setParameter(obj,'A2r',val,0,'real');
        end
        function res = get.A2r(obj)
            res = getParameter(obj,'A2r');
        end
        function obj = set.Sla2(obj,val)
            obj = setParameter(obj,'Sla2',val,0,'real');
        end
        function res = get.Sla2(obj)
            res = getParameter(obj,'Sla2');
        end
        function obj = set.Swa2(obj,val)
            obj = setParameter(obj,'Swa2',val,0,'real');
        end
        function res = get.Swa2(obj)
            res = getParameter(obj,'Swa2');
        end
        function obj = set.A3r(obj,val)
            obj = setParameter(obj,'A3r',val,0,'real');
        end
        function res = get.A3r(obj)
            res = getParameter(obj,'A3r');
        end
        function obj = set.Sla3(obj,val)
            obj = setParameter(obj,'Sla3',val,0,'real');
        end
        function res = get.Sla3(obj)
            res = getParameter(obj,'Sla3');
        end
        function obj = set.Swa3(obj,val)
            obj = setParameter(obj,'Swa3',val,0,'real');
        end
        function res = get.Swa3(obj)
            res = getParameter(obj,'Swa3');
        end
        function obj = set.Iginvr(obj,val)
            obj = setParameter(obj,'Iginvr',val,0,'real');
        end
        function res = get.Iginvr(obj)
            res = getParameter(obj,'Iginvr');
        end
        function obj = set.Binv(obj,val)
            obj = setParameter(obj,'Binv',val,0,'real');
        end
        function res = get.Binv(obj)
            res = getParameter(obj,'Binv');
        end
        function obj = set.Igaccr(obj,val)
            obj = setParameter(obj,'Igaccr',val,0,'real');
        end
        function res = get.Igaccr(obj)
            res = getParameter(obj,'Igaccr');
        end
        function obj = set.Bacc(obj,val)
            obj = setParameter(obj,'Bacc',val,0,'real');
        end
        function res = get.Bacc(obj)
            res = getParameter(obj,'Bacc');
        end
        function obj = set.Vfbov(obj,val)
            obj = setParameter(obj,'Vfbov',val,0,'real');
        end
        function res = get.Vfbov(obj)
            res = getParameter(obj,'Vfbov');
        end
        function obj = set.Kov(obj,val)
            obj = setParameter(obj,'Kov',val,0,'real');
        end
        function res = get.Kov(obj)
            res = getParameter(obj,'Kov');
        end
        function obj = set.Igovr(obj,val)
            obj = setParameter(obj,'Igovr',val,0,'real');
        end
        function res = get.Igovr(obj)
            res = getParameter(obj,'Igovr');
        end
        function obj = set.Tox(obj,val)
            obj = setParameter(obj,'Tox',val,0,'real');
        end
        function res = get.Tox(obj)
            res = getParameter(obj,'Tox');
        end
        function obj = set.Col(obj,val)
            obj = setParameter(obj,'Col',val,0,'real');
        end
        function res = get.Col(obj)
            res = getParameter(obj,'Col');
        end
        function obj = set.Cox(obj,val)
            obj = setParameter(obj,'Cox',val,0,'real');
        end
        function res = get.Cox(obj)
            res = getParameter(obj,'Cox');
        end
        function obj = set.Cgdo(obj,val)
            obj = setParameter(obj,'Cgdo',val,0,'real');
        end
        function res = get.Cgdo(obj)
            res = getParameter(obj,'Cgdo');
        end
        function obj = set.Cgso(obj,val)
            obj = setParameter(obj,'Cgso',val,0,'real');
        end
        function res = get.Cgso(obj)
            res = getParameter(obj,'Cgso');
        end
        function obj = set.Gatenoise(obj,val)
            obj = setParameter(obj,'Gatenoise',val,0,'real');
        end
        function res = get.Gatenoise(obj)
            res = getParameter(obj,'Gatenoise');
        end
        function obj = set.Nt(obj,val)
            obj = setParameter(obj,'Nt',val,0,'real');
        end
        function res = get.Nt(obj)
            res = getParameter(obj,'Nt');
        end
        function obj = set.Nfar(obj,val)
            obj = setParameter(obj,'Nfar',val,0,'real');
        end
        function res = get.Nfar(obj)
            res = getParameter(obj,'Nfar');
        end
        function obj = set.Nfbr(obj,val)
            obj = setParameter(obj,'Nfbr',val,0,'real');
        end
        function res = get.Nfbr(obj)
            res = getParameter(obj,'Nfbr');
        end
        function obj = set.Nfcr(obj,val)
            obj = setParameter(obj,'Nfcr',val,0,'real');
        end
        function res = get.Nfcr(obj)
            res = getParameter(obj,'Nfcr');
        end
        function obj = set.Poko(obj,val)
            obj = setParameter(obj,'Poko',val,0,'real');
        end
        function res = get.Poko(obj)
            res = getParameter(obj,'Poko');
        end
        function obj = set.Plko(obj,val)
            obj = setParameter(obj,'Plko',val,0,'real');
        end
        function res = get.Plko(obj)
            res = getParameter(obj,'Plko');
        end
        function obj = set.Pwko(obj,val)
            obj = setParameter(obj,'Pwko',val,0,'real');
        end
        function res = get.Pwko(obj)
            res = getParameter(obj,'Pwko');
        end
        function obj = set.Plwko(obj,val)
            obj = setParameter(obj,'Plwko',val,0,'real');
        end
        function res = get.Plwko(obj)
            res = getParameter(obj,'Plwko');
        end
        function obj = set.Pophib(obj,val)
            obj = setParameter(obj,'Pophib',val,0,'real');
        end
        function res = get.Pophib(obj)
            res = getParameter(obj,'Pophib');
        end
        function obj = set.Plphib(obj,val)
            obj = setParameter(obj,'Plphib',val,0,'real');
        end
        function res = get.Plphib(obj)
            res = getParameter(obj,'Plphib');
        end
        function obj = set.Pwphib(obj,val)
            obj = setParameter(obj,'Pwphib',val,0,'real');
        end
        function res = get.Pwphib(obj)
            res = getParameter(obj,'Pwphib');
        end
        function obj = set.Plwphib(obj,val)
            obj = setParameter(obj,'Plwphib',val,0,'real');
        end
        function res = get.Plwphib(obj)
            res = getParameter(obj,'Plwphib');
        end
        function obj = set.Pobet(obj,val)
            obj = setParameter(obj,'Pobet',val,0,'real');
        end
        function res = get.Pobet(obj)
            res = getParameter(obj,'Pobet');
        end
        function obj = set.Plbet(obj,val)
            obj = setParameter(obj,'Plbet',val,0,'real');
        end
        function res = get.Plbet(obj)
            res = getParameter(obj,'Plbet');
        end
        function obj = set.Pwbet(obj,val)
            obj = setParameter(obj,'Pwbet',val,0,'real');
        end
        function res = get.Pwbet(obj)
            res = getParameter(obj,'Pwbet');
        end
        function obj = set.Plwbet(obj,val)
            obj = setParameter(obj,'Plwbet',val,0,'real');
        end
        function res = get.Plwbet(obj)
            res = getParameter(obj,'Plwbet');
        end
        function obj = set.Pothesr(obj,val)
            obj = setParameter(obj,'Pothesr',val,0,'real');
        end
        function res = get.Pothesr(obj)
            res = getParameter(obj,'Pothesr');
        end
        function obj = set.Plthesr(obj,val)
            obj = setParameter(obj,'Plthesr',val,0,'real');
        end
        function res = get.Plthesr(obj)
            res = getParameter(obj,'Plthesr');
        end
        function obj = set.Pwthesr(obj,val)
            obj = setParameter(obj,'Pwthesr',val,0,'real');
        end
        function res = get.Pwthesr(obj)
            res = getParameter(obj,'Pwthesr');
        end
        function obj = set.Plwthesr(obj,val)
            obj = setParameter(obj,'Plwthesr',val,0,'real');
        end
        function res = get.Plwthesr(obj)
            res = getParameter(obj,'Plwthesr');
        end
        function obj = set.Potheph(obj,val)
            obj = setParameter(obj,'Potheph',val,0,'real');
        end
        function res = get.Potheph(obj)
            res = getParameter(obj,'Potheph');
        end
        function obj = set.Pltheph(obj,val)
            obj = setParameter(obj,'Pltheph',val,0,'real');
        end
        function res = get.Pltheph(obj)
            res = getParameter(obj,'Pltheph');
        end
        function obj = set.Pwtheph(obj,val)
            obj = setParameter(obj,'Pwtheph',val,0,'real');
        end
        function res = get.Pwtheph(obj)
            res = getParameter(obj,'Pwtheph');
        end
        function obj = set.Plwtheph(obj,val)
            obj = setParameter(obj,'Plwtheph',val,0,'real');
        end
        function res = get.Plwtheph(obj)
            res = getParameter(obj,'Plwtheph');
        end
        function obj = set.Poetamob(obj,val)
            obj = setParameter(obj,'Poetamob',val,0,'real');
        end
        function res = get.Poetamob(obj)
            res = getParameter(obj,'Poetamob');
        end
        function obj = set.Pletamob(obj,val)
            obj = setParameter(obj,'Pletamob',val,0,'real');
        end
        function res = get.Pletamob(obj)
            res = getParameter(obj,'Pletamob');
        end
        function obj = set.Pwetamob(obj,val)
            obj = setParameter(obj,'Pwetamob',val,0,'real');
        end
        function res = get.Pwetamob(obj)
            res = getParameter(obj,'Pwetamob');
        end
        function obj = set.Plwetamob(obj,val)
            obj = setParameter(obj,'Plwetamob',val,0,'real');
        end
        function res = get.Plwetamob(obj)
            res = getParameter(obj,'Plwetamob');
        end
        function obj = set.Pother(obj,val)
            obj = setParameter(obj,'Pother',val,0,'real');
        end
        function res = get.Pother(obj)
            res = getParameter(obj,'Pother');
        end
        function obj = set.Plther(obj,val)
            obj = setParameter(obj,'Plther',val,0,'real');
        end
        function res = get.Plther(obj)
            res = getParameter(obj,'Plther');
        end
        function obj = set.Pwther(obj,val)
            obj = setParameter(obj,'Pwther',val,0,'real');
        end
        function res = get.Pwther(obj)
            res = getParameter(obj,'Pwther');
        end
        function obj = set.Plwther(obj,val)
            obj = setParameter(obj,'Plwther',val,0,'real');
        end
        function res = get.Plwther(obj)
            res = getParameter(obj,'Plwther');
        end
        function obj = set.Pothesat(obj,val)
            obj = setParameter(obj,'Pothesat',val,0,'real');
        end
        function res = get.Pothesat(obj)
            res = getParameter(obj,'Pothesat');
        end
        function obj = set.Plthesat(obj,val)
            obj = setParameter(obj,'Plthesat',val,0,'real');
        end
        function res = get.Plthesat(obj)
            res = getParameter(obj,'Plthesat');
        end
        function obj = set.Pwthesat(obj,val)
            obj = setParameter(obj,'Pwthesat',val,0,'real');
        end
        function res = get.Pwthesat(obj)
            res = getParameter(obj,'Pwthesat');
        end
        function obj = set.Plwthesat(obj,val)
            obj = setParameter(obj,'Plwthesat',val,0,'real');
        end
        function res = get.Plwthesat(obj)
            res = getParameter(obj,'Plwthesat');
        end
        function obj = set.Potheth(obj,val)
            obj = setParameter(obj,'Potheth',val,0,'real');
        end
        function res = get.Potheth(obj)
            res = getParameter(obj,'Potheth');
        end
        function obj = set.Pltheth(obj,val)
            obj = setParameter(obj,'Pltheth',val,0,'real');
        end
        function res = get.Pltheth(obj)
            res = getParameter(obj,'Pltheth');
        end
        function obj = set.Pwtheth(obj,val)
            obj = setParameter(obj,'Pwtheth',val,0,'real');
        end
        function res = get.Pwtheth(obj)
            res = getParameter(obj,'Pwtheth');
        end
        function obj = set.Plwtheth(obj,val)
            obj = setParameter(obj,'Plwtheth',val,0,'real');
        end
        function res = get.Plwtheth(obj)
            res = getParameter(obj,'Plwtheth');
        end
        function obj = set.Posdibl(obj,val)
            obj = setParameter(obj,'Posdibl',val,0,'real');
        end
        function res = get.Posdibl(obj)
            res = getParameter(obj,'Posdibl');
        end
        function obj = set.Plsdibl(obj,val)
            obj = setParameter(obj,'Plsdibl',val,0,'real');
        end
        function res = get.Plsdibl(obj)
            res = getParameter(obj,'Plsdibl');
        end
        function obj = set.Pwsdibl(obj,val)
            obj = setParameter(obj,'Pwsdibl',val,0,'real');
        end
        function res = get.Pwsdibl(obj)
            res = getParameter(obj,'Pwsdibl');
        end
        function obj = set.Plwsdibl(obj,val)
            obj = setParameter(obj,'Plwsdibl',val,0,'real');
        end
        function res = get.Plwsdibl(obj)
            res = getParameter(obj,'Plwsdibl');
        end
        function obj = set.Pomo(obj,val)
            obj = setParameter(obj,'Pomo',val,0,'real');
        end
        function res = get.Pomo(obj)
            res = getParameter(obj,'Pomo');
        end
        function obj = set.Plmo(obj,val)
            obj = setParameter(obj,'Plmo',val,0,'real');
        end
        function res = get.Plmo(obj)
            res = getParameter(obj,'Plmo');
        end
        function obj = set.Pwmo(obj,val)
            obj = setParameter(obj,'Pwmo',val,0,'real');
        end
        function res = get.Pwmo(obj)
            res = getParameter(obj,'Pwmo');
        end
        function obj = set.Plwmo(obj,val)
            obj = setParameter(obj,'Plwmo',val,0,'real');
        end
        function res = get.Plwmo(obj)
            res = getParameter(obj,'Plwmo');
        end
        function obj = set.Possf(obj,val)
            obj = setParameter(obj,'Possf',val,0,'real');
        end
        function res = get.Possf(obj)
            res = getParameter(obj,'Possf');
        end
        function obj = set.Plssf(obj,val)
            obj = setParameter(obj,'Plssf',val,0,'real');
        end
        function res = get.Plssf(obj)
            res = getParameter(obj,'Plssf');
        end
        function obj = set.Pwssf(obj,val)
            obj = setParameter(obj,'Pwssf',val,0,'real');
        end
        function res = get.Pwssf(obj)
            res = getParameter(obj,'Pwssf');
        end
        function obj = set.Plwssf(obj,val)
            obj = setParameter(obj,'Plwssf',val,0,'real');
        end
        function res = get.Plwssf(obj)
            res = getParameter(obj,'Plwssf');
        end
        function obj = set.Poalp(obj,val)
            obj = setParameter(obj,'Poalp',val,0,'real');
        end
        function res = get.Poalp(obj)
            res = getParameter(obj,'Poalp');
        end
        function obj = set.Plalp(obj,val)
            obj = setParameter(obj,'Plalp',val,0,'real');
        end
        function res = get.Plalp(obj)
            res = getParameter(obj,'Plalp');
        end
        function obj = set.Pwalp(obj,val)
            obj = setParameter(obj,'Pwalp',val,0,'real');
        end
        function res = get.Pwalp(obj)
            res = getParameter(obj,'Pwalp');
        end
        function obj = set.Plwalp(obj,val)
            obj = setParameter(obj,'Plwalp',val,0,'real');
        end
        function res = get.Plwalp(obj)
            res = getParameter(obj,'Plwalp');
        end
        function obj = set.Pomexp(obj,val)
            obj = setParameter(obj,'Pomexp',val,0,'real');
        end
        function res = get.Pomexp(obj)
            res = getParameter(obj,'Pomexp');
        end
        function obj = set.Plmexp(obj,val)
            obj = setParameter(obj,'Plmexp',val,0,'real');
        end
        function res = get.Plmexp(obj)
            res = getParameter(obj,'Plmexp');
        end
        function obj = set.Pwmexp(obj,val)
            obj = setParameter(obj,'Pwmexp',val,0,'real');
        end
        function res = get.Pwmexp(obj)
            res = getParameter(obj,'Pwmexp');
        end
        function obj = set.Plwmexp(obj,val)
            obj = setParameter(obj,'Plwmexp',val,0,'real');
        end
        function res = get.Plwmexp(obj)
            res = getParameter(obj,'Plwmexp');
        end
        function obj = set.Poa1(obj,val)
            obj = setParameter(obj,'Poa1',val,0,'real');
        end
        function res = get.Poa1(obj)
            res = getParameter(obj,'Poa1');
        end
        function obj = set.Pla1(obj,val)
            obj = setParameter(obj,'Pla1',val,0,'real');
        end
        function res = get.Pla1(obj)
            res = getParameter(obj,'Pla1');
        end
        function obj = set.Pwa1(obj,val)
            obj = setParameter(obj,'Pwa1',val,0,'real');
        end
        function res = get.Pwa1(obj)
            res = getParameter(obj,'Pwa1');
        end
        function obj = set.Plwa1(obj,val)
            obj = setParameter(obj,'Plwa1',val,0,'real');
        end
        function res = get.Plwa1(obj)
            res = getParameter(obj,'Plwa1');
        end
        function obj = set.Poa2(obj,val)
            obj = setParameter(obj,'Poa2',val,0,'real');
        end
        function res = get.Poa2(obj)
            res = getParameter(obj,'Poa2');
        end
        function obj = set.Pla2(obj,val)
            obj = setParameter(obj,'Pla2',val,0,'real');
        end
        function res = get.Pla2(obj)
            res = getParameter(obj,'Pla2');
        end
        function obj = set.Pwa2(obj,val)
            obj = setParameter(obj,'Pwa2',val,0,'real');
        end
        function res = get.Pwa2(obj)
            res = getParameter(obj,'Pwa2');
        end
        function obj = set.Plwa2(obj,val)
            obj = setParameter(obj,'Plwa2',val,0,'real');
        end
        function res = get.Plwa2(obj)
            res = getParameter(obj,'Plwa2');
        end
        function obj = set.Poa3(obj,val)
            obj = setParameter(obj,'Poa3',val,0,'real');
        end
        function res = get.Poa3(obj)
            res = getParameter(obj,'Poa3');
        end
        function obj = set.Pla3(obj,val)
            obj = setParameter(obj,'Pla3',val,0,'real');
        end
        function res = get.Pla3(obj)
            res = getParameter(obj,'Pla3');
        end
        function obj = set.Pwa3(obj,val)
            obj = setParameter(obj,'Pwa3',val,0,'real');
        end
        function res = get.Pwa3(obj)
            res = getParameter(obj,'Pwa3');
        end
        function obj = set.Plwa3(obj,val)
            obj = setParameter(obj,'Plwa3',val,0,'real');
        end
        function res = get.Plwa3(obj)
            res = getParameter(obj,'Plwa3');
        end
        function obj = set.Poiginv(obj,val)
            obj = setParameter(obj,'Poiginv',val,0,'real');
        end
        function res = get.Poiginv(obj)
            res = getParameter(obj,'Poiginv');
        end
        function obj = set.Pliginv(obj,val)
            obj = setParameter(obj,'Pliginv',val,0,'real');
        end
        function res = get.Pliginv(obj)
            res = getParameter(obj,'Pliginv');
        end
        function obj = set.Pwiginv(obj,val)
            obj = setParameter(obj,'Pwiginv',val,0,'real');
        end
        function res = get.Pwiginv(obj)
            res = getParameter(obj,'Pwiginv');
        end
        function obj = set.Plwiginv(obj,val)
            obj = setParameter(obj,'Plwiginv',val,0,'real');
        end
        function res = get.Plwiginv(obj)
            res = getParameter(obj,'Plwiginv');
        end
        function obj = set.Pobinv(obj,val)
            obj = setParameter(obj,'Pobinv',val,0,'real');
        end
        function res = get.Pobinv(obj)
            res = getParameter(obj,'Pobinv');
        end
        function obj = set.Plbinv(obj,val)
            obj = setParameter(obj,'Plbinv',val,0,'real');
        end
        function res = get.Plbinv(obj)
            res = getParameter(obj,'Plbinv');
        end
        function obj = set.Pwbinv(obj,val)
            obj = setParameter(obj,'Pwbinv',val,0,'real');
        end
        function res = get.Pwbinv(obj)
            res = getParameter(obj,'Pwbinv');
        end
        function obj = set.Plwbinv(obj,val)
            obj = setParameter(obj,'Plwbinv',val,0,'real');
        end
        function res = get.Plwbinv(obj)
            res = getParameter(obj,'Plwbinv');
        end
        function obj = set.Poigacc(obj,val)
            obj = setParameter(obj,'Poigacc',val,0,'real');
        end
        function res = get.Poigacc(obj)
            res = getParameter(obj,'Poigacc');
        end
        function obj = set.Pligacc(obj,val)
            obj = setParameter(obj,'Pligacc',val,0,'real');
        end
        function res = get.Pligacc(obj)
            res = getParameter(obj,'Pligacc');
        end
        function obj = set.Pwigacc(obj,val)
            obj = setParameter(obj,'Pwigacc',val,0,'real');
        end
        function res = get.Pwigacc(obj)
            res = getParameter(obj,'Pwigacc');
        end
        function obj = set.Plwigacc(obj,val)
            obj = setParameter(obj,'Plwigacc',val,0,'real');
        end
        function res = get.Plwigacc(obj)
            res = getParameter(obj,'Plwigacc');
        end
        function obj = set.Pobacc(obj,val)
            obj = setParameter(obj,'Pobacc',val,0,'real');
        end
        function res = get.Pobacc(obj)
            res = getParameter(obj,'Pobacc');
        end
        function obj = set.Plbacc(obj,val)
            obj = setParameter(obj,'Plbacc',val,0,'real');
        end
        function res = get.Plbacc(obj)
            res = getParameter(obj,'Plbacc');
        end
        function obj = set.Pwbacc(obj,val)
            obj = setParameter(obj,'Pwbacc',val,0,'real');
        end
        function res = get.Pwbacc(obj)
            res = getParameter(obj,'Pwbacc');
        end
        function obj = set.Plwbacc(obj,val)
            obj = setParameter(obj,'Plwbacc',val,0,'real');
        end
        function res = get.Plwbacc(obj)
            res = getParameter(obj,'Plwbacc');
        end
        function obj = set.Poigov(obj,val)
            obj = setParameter(obj,'Poigov',val,0,'real');
        end
        function res = get.Poigov(obj)
            res = getParameter(obj,'Poigov');
        end
        function obj = set.Pligov(obj,val)
            obj = setParameter(obj,'Pligov',val,0,'real');
        end
        function res = get.Pligov(obj)
            res = getParameter(obj,'Pligov');
        end
        function obj = set.Pwigov(obj,val)
            obj = setParameter(obj,'Pwigov',val,0,'real');
        end
        function res = get.Pwigov(obj)
            res = getParameter(obj,'Pwigov');
        end
        function obj = set.Plwigov(obj,val)
            obj = setParameter(obj,'Plwigov',val,0,'real');
        end
        function res = get.Plwigov(obj)
            res = getParameter(obj,'Plwigov');
        end
        function obj = set.Pocox(obj,val)
            obj = setParameter(obj,'Pocox',val,0,'real');
        end
        function res = get.Pocox(obj)
            res = getParameter(obj,'Pocox');
        end
        function obj = set.Plcox(obj,val)
            obj = setParameter(obj,'Plcox',val,0,'real');
        end
        function res = get.Plcox(obj)
            res = getParameter(obj,'Plcox');
        end
        function obj = set.Pwcox(obj,val)
            obj = setParameter(obj,'Pwcox',val,0,'real');
        end
        function res = get.Pwcox(obj)
            res = getParameter(obj,'Pwcox');
        end
        function obj = set.Plwcox(obj,val)
            obj = setParameter(obj,'Plwcox',val,0,'real');
        end
        function res = get.Plwcox(obj)
            res = getParameter(obj,'Plwcox');
        end
        function obj = set.Pocgdo(obj,val)
            obj = setParameter(obj,'Pocgdo',val,0,'real');
        end
        function res = get.Pocgdo(obj)
            res = getParameter(obj,'Pocgdo');
        end
        function obj = set.Plcgdo(obj,val)
            obj = setParameter(obj,'Plcgdo',val,0,'real');
        end
        function res = get.Plcgdo(obj)
            res = getParameter(obj,'Plcgdo');
        end
        function obj = set.Pwcgdo(obj,val)
            obj = setParameter(obj,'Pwcgdo',val,0,'real');
        end
        function res = get.Pwcgdo(obj)
            res = getParameter(obj,'Pwcgdo');
        end
        function obj = set.Plwcgdo(obj,val)
            obj = setParameter(obj,'Plwcgdo',val,0,'real');
        end
        function res = get.Plwcgdo(obj)
            res = getParameter(obj,'Plwcgdo');
        end
        function obj = set.Pocgso(obj,val)
            obj = setParameter(obj,'Pocgso',val,0,'real');
        end
        function res = get.Pocgso(obj)
            res = getParameter(obj,'Pocgso');
        end
        function obj = set.Plcgso(obj,val)
            obj = setParameter(obj,'Plcgso',val,0,'real');
        end
        function res = get.Plcgso(obj)
            res = getParameter(obj,'Plcgso');
        end
        function obj = set.Pwcgso(obj,val)
            obj = setParameter(obj,'Pwcgso',val,0,'real');
        end
        function res = get.Pwcgso(obj)
            res = getParameter(obj,'Pwcgso');
        end
        function obj = set.Plwcgso(obj,val)
            obj = setParameter(obj,'Plwcgso',val,0,'real');
        end
        function res = get.Plwcgso(obj)
            res = getParameter(obj,'Plwcgso');
        end
        function obj = set.Ponfa(obj,val)
            obj = setParameter(obj,'Ponfa',val,0,'real');
        end
        function res = get.Ponfa(obj)
            res = getParameter(obj,'Ponfa');
        end
        function obj = set.Plnfa(obj,val)
            obj = setParameter(obj,'Plnfa',val,0,'real');
        end
        function res = get.Plnfa(obj)
            res = getParameter(obj,'Plnfa');
        end
        function obj = set.Pwnfa(obj,val)
            obj = setParameter(obj,'Pwnfa',val,0,'real');
        end
        function res = get.Pwnfa(obj)
            res = getParameter(obj,'Pwnfa');
        end
        function obj = set.Plwnfa(obj,val)
            obj = setParameter(obj,'Plwnfa',val,0,'real');
        end
        function res = get.Plwnfa(obj)
            res = getParameter(obj,'Plwnfa');
        end
        function obj = set.Ponfb(obj,val)
            obj = setParameter(obj,'Ponfb',val,0,'real');
        end
        function res = get.Ponfb(obj)
            res = getParameter(obj,'Ponfb');
        end
        function obj = set.Plnfb(obj,val)
            obj = setParameter(obj,'Plnfb',val,0,'real');
        end
        function res = get.Plnfb(obj)
            res = getParameter(obj,'Plnfb');
        end
        function obj = set.Pwnfb(obj,val)
            obj = setParameter(obj,'Pwnfb',val,0,'real');
        end
        function res = get.Pwnfb(obj)
            res = getParameter(obj,'Pwnfb');
        end
        function obj = set.Plwnfb(obj,val)
            obj = setParameter(obj,'Plwnfb',val,0,'real');
        end
        function res = get.Plwnfb(obj)
            res = getParameter(obj,'Plwnfb');
        end
        function obj = set.Ponfc(obj,val)
            obj = setParameter(obj,'Ponfc',val,0,'real');
        end
        function res = get.Ponfc(obj)
            res = getParameter(obj,'Ponfc');
        end
        function obj = set.Plnfc(obj,val)
            obj = setParameter(obj,'Plnfc',val,0,'real');
        end
        function res = get.Plnfc(obj)
            res = getParameter(obj,'Plnfc');
        end
        function obj = set.Pwnfc(obj,val)
            obj = setParameter(obj,'Pwnfc',val,0,'real');
        end
        function res = get.Pwnfc(obj)
            res = getParameter(obj,'Pwnfc');
        end
        function obj = set.Plwnfc(obj,val)
            obj = setParameter(obj,'Plwnfc',val,0,'real');
        end
        function res = get.Plwnfc(obj)
            res = getParameter(obj,'Plwnfc');
        end
        function obj = set.Potvfb(obj,val)
            obj = setParameter(obj,'Potvfb',val,0,'real');
        end
        function res = get.Potvfb(obj)
            res = getParameter(obj,'Potvfb');
        end
        function obj = set.Pltvfb(obj,val)
            obj = setParameter(obj,'Pltvfb',val,0,'real');
        end
        function res = get.Pltvfb(obj)
            res = getParameter(obj,'Pltvfb');
        end
        function obj = set.Pwtvfb(obj,val)
            obj = setParameter(obj,'Pwtvfb',val,0,'real');
        end
        function res = get.Pwtvfb(obj)
            res = getParameter(obj,'Pwtvfb');
        end
        function obj = set.Plwtvfb(obj,val)
            obj = setParameter(obj,'Plwtvfb',val,0,'real');
        end
        function res = get.Plwtvfb(obj)
            res = getParameter(obj,'Plwtvfb');
        end
        function obj = set.Potphib(obj,val)
            obj = setParameter(obj,'Potphib',val,0,'real');
        end
        function res = get.Potphib(obj)
            res = getParameter(obj,'Potphib');
        end
        function obj = set.Pltphib(obj,val)
            obj = setParameter(obj,'Pltphib',val,0,'real');
        end
        function res = get.Pltphib(obj)
            res = getParameter(obj,'Pltphib');
        end
        function obj = set.Pwtphib(obj,val)
            obj = setParameter(obj,'Pwtphib',val,0,'real');
        end
        function res = get.Pwtphib(obj)
            res = getParameter(obj,'Pwtphib');
        end
        function obj = set.Plwtphib(obj,val)
            obj = setParameter(obj,'Plwtphib',val,0,'real');
        end
        function res = get.Plwtphib(obj)
            res = getParameter(obj,'Plwtphib');
        end
        function obj = set.Potetabet(obj,val)
            obj = setParameter(obj,'Potetabet',val,0,'real');
        end
        function res = get.Potetabet(obj)
            res = getParameter(obj,'Potetabet');
        end
        function obj = set.Pltetabet(obj,val)
            obj = setParameter(obj,'Pltetabet',val,0,'real');
        end
        function res = get.Pltetabet(obj)
            res = getParameter(obj,'Pltetabet');
        end
        function obj = set.Pwtetabet(obj,val)
            obj = setParameter(obj,'Pwtetabet',val,0,'real');
        end
        function res = get.Pwtetabet(obj)
            res = getParameter(obj,'Pwtetabet');
        end
        function obj = set.Plwtetabet(obj,val)
            obj = setParameter(obj,'Plwtetabet',val,0,'real');
        end
        function res = get.Plwtetabet(obj)
            res = getParameter(obj,'Plwtetabet');
        end
        function obj = set.Potetasr(obj,val)
            obj = setParameter(obj,'Potetasr',val,0,'real');
        end
        function res = get.Potetasr(obj)
            res = getParameter(obj,'Potetasr');
        end
        function obj = set.Pltetasr(obj,val)
            obj = setParameter(obj,'Pltetasr',val,0,'real');
        end
        function res = get.Pltetasr(obj)
            res = getParameter(obj,'Pltetasr');
        end
        function obj = set.Pwtetasr(obj,val)
            obj = setParameter(obj,'Pwtetasr',val,0,'real');
        end
        function res = get.Pwtetasr(obj)
            res = getParameter(obj,'Pwtetasr');
        end
        function obj = set.Plwtetasr(obj,val)
            obj = setParameter(obj,'Plwtetasr',val,0,'real');
        end
        function res = get.Plwtetasr(obj)
            res = getParameter(obj,'Plwtetasr');
        end
        function obj = set.Potetaph(obj,val)
            obj = setParameter(obj,'Potetaph',val,0,'real');
        end
        function res = get.Potetaph(obj)
            res = getParameter(obj,'Potetaph');
        end
        function obj = set.Pltetaph(obj,val)
            obj = setParameter(obj,'Pltetaph',val,0,'real');
        end
        function res = get.Pltetaph(obj)
            res = getParameter(obj,'Pltetaph');
        end
        function obj = set.Pwtetaph(obj,val)
            obj = setParameter(obj,'Pwtetaph',val,0,'real');
        end
        function res = get.Pwtetaph(obj)
            res = getParameter(obj,'Pwtetaph');
        end
        function obj = set.Plwtetaph(obj,val)
            obj = setParameter(obj,'Plwtetaph',val,0,'real');
        end
        function res = get.Plwtetaph(obj)
            res = getParameter(obj,'Plwtetaph');
        end
        function obj = set.Potetamob(obj,val)
            obj = setParameter(obj,'Potetamob',val,0,'real');
        end
        function res = get.Potetamob(obj)
            res = getParameter(obj,'Potetamob');
        end
        function obj = set.Pltetamob(obj,val)
            obj = setParameter(obj,'Pltetamob',val,0,'real');
        end
        function res = get.Pltetamob(obj)
            res = getParameter(obj,'Pltetamob');
        end
        function obj = set.Pwtetamob(obj,val)
            obj = setParameter(obj,'Pwtetamob',val,0,'real');
        end
        function res = get.Pwtetamob(obj)
            res = getParameter(obj,'Pwtetamob');
        end
        function obj = set.Plwtetamob(obj,val)
            obj = setParameter(obj,'Plwtetamob',val,0,'real');
        end
        function res = get.Plwtetamob(obj)
            res = getParameter(obj,'Plwtetamob');
        end
        function obj = set.Potnuexp(obj,val)
            obj = setParameter(obj,'Potnuexp',val,0,'real');
        end
        function res = get.Potnuexp(obj)
            res = getParameter(obj,'Potnuexp');
        end
        function obj = set.Pltnuexp(obj,val)
            obj = setParameter(obj,'Pltnuexp',val,0,'real');
        end
        function res = get.Pltnuexp(obj)
            res = getParameter(obj,'Pltnuexp');
        end
        function obj = set.Pwtnuexp(obj,val)
            obj = setParameter(obj,'Pwtnuexp',val,0,'real');
        end
        function res = get.Pwtnuexp(obj)
            res = getParameter(obj,'Pwtnuexp');
        end
        function obj = set.Plwtnuexp(obj,val)
            obj = setParameter(obj,'Plwtnuexp',val,0,'real');
        end
        function res = get.Plwtnuexp(obj)
            res = getParameter(obj,'Plwtnuexp');
        end
        function obj = set.Potetar(obj,val)
            obj = setParameter(obj,'Potetar',val,0,'real');
        end
        function res = get.Potetar(obj)
            res = getParameter(obj,'Potetar');
        end
        function obj = set.Pltetar(obj,val)
            obj = setParameter(obj,'Pltetar',val,0,'real');
        end
        function res = get.Pltetar(obj)
            res = getParameter(obj,'Pltetar');
        end
        function obj = set.Pwtetar(obj,val)
            obj = setParameter(obj,'Pwtetar',val,0,'real');
        end
        function res = get.Pwtetar(obj)
            res = getParameter(obj,'Pwtetar');
        end
        function obj = set.Plwtetar(obj,val)
            obj = setParameter(obj,'Plwtetar',val,0,'real');
        end
        function res = get.Plwtetar(obj)
            res = getParameter(obj,'Plwtetar');
        end
        function obj = set.Potetasat(obj,val)
            obj = setParameter(obj,'Potetasat',val,0,'real');
        end
        function res = get.Potetasat(obj)
            res = getParameter(obj,'Potetasat');
        end
        function obj = set.Pltetasat(obj,val)
            obj = setParameter(obj,'Pltetasat',val,0,'real');
        end
        function res = get.Pltetasat(obj)
            res = getParameter(obj,'Pltetasat');
        end
        function obj = set.Pwtetasat(obj,val)
            obj = setParameter(obj,'Pwtetasat',val,0,'real');
        end
        function res = get.Pwtetasat(obj)
            res = getParameter(obj,'Pwtetasat');
        end
        function obj = set.Plwtetasat(obj,val)
            obj = setParameter(obj,'Plwtetasat',val,0,'real');
        end
        function res = get.Plwtetasat(obj)
            res = getParameter(obj,'Plwtetasat');
        end
        function obj = set.Pota1(obj,val)
            obj = setParameter(obj,'Pota1',val,0,'real');
        end
        function res = get.Pota1(obj)
            res = getParameter(obj,'Pota1');
        end
        function obj = set.Plta1(obj,val)
            obj = setParameter(obj,'Plta1',val,0,'real');
        end
        function res = get.Plta1(obj)
            res = getParameter(obj,'Plta1');
        end
        function obj = set.Pwta1(obj,val)
            obj = setParameter(obj,'Pwta1',val,0,'real');
        end
        function res = get.Pwta1(obj)
            res = getParameter(obj,'Pwta1');
        end
        function obj = set.Plwta1(obj,val)
            obj = setParameter(obj,'Plwta1',val,0,'real');
        end
        function res = get.Plwta1(obj)
            res = getParameter(obj,'Plwta1');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
        function obj = set.wVsubfwd(obj,val)
            obj = setParameter(obj,'wVsubfwd',val,0,'real');
        end
        function res = get.wVsubfwd(obj)
            res = getParameter(obj,'wVsubfwd');
        end
        function obj = set.wBvsub(obj,val)
            obj = setParameter(obj,'wBvsub',val,0,'real');
        end
        function res = get.wBvsub(obj)
            res = getParameter(obj,'wBvsub');
        end
        function obj = set.wBvg(obj,val)
            obj = setParameter(obj,'wBvg',val,0,'real');
        end
        function res = get.wBvg(obj)
            res = getParameter(obj,'wBvg');
        end
        function obj = set.wBvds(obj,val)
            obj = setParameter(obj,'wBvds',val,0,'real');
        end
        function res = get.wBvds(obj)
            res = getParameter(obj,'wBvds');
        end
        function obj = set.wIdsmax(obj,val)
            obj = setParameter(obj,'wIdsmax',val,0,'real');
        end
        function res = get.wIdsmax(obj)
            res = getParameter(obj,'wIdsmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
    end
end
