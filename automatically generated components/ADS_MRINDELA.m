classdef ADS_MRINDELA < ADScomponent
    % ADS_MRINDELA matlab representation for the ADS MRINDELA component
    % Elevated Microstrip Rectangular Inductor
    % MRINDELA [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Number of Segments (Sm-ri) Unit: unknown
        Ns
        % Length of First Segment (Smorr) Unit: m
        L1
        % Length of Second Segment (Smorr) Unit: m
        L2
        % Length of Third Segment (Smorr) Unit: m
        L3
        % Length of Last Segment (Smorr) Unit: m
        Ln
        % Conductor Width (Smorr) Unit: m
        W
        % Conductor Spacing (Smorr) Unit: m
        S
        % Elevation of Inductor above Substrate (Smorr) Unit: m
        Hi
        % Thickness of Conductors (Smorr) Unit: m
        Ti
        % Resistivity (relative to Gold) of Conductors (Smorr) Unit: m
        Ri
        % Spacing Limit between support posts (0 if posts are to be ignored) (Smorr) Unit: m
        Sx
        % Coeff. for Capacitance of corner support posts (ratio of actual post cross-sectional area to W2) (Smorr) Unit: db
        Cc
        % Coeff. for Capacitance of support posts along segment (ratio of actual post cross-sectional area to W2) (Smorr) Unit: db
        Cs
        % Width of Underpass strip Conductor (Smorr) Unit: m
        Wu
        % Angle of departure from innermost segment (Smorr) Unit: deg
        Au
        % Extension of Underpass beyond Inductor (Smorr) Unit: m
        UE
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.Ns(obj,val)
            obj = setParameter(obj,'Ns',val,0,'integer');
        end
        function res = get.Ns(obj)
            res = getParameter(obj,'Ns');
        end
        function obj = set.L1(obj,val)
            obj = setParameter(obj,'L1',val,0,'real');
        end
        function res = get.L1(obj)
            res = getParameter(obj,'L1');
        end
        function obj = set.L2(obj,val)
            obj = setParameter(obj,'L2',val,0,'real');
        end
        function res = get.L2(obj)
            res = getParameter(obj,'L2');
        end
        function obj = set.L3(obj,val)
            obj = setParameter(obj,'L3',val,0,'real');
        end
        function res = get.L3(obj)
            res = getParameter(obj,'L3');
        end
        function obj = set.Ln(obj,val)
            obj = setParameter(obj,'Ln',val,0,'real');
        end
        function res = get.Ln(obj)
            res = getParameter(obj,'Ln');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.Hi(obj,val)
            obj = setParameter(obj,'Hi',val,0,'real');
        end
        function res = get.Hi(obj)
            res = getParameter(obj,'Hi');
        end
        function obj = set.Ti(obj,val)
            obj = setParameter(obj,'Ti',val,0,'real');
        end
        function res = get.Ti(obj)
            res = getParameter(obj,'Ti');
        end
        function obj = set.Ri(obj,val)
            obj = setParameter(obj,'Ri',val,0,'real');
        end
        function res = get.Ri(obj)
            res = getParameter(obj,'Ri');
        end
        function obj = set.Sx(obj,val)
            obj = setParameter(obj,'Sx',val,0,'real');
        end
        function res = get.Sx(obj)
            res = getParameter(obj,'Sx');
        end
        function obj = set.Cc(obj,val)
            obj = setParameter(obj,'Cc',val,0,'real');
        end
        function res = get.Cc(obj)
            res = getParameter(obj,'Cc');
        end
        function obj = set.Cs(obj,val)
            obj = setParameter(obj,'Cs',val,0,'real');
        end
        function res = get.Cs(obj)
            res = getParameter(obj,'Cs');
        end
        function obj = set.Wu(obj,val)
            obj = setParameter(obj,'Wu',val,0,'real');
        end
        function res = get.Wu(obj)
            res = getParameter(obj,'Wu');
        end
        function obj = set.Au(obj,val)
            obj = setParameter(obj,'Au',val,0,'real');
        end
        function res = get.Au(obj)
            res = getParameter(obj,'Au');
        end
        function obj = set.UE(obj,val)
            obj = setParameter(obj,'UE',val,0,'real');
        end
        function res = get.UE(obj)
            res = getParameter(obj,'UE');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end
