classdef ADS_STBJT_Model < ADSmodel
    % ADS_STBJT_Model matlab representation for the ADS STBJT_Model component
    % ST Bipolar Junction Transistor model
    % model ModelName STBJT ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Transistor gender (s---b) 
        Type
        % Parameter extraction measurement temperature (smorr) Unit: deg C
        Tmeas
        % Saturation current [Amps] (sm-r-) Unit: A
        Is
        % Reverse transport saturation current (sm-r-) Unit: A
        Isn
        % Forward beta (sm-r-) 
        Bf
        % Forward emission coefficient (sm-r-) 
        Nf
        % Reverse beta (sm-r-) 
        Br
        % Reverse emission coefficient (sm-r-) 
        Nr
        % Ideal b-e junction saturation current (sm-r-) Unit: A
        Isf
        % Ideal b-e junction emission coefficient (sm-r-) 
        Nbf
        % Ideal b-c junction saturation current (sm-r-) Unit: A
        Isr
        % Ideal b-c junction emission coefficient (sm-r-) 
        Nbr
        % B-E leakage saturation current (sm-r-) Unit: A
        Ise
        % B-E leakage emission coef (sm-r-) 
        Ne
        % B-C leakage saturation current (sm-r-) Unit: A
        Isc
        % B-C leakage emission coef (sm-r-) 
        Nc
        % Forward early voltage (sm-r-) Unit: V
        Vaf
        % Reverse early voltage (sm-r-) Unit: V
        Var
        % Base Push-out Exponent (sm-r-) 
        Enp
        % Low collector current fitting parameter (sm-r-) 
        Rp
        % Ratio of the collector width to the metallurgical base (sm-r-) 
        Rw
        % Modified base-collector built-in potential (sm-r-) 
        Vjj
        % Voltage drop acros the vertical collector resistance at Ic=Ikf (sm-r-) Unit: V
        Vrp
        % Plane junction breakdown of the c-b junction (sm-r-) Unit: V
        Bvc
        % Exponent of the c-b multiplication factor (sm-r-) 
        Mf
        % Ratio of the effective c-b breakdown voltage BVCBO to the plane junction brekdown BVC (sm-r-) 
        Fa
        % Optional fitting parameter describing the c-b generation current at high levels (sm-r-) 
        Avc
        % Plane junction breakdown of the e-b junction (sm-r-) Unit: V
        Bve
        % Exponent of the e-b multiplication factor (sm-r-) 
        Mr
        % Ratio of the effective e-b breakdown voltage BVEB0 to the plane junction brekdown BVE (sm-r-) 
        Fb
        % Optional fitting parameter describing the e-b generation current at high levels (sm-r-) 
        Ave
        % Zero-Bias Base resistance (sm-r-) Unit: Ohms
        Rb
        % Current density at midpoint of base resistance (sm-r-) Unit: A
        Irb
        % Minimum base resistance. (sm-r-) Unit: Ohms
        Rbm
        % Emitter Resistance (sm-r-) Unit: Ohms
        Re
        % Collector Resistance (sm-r-) Unit: Ohms
        Rc
        % Collector resistance in saturation (sm-r-) Unit: Ohms
        Rcs
        % Base-Emitter Junction Capacitance at zero-bias (sm-r-) Unit: F
        Cje
        % Base-emitter built-in junction potential (sm-r-) Unit: V
        Vje
        % Base-emitter junction exponent (sm-r-) 
        Mje
        % Junction capacitor forward-bias threshold (sm-r-) 
        Fc
        % Base-Collector zero-bias capacitance at zero bias (sm-r-) Unit: F
        Cjc
        % Base-collector built-in junction potential (sm-r-) Unit: V
        Vjc
        % Base-collector junction exponent (sm-r-) 
        Mjc
        % Cbc splitting ratio (sm-r-) 
        Xjbc
        % Collector-substrate zero-bias junction capacitance (sm-r-) Unit: F
        Cjs
        % Collector-substrate built-in junction potential (sm-r-) Unit: V
        Vjs
        % Collector-substrate junction exponent (sm-r-) 
        Mjs
        % Fraction of the b-s depletion capacitance (lateral devices) (sm-r-) 
        Xjbs
        % Switches the lateral PNP structure (VERT=0) to the vertical structure (VERT=1) (sm-r-) 
        Vert
        % Allows (Subsn =1) to simulate vertical PNP devices with N type substrate (sm-r-) 
        Subsn
        % Forward transit time (sm-r-) Unit: s
        Tf
        % Forward transit time bias dependence coefficient (sm-r-) 
        Xtf
        % Forward transit time Vbc dependence (sm-r-) Unit: V
        Vtf
        % Forward transit time Ic dependence (sm-r-) Unit: A
        Itf
        % Excess phase (sm-r-) 
        Ptf
        % Switches the default model (SPICE Berkley, Tfcc=0)  for Tf bias dependence to the base push-out model (Tfcc =1) (sm-r-) 
        Tfcc
        % Reverse transit time (sm-r-) Unit: s
        Tr
        % Flicker (1/f) noise coefficient (sm-r-) 
        Kf
        % Flicker (1/f) noise exponent (sm-r-) 
        Af
        % Band Gap Voltage (sm-r-) Unit: V
        Eg
        % Saturation current temperature exponent (sm-r-) 
        Xti
        % Beta temperature exponent (sm-r-) 
        Xtb
        % Linear temperature coefficient for Rb (sm-r-) Unit: K^-1
        Trb1
        % Quadratic temperature coefficient for Rb (sm-r-) 
        Trb2
        % Linear temperature coefficient for Rbm (sm-r-) Unit: K^-1
        Trbm1
        % Quadratic temperature coefficient for Rbm (sm-r-) 
        Trbm2
        % Linear temperature coefficient for Re (sm-r-) Unit: K^-1
        Tre1
        % Quadratic temperature coefficient for Re (sm-r-) 
        Tre2
        % Linear temperature coefficient for Rc (sm-r-) Unit: K^-1
        Trc1
        % Quadratic temperature coefficient for Rc (sm-r-) 
        Trc2
        % Linear temperature coefficient for Rcs (sm-r-) Unit: K^-1
        Trcs1
        % Quadratic temperature coefficient for Rcs (sm-r-) 
        Trcs2
        % Beta forward high current knee (sm-r-) Unit: A
        Ikf
        % Beta reverse high current knee (sm-r-) Unit: A
        Ikr
        % PNP bipolar transistor (smorr) Unit: S
        Gmin
        % Maximum current (smorr) Unit: A
        Imax
        % Debug Flag, prints out some relevant informations (s---b) 
        Debug
        % PN junction model selector (s--ri) 
        Ipnmod
        % Excess phase model selector (s--ri) 
        Exphmod
    end
    methods
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'boolean');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
        function obj = set.Tmeas(obj,val)
            obj = setParameter(obj,'Tmeas',val,0,'real');
        end
        function res = get.Tmeas(obj)
            res = getParameter(obj,'Tmeas');
        end
        function obj = set.Is(obj,val)
            obj = setParameter(obj,'Is',val,0,'string');
        end
        function res = get.Is(obj)
            res = getParameter(obj,'Is');
        end
        function obj = set.Isn(obj,val)
            obj = setParameter(obj,'Isn',val,0,'string');
        end
        function res = get.Isn(obj)
            res = getParameter(obj,'Isn');
        end
        function obj = set.Bf(obj,val)
            obj = setParameter(obj,'Bf',val,0,'string');
        end
        function res = get.Bf(obj)
            res = getParameter(obj,'Bf');
        end
        function obj = set.Nf(obj,val)
            obj = setParameter(obj,'Nf',val,0,'string');
        end
        function res = get.Nf(obj)
            res = getParameter(obj,'Nf');
        end
        function obj = set.Br(obj,val)
            obj = setParameter(obj,'Br',val,0,'string');
        end
        function res = get.Br(obj)
            res = getParameter(obj,'Br');
        end
        function obj = set.Nr(obj,val)
            obj = setParameter(obj,'Nr',val,0,'string');
        end
        function res = get.Nr(obj)
            res = getParameter(obj,'Nr');
        end
        function obj = set.Isf(obj,val)
            obj = setParameter(obj,'Isf',val,0,'string');
        end
        function res = get.Isf(obj)
            res = getParameter(obj,'Isf');
        end
        function obj = set.Nbf(obj,val)
            obj = setParameter(obj,'Nbf',val,0,'string');
        end
        function res = get.Nbf(obj)
            res = getParameter(obj,'Nbf');
        end
        function obj = set.Isr(obj,val)
            obj = setParameter(obj,'Isr',val,0,'string');
        end
        function res = get.Isr(obj)
            res = getParameter(obj,'Isr');
        end
        function obj = set.Nbr(obj,val)
            obj = setParameter(obj,'Nbr',val,0,'string');
        end
        function res = get.Nbr(obj)
            res = getParameter(obj,'Nbr');
        end
        function obj = set.Ise(obj,val)
            obj = setParameter(obj,'Ise',val,0,'string');
        end
        function res = get.Ise(obj)
            res = getParameter(obj,'Ise');
        end
        function obj = set.Ne(obj,val)
            obj = setParameter(obj,'Ne',val,0,'string');
        end
        function res = get.Ne(obj)
            res = getParameter(obj,'Ne');
        end
        function obj = set.Isc(obj,val)
            obj = setParameter(obj,'Isc',val,0,'string');
        end
        function res = get.Isc(obj)
            res = getParameter(obj,'Isc');
        end
        function obj = set.Nc(obj,val)
            obj = setParameter(obj,'Nc',val,0,'string');
        end
        function res = get.Nc(obj)
            res = getParameter(obj,'Nc');
        end
        function obj = set.Vaf(obj,val)
            obj = setParameter(obj,'Vaf',val,0,'string');
        end
        function res = get.Vaf(obj)
            res = getParameter(obj,'Vaf');
        end
        function obj = set.Var(obj,val)
            obj = setParameter(obj,'Var',val,0,'string');
        end
        function res = get.Var(obj)
            res = getParameter(obj,'Var');
        end
        function obj = set.Enp(obj,val)
            obj = setParameter(obj,'Enp',val,0,'string');
        end
        function res = get.Enp(obj)
            res = getParameter(obj,'Enp');
        end
        function obj = set.Rp(obj,val)
            obj = setParameter(obj,'Rp',val,0,'string');
        end
        function res = get.Rp(obj)
            res = getParameter(obj,'Rp');
        end
        function obj = set.Rw(obj,val)
            obj = setParameter(obj,'Rw',val,0,'string');
        end
        function res = get.Rw(obj)
            res = getParameter(obj,'Rw');
        end
        function obj = set.Vjj(obj,val)
            obj = setParameter(obj,'Vjj',val,0,'string');
        end
        function res = get.Vjj(obj)
            res = getParameter(obj,'Vjj');
        end
        function obj = set.Vrp(obj,val)
            obj = setParameter(obj,'Vrp',val,0,'string');
        end
        function res = get.Vrp(obj)
            res = getParameter(obj,'Vrp');
        end
        function obj = set.Bvc(obj,val)
            obj = setParameter(obj,'Bvc',val,0,'string');
        end
        function res = get.Bvc(obj)
            res = getParameter(obj,'Bvc');
        end
        function obj = set.Mf(obj,val)
            obj = setParameter(obj,'Mf',val,0,'string');
        end
        function res = get.Mf(obj)
            res = getParameter(obj,'Mf');
        end
        function obj = set.Fa(obj,val)
            obj = setParameter(obj,'Fa',val,0,'string');
        end
        function res = get.Fa(obj)
            res = getParameter(obj,'Fa');
        end
        function obj = set.Avc(obj,val)
            obj = setParameter(obj,'Avc',val,0,'string');
        end
        function res = get.Avc(obj)
            res = getParameter(obj,'Avc');
        end
        function obj = set.Bve(obj,val)
            obj = setParameter(obj,'Bve',val,0,'string');
        end
        function res = get.Bve(obj)
            res = getParameter(obj,'Bve');
        end
        function obj = set.Mr(obj,val)
            obj = setParameter(obj,'Mr',val,0,'string');
        end
        function res = get.Mr(obj)
            res = getParameter(obj,'Mr');
        end
        function obj = set.Fb(obj,val)
            obj = setParameter(obj,'Fb',val,0,'string');
        end
        function res = get.Fb(obj)
            res = getParameter(obj,'Fb');
        end
        function obj = set.Ave(obj,val)
            obj = setParameter(obj,'Ave',val,0,'string');
        end
        function res = get.Ave(obj)
            res = getParameter(obj,'Ave');
        end
        function obj = set.Rb(obj,val)
            obj = setParameter(obj,'Rb',val,0,'string');
        end
        function res = get.Rb(obj)
            res = getParameter(obj,'Rb');
        end
        function obj = set.Irb(obj,val)
            obj = setParameter(obj,'Irb',val,0,'string');
        end
        function res = get.Irb(obj)
            res = getParameter(obj,'Irb');
        end
        function obj = set.Rbm(obj,val)
            obj = setParameter(obj,'Rbm',val,0,'string');
        end
        function res = get.Rbm(obj)
            res = getParameter(obj,'Rbm');
        end
        function obj = set.Re(obj,val)
            obj = setParameter(obj,'Re',val,0,'string');
        end
        function res = get.Re(obj)
            res = getParameter(obj,'Re');
        end
        function obj = set.Rc(obj,val)
            obj = setParameter(obj,'Rc',val,0,'string');
        end
        function res = get.Rc(obj)
            res = getParameter(obj,'Rc');
        end
        function obj = set.Rcs(obj,val)
            obj = setParameter(obj,'Rcs',val,0,'string');
        end
        function res = get.Rcs(obj)
            res = getParameter(obj,'Rcs');
        end
        function obj = set.Cje(obj,val)
            obj = setParameter(obj,'Cje',val,0,'string');
        end
        function res = get.Cje(obj)
            res = getParameter(obj,'Cje');
        end
        function obj = set.Vje(obj,val)
            obj = setParameter(obj,'Vje',val,0,'string');
        end
        function res = get.Vje(obj)
            res = getParameter(obj,'Vje');
        end
        function obj = set.Mje(obj,val)
            obj = setParameter(obj,'Mje',val,0,'string');
        end
        function res = get.Mje(obj)
            res = getParameter(obj,'Mje');
        end
        function obj = set.Fc(obj,val)
            obj = setParameter(obj,'Fc',val,0,'string');
        end
        function res = get.Fc(obj)
            res = getParameter(obj,'Fc');
        end
        function obj = set.Cjc(obj,val)
            obj = setParameter(obj,'Cjc',val,0,'string');
        end
        function res = get.Cjc(obj)
            res = getParameter(obj,'Cjc');
        end
        function obj = set.Vjc(obj,val)
            obj = setParameter(obj,'Vjc',val,0,'string');
        end
        function res = get.Vjc(obj)
            res = getParameter(obj,'Vjc');
        end
        function obj = set.Mjc(obj,val)
            obj = setParameter(obj,'Mjc',val,0,'string');
        end
        function res = get.Mjc(obj)
            res = getParameter(obj,'Mjc');
        end
        function obj = set.Xjbc(obj,val)
            obj = setParameter(obj,'Xjbc',val,0,'string');
        end
        function res = get.Xjbc(obj)
            res = getParameter(obj,'Xjbc');
        end
        function obj = set.Cjs(obj,val)
            obj = setParameter(obj,'Cjs',val,0,'string');
        end
        function res = get.Cjs(obj)
            res = getParameter(obj,'Cjs');
        end
        function obj = set.Vjs(obj,val)
            obj = setParameter(obj,'Vjs',val,0,'string');
        end
        function res = get.Vjs(obj)
            res = getParameter(obj,'Vjs');
        end
        function obj = set.Mjs(obj,val)
            obj = setParameter(obj,'Mjs',val,0,'string');
        end
        function res = get.Mjs(obj)
            res = getParameter(obj,'Mjs');
        end
        function obj = set.Xjbs(obj,val)
            obj = setParameter(obj,'Xjbs',val,0,'string');
        end
        function res = get.Xjbs(obj)
            res = getParameter(obj,'Xjbs');
        end
        function obj = set.Vert(obj,val)
            obj = setParameter(obj,'Vert',val,0,'string');
        end
        function res = get.Vert(obj)
            res = getParameter(obj,'Vert');
        end
        function obj = set.Subsn(obj,val)
            obj = setParameter(obj,'Subsn',val,0,'string');
        end
        function res = get.Subsn(obj)
            res = getParameter(obj,'Subsn');
        end
        function obj = set.Tf(obj,val)
            obj = setParameter(obj,'Tf',val,0,'string');
        end
        function res = get.Tf(obj)
            res = getParameter(obj,'Tf');
        end
        function obj = set.Xtf(obj,val)
            obj = setParameter(obj,'Xtf',val,0,'string');
        end
        function res = get.Xtf(obj)
            res = getParameter(obj,'Xtf');
        end
        function obj = set.Vtf(obj,val)
            obj = setParameter(obj,'Vtf',val,0,'string');
        end
        function res = get.Vtf(obj)
            res = getParameter(obj,'Vtf');
        end
        function obj = set.Itf(obj,val)
            obj = setParameter(obj,'Itf',val,0,'string');
        end
        function res = get.Itf(obj)
            res = getParameter(obj,'Itf');
        end
        function obj = set.Ptf(obj,val)
            obj = setParameter(obj,'Ptf',val,0,'string');
        end
        function res = get.Ptf(obj)
            res = getParameter(obj,'Ptf');
        end
        function obj = set.Tfcc(obj,val)
            obj = setParameter(obj,'Tfcc',val,0,'string');
        end
        function res = get.Tfcc(obj)
            res = getParameter(obj,'Tfcc');
        end
        function obj = set.Tr(obj,val)
            obj = setParameter(obj,'Tr',val,0,'string');
        end
        function res = get.Tr(obj)
            res = getParameter(obj,'Tr');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'string');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'string');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Eg(obj,val)
            obj = setParameter(obj,'Eg',val,0,'string');
        end
        function res = get.Eg(obj)
            res = getParameter(obj,'Eg');
        end
        function obj = set.Xti(obj,val)
            obj = setParameter(obj,'Xti',val,0,'string');
        end
        function res = get.Xti(obj)
            res = getParameter(obj,'Xti');
        end
        function obj = set.Xtb(obj,val)
            obj = setParameter(obj,'Xtb',val,0,'string');
        end
        function res = get.Xtb(obj)
            res = getParameter(obj,'Xtb');
        end
        function obj = set.Trb1(obj,val)
            obj = setParameter(obj,'Trb1',val,0,'string');
        end
        function res = get.Trb1(obj)
            res = getParameter(obj,'Trb1');
        end
        function obj = set.Trb2(obj,val)
            obj = setParameter(obj,'Trb2',val,0,'string');
        end
        function res = get.Trb2(obj)
            res = getParameter(obj,'Trb2');
        end
        function obj = set.Trbm1(obj,val)
            obj = setParameter(obj,'Trbm1',val,0,'string');
        end
        function res = get.Trbm1(obj)
            res = getParameter(obj,'Trbm1');
        end
        function obj = set.Trbm2(obj,val)
            obj = setParameter(obj,'Trbm2',val,0,'string');
        end
        function res = get.Trbm2(obj)
            res = getParameter(obj,'Trbm2');
        end
        function obj = set.Tre1(obj,val)
            obj = setParameter(obj,'Tre1',val,0,'string');
        end
        function res = get.Tre1(obj)
            res = getParameter(obj,'Tre1');
        end
        function obj = set.Tre2(obj,val)
            obj = setParameter(obj,'Tre2',val,0,'string');
        end
        function res = get.Tre2(obj)
            res = getParameter(obj,'Tre2');
        end
        function obj = set.Trc1(obj,val)
            obj = setParameter(obj,'Trc1',val,0,'string');
        end
        function res = get.Trc1(obj)
            res = getParameter(obj,'Trc1');
        end
        function obj = set.Trc2(obj,val)
            obj = setParameter(obj,'Trc2',val,0,'string');
        end
        function res = get.Trc2(obj)
            res = getParameter(obj,'Trc2');
        end
        function obj = set.Trcs1(obj,val)
            obj = setParameter(obj,'Trcs1',val,0,'string');
        end
        function res = get.Trcs1(obj)
            res = getParameter(obj,'Trcs1');
        end
        function obj = set.Trcs2(obj,val)
            obj = setParameter(obj,'Trcs2',val,0,'string');
        end
        function res = get.Trcs2(obj)
            res = getParameter(obj,'Trcs2');
        end
        function obj = set.Ikf(obj,val)
            obj = setParameter(obj,'Ikf',val,0,'string');
        end
        function res = get.Ikf(obj)
            res = getParameter(obj,'Ikf');
        end
        function obj = set.Ikr(obj,val)
            obj = setParameter(obj,'Ikr',val,0,'string');
        end
        function res = get.Ikr(obj)
            res = getParameter(obj,'Ikr');
        end
        function obj = set.Gmin(obj,val)
            obj = setParameter(obj,'Gmin',val,0,'real');
        end
        function res = get.Gmin(obj)
            res = getParameter(obj,'Gmin');
        end
        function obj = set.Imax(obj,val)
            obj = setParameter(obj,'Imax',val,0,'real');
        end
        function res = get.Imax(obj)
            res = getParameter(obj,'Imax');
        end
        function obj = set.Debug(obj,val)
            obj = setParameter(obj,'Debug',val,0,'boolean');
        end
        function res = get.Debug(obj)
            res = getParameter(obj,'Debug');
        end
        function obj = set.Ipnmod(obj,val)
            obj = setParameter(obj,'Ipnmod',val,0,'integer');
        end
        function res = get.Ipnmod(obj)
            res = getParameter(obj,'Ipnmod');
        end
        function obj = set.Exphmod(obj,val)
            obj = setParameter(obj,'Exphmod',val,0,'integer');
        end
        function res = get.Exphmod(obj)
            res = getParameter(obj,'Exphmod');
        end
    end
end
