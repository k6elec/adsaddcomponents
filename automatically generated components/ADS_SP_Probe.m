classdef ADS_SP_Probe < ADScomponent
    % ADS_SP_Probe matlab representation for the ADS SP_Probe component
    % S-Parameter probe
    % SP_Probe [:Name] L R C
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Reference impedance (smorc) Unit: Ohms
        Z
        % Activate the probe (s--rb) 
        Activate
        % Use the ports specified by L_Port[.] and R_Port[.] (s--rb) 
        UsePorts
        % Left looking ports (s---i) 
        L_Port
        % Right looking ports (s---i) 
        R_Port
    end
    methods
        function obj = set.Z(obj,val)
            obj = setParameter(obj,'Z',val,0,'complex');
        end
        function res = get.Z(obj)
            res = getParameter(obj,'Z');
        end
        function obj = set.Activate(obj,val)
            obj = setParameter(obj,'Activate',val,0,'boolean');
        end
        function res = get.Activate(obj)
            res = getParameter(obj,'Activate');
        end
        function obj = set.UsePorts(obj,val)
            obj = setParameter(obj,'UsePorts',val,0,'boolean');
        end
        function res = get.UsePorts(obj)
            res = getParameter(obj,'UsePorts');
        end
        function obj = set.L_Port(obj,val)
            obj = setParameter(obj,'L_Port',val,0,'integer');
        end
        function res = get.L_Port(obj)
            res = getParameter(obj,'L_Port');
        end
        function obj = set.R_Port(obj,val)
            obj = setParameter(obj,'R_Port',val,0,'integer');
        end
        function res = get.R_Port(obj)
            res = getParameter(obj,'R_Port');
        end
    end
end
