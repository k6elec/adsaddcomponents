classdef ADS_Options < ADSnodeless
    % ADS_Options matlab representation for the ADS Options component
    % Set hpeesofsim options
    % Options [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Temperature (s---r) Unit: deg C
        Temp
        % Default model temperature (s---r) Unit: deg C
        Tnom
        % Relative voltage convergence criterion (smo-r) 
        V_RelTol
        % Absolute voltage convergence criterion (smo-r) Unit: V
        V_AbsTol
        % Relative current convergence criterion (smo-r) 
        I_RelTol
        % Absolute current convergence criterion (smo-r) Unit: A
        I_AbsTol
        % Relative frequency convergence criterion (smo-r) 
        FreqRelTol
        % Absolute frequency convergence criterion (smo-r) Unit: Hz
        FreqAbsTol
        % Relative pivot threshold (smo-r) 
        PivotRelThresh
        % Absolute pivot threshold (smo-r) 
        PivotAbsThresh
        % Minimum voltage present in circuit (smo-r) Unit: V
        Vmin
        % Maximum voltage present in circuit (smo-r) Unit: V
        Vmax
        % Print brief summary of resource usage (s---i) 
        ResourceUsage
        % Check circuit topology for degeneracies (sm--b) 
        TopologyCheck
        % Generate ascii rawfile (s---b) 
        ASCII_Rawfile
        % Narrate the simulation (sm--b) 
        Verbose
        % Give all warning messages (sm--b) 
        GiveAllWarnings
        % Maximum number of warning messages (sm--i) 
        MaxWarnings
        % Silently ignore shorted devices (sm--b) 
        IgnoreShorts
        % Maximum voltage step in DC analysis (smo-r) Unit: V
        MaxDeltaV
        % Convergence mode for DC analysis (s---i) 
        DC_ConvMode
        % ASCII rawfile precision (sm--i) 
        NumDigitsInRawfile
        % Print precision (sm--i) 
        NumDigits
        % Use nutmeg raw-file format (s---b) 
        UseNutmegFormat
        % Screen width (s---i) 
        ScreenWidth
        % Send internal node voltages to raw-file (s---b) 
        OutputInternalNodes
        % This parameter is obsolete (s---i) 
        MaxSpectralSize
        % Send all branch currents to raw-file (s---b) 
        SaveBranchCurrents
        % Force S-parameter calculations whereever possible (s---b) 
        ForceS_Params
        % Minimum epsilon in finite difference calculations (smo-r) 
        MinEpsilon
        % Use M-parameter (RLCG) calculations wherever possible (s---b) 
        UseM_Params
        % Use M-parameter (RLCG) calculations wherever possible (s---b) 
        ForceM_Params
        % File name for the post-processing script (s---s) 
        ScriptFileName
        % Neglect Dispersion in transient simulation using M parameters (s---b) 
        NeglectDispersion
        % timestep value for steady-state analyses (smo-r) 
        TimeStep
        % Use Y-parameters for TLs in Envelope (s---b) 
        EnvUseY_ForTL
        % Name of top-level design (s---s) 
        TopDesignName
        % Minimum time delay between status output (s---i) 
        StatusDelay
        % Suppresses all linear passivity checking (s---b) 
        SuppressPassivityCheck
        % Enable streaming data output to datasets (s---b) 
        StreamData
        % Resistors smaller than this add branch equation (s---r) Unit: Ohms
        MinNodalR
        % Ignore parasitic resistors smaller than this. (s---r) Unit: Ohms
        MinExtR
        % File name for initial guess at DC solution (s---r) 
        DC_InitialGuessFile
        % Read DC_InitialGuessFile prior to a DC solve (s---b) 
        DC_ReadInitialGuess
        % File name for saving final DC solution (s---s) 
        DC_FinalSolutionFile
        % Write last DC solution to DC_FinalSolutionFile (s---b) 
        DC_WriteFinalSolution
        % Annotation level (0-2) when reading DC or HB File (s---i) 
        InitialGuessAnnotation
        % Print device count and matrix information (s---b) 
        Census
        % Enable flicker noise (default=yes) (s---b) 
        FlickerNoise
        % Output Pseudo transient data (internal use only) (s---b) 
        PseudoTranDebugOutput
        % Stop time for Pseudo transient (internal use only) (s---r) 
        PseudoTranStopTime
        % Maximum time step in Pseudo transient (internal use only) (s---r) 
        PseudoTranMaxTimeStep
        % Capacitance value used in Pseudo transient (internal use only) (s---r) 
        PseudoTranCapacitance
        % Inductance value used in Pseudo transient (internal use only) (s---r) 
        PseudoTranInductance
        % Use new singular matrix handler (for internal use) (s---b) 
        UseSingularMatrixHandler
        % Use new default value for explosion current (for internal use) (s---b) 
        UseNewImaxDefault
        % Explosion current (smo-r) Unit: A
        Imax
        % Mosfet (BSIM) Diode limiting current (smo-r) Unit: A
        Ijth
        % P-N junction parallel conductance (smo-r) Unit: S
        Gmin
        % Explosion current (smo-r) Unit: A
        Imelt
        % Output verbose shlib load messages (s---b) 
        ShLoadVerbose
        % Maximum argument value for sym exp() function (s---r) 
        MaxExpArg
        % Replace original FFT algorithm by FFTW (s---i) 
        UseFFTW
        % Use FFTW wisdom: 0-no, 1-supplied, 2-local (s---i) 
        UseFFTWwisdom
        % Enable/disable the SS-HB solution polishing (s---i) 
        EnableOscPolish
        % Enable user-compiled model error trapping (s---b) 
        UserErrorTrap
        % Circuit topology messages mode (sm--b) 
        TopologyCheckMessages
        % Type of data to write to ModelDumpFile (s---i) 
        DumpLevel
        % File name for writing models (s---s) 
        DumpFile
        % Internal dataset block size (s---i) 
        DSblockSize
        % Internal number of blocks to use for each dataset data allocation chunk (s---i) 
        DSdataBlocks
        % Internal dataset block size for merged datasets (s---i) 
        MergedDSblockSize
        % Internal number of blocks to use for each dataset data allocation chunk for merged datasets (s---i) 
        MergedDSdataBlocks
        % Internal number (s---i) 
        DSversion
        % Write impulse response to file (s---b) 
        RunImpulseWriter
        % Save impulse response to dataset (s---b) 
        SaveImpulseToDataset
        % Number of threads to use (0=automatic) (s---i) 
        NumThreads
        % Use GPU for speedup (s---i) 
        GPU
        % Flag to Enable/Disable Optim: <0 - unspecified, 0-no, >0-yes (s---i) 
        EnableOptim
        % Optimize dataset for faster DDS (s---b) 
        DatasetMode
        % Minimum number of variables before variable grouping can occur (s---i) 
        DSminVariablesForGrouping
        % Number of variables per group when grouping is done (s---i) 
        DSvariablesPerGroup
        % Maximum number of variable groups when grouping is done (s---i) 
        DSmaxVariableGroups
        % Display verbose variable grouping parameters (s---b) 
        DSoutputVariableGroupInfo
        % Additional scaling factor applied to instance geometry, to be set by PDKs only (s---r) 
        ScaleFactor
        % Reduce/Collapse large S-Port devices. (s---b) 
        ReduceSport
        % Reduction ratio (collapases the S-Port iff the ratio of (redcuible-ports)/ports is greater than this parameter. (s---r) 
        ReduceSportRatio
        % Use Fast Linear Sim (s---i) 
        doDeltaAC
        % Enable TSMC Safe Operating Area Warnings. Default = Yes (sm--b) 
        WarnSOA
        % Maximum number of Safe Operating Area warning messages. Default = 5 (sm--i) 
        MaxWarnSOA
        % Device instance scaling factor (s---r) 
        Scale
        % Revert to a former skin effect model (s---i) 
        SkinEffectModel
        % Enable electrothermal simulation (s---i) 
        EthLevel
        % Cell name for thermal simulation (s---s) 
        EthCellName
        % File to read containing Temp for various devices (s---s) 
        TvalInFile
        % File to write containing Pdiss for various devices (s---s) 
        PvalOutFile
        % Relative tolerance on power change for convergence (s---r) 
        EthDeltaPreltol
        % Absolute tolernace on temperature change for convergence (s---r) Unit: deg C
        EthDeltaTabstol
        % Maximum number of electrothermal iterations (s---i) 
        EthMaxIters
        % Skip ETH during DC (s---i) 
        EthSkipDC
        % Thermal simulator timestep (s---r) Unit: s
        EthTimestep
        % Maximum device temperature (s---r) Unit: deg C
        EthTmax
        % Power dissipation scaling for duty cycle emulation (s---r) 
        EthPscale
        % Enable linear network collpase: 0:OFF, 1:ON, 2:AUTO (sm-ri) 
        EnableLinearCollapse
        % Enable saving simulation log. (sm--b) 
        SimLogEnabled
        % Status level of the saved simulation log. 0 to turn off. (sm-ri) 
        SimLogStatusLevel
        % Use file specified from user, not the one written by the simulator (s---i) 
        EthUsePkgFile
        % Kambient temp Z axis top face (s---r) 
        EthKambZtop
        % Kambient temp Z axis bottom face (s---r) 
        EthKambZbottom
        % File to read containing Temp for various devices (s---s) 
        EthTopLevelTestBench
        % Print device temperatures (s---i) 
        PrintDeviceTemperatures
        % Multiple Fingers temperature calculation : 0 average, 1: maximum, 2: minimum (s---i) 
        EthMultifingerTcalc
        % Damping factor to apply to temperature changes at each iteration (s---r) 
        EthDeltaTdamp
        % Maximum temperature change for a device at each iteration (s---r) Unit: deg C
        EthDeltaTmax
        % Open thermal viewer when simulation completes (s---i) 
        EthDisplayViewer
        % Tech file name (*.tcl) from thermal PDK (s---s) 
        EthTechFile
        % Tech file directory from thermal PDK (s---s) 
        EthTechFileTopDir
        % Use persistent thermal simulator (s---i) 
        EthPersistent
        % Use Pt as the DC starting method (s---i) 
        PtDCStartingMethod
        % Dcop Output All Sweep Points (s---b) 
        DcopOutputAllSweepPoints
        % Dcop Output Node Voltages (s---b) 
        DcopOutputNodeVoltages
        % Dcop Output Pin Currents (s---b) 
        DcopOutputPinCurrents
        % Dcop Output Operating Point (s---i) 
        DcopOutputDcopType
        % Model Reduction Summary (s---i) 
        ModelReductionSummary
        % Duplicate Param (s---s) 
        DuplicateParam
        % Duplicate Func (s---s) 
        DuplicateFunc
        % Duplicate Model (s---s) 
        DuplicateModel
        % Duplicate Subckt (s---s) 
        DuplicateSubckt
        % Duplicate Instance (s---s) 
        DuplicateInstance
        % Duplicate All (s---s) 
        DuplicateAll
        % Capacitors larger than this value use an alternate form (s---s) 
        LargeCapThreshold
        % Instance name of eth cell (s---s) 
        EthCellInstanceName
        % Manual name of eth cell (s---s) 
        EthManualCellName
        % Pass arbitrary options to Mint models (s---s) 
        MintOptions
        % Enable Assert Checks (s---b) 
        EnableAssertChecks
        % Dump Assert Checks (s---b) 
        DumpAssertChecks
        % Enable Assert Complete Outputs (s---b) 
        EnableAssertCompleteOutputs
        % Reuse thermal solution (s---i) 
        EthReuse
        % Use log(t) time sweep for thermal step response simulation (s---i) 
        EthLogtSweep
        % Enable Spare Removal (s---b) 
        EnableSpareRemoval
        % Output HB AM noise results in DSB format (s---b) 
        OutputAM_NoiseDSB
        % Allow no output to dataset and do not issue warnings (s---b) 
        AllowNoOutputToDataset
    end
    methods
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.V_RelTol(obj,val)
            obj = setParameter(obj,'V_RelTol',val,0,'real');
        end
        function res = get.V_RelTol(obj)
            res = getParameter(obj,'V_RelTol');
        end
        function obj = set.V_AbsTol(obj,val)
            obj = setParameter(obj,'V_AbsTol',val,0,'real');
        end
        function res = get.V_AbsTol(obj)
            res = getParameter(obj,'V_AbsTol');
        end
        function obj = set.I_RelTol(obj,val)
            obj = setParameter(obj,'I_RelTol',val,0,'real');
        end
        function res = get.I_RelTol(obj)
            res = getParameter(obj,'I_RelTol');
        end
        function obj = set.I_AbsTol(obj,val)
            obj = setParameter(obj,'I_AbsTol',val,0,'real');
        end
        function res = get.I_AbsTol(obj)
            res = getParameter(obj,'I_AbsTol');
        end
        function obj = set.FreqRelTol(obj,val)
            obj = setParameter(obj,'FreqRelTol',val,0,'real');
        end
        function res = get.FreqRelTol(obj)
            res = getParameter(obj,'FreqRelTol');
        end
        function obj = set.FreqAbsTol(obj,val)
            obj = setParameter(obj,'FreqAbsTol',val,0,'real');
        end
        function res = get.FreqAbsTol(obj)
            res = getParameter(obj,'FreqAbsTol');
        end
        function obj = set.PivotRelThresh(obj,val)
            obj = setParameter(obj,'PivotRelThresh',val,0,'real');
        end
        function res = get.PivotRelThresh(obj)
            res = getParameter(obj,'PivotRelThresh');
        end
        function obj = set.PivotAbsThresh(obj,val)
            obj = setParameter(obj,'PivotAbsThresh',val,0,'real');
        end
        function res = get.PivotAbsThresh(obj)
            res = getParameter(obj,'PivotAbsThresh');
        end
        function obj = set.Vmin(obj,val)
            obj = setParameter(obj,'Vmin',val,0,'real');
        end
        function res = get.Vmin(obj)
            res = getParameter(obj,'Vmin');
        end
        function obj = set.Vmax(obj,val)
            obj = setParameter(obj,'Vmax',val,0,'real');
        end
        function res = get.Vmax(obj)
            res = getParameter(obj,'Vmax');
        end
        function obj = set.ResourceUsage(obj,val)
            obj = setParameter(obj,'ResourceUsage',val,0,'integer');
        end
        function res = get.ResourceUsage(obj)
            res = getParameter(obj,'ResourceUsage');
        end
        function obj = set.TopologyCheck(obj,val)
            obj = setParameter(obj,'TopologyCheck',val,0,'boolean');
        end
        function res = get.TopologyCheck(obj)
            res = getParameter(obj,'TopologyCheck');
        end
        function obj = set.ASCII_Rawfile(obj,val)
            obj = setParameter(obj,'ASCII_Rawfile',val,0,'boolean');
        end
        function res = get.ASCII_Rawfile(obj)
            res = getParameter(obj,'ASCII_Rawfile');
        end
        function obj = set.Verbose(obj,val)
            obj = setParameter(obj,'Verbose',val,0,'boolean');
        end
        function res = get.Verbose(obj)
            res = getParameter(obj,'Verbose');
        end
        function obj = set.GiveAllWarnings(obj,val)
            obj = setParameter(obj,'GiveAllWarnings',val,0,'boolean');
        end
        function res = get.GiveAllWarnings(obj)
            res = getParameter(obj,'GiveAllWarnings');
        end
        function obj = set.MaxWarnings(obj,val)
            obj = setParameter(obj,'MaxWarnings',val,0,'integer');
        end
        function res = get.MaxWarnings(obj)
            res = getParameter(obj,'MaxWarnings');
        end
        function obj = set.IgnoreShorts(obj,val)
            obj = setParameter(obj,'IgnoreShorts',val,0,'boolean');
        end
        function res = get.IgnoreShorts(obj)
            res = getParameter(obj,'IgnoreShorts');
        end
        function obj = set.MaxDeltaV(obj,val)
            obj = setParameter(obj,'MaxDeltaV',val,0,'real');
        end
        function res = get.MaxDeltaV(obj)
            res = getParameter(obj,'MaxDeltaV');
        end
        function obj = set.DC_ConvMode(obj,val)
            obj = setParameter(obj,'DC_ConvMode',val,0,'integer');
        end
        function res = get.DC_ConvMode(obj)
            res = getParameter(obj,'DC_ConvMode');
        end
        function obj = set.NumDigitsInRawfile(obj,val)
            obj = setParameter(obj,'NumDigitsInRawfile',val,0,'integer');
        end
        function res = get.NumDigitsInRawfile(obj)
            res = getParameter(obj,'NumDigitsInRawfile');
        end
        function obj = set.NumDigits(obj,val)
            obj = setParameter(obj,'NumDigits',val,0,'integer');
        end
        function res = get.NumDigits(obj)
            res = getParameter(obj,'NumDigits');
        end
        function obj = set.UseNutmegFormat(obj,val)
            obj = setParameter(obj,'UseNutmegFormat',val,0,'boolean');
        end
        function res = get.UseNutmegFormat(obj)
            res = getParameter(obj,'UseNutmegFormat');
        end
        function obj = set.ScreenWidth(obj,val)
            obj = setParameter(obj,'ScreenWidth',val,0,'integer');
        end
        function res = get.ScreenWidth(obj)
            res = getParameter(obj,'ScreenWidth');
        end
        function obj = set.OutputInternalNodes(obj,val)
            obj = setParameter(obj,'OutputInternalNodes',val,0,'boolean');
        end
        function res = get.OutputInternalNodes(obj)
            res = getParameter(obj,'OutputInternalNodes');
        end
        function obj = set.MaxSpectralSize(obj,val)
            obj = setParameter(obj,'MaxSpectralSize',val,0,'integer');
        end
        function res = get.MaxSpectralSize(obj)
            res = getParameter(obj,'MaxSpectralSize');
        end
        function obj = set.SaveBranchCurrents(obj,val)
            obj = setParameter(obj,'SaveBranchCurrents',val,0,'boolean');
        end
        function res = get.SaveBranchCurrents(obj)
            res = getParameter(obj,'SaveBranchCurrents');
        end
        function obj = set.ForceS_Params(obj,val)
            obj = setParameter(obj,'ForceS_Params',val,0,'boolean');
        end
        function res = get.ForceS_Params(obj)
            res = getParameter(obj,'ForceS_Params');
        end
        function obj = set.MinEpsilon(obj,val)
            obj = setParameter(obj,'MinEpsilon',val,0,'real');
        end
        function res = get.MinEpsilon(obj)
            res = getParameter(obj,'MinEpsilon');
        end
        function obj = set.UseM_Params(obj,val)
            obj = setParameter(obj,'UseM_Params',val,0,'boolean');
        end
        function res = get.UseM_Params(obj)
            res = getParameter(obj,'UseM_Params');
        end
        function obj = set.ForceM_Params(obj,val)
            obj = setParameter(obj,'ForceM_Params',val,0,'boolean');
        end
        function res = get.ForceM_Params(obj)
            res = getParameter(obj,'ForceM_Params');
        end
        function obj = set.ScriptFileName(obj,val)
            obj = setParameter(obj,'ScriptFileName',val,0,'string');
        end
        function res = get.ScriptFileName(obj)
            res = getParameter(obj,'ScriptFileName');
        end
        function obj = set.NeglectDispersion(obj,val)
            obj = setParameter(obj,'NeglectDispersion',val,0,'boolean');
        end
        function res = get.NeglectDispersion(obj)
            res = getParameter(obj,'NeglectDispersion');
        end
        function obj = set.TimeStep(obj,val)
            obj = setParameter(obj,'TimeStep',val,0,'real');
        end
        function res = get.TimeStep(obj)
            res = getParameter(obj,'TimeStep');
        end
        function obj = set.EnvUseY_ForTL(obj,val)
            obj = setParameter(obj,'EnvUseY_ForTL',val,0,'boolean');
        end
        function res = get.EnvUseY_ForTL(obj)
            res = getParameter(obj,'EnvUseY_ForTL');
        end
        function obj = set.TopDesignName(obj,val)
            obj = setParameter(obj,'TopDesignName',val,0,'string');
        end
        function res = get.TopDesignName(obj)
            res = getParameter(obj,'TopDesignName');
        end
        function obj = set.StatusDelay(obj,val)
            obj = setParameter(obj,'StatusDelay',val,0,'integer');
        end
        function res = get.StatusDelay(obj)
            res = getParameter(obj,'StatusDelay');
        end
        function obj = set.SuppressPassivityCheck(obj,val)
            obj = setParameter(obj,'SuppressPassivityCheck',val,0,'boolean');
        end
        function res = get.SuppressPassivityCheck(obj)
            res = getParameter(obj,'SuppressPassivityCheck');
        end
        function obj = set.StreamData(obj,val)
            obj = setParameter(obj,'StreamData',val,0,'boolean');
        end
        function res = get.StreamData(obj)
            res = getParameter(obj,'StreamData');
        end
        function obj = set.MinNodalR(obj,val)
            obj = setParameter(obj,'MinNodalR',val,0,'real');
        end
        function res = get.MinNodalR(obj)
            res = getParameter(obj,'MinNodalR');
        end
        function obj = set.MinExtR(obj,val)
            obj = setParameter(obj,'MinExtR',val,0,'real');
        end
        function res = get.MinExtR(obj)
            res = getParameter(obj,'MinExtR');
        end
        function obj = set.DC_InitialGuessFile(obj,val)
            obj = setParameter(obj,'DC_InitialGuessFile',val,0,'real');
        end
        function res = get.DC_InitialGuessFile(obj)
            res = getParameter(obj,'DC_InitialGuessFile');
        end
        function obj = set.DC_ReadInitialGuess(obj,val)
            obj = setParameter(obj,'DC_ReadInitialGuess',val,0,'boolean');
        end
        function res = get.DC_ReadInitialGuess(obj)
            res = getParameter(obj,'DC_ReadInitialGuess');
        end
        function obj = set.DC_FinalSolutionFile(obj,val)
            obj = setParameter(obj,'DC_FinalSolutionFile',val,0,'string');
        end
        function res = get.DC_FinalSolutionFile(obj)
            res = getParameter(obj,'DC_FinalSolutionFile');
        end
        function obj = set.DC_WriteFinalSolution(obj,val)
            obj = setParameter(obj,'DC_WriteFinalSolution',val,0,'boolean');
        end
        function res = get.DC_WriteFinalSolution(obj)
            res = getParameter(obj,'DC_WriteFinalSolution');
        end
        function obj = set.InitialGuessAnnotation(obj,val)
            obj = setParameter(obj,'InitialGuessAnnotation',val,0,'integer');
        end
        function res = get.InitialGuessAnnotation(obj)
            res = getParameter(obj,'InitialGuessAnnotation');
        end
        function obj = set.Census(obj,val)
            obj = setParameter(obj,'Census',val,0,'boolean');
        end
        function res = get.Census(obj)
            res = getParameter(obj,'Census');
        end
        function obj = set.FlickerNoise(obj,val)
            obj = setParameter(obj,'FlickerNoise',val,0,'boolean');
        end
        function res = get.FlickerNoise(obj)
            res = getParameter(obj,'FlickerNoise');
        end
        function obj = set.PseudoTranDebugOutput(obj,val)
            obj = setParameter(obj,'PseudoTranDebugOutput',val,0,'boolean');
        end
        function res = get.PseudoTranDebugOutput(obj)
            res = getParameter(obj,'PseudoTranDebugOutput');
        end
        function obj = set.PseudoTranStopTime(obj,val)
            obj = setParameter(obj,'PseudoTranStopTime',val,0,'real');
        end
        function res = get.PseudoTranStopTime(obj)
            res = getParameter(obj,'PseudoTranStopTime');
        end
        function obj = set.PseudoTranMaxTimeStep(obj,val)
            obj = setParameter(obj,'PseudoTranMaxTimeStep',val,0,'real');
        end
        function res = get.PseudoTranMaxTimeStep(obj)
            res = getParameter(obj,'PseudoTranMaxTimeStep');
        end
        function obj = set.PseudoTranCapacitance(obj,val)
            obj = setParameter(obj,'PseudoTranCapacitance',val,0,'real');
        end
        function res = get.PseudoTranCapacitance(obj)
            res = getParameter(obj,'PseudoTranCapacitance');
        end
        function obj = set.PseudoTranInductance(obj,val)
            obj = setParameter(obj,'PseudoTranInductance',val,0,'real');
        end
        function res = get.PseudoTranInductance(obj)
            res = getParameter(obj,'PseudoTranInductance');
        end
        function obj = set.UseSingularMatrixHandler(obj,val)
            obj = setParameter(obj,'UseSingularMatrixHandler',val,0,'boolean');
        end
        function res = get.UseSingularMatrixHandler(obj)
            res = getParameter(obj,'UseSingularMatrixHandler');
        end
        function obj = set.UseNewImaxDefault(obj,val)
            obj = setParameter(obj,'UseNewImaxDefault',val,0,'boolean');
        end
        function res = get.UseNewImaxDefault(obj)
            res = getParameter(obj,'UseNewImaxDefault');
        end
        function obj = set.Imax(obj,val)
            obj = setParameter(obj,'Imax',val,0,'real');
        end
        function res = get.Imax(obj)
            res = getParameter(obj,'Imax');
        end
        function obj = set.Ijth(obj,val)
            obj = setParameter(obj,'Ijth',val,0,'real');
        end
        function res = get.Ijth(obj)
            res = getParameter(obj,'Ijth');
        end
        function obj = set.Gmin(obj,val)
            obj = setParameter(obj,'Gmin',val,0,'real');
        end
        function res = get.Gmin(obj)
            res = getParameter(obj,'Gmin');
        end
        function obj = set.Imelt(obj,val)
            obj = setParameter(obj,'Imelt',val,0,'real');
        end
        function res = get.Imelt(obj)
            res = getParameter(obj,'Imelt');
        end
        function obj = set.ShLoadVerbose(obj,val)
            obj = setParameter(obj,'ShLoadVerbose',val,0,'boolean');
        end
        function res = get.ShLoadVerbose(obj)
            res = getParameter(obj,'ShLoadVerbose');
        end
        function obj = set.MaxExpArg(obj,val)
            obj = setParameter(obj,'MaxExpArg',val,0,'real');
        end
        function res = get.MaxExpArg(obj)
            res = getParameter(obj,'MaxExpArg');
        end
        function obj = set.UseFFTW(obj,val)
            obj = setParameter(obj,'UseFFTW',val,0,'integer');
        end
        function res = get.UseFFTW(obj)
            res = getParameter(obj,'UseFFTW');
        end
        function obj = set.UseFFTWwisdom(obj,val)
            obj = setParameter(obj,'UseFFTWwisdom',val,0,'integer');
        end
        function res = get.UseFFTWwisdom(obj)
            res = getParameter(obj,'UseFFTWwisdom');
        end
        function obj = set.EnableOscPolish(obj,val)
            obj = setParameter(obj,'EnableOscPolish',val,0,'integer');
        end
        function res = get.EnableOscPolish(obj)
            res = getParameter(obj,'EnableOscPolish');
        end
        function obj = set.UserErrorTrap(obj,val)
            obj = setParameter(obj,'UserErrorTrap',val,0,'boolean');
        end
        function res = get.UserErrorTrap(obj)
            res = getParameter(obj,'UserErrorTrap');
        end
        function obj = set.TopologyCheckMessages(obj,val)
            obj = setParameter(obj,'TopologyCheckMessages',val,0,'boolean');
        end
        function res = get.TopologyCheckMessages(obj)
            res = getParameter(obj,'TopologyCheckMessages');
        end
        function obj = set.DumpLevel(obj,val)
            obj = setParameter(obj,'DumpLevel',val,0,'integer');
        end
        function res = get.DumpLevel(obj)
            res = getParameter(obj,'DumpLevel');
        end
        function obj = set.DumpFile(obj,val)
            obj = setParameter(obj,'DumpFile',val,0,'string');
        end
        function res = get.DumpFile(obj)
            res = getParameter(obj,'DumpFile');
        end
        function obj = set.DSblockSize(obj,val)
            obj = setParameter(obj,'DSblockSize',val,0,'integer');
        end
        function res = get.DSblockSize(obj)
            res = getParameter(obj,'DSblockSize');
        end
        function obj = set.DSdataBlocks(obj,val)
            obj = setParameter(obj,'DSdataBlocks',val,0,'integer');
        end
        function res = get.DSdataBlocks(obj)
            res = getParameter(obj,'DSdataBlocks');
        end
        function obj = set.MergedDSblockSize(obj,val)
            obj = setParameter(obj,'MergedDSblockSize',val,0,'integer');
        end
        function res = get.MergedDSblockSize(obj)
            res = getParameter(obj,'MergedDSblockSize');
        end
        function obj = set.MergedDSdataBlocks(obj,val)
            obj = setParameter(obj,'MergedDSdataBlocks',val,0,'integer');
        end
        function res = get.MergedDSdataBlocks(obj)
            res = getParameter(obj,'MergedDSdataBlocks');
        end
        function obj = set.DSversion(obj,val)
            obj = setParameter(obj,'DSversion',val,0,'integer');
        end
        function res = get.DSversion(obj)
            res = getParameter(obj,'DSversion');
        end
        function obj = set.RunImpulseWriter(obj,val)
            obj = setParameter(obj,'RunImpulseWriter',val,0,'boolean');
        end
        function res = get.RunImpulseWriter(obj)
            res = getParameter(obj,'RunImpulseWriter');
        end
        function obj = set.SaveImpulseToDataset(obj,val)
            obj = setParameter(obj,'SaveImpulseToDataset',val,0,'boolean');
        end
        function res = get.SaveImpulseToDataset(obj)
            res = getParameter(obj,'SaveImpulseToDataset');
        end
        function obj = set.NumThreads(obj,val)
            obj = setParameter(obj,'NumThreads',val,0,'integer');
        end
        function res = get.NumThreads(obj)
            res = getParameter(obj,'NumThreads');
        end
        function obj = set.GPU(obj,val)
            obj = setParameter(obj,'GPU',val,0,'integer');
        end
        function res = get.GPU(obj)
            res = getParameter(obj,'GPU');
        end
        function obj = set.EnableOptim(obj,val)
            obj = setParameter(obj,'EnableOptim',val,0,'integer');
        end
        function res = get.EnableOptim(obj)
            res = getParameter(obj,'EnableOptim');
        end
        function obj = set.DatasetMode(obj,val)
            obj = setParameter(obj,'DatasetMode',val,0,'boolean');
        end
        function res = get.DatasetMode(obj)
            res = getParameter(obj,'DatasetMode');
        end
        function obj = set.DSminVariablesForGrouping(obj,val)
            obj = setParameter(obj,'DSminVariablesForGrouping',val,0,'integer');
        end
        function res = get.DSminVariablesForGrouping(obj)
            res = getParameter(obj,'DSminVariablesForGrouping');
        end
        function obj = set.DSvariablesPerGroup(obj,val)
            obj = setParameter(obj,'DSvariablesPerGroup',val,0,'integer');
        end
        function res = get.DSvariablesPerGroup(obj)
            res = getParameter(obj,'DSvariablesPerGroup');
        end
        function obj = set.DSmaxVariableGroups(obj,val)
            obj = setParameter(obj,'DSmaxVariableGroups',val,0,'integer');
        end
        function res = get.DSmaxVariableGroups(obj)
            res = getParameter(obj,'DSmaxVariableGroups');
        end
        function obj = set.DSoutputVariableGroupInfo(obj,val)
            obj = setParameter(obj,'DSoutputVariableGroupInfo',val,0,'boolean');
        end
        function res = get.DSoutputVariableGroupInfo(obj)
            res = getParameter(obj,'DSoutputVariableGroupInfo');
        end
        function obj = set.ScaleFactor(obj,val)
            obj = setParameter(obj,'ScaleFactor',val,0,'real');
        end
        function res = get.ScaleFactor(obj)
            res = getParameter(obj,'ScaleFactor');
        end
        function obj = set.ReduceSport(obj,val)
            obj = setParameter(obj,'ReduceSport',val,0,'boolean');
        end
        function res = get.ReduceSport(obj)
            res = getParameter(obj,'ReduceSport');
        end
        function obj = set.ReduceSportRatio(obj,val)
            obj = setParameter(obj,'ReduceSportRatio',val,0,'real');
        end
        function res = get.ReduceSportRatio(obj)
            res = getParameter(obj,'ReduceSportRatio');
        end
        function obj = set.doDeltaAC(obj,val)
            obj = setParameter(obj,'doDeltaAC',val,0,'integer');
        end
        function res = get.doDeltaAC(obj)
            res = getParameter(obj,'doDeltaAC');
        end
        function obj = set.WarnSOA(obj,val)
            obj = setParameter(obj,'WarnSOA',val,0,'boolean');
        end
        function res = get.WarnSOA(obj)
            res = getParameter(obj,'WarnSOA');
        end
        function obj = set.MaxWarnSOA(obj,val)
            obj = setParameter(obj,'MaxWarnSOA',val,0,'integer');
        end
        function res = get.MaxWarnSOA(obj)
            res = getParameter(obj,'MaxWarnSOA');
        end
        function obj = set.Scale(obj,val)
            obj = setParameter(obj,'Scale',val,0,'real');
        end
        function res = get.Scale(obj)
            res = getParameter(obj,'Scale');
        end
        function obj = set.SkinEffectModel(obj,val)
            obj = setParameter(obj,'SkinEffectModel',val,0,'integer');
        end
        function res = get.SkinEffectModel(obj)
            res = getParameter(obj,'SkinEffectModel');
        end
        function obj = set.EthLevel(obj,val)
            obj = setParameter(obj,'EthLevel',val,0,'integer');
        end
        function res = get.EthLevel(obj)
            res = getParameter(obj,'EthLevel');
        end
        function obj = set.EthCellName(obj,val)
            obj = setParameter(obj,'EthCellName',val,0,'string');
        end
        function res = get.EthCellName(obj)
            res = getParameter(obj,'EthCellName');
        end
        function obj = set.TvalInFile(obj,val)
            obj = setParameter(obj,'TvalInFile',val,0,'string');
        end
        function res = get.TvalInFile(obj)
            res = getParameter(obj,'TvalInFile');
        end
        function obj = set.PvalOutFile(obj,val)
            obj = setParameter(obj,'PvalOutFile',val,0,'string');
        end
        function res = get.PvalOutFile(obj)
            res = getParameter(obj,'PvalOutFile');
        end
        function obj = set.EthDeltaPreltol(obj,val)
            obj = setParameter(obj,'EthDeltaPreltol',val,0,'real');
        end
        function res = get.EthDeltaPreltol(obj)
            res = getParameter(obj,'EthDeltaPreltol');
        end
        function obj = set.EthDeltaTabstol(obj,val)
            obj = setParameter(obj,'EthDeltaTabstol',val,0,'real');
        end
        function res = get.EthDeltaTabstol(obj)
            res = getParameter(obj,'EthDeltaTabstol');
        end
        function obj = set.EthMaxIters(obj,val)
            obj = setParameter(obj,'EthMaxIters',val,0,'integer');
        end
        function res = get.EthMaxIters(obj)
            res = getParameter(obj,'EthMaxIters');
        end
        function obj = set.EthSkipDC(obj,val)
            obj = setParameter(obj,'EthSkipDC',val,0,'integer');
        end
        function res = get.EthSkipDC(obj)
            res = getParameter(obj,'EthSkipDC');
        end
        function obj = set.EthTimestep(obj,val)
            obj = setParameter(obj,'EthTimestep',val,0,'real');
        end
        function res = get.EthTimestep(obj)
            res = getParameter(obj,'EthTimestep');
        end
        function obj = set.EthTmax(obj,val)
            obj = setParameter(obj,'EthTmax',val,0,'real');
        end
        function res = get.EthTmax(obj)
            res = getParameter(obj,'EthTmax');
        end
        function obj = set.EthPscale(obj,val)
            obj = setParameter(obj,'EthPscale',val,0,'real');
        end
        function res = get.EthPscale(obj)
            res = getParameter(obj,'EthPscale');
        end
        function obj = set.EnableLinearCollapse(obj,val)
            obj = setParameter(obj,'EnableLinearCollapse',val,0,'integer');
        end
        function res = get.EnableLinearCollapse(obj)
            res = getParameter(obj,'EnableLinearCollapse');
        end
        function obj = set.SimLogEnabled(obj,val)
            obj = setParameter(obj,'SimLogEnabled',val,0,'boolean');
        end
        function res = get.SimLogEnabled(obj)
            res = getParameter(obj,'SimLogEnabled');
        end
        function obj = set.SimLogStatusLevel(obj,val)
            obj = setParameter(obj,'SimLogStatusLevel',val,0,'integer');
        end
        function res = get.SimLogStatusLevel(obj)
            res = getParameter(obj,'SimLogStatusLevel');
        end
        function obj = set.EthUsePkgFile(obj,val)
            obj = setParameter(obj,'EthUsePkgFile',val,0,'integer');
        end
        function res = get.EthUsePkgFile(obj)
            res = getParameter(obj,'EthUsePkgFile');
        end
        function obj = set.EthKambZtop(obj,val)
            obj = setParameter(obj,'EthKambZtop',val,0,'real');
        end
        function res = get.EthKambZtop(obj)
            res = getParameter(obj,'EthKambZtop');
        end
        function obj = set.EthKambZbottom(obj,val)
            obj = setParameter(obj,'EthKambZbottom',val,0,'real');
        end
        function res = get.EthKambZbottom(obj)
            res = getParameter(obj,'EthKambZbottom');
        end
        function obj = set.EthTopLevelTestBench(obj,val)
            obj = setParameter(obj,'EthTopLevelTestBench',val,0,'string');
        end
        function res = get.EthTopLevelTestBench(obj)
            res = getParameter(obj,'EthTopLevelTestBench');
        end
        function obj = set.PrintDeviceTemperatures(obj,val)
            obj = setParameter(obj,'PrintDeviceTemperatures',val,0,'integer');
        end
        function res = get.PrintDeviceTemperatures(obj)
            res = getParameter(obj,'PrintDeviceTemperatures');
        end
        function obj = set.EthMultifingerTcalc(obj,val)
            obj = setParameter(obj,'EthMultifingerTcalc',val,0,'integer');
        end
        function res = get.EthMultifingerTcalc(obj)
            res = getParameter(obj,'EthMultifingerTcalc');
        end
        function obj = set.EthDeltaTdamp(obj,val)
            obj = setParameter(obj,'EthDeltaTdamp',val,0,'real');
        end
        function res = get.EthDeltaTdamp(obj)
            res = getParameter(obj,'EthDeltaTdamp');
        end
        function obj = set.EthDeltaTmax(obj,val)
            obj = setParameter(obj,'EthDeltaTmax',val,0,'real');
        end
        function res = get.EthDeltaTmax(obj)
            res = getParameter(obj,'EthDeltaTmax');
        end
        function obj = set.EthDisplayViewer(obj,val)
            obj = setParameter(obj,'EthDisplayViewer',val,0,'integer');
        end
        function res = get.EthDisplayViewer(obj)
            res = getParameter(obj,'EthDisplayViewer');
        end
        function obj = set.EthTechFile(obj,val)
            obj = setParameter(obj,'EthTechFile',val,0,'string');
        end
        function res = get.EthTechFile(obj)
            res = getParameter(obj,'EthTechFile');
        end
        function obj = set.EthTechFileTopDir(obj,val)
            obj = setParameter(obj,'EthTechFileTopDir',val,0,'string');
        end
        function res = get.EthTechFileTopDir(obj)
            res = getParameter(obj,'EthTechFileTopDir');
        end
        function obj = set.EthPersistent(obj,val)
            obj = setParameter(obj,'EthPersistent',val,0,'integer');
        end
        function res = get.EthPersistent(obj)
            res = getParameter(obj,'EthPersistent');
        end
        function obj = set.PtDCStartingMethod(obj,val)
            obj = setParameter(obj,'PtDCStartingMethod',val,0,'integer');
        end
        function res = get.PtDCStartingMethod(obj)
            res = getParameter(obj,'PtDCStartingMethod');
        end
        function obj = set.DcopOutputAllSweepPoints(obj,val)
            obj = setParameter(obj,'DcopOutputAllSweepPoints',val,0,'boolean');
        end
        function res = get.DcopOutputAllSweepPoints(obj)
            res = getParameter(obj,'DcopOutputAllSweepPoints');
        end
        function obj = set.DcopOutputNodeVoltages(obj,val)
            obj = setParameter(obj,'DcopOutputNodeVoltages',val,0,'boolean');
        end
        function res = get.DcopOutputNodeVoltages(obj)
            res = getParameter(obj,'DcopOutputNodeVoltages');
        end
        function obj = set.DcopOutputPinCurrents(obj,val)
            obj = setParameter(obj,'DcopOutputPinCurrents',val,0,'boolean');
        end
        function res = get.DcopOutputPinCurrents(obj)
            res = getParameter(obj,'DcopOutputPinCurrents');
        end
        function obj = set.DcopOutputDcopType(obj,val)
            obj = setParameter(obj,'DcopOutputDcopType',val,0,'integer');
        end
        function res = get.DcopOutputDcopType(obj)
            res = getParameter(obj,'DcopOutputDcopType');
        end
        function obj = set.ModelReductionSummary(obj,val)
            obj = setParameter(obj,'ModelReductionSummary',val,0,'integer');
        end
        function res = get.ModelReductionSummary(obj)
            res = getParameter(obj,'ModelReductionSummary');
        end
        function obj = set.DuplicateParam(obj,val)
            obj = setParameter(obj,'DuplicateParam',val,0,'string');
        end
        function res = get.DuplicateParam(obj)
            res = getParameter(obj,'DuplicateParam');
        end
        function obj = set.DuplicateFunc(obj,val)
            obj = setParameter(obj,'DuplicateFunc',val,0,'string');
        end
        function res = get.DuplicateFunc(obj)
            res = getParameter(obj,'DuplicateFunc');
        end
        function obj = set.DuplicateModel(obj,val)
            obj = setParameter(obj,'DuplicateModel',val,0,'string');
        end
        function res = get.DuplicateModel(obj)
            res = getParameter(obj,'DuplicateModel');
        end
        function obj = set.DuplicateSubckt(obj,val)
            obj = setParameter(obj,'DuplicateSubckt',val,0,'string');
        end
        function res = get.DuplicateSubckt(obj)
            res = getParameter(obj,'DuplicateSubckt');
        end
        function obj = set.DuplicateInstance(obj,val)
            obj = setParameter(obj,'DuplicateInstance',val,0,'string');
        end
        function res = get.DuplicateInstance(obj)
            res = getParameter(obj,'DuplicateInstance');
        end
        function obj = set.DuplicateAll(obj,val)
            obj = setParameter(obj,'DuplicateAll',val,0,'string');
        end
        function res = get.DuplicateAll(obj)
            res = getParameter(obj,'DuplicateAll');
        end
        function obj = set.LargeCapThreshold(obj,val)
            obj = setParameter(obj,'LargeCapThreshold',val,0,'string');
        end
        function res = get.LargeCapThreshold(obj)
            res = getParameter(obj,'LargeCapThreshold');
        end
        function obj = set.EthCellInstanceName(obj,val)
            obj = setParameter(obj,'EthCellInstanceName',val,0,'string');
        end
        function res = get.EthCellInstanceName(obj)
            res = getParameter(obj,'EthCellInstanceName');
        end
        function obj = set.EthManualCellName(obj,val)
            obj = setParameter(obj,'EthManualCellName',val,0,'string');
        end
        function res = get.EthManualCellName(obj)
            res = getParameter(obj,'EthManualCellName');
        end
        function obj = set.MintOptions(obj,val)
            obj = setParameter(obj,'MintOptions',val,0,'string');
        end
        function res = get.MintOptions(obj)
            res = getParameter(obj,'MintOptions');
        end
        function obj = set.EnableAssertChecks(obj,val)
            obj = setParameter(obj,'EnableAssertChecks',val,0,'boolean');
        end
        function res = get.EnableAssertChecks(obj)
            res = getParameter(obj,'EnableAssertChecks');
        end
        function obj = set.DumpAssertChecks(obj,val)
            obj = setParameter(obj,'DumpAssertChecks',val,0,'boolean');
        end
        function res = get.DumpAssertChecks(obj)
            res = getParameter(obj,'DumpAssertChecks');
        end
        function obj = set.EnableAssertCompleteOutputs(obj,val)
            obj = setParameter(obj,'EnableAssertCompleteOutputs',val,0,'boolean');
        end
        function res = get.EnableAssertCompleteOutputs(obj)
            res = getParameter(obj,'EnableAssertCompleteOutputs');
        end
        function obj = set.EthReuse(obj,val)
            obj = setParameter(obj,'EthReuse',val,0,'integer');
        end
        function res = get.EthReuse(obj)
            res = getParameter(obj,'EthReuse');
        end
        function obj = set.EthLogtSweep(obj,val)
            obj = setParameter(obj,'EthLogtSweep',val,0,'integer');
        end
        function res = get.EthLogtSweep(obj)
            res = getParameter(obj,'EthLogtSweep');
        end
        function obj = set.EnableSpareRemoval(obj,val)
            obj = setParameter(obj,'EnableSpareRemoval',val,0,'boolean');
        end
        function res = get.EnableSpareRemoval(obj)
            res = getParameter(obj,'EnableSpareRemoval');
        end
        function obj = set.OutputAM_NoiseDSB(obj,val)
            obj = setParameter(obj,'OutputAM_NoiseDSB',val,0,'boolean');
        end
        function res = get.OutputAM_NoiseDSB(obj)
            res = getParameter(obj,'OutputAM_NoiseDSB');
        end
        function obj = set.AllowNoOutputToDataset(obj,val)
            obj = setParameter(obj,'AllowNoOutputToDataset',val,0,'boolean');
        end
        function res = get.AllowNoOutputToDataset(obj)
            res = getParameter(obj,'AllowNoOutputToDataset');
        end
    end
end
