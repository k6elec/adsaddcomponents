classdef ADS_Assert < ADSnodeless
    % ADS_Assert matlab representation for the ADS Assert component
    % Assert Component
    % Assert [:Name]
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % subcircuit master... (s--rs) 
        sub
        % subcircuit masters... (s--rs) 
        subs
        % primitive... (s--rs) 
        primitive
        % model... (s--rs) 
        mod
        % instance... (s--rs) 
        dev
        % parameter... (s--rs) 
        param
        % model parameter... (s--rs) 
        modelparam
        % expression... (s--rs) 
        expr
        % min value... (s--rs) 
        min
        % max value... (s--rs) 
        max
        % duration value... (s--rr) 
        duration
        % message... (s--rs) 
        message
        % level... (s--rs) 
        level
        % info... (s--rs) 
        info
        % values... (s--rr) 
        values
        % boolean... (s--rs) 
        boolean
        % anal_types... (s--rs) 
        anal_types
        % check_windows... (s--rs) 
        check_windows
        % maxvio_perinst... (s--rr) 
        maxvio_perinst
        % maxvio_all... (s--rr) 
        maxvio_all
    end
    methods
        function obj = set.sub(obj,val)
            obj = setParameter(obj,'sub',val,0,'string');
        end
        function res = get.sub(obj)
            res = getParameter(obj,'sub');
        end
        function obj = set.subs(obj,val)
            obj = setParameter(obj,'subs',val,1,'string');
        end
        function res = get.subs(obj)
            res = getParameter(obj,'subs');
        end
        function obj = set.primitive(obj,val)
            obj = setParameter(obj,'primitive',val,0,'string');
        end
        function res = get.primitive(obj)
            res = getParameter(obj,'primitive');
        end
        function obj = set.mod(obj,val)
            obj = setParameter(obj,'mod',val,0,'string');
        end
        function res = get.mod(obj)
            res = getParameter(obj,'mod');
        end
        function obj = set.dev(obj,val)
            obj = setParameter(obj,'dev',val,0,'string');
        end
        function res = get.dev(obj)
            res = getParameter(obj,'dev');
        end
        function obj = set.param(obj,val)
            obj = setParameter(obj,'param',val,0,'string');
        end
        function res = get.param(obj)
            res = getParameter(obj,'param');
        end
        function obj = set.modelparam(obj,val)
            obj = setParameter(obj,'modelparam',val,0,'string');
        end
        function res = get.modelparam(obj)
            res = getParameter(obj,'modelparam');
        end
        function obj = set.expr(obj,val)
            obj = setParameter(obj,'expr',val,0,'string');
        end
        function res = get.expr(obj)
            res = getParameter(obj,'expr');
        end
        function obj = set.min(obj,val)
            obj = setParameter(obj,'min',val,0,'string');
        end
        function res = get.min(obj)
            res = getParameter(obj,'min');
        end
        function obj = set.max(obj,val)
            obj = setParameter(obj,'max',val,0,'string');
        end
        function res = get.max(obj)
            res = getParameter(obj,'max');
        end
        function obj = set.duration(obj,val)
            obj = setParameter(obj,'duration',val,0,'real');
        end
        function res = get.duration(obj)
            res = getParameter(obj,'duration');
        end
        function obj = set.message(obj,val)
            obj = setParameter(obj,'message',val,0,'string');
        end
        function res = get.message(obj)
            res = getParameter(obj,'message');
        end
        function obj = set.level(obj,val)
            obj = setParameter(obj,'level',val,0,'string');
        end
        function res = get.level(obj)
            res = getParameter(obj,'level');
        end
        function obj = set.info(obj,val)
            obj = setParameter(obj,'info',val,0,'string');
        end
        function res = get.info(obj)
            res = getParameter(obj,'info');
        end
        function obj = set.values(obj,val)
            obj = setParameter(obj,'values',val,1,'real');
        end
        function res = get.values(obj)
            res = getParameter(obj,'values');
        end
        function obj = set.boolean(obj,val)
            obj = setParameter(obj,'boolean',val,0,'string');
        end
        function res = get.boolean(obj)
            res = getParameter(obj,'boolean');
        end
        function obj = set.anal_types(obj,val)
            obj = setParameter(obj,'anal_types',val,1,'string');
        end
        function res = get.anal_types(obj)
            res = getParameter(obj,'anal_types');
        end
        function obj = set.check_windows(obj,val)
            obj = setParameter(obj,'check_windows',val,1,'string');
        end
        function res = get.check_windows(obj)
            res = getParameter(obj,'check_windows');
        end
        function obj = set.maxvio_perinst(obj,val)
            obj = setParameter(obj,'maxvio_perinst',val,0,'real');
        end
        function res = get.maxvio_perinst(obj)
            res = getParameter(obj,'maxvio_perinst');
        end
        function obj = set.maxvio_all(obj,val)
            obj = setParameter(obj,'maxvio_all',val,0,'real');
        end
        function res = get.maxvio_all(obj)
            res = getParameter(obj,'maxvio_all');
        end
    end
end
