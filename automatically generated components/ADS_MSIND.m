classdef ADS_MSIND < ADScomponent
    % ADS_MSIND matlab representation for the ADS MSIND component
    % Microstrip Round Spiral Inductor
    % MSIND [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Number of Turns (Smorr) Unit: unknown
        N
        % Inner Radius measured to the center of the Conductor (Smorr) Unit: m
        Ri
        % Conductor Width (Smorr) Unit: m
        W
        % Conductor Spacing (Smorr) Unit: m
        S
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Ri(obj,val)
            obj = setParameter(obj,'Ri',val,0,'real');
        end
        function res = get.Ri(obj)
            res = getParameter(obj,'Ri');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end
