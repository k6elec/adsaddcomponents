classdef ADS_B3SOI_Model < ADSmodel
    % ADS_B3SOI_Model matlab representation for the ADS B3SOI_Model component
    % PD SOI Metal Oxide Semiconductor Transistor model
    % model ModelName B3SOI ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % NMOS type B3SOI MOSFET (s---b) 
        NMOS
        % PMOS type B3SOI MOSFET (s---b) 
        PMOS
        % Capacitance model selector (s---i) 
        Capmod
        % Mobility model selector (s---i) 
        Mobmod
        % Noise model selector (s---i) 
        Noimod
        % Model parameter checking selector (s---i) 
        Paramchk
        % Bin unit selector (s---i) 
        Binunit
        % Parameter for model version (s---r) 
        Version
        % Gate oxide thickness (smorr) Unit: m
        Tox
        % Drain/Source and channel coupling capacitance (smorr) Unit: F/m^2
        Cdsc
        % Body effect coefficient of Cdsc (smorr) Unit: F/(V*m^2)
        Cdscb
        % Drain bias dependence of Cdsc (smorr) Unit: F/(V*m^2)
        Cdscd
        % Capacitance due to interface charge (smorr) Unit: F/m^2
        Cit
        % Subthreshold Swing Factor (smorr) 
        Nfactor
        % Saturation velocity at Temp=Tnom (smorr) Unit: m/s
        Vsat
        % Temperature coefficient for saturation velocity (smorr) Unit: m/s
        At
        % Bulk charge effect coefficient (smorr) 
        A0
        % Gate bias coefficient of Abulk (smorr) Unit: V^-1
        Ags
        % First non-saturation factor (smorr) Unit: V^-1
        A1
        % Second non-saturation factor (smorr) 
        A2
        % Body-bias coefficient of the bulk charge effect (smorr) Unit: V^-1
        Keta
        % Substrate doping concentration with polarity (smorr) Unit: cm^-3
        Nsub
        % Channel doping concentration (smorr) Unit: cm^-3
        Nch
        % Poly-gate doping concentration (smorr) Unit: cm^-3
        Ngate
        % Body-effect coefficient near the interface (smorr) Unit: V^(1/2)
        Gamma1
        % Body-effect coefficient in the bulk (smorr) Unit: V^(1/2)
        Gamma2
        % Vth transition body Voltage (smorr) Unit: V
        Vbx
        % Maximum body voltage (smorr) Unit: V
        Vbm
        % Doping depth (smorr) Unit: m
        Xt
        % Body effect coefficient (smorr) Unit: V^(1/2)
        K1
        % Temperature coefficient for threshold voltage (smorr) Unit: V
        Kt1
        % Channel length sensitivity of kt1 (smorr) Unit: V*m
        Kt1l
        % Body-bias coeff. of the Vth temperature effect (smorr) 
        Kt2
        % Bulk effect coefficient 2 (smorr) 
        K2
        % Narrow width coefficient (smorr) 
        K3
        % Body effect coefficient of K3 (smorr) Unit: V^-1
        K3b
        % Narrow width parameter (smorr) Unit: m
        W0
        % Lateral non-uniform doping coefficient (smorr) Unit: m
        Nlx
        % First Coefficient of short-channel effect on Vth (smorr) 
        Dvt0
        % Second Coefficient of short-channel effect on Vth (smorr) 
        Dvt1
        % Body-bias Coefficient of short-channel effect on Vth (smorr) Unit: V^-1
        Dvt2
        % First Coefficient of narrow-width effect on Vth (smorr) 
        Dvt0w
        % Second Coefficient of narrow-width effect on Vth (smorr) Unit: m^-1
        Dvt1w
        % Body-bias Coefficient of narrow-width effect on Vth (smorr) Unit: V^-1
        Dvt2w
        % L depend. coeff. of the DIBL effect in Rout (smorr) 
        Drout
        % DIBL coefficient in subthreshold region (smorr) 
        Dsub
        % Zero-bias threshold voltage (smorr) Unit: V
        Vth0
        % First order mobility degradation coefficient (smorr) Unit: m/V
        Ua
        % Temperature coefficient of parameter ua (smorr) Unit: m/V
        Ua1
        % Second order mobility degradation coefficient (smorr) Unit: (m/V)^2
        Ub
        % Temperature coefficient of parameter ub (smorr) Unit: (m/V)^2
        Ub1
        % Body bias mobility degradation coefficient (smorr) Unit: V^-1
        Uc
        % Temperature coefficient of parameter uc (smorr) Unit: V^-1
        Uc1
        % Mobility at Temp = Tnom (smorr) Unit: m^2/(V*s)
        U0
        % Mobility temperature exponent (smorr) 
        Ute
        % Offset Voltage in subthreshold region (smorr) Unit: V
        Voff
        % Parameter measurement temperature (smorr) Unit: deg C
        Tnom
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % G-S overlap capacitance per meter channel width (smorr) Unit: F/m
        Cgso
        % G-D overlap capacitance per meter channel  width (smorr) Unit: F/m
        Cgdo
        % Coefficient of channel charge share (smorr) 
        Xpart
        % Effective Vds parameter (smorr) Unit: V
        Delta
        % Drain and source diffusion sheet resistance (smorr) Unit: Ohms
        Rsh
        % Parasitic resistance per unit width (smorr) Unit: Ohms*um
        Rdsw
        % Gate bias effect on parasitic resistance (smorr) Unit: V^-1
        Prwg
        % Body effect on parasitic resistance (smorr) Unit: V^(-1/2)
        Prwb
        % Temperature coefficient of parasitic resistance (smorr) Unit: Ohms*um
        Prt
        % Subthreshold region DIBL coefficient (smorr) 
        Eta0
        % Subthreshold region DIBL coefficient (smorr) Unit: V^-1
        Etab
        % Channel-Length_Modulation Effect Coefficient (smorr) 
        Pclm
        % Drain Induced Barrier Lowering Effect Coefficient 1 (smorr) 
        Pdiblc1
        % Drain Induced Barrier Lowering Effect Coefficient 2 (smorr) 
        Pdiblc2
        % Body-effect on Drain Induced Barrier Lowering (smorr) Unit: V^-1
        Pdiblcb
        % Gate voltage dependence of Rout coefficient (smorr) 
        Pvag
        % Self heating mode selector (s---i) 
        Shmod
        % Dynamic depletion mode selector (s---i) 
        Ddmod
        % Back gate oxide thickness (smorr) Unit: m
        Tbox
        % Silicon-on-insulator thickness (smorr) Unit: m
        Tsi
        % Metallurgical Junction Depth (smorr) Unit: m
        Xj
        % Self-heating Thermal Resistance (smorr) Unit: Ohms
        Rth0
        % Self-heating Thermal Capacitance (smorr) Unit: F
        Cth0
        % GIDL first parameter (smorr) Unit: V
        Ngidl
        % GIDL second parameter (smorr) Unit: Ohm^-1
        Agidl
        % GIDL third parameter (smorr) Unit: V/m
        Bgidl
        % Diode non-ideality factor (smorr) 
        Ndiode
        % Temperature coefficient for Isbjt (smorr) 
        Xbjt
        % Temperature coefficient for Isdif (smorr) 
        Xdif
        % Temperature coefficient for Isrec (smorr) 
        Xrec
        % Temperature coefficient for Istun (smorr) 
        Xtun
        % S/D (gate side) sidewall junction built in potential (smorr) Unit: V
        Pbswg
        % S/D (gate side) sidewall junction grading coefficient (smorr) 
        Mjswg
        % S/D (gate side) sidewall junction capacitance (smorr) Unit: F/m
        Cjswg
        % Length reduction parameter (smorr) Unit: m
        Lint
        % Coefficient of length dependence for length offset (smorr) Unit: m
        Ll
        % Power of length dependence of length offset (smorr) 
        Lln
        % Coefficient of width dependence for length offset (smorr) Unit: m
        Lw
        % Power of width dependence of length offset (smorr) 
        Lwn
        % Coefficient of length and width cross term length offset (smorr) Unit: m
        Lwl
        % Width dependence of Rds (smorr) 
        Wr
        % Width reduction parameter (smorr) Unit: m
        Wint
        % Coefficient of Weff's gate dependence (smorr) Unit: m/V
        Dwg
        % Coefficient of Weff's substrate body bias dependence (smorr) Unit: m/V^(1/2)
        Dwb
        % Coefficient of length dependence for width offset (smorr) Unit: m
        Wl
        % Power of length dependence of width offset (smorr) 
        Wln
        % Coefficient of width dependence for width offset (smorr) Unit: m
        Ww
        % Power of width dependence of width offset (smorr) 
        Wwn
        % Coefficient of length and width cross term width offset (smorr) Unit: m
        Wwl
        % Bulk charge effect coefficient for channel width (smorr) Unit: m
        B0
        % Bulk charge effect width offset (smorr) Unit: m
        B1
        % Light doped source-gate region overlap capacitance (smorr) Unit: F/m
        Cgsl
        % Light doped drain-gate region overlap capacitance (smorr) Unit: F/m
        Cgdl
        % Coefficient for light doped region overlap capacitance (smorr) Unit: F/m
        Ckappa
        % Fringing field capacitance (smorr) Unit: F/m
        Cf
        % Constant term for the short channel model (smorr) Unit: m
        Clc
        % Exponential term for the short channel model (smorr) 
        Cle
        % Width offset fitting parameter from C-V (smorr) Unit: m
        Dwc
        % Length offset fitting parameter from C-V (smorr) Unit: m
        Dlc
        % First parameter of impact ionization current (smorr) Unit: m/V
        Alpha0
        % Noise parameter A (smorr) 
        Noia
        % Noise parameter B (smorr) 
        Noib
        % Noise parameter C (smorr) 
        Noic
        % Flicker (1/f) noise parameter (smorr) Unit: V/m
        Em
        % Flicker (1/f) noise frequency exponent (smorr) 
        Ef
        % Flicker (1/f) noise exponent (smorr) 
        Af
        % Flicker (1/f) noise coefficient (smorr) 
        Kf
        % Floating body excess noise ideality factor (smorr) 
        Noif
        % First Body effect width dependent parameter (smorr) Unit: m
        K1w1
        % Second Body effect width dependent parameter (smorr) Unit: m
        K1w2
        % Surface potential adjustment for bulk charge effect (smorr) Unit: V
        Ketas
        % Width offset for body contact isolation edge (smorr) Unit: m
        Dwbc
        % First Vds parameter of impact ionization current (smorr) Unit: V^-1
        Beta0
        % Second Vds parameter of impact ionization current (smorr) 
        Beta1
        % Third Vds parameter of impact ionization current (smorr) Unit: V
        Beta2
        % Nominal drain saturation voltage at threshold for impact ionization current (smorr) Unit: V
        Vdsatii0
        % Temperature dependent parameter for impact ionization (smorr) 
        Tii
        % Channel length dependent parameter at threshold for impact ionization (smorr) 
        Lii
        % First Vgs dependent parameter for impact ionization current (smorr) Unit: V^-1
        Sii0
        % Second Vgs dependent parameter for impact ionization current (smorr) Unit: V^-1
        Sii1
        % Third Vgs dependent parameter for impact ionization current (smorr) Unit: V^-1
        Sii2
        % Vds dependent parameter for impact ionization current (smorr) Unit: V^-1
        Siid
        % Fraction of bipolar current affecting the impact ionization (smorr) 
        Fbjtii
        % Saturation electric field for impact ionization (smorr) Unit: V/m
        Esatii
        % Reverse tunneling non-ideality factor (smorr) 
        Ntun
        % Recombination non-ideality factor at forward bias (smorr) 
        Nrecf0
        % Recombination non-ideality factor at reversed bias (smorr) 
        Nrecr0
        % BJT injection saturation current (smorr) Unit: A/m^2
        Isbjt
        % Body to source/drain injection saturation current (smorr) Unit: A/m^2
        Isdif
        % Recombination in depletion saturation current (smorr) Unit: A/m^2
        Isrec
        % Reverse tunneling saturation current (smorr) Unit: A/m^2
        Istun
        % Electron/hole diffusion length (smorr) Unit: m
        Ln
        % Voltage dependent parameter for recombination current (smorr) Unit: V
        Vrec0
        % Voltage dependent parameter for tunneling current (smorr) Unit: V
        Vtun0
        % Power coefficient of channel length dependency for bipolar current (smorr) 
        Nbjt
        % Reference channel length for bipolar current (smorr) Unit: m
        Lbjt0
        % Channel length dependency coefficient of diffusion cap (smorr) 
        Ldif0
        % Early voltage for bipolar current (smorr) Unit: V
        Vabjt
        % Channel length dependency of early voltage for bipolar current (smorr) Unit: V/m
        Aely
        % High level injection parameter for bipolar current (smorr) 
        Ahli
        % Intrinsic body contact sheet resistance (smorr) Unit: Ohms
        Rbody
        % Extrinsic body contact sheet resistance (smorr) Unit: Ohms
        Rbsh
        % Gate substrate overlap capacitance per unit channel length (smorr) Unit: F/m
        Cgeo
        % Diffusion capacitance transit time coefficient (smorr) Unit: s
        Tt
        % Power coefficient of channel length dependency for diffusion capacitance (smorr) 
        Ndif
        % Source/drain bottom diffusion capacitance flatband voltage (smorr) Unit: V
        Vsdfb
        % Source/drain bottom diffusion capacitance threshold voltage (smorr) Unit: V
        Vsdth
        % Source/drain bottom diffusion minimum capacitance (smorr) Unit: F
        Csdmin
        % Source/drain bottom diffusion smoothing parameter (smorr) 
        Asd
        % Source/drain sidewall fringing capacitance per unit channel length (smorr) Unit: F/m
        Csdesw
        % Temperature coefficient for Nrecf (smorr) 
        Ntrecf
        % Temperature coefficient for Nrecr (smorr) 
        Ntrecr
        % Length offset fitting parameter for body charge (smorr) Unit: m
        Dlcb
        % Scaling factor for body charge (smorr) 
        Fbody
        % Temperature coefficient of Cjswg (smorr) Unit: K^-1
        Tcjswg
        % Temperature coefficient of Pbswg (smorr) Unit: V/K
        Tpbswg
        % Exponential coefficient for finite charge thickness (smorr) Unit: m/V
        Acde
        % Coefficient for gate-bias dependent surface potential (smorr) Unit: V^(1/2)
        Moin
        % Threshold voltage adjust for CV (smorr) Unit: V
        Delvt
        % Coefficient of Vbs0 dependency on Ves (smorr) 
        Kb1
        % Length offset fitting parameter for backgate charge (smorr) Unit: m
        Dlbg
        % Gate current model selector (s---i) 
        Igmod
        % Effective oxide thickness considering quantum effect (smorr) Unit: m
        Toxqm
        % Minimum width for thermal resistance calculation (smorr) Unit: m
        Wth0
        % Body halo sheet resistance (smorr) Unit: Ohms
        Rhalo
        % Power term of gate current (smorr) 
        Ntox
        % Target oxide thickness (smorr) Unit: m
        Toxref
        % Effective bandgap in gate current calculation (smorr) Unit: V
        Ebg
        % Valence-band electron non-ideality factor (smorr) Unit: V
        Nevb
        % First Vox dependent parameter for gate current in inversion (smorr) 
        Alphagb1
        % Second Vox dependent parameter for gate current in inversion (smorr) 
        Betagb1
        % Third Vox dependent parameter for gate current in inversion (smorr) 
        Vgb1
        % Conduction-band electron non-ideality factor (smorr) 
        Necb
        % First Vox dependent parameter for gate current in accumulation (smorr) 
        Alphagb2
        % Second Vox dependent parameter for gate current in accumulation (smorr) 
        Betagb2
        % Third Vox dependent parameter for gate current in accumulation (smorr) 
        Vgb2
        % Limit of Vox in gate current calculation (smorr) Unit: V
        Voxh
        % Smoothing parameter in the Vox smoothing function (smorr) Unit: V
        Deltavox
        % Length dependence of nch (smorr) 
        Lnch
        % Length dependence of nsub (smorr) 
        Lnsub
        % Length dependence of ngate (smorr) 
        Lngate
        % Length dependence of vth0 (smorr) 
        Lvth0
        % Length dependence of body effect coefficient (smorr) Unit: um*V^(1/2)
        Lk1
        % Length dependence of K1w1 (smorr) 
        Lk1w1
        % Length dependence of K1w2 (smorr) 
        Lk1w2
        % Length dependence of charge sharing coefficient (smorr) Unit: um
        Lk2
        % Length dependence of k3 (smorr) 
        Lk3
        % Length dependence of k3b (smorr) 
        Lk3b
        % Length dependence of kb1 (smorr) 
        Lkb1
        % Length dependence of w0 (smorr) 
        Lw0
        % Length dependence of nlx (smorr) 
        Lnlx
        % Length dependence of dvt0 (smorr) 
        Ldvt0
        % Length dependence of dvt1 (smorr) 
        Ldvt1
        % Length dependence of dvt2 (smorr) 
        Ldvt2
        % Length dependence of dvt0w (smorr) 
        Ldvt0w
        % Length dependence of dvt1w (smorr) 
        Ldvt1w
        % Length dependence of dvt2w (smorr) 
        Ldvt2w
        % Length dependence of u0 (smorr) 
        Lu0
        % Length dependence of ua (smorr) 
        Lua
        % Length dependence of ub (smorr) 
        Lub
        % Length dependence of uc (smorr) 
        Luc
        % Length dependence of vsat (smorr) 
        Lvsat
        % Length dependence of a0 (smorr) 
        La0
        % Length dependence of ags (smorr) 
        Lags
        % Length dependence of b0 (smorr) 
        Lb0
        % Length dependence of b1 (smorr) 
        Lb1
        % Length dependence of keta (smorr) 
        Lketa
        % Length dependence of ketas (smorr) 
        Lketas
        % Length dependence of a1 (smorr) 
        La1
        % Length dependence of a2 (smorr) 
        La2
        % Length dependence of rdsw (smorr) 
        Lrdsw
        % Length dependence of prwb (smorr) 
        Lprwb
        % Length dependence of prwg (smorr) 
        Lprwg
        % Length dependence of wr (smorr) 
        Lwr
        % Length dependence of nfactor (smorr) 
        Lnfactor
        % Length dependence of dwg (smorr) 
        Ldwg
        % Length dependence of dwb (smorr) 
        Ldwb
        % Length dependence of voff (smorr) 
        Lvoff
        % Length dependence of barrier lowering coefficient (smorr) Unit: um
        Leta0
        % Length dependence of sens. of Eta to Vbs (smorr) Unit: um/V
        Letab
        % Length dependence of dsub (smorr) 
        Ldsub
        % Length dependence of cit (smorr) 
        Lcit
        % Length dependence of cdsc (smorr) 
        Lcdsc
        % Length dependence of cdscb (smorr) 
        Lcdscb
        % Length dependence of cdscd (smorr) 
        Lcdscd
        % Length dependence of pclm (smorr) 
        Lpclm
        % Length dependence of pdiblc1 (smorr) 
        Lpdiblc1
        % Length dependence of pdiblc2 (smorr) 
        Lpdiblc2
        % Length dependence of pdiblcb (smorr) 
        Lpdiblcb
        % Length dependence of drout (smorr) 
        Ldrout
        % Length dependence of pvag (smorr) 
        Lpvag
        % Length dependence of delta (smorr) 
        Ldelta
        % Length dependence of alpha0 (smorr) 
        Lalpha0
        % Length dependence of fbjtii (smorr) 
        Lfbjtii
        % Length dependence of beta0 (smorr) 
        Lbeta0
        % Length dependence of beta1 (smorr) 
        Lbeta1
        % Length dependence of beta2 (smorr) 
        Lbeta2
        % Length dependence of vdsatii0 (smorr) 
        Lvdsatii0
        % Length dependence of lii (smorr) 
        Llii
        % Length dependence of esatii (smorr) 
        Lesatii
        % Length dependence of sii0 (smorr) 
        Lsii0
        % Length dependence of sii1 (smorr) 
        Lsii1
        % Length dependence of sii2 (smorr) 
        Lsii2
        % Length dependence of siid (smorr) 
        Lsiid
        % Length dependence of agidl (smorr) 
        Lagidl
        % Length dependence of bgidl (smorr) 
        Lbgidl
        % Length dependence of ngidl (smorr) 
        Lngidl
        % Length dependence of ntun (smorr) 
        Lntun
        % Length dependence of ndiode (smorr) 
        Lndiode
        % Length dependence of nrecf0 (smorr) 
        Lnrecf0
        % Length dependence of nrecr0 (smorr) 
        Lnrecr0
        % Length dependence of isbjt (smorr) 
        Lisbjt
        % Length dependence of isdif (smorr) 
        Lisdif
        % Length dependence of istun (smorr) 
        Listun
        % Length dependence of vrec0 (smorr) 
        Lvrec0
        % Length dependence of vtun0 (smorr) 
        Lvtun0
        % Length dependence of nbjt (smorr) 
        Lnbjt
        % Length dependence of lbjt0 (smorr) 
        Llbjt0
        % Length dependence of vabjt (smorr) 
        Lvabjt
        % Length dependence of aely (smorr) 
        Laely
        % Length dependence of ahli (smorr) 
        Lahli
        % Length dependence of vsdfb (smorr) 
        Lvsdfb
        % Length dependence of vsdth (smorr) 
        Lvsdth
        % Length dependence of delvt (smorr) 
        Ldelvt
        % Length dependence of Acde (smorr) 
        Lacde
        % Length dependence of Moin (smorr) Unit: um*V^(1/2)
        Lmoin
        % Width dependence of nch (smorr) 
        Wnch
        % Width dependence of nsub (smorr) 
        Wnsub
        % Width dependence of ngate (smorr) 
        Wngate
        % Width dependence of vth0 (smorr) 
        Wvth0
        % Width dependence of body effect coefficient (smorr) Unit: um*V^(1/2)
        Wk1
        % Width dependence of K1w1 (smorr) 
        Wk1w1
        % Width dependence of K1w2 (smorr) 
        Wk1w2
        % Width dependence of charge sharing coefficient (smorr) Unit: um
        Wk2
        % Width dependence of k3 (smorr) 
        Wk3
        % Width dependence of k3b (smorr) 
        Wk3b
        % Width dependence of kb1 (smorr) 
        Wkb1
        % Width dependence of w0 (smorr) 
        Ww0
        % Width dependence of nlx (smorr) 
        Wnlx
        % Width dependence of dvt0 (smorr) 
        Wdvt0
        % Width dependence of dvt1 (smorr) 
        Wdvt1
        % Width dependence of dvt2 (smorr) 
        Wdvt2
        % Width dependence of dvt0w (smorr) 
        Wdvt0w
        % Width dependence of dvt1w (smorr) 
        Wdvt1w
        % Width dependence of dvt2w (smorr) 
        Wdvt2w
        % Width dependence of mobility degradation coefficient (smorr) 
        Wu0
        % Width dependence of ua (smorr) 
        Wua
        % Width dependence of ub (smorr) 
        Wub
        % Width dependence of uc (smorr) 
        Wuc
        % Width dependence of vsat (smorr) 
        Wvsat
        % Width dependence of a0 (smorr) 
        Wa0
        % Width dependence of ags (smorr) 
        Wags
        % Width dependence of b0 (smorr) 
        Wb0
        % Width dependence of b1 (smorr) 
        Wb1
        % Width dependence of keta (smorr) 
        Wketa
        % Width dependence of ketas (smorr) 
        Wketas
        % Width dependence of a1 (smorr) 
        Wa1
        % Width dependence of a2 (smorr) 
        Wa2
        % Width dependence of rdsw (smorr) 
        Wrdsw
        % Width dependence of prwb (smorr) 
        Wprwb
        % Width dependence of prwg (smorr) 
        Wprwg
        % Width dependence of wr (smorr) 
        Wwr
        % Width dependence of nfactor (smorr) 
        Wnfactor
        % Width dependence of dwg (smorr) 
        Wdwg
        % Width dependence of dwb (smorr) 
        Wdwb
        % Width dependence of voff (smorr) 
        Wvoff
        % Width dependence of barrier lowering coefficient (smorr) Unit: um
        Weta0
        % Width dependence of sens. of Eta to Vbs (smorr) Unit: um/V
        Wetab
        % Width dependence of dsub (smorr) 
        Wdsub
        % Width dependence of cit (smorr) 
        Wcit
        % Width dependence of cdsc (smorr) 
        Wcdsc
        % Width dependence of cdscb (smorr) 
        Wcdscb
        % Width dependence of cdscd (smorr) 
        Wcdscd
        % Width dependence of pclm (smorr) 
        Wpclm
        % Width dependence of pdiblc1 (smorr) 
        Wpdiblc1
        % Width dependence of pdiblc2 (smorr) 
        Wpdiblc2
        % Width dependence of pdiblcb (smorr) 
        Wpdiblcb
        % Width dependence of drout (smorr) 
        Wdrout
        % Width dependence of pvag (smorr) 
        Wpvag
        % Width dependence of delta (smorr) 
        Wdelta
        % Width dependence of alpha0 (smorr) 
        Walpha0
        % Width dependence of fbjtii (smorr) 
        Wfbjtii
        % Width dependence of bet (smorr) 
        Wbeta0
        % Width dependence of beta1 (smorr) 
        Wbeta1
        % Width dependence of beta2 (smorr) 
        Wbeta2
        % Width dependence of vdsatii0 (smorr) 
        Wvdsatii0
        % Width dependence of lii (smorr) 
        Wlii
        % Width dependence of esatii (smorr) 
        Wesatii
        % Width dependence of sii0 (smorr) 
        Wsii0
        % Width dependence of sii1 (smorr) 
        Wsii1
        % Width dependence of sii2 (smorr) 
        Wsii2
        % Width dependence of siid (smorr) 
        Wsiid
        % Width dependence of agidl (smorr) 
        Wagidl
        % Width dependence of bgidl (smorr) 
        Wbgidl
        % Width dependence of ngidl (smorr) 
        Wngidl
        % Width dependence of ntun (smorr) 
        Wntun
        % Width dependence of ndiode (smorr) 
        Wndiode
        % Width dependence of nrecf0 (smorr) 
        Wnrecf0
        % Width dependence of nrecr0 (smorr) 
        Wnrecr0
        % Width dependence of isbjt (smorr) 
        Wisbjt
        % Width dependence of isdif (smorr) 
        Wisdif
        % Width dependence of istun (smorr) 
        Wistun
        % Width dependence of vrec0 (smorr) 
        Wvrec0
        % Width dependence of vtun0 (smorr) 
        Wvtun0
        % Width dependence of nbjt (smorr) 
        Wnbjt
        % Width dependence of lbjt0 (smorr) 
        Wlbjt0
        % Width dependence of vabjt (smorr) 
        Wvabjt
        % Width dependence of aely (smorr) 
        Waely
        % Width dependence of ahli (smorr) 
        Wahli
        % Width dependence of vsdfb (smorr) 
        Wvsdfb
        % Width dependence of vsdth (smorr) 
        Wvsdth
        % Width dependence of delvt (smorr) 
        Wdelvt
        % Width dependence of Acde (smorr) 
        Wacde
        % Width dependence of Moin (smorr) Unit: um*V^(1/2)
        Wmoin
        % Cross-term dependence of nch (smorr) 
        Pnch
        % Cross-term dependence of nsub (smorr) 
        Pnsub
        % Cross-term dependence of ngate (smorr) 
        Pngate
        % Cross-term dependence of vth0 (smorr) 
        Pvth0
        % Cross-term dependence of k1 (smorr) 
        Pk1
        % Cross-term dependence of K1w1 (smorr) 
        Pk1w1
        % Cross-term dependence of K1w2 (smorr) 
        Pk1w2
        % Cross-term dependence of k2 (smorr) 
        Pk2
        % Cross-term dependence of k3 (smorr) 
        Pk3
        % Cross-term dependence of k3b (smorr) 
        Pk3b
        % Cross-term dependence of kb1 (smorr) 
        Pkb1
        % Cross-term dependence of w0 (smorr) 
        Pw0
        % Cross-term dependence of nlx (smorr) 
        Pnlx
        % Cross-term dependence of dvt0 (smorr) 
        Pdvt0
        % Cross-term dependence of dvt1 (smorr) 
        Pdvt1
        % Cross-term dependence of dvt2 (smorr) 
        Pdvt2
        % Cross-term dependence of dvt0w (smorr) 
        Pdvt0w
        % Cross-term dependence of dvt1w (smorr) 
        Pdvt1w
        % Cross-term dependence of dvt2w (smorr) 
        Pdvt2w
        % Cross-term dependence of u0 (smorr) 
        Pu0
        % Cross-term dependence of ua (smorr) 
        Pua
        % Cross-term dependence of ub (smorr) 
        Pub
        % Cross-term dependence of uc (smorr) 
        Puc
        % Cross-term dependence of vsat (smorr) 
        Pvsat
        % Cross-term dependence of a0 (smorr) 
        Pa0
        % Cross-term dependence of ags (smorr) 
        Pags
        % Cross-term dependence of b0 (smorr) 
        Pb0
        % Cross-term dependence of b1 (smorr) 
        Pb1
        % Cross-term dependence of keta (smorr) 
        Pketa
        % Cross-term dependence of ketas (smorr) 
        Pketas
        % Cross-term dependence of a1 (smorr) 
        Pa1
        % Cross-term dependence of a2 (smorr) 
        Pa2
        % Cross-term dependence of rdsw (smorr) 
        Prdsw
        % Cross-term dependence of prwb (smorr) 
        Pprwb
        % Cross-term dependence of prwg (smorr) 
        Pprwg
        % Cross-term dependence of wr (smorr) 
        Pwr
        % Cross-term dependence of nfactor (smorr) 
        Pnfactor
        % Cross-term dependence of dwg (smorr) 
        Pdwg
        % Cross-term dependence of dwb (smorr) 
        Pdwb
        % Cross-term dependence of voff (smorr) 
        Pvoff
        % Cross-term dependence of eta0 (smorr) 
        Peta0
        % Cross-term dependence of etab (smorr) 
        Petab
        % Cross-term dependence of dsub (smorr) 
        Pdsub
        % Cross-term dependence of cit (smorr) 
        Pcit
        % Cross-term dependence of cdsc (smorr) 
        Pcdsc
        % Cross-term dependence of cdscb (smorr) 
        Pcdscb
        % Cross-term dependence of cdscd (smorr) 
        Pcdscd
        % Cross-term dependence of pclm (smorr) 
        Ppclm
        % Cross-term dependence of pdiblc1 (smorr) 
        Ppdiblc1
        % Cross-term dependence of pdiblc2 (smorr) 
        Ppdiblc2
        % Cross-term dependence of pdiblcb (smorr) 
        Ppdiblcb
        % Cross-term dependence of drout (smorr) 
        Pdrout
        % Cross-term dependence of pvag (smorr) 
        Ppvag
        % Cross-term dependence of delta (smorr) 
        Pdelta
        % Cross-term dependence of alpha0 (smorr) 
        Palpha0
        % Cross-term dependence of fbjtii (smorr) 
        Pfbjtii
        % Cross-term dependence of beta0 (smorr) 
        Pbeta0
        % Cross-term dependence of beta1 (smorr) 
        Pbeta1
        % Cross-term dependence of beta2 (smorr) 
        Pbeta2
        % Cross-term dependence of vdsatii0 (smorr) 
        Pvdsatii0
        % Cross-term dependence of lii (smorr) 
        Plii
        % Cross-term dependence of esatii (smorr) 
        Pesatii
        % Cross-term dependence of sii0 (smorr) 
        Psii0
        % Cross-term dependence of sii1 (smorr) 
        Psii1
        % Cross-term dependence of sii2 (smorr) 
        Psii2
        % Cross-term dependence of siid (smorr) 
        Psiid
        % Cross-term dependence of agidl (smorr) 
        Pagidl
        % Cross-term dependence of bgidl (smorr) 
        Pbgidl
        % Cross-term dependence of ngidl (smorr) 
        Pngidl
        % Cross-term dependence of ntun (smorr) 
        Pntun
        % Cross-term dependence of ndiode (smorr) 
        Pndiode
        % Cross-term dependence of nrecf0 (smorr) 
        Pnrecf0
        % Cross-term dependence of nrecr0 (smorr) 
        Pnrecr0
        % Cross-term dependence of isbjt (smorr) 
        Pisbjt
        % Cross-term dependence of isdif (smorr) 
        Pisdif
        % Cross-term dependence of istun (smorr) 
        Pistun
        % Cross-term dependence of vrec0 (smorr) 
        Pvrec0
        % Cross-term dependence of vtun0 (smorr) 
        Pvtun0
        % Cross-term dependence of nbjt (smorr) 
        Pnbjt
        % Cross-term dependence of lbjt0 (smorr) 
        Plbjt0
        % Cross-term dependence of vabjt (smorr) 
        Pvabjt
        % Cross-term dependence of aely (smorr) 
        Paely
        % Cross-term dependence of ahli (smorr) 
        Pahli
        % Cross-term dependence of vsdfb (smorr) 
        Pvsdfb
        % Cross-term dependence of vsdth (smorr) 
        Pvsdth
        % Cross-term dependence of delvt (smorr) 
        Pdelvt
        % Cross-term dependence of Acde (smorr) 
        Pacde
        % Cross-term dependence of Moin (smorr) 
        Pmoin
        % Substrate junction forward bias (warning) (smorr) Unit: V
        wVsubfwd
        % Substrate junction reverse breakdown voltage (warning) (smorr) Unit: V
        wBvsub
        % Gate oxide breakdown voltage (warning) (smorr) Unit: V
        wBvg
        % Drain-source breakdown voltage (warning) (smorr) Unit: V
        wBvds
        % Maximum drain-source current (warning) (smorr) Unit: A
        wIdsmax
        % Maximum power dissipation (warning) (smorr) Unit: W
        wPmax
        % Secured model parameters (s---b) 
        Secured
    end
    methods
        function obj = set.NMOS(obj,val)
            obj = setParameter(obj,'NMOS',val,0,'boolean');
        end
        function res = get.NMOS(obj)
            res = getParameter(obj,'NMOS');
        end
        function obj = set.PMOS(obj,val)
            obj = setParameter(obj,'PMOS',val,0,'boolean');
        end
        function res = get.PMOS(obj)
            res = getParameter(obj,'PMOS');
        end
        function obj = set.Capmod(obj,val)
            obj = setParameter(obj,'Capmod',val,0,'integer');
        end
        function res = get.Capmod(obj)
            res = getParameter(obj,'Capmod');
        end
        function obj = set.Mobmod(obj,val)
            obj = setParameter(obj,'Mobmod',val,0,'integer');
        end
        function res = get.Mobmod(obj)
            res = getParameter(obj,'Mobmod');
        end
        function obj = set.Noimod(obj,val)
            obj = setParameter(obj,'Noimod',val,0,'integer');
        end
        function res = get.Noimod(obj)
            res = getParameter(obj,'Noimod');
        end
        function obj = set.Paramchk(obj,val)
            obj = setParameter(obj,'Paramchk',val,0,'integer');
        end
        function res = get.Paramchk(obj)
            res = getParameter(obj,'Paramchk');
        end
        function obj = set.Binunit(obj,val)
            obj = setParameter(obj,'Binunit',val,0,'integer');
        end
        function res = get.Binunit(obj)
            res = getParameter(obj,'Binunit');
        end
        function obj = set.Version(obj,val)
            obj = setParameter(obj,'Version',val,0,'real');
        end
        function res = get.Version(obj)
            res = getParameter(obj,'Version');
        end
        function obj = set.Tox(obj,val)
            obj = setParameter(obj,'Tox',val,0,'real');
        end
        function res = get.Tox(obj)
            res = getParameter(obj,'Tox');
        end
        function obj = set.Cdsc(obj,val)
            obj = setParameter(obj,'Cdsc',val,0,'real');
        end
        function res = get.Cdsc(obj)
            res = getParameter(obj,'Cdsc');
        end
        function obj = set.Cdscb(obj,val)
            obj = setParameter(obj,'Cdscb',val,0,'real');
        end
        function res = get.Cdscb(obj)
            res = getParameter(obj,'Cdscb');
        end
        function obj = set.Cdscd(obj,val)
            obj = setParameter(obj,'Cdscd',val,0,'real');
        end
        function res = get.Cdscd(obj)
            res = getParameter(obj,'Cdscd');
        end
        function obj = set.Cit(obj,val)
            obj = setParameter(obj,'Cit',val,0,'real');
        end
        function res = get.Cit(obj)
            res = getParameter(obj,'Cit');
        end
        function obj = set.Nfactor(obj,val)
            obj = setParameter(obj,'Nfactor',val,0,'real');
        end
        function res = get.Nfactor(obj)
            res = getParameter(obj,'Nfactor');
        end
        function obj = set.Vsat(obj,val)
            obj = setParameter(obj,'Vsat',val,0,'real');
        end
        function res = get.Vsat(obj)
            res = getParameter(obj,'Vsat');
        end
        function obj = set.At(obj,val)
            obj = setParameter(obj,'At',val,0,'real');
        end
        function res = get.At(obj)
            res = getParameter(obj,'At');
        end
        function obj = set.A0(obj,val)
            obj = setParameter(obj,'A0',val,0,'real');
        end
        function res = get.A0(obj)
            res = getParameter(obj,'A0');
        end
        function obj = set.Ags(obj,val)
            obj = setParameter(obj,'Ags',val,0,'real');
        end
        function res = get.Ags(obj)
            res = getParameter(obj,'Ags');
        end
        function obj = set.A1(obj,val)
            obj = setParameter(obj,'A1',val,0,'real');
        end
        function res = get.A1(obj)
            res = getParameter(obj,'A1');
        end
        function obj = set.A2(obj,val)
            obj = setParameter(obj,'A2',val,0,'real');
        end
        function res = get.A2(obj)
            res = getParameter(obj,'A2');
        end
        function obj = set.Keta(obj,val)
            obj = setParameter(obj,'Keta',val,0,'real');
        end
        function res = get.Keta(obj)
            res = getParameter(obj,'Keta');
        end
        function obj = set.Nsub(obj,val)
            obj = setParameter(obj,'Nsub',val,0,'real');
        end
        function res = get.Nsub(obj)
            res = getParameter(obj,'Nsub');
        end
        function obj = set.Nch(obj,val)
            obj = setParameter(obj,'Nch',val,0,'real');
        end
        function res = get.Nch(obj)
            res = getParameter(obj,'Nch');
        end
        function obj = set.Ngate(obj,val)
            obj = setParameter(obj,'Ngate',val,0,'real');
        end
        function res = get.Ngate(obj)
            res = getParameter(obj,'Ngate');
        end
        function obj = set.Gamma1(obj,val)
            obj = setParameter(obj,'Gamma1',val,0,'real');
        end
        function res = get.Gamma1(obj)
            res = getParameter(obj,'Gamma1');
        end
        function obj = set.Gamma2(obj,val)
            obj = setParameter(obj,'Gamma2',val,0,'real');
        end
        function res = get.Gamma2(obj)
            res = getParameter(obj,'Gamma2');
        end
        function obj = set.Vbx(obj,val)
            obj = setParameter(obj,'Vbx',val,0,'real');
        end
        function res = get.Vbx(obj)
            res = getParameter(obj,'Vbx');
        end
        function obj = set.Vbm(obj,val)
            obj = setParameter(obj,'Vbm',val,0,'real');
        end
        function res = get.Vbm(obj)
            res = getParameter(obj,'Vbm');
        end
        function obj = set.Xt(obj,val)
            obj = setParameter(obj,'Xt',val,0,'real');
        end
        function res = get.Xt(obj)
            res = getParameter(obj,'Xt');
        end
        function obj = set.K1(obj,val)
            obj = setParameter(obj,'K1',val,0,'real');
        end
        function res = get.K1(obj)
            res = getParameter(obj,'K1');
        end
        function obj = set.Kt1(obj,val)
            obj = setParameter(obj,'Kt1',val,0,'real');
        end
        function res = get.Kt1(obj)
            res = getParameter(obj,'Kt1');
        end
        function obj = set.Kt1l(obj,val)
            obj = setParameter(obj,'Kt1l',val,0,'real');
        end
        function res = get.Kt1l(obj)
            res = getParameter(obj,'Kt1l');
        end
        function obj = set.Kt2(obj,val)
            obj = setParameter(obj,'Kt2',val,0,'real');
        end
        function res = get.Kt2(obj)
            res = getParameter(obj,'Kt2');
        end
        function obj = set.K2(obj,val)
            obj = setParameter(obj,'K2',val,0,'real');
        end
        function res = get.K2(obj)
            res = getParameter(obj,'K2');
        end
        function obj = set.K3(obj,val)
            obj = setParameter(obj,'K3',val,0,'real');
        end
        function res = get.K3(obj)
            res = getParameter(obj,'K3');
        end
        function obj = set.K3b(obj,val)
            obj = setParameter(obj,'K3b',val,0,'real');
        end
        function res = get.K3b(obj)
            res = getParameter(obj,'K3b');
        end
        function obj = set.W0(obj,val)
            obj = setParameter(obj,'W0',val,0,'real');
        end
        function res = get.W0(obj)
            res = getParameter(obj,'W0');
        end
        function obj = set.Nlx(obj,val)
            obj = setParameter(obj,'Nlx',val,0,'real');
        end
        function res = get.Nlx(obj)
            res = getParameter(obj,'Nlx');
        end
        function obj = set.Dvt0(obj,val)
            obj = setParameter(obj,'Dvt0',val,0,'real');
        end
        function res = get.Dvt0(obj)
            res = getParameter(obj,'Dvt0');
        end
        function obj = set.Dvt1(obj,val)
            obj = setParameter(obj,'Dvt1',val,0,'real');
        end
        function res = get.Dvt1(obj)
            res = getParameter(obj,'Dvt1');
        end
        function obj = set.Dvt2(obj,val)
            obj = setParameter(obj,'Dvt2',val,0,'real');
        end
        function res = get.Dvt2(obj)
            res = getParameter(obj,'Dvt2');
        end
        function obj = set.Dvt0w(obj,val)
            obj = setParameter(obj,'Dvt0w',val,0,'real');
        end
        function res = get.Dvt0w(obj)
            res = getParameter(obj,'Dvt0w');
        end
        function obj = set.Dvt1w(obj,val)
            obj = setParameter(obj,'Dvt1w',val,0,'real');
        end
        function res = get.Dvt1w(obj)
            res = getParameter(obj,'Dvt1w');
        end
        function obj = set.Dvt2w(obj,val)
            obj = setParameter(obj,'Dvt2w',val,0,'real');
        end
        function res = get.Dvt2w(obj)
            res = getParameter(obj,'Dvt2w');
        end
        function obj = set.Drout(obj,val)
            obj = setParameter(obj,'Drout',val,0,'real');
        end
        function res = get.Drout(obj)
            res = getParameter(obj,'Drout');
        end
        function obj = set.Dsub(obj,val)
            obj = setParameter(obj,'Dsub',val,0,'real');
        end
        function res = get.Dsub(obj)
            res = getParameter(obj,'Dsub');
        end
        function obj = set.Vth0(obj,val)
            obj = setParameter(obj,'Vth0',val,0,'real');
        end
        function res = get.Vth0(obj)
            res = getParameter(obj,'Vth0');
        end
        function obj = set.Ua(obj,val)
            obj = setParameter(obj,'Ua',val,0,'real');
        end
        function res = get.Ua(obj)
            res = getParameter(obj,'Ua');
        end
        function obj = set.Ua1(obj,val)
            obj = setParameter(obj,'Ua1',val,0,'real');
        end
        function res = get.Ua1(obj)
            res = getParameter(obj,'Ua1');
        end
        function obj = set.Ub(obj,val)
            obj = setParameter(obj,'Ub',val,0,'real');
        end
        function res = get.Ub(obj)
            res = getParameter(obj,'Ub');
        end
        function obj = set.Ub1(obj,val)
            obj = setParameter(obj,'Ub1',val,0,'real');
        end
        function res = get.Ub1(obj)
            res = getParameter(obj,'Ub1');
        end
        function obj = set.Uc(obj,val)
            obj = setParameter(obj,'Uc',val,0,'real');
        end
        function res = get.Uc(obj)
            res = getParameter(obj,'Uc');
        end
        function obj = set.Uc1(obj,val)
            obj = setParameter(obj,'Uc1',val,0,'real');
        end
        function res = get.Uc1(obj)
            res = getParameter(obj,'Uc1');
        end
        function obj = set.U0(obj,val)
            obj = setParameter(obj,'U0',val,0,'real');
        end
        function res = get.U0(obj)
            res = getParameter(obj,'U0');
        end
        function obj = set.Ute(obj,val)
            obj = setParameter(obj,'Ute',val,0,'real');
        end
        function res = get.Ute(obj)
            res = getParameter(obj,'Ute');
        end
        function obj = set.Voff(obj,val)
            obj = setParameter(obj,'Voff',val,0,'real');
        end
        function res = get.Voff(obj)
            res = getParameter(obj,'Voff');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Cgso(obj,val)
            obj = setParameter(obj,'Cgso',val,0,'real');
        end
        function res = get.Cgso(obj)
            res = getParameter(obj,'Cgso');
        end
        function obj = set.Cgdo(obj,val)
            obj = setParameter(obj,'Cgdo',val,0,'real');
        end
        function res = get.Cgdo(obj)
            res = getParameter(obj,'Cgdo');
        end
        function obj = set.Xpart(obj,val)
            obj = setParameter(obj,'Xpart',val,0,'real');
        end
        function res = get.Xpart(obj)
            res = getParameter(obj,'Xpart');
        end
        function obj = set.Delta(obj,val)
            obj = setParameter(obj,'Delta',val,0,'real');
        end
        function res = get.Delta(obj)
            res = getParameter(obj,'Delta');
        end
        function obj = set.Rsh(obj,val)
            obj = setParameter(obj,'Rsh',val,0,'real');
        end
        function res = get.Rsh(obj)
            res = getParameter(obj,'Rsh');
        end
        function obj = set.Rdsw(obj,val)
            obj = setParameter(obj,'Rdsw',val,0,'real');
        end
        function res = get.Rdsw(obj)
            res = getParameter(obj,'Rdsw');
        end
        function obj = set.Prwg(obj,val)
            obj = setParameter(obj,'Prwg',val,0,'real');
        end
        function res = get.Prwg(obj)
            res = getParameter(obj,'Prwg');
        end
        function obj = set.Prwb(obj,val)
            obj = setParameter(obj,'Prwb',val,0,'real');
        end
        function res = get.Prwb(obj)
            res = getParameter(obj,'Prwb');
        end
        function obj = set.Prt(obj,val)
            obj = setParameter(obj,'Prt',val,0,'real');
        end
        function res = get.Prt(obj)
            res = getParameter(obj,'Prt');
        end
        function obj = set.Eta0(obj,val)
            obj = setParameter(obj,'Eta0',val,0,'real');
        end
        function res = get.Eta0(obj)
            res = getParameter(obj,'Eta0');
        end
        function obj = set.Etab(obj,val)
            obj = setParameter(obj,'Etab',val,0,'real');
        end
        function res = get.Etab(obj)
            res = getParameter(obj,'Etab');
        end
        function obj = set.Pclm(obj,val)
            obj = setParameter(obj,'Pclm',val,0,'real');
        end
        function res = get.Pclm(obj)
            res = getParameter(obj,'Pclm');
        end
        function obj = set.Pdiblc1(obj,val)
            obj = setParameter(obj,'Pdiblc1',val,0,'real');
        end
        function res = get.Pdiblc1(obj)
            res = getParameter(obj,'Pdiblc1');
        end
        function obj = set.Pdiblc2(obj,val)
            obj = setParameter(obj,'Pdiblc2',val,0,'real');
        end
        function res = get.Pdiblc2(obj)
            res = getParameter(obj,'Pdiblc2');
        end
        function obj = set.Pdiblcb(obj,val)
            obj = setParameter(obj,'Pdiblcb',val,0,'real');
        end
        function res = get.Pdiblcb(obj)
            res = getParameter(obj,'Pdiblcb');
        end
        function obj = set.Pvag(obj,val)
            obj = setParameter(obj,'Pvag',val,0,'real');
        end
        function res = get.Pvag(obj)
            res = getParameter(obj,'Pvag');
        end
        function obj = set.Shmod(obj,val)
            obj = setParameter(obj,'Shmod',val,0,'integer');
        end
        function res = get.Shmod(obj)
            res = getParameter(obj,'Shmod');
        end
        function obj = set.Ddmod(obj,val)
            obj = setParameter(obj,'Ddmod',val,0,'integer');
        end
        function res = get.Ddmod(obj)
            res = getParameter(obj,'Ddmod');
        end
        function obj = set.Tbox(obj,val)
            obj = setParameter(obj,'Tbox',val,0,'real');
        end
        function res = get.Tbox(obj)
            res = getParameter(obj,'Tbox');
        end
        function obj = set.Tsi(obj,val)
            obj = setParameter(obj,'Tsi',val,0,'real');
        end
        function res = get.Tsi(obj)
            res = getParameter(obj,'Tsi');
        end
        function obj = set.Xj(obj,val)
            obj = setParameter(obj,'Xj',val,0,'real');
        end
        function res = get.Xj(obj)
            res = getParameter(obj,'Xj');
        end
        function obj = set.Rth0(obj,val)
            obj = setParameter(obj,'Rth0',val,0,'real');
        end
        function res = get.Rth0(obj)
            res = getParameter(obj,'Rth0');
        end
        function obj = set.Cth0(obj,val)
            obj = setParameter(obj,'Cth0',val,0,'real');
        end
        function res = get.Cth0(obj)
            res = getParameter(obj,'Cth0');
        end
        function obj = set.Ngidl(obj,val)
            obj = setParameter(obj,'Ngidl',val,0,'real');
        end
        function res = get.Ngidl(obj)
            res = getParameter(obj,'Ngidl');
        end
        function obj = set.Agidl(obj,val)
            obj = setParameter(obj,'Agidl',val,0,'real');
        end
        function res = get.Agidl(obj)
            res = getParameter(obj,'Agidl');
        end
        function obj = set.Bgidl(obj,val)
            obj = setParameter(obj,'Bgidl',val,0,'real');
        end
        function res = get.Bgidl(obj)
            res = getParameter(obj,'Bgidl');
        end
        function obj = set.Ndiode(obj,val)
            obj = setParameter(obj,'Ndiode',val,0,'real');
        end
        function res = get.Ndiode(obj)
            res = getParameter(obj,'Ndiode');
        end
        function obj = set.Xbjt(obj,val)
            obj = setParameter(obj,'Xbjt',val,0,'real');
        end
        function res = get.Xbjt(obj)
            res = getParameter(obj,'Xbjt');
        end
        function obj = set.Xdif(obj,val)
            obj = setParameter(obj,'Xdif',val,0,'real');
        end
        function res = get.Xdif(obj)
            res = getParameter(obj,'Xdif');
        end
        function obj = set.Xrec(obj,val)
            obj = setParameter(obj,'Xrec',val,0,'real');
        end
        function res = get.Xrec(obj)
            res = getParameter(obj,'Xrec');
        end
        function obj = set.Xtun(obj,val)
            obj = setParameter(obj,'Xtun',val,0,'real');
        end
        function res = get.Xtun(obj)
            res = getParameter(obj,'Xtun');
        end
        function obj = set.Pbswg(obj,val)
            obj = setParameter(obj,'Pbswg',val,0,'real');
        end
        function res = get.Pbswg(obj)
            res = getParameter(obj,'Pbswg');
        end
        function obj = set.Mjswg(obj,val)
            obj = setParameter(obj,'Mjswg',val,0,'real');
        end
        function res = get.Mjswg(obj)
            res = getParameter(obj,'Mjswg');
        end
        function obj = set.Cjswg(obj,val)
            obj = setParameter(obj,'Cjswg',val,0,'real');
        end
        function res = get.Cjswg(obj)
            res = getParameter(obj,'Cjswg');
        end
        function obj = set.Lint(obj,val)
            obj = setParameter(obj,'Lint',val,0,'real');
        end
        function res = get.Lint(obj)
            res = getParameter(obj,'Lint');
        end
        function obj = set.Ll(obj,val)
            obj = setParameter(obj,'Ll',val,0,'real');
        end
        function res = get.Ll(obj)
            res = getParameter(obj,'Ll');
        end
        function obj = set.Lln(obj,val)
            obj = setParameter(obj,'Lln',val,0,'real');
        end
        function res = get.Lln(obj)
            res = getParameter(obj,'Lln');
        end
        function obj = set.Lw(obj,val)
            obj = setParameter(obj,'Lw',val,0,'real');
        end
        function res = get.Lw(obj)
            res = getParameter(obj,'Lw');
        end
        function obj = set.Lwn(obj,val)
            obj = setParameter(obj,'Lwn',val,0,'real');
        end
        function res = get.Lwn(obj)
            res = getParameter(obj,'Lwn');
        end
        function obj = set.Lwl(obj,val)
            obj = setParameter(obj,'Lwl',val,0,'real');
        end
        function res = get.Lwl(obj)
            res = getParameter(obj,'Lwl');
        end
        function obj = set.Wr(obj,val)
            obj = setParameter(obj,'Wr',val,0,'real');
        end
        function res = get.Wr(obj)
            res = getParameter(obj,'Wr');
        end
        function obj = set.Wint(obj,val)
            obj = setParameter(obj,'Wint',val,0,'real');
        end
        function res = get.Wint(obj)
            res = getParameter(obj,'Wint');
        end
        function obj = set.Dwg(obj,val)
            obj = setParameter(obj,'Dwg',val,0,'real');
        end
        function res = get.Dwg(obj)
            res = getParameter(obj,'Dwg');
        end
        function obj = set.Dwb(obj,val)
            obj = setParameter(obj,'Dwb',val,0,'real');
        end
        function res = get.Dwb(obj)
            res = getParameter(obj,'Dwb');
        end
        function obj = set.Wl(obj,val)
            obj = setParameter(obj,'Wl',val,0,'real');
        end
        function res = get.Wl(obj)
            res = getParameter(obj,'Wl');
        end
        function obj = set.Wln(obj,val)
            obj = setParameter(obj,'Wln',val,0,'real');
        end
        function res = get.Wln(obj)
            res = getParameter(obj,'Wln');
        end
        function obj = set.Ww(obj,val)
            obj = setParameter(obj,'Ww',val,0,'real');
        end
        function res = get.Ww(obj)
            res = getParameter(obj,'Ww');
        end
        function obj = set.Wwn(obj,val)
            obj = setParameter(obj,'Wwn',val,0,'real');
        end
        function res = get.Wwn(obj)
            res = getParameter(obj,'Wwn');
        end
        function obj = set.Wwl(obj,val)
            obj = setParameter(obj,'Wwl',val,0,'real');
        end
        function res = get.Wwl(obj)
            res = getParameter(obj,'Wwl');
        end
        function obj = set.B0(obj,val)
            obj = setParameter(obj,'B0',val,0,'real');
        end
        function res = get.B0(obj)
            res = getParameter(obj,'B0');
        end
        function obj = set.B1(obj,val)
            obj = setParameter(obj,'B1',val,0,'real');
        end
        function res = get.B1(obj)
            res = getParameter(obj,'B1');
        end
        function obj = set.Cgsl(obj,val)
            obj = setParameter(obj,'Cgsl',val,0,'real');
        end
        function res = get.Cgsl(obj)
            res = getParameter(obj,'Cgsl');
        end
        function obj = set.Cgdl(obj,val)
            obj = setParameter(obj,'Cgdl',val,0,'real');
        end
        function res = get.Cgdl(obj)
            res = getParameter(obj,'Cgdl');
        end
        function obj = set.Ckappa(obj,val)
            obj = setParameter(obj,'Ckappa',val,0,'real');
        end
        function res = get.Ckappa(obj)
            res = getParameter(obj,'Ckappa');
        end
        function obj = set.Cf(obj,val)
            obj = setParameter(obj,'Cf',val,0,'real');
        end
        function res = get.Cf(obj)
            res = getParameter(obj,'Cf');
        end
        function obj = set.Clc(obj,val)
            obj = setParameter(obj,'Clc',val,0,'real');
        end
        function res = get.Clc(obj)
            res = getParameter(obj,'Clc');
        end
        function obj = set.Cle(obj,val)
            obj = setParameter(obj,'Cle',val,0,'real');
        end
        function res = get.Cle(obj)
            res = getParameter(obj,'Cle');
        end
        function obj = set.Dwc(obj,val)
            obj = setParameter(obj,'Dwc',val,0,'real');
        end
        function res = get.Dwc(obj)
            res = getParameter(obj,'Dwc');
        end
        function obj = set.Dlc(obj,val)
            obj = setParameter(obj,'Dlc',val,0,'real');
        end
        function res = get.Dlc(obj)
            res = getParameter(obj,'Dlc');
        end
        function obj = set.Alpha0(obj,val)
            obj = setParameter(obj,'Alpha0',val,0,'real');
        end
        function res = get.Alpha0(obj)
            res = getParameter(obj,'Alpha0');
        end
        function obj = set.Noia(obj,val)
            obj = setParameter(obj,'Noia',val,0,'real');
        end
        function res = get.Noia(obj)
            res = getParameter(obj,'Noia');
        end
        function obj = set.Noib(obj,val)
            obj = setParameter(obj,'Noib',val,0,'real');
        end
        function res = get.Noib(obj)
            res = getParameter(obj,'Noib');
        end
        function obj = set.Noic(obj,val)
            obj = setParameter(obj,'Noic',val,0,'real');
        end
        function res = get.Noic(obj)
            res = getParameter(obj,'Noic');
        end
        function obj = set.Em(obj,val)
            obj = setParameter(obj,'Em',val,0,'real');
        end
        function res = get.Em(obj)
            res = getParameter(obj,'Em');
        end
        function obj = set.Ef(obj,val)
            obj = setParameter(obj,'Ef',val,0,'real');
        end
        function res = get.Ef(obj)
            res = getParameter(obj,'Ef');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Noif(obj,val)
            obj = setParameter(obj,'Noif',val,0,'real');
        end
        function res = get.Noif(obj)
            res = getParameter(obj,'Noif');
        end
        function obj = set.K1w1(obj,val)
            obj = setParameter(obj,'K1w1',val,0,'real');
        end
        function res = get.K1w1(obj)
            res = getParameter(obj,'K1w1');
        end
        function obj = set.K1w2(obj,val)
            obj = setParameter(obj,'K1w2',val,0,'real');
        end
        function res = get.K1w2(obj)
            res = getParameter(obj,'K1w2');
        end
        function obj = set.Ketas(obj,val)
            obj = setParameter(obj,'Ketas',val,0,'real');
        end
        function res = get.Ketas(obj)
            res = getParameter(obj,'Ketas');
        end
        function obj = set.Dwbc(obj,val)
            obj = setParameter(obj,'Dwbc',val,0,'real');
        end
        function res = get.Dwbc(obj)
            res = getParameter(obj,'Dwbc');
        end
        function obj = set.Beta0(obj,val)
            obj = setParameter(obj,'Beta0',val,0,'real');
        end
        function res = get.Beta0(obj)
            res = getParameter(obj,'Beta0');
        end
        function obj = set.Beta1(obj,val)
            obj = setParameter(obj,'Beta1',val,0,'real');
        end
        function res = get.Beta1(obj)
            res = getParameter(obj,'Beta1');
        end
        function obj = set.Beta2(obj,val)
            obj = setParameter(obj,'Beta2',val,0,'real');
        end
        function res = get.Beta2(obj)
            res = getParameter(obj,'Beta2');
        end
        function obj = set.Vdsatii0(obj,val)
            obj = setParameter(obj,'Vdsatii0',val,0,'real');
        end
        function res = get.Vdsatii0(obj)
            res = getParameter(obj,'Vdsatii0');
        end
        function obj = set.Tii(obj,val)
            obj = setParameter(obj,'Tii',val,0,'real');
        end
        function res = get.Tii(obj)
            res = getParameter(obj,'Tii');
        end
        function obj = set.Lii(obj,val)
            obj = setParameter(obj,'Lii',val,0,'real');
        end
        function res = get.Lii(obj)
            res = getParameter(obj,'Lii');
        end
        function obj = set.Sii0(obj,val)
            obj = setParameter(obj,'Sii0',val,0,'real');
        end
        function res = get.Sii0(obj)
            res = getParameter(obj,'Sii0');
        end
        function obj = set.Sii1(obj,val)
            obj = setParameter(obj,'Sii1',val,0,'real');
        end
        function res = get.Sii1(obj)
            res = getParameter(obj,'Sii1');
        end
        function obj = set.Sii2(obj,val)
            obj = setParameter(obj,'Sii2',val,0,'real');
        end
        function res = get.Sii2(obj)
            res = getParameter(obj,'Sii2');
        end
        function obj = set.Siid(obj,val)
            obj = setParameter(obj,'Siid',val,0,'real');
        end
        function res = get.Siid(obj)
            res = getParameter(obj,'Siid');
        end
        function obj = set.Fbjtii(obj,val)
            obj = setParameter(obj,'Fbjtii',val,0,'real');
        end
        function res = get.Fbjtii(obj)
            res = getParameter(obj,'Fbjtii');
        end
        function obj = set.Esatii(obj,val)
            obj = setParameter(obj,'Esatii',val,0,'real');
        end
        function res = get.Esatii(obj)
            res = getParameter(obj,'Esatii');
        end
        function obj = set.Ntun(obj,val)
            obj = setParameter(obj,'Ntun',val,0,'real');
        end
        function res = get.Ntun(obj)
            res = getParameter(obj,'Ntun');
        end
        function obj = set.Nrecf0(obj,val)
            obj = setParameter(obj,'Nrecf0',val,0,'real');
        end
        function res = get.Nrecf0(obj)
            res = getParameter(obj,'Nrecf0');
        end
        function obj = set.Nrecr0(obj,val)
            obj = setParameter(obj,'Nrecr0',val,0,'real');
        end
        function res = get.Nrecr0(obj)
            res = getParameter(obj,'Nrecr0');
        end
        function obj = set.Isbjt(obj,val)
            obj = setParameter(obj,'Isbjt',val,0,'real');
        end
        function res = get.Isbjt(obj)
            res = getParameter(obj,'Isbjt');
        end
        function obj = set.Isdif(obj,val)
            obj = setParameter(obj,'Isdif',val,0,'real');
        end
        function res = get.Isdif(obj)
            res = getParameter(obj,'Isdif');
        end
        function obj = set.Isrec(obj,val)
            obj = setParameter(obj,'Isrec',val,0,'real');
        end
        function res = get.Isrec(obj)
            res = getParameter(obj,'Isrec');
        end
        function obj = set.Istun(obj,val)
            obj = setParameter(obj,'Istun',val,0,'real');
        end
        function res = get.Istun(obj)
            res = getParameter(obj,'Istun');
        end
        function obj = set.Ln(obj,val)
            obj = setParameter(obj,'Ln',val,0,'real');
        end
        function res = get.Ln(obj)
            res = getParameter(obj,'Ln');
        end
        function obj = set.Vrec0(obj,val)
            obj = setParameter(obj,'Vrec0',val,0,'real');
        end
        function res = get.Vrec0(obj)
            res = getParameter(obj,'Vrec0');
        end
        function obj = set.Vtun0(obj,val)
            obj = setParameter(obj,'Vtun0',val,0,'real');
        end
        function res = get.Vtun0(obj)
            res = getParameter(obj,'Vtun0');
        end
        function obj = set.Nbjt(obj,val)
            obj = setParameter(obj,'Nbjt',val,0,'real');
        end
        function res = get.Nbjt(obj)
            res = getParameter(obj,'Nbjt');
        end
        function obj = set.Lbjt0(obj,val)
            obj = setParameter(obj,'Lbjt0',val,0,'real');
        end
        function res = get.Lbjt0(obj)
            res = getParameter(obj,'Lbjt0');
        end
        function obj = set.Ldif0(obj,val)
            obj = setParameter(obj,'Ldif0',val,0,'real');
        end
        function res = get.Ldif0(obj)
            res = getParameter(obj,'Ldif0');
        end
        function obj = set.Vabjt(obj,val)
            obj = setParameter(obj,'Vabjt',val,0,'real');
        end
        function res = get.Vabjt(obj)
            res = getParameter(obj,'Vabjt');
        end
        function obj = set.Aely(obj,val)
            obj = setParameter(obj,'Aely',val,0,'real');
        end
        function res = get.Aely(obj)
            res = getParameter(obj,'Aely');
        end
        function obj = set.Ahli(obj,val)
            obj = setParameter(obj,'Ahli',val,0,'real');
        end
        function res = get.Ahli(obj)
            res = getParameter(obj,'Ahli');
        end
        function obj = set.Rbody(obj,val)
            obj = setParameter(obj,'Rbody',val,0,'real');
        end
        function res = get.Rbody(obj)
            res = getParameter(obj,'Rbody');
        end
        function obj = set.Rbsh(obj,val)
            obj = setParameter(obj,'Rbsh',val,0,'real');
        end
        function res = get.Rbsh(obj)
            res = getParameter(obj,'Rbsh');
        end
        function obj = set.Cgeo(obj,val)
            obj = setParameter(obj,'Cgeo',val,0,'real');
        end
        function res = get.Cgeo(obj)
            res = getParameter(obj,'Cgeo');
        end
        function obj = set.Tt(obj,val)
            obj = setParameter(obj,'Tt',val,0,'real');
        end
        function res = get.Tt(obj)
            res = getParameter(obj,'Tt');
        end
        function obj = set.Ndif(obj,val)
            obj = setParameter(obj,'Ndif',val,0,'real');
        end
        function res = get.Ndif(obj)
            res = getParameter(obj,'Ndif');
        end
        function obj = set.Vsdfb(obj,val)
            obj = setParameter(obj,'Vsdfb',val,0,'real');
        end
        function res = get.Vsdfb(obj)
            res = getParameter(obj,'Vsdfb');
        end
        function obj = set.Vsdth(obj,val)
            obj = setParameter(obj,'Vsdth',val,0,'real');
        end
        function res = get.Vsdth(obj)
            res = getParameter(obj,'Vsdth');
        end
        function obj = set.Csdmin(obj,val)
            obj = setParameter(obj,'Csdmin',val,0,'real');
        end
        function res = get.Csdmin(obj)
            res = getParameter(obj,'Csdmin');
        end
        function obj = set.Asd(obj,val)
            obj = setParameter(obj,'Asd',val,0,'real');
        end
        function res = get.Asd(obj)
            res = getParameter(obj,'Asd');
        end
        function obj = set.Csdesw(obj,val)
            obj = setParameter(obj,'Csdesw',val,0,'real');
        end
        function res = get.Csdesw(obj)
            res = getParameter(obj,'Csdesw');
        end
        function obj = set.Ntrecf(obj,val)
            obj = setParameter(obj,'Ntrecf',val,0,'real');
        end
        function res = get.Ntrecf(obj)
            res = getParameter(obj,'Ntrecf');
        end
        function obj = set.Ntrecr(obj,val)
            obj = setParameter(obj,'Ntrecr',val,0,'real');
        end
        function res = get.Ntrecr(obj)
            res = getParameter(obj,'Ntrecr');
        end
        function obj = set.Dlcb(obj,val)
            obj = setParameter(obj,'Dlcb',val,0,'real');
        end
        function res = get.Dlcb(obj)
            res = getParameter(obj,'Dlcb');
        end
        function obj = set.Fbody(obj,val)
            obj = setParameter(obj,'Fbody',val,0,'real');
        end
        function res = get.Fbody(obj)
            res = getParameter(obj,'Fbody');
        end
        function obj = set.Tcjswg(obj,val)
            obj = setParameter(obj,'Tcjswg',val,0,'real');
        end
        function res = get.Tcjswg(obj)
            res = getParameter(obj,'Tcjswg');
        end
        function obj = set.Tpbswg(obj,val)
            obj = setParameter(obj,'Tpbswg',val,0,'real');
        end
        function res = get.Tpbswg(obj)
            res = getParameter(obj,'Tpbswg');
        end
        function obj = set.Acde(obj,val)
            obj = setParameter(obj,'Acde',val,0,'real');
        end
        function res = get.Acde(obj)
            res = getParameter(obj,'Acde');
        end
        function obj = set.Moin(obj,val)
            obj = setParameter(obj,'Moin',val,0,'real');
        end
        function res = get.Moin(obj)
            res = getParameter(obj,'Moin');
        end
        function obj = set.Delvt(obj,val)
            obj = setParameter(obj,'Delvt',val,0,'real');
        end
        function res = get.Delvt(obj)
            res = getParameter(obj,'Delvt');
        end
        function obj = set.Kb1(obj,val)
            obj = setParameter(obj,'Kb1',val,0,'real');
        end
        function res = get.Kb1(obj)
            res = getParameter(obj,'Kb1');
        end
        function obj = set.Dlbg(obj,val)
            obj = setParameter(obj,'Dlbg',val,0,'real');
        end
        function res = get.Dlbg(obj)
            res = getParameter(obj,'Dlbg');
        end
        function obj = set.Igmod(obj,val)
            obj = setParameter(obj,'Igmod',val,0,'integer');
        end
        function res = get.Igmod(obj)
            res = getParameter(obj,'Igmod');
        end
        function obj = set.Toxqm(obj,val)
            obj = setParameter(obj,'Toxqm',val,0,'real');
        end
        function res = get.Toxqm(obj)
            res = getParameter(obj,'Toxqm');
        end
        function obj = set.Wth0(obj,val)
            obj = setParameter(obj,'Wth0',val,0,'real');
        end
        function res = get.Wth0(obj)
            res = getParameter(obj,'Wth0');
        end
        function obj = set.Rhalo(obj,val)
            obj = setParameter(obj,'Rhalo',val,0,'real');
        end
        function res = get.Rhalo(obj)
            res = getParameter(obj,'Rhalo');
        end
        function obj = set.Ntox(obj,val)
            obj = setParameter(obj,'Ntox',val,0,'real');
        end
        function res = get.Ntox(obj)
            res = getParameter(obj,'Ntox');
        end
        function obj = set.Toxref(obj,val)
            obj = setParameter(obj,'Toxref',val,0,'real');
        end
        function res = get.Toxref(obj)
            res = getParameter(obj,'Toxref');
        end
        function obj = set.Ebg(obj,val)
            obj = setParameter(obj,'Ebg',val,0,'real');
        end
        function res = get.Ebg(obj)
            res = getParameter(obj,'Ebg');
        end
        function obj = set.Nevb(obj,val)
            obj = setParameter(obj,'Nevb',val,0,'real');
        end
        function res = get.Nevb(obj)
            res = getParameter(obj,'Nevb');
        end
        function obj = set.Alphagb1(obj,val)
            obj = setParameter(obj,'Alphagb1',val,0,'real');
        end
        function res = get.Alphagb1(obj)
            res = getParameter(obj,'Alphagb1');
        end
        function obj = set.Betagb1(obj,val)
            obj = setParameter(obj,'Betagb1',val,0,'real');
        end
        function res = get.Betagb1(obj)
            res = getParameter(obj,'Betagb1');
        end
        function obj = set.Vgb1(obj,val)
            obj = setParameter(obj,'Vgb1',val,0,'real');
        end
        function res = get.Vgb1(obj)
            res = getParameter(obj,'Vgb1');
        end
        function obj = set.Necb(obj,val)
            obj = setParameter(obj,'Necb',val,0,'real');
        end
        function res = get.Necb(obj)
            res = getParameter(obj,'Necb');
        end
        function obj = set.Alphagb2(obj,val)
            obj = setParameter(obj,'Alphagb2',val,0,'real');
        end
        function res = get.Alphagb2(obj)
            res = getParameter(obj,'Alphagb2');
        end
        function obj = set.Betagb2(obj,val)
            obj = setParameter(obj,'Betagb2',val,0,'real');
        end
        function res = get.Betagb2(obj)
            res = getParameter(obj,'Betagb2');
        end
        function obj = set.Vgb2(obj,val)
            obj = setParameter(obj,'Vgb2',val,0,'real');
        end
        function res = get.Vgb2(obj)
            res = getParameter(obj,'Vgb2');
        end
        function obj = set.Voxh(obj,val)
            obj = setParameter(obj,'Voxh',val,0,'real');
        end
        function res = get.Voxh(obj)
            res = getParameter(obj,'Voxh');
        end
        function obj = set.Deltavox(obj,val)
            obj = setParameter(obj,'Deltavox',val,0,'real');
        end
        function res = get.Deltavox(obj)
            res = getParameter(obj,'Deltavox');
        end
        function obj = set.Lnch(obj,val)
            obj = setParameter(obj,'Lnch',val,0,'real');
        end
        function res = get.Lnch(obj)
            res = getParameter(obj,'Lnch');
        end
        function obj = set.Lnsub(obj,val)
            obj = setParameter(obj,'Lnsub',val,0,'real');
        end
        function res = get.Lnsub(obj)
            res = getParameter(obj,'Lnsub');
        end
        function obj = set.Lngate(obj,val)
            obj = setParameter(obj,'Lngate',val,0,'real');
        end
        function res = get.Lngate(obj)
            res = getParameter(obj,'Lngate');
        end
        function obj = set.Lvth0(obj,val)
            obj = setParameter(obj,'Lvth0',val,0,'real');
        end
        function res = get.Lvth0(obj)
            res = getParameter(obj,'Lvth0');
        end
        function obj = set.Lk1(obj,val)
            obj = setParameter(obj,'Lk1',val,0,'real');
        end
        function res = get.Lk1(obj)
            res = getParameter(obj,'Lk1');
        end
        function obj = set.Lk1w1(obj,val)
            obj = setParameter(obj,'Lk1w1',val,0,'real');
        end
        function res = get.Lk1w1(obj)
            res = getParameter(obj,'Lk1w1');
        end
        function obj = set.Lk1w2(obj,val)
            obj = setParameter(obj,'Lk1w2',val,0,'real');
        end
        function res = get.Lk1w2(obj)
            res = getParameter(obj,'Lk1w2');
        end
        function obj = set.Lk2(obj,val)
            obj = setParameter(obj,'Lk2',val,0,'real');
        end
        function res = get.Lk2(obj)
            res = getParameter(obj,'Lk2');
        end
        function obj = set.Lk3(obj,val)
            obj = setParameter(obj,'Lk3',val,0,'real');
        end
        function res = get.Lk3(obj)
            res = getParameter(obj,'Lk3');
        end
        function obj = set.Lk3b(obj,val)
            obj = setParameter(obj,'Lk3b',val,0,'real');
        end
        function res = get.Lk3b(obj)
            res = getParameter(obj,'Lk3b');
        end
        function obj = set.Lkb1(obj,val)
            obj = setParameter(obj,'Lkb1',val,0,'real');
        end
        function res = get.Lkb1(obj)
            res = getParameter(obj,'Lkb1');
        end
        function obj = set.Lw0(obj,val)
            obj = setParameter(obj,'Lw0',val,0,'real');
        end
        function res = get.Lw0(obj)
            res = getParameter(obj,'Lw0');
        end
        function obj = set.Lnlx(obj,val)
            obj = setParameter(obj,'Lnlx',val,0,'real');
        end
        function res = get.Lnlx(obj)
            res = getParameter(obj,'Lnlx');
        end
        function obj = set.Ldvt0(obj,val)
            obj = setParameter(obj,'Ldvt0',val,0,'real');
        end
        function res = get.Ldvt0(obj)
            res = getParameter(obj,'Ldvt0');
        end
        function obj = set.Ldvt1(obj,val)
            obj = setParameter(obj,'Ldvt1',val,0,'real');
        end
        function res = get.Ldvt1(obj)
            res = getParameter(obj,'Ldvt1');
        end
        function obj = set.Ldvt2(obj,val)
            obj = setParameter(obj,'Ldvt2',val,0,'real');
        end
        function res = get.Ldvt2(obj)
            res = getParameter(obj,'Ldvt2');
        end
        function obj = set.Ldvt0w(obj,val)
            obj = setParameter(obj,'Ldvt0w',val,0,'real');
        end
        function res = get.Ldvt0w(obj)
            res = getParameter(obj,'Ldvt0w');
        end
        function obj = set.Ldvt1w(obj,val)
            obj = setParameter(obj,'Ldvt1w',val,0,'real');
        end
        function res = get.Ldvt1w(obj)
            res = getParameter(obj,'Ldvt1w');
        end
        function obj = set.Ldvt2w(obj,val)
            obj = setParameter(obj,'Ldvt2w',val,0,'real');
        end
        function res = get.Ldvt2w(obj)
            res = getParameter(obj,'Ldvt2w');
        end
        function obj = set.Lu0(obj,val)
            obj = setParameter(obj,'Lu0',val,0,'real');
        end
        function res = get.Lu0(obj)
            res = getParameter(obj,'Lu0');
        end
        function obj = set.Lua(obj,val)
            obj = setParameter(obj,'Lua',val,0,'real');
        end
        function res = get.Lua(obj)
            res = getParameter(obj,'Lua');
        end
        function obj = set.Lub(obj,val)
            obj = setParameter(obj,'Lub',val,0,'real');
        end
        function res = get.Lub(obj)
            res = getParameter(obj,'Lub');
        end
        function obj = set.Luc(obj,val)
            obj = setParameter(obj,'Luc',val,0,'real');
        end
        function res = get.Luc(obj)
            res = getParameter(obj,'Luc');
        end
        function obj = set.Lvsat(obj,val)
            obj = setParameter(obj,'Lvsat',val,0,'real');
        end
        function res = get.Lvsat(obj)
            res = getParameter(obj,'Lvsat');
        end
        function obj = set.La0(obj,val)
            obj = setParameter(obj,'La0',val,0,'real');
        end
        function res = get.La0(obj)
            res = getParameter(obj,'La0');
        end
        function obj = set.Lags(obj,val)
            obj = setParameter(obj,'Lags',val,0,'real');
        end
        function res = get.Lags(obj)
            res = getParameter(obj,'Lags');
        end
        function obj = set.Lb0(obj,val)
            obj = setParameter(obj,'Lb0',val,0,'real');
        end
        function res = get.Lb0(obj)
            res = getParameter(obj,'Lb0');
        end
        function obj = set.Lb1(obj,val)
            obj = setParameter(obj,'Lb1',val,0,'real');
        end
        function res = get.Lb1(obj)
            res = getParameter(obj,'Lb1');
        end
        function obj = set.Lketa(obj,val)
            obj = setParameter(obj,'Lketa',val,0,'real');
        end
        function res = get.Lketa(obj)
            res = getParameter(obj,'Lketa');
        end
        function obj = set.Lketas(obj,val)
            obj = setParameter(obj,'Lketas',val,0,'real');
        end
        function res = get.Lketas(obj)
            res = getParameter(obj,'Lketas');
        end
        function obj = set.La1(obj,val)
            obj = setParameter(obj,'La1',val,0,'real');
        end
        function res = get.La1(obj)
            res = getParameter(obj,'La1');
        end
        function obj = set.La2(obj,val)
            obj = setParameter(obj,'La2',val,0,'real');
        end
        function res = get.La2(obj)
            res = getParameter(obj,'La2');
        end
        function obj = set.Lrdsw(obj,val)
            obj = setParameter(obj,'Lrdsw',val,0,'real');
        end
        function res = get.Lrdsw(obj)
            res = getParameter(obj,'Lrdsw');
        end
        function obj = set.Lprwb(obj,val)
            obj = setParameter(obj,'Lprwb',val,0,'real');
        end
        function res = get.Lprwb(obj)
            res = getParameter(obj,'Lprwb');
        end
        function obj = set.Lprwg(obj,val)
            obj = setParameter(obj,'Lprwg',val,0,'real');
        end
        function res = get.Lprwg(obj)
            res = getParameter(obj,'Lprwg');
        end
        function obj = set.Lwr(obj,val)
            obj = setParameter(obj,'Lwr',val,0,'real');
        end
        function res = get.Lwr(obj)
            res = getParameter(obj,'Lwr');
        end
        function obj = set.Lnfactor(obj,val)
            obj = setParameter(obj,'Lnfactor',val,0,'real');
        end
        function res = get.Lnfactor(obj)
            res = getParameter(obj,'Lnfactor');
        end
        function obj = set.Ldwg(obj,val)
            obj = setParameter(obj,'Ldwg',val,0,'real');
        end
        function res = get.Ldwg(obj)
            res = getParameter(obj,'Ldwg');
        end
        function obj = set.Ldwb(obj,val)
            obj = setParameter(obj,'Ldwb',val,0,'real');
        end
        function res = get.Ldwb(obj)
            res = getParameter(obj,'Ldwb');
        end
        function obj = set.Lvoff(obj,val)
            obj = setParameter(obj,'Lvoff',val,0,'real');
        end
        function res = get.Lvoff(obj)
            res = getParameter(obj,'Lvoff');
        end
        function obj = set.Leta0(obj,val)
            obj = setParameter(obj,'Leta0',val,0,'real');
        end
        function res = get.Leta0(obj)
            res = getParameter(obj,'Leta0');
        end
        function obj = set.Letab(obj,val)
            obj = setParameter(obj,'Letab',val,0,'real');
        end
        function res = get.Letab(obj)
            res = getParameter(obj,'Letab');
        end
        function obj = set.Ldsub(obj,val)
            obj = setParameter(obj,'Ldsub',val,0,'real');
        end
        function res = get.Ldsub(obj)
            res = getParameter(obj,'Ldsub');
        end
        function obj = set.Lcit(obj,val)
            obj = setParameter(obj,'Lcit',val,0,'real');
        end
        function res = get.Lcit(obj)
            res = getParameter(obj,'Lcit');
        end
        function obj = set.Lcdsc(obj,val)
            obj = setParameter(obj,'Lcdsc',val,0,'real');
        end
        function res = get.Lcdsc(obj)
            res = getParameter(obj,'Lcdsc');
        end
        function obj = set.Lcdscb(obj,val)
            obj = setParameter(obj,'Lcdscb',val,0,'real');
        end
        function res = get.Lcdscb(obj)
            res = getParameter(obj,'Lcdscb');
        end
        function obj = set.Lcdscd(obj,val)
            obj = setParameter(obj,'Lcdscd',val,0,'real');
        end
        function res = get.Lcdscd(obj)
            res = getParameter(obj,'Lcdscd');
        end
        function obj = set.Lpclm(obj,val)
            obj = setParameter(obj,'Lpclm',val,0,'real');
        end
        function res = get.Lpclm(obj)
            res = getParameter(obj,'Lpclm');
        end
        function obj = set.Lpdiblc1(obj,val)
            obj = setParameter(obj,'Lpdiblc1',val,0,'real');
        end
        function res = get.Lpdiblc1(obj)
            res = getParameter(obj,'Lpdiblc1');
        end
        function obj = set.Lpdiblc2(obj,val)
            obj = setParameter(obj,'Lpdiblc2',val,0,'real');
        end
        function res = get.Lpdiblc2(obj)
            res = getParameter(obj,'Lpdiblc2');
        end
        function obj = set.Lpdiblcb(obj,val)
            obj = setParameter(obj,'Lpdiblcb',val,0,'real');
        end
        function res = get.Lpdiblcb(obj)
            res = getParameter(obj,'Lpdiblcb');
        end
        function obj = set.Ldrout(obj,val)
            obj = setParameter(obj,'Ldrout',val,0,'real');
        end
        function res = get.Ldrout(obj)
            res = getParameter(obj,'Ldrout');
        end
        function obj = set.Lpvag(obj,val)
            obj = setParameter(obj,'Lpvag',val,0,'real');
        end
        function res = get.Lpvag(obj)
            res = getParameter(obj,'Lpvag');
        end
        function obj = set.Ldelta(obj,val)
            obj = setParameter(obj,'Ldelta',val,0,'real');
        end
        function res = get.Ldelta(obj)
            res = getParameter(obj,'Ldelta');
        end
        function obj = set.Lalpha0(obj,val)
            obj = setParameter(obj,'Lalpha0',val,0,'real');
        end
        function res = get.Lalpha0(obj)
            res = getParameter(obj,'Lalpha0');
        end
        function obj = set.Lfbjtii(obj,val)
            obj = setParameter(obj,'Lfbjtii',val,0,'real');
        end
        function res = get.Lfbjtii(obj)
            res = getParameter(obj,'Lfbjtii');
        end
        function obj = set.Lbeta0(obj,val)
            obj = setParameter(obj,'Lbeta0',val,0,'real');
        end
        function res = get.Lbeta0(obj)
            res = getParameter(obj,'Lbeta0');
        end
        function obj = set.Lbeta1(obj,val)
            obj = setParameter(obj,'Lbeta1',val,0,'real');
        end
        function res = get.Lbeta1(obj)
            res = getParameter(obj,'Lbeta1');
        end
        function obj = set.Lbeta2(obj,val)
            obj = setParameter(obj,'Lbeta2',val,0,'real');
        end
        function res = get.Lbeta2(obj)
            res = getParameter(obj,'Lbeta2');
        end
        function obj = set.Lvdsatii0(obj,val)
            obj = setParameter(obj,'Lvdsatii0',val,0,'real');
        end
        function res = get.Lvdsatii0(obj)
            res = getParameter(obj,'Lvdsatii0');
        end
        function obj = set.Llii(obj,val)
            obj = setParameter(obj,'Llii',val,0,'real');
        end
        function res = get.Llii(obj)
            res = getParameter(obj,'Llii');
        end
        function obj = set.Lesatii(obj,val)
            obj = setParameter(obj,'Lesatii',val,0,'real');
        end
        function res = get.Lesatii(obj)
            res = getParameter(obj,'Lesatii');
        end
        function obj = set.Lsii0(obj,val)
            obj = setParameter(obj,'Lsii0',val,0,'real');
        end
        function res = get.Lsii0(obj)
            res = getParameter(obj,'Lsii0');
        end
        function obj = set.Lsii1(obj,val)
            obj = setParameter(obj,'Lsii1',val,0,'real');
        end
        function res = get.Lsii1(obj)
            res = getParameter(obj,'Lsii1');
        end
        function obj = set.Lsii2(obj,val)
            obj = setParameter(obj,'Lsii2',val,0,'real');
        end
        function res = get.Lsii2(obj)
            res = getParameter(obj,'Lsii2');
        end
        function obj = set.Lsiid(obj,val)
            obj = setParameter(obj,'Lsiid',val,0,'real');
        end
        function res = get.Lsiid(obj)
            res = getParameter(obj,'Lsiid');
        end
        function obj = set.Lagidl(obj,val)
            obj = setParameter(obj,'Lagidl',val,0,'real');
        end
        function res = get.Lagidl(obj)
            res = getParameter(obj,'Lagidl');
        end
        function obj = set.Lbgidl(obj,val)
            obj = setParameter(obj,'Lbgidl',val,0,'real');
        end
        function res = get.Lbgidl(obj)
            res = getParameter(obj,'Lbgidl');
        end
        function obj = set.Lngidl(obj,val)
            obj = setParameter(obj,'Lngidl',val,0,'real');
        end
        function res = get.Lngidl(obj)
            res = getParameter(obj,'Lngidl');
        end
        function obj = set.Lntun(obj,val)
            obj = setParameter(obj,'Lntun',val,0,'real');
        end
        function res = get.Lntun(obj)
            res = getParameter(obj,'Lntun');
        end
        function obj = set.Lndiode(obj,val)
            obj = setParameter(obj,'Lndiode',val,0,'real');
        end
        function res = get.Lndiode(obj)
            res = getParameter(obj,'Lndiode');
        end
        function obj = set.Lnrecf0(obj,val)
            obj = setParameter(obj,'Lnrecf0',val,0,'real');
        end
        function res = get.Lnrecf0(obj)
            res = getParameter(obj,'Lnrecf0');
        end
        function obj = set.Lnrecr0(obj,val)
            obj = setParameter(obj,'Lnrecr0',val,0,'real');
        end
        function res = get.Lnrecr0(obj)
            res = getParameter(obj,'Lnrecr0');
        end
        function obj = set.Lisbjt(obj,val)
            obj = setParameter(obj,'Lisbjt',val,0,'real');
        end
        function res = get.Lisbjt(obj)
            res = getParameter(obj,'Lisbjt');
        end
        function obj = set.Lisdif(obj,val)
            obj = setParameter(obj,'Lisdif',val,0,'real');
        end
        function res = get.Lisdif(obj)
            res = getParameter(obj,'Lisdif');
        end
        function obj = set.Listun(obj,val)
            obj = setParameter(obj,'Listun',val,0,'real');
        end
        function res = get.Listun(obj)
            res = getParameter(obj,'Listun');
        end
        function obj = set.Lvrec0(obj,val)
            obj = setParameter(obj,'Lvrec0',val,0,'real');
        end
        function res = get.Lvrec0(obj)
            res = getParameter(obj,'Lvrec0');
        end
        function obj = set.Lvtun0(obj,val)
            obj = setParameter(obj,'Lvtun0',val,0,'real');
        end
        function res = get.Lvtun0(obj)
            res = getParameter(obj,'Lvtun0');
        end
        function obj = set.Lnbjt(obj,val)
            obj = setParameter(obj,'Lnbjt',val,0,'real');
        end
        function res = get.Lnbjt(obj)
            res = getParameter(obj,'Lnbjt');
        end
        function obj = set.Llbjt0(obj,val)
            obj = setParameter(obj,'Llbjt0',val,0,'real');
        end
        function res = get.Llbjt0(obj)
            res = getParameter(obj,'Llbjt0');
        end
        function obj = set.Lvabjt(obj,val)
            obj = setParameter(obj,'Lvabjt',val,0,'real');
        end
        function res = get.Lvabjt(obj)
            res = getParameter(obj,'Lvabjt');
        end
        function obj = set.Laely(obj,val)
            obj = setParameter(obj,'Laely',val,0,'real');
        end
        function res = get.Laely(obj)
            res = getParameter(obj,'Laely');
        end
        function obj = set.Lahli(obj,val)
            obj = setParameter(obj,'Lahli',val,0,'real');
        end
        function res = get.Lahli(obj)
            res = getParameter(obj,'Lahli');
        end
        function obj = set.Lvsdfb(obj,val)
            obj = setParameter(obj,'Lvsdfb',val,0,'real');
        end
        function res = get.Lvsdfb(obj)
            res = getParameter(obj,'Lvsdfb');
        end
        function obj = set.Lvsdth(obj,val)
            obj = setParameter(obj,'Lvsdth',val,0,'real');
        end
        function res = get.Lvsdth(obj)
            res = getParameter(obj,'Lvsdth');
        end
        function obj = set.Ldelvt(obj,val)
            obj = setParameter(obj,'Ldelvt',val,0,'real');
        end
        function res = get.Ldelvt(obj)
            res = getParameter(obj,'Ldelvt');
        end
        function obj = set.Lacde(obj,val)
            obj = setParameter(obj,'Lacde',val,0,'real');
        end
        function res = get.Lacde(obj)
            res = getParameter(obj,'Lacde');
        end
        function obj = set.Lmoin(obj,val)
            obj = setParameter(obj,'Lmoin',val,0,'real');
        end
        function res = get.Lmoin(obj)
            res = getParameter(obj,'Lmoin');
        end
        function obj = set.Wnch(obj,val)
            obj = setParameter(obj,'Wnch',val,0,'real');
        end
        function res = get.Wnch(obj)
            res = getParameter(obj,'Wnch');
        end
        function obj = set.Wnsub(obj,val)
            obj = setParameter(obj,'Wnsub',val,0,'real');
        end
        function res = get.Wnsub(obj)
            res = getParameter(obj,'Wnsub');
        end
        function obj = set.Wngate(obj,val)
            obj = setParameter(obj,'Wngate',val,0,'real');
        end
        function res = get.Wngate(obj)
            res = getParameter(obj,'Wngate');
        end
        function obj = set.Wvth0(obj,val)
            obj = setParameter(obj,'Wvth0',val,0,'real');
        end
        function res = get.Wvth0(obj)
            res = getParameter(obj,'Wvth0');
        end
        function obj = set.Wk1(obj,val)
            obj = setParameter(obj,'Wk1',val,0,'real');
        end
        function res = get.Wk1(obj)
            res = getParameter(obj,'Wk1');
        end
        function obj = set.Wk1w1(obj,val)
            obj = setParameter(obj,'Wk1w1',val,0,'real');
        end
        function res = get.Wk1w1(obj)
            res = getParameter(obj,'Wk1w1');
        end
        function obj = set.Wk1w2(obj,val)
            obj = setParameter(obj,'Wk1w2',val,0,'real');
        end
        function res = get.Wk1w2(obj)
            res = getParameter(obj,'Wk1w2');
        end
        function obj = set.Wk2(obj,val)
            obj = setParameter(obj,'Wk2',val,0,'real');
        end
        function res = get.Wk2(obj)
            res = getParameter(obj,'Wk2');
        end
        function obj = set.Wk3(obj,val)
            obj = setParameter(obj,'Wk3',val,0,'real');
        end
        function res = get.Wk3(obj)
            res = getParameter(obj,'Wk3');
        end
        function obj = set.Wk3b(obj,val)
            obj = setParameter(obj,'Wk3b',val,0,'real');
        end
        function res = get.Wk3b(obj)
            res = getParameter(obj,'Wk3b');
        end
        function obj = set.Wkb1(obj,val)
            obj = setParameter(obj,'Wkb1',val,0,'real');
        end
        function res = get.Wkb1(obj)
            res = getParameter(obj,'Wkb1');
        end
        function obj = set.Ww0(obj,val)
            obj = setParameter(obj,'Ww0',val,0,'real');
        end
        function res = get.Ww0(obj)
            res = getParameter(obj,'Ww0');
        end
        function obj = set.Wnlx(obj,val)
            obj = setParameter(obj,'Wnlx',val,0,'real');
        end
        function res = get.Wnlx(obj)
            res = getParameter(obj,'Wnlx');
        end
        function obj = set.Wdvt0(obj,val)
            obj = setParameter(obj,'Wdvt0',val,0,'real');
        end
        function res = get.Wdvt0(obj)
            res = getParameter(obj,'Wdvt0');
        end
        function obj = set.Wdvt1(obj,val)
            obj = setParameter(obj,'Wdvt1',val,0,'real');
        end
        function res = get.Wdvt1(obj)
            res = getParameter(obj,'Wdvt1');
        end
        function obj = set.Wdvt2(obj,val)
            obj = setParameter(obj,'Wdvt2',val,0,'real');
        end
        function res = get.Wdvt2(obj)
            res = getParameter(obj,'Wdvt2');
        end
        function obj = set.Wdvt0w(obj,val)
            obj = setParameter(obj,'Wdvt0w',val,0,'real');
        end
        function res = get.Wdvt0w(obj)
            res = getParameter(obj,'Wdvt0w');
        end
        function obj = set.Wdvt1w(obj,val)
            obj = setParameter(obj,'Wdvt1w',val,0,'real');
        end
        function res = get.Wdvt1w(obj)
            res = getParameter(obj,'Wdvt1w');
        end
        function obj = set.Wdvt2w(obj,val)
            obj = setParameter(obj,'Wdvt2w',val,0,'real');
        end
        function res = get.Wdvt2w(obj)
            res = getParameter(obj,'Wdvt2w');
        end
        function obj = set.Wu0(obj,val)
            obj = setParameter(obj,'Wu0',val,0,'real');
        end
        function res = get.Wu0(obj)
            res = getParameter(obj,'Wu0');
        end
        function obj = set.Wua(obj,val)
            obj = setParameter(obj,'Wua',val,0,'real');
        end
        function res = get.Wua(obj)
            res = getParameter(obj,'Wua');
        end
        function obj = set.Wub(obj,val)
            obj = setParameter(obj,'Wub',val,0,'real');
        end
        function res = get.Wub(obj)
            res = getParameter(obj,'Wub');
        end
        function obj = set.Wuc(obj,val)
            obj = setParameter(obj,'Wuc',val,0,'real');
        end
        function res = get.Wuc(obj)
            res = getParameter(obj,'Wuc');
        end
        function obj = set.Wvsat(obj,val)
            obj = setParameter(obj,'Wvsat',val,0,'real');
        end
        function res = get.Wvsat(obj)
            res = getParameter(obj,'Wvsat');
        end
        function obj = set.Wa0(obj,val)
            obj = setParameter(obj,'Wa0',val,0,'real');
        end
        function res = get.Wa0(obj)
            res = getParameter(obj,'Wa0');
        end
        function obj = set.Wags(obj,val)
            obj = setParameter(obj,'Wags',val,0,'real');
        end
        function res = get.Wags(obj)
            res = getParameter(obj,'Wags');
        end
        function obj = set.Wb0(obj,val)
            obj = setParameter(obj,'Wb0',val,0,'real');
        end
        function res = get.Wb0(obj)
            res = getParameter(obj,'Wb0');
        end
        function obj = set.Wb1(obj,val)
            obj = setParameter(obj,'Wb1',val,0,'real');
        end
        function res = get.Wb1(obj)
            res = getParameter(obj,'Wb1');
        end
        function obj = set.Wketa(obj,val)
            obj = setParameter(obj,'Wketa',val,0,'real');
        end
        function res = get.Wketa(obj)
            res = getParameter(obj,'Wketa');
        end
        function obj = set.Wketas(obj,val)
            obj = setParameter(obj,'Wketas',val,0,'real');
        end
        function res = get.Wketas(obj)
            res = getParameter(obj,'Wketas');
        end
        function obj = set.Wa1(obj,val)
            obj = setParameter(obj,'Wa1',val,0,'real');
        end
        function res = get.Wa1(obj)
            res = getParameter(obj,'Wa1');
        end
        function obj = set.Wa2(obj,val)
            obj = setParameter(obj,'Wa2',val,0,'real');
        end
        function res = get.Wa2(obj)
            res = getParameter(obj,'Wa2');
        end
        function obj = set.Wrdsw(obj,val)
            obj = setParameter(obj,'Wrdsw',val,0,'real');
        end
        function res = get.Wrdsw(obj)
            res = getParameter(obj,'Wrdsw');
        end
        function obj = set.Wprwb(obj,val)
            obj = setParameter(obj,'Wprwb',val,0,'real');
        end
        function res = get.Wprwb(obj)
            res = getParameter(obj,'Wprwb');
        end
        function obj = set.Wprwg(obj,val)
            obj = setParameter(obj,'Wprwg',val,0,'real');
        end
        function res = get.Wprwg(obj)
            res = getParameter(obj,'Wprwg');
        end
        function obj = set.Wwr(obj,val)
            obj = setParameter(obj,'Wwr',val,0,'real');
        end
        function res = get.Wwr(obj)
            res = getParameter(obj,'Wwr');
        end
        function obj = set.Wnfactor(obj,val)
            obj = setParameter(obj,'Wnfactor',val,0,'real');
        end
        function res = get.Wnfactor(obj)
            res = getParameter(obj,'Wnfactor');
        end
        function obj = set.Wdwg(obj,val)
            obj = setParameter(obj,'Wdwg',val,0,'real');
        end
        function res = get.Wdwg(obj)
            res = getParameter(obj,'Wdwg');
        end
        function obj = set.Wdwb(obj,val)
            obj = setParameter(obj,'Wdwb',val,0,'real');
        end
        function res = get.Wdwb(obj)
            res = getParameter(obj,'Wdwb');
        end
        function obj = set.Wvoff(obj,val)
            obj = setParameter(obj,'Wvoff',val,0,'real');
        end
        function res = get.Wvoff(obj)
            res = getParameter(obj,'Wvoff');
        end
        function obj = set.Weta0(obj,val)
            obj = setParameter(obj,'Weta0',val,0,'real');
        end
        function res = get.Weta0(obj)
            res = getParameter(obj,'Weta0');
        end
        function obj = set.Wetab(obj,val)
            obj = setParameter(obj,'Wetab',val,0,'real');
        end
        function res = get.Wetab(obj)
            res = getParameter(obj,'Wetab');
        end
        function obj = set.Wdsub(obj,val)
            obj = setParameter(obj,'Wdsub',val,0,'real');
        end
        function res = get.Wdsub(obj)
            res = getParameter(obj,'Wdsub');
        end
        function obj = set.Wcit(obj,val)
            obj = setParameter(obj,'Wcit',val,0,'real');
        end
        function res = get.Wcit(obj)
            res = getParameter(obj,'Wcit');
        end
        function obj = set.Wcdsc(obj,val)
            obj = setParameter(obj,'Wcdsc',val,0,'real');
        end
        function res = get.Wcdsc(obj)
            res = getParameter(obj,'Wcdsc');
        end
        function obj = set.Wcdscb(obj,val)
            obj = setParameter(obj,'Wcdscb',val,0,'real');
        end
        function res = get.Wcdscb(obj)
            res = getParameter(obj,'Wcdscb');
        end
        function obj = set.Wcdscd(obj,val)
            obj = setParameter(obj,'Wcdscd',val,0,'real');
        end
        function res = get.Wcdscd(obj)
            res = getParameter(obj,'Wcdscd');
        end
        function obj = set.Wpclm(obj,val)
            obj = setParameter(obj,'Wpclm',val,0,'real');
        end
        function res = get.Wpclm(obj)
            res = getParameter(obj,'Wpclm');
        end
        function obj = set.Wpdiblc1(obj,val)
            obj = setParameter(obj,'Wpdiblc1',val,0,'real');
        end
        function res = get.Wpdiblc1(obj)
            res = getParameter(obj,'Wpdiblc1');
        end
        function obj = set.Wpdiblc2(obj,val)
            obj = setParameter(obj,'Wpdiblc2',val,0,'real');
        end
        function res = get.Wpdiblc2(obj)
            res = getParameter(obj,'Wpdiblc2');
        end
        function obj = set.Wpdiblcb(obj,val)
            obj = setParameter(obj,'Wpdiblcb',val,0,'real');
        end
        function res = get.Wpdiblcb(obj)
            res = getParameter(obj,'Wpdiblcb');
        end
        function obj = set.Wdrout(obj,val)
            obj = setParameter(obj,'Wdrout',val,0,'real');
        end
        function res = get.Wdrout(obj)
            res = getParameter(obj,'Wdrout');
        end
        function obj = set.Wpvag(obj,val)
            obj = setParameter(obj,'Wpvag',val,0,'real');
        end
        function res = get.Wpvag(obj)
            res = getParameter(obj,'Wpvag');
        end
        function obj = set.Wdelta(obj,val)
            obj = setParameter(obj,'Wdelta',val,0,'real');
        end
        function res = get.Wdelta(obj)
            res = getParameter(obj,'Wdelta');
        end
        function obj = set.Walpha0(obj,val)
            obj = setParameter(obj,'Walpha0',val,0,'real');
        end
        function res = get.Walpha0(obj)
            res = getParameter(obj,'Walpha0');
        end
        function obj = set.Wfbjtii(obj,val)
            obj = setParameter(obj,'Wfbjtii',val,0,'real');
        end
        function res = get.Wfbjtii(obj)
            res = getParameter(obj,'Wfbjtii');
        end
        function obj = set.Wbeta0(obj,val)
            obj = setParameter(obj,'Wbeta0',val,0,'real');
        end
        function res = get.Wbeta0(obj)
            res = getParameter(obj,'Wbeta0');
        end
        function obj = set.Wbeta1(obj,val)
            obj = setParameter(obj,'Wbeta1',val,0,'real');
        end
        function res = get.Wbeta1(obj)
            res = getParameter(obj,'Wbeta1');
        end
        function obj = set.Wbeta2(obj,val)
            obj = setParameter(obj,'Wbeta2',val,0,'real');
        end
        function res = get.Wbeta2(obj)
            res = getParameter(obj,'Wbeta2');
        end
        function obj = set.Wvdsatii0(obj,val)
            obj = setParameter(obj,'Wvdsatii0',val,0,'real');
        end
        function res = get.Wvdsatii0(obj)
            res = getParameter(obj,'Wvdsatii0');
        end
        function obj = set.Wlii(obj,val)
            obj = setParameter(obj,'Wlii',val,0,'real');
        end
        function res = get.Wlii(obj)
            res = getParameter(obj,'Wlii');
        end
        function obj = set.Wesatii(obj,val)
            obj = setParameter(obj,'Wesatii',val,0,'real');
        end
        function res = get.Wesatii(obj)
            res = getParameter(obj,'Wesatii');
        end
        function obj = set.Wsii0(obj,val)
            obj = setParameter(obj,'Wsii0',val,0,'real');
        end
        function res = get.Wsii0(obj)
            res = getParameter(obj,'Wsii0');
        end
        function obj = set.Wsii1(obj,val)
            obj = setParameter(obj,'Wsii1',val,0,'real');
        end
        function res = get.Wsii1(obj)
            res = getParameter(obj,'Wsii1');
        end
        function obj = set.Wsii2(obj,val)
            obj = setParameter(obj,'Wsii2',val,0,'real');
        end
        function res = get.Wsii2(obj)
            res = getParameter(obj,'Wsii2');
        end
        function obj = set.Wsiid(obj,val)
            obj = setParameter(obj,'Wsiid',val,0,'real');
        end
        function res = get.Wsiid(obj)
            res = getParameter(obj,'Wsiid');
        end
        function obj = set.Wagidl(obj,val)
            obj = setParameter(obj,'Wagidl',val,0,'real');
        end
        function res = get.Wagidl(obj)
            res = getParameter(obj,'Wagidl');
        end
        function obj = set.Wbgidl(obj,val)
            obj = setParameter(obj,'Wbgidl',val,0,'real');
        end
        function res = get.Wbgidl(obj)
            res = getParameter(obj,'Wbgidl');
        end
        function obj = set.Wngidl(obj,val)
            obj = setParameter(obj,'Wngidl',val,0,'real');
        end
        function res = get.Wngidl(obj)
            res = getParameter(obj,'Wngidl');
        end
        function obj = set.Wntun(obj,val)
            obj = setParameter(obj,'Wntun',val,0,'real');
        end
        function res = get.Wntun(obj)
            res = getParameter(obj,'Wntun');
        end
        function obj = set.Wndiode(obj,val)
            obj = setParameter(obj,'Wndiode',val,0,'real');
        end
        function res = get.Wndiode(obj)
            res = getParameter(obj,'Wndiode');
        end
        function obj = set.Wnrecf0(obj,val)
            obj = setParameter(obj,'Wnrecf0',val,0,'real');
        end
        function res = get.Wnrecf0(obj)
            res = getParameter(obj,'Wnrecf0');
        end
        function obj = set.Wnrecr0(obj,val)
            obj = setParameter(obj,'Wnrecr0',val,0,'real');
        end
        function res = get.Wnrecr0(obj)
            res = getParameter(obj,'Wnrecr0');
        end
        function obj = set.Wisbjt(obj,val)
            obj = setParameter(obj,'Wisbjt',val,0,'real');
        end
        function res = get.Wisbjt(obj)
            res = getParameter(obj,'Wisbjt');
        end
        function obj = set.Wisdif(obj,val)
            obj = setParameter(obj,'Wisdif',val,0,'real');
        end
        function res = get.Wisdif(obj)
            res = getParameter(obj,'Wisdif');
        end
        function obj = set.Wistun(obj,val)
            obj = setParameter(obj,'Wistun',val,0,'real');
        end
        function res = get.Wistun(obj)
            res = getParameter(obj,'Wistun');
        end
        function obj = set.Wvrec0(obj,val)
            obj = setParameter(obj,'Wvrec0',val,0,'real');
        end
        function res = get.Wvrec0(obj)
            res = getParameter(obj,'Wvrec0');
        end
        function obj = set.Wvtun0(obj,val)
            obj = setParameter(obj,'Wvtun0',val,0,'real');
        end
        function res = get.Wvtun0(obj)
            res = getParameter(obj,'Wvtun0');
        end
        function obj = set.Wnbjt(obj,val)
            obj = setParameter(obj,'Wnbjt',val,0,'real');
        end
        function res = get.Wnbjt(obj)
            res = getParameter(obj,'Wnbjt');
        end
        function obj = set.Wlbjt0(obj,val)
            obj = setParameter(obj,'Wlbjt0',val,0,'real');
        end
        function res = get.Wlbjt0(obj)
            res = getParameter(obj,'Wlbjt0');
        end
        function obj = set.Wvabjt(obj,val)
            obj = setParameter(obj,'Wvabjt',val,0,'real');
        end
        function res = get.Wvabjt(obj)
            res = getParameter(obj,'Wvabjt');
        end
        function obj = set.Waely(obj,val)
            obj = setParameter(obj,'Waely',val,0,'real');
        end
        function res = get.Waely(obj)
            res = getParameter(obj,'Waely');
        end
        function obj = set.Wahli(obj,val)
            obj = setParameter(obj,'Wahli',val,0,'real');
        end
        function res = get.Wahli(obj)
            res = getParameter(obj,'Wahli');
        end
        function obj = set.Wvsdfb(obj,val)
            obj = setParameter(obj,'Wvsdfb',val,0,'real');
        end
        function res = get.Wvsdfb(obj)
            res = getParameter(obj,'Wvsdfb');
        end
        function obj = set.Wvsdth(obj,val)
            obj = setParameter(obj,'Wvsdth',val,0,'real');
        end
        function res = get.Wvsdth(obj)
            res = getParameter(obj,'Wvsdth');
        end
        function obj = set.Wdelvt(obj,val)
            obj = setParameter(obj,'Wdelvt',val,0,'real');
        end
        function res = get.Wdelvt(obj)
            res = getParameter(obj,'Wdelvt');
        end
        function obj = set.Wacde(obj,val)
            obj = setParameter(obj,'Wacde',val,0,'real');
        end
        function res = get.Wacde(obj)
            res = getParameter(obj,'Wacde');
        end
        function obj = set.Wmoin(obj,val)
            obj = setParameter(obj,'Wmoin',val,0,'real');
        end
        function res = get.Wmoin(obj)
            res = getParameter(obj,'Wmoin');
        end
        function obj = set.Pnch(obj,val)
            obj = setParameter(obj,'Pnch',val,0,'real');
        end
        function res = get.Pnch(obj)
            res = getParameter(obj,'Pnch');
        end
        function obj = set.Pnsub(obj,val)
            obj = setParameter(obj,'Pnsub',val,0,'real');
        end
        function res = get.Pnsub(obj)
            res = getParameter(obj,'Pnsub');
        end
        function obj = set.Pngate(obj,val)
            obj = setParameter(obj,'Pngate',val,0,'real');
        end
        function res = get.Pngate(obj)
            res = getParameter(obj,'Pngate');
        end
        function obj = set.Pvth0(obj,val)
            obj = setParameter(obj,'Pvth0',val,0,'real');
        end
        function res = get.Pvth0(obj)
            res = getParameter(obj,'Pvth0');
        end
        function obj = set.Pk1(obj,val)
            obj = setParameter(obj,'Pk1',val,0,'real');
        end
        function res = get.Pk1(obj)
            res = getParameter(obj,'Pk1');
        end
        function obj = set.Pk1w1(obj,val)
            obj = setParameter(obj,'Pk1w1',val,0,'real');
        end
        function res = get.Pk1w1(obj)
            res = getParameter(obj,'Pk1w1');
        end
        function obj = set.Pk1w2(obj,val)
            obj = setParameter(obj,'Pk1w2',val,0,'real');
        end
        function res = get.Pk1w2(obj)
            res = getParameter(obj,'Pk1w2');
        end
        function obj = set.Pk2(obj,val)
            obj = setParameter(obj,'Pk2',val,0,'real');
        end
        function res = get.Pk2(obj)
            res = getParameter(obj,'Pk2');
        end
        function obj = set.Pk3(obj,val)
            obj = setParameter(obj,'Pk3',val,0,'real');
        end
        function res = get.Pk3(obj)
            res = getParameter(obj,'Pk3');
        end
        function obj = set.Pk3b(obj,val)
            obj = setParameter(obj,'Pk3b',val,0,'real');
        end
        function res = get.Pk3b(obj)
            res = getParameter(obj,'Pk3b');
        end
        function obj = set.Pkb1(obj,val)
            obj = setParameter(obj,'Pkb1',val,0,'real');
        end
        function res = get.Pkb1(obj)
            res = getParameter(obj,'Pkb1');
        end
        function obj = set.Pw0(obj,val)
            obj = setParameter(obj,'Pw0',val,0,'real');
        end
        function res = get.Pw0(obj)
            res = getParameter(obj,'Pw0');
        end
        function obj = set.Pnlx(obj,val)
            obj = setParameter(obj,'Pnlx',val,0,'real');
        end
        function res = get.Pnlx(obj)
            res = getParameter(obj,'Pnlx');
        end
        function obj = set.Pdvt0(obj,val)
            obj = setParameter(obj,'Pdvt0',val,0,'real');
        end
        function res = get.Pdvt0(obj)
            res = getParameter(obj,'Pdvt0');
        end
        function obj = set.Pdvt1(obj,val)
            obj = setParameter(obj,'Pdvt1',val,0,'real');
        end
        function res = get.Pdvt1(obj)
            res = getParameter(obj,'Pdvt1');
        end
        function obj = set.Pdvt2(obj,val)
            obj = setParameter(obj,'Pdvt2',val,0,'real');
        end
        function res = get.Pdvt2(obj)
            res = getParameter(obj,'Pdvt2');
        end
        function obj = set.Pdvt0w(obj,val)
            obj = setParameter(obj,'Pdvt0w',val,0,'real');
        end
        function res = get.Pdvt0w(obj)
            res = getParameter(obj,'Pdvt0w');
        end
        function obj = set.Pdvt1w(obj,val)
            obj = setParameter(obj,'Pdvt1w',val,0,'real');
        end
        function res = get.Pdvt1w(obj)
            res = getParameter(obj,'Pdvt1w');
        end
        function obj = set.Pdvt2w(obj,val)
            obj = setParameter(obj,'Pdvt2w',val,0,'real');
        end
        function res = get.Pdvt2w(obj)
            res = getParameter(obj,'Pdvt2w');
        end
        function obj = set.Pu0(obj,val)
            obj = setParameter(obj,'Pu0',val,0,'real');
        end
        function res = get.Pu0(obj)
            res = getParameter(obj,'Pu0');
        end
        function obj = set.Pua(obj,val)
            obj = setParameter(obj,'Pua',val,0,'real');
        end
        function res = get.Pua(obj)
            res = getParameter(obj,'Pua');
        end
        function obj = set.Pub(obj,val)
            obj = setParameter(obj,'Pub',val,0,'real');
        end
        function res = get.Pub(obj)
            res = getParameter(obj,'Pub');
        end
        function obj = set.Puc(obj,val)
            obj = setParameter(obj,'Puc',val,0,'real');
        end
        function res = get.Puc(obj)
            res = getParameter(obj,'Puc');
        end
        function obj = set.Pvsat(obj,val)
            obj = setParameter(obj,'Pvsat',val,0,'real');
        end
        function res = get.Pvsat(obj)
            res = getParameter(obj,'Pvsat');
        end
        function obj = set.Pa0(obj,val)
            obj = setParameter(obj,'Pa0',val,0,'real');
        end
        function res = get.Pa0(obj)
            res = getParameter(obj,'Pa0');
        end
        function obj = set.Pags(obj,val)
            obj = setParameter(obj,'Pags',val,0,'real');
        end
        function res = get.Pags(obj)
            res = getParameter(obj,'Pags');
        end
        function obj = set.Pb0(obj,val)
            obj = setParameter(obj,'Pb0',val,0,'real');
        end
        function res = get.Pb0(obj)
            res = getParameter(obj,'Pb0');
        end
        function obj = set.Pb1(obj,val)
            obj = setParameter(obj,'Pb1',val,0,'real');
        end
        function res = get.Pb1(obj)
            res = getParameter(obj,'Pb1');
        end
        function obj = set.Pketa(obj,val)
            obj = setParameter(obj,'Pketa',val,0,'real');
        end
        function res = get.Pketa(obj)
            res = getParameter(obj,'Pketa');
        end
        function obj = set.Pketas(obj,val)
            obj = setParameter(obj,'Pketas',val,0,'real');
        end
        function res = get.Pketas(obj)
            res = getParameter(obj,'Pketas');
        end
        function obj = set.Pa1(obj,val)
            obj = setParameter(obj,'Pa1',val,0,'real');
        end
        function res = get.Pa1(obj)
            res = getParameter(obj,'Pa1');
        end
        function obj = set.Pa2(obj,val)
            obj = setParameter(obj,'Pa2',val,0,'real');
        end
        function res = get.Pa2(obj)
            res = getParameter(obj,'Pa2');
        end
        function obj = set.Prdsw(obj,val)
            obj = setParameter(obj,'Prdsw',val,0,'real');
        end
        function res = get.Prdsw(obj)
            res = getParameter(obj,'Prdsw');
        end
        function obj = set.Pprwb(obj,val)
            obj = setParameter(obj,'Pprwb',val,0,'real');
        end
        function res = get.Pprwb(obj)
            res = getParameter(obj,'Pprwb');
        end
        function obj = set.Pprwg(obj,val)
            obj = setParameter(obj,'Pprwg',val,0,'real');
        end
        function res = get.Pprwg(obj)
            res = getParameter(obj,'Pprwg');
        end
        function obj = set.Pwr(obj,val)
            obj = setParameter(obj,'Pwr',val,0,'real');
        end
        function res = get.Pwr(obj)
            res = getParameter(obj,'Pwr');
        end
        function obj = set.Pnfactor(obj,val)
            obj = setParameter(obj,'Pnfactor',val,0,'real');
        end
        function res = get.Pnfactor(obj)
            res = getParameter(obj,'Pnfactor');
        end
        function obj = set.Pdwg(obj,val)
            obj = setParameter(obj,'Pdwg',val,0,'real');
        end
        function res = get.Pdwg(obj)
            res = getParameter(obj,'Pdwg');
        end
        function obj = set.Pdwb(obj,val)
            obj = setParameter(obj,'Pdwb',val,0,'real');
        end
        function res = get.Pdwb(obj)
            res = getParameter(obj,'Pdwb');
        end
        function obj = set.Pvoff(obj,val)
            obj = setParameter(obj,'Pvoff',val,0,'real');
        end
        function res = get.Pvoff(obj)
            res = getParameter(obj,'Pvoff');
        end
        function obj = set.Peta0(obj,val)
            obj = setParameter(obj,'Peta0',val,0,'real');
        end
        function res = get.Peta0(obj)
            res = getParameter(obj,'Peta0');
        end
        function obj = set.Petab(obj,val)
            obj = setParameter(obj,'Petab',val,0,'real');
        end
        function res = get.Petab(obj)
            res = getParameter(obj,'Petab');
        end
        function obj = set.Pdsub(obj,val)
            obj = setParameter(obj,'Pdsub',val,0,'real');
        end
        function res = get.Pdsub(obj)
            res = getParameter(obj,'Pdsub');
        end
        function obj = set.Pcit(obj,val)
            obj = setParameter(obj,'Pcit',val,0,'real');
        end
        function res = get.Pcit(obj)
            res = getParameter(obj,'Pcit');
        end
        function obj = set.Pcdsc(obj,val)
            obj = setParameter(obj,'Pcdsc',val,0,'real');
        end
        function res = get.Pcdsc(obj)
            res = getParameter(obj,'Pcdsc');
        end
        function obj = set.Pcdscb(obj,val)
            obj = setParameter(obj,'Pcdscb',val,0,'real');
        end
        function res = get.Pcdscb(obj)
            res = getParameter(obj,'Pcdscb');
        end
        function obj = set.Pcdscd(obj,val)
            obj = setParameter(obj,'Pcdscd',val,0,'real');
        end
        function res = get.Pcdscd(obj)
            res = getParameter(obj,'Pcdscd');
        end
        function obj = set.Ppclm(obj,val)
            obj = setParameter(obj,'Ppclm',val,0,'real');
        end
        function res = get.Ppclm(obj)
            res = getParameter(obj,'Ppclm');
        end
        function obj = set.Ppdiblc1(obj,val)
            obj = setParameter(obj,'Ppdiblc1',val,0,'real');
        end
        function res = get.Ppdiblc1(obj)
            res = getParameter(obj,'Ppdiblc1');
        end
        function obj = set.Ppdiblc2(obj,val)
            obj = setParameter(obj,'Ppdiblc2',val,0,'real');
        end
        function res = get.Ppdiblc2(obj)
            res = getParameter(obj,'Ppdiblc2');
        end
        function obj = set.Ppdiblcb(obj,val)
            obj = setParameter(obj,'Ppdiblcb',val,0,'real');
        end
        function res = get.Ppdiblcb(obj)
            res = getParameter(obj,'Ppdiblcb');
        end
        function obj = set.Pdrout(obj,val)
            obj = setParameter(obj,'Pdrout',val,0,'real');
        end
        function res = get.Pdrout(obj)
            res = getParameter(obj,'Pdrout');
        end
        function obj = set.Ppvag(obj,val)
            obj = setParameter(obj,'Ppvag',val,0,'real');
        end
        function res = get.Ppvag(obj)
            res = getParameter(obj,'Ppvag');
        end
        function obj = set.Pdelta(obj,val)
            obj = setParameter(obj,'Pdelta',val,0,'real');
        end
        function res = get.Pdelta(obj)
            res = getParameter(obj,'Pdelta');
        end
        function obj = set.Palpha0(obj,val)
            obj = setParameter(obj,'Palpha0',val,0,'real');
        end
        function res = get.Palpha0(obj)
            res = getParameter(obj,'Palpha0');
        end
        function obj = set.Pfbjtii(obj,val)
            obj = setParameter(obj,'Pfbjtii',val,0,'real');
        end
        function res = get.Pfbjtii(obj)
            res = getParameter(obj,'Pfbjtii');
        end
        function obj = set.Pbeta0(obj,val)
            obj = setParameter(obj,'Pbeta0',val,0,'real');
        end
        function res = get.Pbeta0(obj)
            res = getParameter(obj,'Pbeta0');
        end
        function obj = set.Pbeta1(obj,val)
            obj = setParameter(obj,'Pbeta1',val,0,'real');
        end
        function res = get.Pbeta1(obj)
            res = getParameter(obj,'Pbeta1');
        end
        function obj = set.Pbeta2(obj,val)
            obj = setParameter(obj,'Pbeta2',val,0,'real');
        end
        function res = get.Pbeta2(obj)
            res = getParameter(obj,'Pbeta2');
        end
        function obj = set.Pvdsatii0(obj,val)
            obj = setParameter(obj,'Pvdsatii0',val,0,'real');
        end
        function res = get.Pvdsatii0(obj)
            res = getParameter(obj,'Pvdsatii0');
        end
        function obj = set.Plii(obj,val)
            obj = setParameter(obj,'Plii',val,0,'real');
        end
        function res = get.Plii(obj)
            res = getParameter(obj,'Plii');
        end
        function obj = set.Pesatii(obj,val)
            obj = setParameter(obj,'Pesatii',val,0,'real');
        end
        function res = get.Pesatii(obj)
            res = getParameter(obj,'Pesatii');
        end
        function obj = set.Psii0(obj,val)
            obj = setParameter(obj,'Psii0',val,0,'real');
        end
        function res = get.Psii0(obj)
            res = getParameter(obj,'Psii0');
        end
        function obj = set.Psii1(obj,val)
            obj = setParameter(obj,'Psii1',val,0,'real');
        end
        function res = get.Psii1(obj)
            res = getParameter(obj,'Psii1');
        end
        function obj = set.Psii2(obj,val)
            obj = setParameter(obj,'Psii2',val,0,'real');
        end
        function res = get.Psii2(obj)
            res = getParameter(obj,'Psii2');
        end
        function obj = set.Psiid(obj,val)
            obj = setParameter(obj,'Psiid',val,0,'real');
        end
        function res = get.Psiid(obj)
            res = getParameter(obj,'Psiid');
        end
        function obj = set.Pagidl(obj,val)
            obj = setParameter(obj,'Pagidl',val,0,'real');
        end
        function res = get.Pagidl(obj)
            res = getParameter(obj,'Pagidl');
        end
        function obj = set.Pbgidl(obj,val)
            obj = setParameter(obj,'Pbgidl',val,0,'real');
        end
        function res = get.Pbgidl(obj)
            res = getParameter(obj,'Pbgidl');
        end
        function obj = set.Pngidl(obj,val)
            obj = setParameter(obj,'Pngidl',val,0,'real');
        end
        function res = get.Pngidl(obj)
            res = getParameter(obj,'Pngidl');
        end
        function obj = set.Pntun(obj,val)
            obj = setParameter(obj,'Pntun',val,0,'real');
        end
        function res = get.Pntun(obj)
            res = getParameter(obj,'Pntun');
        end
        function obj = set.Pndiode(obj,val)
            obj = setParameter(obj,'Pndiode',val,0,'real');
        end
        function res = get.Pndiode(obj)
            res = getParameter(obj,'Pndiode');
        end
        function obj = set.Pnrecf0(obj,val)
            obj = setParameter(obj,'Pnrecf0',val,0,'real');
        end
        function res = get.Pnrecf0(obj)
            res = getParameter(obj,'Pnrecf0');
        end
        function obj = set.Pnrecr0(obj,val)
            obj = setParameter(obj,'Pnrecr0',val,0,'real');
        end
        function res = get.Pnrecr0(obj)
            res = getParameter(obj,'Pnrecr0');
        end
        function obj = set.Pisbjt(obj,val)
            obj = setParameter(obj,'Pisbjt',val,0,'real');
        end
        function res = get.Pisbjt(obj)
            res = getParameter(obj,'Pisbjt');
        end
        function obj = set.Pisdif(obj,val)
            obj = setParameter(obj,'Pisdif',val,0,'real');
        end
        function res = get.Pisdif(obj)
            res = getParameter(obj,'Pisdif');
        end
        function obj = set.Pistun(obj,val)
            obj = setParameter(obj,'Pistun',val,0,'real');
        end
        function res = get.Pistun(obj)
            res = getParameter(obj,'Pistun');
        end
        function obj = set.Pvrec0(obj,val)
            obj = setParameter(obj,'Pvrec0',val,0,'real');
        end
        function res = get.Pvrec0(obj)
            res = getParameter(obj,'Pvrec0');
        end
        function obj = set.Pvtun0(obj,val)
            obj = setParameter(obj,'Pvtun0',val,0,'real');
        end
        function res = get.Pvtun0(obj)
            res = getParameter(obj,'Pvtun0');
        end
        function obj = set.Pnbjt(obj,val)
            obj = setParameter(obj,'Pnbjt',val,0,'real');
        end
        function res = get.Pnbjt(obj)
            res = getParameter(obj,'Pnbjt');
        end
        function obj = set.Plbjt0(obj,val)
            obj = setParameter(obj,'Plbjt0',val,0,'real');
        end
        function res = get.Plbjt0(obj)
            res = getParameter(obj,'Plbjt0');
        end
        function obj = set.Pvabjt(obj,val)
            obj = setParameter(obj,'Pvabjt',val,0,'real');
        end
        function res = get.Pvabjt(obj)
            res = getParameter(obj,'Pvabjt');
        end
        function obj = set.Paely(obj,val)
            obj = setParameter(obj,'Paely',val,0,'real');
        end
        function res = get.Paely(obj)
            res = getParameter(obj,'Paely');
        end
        function obj = set.Pahli(obj,val)
            obj = setParameter(obj,'Pahli',val,0,'real');
        end
        function res = get.Pahli(obj)
            res = getParameter(obj,'Pahli');
        end
        function obj = set.Pvsdfb(obj,val)
            obj = setParameter(obj,'Pvsdfb',val,0,'real');
        end
        function res = get.Pvsdfb(obj)
            res = getParameter(obj,'Pvsdfb');
        end
        function obj = set.Pvsdth(obj,val)
            obj = setParameter(obj,'Pvsdth',val,0,'real');
        end
        function res = get.Pvsdth(obj)
            res = getParameter(obj,'Pvsdth');
        end
        function obj = set.Pdelvt(obj,val)
            obj = setParameter(obj,'Pdelvt',val,0,'real');
        end
        function res = get.Pdelvt(obj)
            res = getParameter(obj,'Pdelvt');
        end
        function obj = set.Pacde(obj,val)
            obj = setParameter(obj,'Pacde',val,0,'real');
        end
        function res = get.Pacde(obj)
            res = getParameter(obj,'Pacde');
        end
        function obj = set.Pmoin(obj,val)
            obj = setParameter(obj,'Pmoin',val,0,'real');
        end
        function res = get.Pmoin(obj)
            res = getParameter(obj,'Pmoin');
        end
        function obj = set.wVsubfwd(obj,val)
            obj = setParameter(obj,'wVsubfwd',val,0,'real');
        end
        function res = get.wVsubfwd(obj)
            res = getParameter(obj,'wVsubfwd');
        end
        function obj = set.wBvsub(obj,val)
            obj = setParameter(obj,'wBvsub',val,0,'real');
        end
        function res = get.wBvsub(obj)
            res = getParameter(obj,'wBvsub');
        end
        function obj = set.wBvg(obj,val)
            obj = setParameter(obj,'wBvg',val,0,'real');
        end
        function res = get.wBvg(obj)
            res = getParameter(obj,'wBvg');
        end
        function obj = set.wBvds(obj,val)
            obj = setParameter(obj,'wBvds',val,0,'real');
        end
        function res = get.wBvds(obj)
            res = getParameter(obj,'wBvds');
        end
        function obj = set.wIdsmax(obj,val)
            obj = setParameter(obj,'wIdsmax',val,0,'real');
        end
        function res = get.wIdsmax(obj)
            res = getParameter(obj,'wIdsmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end
