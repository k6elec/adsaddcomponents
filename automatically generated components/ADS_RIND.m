classdef ADS_RIND < ADScomponent
    % ADS_RIND matlab representation for the ADS RIND component
    % Rectangular Inductor
    % RIND [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Number of turns (Smorr) Unit: unknown
        N
        % Length of second outermost segment (Smorr) Unit: m
        L1
        % Length of outermost segment (Smorr) Unit: m
        L2
        % Conductor width (Smorr) Unit: m
        W
        % Conductor spacing (Smorr) Unit: m
        S
        % Conductor thickness (Smorr) Unit: m
        T
        % Conductor resistivity (rel. to copper) (Smorr) Unit: unknown
        Rho
        % Resonant frequency (Smorr) Unit: Hz
        FR
        % Physical temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.L1(obj,val)
            obj = setParameter(obj,'L1',val,0,'real');
        end
        function res = get.L1(obj)
            res = getParameter(obj,'L1');
        end
        function obj = set.L2(obj,val)
            obj = setParameter(obj,'L2',val,0,'real');
        end
        function res = get.L2(obj)
            res = getParameter(obj,'L2');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.T(obj,val)
            obj = setParameter(obj,'T',val,0,'real');
        end
        function res = get.T(obj)
            res = getParameter(obj,'T');
        end
        function obj = set.Rho(obj,val)
            obj = setParameter(obj,'Rho',val,0,'real');
        end
        function res = get.Rho(obj)
            res = getParameter(obj,'Rho');
        end
        function obj = set.FR(obj,val)
            obj = setParameter(obj,'FR',val,0,'real');
        end
        function res = get.FR(obj)
            res = getParameter(obj,'FR');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end
