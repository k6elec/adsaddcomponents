classdef ADS_ComponentWithNodes < ADScomponent
    % ADS_ComponentWithNodes matlab representation for the ADS ComponentWithNodes component
    % Component Definition
    % ComponentWithNodes [:Name] node1 ...
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % Module Name (s---s) 
        Module
        % Module Type (s---s) 
        Type
    end
    methods
        function obj = set.Module(obj,val)
            obj = setParameter(obj,'Module',val,0,'string');
        end
        function res = get.Module(obj)
            res = getParameter(obj,'Module');
        end
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'string');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
    end
end
