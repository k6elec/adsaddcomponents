classdef ADS_SBEND2 < ADScomponent
    % ADS_SBEND2 matlab representation for the ADS SBEND2 component
    % Stripline Bend (Arbitrary Angle/Miter)
    % SBEND2 [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Conductor width (Smorr) Unit: m
        W
        % Angle of bend (Smorr) Unit: deg
        Angle
        % Miter fraction (Smorr) Unit: unknown
        M
        % Stripline substrate (Sm-rs) Unit: unknown
        Subst
        % Physical temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Angle(obj,val)
            obj = setParameter(obj,'Angle',val,0,'real');
        end
        function res = get.Angle(obj)
            res = getParameter(obj,'Angle');
        end
        function obj = set.M(obj,val)
            obj = setParameter(obj,'M',val,0,'real');
        end
        function res = get.M(obj)
            res = getParameter(obj,'M');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end
