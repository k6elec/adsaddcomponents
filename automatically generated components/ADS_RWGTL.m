classdef ADS_RWGTL < ADScomponent
    % ADS_RWGTL matlab representation for the ADS RWGTL component
    % Rectangular Waveguide Transmission Line
    % RWGTL [:Name] t1 t2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Width (smorr) Unit: m
        A
        % Height (smorr) Unit: m
        B
        % Length (smorr) Unit: m
        L
        % TE Mode (smorr) 
        M
        % TE Mode (smorr) 
        N
        % Permittivity (smorr) 
        Er
        % Permeability (smorr) 
        Mur
        % Conductivity per meter (smorr) Unit: S/m
        Cond
        % Permeability of Walls (smorr) 
        Mu
        % Code for characteristic impedance type (smorr) 
        Code
    end
    methods
        function obj = set.A(obj,val)
            obj = setParameter(obj,'A',val,0,'real');
        end
        function res = get.A(obj)
            res = getParameter(obj,'A');
        end
        function obj = set.B(obj,val)
            obj = setParameter(obj,'B',val,0,'real');
        end
        function res = get.B(obj)
            res = getParameter(obj,'B');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.M(obj,val)
            obj = setParameter(obj,'M',val,0,'real');
        end
        function res = get.M(obj)
            res = getParameter(obj,'M');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Er(obj,val)
            obj = setParameter(obj,'Er',val,0,'real');
        end
        function res = get.Er(obj)
            res = getParameter(obj,'Er');
        end
        function obj = set.Mur(obj,val)
            obj = setParameter(obj,'Mur',val,0,'real');
        end
        function res = get.Mur(obj)
            res = getParameter(obj,'Mur');
        end
        function obj = set.Cond(obj,val)
            obj = setParameter(obj,'Cond',val,0,'real');
        end
        function res = get.Cond(obj)
            res = getParameter(obj,'Cond');
        end
        function obj = set.Mu(obj,val)
            obj = setParameter(obj,'Mu',val,0,'real');
        end
        function res = get.Mu(obj)
            res = getParameter(obj,'Mu');
        end
        function obj = set.Code(obj,val)
            obj = setParameter(obj,'Code',val,0,'real');
        end
        function res = get.Code(obj)
            res = getParameter(obj,'Code');
        end
    end
end
