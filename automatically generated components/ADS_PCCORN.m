classdef ADS_PCCORN < ADScomponent
    % ADS_PCCORN matlab representation for the ADS PCCORN component
    % Printed Circuit Corner
    % PCCORN [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Conductor width (Smorr) Unit: m
        W
        % Conductor layer number (Sm-ri) Unit: unknown
        CLayer
        % Printed circuit substrate (Sm-rs) Unit: unknown
        Subst
        % Physical temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.CLayer(obj,val)
            obj = setParameter(obj,'CLayer',val,0,'integer');
        end
        function res = get.CLayer(obj)
            res = getParameter(obj,'CLayer');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end
