
% let ADS shit out all its documentation into a bunch of xml and html files
generateXMLs
% correct the errors keysight introduces into their xmls
correctxml
% load the data from the xmls into a matlab struct
loadDataToMatlab
% parse the struct to generate a class for every component
generateClasses

% clean up the folders
rmdir('xml')
rmdir('html')
rmdir('new xml')