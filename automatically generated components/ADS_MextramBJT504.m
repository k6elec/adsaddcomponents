classdef ADS_MextramBJT504 < ADScomponent
    % ADS_MextramBJT504 matlab representation for the ADS MextramBJT504 component
    % Mextram Bipolar Junction Transistor level 504
    % ModelName [:Name] collector base emitter ...
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Device scaling factor (smorr) 
        Mult
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Difference between the device and ambient temperature (smorr) Unit: deg C
        Dta
        % Nonlinear spectral model on/off (s---i) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % Include selfheating effects on/off (s---b) 
        Selfheating
        % Forward transconductance (dIn_dVb2e2) (---rr) Unit: S
        Gx
        % Reverse transconductance (dIn_dVb2c2) (---rr) Unit: S
        Gy
        % Reverse transconductance (dIn_dVb2c1) (---rr) Unit: S
        Gz
        % Conductance sidewall base-emitter junction (dSIb1_dVb1e1) (---rr) Unit: S
        Sgpi
        % Conductance floor base-emitter junction (d(Ib1 + Ib2)_dVb2e1) (---rr) Unit: S
        Gpix
        % Early effect on recombination base current (dIb1_dVb2c2) (---rr) Unit: S
        Gpiy
        % Early effect on recombination base current (dIb1_dVb2c1) (---rr) Unit: S
        Gpiz
        % Early effect on avalanche current limiting (-dIavl_dVb2e1) (---rr) Unit: S
        Gmux
        % Conductance of avalanche current (-dIavl_dVb2c2) (---rr) Unit: S
        Gmuy
        % Conductance of avalanche current (-dIavl_dVb2c1) (---rr) Unit: S
        Gmuz
        % Conductance extrinsic base-collector junction (d(Iex + Ib3)_dVb1c1) (---rr) Unit: S
        Gmuex
        % Conductance extrinsic base-collector junction (dXIex_dVbc1) (---rr) Unit: S
        Xgmuex
        % Conductance of epilayer current (dIc1c2_dVb2c2) (---rr) Unit: S
        Grcvy
        % Conductance of epilayer current (dIc1c2_dVb2c1) (---rr) Unit: S
        Grcvz
        % Base resistance (1/(dIb1b2_dVb1b2)) (---rr) Unit: Ohms
        Rbv
        % Early effect on base resistance (dIb1b2_dVb2e1) (---rr) Unit: S
        Grbvx
        % Early effect on base resistance (dIb1b2_dVb2c2) (---rr) Unit: S
        Grbvy
        % Early effect on base resistance (dIb1b2_dVb2c1) (---rr) Unit: S
        Grbvz
        % Emitter resistance (---rr) Unit: Ohms
        Re
        % Constant base resistance (---rr) Unit: Ohms
        Rbc
        % Constant collector resistance (---rr) Unit: Ohms
        Rcc
        % Conductance parasitic PNP transistor (dIsub_dVb1c1) (---rr) Unit: S
        Gs
        % Conductance parasitic PNP transistor (dXIsub_dVbc1) (---rr) Unit: S
        Xgs
        % Conductance substrate failure current (dIsf_dVsc1) (---rr) Unit: S
        Gsf
        % Capacitance sidewall base-emitter junction (dSQte_dVb1e1) (---rr) Unit: F
        Scbe
        % Capacitance floor base-emitter junction (d(Qte + Qbe + Qe)_dVb2e1) (---rr) Unit: F
        Cbex
        % Early effect on base-emitter diffusion charge (dQbe_dVb2c2) (---rr) Unit: F
        Cbey
        % Early effect on base-emitter diffusion charge (dQbe_dVb2c1) (---rr) Unit: F
        Cbez
        % Early effect on base-collector diffusion charge (dQbc_dVb2e1) (---rr) Unit: F
        Cbcx
        % Capacitance floor base-collector junction (d(Qtc + Qbc + Qepi)_dVb2c2) (---rr) Unit: F
        Cbcy
        % Capacitance floor base-collector junction (d(Qtc + Qbc + Qepi)_dVb2c1) (---rr) Unit: F
        Cbcz
        % Capacitance extrinsic base-collector junction (d(Qtex + Qex)_dVb1c1) (---rr) Unit: F
        Cbcex
        % Capacitance extrinsic base-collector junction (d(XQtex + XQex)_dVbc1) (---rr) Unit: F
        Xcbcex
        % Capacitance AC currenct crowding (dQb1b2_dVb1b2) (---rr) Unit: F
        Cb1b2
        % Cross-capacitance AC current crowding (dQb1b2_dVb2e1) (---rr) Unit: F
        Cb1b2x
        % Cross-capacitance AC current crowding (dQb1b2_dVb2c2) (---rr) Unit: F
        Cb1b2y
        % Cross-capacitance AC current crowding (dQb1b2_dVb2c1) (---rr) Unit: F
        Cb1b2z
        % Capacitance substrate-collector junction (dQts_dVsc1) (---rr) Unit: F
        Cts
    end
    methods
        function obj = set.Mult(obj,val)
            obj = setParameter(obj,'Mult',val,0,'real');
        end
        function res = get.Mult(obj)
            res = getParameter(obj,'Mult');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Dta(obj,val)
            obj = setParameter(obj,'Dta',val,0,'real');
        end
        function res = get.Dta(obj)
            res = getParameter(obj,'Dta');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Selfheating(obj,val)
            obj = setParameter(obj,'Selfheating',val,0,'boolean');
        end
        function res = get.Selfheating(obj)
            res = getParameter(obj,'Selfheating');
        end
        function obj = set.Gx(obj,val)
            obj = setParameter(obj,'Gx',val,0,'real');
        end
        function res = get.Gx(obj)
            res = getParameter(obj,'Gx');
        end
        function obj = set.Gy(obj,val)
            obj = setParameter(obj,'Gy',val,0,'real');
        end
        function res = get.Gy(obj)
            res = getParameter(obj,'Gy');
        end
        function obj = set.Gz(obj,val)
            obj = setParameter(obj,'Gz',val,0,'real');
        end
        function res = get.Gz(obj)
            res = getParameter(obj,'Gz');
        end
        function obj = set.Sgpi(obj,val)
            obj = setParameter(obj,'Sgpi',val,0,'real');
        end
        function res = get.Sgpi(obj)
            res = getParameter(obj,'Sgpi');
        end
        function obj = set.Gpix(obj,val)
            obj = setParameter(obj,'Gpix',val,0,'real');
        end
        function res = get.Gpix(obj)
            res = getParameter(obj,'Gpix');
        end
        function obj = set.Gpiy(obj,val)
            obj = setParameter(obj,'Gpiy',val,0,'real');
        end
        function res = get.Gpiy(obj)
            res = getParameter(obj,'Gpiy');
        end
        function obj = set.Gpiz(obj,val)
            obj = setParameter(obj,'Gpiz',val,0,'real');
        end
        function res = get.Gpiz(obj)
            res = getParameter(obj,'Gpiz');
        end
        function obj = set.Gmux(obj,val)
            obj = setParameter(obj,'Gmux',val,0,'real');
        end
        function res = get.Gmux(obj)
            res = getParameter(obj,'Gmux');
        end
        function obj = set.Gmuy(obj,val)
            obj = setParameter(obj,'Gmuy',val,0,'real');
        end
        function res = get.Gmuy(obj)
            res = getParameter(obj,'Gmuy');
        end
        function obj = set.Gmuz(obj,val)
            obj = setParameter(obj,'Gmuz',val,0,'real');
        end
        function res = get.Gmuz(obj)
            res = getParameter(obj,'Gmuz');
        end
        function obj = set.Gmuex(obj,val)
            obj = setParameter(obj,'Gmuex',val,0,'real');
        end
        function res = get.Gmuex(obj)
            res = getParameter(obj,'Gmuex');
        end
        function obj = set.Xgmuex(obj,val)
            obj = setParameter(obj,'Xgmuex',val,0,'real');
        end
        function res = get.Xgmuex(obj)
            res = getParameter(obj,'Xgmuex');
        end
        function obj = set.Grcvy(obj,val)
            obj = setParameter(obj,'Grcvy',val,0,'real');
        end
        function res = get.Grcvy(obj)
            res = getParameter(obj,'Grcvy');
        end
        function obj = set.Grcvz(obj,val)
            obj = setParameter(obj,'Grcvz',val,0,'real');
        end
        function res = get.Grcvz(obj)
            res = getParameter(obj,'Grcvz');
        end
        function obj = set.Rbv(obj,val)
            obj = setParameter(obj,'Rbv',val,0,'real');
        end
        function res = get.Rbv(obj)
            res = getParameter(obj,'Rbv');
        end
        function obj = set.Grbvx(obj,val)
            obj = setParameter(obj,'Grbvx',val,0,'real');
        end
        function res = get.Grbvx(obj)
            res = getParameter(obj,'Grbvx');
        end
        function obj = set.Grbvy(obj,val)
            obj = setParameter(obj,'Grbvy',val,0,'real');
        end
        function res = get.Grbvy(obj)
            res = getParameter(obj,'Grbvy');
        end
        function obj = set.Grbvz(obj,val)
            obj = setParameter(obj,'Grbvz',val,0,'real');
        end
        function res = get.Grbvz(obj)
            res = getParameter(obj,'Grbvz');
        end
        function obj = set.Re(obj,val)
            obj = setParameter(obj,'Re',val,0,'real');
        end
        function res = get.Re(obj)
            res = getParameter(obj,'Re');
        end
        function obj = set.Rbc(obj,val)
            obj = setParameter(obj,'Rbc',val,0,'real');
        end
        function res = get.Rbc(obj)
            res = getParameter(obj,'Rbc');
        end
        function obj = set.Rcc(obj,val)
            obj = setParameter(obj,'Rcc',val,0,'real');
        end
        function res = get.Rcc(obj)
            res = getParameter(obj,'Rcc');
        end
        function obj = set.Gs(obj,val)
            obj = setParameter(obj,'Gs',val,0,'real');
        end
        function res = get.Gs(obj)
            res = getParameter(obj,'Gs');
        end
        function obj = set.Xgs(obj,val)
            obj = setParameter(obj,'Xgs',val,0,'real');
        end
        function res = get.Xgs(obj)
            res = getParameter(obj,'Xgs');
        end
        function obj = set.Gsf(obj,val)
            obj = setParameter(obj,'Gsf',val,0,'real');
        end
        function res = get.Gsf(obj)
            res = getParameter(obj,'Gsf');
        end
        function obj = set.Scbe(obj,val)
            obj = setParameter(obj,'Scbe',val,0,'real');
        end
        function res = get.Scbe(obj)
            res = getParameter(obj,'Scbe');
        end
        function obj = set.Cbex(obj,val)
            obj = setParameter(obj,'Cbex',val,0,'real');
        end
        function res = get.Cbex(obj)
            res = getParameter(obj,'Cbex');
        end
        function obj = set.Cbey(obj,val)
            obj = setParameter(obj,'Cbey',val,0,'real');
        end
        function res = get.Cbey(obj)
            res = getParameter(obj,'Cbey');
        end
        function obj = set.Cbez(obj,val)
            obj = setParameter(obj,'Cbez',val,0,'real');
        end
        function res = get.Cbez(obj)
            res = getParameter(obj,'Cbez');
        end
        function obj = set.Cbcx(obj,val)
            obj = setParameter(obj,'Cbcx',val,0,'real');
        end
        function res = get.Cbcx(obj)
            res = getParameter(obj,'Cbcx');
        end
        function obj = set.Cbcy(obj,val)
            obj = setParameter(obj,'Cbcy',val,0,'real');
        end
        function res = get.Cbcy(obj)
            res = getParameter(obj,'Cbcy');
        end
        function obj = set.Cbcz(obj,val)
            obj = setParameter(obj,'Cbcz',val,0,'real');
        end
        function res = get.Cbcz(obj)
            res = getParameter(obj,'Cbcz');
        end
        function obj = set.Cbcex(obj,val)
            obj = setParameter(obj,'Cbcex',val,0,'real');
        end
        function res = get.Cbcex(obj)
            res = getParameter(obj,'Cbcex');
        end
        function obj = set.Xcbcex(obj,val)
            obj = setParameter(obj,'Xcbcex',val,0,'real');
        end
        function res = get.Xcbcex(obj)
            res = getParameter(obj,'Xcbcex');
        end
        function obj = set.Cb1b2(obj,val)
            obj = setParameter(obj,'Cb1b2',val,0,'real');
        end
        function res = get.Cb1b2(obj)
            res = getParameter(obj,'Cb1b2');
        end
        function obj = set.Cb1b2x(obj,val)
            obj = setParameter(obj,'Cb1b2x',val,0,'real');
        end
        function res = get.Cb1b2x(obj)
            res = getParameter(obj,'Cb1b2x');
        end
        function obj = set.Cb1b2y(obj,val)
            obj = setParameter(obj,'Cb1b2y',val,0,'real');
        end
        function res = get.Cb1b2y(obj)
            res = getParameter(obj,'Cb1b2y');
        end
        function obj = set.Cb1b2z(obj,val)
            obj = setParameter(obj,'Cb1b2z',val,0,'real');
        end
        function res = get.Cb1b2z(obj)
            res = getParameter(obj,'Cb1b2z');
        end
        function obj = set.Cts(obj,val)
            obj = setParameter(obj,'Cts',val,0,'real');
        end
        function res = get.Cts(obj)
            res = getParameter(obj,'Cts');
        end
    end
end
