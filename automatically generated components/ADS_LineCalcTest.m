classdef ADS_LineCalcTest < ADSnodeless
    % ADS_LineCalcTest matlab representation for the ADS LineCalcTest component
    % LineCalc Test
    % LineCalcTest [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Synthesis (s---b) 
        Synth
    end
    methods
        function obj = set.Synth(obj,val)
            obj = setParameter(obj,'Synth',val,0,'boolean');
        end
        function res = get.Synth(obj)
            res = getParameter(obj,'Synth');
        end
    end
end
