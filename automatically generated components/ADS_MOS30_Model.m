classdef ADS_MOS30_Model < ADSmodel
    % ADS_MOS30_Model matlab representation for the ADS MOS30_Model component
    % Mos Model 30 Version 30.02 model
    % model ModelName MOS30 ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % NMOS type MM30 (s---b) 
        NMOS
        % PMOS type MM30 (s---b) 
        PMOS
        % selection of Mos Model 30 release 30.2 (s---r) 
        Release
        % Ohmic resistance at zero-bias (smorr) Unit: Ohms
        Ron
        % Space charge resistance at zero-bias (smorr) Unit: Ohms
        Rsat
        % Critical drain-source voltage for hot carriers (smorr) Unit: V
        Vsat
        % Velocity saturation coefficient (smorr) 
        Psat
        % Pinch off voltage at zero gate and substrate voltages (VP=0 -> No depletion and/or accumulation in the channel) (smorr) Unit: V
        Vp
        % Gate oxide thickness (TOX>0 MOSFET Device, TOX<=0 No depletion at the surface) (smorr) Unit: cm
        Tox
        % Doping level channel (smorr) Unit: cm^-3
        Dch
        % Doping level substrate (DSUB<=0 -> No depletion from the substrate) (smorr) Unit: cm^-3
        Dsub
        % Substrate Diffusion Voltage (smorr) Unit: V
        Vsub
        % Bandgap voltage channel (smorr) Unit: V
        Vgap
        % Gate capacitance at zero bias (smorr) Unit: F
        Cgate
        % Substrate capacitance at zero bias (smorr) Unit: F
        Csub
        % Space charge transit time of the channel (smorr) Unit: s
        Tausc
        % Temperature coefficient resistivity of the channel (smorr) 
        Ach
        % Flicker noise coefficient (smorr) 
        Kf
        % Flicker noise exponent (smorr) 
        Af
        % Reference temperature (smorr) Unit: deg C
        Tref
        % Reference temperature (smorr) Unit: deg C
        Tnom
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
    end
    methods
        function obj = set.NMOS(obj,val)
            obj = setParameter(obj,'NMOS',val,0,'boolean');
        end
        function res = get.NMOS(obj)
            res = getParameter(obj,'NMOS');
        end
        function obj = set.PMOS(obj,val)
            obj = setParameter(obj,'PMOS',val,0,'boolean');
        end
        function res = get.PMOS(obj)
            res = getParameter(obj,'PMOS');
        end
        function obj = set.Release(obj,val)
            obj = setParameter(obj,'Release',val,0,'real');
        end
        function res = get.Release(obj)
            res = getParameter(obj,'Release');
        end
        function obj = set.Ron(obj,val)
            obj = setParameter(obj,'Ron',val,0,'real');
        end
        function res = get.Ron(obj)
            res = getParameter(obj,'Ron');
        end
        function obj = set.Rsat(obj,val)
            obj = setParameter(obj,'Rsat',val,0,'real');
        end
        function res = get.Rsat(obj)
            res = getParameter(obj,'Rsat');
        end
        function obj = set.Vsat(obj,val)
            obj = setParameter(obj,'Vsat',val,0,'real');
        end
        function res = get.Vsat(obj)
            res = getParameter(obj,'Vsat');
        end
        function obj = set.Psat(obj,val)
            obj = setParameter(obj,'Psat',val,0,'real');
        end
        function res = get.Psat(obj)
            res = getParameter(obj,'Psat');
        end
        function obj = set.Vp(obj,val)
            obj = setParameter(obj,'Vp',val,0,'real');
        end
        function res = get.Vp(obj)
            res = getParameter(obj,'Vp');
        end
        function obj = set.Tox(obj,val)
            obj = setParameter(obj,'Tox',val,0,'real');
        end
        function res = get.Tox(obj)
            res = getParameter(obj,'Tox');
        end
        function obj = set.Dch(obj,val)
            obj = setParameter(obj,'Dch',val,0,'real');
        end
        function res = get.Dch(obj)
            res = getParameter(obj,'Dch');
        end
        function obj = set.Dsub(obj,val)
            obj = setParameter(obj,'Dsub',val,0,'real');
        end
        function res = get.Dsub(obj)
            res = getParameter(obj,'Dsub');
        end
        function obj = set.Vsub(obj,val)
            obj = setParameter(obj,'Vsub',val,0,'real');
        end
        function res = get.Vsub(obj)
            res = getParameter(obj,'Vsub');
        end
        function obj = set.Vgap(obj,val)
            obj = setParameter(obj,'Vgap',val,0,'real');
        end
        function res = get.Vgap(obj)
            res = getParameter(obj,'Vgap');
        end
        function obj = set.Cgate(obj,val)
            obj = setParameter(obj,'Cgate',val,0,'real');
        end
        function res = get.Cgate(obj)
            res = getParameter(obj,'Cgate');
        end
        function obj = set.Csub(obj,val)
            obj = setParameter(obj,'Csub',val,0,'real');
        end
        function res = get.Csub(obj)
            res = getParameter(obj,'Csub');
        end
        function obj = set.Tausc(obj,val)
            obj = setParameter(obj,'Tausc',val,0,'real');
        end
        function res = get.Tausc(obj)
            res = getParameter(obj,'Tausc');
        end
        function obj = set.Ach(obj,val)
            obj = setParameter(obj,'Ach',val,0,'real');
        end
        function res = get.Ach(obj)
            res = getParameter(obj,'Ach');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Tref(obj,val)
            obj = setParameter(obj,'Tref',val,0,'real');
        end
        function res = get.Tref(obj)
            res = getParameter(obj,'Tref');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
    end
end
