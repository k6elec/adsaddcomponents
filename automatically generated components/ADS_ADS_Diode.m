classdef ADS_ADS_Diode < ADScomponent
    % ADS_ADS_Diode matlab representation for the ADS ADS_Diode component
    % ADS diode
    % ModelName [:Name] anode cathode
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Junction area (smorr) Unit: m^2
        Area
        % DC operating region, 0=off, 1=on (s---i) 
        Region
        % Small signal conductance (---rr) Unit: S
        Gd
        % Small signal capacitance (---rr) Unit: F
        Cd
    end
    methods
        function obj = set.Area(obj,val)
            obj = setParameter(obj,'Area',val,0,'real');
        end
        function res = get.Area(obj)
            res = getParameter(obj,'Area');
        end
        function obj = set.Region(obj,val)
            obj = setParameter(obj,'Region',val,0,'integer');
        end
        function res = get.Region(obj)
            res = getParameter(obj,'Region');
        end
        function obj = set.Gd(obj,val)
            obj = setParameter(obj,'Gd',val,0,'real');
        end
        function res = get.Gd(obj)
            res = getParameter(obj,'Gd');
        end
        function obj = set.Cd(obj,val)
            obj = setParameter(obj,'Cd',val,0,'real');
        end
        function res = get.Cd(obj)
            res = getParameter(obj,'Cd');
        end
    end
end
