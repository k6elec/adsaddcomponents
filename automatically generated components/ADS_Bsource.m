classdef ADS_Bsource < ADScomponent
    % ADS_Bsource matlab representation for the ADS Bsource component
    % Behavioral Source
    % Bsource [:Name] p1 n1 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Behavioral expression for r in the equation v=r*i (smo-r) Unit: Ohms
        R
        % Behavioral expression for g in the equation i=g*v (smo-r) Unit: S
        G
        % Behavioral expression for l in the equation v=d(l*i)/dt (smo-r) Unit: H
        L
        % Behavioral expression for phi in the equation v=d(phi)/dt (smo-r) Unit: Wb
        Phi
        % Behavioral expression for c in the equation i=d(c*v)/dt (smo-r) Unit: F
        C
        % Behavioral expression for q in the equation i=dq/dt (smo-r) Unit: C
        Q
        % Behavioral expression defining Bsource voltage: v = expr (smo-r) Unit: V
        V
        % Behavioral expression defining Bsource current: i = expr (smo-r) Unit: A
        I
        % Expression to differentiate: _ddtk = ddt(Ddt[k]) (smo-r) 
        Ddt
        % Expression to integrate: _idtk = idt(Idt[k]) (smo-r) 
        Idt
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Temperature above circuit ambient (smorr) Unit: deg C
        Trise
        % Device operating temperature (smorr) Unit: deg C
        Tnom
        % Coefficient for linear temperature scaling (smorr) Unit: 1/deg C
        Tc1
        % Coefficient for quadratic temperature scaling (smorr) Unit: 1/(deg C)^2
        Tc2
        % Minimum value for clipping (smorr) 
        Min_val
        % Maximum value for clipping (smorr) 
        Max_val
        % Noise generation on/off (s---b) 
        Noise
        % Behavioral expression defining Bsource white noise (smo-r) 
        White_noise
        % Behavioral expression defining Bsource flicker noise (smo-r) 
        Flicker_noise
        % Flicker-noise coefficient (smorr) 
        Kf
        % Flicker-noise exponent (smorr) 
        Af
        % Flicker noise frequency exponent (smorr) 
        Fexp
        % Resistor physical length (for flicker noise contribution) (smorr) Unit: m
        Length
        % Resistor physical width (for flicker noise contribution) (smorr) Unit: m
        Width
        % Flicker noise length exponent (smorr) 
        Ldexp
        % Flicker noise width exponent (smorr) 
        Wdexp
        % Current probe instance name (s---d) 
        CurrentProbe
        % Current probe port of the controlling current (s---i) 
        CurrentProbePort
        % Expression evaluated to detect strange DC behavior (s---r) 
        StrangeDC
    end
    methods
        function obj = set.R(obj,val)
            obj = setParameter(obj,'R',val,0,'real');
        end
        function res = get.R(obj)
            res = getParameter(obj,'R');
        end
        function obj = set.G(obj,val)
            obj = setParameter(obj,'G',val,0,'real');
        end
        function res = get.G(obj)
            res = getParameter(obj,'G');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Phi(obj,val)
            obj = setParameter(obj,'Phi',val,0,'real');
        end
        function res = get.Phi(obj)
            res = getParameter(obj,'Phi');
        end
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,0,'real');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.Q(obj,val)
            obj = setParameter(obj,'Q',val,0,'real');
        end
        function res = get.Q(obj)
            res = getParameter(obj,'Q');
        end
        function obj = set.V(obj,val)
            obj = setParameter(obj,'V',val,0,'real');
        end
        function res = get.V(obj)
            res = getParameter(obj,'V');
        end
        function obj = set.I(obj,val)
            obj = setParameter(obj,'I',val,0,'real');
        end
        function res = get.I(obj)
            res = getParameter(obj,'I');
        end
        function obj = set.Ddt(obj,val)
            obj = setParameter(obj,'Ddt',val,1,'real');
        end
        function res = get.Ddt(obj)
            res = getParameter(obj,'Ddt');
        end
        function obj = set.Idt(obj,val)
            obj = setParameter(obj,'Idt',val,1,'real');
        end
        function res = get.Idt(obj)
            res = getParameter(obj,'Idt');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Tc1(obj,val)
            obj = setParameter(obj,'Tc1',val,0,'real');
        end
        function res = get.Tc1(obj)
            res = getParameter(obj,'Tc1');
        end
        function obj = set.Tc2(obj,val)
            obj = setParameter(obj,'Tc2',val,0,'real');
        end
        function res = get.Tc2(obj)
            res = getParameter(obj,'Tc2');
        end
        function obj = set.Min_val(obj,val)
            obj = setParameter(obj,'Min_val',val,0,'real');
        end
        function res = get.Min_val(obj)
            res = getParameter(obj,'Min_val');
        end
        function obj = set.Max_val(obj,val)
            obj = setParameter(obj,'Max_val',val,0,'real');
        end
        function res = get.Max_val(obj)
            res = getParameter(obj,'Max_val');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.White_noise(obj,val)
            obj = setParameter(obj,'White_noise',val,0,'real');
        end
        function res = get.White_noise(obj)
            res = getParameter(obj,'White_noise');
        end
        function obj = set.Flicker_noise(obj,val)
            obj = setParameter(obj,'Flicker_noise',val,0,'real');
        end
        function res = get.Flicker_noise(obj)
            res = getParameter(obj,'Flicker_noise');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Fexp(obj,val)
            obj = setParameter(obj,'Fexp',val,0,'real');
        end
        function res = get.Fexp(obj)
            res = getParameter(obj,'Fexp');
        end
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.Width(obj,val)
            obj = setParameter(obj,'Width',val,0,'real');
        end
        function res = get.Width(obj)
            res = getParameter(obj,'Width');
        end
        function obj = set.Ldexp(obj,val)
            obj = setParameter(obj,'Ldexp',val,0,'real');
        end
        function res = get.Ldexp(obj)
            res = getParameter(obj,'Ldexp');
        end
        function obj = set.Wdexp(obj,val)
            obj = setParameter(obj,'Wdexp',val,0,'real');
        end
        function res = get.Wdexp(obj)
            res = getParameter(obj,'Wdexp');
        end
        function obj = set.CurrentProbe(obj,val)
            obj = setParameter(obj,'CurrentProbe',val,1,'string');
        end
        function res = get.CurrentProbe(obj)
            res = getParameter(obj,'CurrentProbe');
        end
        function obj = set.CurrentProbePort(obj,val)
            obj = setParameter(obj,'CurrentProbePort',val,1,'integer');
        end
        function res = get.CurrentProbePort(obj)
            res = getParameter(obj,'CurrentProbePort');
        end
        function obj = set.StrangeDC(obj,val)
            obj = setParameter(obj,'StrangeDC',val,0,'real');
        end
        function res = get.StrangeDC(obj)
            res = getParameter(obj,'StrangeDC');
        end
    end
end
