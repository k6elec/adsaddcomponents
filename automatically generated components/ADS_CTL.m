classdef ADS_CTL < ADScomponent
    % ADS_CTL matlab representation for the ADS CTL component
    % Ideal coupled transmission line
    % CTL [:Name] n1 n2 n3 n4
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Even mode impedance of coupled transmission line (Smorr) Unit: oh
        Ze
        % Odd mode impedance of coupled transmission line (Smorr) Unit: oh
        Zo
        % Length of coupled transmission line (Smorr) Unit: m
        L
        % Even mode relative velocity of coupled transmission line (Smorr) Unit: 1
        Ve
        % Odd mode relative velocity of coupled transmission line (Smorr) Unit: 1
        Vo
        % Even mode series Resistance of coupled transmission line (smorr) Unit: oh/m
        Re
        % Odd mode series Resistance of coupled transmission line (smorr) Unit: oh/m
        Ro
        % Reference frequency of coupled transmission line (smorr) Unit: Hz
        F
        % Even mode shunt Conductance of coupled transmission line (smorr) Unit: Sie/m
        Ge
        % Odd mode shunt Conductance of coupled transmission line (smorr) Unit: Sie/m
        Go
    end
    methods
        function obj = set.Ze(obj,val)
            obj = setParameter(obj,'Ze',val,0,'real');
        end
        function res = get.Ze(obj)
            res = getParameter(obj,'Ze');
        end
        function obj = set.Zo(obj,val)
            obj = setParameter(obj,'Zo',val,0,'real');
        end
        function res = get.Zo(obj)
            res = getParameter(obj,'Zo');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Ve(obj,val)
            obj = setParameter(obj,'Ve',val,0,'real');
        end
        function res = get.Ve(obj)
            res = getParameter(obj,'Ve');
        end
        function obj = set.Vo(obj,val)
            obj = setParameter(obj,'Vo',val,0,'real');
        end
        function res = get.Vo(obj)
            res = getParameter(obj,'Vo');
        end
        function obj = set.Re(obj,val)
            obj = setParameter(obj,'Re',val,0,'real');
        end
        function res = get.Re(obj)
            res = getParameter(obj,'Re');
        end
        function obj = set.Ro(obj,val)
            obj = setParameter(obj,'Ro',val,0,'real');
        end
        function res = get.Ro(obj)
            res = getParameter(obj,'Ro');
        end
        function obj = set.F(obj,val)
            obj = setParameter(obj,'F',val,0,'real');
        end
        function res = get.F(obj)
            res = getParameter(obj,'F');
        end
        function obj = set.Ge(obj,val)
            obj = setParameter(obj,'Ge',val,0,'real');
        end
        function res = get.Ge(obj)
            res = getParameter(obj,'Ge');
        end
        function obj = set.Go(obj,val)
            obj = setParameter(obj,'Go',val,0,'real');
        end
        function res = get.Go(obj)
            res = getParameter(obj,'Go');
        end
    end
end
