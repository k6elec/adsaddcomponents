classdef ADS_BJT_Model < ADSmodel
    % ADS_BJT_Model matlab representation for the ADS BJT_Model component
    % Bipolar Junction Transistor model
    % model ModelName BJT ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % NPN bipolar transistor (s---b) 
        NPN
        % PNP bipolar transistor (s---b) 
        PNP
        % Saturation current (smorr) Unit: A
        Is
        % Base-collector saturation current (smorr) Unit: A
        Ibc
        % Base-emitter saturation current (smorr) Unit: A
        Ibe
        % Forward beta (smorr) 
        Bf
        % Forward emission coefficient (smorr) 
        Nf
        % Forward early voltage (smorr) Unit: V
        Vaf
        % High current corner for forward beta (smorr) Unit: A
        Ikf
        % B-E leakage saturation current (smorr) Unit: A
        Ise
        % Forward leakage saturation current coefficient. If Ise is not given, Ise=C2*Is (smorr) 
        C2
        % B-E leakage emission coef (smorr) 
        Ne
        % Reverse beta (smorr) 
        Br
        % Reverse emission coefficient (smorr) 
        Nr
        % Reverse early voltage (smorr) Unit: V
        Var
        % High current corner for reverse beta (smorr) Unit: A
        Ikr
        % B-C leakage saturation current (smorr) Unit: A
        Isc
        % Reverse leakage saturation current coefficient. If Isc is not given, Isc=C4*Is (smorr) 
        C4
        % B-C leakage emission coef (smorr) 
        Nc
        % Zero bias base resistance (smorr) Unit: Ohms
        Rb
        % Current for base resistance midpoint (smorr) Unit: A
        Irb
        % Minimum base resistance for high currents (smorr) Unit: Ohms
        Rbm
        % Effective base noise resistance (smorr) 
        Rbnoi
        % Emitter resistance (smorr) Unit: Ohms
        Re
        % Collector resistance (smorr) Unit: Ohms
        Rc
        % Minimum value of parasitic resistances (smorr) Unit: Ohms
        Minr
        % Explosion current (smorr) Unit: A
        Imax
        % B-E zero-bias junction capacitance (smorr) Unit: F
        Cje
        % B-E built-in junction potential (smorr) Unit: V
        Vje
        % B-E junction exponent (smorr) 
        Mje
        % B-C zero-bias junction capacitance (smorr) Unit: F
        Cjc
        % B-C built-in junction potential (smorr) Unit: V
        Vjc
        % B-C junction exponent (smorr) 
        Mjc
        % Fraction of B-C capacitance tied to internal base (smorr) 
        Xcjc
        % C-S zero-bias junction capacitance (smorr) Unit: F
        Cjs
        % C-S built-in junction potential (smorr) Unit: V
        Vjs
        % C-S junction exponent (smorr) 
        Mjs
        % Junction capacitor forward-bias threshold (smorr) 
        Fc
        % Ideal forward transit time (smorr) Unit: s
        Tf
        % Coefficient for bias dependence of `tf' (smorr) 
        Xtf
        % Voltage describing Vbc dependence of `tf' (smorr) Unit: V
        Vtf
        % High current parameter for effect on `tf' (smorr) Unit: A
        Itf
        % Excess phase at freq = 1.0/(tf*2pi) Hz (smorr) Unit: deg
        Ptf
        % Ideal reverse transit time (smorr) Unit: s
        Tr
        % Flicker (1/f) noise coefficient (smorr) 
        Kf
        % Flicker (1/f) noise exponent (smorr) 
        Af
        % Burst noise coefficient (popcorn noise) (smorr) 
        Kb
        % Burst noise exponent (popcorn noise) (smorr) 
        Ab
        % Burst noise corner frequency (popcorn noise) (smorr) Unit: Hz
        Fb
        % Collector-substrate saturation current (smorr) Unit: A
        Iss
        % Collector-substrate emission coeff (smorr) 
        Ns
        % Secured model parameters (s---b) 
        Secured
        % High-current roll-off coeff (smorr) 
        Nk
        % Flicker noise frequency exponent (smorr) 
        Ffe
        % Lateral substrate geometry (s--rb) 
        Lateral
        % base resistance model: Spice=1, MDS=0 (s--rb) 
        RbModel
        % use the approximation for Qb vs early voltage (s---b) 
        Approxqb
        % Parameter measurement temperature (smorr) Unit: deg C
        Tnom
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Temperature equation selector (0/1/2/3) (s---i) 
        Tlev
        % Temperature equation selector for capacitance (0/1/2/3) (s---i) 
        Tlevc
        % Band gap (smorr) Unit: eV
        Eg
        % Energy gap temperature coefficient alpha (smorr) Unit: V/deg C
        EgAlpha
        % Energy gap temperature coefficient beta (smorr) Unit: K
        EgBeta
        % Bf linear temperature coefficient (smorr) Unit: 1/deg C
        Tbf1
        % Bf quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tbf2
        % Br linear temperature coefficient (smorr) Unit: 1/deg C
        Tbr1
        % Br quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tbr2
        % Cbc linear temperature coefficient (smorr) Unit: 1/deg C
        Tcbc
        % Cbe linear temperature coefficient (smorr) Unit: 1/deg C
        Tcbe
        % Ccs linear temperature coefficient (smorr) Unit: 1/deg C
        Tccs
        % Ikf linear temperature coefficient (smorr) Unit: 1/deg C
        Tikf1
        % Ikf quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tikf2
        % Ikr linear temperature coefficient (smorr) Unit: 1/deg C
        Tikr1
        % Ikr quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tikr2
        % Irb linear temperature coefficient (smorr) Unit: 1/deg C
        Tirb1
        % Irb quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tirb2
        % Is/Ibe/Ibc linear temperature coefficient (smorr) Unit: 1/deg C
        Tis1
        % Is/Ibe/Ibc quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tis2
        % Isc linear temperature coefficient (smorr) Unit: 1/deg C
        Tisc1
        % Isc quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tisc2
        % Ise linear temperature coefficient (smorr) Unit: 1/deg C
        Tise1
        % Ise quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tise2
        % Iss linear temperature coefficient (smorr) Unit: 1/deg C
        Tiss1
        % Iss quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tiss2
        % Itf linear temperature coefficient (smorr) Unit: 1/deg C
        Titf1
        % Itf quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Titf2
        % Vtf linear temperature coefficient (smorr) Unit: 1/deg C
        Tvtf1
        % Vtf quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tvtf2
        % Xtf linear temperature coefficient (smorr) Unit: 1/deg C
        Txtf1
        % Xtf quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Txtf2
        % Mjc linear temperature coefficient (smorr) Unit: 1/deg C
        Tmjc1
        % Mjc quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tmjc2
        % Mje linear temperature coefficient (smorr) Unit: 1/deg C
        Tmje1
        % Mje quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tmje2
        % Mjs linear temperature coefficient (smorr) Unit: 1/deg C
        Tmjs1
        % Mjs quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tmjs2
        % Nc linear temperature coefficient (smorr) Unit: 1/deg C
        Tnc1
        % Nc quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tnc2
        % Ne linear temperature coefficient (smorr) Unit: 1/deg C
        Tne1
        % Ne quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tne2
        % Nf linear temperature coefficient (smorr) Unit: 1/deg C
        Tnf1
        % Nf quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tnf2
        % Nr linear temperature coefficient (smorr) Unit: 1/deg C
        Tnr1
        % Nr quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tnr2
        % Ns linear temperature coefficient (smorr) Unit: 1/deg C
        Tns1
        % Ns quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tns2
        % Rb linear temperature coefficient (smorr) Unit: 1/deg C
        Trb1
        % Rb quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Trb2
        % Rc linear temperature coefficient (smorr) Unit: 1/deg C
        Trc1
        % Rc quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Trc2
        % Re linear temperature coefficient (smorr) Unit: 1/deg C
        Tre1
        % Re quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tre2
        % Rbm linear temperature coefficient (smorr) Unit: 1/deg C
        Trm1
        % Rbm quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Trm2
        % Tf linear temperature coefficient (smorr) Unit: 1/deg C
        Ttf1
        % Tf quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Ttf2
        % Tr linear temperature coefficient (smorr) Unit: 1/deg C
        Ttr1
        % Tr quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Ttr2
        % Vaf linear temperature coefficient (smorr) Unit: 1/deg C
        Tvaf1
        % Vaf quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tvaf2
        % Var linear temperature coefficient (smorr) Unit: 1/deg C
        Tvar1
        % Var quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tvar2
        % Vjc linear temperature coefficient (smorr) Unit: 1/deg C
        Tvjc
        % Vje linear temperature coefficient (smorr) Unit: 1/deg C
        Tvje
        % Vjs linear temperature coefficient (smorr) Unit: 1/deg C
        Tvjs
        % Cbo linear temperature coefficient (smorr) Unit: 1/deg C
        Tcbo
        % Gbo linear temperature coefficient (smorr) Unit: 1/deg C
        Tgbo
        % Beta temperature exponent (smorr) 
        Xtb
        % Temperature exponent for effect on Js (smorr) 
        Xti
        % Explosion current (smorr) Unit: A
        Imelt
        % B-E space charge integral multiplier (smorr) Unit: V^-1
        Ke
        % B-C space charge integral multiplier (smorr) Unit: V^-1
        Kc
        % Variable collector resistance (smorr) Unit: Ohms
        Rcv
        % Minimum collector resistance (smorr) Unit: Ohms
        Rcm
        % Collector background doping concentration (smorr) Unit: cm^-3
        Dope
        % Current crowding exponent (smorr) 
        Cex
        % Current crowding normalization constant (smorr) Unit: A
        Cco
        % Extrapolated 0-volt B-C leagage current (smorr) Unit: A
        Cbo
        % Slope of Icbo vs. Vbc above Vbo (smorr) Unit: S
        Gbo
        % Slope of Icbo vs. Vbc at Vbc=0 (smorr) Unit: V
        Vbo
        % not used (smorr) 
        Xcjc2
        % not used (smorr) Unit: s
        Td
        % Substrate junction forward bias (warning) (s--rr) Unit: V
        wVsubfwd
        % Substrate junction reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvsub
        % Base-emitter reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvbe
        % Base-collector reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvbc
        % Collector-emitter breakdown voltage (warning) (s--rr) Unit: V
        wBvce
        % Base-collector forward bias (warning) (s--rr) Unit: V
        wVbcfwd
        % Maximum base current (warning) (s--rr) Unit: A
        wIbmax
        % Maximum collector current (warning) (s--rr) Unit: A
        wIcmax
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
        % If SPECTRE compatible is 1, set by parser (sm-ri) 
        SpectreCompatible
    end
    methods
        function obj = set.NPN(obj,val)
            obj = setParameter(obj,'NPN',val,0,'boolean');
        end
        function res = get.NPN(obj)
            res = getParameter(obj,'NPN');
        end
        function obj = set.PNP(obj,val)
            obj = setParameter(obj,'PNP',val,0,'boolean');
        end
        function res = get.PNP(obj)
            res = getParameter(obj,'PNP');
        end
        function obj = set.Is(obj,val)
            obj = setParameter(obj,'Is',val,0,'real');
        end
        function res = get.Is(obj)
            res = getParameter(obj,'Is');
        end
        function obj = set.Ibc(obj,val)
            obj = setParameter(obj,'Ibc',val,0,'real');
        end
        function res = get.Ibc(obj)
            res = getParameter(obj,'Ibc');
        end
        function obj = set.Ibe(obj,val)
            obj = setParameter(obj,'Ibe',val,0,'real');
        end
        function res = get.Ibe(obj)
            res = getParameter(obj,'Ibe');
        end
        function obj = set.Bf(obj,val)
            obj = setParameter(obj,'Bf',val,0,'real');
        end
        function res = get.Bf(obj)
            res = getParameter(obj,'Bf');
        end
        function obj = set.Nf(obj,val)
            obj = setParameter(obj,'Nf',val,0,'real');
        end
        function res = get.Nf(obj)
            res = getParameter(obj,'Nf');
        end
        function obj = set.Vaf(obj,val)
            obj = setParameter(obj,'Vaf',val,0,'real');
        end
        function res = get.Vaf(obj)
            res = getParameter(obj,'Vaf');
        end
        function obj = set.Ikf(obj,val)
            obj = setParameter(obj,'Ikf',val,0,'real');
        end
        function res = get.Ikf(obj)
            res = getParameter(obj,'Ikf');
        end
        function obj = set.Ise(obj,val)
            obj = setParameter(obj,'Ise',val,0,'real');
        end
        function res = get.Ise(obj)
            res = getParameter(obj,'Ise');
        end
        function obj = set.C2(obj,val)
            obj = setParameter(obj,'C2',val,0,'real');
        end
        function res = get.C2(obj)
            res = getParameter(obj,'C2');
        end
        function obj = set.Ne(obj,val)
            obj = setParameter(obj,'Ne',val,0,'real');
        end
        function res = get.Ne(obj)
            res = getParameter(obj,'Ne');
        end
        function obj = set.Br(obj,val)
            obj = setParameter(obj,'Br',val,0,'real');
        end
        function res = get.Br(obj)
            res = getParameter(obj,'Br');
        end
        function obj = set.Nr(obj,val)
            obj = setParameter(obj,'Nr',val,0,'real');
        end
        function res = get.Nr(obj)
            res = getParameter(obj,'Nr');
        end
        function obj = set.Var(obj,val)
            obj = setParameter(obj,'Var',val,0,'real');
        end
        function res = get.Var(obj)
            res = getParameter(obj,'Var');
        end
        function obj = set.Ikr(obj,val)
            obj = setParameter(obj,'Ikr',val,0,'real');
        end
        function res = get.Ikr(obj)
            res = getParameter(obj,'Ikr');
        end
        function obj = set.Isc(obj,val)
            obj = setParameter(obj,'Isc',val,0,'real');
        end
        function res = get.Isc(obj)
            res = getParameter(obj,'Isc');
        end
        function obj = set.C4(obj,val)
            obj = setParameter(obj,'C4',val,0,'real');
        end
        function res = get.C4(obj)
            res = getParameter(obj,'C4');
        end
        function obj = set.Nc(obj,val)
            obj = setParameter(obj,'Nc',val,0,'real');
        end
        function res = get.Nc(obj)
            res = getParameter(obj,'Nc');
        end
        function obj = set.Rb(obj,val)
            obj = setParameter(obj,'Rb',val,0,'real');
        end
        function res = get.Rb(obj)
            res = getParameter(obj,'Rb');
        end
        function obj = set.Irb(obj,val)
            obj = setParameter(obj,'Irb',val,0,'real');
        end
        function res = get.Irb(obj)
            res = getParameter(obj,'Irb');
        end
        function obj = set.Rbm(obj,val)
            obj = setParameter(obj,'Rbm',val,0,'real');
        end
        function res = get.Rbm(obj)
            res = getParameter(obj,'Rbm');
        end
        function obj = set.Rbnoi(obj,val)
            obj = setParameter(obj,'Rbnoi',val,0,'real');
        end
        function res = get.Rbnoi(obj)
            res = getParameter(obj,'Rbnoi');
        end
        function obj = set.Re(obj,val)
            obj = setParameter(obj,'Re',val,0,'real');
        end
        function res = get.Re(obj)
            res = getParameter(obj,'Re');
        end
        function obj = set.Rc(obj,val)
            obj = setParameter(obj,'Rc',val,0,'real');
        end
        function res = get.Rc(obj)
            res = getParameter(obj,'Rc');
        end
        function obj = set.Minr(obj,val)
            obj = setParameter(obj,'Minr',val,0,'real');
        end
        function res = get.Minr(obj)
            res = getParameter(obj,'Minr');
        end
        function obj = set.Imax(obj,val)
            obj = setParameter(obj,'Imax',val,0,'real');
        end
        function res = get.Imax(obj)
            res = getParameter(obj,'Imax');
        end
        function obj = set.Cje(obj,val)
            obj = setParameter(obj,'Cje',val,0,'real');
        end
        function res = get.Cje(obj)
            res = getParameter(obj,'Cje');
        end
        function obj = set.Vje(obj,val)
            obj = setParameter(obj,'Vje',val,0,'real');
        end
        function res = get.Vje(obj)
            res = getParameter(obj,'Vje');
        end
        function obj = set.Mje(obj,val)
            obj = setParameter(obj,'Mje',val,0,'real');
        end
        function res = get.Mje(obj)
            res = getParameter(obj,'Mje');
        end
        function obj = set.Cjc(obj,val)
            obj = setParameter(obj,'Cjc',val,0,'real');
        end
        function res = get.Cjc(obj)
            res = getParameter(obj,'Cjc');
        end
        function obj = set.Vjc(obj,val)
            obj = setParameter(obj,'Vjc',val,0,'real');
        end
        function res = get.Vjc(obj)
            res = getParameter(obj,'Vjc');
        end
        function obj = set.Mjc(obj,val)
            obj = setParameter(obj,'Mjc',val,0,'real');
        end
        function res = get.Mjc(obj)
            res = getParameter(obj,'Mjc');
        end
        function obj = set.Xcjc(obj,val)
            obj = setParameter(obj,'Xcjc',val,0,'real');
        end
        function res = get.Xcjc(obj)
            res = getParameter(obj,'Xcjc');
        end
        function obj = set.Cjs(obj,val)
            obj = setParameter(obj,'Cjs',val,0,'real');
        end
        function res = get.Cjs(obj)
            res = getParameter(obj,'Cjs');
        end
        function obj = set.Vjs(obj,val)
            obj = setParameter(obj,'Vjs',val,0,'real');
        end
        function res = get.Vjs(obj)
            res = getParameter(obj,'Vjs');
        end
        function obj = set.Mjs(obj,val)
            obj = setParameter(obj,'Mjs',val,0,'real');
        end
        function res = get.Mjs(obj)
            res = getParameter(obj,'Mjs');
        end
        function obj = set.Fc(obj,val)
            obj = setParameter(obj,'Fc',val,0,'real');
        end
        function res = get.Fc(obj)
            res = getParameter(obj,'Fc');
        end
        function obj = set.Tf(obj,val)
            obj = setParameter(obj,'Tf',val,0,'real');
        end
        function res = get.Tf(obj)
            res = getParameter(obj,'Tf');
        end
        function obj = set.Xtf(obj,val)
            obj = setParameter(obj,'Xtf',val,0,'real');
        end
        function res = get.Xtf(obj)
            res = getParameter(obj,'Xtf');
        end
        function obj = set.Vtf(obj,val)
            obj = setParameter(obj,'Vtf',val,0,'real');
        end
        function res = get.Vtf(obj)
            res = getParameter(obj,'Vtf');
        end
        function obj = set.Itf(obj,val)
            obj = setParameter(obj,'Itf',val,0,'real');
        end
        function res = get.Itf(obj)
            res = getParameter(obj,'Itf');
        end
        function obj = set.Ptf(obj,val)
            obj = setParameter(obj,'Ptf',val,0,'real');
        end
        function res = get.Ptf(obj)
            res = getParameter(obj,'Ptf');
        end
        function obj = set.Tr(obj,val)
            obj = setParameter(obj,'Tr',val,0,'real');
        end
        function res = get.Tr(obj)
            res = getParameter(obj,'Tr');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Kb(obj,val)
            obj = setParameter(obj,'Kb',val,0,'real');
        end
        function res = get.Kb(obj)
            res = getParameter(obj,'Kb');
        end
        function obj = set.Ab(obj,val)
            obj = setParameter(obj,'Ab',val,0,'real');
        end
        function res = get.Ab(obj)
            res = getParameter(obj,'Ab');
        end
        function obj = set.Fb(obj,val)
            obj = setParameter(obj,'Fb',val,0,'real');
        end
        function res = get.Fb(obj)
            res = getParameter(obj,'Fb');
        end
        function obj = set.Iss(obj,val)
            obj = setParameter(obj,'Iss',val,0,'real');
        end
        function res = get.Iss(obj)
            res = getParameter(obj,'Iss');
        end
        function obj = set.Ns(obj,val)
            obj = setParameter(obj,'Ns',val,0,'real');
        end
        function res = get.Ns(obj)
            res = getParameter(obj,'Ns');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
        function obj = set.Nk(obj,val)
            obj = setParameter(obj,'Nk',val,0,'real');
        end
        function res = get.Nk(obj)
            res = getParameter(obj,'Nk');
        end
        function obj = set.Ffe(obj,val)
            obj = setParameter(obj,'Ffe',val,0,'real');
        end
        function res = get.Ffe(obj)
            res = getParameter(obj,'Ffe');
        end
        function obj = set.Lateral(obj,val)
            obj = setParameter(obj,'Lateral',val,0,'boolean');
        end
        function res = get.Lateral(obj)
            res = getParameter(obj,'Lateral');
        end
        function obj = set.RbModel(obj,val)
            obj = setParameter(obj,'RbModel',val,0,'boolean');
        end
        function res = get.RbModel(obj)
            res = getParameter(obj,'RbModel');
        end
        function obj = set.Approxqb(obj,val)
            obj = setParameter(obj,'Approxqb',val,0,'boolean');
        end
        function res = get.Approxqb(obj)
            res = getParameter(obj,'Approxqb');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Tlev(obj,val)
            obj = setParameter(obj,'Tlev',val,0,'integer');
        end
        function res = get.Tlev(obj)
            res = getParameter(obj,'Tlev');
        end
        function obj = set.Tlevc(obj,val)
            obj = setParameter(obj,'Tlevc',val,0,'integer');
        end
        function res = get.Tlevc(obj)
            res = getParameter(obj,'Tlevc');
        end
        function obj = set.Eg(obj,val)
            obj = setParameter(obj,'Eg',val,0,'real');
        end
        function res = get.Eg(obj)
            res = getParameter(obj,'Eg');
        end
        function obj = set.EgAlpha(obj,val)
            obj = setParameter(obj,'EgAlpha',val,0,'real');
        end
        function res = get.EgAlpha(obj)
            res = getParameter(obj,'EgAlpha');
        end
        function obj = set.EgBeta(obj,val)
            obj = setParameter(obj,'EgBeta',val,0,'real');
        end
        function res = get.EgBeta(obj)
            res = getParameter(obj,'EgBeta');
        end
        function obj = set.Tbf1(obj,val)
            obj = setParameter(obj,'Tbf1',val,0,'real');
        end
        function res = get.Tbf1(obj)
            res = getParameter(obj,'Tbf1');
        end
        function obj = set.Tbf2(obj,val)
            obj = setParameter(obj,'Tbf2',val,0,'real');
        end
        function res = get.Tbf2(obj)
            res = getParameter(obj,'Tbf2');
        end
        function obj = set.Tbr1(obj,val)
            obj = setParameter(obj,'Tbr1',val,0,'real');
        end
        function res = get.Tbr1(obj)
            res = getParameter(obj,'Tbr1');
        end
        function obj = set.Tbr2(obj,val)
            obj = setParameter(obj,'Tbr2',val,0,'real');
        end
        function res = get.Tbr2(obj)
            res = getParameter(obj,'Tbr2');
        end
        function obj = set.Tcbc(obj,val)
            obj = setParameter(obj,'Tcbc',val,0,'real');
        end
        function res = get.Tcbc(obj)
            res = getParameter(obj,'Tcbc');
        end
        function obj = set.Tcbe(obj,val)
            obj = setParameter(obj,'Tcbe',val,0,'real');
        end
        function res = get.Tcbe(obj)
            res = getParameter(obj,'Tcbe');
        end
        function obj = set.Tccs(obj,val)
            obj = setParameter(obj,'Tccs',val,0,'real');
        end
        function res = get.Tccs(obj)
            res = getParameter(obj,'Tccs');
        end
        function obj = set.Tikf1(obj,val)
            obj = setParameter(obj,'Tikf1',val,0,'real');
        end
        function res = get.Tikf1(obj)
            res = getParameter(obj,'Tikf1');
        end
        function obj = set.Tikf2(obj,val)
            obj = setParameter(obj,'Tikf2',val,0,'real');
        end
        function res = get.Tikf2(obj)
            res = getParameter(obj,'Tikf2');
        end
        function obj = set.Tikr1(obj,val)
            obj = setParameter(obj,'Tikr1',val,0,'real');
        end
        function res = get.Tikr1(obj)
            res = getParameter(obj,'Tikr1');
        end
        function obj = set.Tikr2(obj,val)
            obj = setParameter(obj,'Tikr2',val,0,'real');
        end
        function res = get.Tikr2(obj)
            res = getParameter(obj,'Tikr2');
        end
        function obj = set.Tirb1(obj,val)
            obj = setParameter(obj,'Tirb1',val,0,'real');
        end
        function res = get.Tirb1(obj)
            res = getParameter(obj,'Tirb1');
        end
        function obj = set.Tirb2(obj,val)
            obj = setParameter(obj,'Tirb2',val,0,'real');
        end
        function res = get.Tirb2(obj)
            res = getParameter(obj,'Tirb2');
        end
        function obj = set.Tis1(obj,val)
            obj = setParameter(obj,'Tis1',val,0,'real');
        end
        function res = get.Tis1(obj)
            res = getParameter(obj,'Tis1');
        end
        function obj = set.Tis2(obj,val)
            obj = setParameter(obj,'Tis2',val,0,'real');
        end
        function res = get.Tis2(obj)
            res = getParameter(obj,'Tis2');
        end
        function obj = set.Tisc1(obj,val)
            obj = setParameter(obj,'Tisc1',val,0,'real');
        end
        function res = get.Tisc1(obj)
            res = getParameter(obj,'Tisc1');
        end
        function obj = set.Tisc2(obj,val)
            obj = setParameter(obj,'Tisc2',val,0,'real');
        end
        function res = get.Tisc2(obj)
            res = getParameter(obj,'Tisc2');
        end
        function obj = set.Tise1(obj,val)
            obj = setParameter(obj,'Tise1',val,0,'real');
        end
        function res = get.Tise1(obj)
            res = getParameter(obj,'Tise1');
        end
        function obj = set.Tise2(obj,val)
            obj = setParameter(obj,'Tise2',val,0,'real');
        end
        function res = get.Tise2(obj)
            res = getParameter(obj,'Tise2');
        end
        function obj = set.Tiss1(obj,val)
            obj = setParameter(obj,'Tiss1',val,0,'real');
        end
        function res = get.Tiss1(obj)
            res = getParameter(obj,'Tiss1');
        end
        function obj = set.Tiss2(obj,val)
            obj = setParameter(obj,'Tiss2',val,0,'real');
        end
        function res = get.Tiss2(obj)
            res = getParameter(obj,'Tiss2');
        end
        function obj = set.Titf1(obj,val)
            obj = setParameter(obj,'Titf1',val,0,'real');
        end
        function res = get.Titf1(obj)
            res = getParameter(obj,'Titf1');
        end
        function obj = set.Titf2(obj,val)
            obj = setParameter(obj,'Titf2',val,0,'real');
        end
        function res = get.Titf2(obj)
            res = getParameter(obj,'Titf2');
        end
        function obj = set.Tvtf1(obj,val)
            obj = setParameter(obj,'Tvtf1',val,0,'real');
        end
        function res = get.Tvtf1(obj)
            res = getParameter(obj,'Tvtf1');
        end
        function obj = set.Tvtf2(obj,val)
            obj = setParameter(obj,'Tvtf2',val,0,'real');
        end
        function res = get.Tvtf2(obj)
            res = getParameter(obj,'Tvtf2');
        end
        function obj = set.Txtf1(obj,val)
            obj = setParameter(obj,'Txtf1',val,0,'real');
        end
        function res = get.Txtf1(obj)
            res = getParameter(obj,'Txtf1');
        end
        function obj = set.Txtf2(obj,val)
            obj = setParameter(obj,'Txtf2',val,0,'real');
        end
        function res = get.Txtf2(obj)
            res = getParameter(obj,'Txtf2');
        end
        function obj = set.Tmjc1(obj,val)
            obj = setParameter(obj,'Tmjc1',val,0,'real');
        end
        function res = get.Tmjc1(obj)
            res = getParameter(obj,'Tmjc1');
        end
        function obj = set.Tmjc2(obj,val)
            obj = setParameter(obj,'Tmjc2',val,0,'real');
        end
        function res = get.Tmjc2(obj)
            res = getParameter(obj,'Tmjc2');
        end
        function obj = set.Tmje1(obj,val)
            obj = setParameter(obj,'Tmje1',val,0,'real');
        end
        function res = get.Tmje1(obj)
            res = getParameter(obj,'Tmje1');
        end
        function obj = set.Tmje2(obj,val)
            obj = setParameter(obj,'Tmje2',val,0,'real');
        end
        function res = get.Tmje2(obj)
            res = getParameter(obj,'Tmje2');
        end
        function obj = set.Tmjs1(obj,val)
            obj = setParameter(obj,'Tmjs1',val,0,'real');
        end
        function res = get.Tmjs1(obj)
            res = getParameter(obj,'Tmjs1');
        end
        function obj = set.Tmjs2(obj,val)
            obj = setParameter(obj,'Tmjs2',val,0,'real');
        end
        function res = get.Tmjs2(obj)
            res = getParameter(obj,'Tmjs2');
        end
        function obj = set.Tnc1(obj,val)
            obj = setParameter(obj,'Tnc1',val,0,'real');
        end
        function res = get.Tnc1(obj)
            res = getParameter(obj,'Tnc1');
        end
        function obj = set.Tnc2(obj,val)
            obj = setParameter(obj,'Tnc2',val,0,'real');
        end
        function res = get.Tnc2(obj)
            res = getParameter(obj,'Tnc2');
        end
        function obj = set.Tne1(obj,val)
            obj = setParameter(obj,'Tne1',val,0,'real');
        end
        function res = get.Tne1(obj)
            res = getParameter(obj,'Tne1');
        end
        function obj = set.Tne2(obj,val)
            obj = setParameter(obj,'Tne2',val,0,'real');
        end
        function res = get.Tne2(obj)
            res = getParameter(obj,'Tne2');
        end
        function obj = set.Tnf1(obj,val)
            obj = setParameter(obj,'Tnf1',val,0,'real');
        end
        function res = get.Tnf1(obj)
            res = getParameter(obj,'Tnf1');
        end
        function obj = set.Tnf2(obj,val)
            obj = setParameter(obj,'Tnf2',val,0,'real');
        end
        function res = get.Tnf2(obj)
            res = getParameter(obj,'Tnf2');
        end
        function obj = set.Tnr1(obj,val)
            obj = setParameter(obj,'Tnr1',val,0,'real');
        end
        function res = get.Tnr1(obj)
            res = getParameter(obj,'Tnr1');
        end
        function obj = set.Tnr2(obj,val)
            obj = setParameter(obj,'Tnr2',val,0,'real');
        end
        function res = get.Tnr2(obj)
            res = getParameter(obj,'Tnr2');
        end
        function obj = set.Tns1(obj,val)
            obj = setParameter(obj,'Tns1',val,0,'real');
        end
        function res = get.Tns1(obj)
            res = getParameter(obj,'Tns1');
        end
        function obj = set.Tns2(obj,val)
            obj = setParameter(obj,'Tns2',val,0,'real');
        end
        function res = get.Tns2(obj)
            res = getParameter(obj,'Tns2');
        end
        function obj = set.Trb1(obj,val)
            obj = setParameter(obj,'Trb1',val,0,'real');
        end
        function res = get.Trb1(obj)
            res = getParameter(obj,'Trb1');
        end
        function obj = set.Trb2(obj,val)
            obj = setParameter(obj,'Trb2',val,0,'real');
        end
        function res = get.Trb2(obj)
            res = getParameter(obj,'Trb2');
        end
        function obj = set.Trc1(obj,val)
            obj = setParameter(obj,'Trc1',val,0,'real');
        end
        function res = get.Trc1(obj)
            res = getParameter(obj,'Trc1');
        end
        function obj = set.Trc2(obj,val)
            obj = setParameter(obj,'Trc2',val,0,'real');
        end
        function res = get.Trc2(obj)
            res = getParameter(obj,'Trc2');
        end
        function obj = set.Tre1(obj,val)
            obj = setParameter(obj,'Tre1',val,0,'real');
        end
        function res = get.Tre1(obj)
            res = getParameter(obj,'Tre1');
        end
        function obj = set.Tre2(obj,val)
            obj = setParameter(obj,'Tre2',val,0,'real');
        end
        function res = get.Tre2(obj)
            res = getParameter(obj,'Tre2');
        end
        function obj = set.Trm1(obj,val)
            obj = setParameter(obj,'Trm1',val,0,'real');
        end
        function res = get.Trm1(obj)
            res = getParameter(obj,'Trm1');
        end
        function obj = set.Trm2(obj,val)
            obj = setParameter(obj,'Trm2',val,0,'real');
        end
        function res = get.Trm2(obj)
            res = getParameter(obj,'Trm2');
        end
        function obj = set.Ttf1(obj,val)
            obj = setParameter(obj,'Ttf1',val,0,'real');
        end
        function res = get.Ttf1(obj)
            res = getParameter(obj,'Ttf1');
        end
        function obj = set.Ttf2(obj,val)
            obj = setParameter(obj,'Ttf2',val,0,'real');
        end
        function res = get.Ttf2(obj)
            res = getParameter(obj,'Ttf2');
        end
        function obj = set.Ttr1(obj,val)
            obj = setParameter(obj,'Ttr1',val,0,'real');
        end
        function res = get.Ttr1(obj)
            res = getParameter(obj,'Ttr1');
        end
        function obj = set.Ttr2(obj,val)
            obj = setParameter(obj,'Ttr2',val,0,'real');
        end
        function res = get.Ttr2(obj)
            res = getParameter(obj,'Ttr2');
        end
        function obj = set.Tvaf1(obj,val)
            obj = setParameter(obj,'Tvaf1',val,0,'real');
        end
        function res = get.Tvaf1(obj)
            res = getParameter(obj,'Tvaf1');
        end
        function obj = set.Tvaf2(obj,val)
            obj = setParameter(obj,'Tvaf2',val,0,'real');
        end
        function res = get.Tvaf2(obj)
            res = getParameter(obj,'Tvaf2');
        end
        function obj = set.Tvar1(obj,val)
            obj = setParameter(obj,'Tvar1',val,0,'real');
        end
        function res = get.Tvar1(obj)
            res = getParameter(obj,'Tvar1');
        end
        function obj = set.Tvar2(obj,val)
            obj = setParameter(obj,'Tvar2',val,0,'real');
        end
        function res = get.Tvar2(obj)
            res = getParameter(obj,'Tvar2');
        end
        function obj = set.Tvjc(obj,val)
            obj = setParameter(obj,'Tvjc',val,0,'real');
        end
        function res = get.Tvjc(obj)
            res = getParameter(obj,'Tvjc');
        end
        function obj = set.Tvje(obj,val)
            obj = setParameter(obj,'Tvje',val,0,'real');
        end
        function res = get.Tvje(obj)
            res = getParameter(obj,'Tvje');
        end
        function obj = set.Tvjs(obj,val)
            obj = setParameter(obj,'Tvjs',val,0,'real');
        end
        function res = get.Tvjs(obj)
            res = getParameter(obj,'Tvjs');
        end
        function obj = set.Tcbo(obj,val)
            obj = setParameter(obj,'Tcbo',val,0,'real');
        end
        function res = get.Tcbo(obj)
            res = getParameter(obj,'Tcbo');
        end
        function obj = set.Tgbo(obj,val)
            obj = setParameter(obj,'Tgbo',val,0,'real');
        end
        function res = get.Tgbo(obj)
            res = getParameter(obj,'Tgbo');
        end
        function obj = set.Xtb(obj,val)
            obj = setParameter(obj,'Xtb',val,0,'real');
        end
        function res = get.Xtb(obj)
            res = getParameter(obj,'Xtb');
        end
        function obj = set.Xti(obj,val)
            obj = setParameter(obj,'Xti',val,0,'real');
        end
        function res = get.Xti(obj)
            res = getParameter(obj,'Xti');
        end
        function obj = set.Imelt(obj,val)
            obj = setParameter(obj,'Imelt',val,0,'real');
        end
        function res = get.Imelt(obj)
            res = getParameter(obj,'Imelt');
        end
        function obj = set.Ke(obj,val)
            obj = setParameter(obj,'Ke',val,0,'real');
        end
        function res = get.Ke(obj)
            res = getParameter(obj,'Ke');
        end
        function obj = set.Kc(obj,val)
            obj = setParameter(obj,'Kc',val,0,'real');
        end
        function res = get.Kc(obj)
            res = getParameter(obj,'Kc');
        end
        function obj = set.Rcv(obj,val)
            obj = setParameter(obj,'Rcv',val,0,'real');
        end
        function res = get.Rcv(obj)
            res = getParameter(obj,'Rcv');
        end
        function obj = set.Rcm(obj,val)
            obj = setParameter(obj,'Rcm',val,0,'real');
        end
        function res = get.Rcm(obj)
            res = getParameter(obj,'Rcm');
        end
        function obj = set.Dope(obj,val)
            obj = setParameter(obj,'Dope',val,0,'real');
        end
        function res = get.Dope(obj)
            res = getParameter(obj,'Dope');
        end
        function obj = set.Cex(obj,val)
            obj = setParameter(obj,'Cex',val,0,'real');
        end
        function res = get.Cex(obj)
            res = getParameter(obj,'Cex');
        end
        function obj = set.Cco(obj,val)
            obj = setParameter(obj,'Cco',val,0,'real');
        end
        function res = get.Cco(obj)
            res = getParameter(obj,'Cco');
        end
        function obj = set.Cbo(obj,val)
            obj = setParameter(obj,'Cbo',val,0,'real');
        end
        function res = get.Cbo(obj)
            res = getParameter(obj,'Cbo');
        end
        function obj = set.Gbo(obj,val)
            obj = setParameter(obj,'Gbo',val,0,'real');
        end
        function res = get.Gbo(obj)
            res = getParameter(obj,'Gbo');
        end
        function obj = set.Vbo(obj,val)
            obj = setParameter(obj,'Vbo',val,0,'real');
        end
        function res = get.Vbo(obj)
            res = getParameter(obj,'Vbo');
        end
        function obj = set.Xcjc2(obj,val)
            obj = setParameter(obj,'Xcjc2',val,0,'real');
        end
        function res = get.Xcjc2(obj)
            res = getParameter(obj,'Xcjc2');
        end
        function obj = set.Td(obj,val)
            obj = setParameter(obj,'Td',val,0,'real');
        end
        function res = get.Td(obj)
            res = getParameter(obj,'Td');
        end
        function obj = set.wVsubfwd(obj,val)
            obj = setParameter(obj,'wVsubfwd',val,0,'real');
        end
        function res = get.wVsubfwd(obj)
            res = getParameter(obj,'wVsubfwd');
        end
        function obj = set.wBvsub(obj,val)
            obj = setParameter(obj,'wBvsub',val,0,'real');
        end
        function res = get.wBvsub(obj)
            res = getParameter(obj,'wBvsub');
        end
        function obj = set.wBvbe(obj,val)
            obj = setParameter(obj,'wBvbe',val,0,'real');
        end
        function res = get.wBvbe(obj)
            res = getParameter(obj,'wBvbe');
        end
        function obj = set.wBvbc(obj,val)
            obj = setParameter(obj,'wBvbc',val,0,'real');
        end
        function res = get.wBvbc(obj)
            res = getParameter(obj,'wBvbc');
        end
        function obj = set.wBvce(obj,val)
            obj = setParameter(obj,'wBvce',val,0,'real');
        end
        function res = get.wBvce(obj)
            res = getParameter(obj,'wBvce');
        end
        function obj = set.wVbcfwd(obj,val)
            obj = setParameter(obj,'wVbcfwd',val,0,'real');
        end
        function res = get.wVbcfwd(obj)
            res = getParameter(obj,'wVbcfwd');
        end
        function obj = set.wIbmax(obj,val)
            obj = setParameter(obj,'wIbmax',val,0,'real');
        end
        function res = get.wIbmax(obj)
            res = getParameter(obj,'wIbmax');
        end
        function obj = set.wIcmax(obj,val)
            obj = setParameter(obj,'wIcmax',val,0,'real');
        end
        function res = get.wIcmax(obj)
            res = getParameter(obj,'wIcmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.SpectreCompatible(obj,val)
            obj = setParameter(obj,'SpectreCompatible',val,0,'integer');
        end
        function res = get.SpectreCompatible(obj)
            res = getParameter(obj,'SpectreCompatible');
        end
    end
end
