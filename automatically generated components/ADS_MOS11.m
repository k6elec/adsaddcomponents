classdef ADS_MOS11 < ADScomponent
    % ADS_MOS11 matlab representation for the ADS MOS11 component
    % MOS MODEL 11
    % ModelName [:Name] drain gate source bulk
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Number of devices in parallel (smorr) 
        Mult
        % Channel length (smorr) Unit: m
        L
        % Channel width (smorr) Unit: m
        W
        % Ambient device temperature (smorr) Unit: deg C
        Ta
        % Temperature offset of the device with respect to Ta (smorr) Unit: K
        Dta
        % Nonlinear spectral model on/off (s---i) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % Small signal Vds to Id transconductance (---rr) Unit: S
        gId_ds
        % Small signal Vgs to Id transconductance (---rr) Unit: S
        gId_gs
        % Small signal Vsb to Id transconductance (---rr) Unit: S
        gId_sb
        % Small signal Vds to Ib transconductance (---rr) Unit: S
        gIb_ds
        % Small signal Vgs to Ib transconductance (---rr) Unit: S
        gIb_gs
        % Small signal Vsb to Ib transconductance (---rr) Unit: S
        gIb_sb
        % Small signal Vds to Is transconductance (---rr) Unit: S
        gIs_ds
        % Small signal Vgs to Is transconductance (---rr) Unit: S
        gIs_gs
        % Small signal Vsb to Is transconductance (---rr) Unit: S
        gIs_sb
        % Small signal gate capacitance cg_ds (---rr) Unit: F
        Cg_ds
        % Small signal gate capacitance cg_gs (---rr) Unit: F
        Cg_gs
        % Small signal gate capacitance cg_sb (---rr) Unit: F
        Cg_sb
        % Small signal bulk capacitance cb_ds (---rr) Unit: F
        Cb_ds
        % Small signal bulk capacitance cb_gs (---rr) Unit: F
        Cb_gs
        % Small signal bulk capacitance cb_sb (---rr) Unit: F
        Cb_sb
        % Small signal source capacitance cs_ds (---rr) Unit: F
        Cs_ds
        % Small signal source capacitance cs_gs (---rr) Unit: F
        Cs_gs
        % Small signal source capacitance cs_sb (---rr) Unit: F
        Cs_sb
        % Small signal drain capacitance cd_ds (---rr) Unit: F
        Cd_ds
        % Small signal drain capacitance cd_gs (---rr) Unit: F
        Cd_gs
        % Small signal drain capacitance cd_sb (---rr) Unit: F
        Cd_sb
        % Gate charge (---rr) Unit: C
        Qg
        % Drain charge (---rr) Unit: C
        Qd
        % Bulk charge (---rr) Unit: C
        Qb
        % NMOS type MOS11 (---rb) 
        NMOS
        % PMOS type MOS11 (---rb) 
        PMOS
    end
    methods
        function obj = set.Mult(obj,val)
            obj = setParameter(obj,'Mult',val,0,'real');
        end
        function res = get.Mult(obj)
            res = getParameter(obj,'Mult');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Ta(obj,val)
            obj = setParameter(obj,'Ta',val,0,'real');
        end
        function res = get.Ta(obj)
            res = getParameter(obj,'Ta');
        end
        function obj = set.Dta(obj,val)
            obj = setParameter(obj,'Dta',val,0,'real');
        end
        function res = get.Dta(obj)
            res = getParameter(obj,'Dta');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.gId_ds(obj,val)
            obj = setParameter(obj,'gId_ds',val,0,'real');
        end
        function res = get.gId_ds(obj)
            res = getParameter(obj,'gId_ds');
        end
        function obj = set.gId_gs(obj,val)
            obj = setParameter(obj,'gId_gs',val,0,'real');
        end
        function res = get.gId_gs(obj)
            res = getParameter(obj,'gId_gs');
        end
        function obj = set.gId_sb(obj,val)
            obj = setParameter(obj,'gId_sb',val,0,'real');
        end
        function res = get.gId_sb(obj)
            res = getParameter(obj,'gId_sb');
        end
        function obj = set.gIb_ds(obj,val)
            obj = setParameter(obj,'gIb_ds',val,0,'real');
        end
        function res = get.gIb_ds(obj)
            res = getParameter(obj,'gIb_ds');
        end
        function obj = set.gIb_gs(obj,val)
            obj = setParameter(obj,'gIb_gs',val,0,'real');
        end
        function res = get.gIb_gs(obj)
            res = getParameter(obj,'gIb_gs');
        end
        function obj = set.gIb_sb(obj,val)
            obj = setParameter(obj,'gIb_sb',val,0,'real');
        end
        function res = get.gIb_sb(obj)
            res = getParameter(obj,'gIb_sb');
        end
        function obj = set.gIs_ds(obj,val)
            obj = setParameter(obj,'gIs_ds',val,0,'real');
        end
        function res = get.gIs_ds(obj)
            res = getParameter(obj,'gIs_ds');
        end
        function obj = set.gIs_gs(obj,val)
            obj = setParameter(obj,'gIs_gs',val,0,'real');
        end
        function res = get.gIs_gs(obj)
            res = getParameter(obj,'gIs_gs');
        end
        function obj = set.gIs_sb(obj,val)
            obj = setParameter(obj,'gIs_sb',val,0,'real');
        end
        function res = get.gIs_sb(obj)
            res = getParameter(obj,'gIs_sb');
        end
        function obj = set.Cg_ds(obj,val)
            obj = setParameter(obj,'Cg_ds',val,0,'real');
        end
        function res = get.Cg_ds(obj)
            res = getParameter(obj,'Cg_ds');
        end
        function obj = set.Cg_gs(obj,val)
            obj = setParameter(obj,'Cg_gs',val,0,'real');
        end
        function res = get.Cg_gs(obj)
            res = getParameter(obj,'Cg_gs');
        end
        function obj = set.Cg_sb(obj,val)
            obj = setParameter(obj,'Cg_sb',val,0,'real');
        end
        function res = get.Cg_sb(obj)
            res = getParameter(obj,'Cg_sb');
        end
        function obj = set.Cb_ds(obj,val)
            obj = setParameter(obj,'Cb_ds',val,0,'real');
        end
        function res = get.Cb_ds(obj)
            res = getParameter(obj,'Cb_ds');
        end
        function obj = set.Cb_gs(obj,val)
            obj = setParameter(obj,'Cb_gs',val,0,'real');
        end
        function res = get.Cb_gs(obj)
            res = getParameter(obj,'Cb_gs');
        end
        function obj = set.Cb_sb(obj,val)
            obj = setParameter(obj,'Cb_sb',val,0,'real');
        end
        function res = get.Cb_sb(obj)
            res = getParameter(obj,'Cb_sb');
        end
        function obj = set.Cs_ds(obj,val)
            obj = setParameter(obj,'Cs_ds',val,0,'real');
        end
        function res = get.Cs_ds(obj)
            res = getParameter(obj,'Cs_ds');
        end
        function obj = set.Cs_gs(obj,val)
            obj = setParameter(obj,'Cs_gs',val,0,'real');
        end
        function res = get.Cs_gs(obj)
            res = getParameter(obj,'Cs_gs');
        end
        function obj = set.Cs_sb(obj,val)
            obj = setParameter(obj,'Cs_sb',val,0,'real');
        end
        function res = get.Cs_sb(obj)
            res = getParameter(obj,'Cs_sb');
        end
        function obj = set.Cd_ds(obj,val)
            obj = setParameter(obj,'Cd_ds',val,0,'real');
        end
        function res = get.Cd_ds(obj)
            res = getParameter(obj,'Cd_ds');
        end
        function obj = set.Cd_gs(obj,val)
            obj = setParameter(obj,'Cd_gs',val,0,'real');
        end
        function res = get.Cd_gs(obj)
            res = getParameter(obj,'Cd_gs');
        end
        function obj = set.Cd_sb(obj,val)
            obj = setParameter(obj,'Cd_sb',val,0,'real');
        end
        function res = get.Cd_sb(obj)
            res = getParameter(obj,'Cd_sb');
        end
        function obj = set.Qg(obj,val)
            obj = setParameter(obj,'Qg',val,0,'real');
        end
        function res = get.Qg(obj)
            res = getParameter(obj,'Qg');
        end
        function obj = set.Qd(obj,val)
            obj = setParameter(obj,'Qd',val,0,'real');
        end
        function res = get.Qd(obj)
            res = getParameter(obj,'Qd');
        end
        function obj = set.Qb(obj,val)
            obj = setParameter(obj,'Qb',val,0,'real');
        end
        function res = get.Qb(obj)
            res = getParameter(obj,'Qb');
        end
        function obj = set.NMOS(obj,val)
            obj = setParameter(obj,'NMOS',val,0,'boolean');
        end
        function res = get.NMOS(obj)
            res = getParameter(obj,'NMOS');
        end
        function obj = set.PMOS(obj,val)
            obj = setParameter(obj,'PMOS',val,0,'boolean');
        end
        function res = get.PMOS(obj)
            res = getParameter(obj,'PMOS');
        end
    end
end
