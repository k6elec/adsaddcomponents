classdef ADS_L_Model_Model < ADSmodel
    % ADS_L_Model_Model matlab representation for the ADS L_Model_Model component
    % Linear Two Terminal Inductor with Model model
    % model ModelName L_Model ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Inductance (smorr) Unit: H
        L
        % Series Resistance (smorr) Unit: Ohms
        R
        % Nominal temperature of inductor in degrees Celsius (smorr) Unit: deg C
        Tnom
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Temperature coefficient; per degree Celsius (smorr) Unit: 1/deg C
        TC1
        % Temperature coefficient; per degree Celsius squared (smorr) Unit: 1/(deg C)^2
        TC2
        % Inductance scaling factor (smorr) 
        Scale
        % Nonlinear inductor polynomial coeffs, syntax: Coeffs=list(c1,c2,...) (smorr) 
        Coeffs
        % Flicker noise coefficient (smorr) 
        Kf
        % Flicker noise exponent (smorr) 
        Af
    end
    methods
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.R(obj,val)
            obj = setParameter(obj,'R',val,0,'real');
        end
        function res = get.R(obj)
            res = getParameter(obj,'R');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.TC1(obj,val)
            obj = setParameter(obj,'TC1',val,0,'real');
        end
        function res = get.TC1(obj)
            res = getParameter(obj,'TC1');
        end
        function obj = set.TC2(obj,val)
            obj = setParameter(obj,'TC2',val,0,'real');
        end
        function res = get.TC2(obj)
            res = getParameter(obj,'TC2');
        end
        function obj = set.Scale(obj,val)
            obj = setParameter(obj,'Scale',val,0,'real');
        end
        function res = get.Scale(obj)
            res = getParameter(obj,'Scale');
        end
        function obj = set.Coeffs(obj,val)
            obj = setParameter(obj,'Coeffs',val,0,'real');
        end
        function res = get.Coeffs(obj)
            res = getParameter(obj,'Coeffs');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
    end
end
