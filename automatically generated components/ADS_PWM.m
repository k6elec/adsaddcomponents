classdef ADS_PWM < ADScomponent
    % ADS_PWM matlab representation for the ADS PWM component
    % Pulse Width Modulator
    % PWM [:Name] outp outn controlp controln
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Period of PWM (S--rr) Unit: s
        Period
        % Max control voltage of PWM (Smorr) Unit: V
        VcMax
        % Min control voltage of PWM (Smorr) Unit: V
        VcMin
        % Max output voltage of PWM (Smorr) Unit: V
        VoMax
        % Min output voltage of PWM (Smorr) Unit: V
        VoMin
        % Min duty cycle of PWM as a ratio of period (smorr) 
        DutyMin
        % Max duty cycle of PWM as a ratio of period (smorr) 
        DutyMax
        % Rise/fall time of PWM (smorr) Unit: s
        tedge
        % Delay time of PWM (s--rr) Unit: s
        tdelay
        % Dead time of PWM in each cycle (smorr) Unit: s
        tdead
        % Inverse control voltage (sm-ri) 
        InvCtrl
        % Inverse output pulses (sm-ri) 
        InvOut
        % Average control voltage over the past periods (s--ri) 
        AvgVcon
        % Number of harmonics to be included in the control voltage (s--ri) 
        nHarms
        % Polarity of PWM output (s--rs) 
        Polar
    end
    methods
        function obj = set.Period(obj,val)
            obj = setParameter(obj,'Period',val,0,'real');
        end
        function res = get.Period(obj)
            res = getParameter(obj,'Period');
        end
        function obj = set.VcMax(obj,val)
            obj = setParameter(obj,'VcMax',val,0,'real');
        end
        function res = get.VcMax(obj)
            res = getParameter(obj,'VcMax');
        end
        function obj = set.VcMin(obj,val)
            obj = setParameter(obj,'VcMin',val,0,'real');
        end
        function res = get.VcMin(obj)
            res = getParameter(obj,'VcMin');
        end
        function obj = set.VoMax(obj,val)
            obj = setParameter(obj,'VoMax',val,0,'real');
        end
        function res = get.VoMax(obj)
            res = getParameter(obj,'VoMax');
        end
        function obj = set.VoMin(obj,val)
            obj = setParameter(obj,'VoMin',val,0,'real');
        end
        function res = get.VoMin(obj)
            res = getParameter(obj,'VoMin');
        end
        function obj = set.DutyMin(obj,val)
            obj = setParameter(obj,'DutyMin',val,0,'real');
        end
        function res = get.DutyMin(obj)
            res = getParameter(obj,'DutyMin');
        end
        function obj = set.DutyMax(obj,val)
            obj = setParameter(obj,'DutyMax',val,0,'real');
        end
        function res = get.DutyMax(obj)
            res = getParameter(obj,'DutyMax');
        end
        function obj = set.tedge(obj,val)
            obj = setParameter(obj,'tedge',val,0,'real');
        end
        function res = get.tedge(obj)
            res = getParameter(obj,'tedge');
        end
        function obj = set.tdelay(obj,val)
            obj = setParameter(obj,'tdelay',val,0,'real');
        end
        function res = get.tdelay(obj)
            res = getParameter(obj,'tdelay');
        end
        function obj = set.tdead(obj,val)
            obj = setParameter(obj,'tdead',val,0,'real');
        end
        function res = get.tdead(obj)
            res = getParameter(obj,'tdead');
        end
        function obj = set.InvCtrl(obj,val)
            obj = setParameter(obj,'InvCtrl',val,0,'integer');
        end
        function res = get.InvCtrl(obj)
            res = getParameter(obj,'InvCtrl');
        end
        function obj = set.InvOut(obj,val)
            obj = setParameter(obj,'InvOut',val,0,'integer');
        end
        function res = get.InvOut(obj)
            res = getParameter(obj,'InvOut');
        end
        function obj = set.AvgVcon(obj,val)
            obj = setParameter(obj,'AvgVcon',val,0,'integer');
        end
        function res = get.AvgVcon(obj)
            res = getParameter(obj,'AvgVcon');
        end
        function obj = set.nHarms(obj,val)
            obj = setParameter(obj,'nHarms',val,0,'integer');
        end
        function res = get.nHarms(obj)
            res = getParameter(obj,'nHarms');
        end
        function obj = set.Polar(obj,val)
            obj = setParameter(obj,'Polar',val,0,'string');
        end
        function res = get.Polar(obj)
            res = getParameter(obj,'Polar');
        end
    end
end
