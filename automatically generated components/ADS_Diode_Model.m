classdef ADS_Diode_Model < ADSmodel
    % ADS_Diode_Model matlab representation for the ADS Diode_Model component
    % Junction Diode model
    % model ModelName Diode ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Diode model level 1=standard 3=Hspice geometry 11=Spectre geometry (s--ri) 
        Level
        % Saturation current (smorr) Unit: A
        Is
        % Series resistance (smorr) Unit: Ohms
        Rs
        % Minimum value of parasitic resistances (smorr) Unit: Ohms
        Minr
        % Emission coefficient (smorr) 
        N
        % Transit time (smorr) Unit: s
        Tt
        % Zero-bias junction capacitance (smorr) Unit: F
        Cjo
        % Junction potential (smorr) Unit: V
        Vj
        % Grading coefficient (smorr) 
        M
        % Forward-bias depletion capacitance threshold (smorr) 
        Fc
        % Explosion current (smorr) Unit: A
        Imax
        % Recombination current parameter (smorr) Unit: A
        Isr
        % Emission coefficient of recombination current (smorr) 
        Nr
        % High injection knee current (smorr) Unit: A
        Ikf
        % Reverse high injection knee current (smorr) Unit: A
        Ikr
        % Ikf model: ADS/Libra/Pspice=1 Hspice=2 (s--ri) 
        IkModel
        % Reverse breakdown voltage (smorr) Unit: V
        Bv
        % Current at breakdown voltage (smorr) Unit: A
        Ibv
        % Reverse breakdown ideality factor (smorr) 
        Nbv
        % Low-level reverse breakdow knee current (smorr) Unit: A
        Ibvl
        % low-level reverse breakdow ideality factor (smorr) 
        Nbvl
        % Flicker-noise coefficient (smorr) 
        Kf
        % Flicker-noise exponent (smorr) 
        Af
        % Flicker noise frequency exponent (smorr) 
        Ffe
        % Tunneling saturation current [per effective area] (smorr) Unit: A
        Jtun
        % Tunneling emission coefficient (smorr) 
        Ntun
        % Tunneling current temperature coefficient (smorr) 
        XTItun
        % Tunneling current bandgap (smorr) Unit: K
        EGtun
        % EG correction factor for tunneling (smorr) 
        KEG
        % Sidewall saturation current (smorr) Unit: A
        Jsw
        % Sidewall zero-bias junction capacitance (smorr) Unit: F
        Cjsw
        % Sidewall grading coefficient (smorr) 
        Msw
        % Sidewall junction potential (smorr) Unit: V
        Vjsw
        % Forward-bias depletion capacitance threshold (smorr) 
        Fcsw
        % Sidewall tunneling saturation current [per junction length] (smorr) Unit: A
        Jtunsw
        % Default diode area (smorr) 
        Area
        % Default diode periphery (smorr) 
        Periph
        % Default diode periphery (smorr) 
        Pj
        % Geometry width (smorr) 
        Width
        % Geometry length (smorr) 
        Length
        % Geometry width and length addition (smorr) 
        Dwl
        % Geometry shrink factor (smorr) 
        Shrink
        % Parameter measurement temperature (smorr) Unit: deg C
        Tnom
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Temperature equation selector (0/1/2) (s--ri) 
        Tlev
        % Temperature equation selector for capacitance (0/1/2/3) (s--ri) 
        Tlevc
        % Saturation current temperature exponent (smorr) 
        Xti
        % Band gap (smorr) Unit: eV
        Eg
        % Energy gap temperature coefficient alpha (smorr) Unit: V/deg C
        EgAlpha
        % Energy gap temperature coefficient beta (smorr) Unit: K
        EgBeta
        % Cjo linear temperature coefficient (smorr) Unit: 1/deg C
        Tcjo
        % Cjsw linear temperature coeffient (smorr) Unit: 1/deg C
        Tcjsw
        % Tt linear temperature coefficient (smorr) Unit: 1/deg C
        Ttt1
        % Tt quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Ttt2
        % Mj linear temperature coefficient (smorr) Unit: 1/deg C
        Tm1
        % Mj quadratic temperature coefficient (smorr) Unit: 1/(deg C)^2
        Tm2
        % Vj linear temperature coefficient (smorr) Unit: 1/deg C
        Tvj
        % Vjsw linear temperature coefficient (smorr) Unit: 1/deg C
        Tvjsw
        % Rs linear temperature coefficient (smorr) Unit: 1/deg C
        Trs
        % Rs quadratic temperature coefficient (smorr) Unit: 1/deg C
        Trs2
        % Bv linear temperature coefficient (smorr) Unit: 1/deg C
        Tbv
        % Bv quadratic temperature coefficient (smorr) Unit: 1/deg C
        Tbv2
        % Allow scale option and instance scale parameter to affect diode instance geometry parameters (s--rb) 
        AllowScaling
        % Sidewall narrowing due to etching per side (smorr) Unit: m
        Etch
        % Sidewall length reduction due to etching per side (smorr) Unit: m
        Etchl
        % Sidewall series resistance (smorr) Unit: Ohms
        Rsw
        % Bottom junction leakage conductance (smorr) Unit: S
        Gleak
        % Sidewall junction leakage conductance (smorr) Unit: S
        Gleaksw
        % Sidewall emission coefficient (smorr) 
        Ns
        % High injection knee current for sidewall (smorr) Unit: A
        Ikp
        % Linear capacitance (smorr) Unit: F
        Cd
        % Linear temperature coefficient for leakage conductance (smorr) Unit: 1/deg C
        Tgs
        % Quadratic temperature coefficient for leakage conductance (smorr) Unit: 1/(deg C)^2
        Tgs2
        % Explosion current (smorr) Unit: A
        Imelt
        % Diode reverse breakdown voltage (warning) (s--rr) Unit: V
        wBv
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
        % 0=exp() 1=exp1() 2=exp2() (s--ri) 
        ExpModel
        % Secured model parameters (s---b) 
        Secured
        % Maximum reverse operating voltage (TSMC SOA warning) (smorr) Unit: V
        Bv_max
        % Maximum forward operating voltage (TSMC SOA warning) (smorr) Unit: V
        Fv_max
        % Enable Pspice diode: 0=off, 1=on (s--ri) 
        Pspice
        % Spectre/Hspice compatibility 0=off, 1=on (s--ri) 
        Hcomp
    end
    methods
        function obj = set.Level(obj,val)
            obj = setParameter(obj,'Level',val,0,'integer');
        end
        function res = get.Level(obj)
            res = getParameter(obj,'Level');
        end
        function obj = set.Is(obj,val)
            obj = setParameter(obj,'Is',val,0,'real');
        end
        function res = get.Is(obj)
            res = getParameter(obj,'Is');
        end
        function obj = set.Rs(obj,val)
            obj = setParameter(obj,'Rs',val,0,'real');
        end
        function res = get.Rs(obj)
            res = getParameter(obj,'Rs');
        end
        function obj = set.Minr(obj,val)
            obj = setParameter(obj,'Minr',val,0,'real');
        end
        function res = get.Minr(obj)
            res = getParameter(obj,'Minr');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Tt(obj,val)
            obj = setParameter(obj,'Tt',val,0,'real');
        end
        function res = get.Tt(obj)
            res = getParameter(obj,'Tt');
        end
        function obj = set.Cjo(obj,val)
            obj = setParameter(obj,'Cjo',val,0,'real');
        end
        function res = get.Cjo(obj)
            res = getParameter(obj,'Cjo');
        end
        function obj = set.Vj(obj,val)
            obj = setParameter(obj,'Vj',val,0,'real');
        end
        function res = get.Vj(obj)
            res = getParameter(obj,'Vj');
        end
        function obj = set.M(obj,val)
            obj = setParameter(obj,'M',val,0,'real');
        end
        function res = get.M(obj)
            res = getParameter(obj,'M');
        end
        function obj = set.Fc(obj,val)
            obj = setParameter(obj,'Fc',val,0,'real');
        end
        function res = get.Fc(obj)
            res = getParameter(obj,'Fc');
        end
        function obj = set.Imax(obj,val)
            obj = setParameter(obj,'Imax',val,0,'real');
        end
        function res = get.Imax(obj)
            res = getParameter(obj,'Imax');
        end
        function obj = set.Isr(obj,val)
            obj = setParameter(obj,'Isr',val,0,'real');
        end
        function res = get.Isr(obj)
            res = getParameter(obj,'Isr');
        end
        function obj = set.Nr(obj,val)
            obj = setParameter(obj,'Nr',val,0,'real');
        end
        function res = get.Nr(obj)
            res = getParameter(obj,'Nr');
        end
        function obj = set.Ikf(obj,val)
            obj = setParameter(obj,'Ikf',val,0,'real');
        end
        function res = get.Ikf(obj)
            res = getParameter(obj,'Ikf');
        end
        function obj = set.Ikr(obj,val)
            obj = setParameter(obj,'Ikr',val,0,'real');
        end
        function res = get.Ikr(obj)
            res = getParameter(obj,'Ikr');
        end
        function obj = set.IkModel(obj,val)
            obj = setParameter(obj,'IkModel',val,0,'integer');
        end
        function res = get.IkModel(obj)
            res = getParameter(obj,'IkModel');
        end
        function obj = set.Bv(obj,val)
            obj = setParameter(obj,'Bv',val,0,'real');
        end
        function res = get.Bv(obj)
            res = getParameter(obj,'Bv');
        end
        function obj = set.Ibv(obj,val)
            obj = setParameter(obj,'Ibv',val,0,'real');
        end
        function res = get.Ibv(obj)
            res = getParameter(obj,'Ibv');
        end
        function obj = set.Nbv(obj,val)
            obj = setParameter(obj,'Nbv',val,0,'real');
        end
        function res = get.Nbv(obj)
            res = getParameter(obj,'Nbv');
        end
        function obj = set.Ibvl(obj,val)
            obj = setParameter(obj,'Ibvl',val,0,'real');
        end
        function res = get.Ibvl(obj)
            res = getParameter(obj,'Ibvl');
        end
        function obj = set.Nbvl(obj,val)
            obj = setParameter(obj,'Nbvl',val,0,'real');
        end
        function res = get.Nbvl(obj)
            res = getParameter(obj,'Nbvl');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Ffe(obj,val)
            obj = setParameter(obj,'Ffe',val,0,'real');
        end
        function res = get.Ffe(obj)
            res = getParameter(obj,'Ffe');
        end
        function obj = set.Jtun(obj,val)
            obj = setParameter(obj,'Jtun',val,0,'real');
        end
        function res = get.Jtun(obj)
            res = getParameter(obj,'Jtun');
        end
        function obj = set.Ntun(obj,val)
            obj = setParameter(obj,'Ntun',val,0,'real');
        end
        function res = get.Ntun(obj)
            res = getParameter(obj,'Ntun');
        end
        function obj = set.XTItun(obj,val)
            obj = setParameter(obj,'XTItun',val,0,'real');
        end
        function res = get.XTItun(obj)
            res = getParameter(obj,'XTItun');
        end
        function obj = set.EGtun(obj,val)
            obj = setParameter(obj,'EGtun',val,0,'real');
        end
        function res = get.EGtun(obj)
            res = getParameter(obj,'EGtun');
        end
        function obj = set.KEG(obj,val)
            obj = setParameter(obj,'KEG',val,0,'real');
        end
        function res = get.KEG(obj)
            res = getParameter(obj,'KEG');
        end
        function obj = set.Jsw(obj,val)
            obj = setParameter(obj,'Jsw',val,0,'real');
        end
        function res = get.Jsw(obj)
            res = getParameter(obj,'Jsw');
        end
        function obj = set.Cjsw(obj,val)
            obj = setParameter(obj,'Cjsw',val,0,'real');
        end
        function res = get.Cjsw(obj)
            res = getParameter(obj,'Cjsw');
        end
        function obj = set.Msw(obj,val)
            obj = setParameter(obj,'Msw',val,0,'real');
        end
        function res = get.Msw(obj)
            res = getParameter(obj,'Msw');
        end
        function obj = set.Vjsw(obj,val)
            obj = setParameter(obj,'Vjsw',val,0,'real');
        end
        function res = get.Vjsw(obj)
            res = getParameter(obj,'Vjsw');
        end
        function obj = set.Fcsw(obj,val)
            obj = setParameter(obj,'Fcsw',val,0,'real');
        end
        function res = get.Fcsw(obj)
            res = getParameter(obj,'Fcsw');
        end
        function obj = set.Jtunsw(obj,val)
            obj = setParameter(obj,'Jtunsw',val,0,'real');
        end
        function res = get.Jtunsw(obj)
            res = getParameter(obj,'Jtunsw');
        end
        function obj = set.Area(obj,val)
            obj = setParameter(obj,'Area',val,0,'real');
        end
        function res = get.Area(obj)
            res = getParameter(obj,'Area');
        end
        function obj = set.Periph(obj,val)
            obj = setParameter(obj,'Periph',val,0,'real');
        end
        function res = get.Periph(obj)
            res = getParameter(obj,'Periph');
        end
        function obj = set.Pj(obj,val)
            obj = setParameter(obj,'Pj',val,0,'real');
        end
        function res = get.Pj(obj)
            res = getParameter(obj,'Pj');
        end
        function obj = set.Width(obj,val)
            obj = setParameter(obj,'Width',val,0,'real');
        end
        function res = get.Width(obj)
            res = getParameter(obj,'Width');
        end
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.Dwl(obj,val)
            obj = setParameter(obj,'Dwl',val,0,'real');
        end
        function res = get.Dwl(obj)
            res = getParameter(obj,'Dwl');
        end
        function obj = set.Shrink(obj,val)
            obj = setParameter(obj,'Shrink',val,0,'real');
        end
        function res = get.Shrink(obj)
            res = getParameter(obj,'Shrink');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Tlev(obj,val)
            obj = setParameter(obj,'Tlev',val,0,'integer');
        end
        function res = get.Tlev(obj)
            res = getParameter(obj,'Tlev');
        end
        function obj = set.Tlevc(obj,val)
            obj = setParameter(obj,'Tlevc',val,0,'integer');
        end
        function res = get.Tlevc(obj)
            res = getParameter(obj,'Tlevc');
        end
        function obj = set.Xti(obj,val)
            obj = setParameter(obj,'Xti',val,0,'real');
        end
        function res = get.Xti(obj)
            res = getParameter(obj,'Xti');
        end
        function obj = set.Eg(obj,val)
            obj = setParameter(obj,'Eg',val,0,'real');
        end
        function res = get.Eg(obj)
            res = getParameter(obj,'Eg');
        end
        function obj = set.EgAlpha(obj,val)
            obj = setParameter(obj,'EgAlpha',val,0,'real');
        end
        function res = get.EgAlpha(obj)
            res = getParameter(obj,'EgAlpha');
        end
        function obj = set.EgBeta(obj,val)
            obj = setParameter(obj,'EgBeta',val,0,'real');
        end
        function res = get.EgBeta(obj)
            res = getParameter(obj,'EgBeta');
        end
        function obj = set.Tcjo(obj,val)
            obj = setParameter(obj,'Tcjo',val,0,'real');
        end
        function res = get.Tcjo(obj)
            res = getParameter(obj,'Tcjo');
        end
        function obj = set.Tcjsw(obj,val)
            obj = setParameter(obj,'Tcjsw',val,0,'real');
        end
        function res = get.Tcjsw(obj)
            res = getParameter(obj,'Tcjsw');
        end
        function obj = set.Ttt1(obj,val)
            obj = setParameter(obj,'Ttt1',val,0,'real');
        end
        function res = get.Ttt1(obj)
            res = getParameter(obj,'Ttt1');
        end
        function obj = set.Ttt2(obj,val)
            obj = setParameter(obj,'Ttt2',val,0,'real');
        end
        function res = get.Ttt2(obj)
            res = getParameter(obj,'Ttt2');
        end
        function obj = set.Tm1(obj,val)
            obj = setParameter(obj,'Tm1',val,0,'real');
        end
        function res = get.Tm1(obj)
            res = getParameter(obj,'Tm1');
        end
        function obj = set.Tm2(obj,val)
            obj = setParameter(obj,'Tm2',val,0,'real');
        end
        function res = get.Tm2(obj)
            res = getParameter(obj,'Tm2');
        end
        function obj = set.Tvj(obj,val)
            obj = setParameter(obj,'Tvj',val,0,'real');
        end
        function res = get.Tvj(obj)
            res = getParameter(obj,'Tvj');
        end
        function obj = set.Tvjsw(obj,val)
            obj = setParameter(obj,'Tvjsw',val,0,'real');
        end
        function res = get.Tvjsw(obj)
            res = getParameter(obj,'Tvjsw');
        end
        function obj = set.Trs(obj,val)
            obj = setParameter(obj,'Trs',val,0,'real');
        end
        function res = get.Trs(obj)
            res = getParameter(obj,'Trs');
        end
        function obj = set.Trs2(obj,val)
            obj = setParameter(obj,'Trs2',val,0,'real');
        end
        function res = get.Trs2(obj)
            res = getParameter(obj,'Trs2');
        end
        function obj = set.Tbv(obj,val)
            obj = setParameter(obj,'Tbv',val,0,'real');
        end
        function res = get.Tbv(obj)
            res = getParameter(obj,'Tbv');
        end
        function obj = set.Tbv2(obj,val)
            obj = setParameter(obj,'Tbv2',val,0,'real');
        end
        function res = get.Tbv2(obj)
            res = getParameter(obj,'Tbv2');
        end
        function obj = set.AllowScaling(obj,val)
            obj = setParameter(obj,'AllowScaling',val,0,'boolean');
        end
        function res = get.AllowScaling(obj)
            res = getParameter(obj,'AllowScaling');
        end
        function obj = set.Etch(obj,val)
            obj = setParameter(obj,'Etch',val,0,'real');
        end
        function res = get.Etch(obj)
            res = getParameter(obj,'Etch');
        end
        function obj = set.Etchl(obj,val)
            obj = setParameter(obj,'Etchl',val,0,'real');
        end
        function res = get.Etchl(obj)
            res = getParameter(obj,'Etchl');
        end
        function obj = set.Rsw(obj,val)
            obj = setParameter(obj,'Rsw',val,0,'real');
        end
        function res = get.Rsw(obj)
            res = getParameter(obj,'Rsw');
        end
        function obj = set.Gleak(obj,val)
            obj = setParameter(obj,'Gleak',val,0,'real');
        end
        function res = get.Gleak(obj)
            res = getParameter(obj,'Gleak');
        end
        function obj = set.Gleaksw(obj,val)
            obj = setParameter(obj,'Gleaksw',val,0,'real');
        end
        function res = get.Gleaksw(obj)
            res = getParameter(obj,'Gleaksw');
        end
        function obj = set.Ns(obj,val)
            obj = setParameter(obj,'Ns',val,0,'real');
        end
        function res = get.Ns(obj)
            res = getParameter(obj,'Ns');
        end
        function obj = set.Ikp(obj,val)
            obj = setParameter(obj,'Ikp',val,0,'real');
        end
        function res = get.Ikp(obj)
            res = getParameter(obj,'Ikp');
        end
        function obj = set.Cd(obj,val)
            obj = setParameter(obj,'Cd',val,0,'real');
        end
        function res = get.Cd(obj)
            res = getParameter(obj,'Cd');
        end
        function obj = set.Tgs(obj,val)
            obj = setParameter(obj,'Tgs',val,0,'real');
        end
        function res = get.Tgs(obj)
            res = getParameter(obj,'Tgs');
        end
        function obj = set.Tgs2(obj,val)
            obj = setParameter(obj,'Tgs2',val,0,'real');
        end
        function res = get.Tgs2(obj)
            res = getParameter(obj,'Tgs2');
        end
        function obj = set.Imelt(obj,val)
            obj = setParameter(obj,'Imelt',val,0,'real');
        end
        function res = get.Imelt(obj)
            res = getParameter(obj,'Imelt');
        end
        function obj = set.wBv(obj,val)
            obj = setParameter(obj,'wBv',val,0,'real');
        end
        function res = get.wBv(obj)
            res = getParameter(obj,'wBv');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.ExpModel(obj,val)
            obj = setParameter(obj,'ExpModel',val,0,'integer');
        end
        function res = get.ExpModel(obj)
            res = getParameter(obj,'ExpModel');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
        function obj = set.Bv_max(obj,val)
            obj = setParameter(obj,'Bv_max',val,0,'real');
        end
        function res = get.Bv_max(obj)
            res = getParameter(obj,'Bv_max');
        end
        function obj = set.Fv_max(obj,val)
            obj = setParameter(obj,'Fv_max',val,0,'real');
        end
        function res = get.Fv_max(obj)
            res = getParameter(obj,'Fv_max');
        end
        function obj = set.Pspice(obj,val)
            obj = setParameter(obj,'Pspice',val,0,'integer');
        end
        function res = get.Pspice(obj)
            res = getParameter(obj,'Pspice');
        end
        function obj = set.Hcomp(obj,val)
            obj = setParameter(obj,'Hcomp',val,0,'integer');
        end
        function res = get.Hcomp(obj)
            res = getParameter(obj,'Hcomp');
        end
    end
end
