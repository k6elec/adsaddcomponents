classdef ADS_VBIC < ADScomponent
    % ADS_VBIC matlab representation for the ADS VBIC component
    % VBIC Bipolar Junction Transistor
    % ModelName [:Name] collector base emitter ...
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Scale factor (smorr) 
        Scale
        % DC operating region, 0=off, 1=on, 2=rev, 3=sat (s---i) 
        Region
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Nonlinear spectral model on/off (s---i) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % NPN bipolar transistor (s---b) 
        NPN
        % PNP bipolar transistor (s---b) 
        PNP
        % Small Signal Transconductance dIcc_dVbei (---rr) Unit: S
        dIcc_dVbei
        % Small Signal Transconductance dIcc_dVbci (---rr) Unit: S
        dIcc_dVbci
        % Small Signal Transconductance dIccp_dVbep (---rr) Unit: S
        dIccp_dVbep
        % Small Signal Transconductance dIccp_dVbcp (---rr) Unit: S
        dIccp_dVbcp
        % Small Signal Transconductance dIccp_dVbci (---rr) Unit: S
        dIccp_dVbci
        % Small Signal Transconductance dIbc_dVbei (---rr) Unit: S
        dIbc_dVbei
        % Small Signal Transconductance Grbi (---rr) Unit: S
        Grbi
        % Small Signal Transconductance dIrbi_dVbei (---rr) Unit: S
        dIrbi_dVbei
        % Small Signal Transconductance dIrbi_dVbci (---rr) Unit: S
        dIrbi_dVbci
        % Small Signal Transconductance Grbp (---rr) Unit: S
        Grbp
        % Small Signal Transconductance dIrbp_dVbep (---rr) Unit: S
        dIrbp_dVbep
        % Small Signal Transconductance dIrbp_dVbci (---rr) Unit: S
        dIrbp_dVbci
        % Small Signal Transconductance Grci (---rr) Unit: S
        Grci
        % Small Signal Transconductance dIrci_dVbci (---rr) Unit: S
        dIrci_dVbci
        % Small Signal Transconductance Gbe (---rr) Unit: S
        Gbe
        % Small Signal Base Emmiter Capacitance Cbe (---rr) Unit: F
        Cbe
        % Small Signal Transconductance Gbex (---rr) Unit: S
        Gbex
        % Small Signal Base Emmiter Capacitance Cbex (---rr) Unit: F
        Cbex
        % Small Signal Transconductance Gbep (---rr) Unit: S
        Gbep
        % Small Signal Base Emmiter Capacitance Cbep (---rr) Unit: F
        Cbep
        % Small Signal Transconductance Gbcp (---rr) Unit: S
        Gbcp
        % Small Signal Base Collector Capacitance Cbcp (---rr) Unit: F
        Cbcp
        % Small Signal Transconductance Gbc (---rr) Unit: S
        Gbc
        % Small Signal Base Collector Capacitance Cbc (---rr) Unit: F
        Cbc
        % Small Signal Transcapacitance dQbe_dVbci (---rr) Unit: F
        dQbe_dVbci
        % Small Signal Transcapacitance dQbep_dVbci (---rr) Unit: F
        dQbep_dVbci
        % Small Signal Transcapacitance dQbcx_dVbci (---rr) Unit: F
        dQbcx_dVbci
        % Small Signal Transcapacitance dQbcx_dVrci (---rr) Unit: F
        dQbcx_dVrci
    end
    methods
        function obj = set.Scale(obj,val)
            obj = setParameter(obj,'Scale',val,0,'real');
        end
        function res = get.Scale(obj)
            res = getParameter(obj,'Scale');
        end
        function obj = set.Region(obj,val)
            obj = setParameter(obj,'Region',val,0,'integer');
        end
        function res = get.Region(obj)
            res = getParameter(obj,'Region');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.NPN(obj,val)
            obj = setParameter(obj,'NPN',val,0,'boolean');
        end
        function res = get.NPN(obj)
            res = getParameter(obj,'NPN');
        end
        function obj = set.PNP(obj,val)
            obj = setParameter(obj,'PNP',val,0,'boolean');
        end
        function res = get.PNP(obj)
            res = getParameter(obj,'PNP');
        end
        function obj = set.dIcc_dVbei(obj,val)
            obj = setParameter(obj,'dIcc_dVbei',val,0,'real');
        end
        function res = get.dIcc_dVbei(obj)
            res = getParameter(obj,'dIcc_dVbei');
        end
        function obj = set.dIcc_dVbci(obj,val)
            obj = setParameter(obj,'dIcc_dVbci',val,0,'real');
        end
        function res = get.dIcc_dVbci(obj)
            res = getParameter(obj,'dIcc_dVbci');
        end
        function obj = set.dIccp_dVbep(obj,val)
            obj = setParameter(obj,'dIccp_dVbep',val,0,'real');
        end
        function res = get.dIccp_dVbep(obj)
            res = getParameter(obj,'dIccp_dVbep');
        end
        function obj = set.dIccp_dVbcp(obj,val)
            obj = setParameter(obj,'dIccp_dVbcp',val,0,'real');
        end
        function res = get.dIccp_dVbcp(obj)
            res = getParameter(obj,'dIccp_dVbcp');
        end
        function obj = set.dIccp_dVbci(obj,val)
            obj = setParameter(obj,'dIccp_dVbci',val,0,'real');
        end
        function res = get.dIccp_dVbci(obj)
            res = getParameter(obj,'dIccp_dVbci');
        end
        function obj = set.dIbc_dVbei(obj,val)
            obj = setParameter(obj,'dIbc_dVbei',val,0,'real');
        end
        function res = get.dIbc_dVbei(obj)
            res = getParameter(obj,'dIbc_dVbei');
        end
        function obj = set.Grbi(obj,val)
            obj = setParameter(obj,'Grbi',val,0,'real');
        end
        function res = get.Grbi(obj)
            res = getParameter(obj,'Grbi');
        end
        function obj = set.dIrbi_dVbei(obj,val)
            obj = setParameter(obj,'dIrbi_dVbei',val,0,'real');
        end
        function res = get.dIrbi_dVbei(obj)
            res = getParameter(obj,'dIrbi_dVbei');
        end
        function obj = set.dIrbi_dVbci(obj,val)
            obj = setParameter(obj,'dIrbi_dVbci',val,0,'real');
        end
        function res = get.dIrbi_dVbci(obj)
            res = getParameter(obj,'dIrbi_dVbci');
        end
        function obj = set.Grbp(obj,val)
            obj = setParameter(obj,'Grbp',val,0,'real');
        end
        function res = get.Grbp(obj)
            res = getParameter(obj,'Grbp');
        end
        function obj = set.dIrbp_dVbep(obj,val)
            obj = setParameter(obj,'dIrbp_dVbep',val,0,'real');
        end
        function res = get.dIrbp_dVbep(obj)
            res = getParameter(obj,'dIrbp_dVbep');
        end
        function obj = set.dIrbp_dVbci(obj,val)
            obj = setParameter(obj,'dIrbp_dVbci',val,0,'real');
        end
        function res = get.dIrbp_dVbci(obj)
            res = getParameter(obj,'dIrbp_dVbci');
        end
        function obj = set.Grci(obj,val)
            obj = setParameter(obj,'Grci',val,0,'real');
        end
        function res = get.Grci(obj)
            res = getParameter(obj,'Grci');
        end
        function obj = set.dIrci_dVbci(obj,val)
            obj = setParameter(obj,'dIrci_dVbci',val,0,'real');
        end
        function res = get.dIrci_dVbci(obj)
            res = getParameter(obj,'dIrci_dVbci');
        end
        function obj = set.Gbe(obj,val)
            obj = setParameter(obj,'Gbe',val,0,'real');
        end
        function res = get.Gbe(obj)
            res = getParameter(obj,'Gbe');
        end
        function obj = set.Cbe(obj,val)
            obj = setParameter(obj,'Cbe',val,0,'real');
        end
        function res = get.Cbe(obj)
            res = getParameter(obj,'Cbe');
        end
        function obj = set.Gbex(obj,val)
            obj = setParameter(obj,'Gbex',val,0,'real');
        end
        function res = get.Gbex(obj)
            res = getParameter(obj,'Gbex');
        end
        function obj = set.Cbex(obj,val)
            obj = setParameter(obj,'Cbex',val,0,'real');
        end
        function res = get.Cbex(obj)
            res = getParameter(obj,'Cbex');
        end
        function obj = set.Gbep(obj,val)
            obj = setParameter(obj,'Gbep',val,0,'real');
        end
        function res = get.Gbep(obj)
            res = getParameter(obj,'Gbep');
        end
        function obj = set.Cbep(obj,val)
            obj = setParameter(obj,'Cbep',val,0,'real');
        end
        function res = get.Cbep(obj)
            res = getParameter(obj,'Cbep');
        end
        function obj = set.Gbcp(obj,val)
            obj = setParameter(obj,'Gbcp',val,0,'real');
        end
        function res = get.Gbcp(obj)
            res = getParameter(obj,'Gbcp');
        end
        function obj = set.Cbcp(obj,val)
            obj = setParameter(obj,'Cbcp',val,0,'real');
        end
        function res = get.Cbcp(obj)
            res = getParameter(obj,'Cbcp');
        end
        function obj = set.Gbc(obj,val)
            obj = setParameter(obj,'Gbc',val,0,'real');
        end
        function res = get.Gbc(obj)
            res = getParameter(obj,'Gbc');
        end
        function obj = set.Cbc(obj,val)
            obj = setParameter(obj,'Cbc',val,0,'real');
        end
        function res = get.Cbc(obj)
            res = getParameter(obj,'Cbc');
        end
        function obj = set.dQbe_dVbci(obj,val)
            obj = setParameter(obj,'dQbe_dVbci',val,0,'real');
        end
        function res = get.dQbe_dVbci(obj)
            res = getParameter(obj,'dQbe_dVbci');
        end
        function obj = set.dQbep_dVbci(obj,val)
            obj = setParameter(obj,'dQbep_dVbci',val,0,'real');
        end
        function res = get.dQbep_dVbci(obj)
            res = getParameter(obj,'dQbep_dVbci');
        end
        function obj = set.dQbcx_dVbci(obj,val)
            obj = setParameter(obj,'dQbcx_dVbci',val,0,'real');
        end
        function res = get.dQbcx_dVbci(obj)
            res = getParameter(obj,'dQbcx_dVbci');
        end
        function obj = set.dQbcx_dVrci(obj,val)
            obj = setParameter(obj,'dQbcx_dVrci',val,0,'real');
        end
        function res = get.dQbcx_dVrci(obj)
            res = getParameter(obj,'dQbcx_dVrci');
        end
    end
end
