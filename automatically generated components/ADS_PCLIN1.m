classdef ADS_PCLIN1 < ADScomponent
    % ADS_PCLIN1 matlab representation for the ADS PCLIN1 component
    % 1 Printed Circuit Coupled Line
    % PCLIN1 [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Width of line (Smorr) Unit: m
        W
        % Distance from line to left wall (Smorr) Unit: m
        S1
        % Conductor layer number (Sm-ri) Unit: unknown
        CLayer1
        % Length of line (Smorr) Unit: m
        L
        % Printed circuit substrate (Sm-rs) Unit: unknown
        Subst
        % Physical temperature (smorr) Unit: C
        Temp
        % Factor to refine the background grid (Sm-ri) Unit: unknown
        Refine_grid_factor
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.S1(obj,val)
            obj = setParameter(obj,'S1',val,0,'real');
        end
        function res = get.S1(obj)
            res = getParameter(obj,'S1');
        end
        function obj = set.CLayer1(obj,val)
            obj = setParameter(obj,'CLayer1',val,0,'integer');
        end
        function res = get.CLayer1(obj)
            res = getParameter(obj,'CLayer1');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Refine_grid_factor(obj,val)
            obj = setParameter(obj,'Refine_grid_factor',val,0,'integer');
        end
        function res = get.Refine_grid_factor(obj)
            res = getParameter(obj,'Refine_grid_factor');
        end
    end
end
