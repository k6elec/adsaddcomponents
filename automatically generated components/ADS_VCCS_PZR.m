classdef ADS_VCCS_PZR < ADScomponent
    % ADS_VCCS_PZR matlab representation for the ADS VCCS_PZR component
    % VCCS defined by transfer function in pole-zero/residue format
    % VCCS_PZR [:Name] p1 n1 L�QH�I ��}��̋AL��A� A�
    properties (Access=protected)
        NumberOfNodes = 5
    end
    properties (Dependent)
        % Poles of the transfer function (smorc) 
        Poles
        % Zeros of the transfer function (smorc) 
        Zeros
        % Residues of the transfer function (smorc) 
        Residues
        % Multiplier for the transfer function value (smorr) 
        Scale
        % Temperature coefficient; per degree Celsius (smorr) Unit: 1/deg C
        TC1
        % Temperature coefficient; per degree Celsius squared (smorr) Unit: 1/(deg C)^2
        TC2
        % Is a SVCCS (sm-ri) 
        SpectreInput
    end
    methods
        function obj = set.Poles(obj,val)
            obj = setParameter(obj,'Poles',val,2,'complex');
        end
        function res = get.Poles(obj)
            res = getParameter(obj,'Poles');
        end
        function obj = set.Zeros(obj,val)
            obj = setParameter(obj,'Zeros',val,2,'complex');
        end
        function res = get.Zeros(obj)
            res = getParameter(obj,'Zeros');
        end
        function obj = set.Residues(obj,val)
            obj = setParameter(obj,'Residues',val,2,'complex');
        end
        function res = get.Residues(obj)
            res = getParameter(obj,'Residues');
        end
        function obj = set.Scale(obj,val)
            obj = setParameter(obj,'Scale',val,0,'real');
        end
        function res = get.Scale(obj)
            res = getParameter(obj,'Scale');
        end
        function obj = set.TC1(obj,val)
            obj = setParameter(obj,'TC1',val,0,'real');
        end
        function res = get.TC1(obj)
            res = getParameter(obj,'TC1');
        end
        function obj = set.TC2(obj,val)
            obj = setParameter(obj,'TC2',val,0,'real');
        end
        function res = get.TC2(obj)
            res = getParameter(obj,'TC2');
        end
        function obj = set.SpectreInput(obj,val)
            obj = setParameter(obj,'SpectreInput',val,0,'integer');
        end
        function res = get.SpectreInput(obj)
            res = getParameter(obj,'SpectreInput');
        end
    end
end
