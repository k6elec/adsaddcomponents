classdef ADS_V_Source < ADScomponent
    % ADS_V_Source matlab representation for the ADS V_Source component
    % Independent Voltage Source
    % V_Source [:Name] p n
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % DC voltage (smorr) Unit: V
        Vdc
        % AC voltage (smorc) Unit: V
        Vac
        % Large signal voltage at a given harmonic (smorc) Unit: V
        V
        % Large signal voltage at all harmonics (smorc) 
        V_All
        % Small signal voltage at a given upper sideband (smo-c) Unit: V
        V_USB
        % Small signal voltage at a given lower sideband (smo-c) Unit: V
        V_LSB
        % Single fundamental index for this source (sm--i) 
        FundIndex
        % Noise Voltage (smorr) Unit: V
        V_Noise
        % Phase noise as a function of offset frequency (smo-r) Unit: dB
        PhaseNoise
        % Restrict phase noise to the USB/LSB of the carrier signal (s---b) 
        LimitPhaseNoiseToCarrier
        % Transient voltage (smorr) Unit: V
        V_Tran
        % Array of large signal harmonics (smo-c) Unit: V
        HarmList
        % Actual n'th frequency value (smo-r) Unit: Hz
        Freq
        % Send source current to rawfile (s--rb) 
        SaveCurrent
        % Nominal temperature in degrees Celsisus (smorr) Unit: deg C
        Tnom
        % Linear temperature coefficient; per degree Celsius (smorr) Unit: 1/deg C
        TC1
        % Temperature coefficient; per degree Celsius squared (smorr) Unit: 1/(deg C)^2
        TC2
        % Source Type (e.g. V_DC for DC source) genereated by PDE. (s--rs) 
        Type
    end
    methods
        function obj = set.Vdc(obj,val)
            obj = setParameter(obj,'Vdc',val,0,'real');
        end
        function res = get.Vdc(obj)
            res = getParameter(obj,'Vdc');
        end
        function obj = set.Vac(obj,val)
            obj = setParameter(obj,'Vac',val,0,'complex');
        end
        function res = get.Vac(obj)
            res = getParameter(obj,'Vac');
        end
        function obj = set.V(obj,val)
            obj = setParameter(obj,'V',val,3,'complex');
        end
        function res = get.V(obj)
            res = getParameter(obj,'V');
        end
        function obj = set.V_All(obj,val)
            obj = setParameter(obj,'V_All',val,0,'complex');
        end
        function res = get.V_All(obj)
            res = getParameter(obj,'V_All');
        end
        function obj = set.V_USB(obj,val)
            obj = setParameter(obj,'V_USB',val,3,'complex');
        end
        function res = get.V_USB(obj)
            res = getParameter(obj,'V_USB');
        end
        function obj = set.V_LSB(obj,val)
            obj = setParameter(obj,'V_LSB',val,3,'complex');
        end
        function res = get.V_LSB(obj)
            res = getParameter(obj,'V_LSB');
        end
        function obj = set.FundIndex(obj,val)
            obj = setParameter(obj,'FundIndex',val,0,'integer');
        end
        function res = get.FundIndex(obj)
            res = getParameter(obj,'FundIndex');
        end
        function obj = set.V_Noise(obj,val)
            obj = setParameter(obj,'V_Noise',val,0,'real');
        end
        function res = get.V_Noise(obj)
            res = getParameter(obj,'V_Noise');
        end
        function obj = set.PhaseNoise(obj,val)
            obj = setParameter(obj,'PhaseNoise',val,0,'real');
        end
        function res = get.PhaseNoise(obj)
            res = getParameter(obj,'PhaseNoise');
        end
        function obj = set.LimitPhaseNoiseToCarrier(obj,val)
            obj = setParameter(obj,'LimitPhaseNoiseToCarrier',val,0,'boolean');
        end
        function res = get.LimitPhaseNoiseToCarrier(obj)
            res = getParameter(obj,'LimitPhaseNoiseToCarrier');
        end
        function obj = set.V_Tran(obj,val)
            obj = setParameter(obj,'V_Tran',val,0,'real');
        end
        function res = get.V_Tran(obj)
            res = getParameter(obj,'V_Tran');
        end
        function obj = set.HarmList(obj,val)
            obj = setParameter(obj,'HarmList',val,0,'complex');
        end
        function res = get.HarmList(obj)
            res = getParameter(obj,'HarmList');
        end
        function obj = set.Freq(obj,val)
            obj = setParameter(obj,'Freq',val,0,'real');
        end
        function res = get.Freq(obj)
            res = getParameter(obj,'Freq');
        end
        function obj = set.SaveCurrent(obj,val)
            obj = setParameter(obj,'SaveCurrent',val,0,'boolean');
        end
        function res = get.SaveCurrent(obj)
            res = getParameter(obj,'SaveCurrent');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.TC1(obj,val)
            obj = setParameter(obj,'TC1',val,0,'real');
        end
        function res = get.TC1(obj)
            res = getParameter(obj,'TC1');
        end
        function obj = set.TC2(obj,val)
            obj = setParameter(obj,'TC2',val,0,'real');
        end
        function res = get.TC2(obj)
            res = getParameter(obj,'TC2');
        end
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'string');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
    end
end
