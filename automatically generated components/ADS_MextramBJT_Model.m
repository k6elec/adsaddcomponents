classdef ADS_MextramBJT_Model < ADSmodel
    % ADS_MextramBJT_Model matlab representation for the ADS MextramBJT_Model component
    % Mextram Bipolar Junction Transistor model
    % model ModelName MextramBJT ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % NPN bipolar transistor (s---b) 
        NPN
        % PNP bipolar transistor (s---b) 
        PNP
        % selection of Mextram release 503, 504, 505 (s---b) 
        Release
        % Flag for extended modeling of the reverse current gain (s---b) 
        Exmod
        % Flag for distributed high frequency effects in transient (s---b) 
        Exphi
        % Flag of extended modeling of avalance currents (s---b) 
        Exavl
        % Collector-emitter saturation current (smorr) Unit: A/m^2
        Is
        % Ideal forward current gain (smorr) 
        Bf
        % Fraction of ideal base current that belongs to the sidewall (smorr) 
        Xibi
        % Saturation current of the non-ideal forward base current (smorr) Unit: A/m^2
        Ibf
        % Cross-over voltage of the non-ideal forward base current (smorr) Unit: V
        Vlf
        % High-injection knee current (smorr) Unit: A/m^2
        Ik
        % Ideal reverse current gain (smorr) 
        Bri
        % Saturation current of the non-ideal reverse base current (smorr) Unit: A/m^2
        Ibr
        % Cross-over voltage of the non-ideal reverse base current (smorr) Unit: V
        Vlr
        % Part that depends on the base-collector voltage Vbc1 (smorr) 
        Xext
        % Base charge at zero bias (smorr) 
        Qb0
        % Base charge at zero bias (smorr) 
        Qbo
        % Factor of the built-in field of the base (smorr) 
        Eta
        % Weak avalanche parameter (smorr) 
        Avl
        % Electric field intercept (smorr) 
        Efi
        % Critical current for hot carriers (smorr) Unit: A/m^2
        Ihc
        % Constant part of the collector resistance (smorr) Unit: Ohms/m^2
        Rcc
        % RCV resistance of the unmodulated epilayer (smorr) Unit: Ohms/m^2
        Rcv
        % Space charge resistance of the epilayer (smorr) Unit: Ohms/m^2
        Scrcv
        % Current spreading factor epilayer (smorr) 
        Sfh
        % Constant part of the base resistance (smorr) Unit: Ohms/m^2
        Rbc
        % Variable part of the base resistance (smorr) Unit: Ohms/m^2
        Rbv
        % Emitter series resistance (smorr) Unit: Ohms/m^2
        Re
        % Minimum delay time of neutral and emitter charge (smorr) Unit: s
        Taune
        % Non-ideality factor of the neutral and emitter charge (smorr) 
        Mtau
        % Zero bias emitter-base depletion capacitance (smorr) Unit: F/m^2
        Cje
        % Emitter-base diffusion voltage (smorr) Unit: V
        Vde
        % Emitter-base grading coefficient (smorr) 
        Pe
        % Fraction of E-B capacitance that belongs to the sidewall (smorr) 
        Xcje
        % Zero bias collector-base depletion capacitance (smorr) Unit: F/m^2
        Cjc
        % Collector-base diffusion voltage (smorr) Unit: V
        Vdc
        % Collector-base grading coefficient variable part (smorr) 
        Pc
        % Constant part of CJC (smorr) 
        Xp
        % Collector current modulation coefficient (smorr) 
        Mc
        % Fraction of the c-b capacitance under the emitter area (smorr) 
        Xcjc
        % Reference temperature (smorr) Unit: deg C
        Tref
        % Difference of the device temperature to the ambient temperature (smorr) Unit: deg
        Dta
        % Band-gap voltage of the emitter (smorr) Unit: eV
        Vge
        % Band-gap voltage of the base (smorr) Unit: eV
        Vgb
        % Band-gap voltage of the collector (smorr) Unit: eV
        Vgc
        % Band-gap voltage recombination emitter-base junction (smorr) Unit: eV
        Vgj
        % Ionization voltage base dope (smorr) Unit: V
        Vi
        % Maximum base dope concentration (smorr) 
        Na
        % Temperature coefficient of VLF and VLR (smorr) 
        Er
        % Temperature coefficient resistivity base (smorr) 
        Ab
        % Temperature coefficient resistivity of the epilayer (smorr) 
        Aepi
        % Temperature coefficient resistivity of the extrinsic base (smorr) 
        Aex
        % Temperature coefficient resistivity of the buried layer (smorr) 
        Ac
        % Flickernoise coefficient ideal base current (smorr) 
        Kf
        % Flickernoise coefficient non-ideal base current (smorr) 
        Kfn
        % Flickernoise exponent (smorr) 
        Af
        % Base-substrate saturation current (smorr) Unit: A/m^2
        Iss
        % Knee current of the substrate (smorr) Unit: A/m^2
        Iks
        % Zero bias col-sub depletion capacitance (smorr) Unit: F/m^2
        Cjs
        % Collector-substrate diffusion voltage (smorr) Unit: V
        Vds
        % Collector-substrate grading coefficient (smorr) 
        Ps
        % Band-gap voltage of the substrate (smorr) Unit: V
        Vgs
        % For closed / open buried layer (smorr) 
        As
        % Substrate junction forward bias (warning) (s--rr) Unit: V
        wVsubfwd
        % Substrate junction reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvsub
        % Base-emitter reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvbe
        % Base-collector reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvbc
        % Base-collector forward bias (warning) (s--rr) Unit: V
        wVbcfwd
        % Maximum base current (warning) (s--rr) Unit: A/m^2
        wIbmax
        % Maximum collector current (warning) (s--rr) Unit: A/m^2
        wIcmax
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
    end
    methods
        function obj = set.NPN(obj,val)
            obj = setParameter(obj,'NPN',val,0,'boolean');
        end
        function res = get.NPN(obj)
            res = getParameter(obj,'NPN');
        end
        function obj = set.PNP(obj,val)
            obj = setParameter(obj,'PNP',val,0,'boolean');
        end
        function res = get.PNP(obj)
            res = getParameter(obj,'PNP');
        end
        function obj = set.Release(obj,val)
            obj = setParameter(obj,'Release',val,0,'boolean');
        end
        function res = get.Release(obj)
            res = getParameter(obj,'Release');
        end
        function obj = set.Exmod(obj,val)
            obj = setParameter(obj,'Exmod',val,0,'boolean');
        end
        function res = get.Exmod(obj)
            res = getParameter(obj,'Exmod');
        end
        function obj = set.Exphi(obj,val)
            obj = setParameter(obj,'Exphi',val,0,'boolean');
        end
        function res = get.Exphi(obj)
            res = getParameter(obj,'Exphi');
        end
        function obj = set.Exavl(obj,val)
            obj = setParameter(obj,'Exavl',val,0,'boolean');
        end
        function res = get.Exavl(obj)
            res = getParameter(obj,'Exavl');
        end
        function obj = set.Is(obj,val)
            obj = setParameter(obj,'Is',val,0,'real');
        end
        function res = get.Is(obj)
            res = getParameter(obj,'Is');
        end
        function obj = set.Bf(obj,val)
            obj = setParameter(obj,'Bf',val,0,'real');
        end
        function res = get.Bf(obj)
            res = getParameter(obj,'Bf');
        end
        function obj = set.Xibi(obj,val)
            obj = setParameter(obj,'Xibi',val,0,'real');
        end
        function res = get.Xibi(obj)
            res = getParameter(obj,'Xibi');
        end
        function obj = set.Ibf(obj,val)
            obj = setParameter(obj,'Ibf',val,0,'real');
        end
        function res = get.Ibf(obj)
            res = getParameter(obj,'Ibf');
        end
        function obj = set.Vlf(obj,val)
            obj = setParameter(obj,'Vlf',val,0,'real');
        end
        function res = get.Vlf(obj)
            res = getParameter(obj,'Vlf');
        end
        function obj = set.Ik(obj,val)
            obj = setParameter(obj,'Ik',val,0,'real');
        end
        function res = get.Ik(obj)
            res = getParameter(obj,'Ik');
        end
        function obj = set.Bri(obj,val)
            obj = setParameter(obj,'Bri',val,0,'real');
        end
        function res = get.Bri(obj)
            res = getParameter(obj,'Bri');
        end
        function obj = set.Ibr(obj,val)
            obj = setParameter(obj,'Ibr',val,0,'real');
        end
        function res = get.Ibr(obj)
            res = getParameter(obj,'Ibr');
        end
        function obj = set.Vlr(obj,val)
            obj = setParameter(obj,'Vlr',val,0,'real');
        end
        function res = get.Vlr(obj)
            res = getParameter(obj,'Vlr');
        end
        function obj = set.Xext(obj,val)
            obj = setParameter(obj,'Xext',val,0,'real');
        end
        function res = get.Xext(obj)
            res = getParameter(obj,'Xext');
        end
        function obj = set.Qb0(obj,val)
            obj = setParameter(obj,'Qb0',val,0,'real');
        end
        function res = get.Qb0(obj)
            res = getParameter(obj,'Qb0');
        end
        function obj = set.Qbo(obj,val)
            obj = setParameter(obj,'Qbo',val,0,'real');
        end
        function res = get.Qbo(obj)
            res = getParameter(obj,'Qbo');
        end
        function obj = set.Eta(obj,val)
            obj = setParameter(obj,'Eta',val,0,'real');
        end
        function res = get.Eta(obj)
            res = getParameter(obj,'Eta');
        end
        function obj = set.Avl(obj,val)
            obj = setParameter(obj,'Avl',val,0,'real');
        end
        function res = get.Avl(obj)
            res = getParameter(obj,'Avl');
        end
        function obj = set.Efi(obj,val)
            obj = setParameter(obj,'Efi',val,0,'real');
        end
        function res = get.Efi(obj)
            res = getParameter(obj,'Efi');
        end
        function obj = set.Ihc(obj,val)
            obj = setParameter(obj,'Ihc',val,0,'real');
        end
        function res = get.Ihc(obj)
            res = getParameter(obj,'Ihc');
        end
        function obj = set.Rcc(obj,val)
            obj = setParameter(obj,'Rcc',val,0,'real');
        end
        function res = get.Rcc(obj)
            res = getParameter(obj,'Rcc');
        end
        function obj = set.Rcv(obj,val)
            obj = setParameter(obj,'Rcv',val,0,'real');
        end
        function res = get.Rcv(obj)
            res = getParameter(obj,'Rcv');
        end
        function obj = set.Scrcv(obj,val)
            obj = setParameter(obj,'Scrcv',val,0,'real');
        end
        function res = get.Scrcv(obj)
            res = getParameter(obj,'Scrcv');
        end
        function obj = set.Sfh(obj,val)
            obj = setParameter(obj,'Sfh',val,0,'real');
        end
        function res = get.Sfh(obj)
            res = getParameter(obj,'Sfh');
        end
        function obj = set.Rbc(obj,val)
            obj = setParameter(obj,'Rbc',val,0,'real');
        end
        function res = get.Rbc(obj)
            res = getParameter(obj,'Rbc');
        end
        function obj = set.Rbv(obj,val)
            obj = setParameter(obj,'Rbv',val,0,'real');
        end
        function res = get.Rbv(obj)
            res = getParameter(obj,'Rbv');
        end
        function obj = set.Re(obj,val)
            obj = setParameter(obj,'Re',val,0,'real');
        end
        function res = get.Re(obj)
            res = getParameter(obj,'Re');
        end
        function obj = set.Taune(obj,val)
            obj = setParameter(obj,'Taune',val,0,'real');
        end
        function res = get.Taune(obj)
            res = getParameter(obj,'Taune');
        end
        function obj = set.Mtau(obj,val)
            obj = setParameter(obj,'Mtau',val,0,'real');
        end
        function res = get.Mtau(obj)
            res = getParameter(obj,'Mtau');
        end
        function obj = set.Cje(obj,val)
            obj = setParameter(obj,'Cje',val,0,'real');
        end
        function res = get.Cje(obj)
            res = getParameter(obj,'Cje');
        end
        function obj = set.Vde(obj,val)
            obj = setParameter(obj,'Vde',val,0,'real');
        end
        function res = get.Vde(obj)
            res = getParameter(obj,'Vde');
        end
        function obj = set.Pe(obj,val)
            obj = setParameter(obj,'Pe',val,0,'real');
        end
        function res = get.Pe(obj)
            res = getParameter(obj,'Pe');
        end
        function obj = set.Xcje(obj,val)
            obj = setParameter(obj,'Xcje',val,0,'real');
        end
        function res = get.Xcje(obj)
            res = getParameter(obj,'Xcje');
        end
        function obj = set.Cjc(obj,val)
            obj = setParameter(obj,'Cjc',val,0,'real');
        end
        function res = get.Cjc(obj)
            res = getParameter(obj,'Cjc');
        end
        function obj = set.Vdc(obj,val)
            obj = setParameter(obj,'Vdc',val,0,'real');
        end
        function res = get.Vdc(obj)
            res = getParameter(obj,'Vdc');
        end
        function obj = set.Pc(obj,val)
            obj = setParameter(obj,'Pc',val,0,'real');
        end
        function res = get.Pc(obj)
            res = getParameter(obj,'Pc');
        end
        function obj = set.Xp(obj,val)
            obj = setParameter(obj,'Xp',val,0,'real');
        end
        function res = get.Xp(obj)
            res = getParameter(obj,'Xp');
        end
        function obj = set.Mc(obj,val)
            obj = setParameter(obj,'Mc',val,0,'real');
        end
        function res = get.Mc(obj)
            res = getParameter(obj,'Mc');
        end
        function obj = set.Xcjc(obj,val)
            obj = setParameter(obj,'Xcjc',val,0,'real');
        end
        function res = get.Xcjc(obj)
            res = getParameter(obj,'Xcjc');
        end
        function obj = set.Tref(obj,val)
            obj = setParameter(obj,'Tref',val,0,'real');
        end
        function res = get.Tref(obj)
            res = getParameter(obj,'Tref');
        end
        function obj = set.Dta(obj,val)
            obj = setParameter(obj,'Dta',val,0,'real');
        end
        function res = get.Dta(obj)
            res = getParameter(obj,'Dta');
        end
        function obj = set.Vge(obj,val)
            obj = setParameter(obj,'Vge',val,0,'real');
        end
        function res = get.Vge(obj)
            res = getParameter(obj,'Vge');
        end
        function obj = set.Vgb(obj,val)
            obj = setParameter(obj,'Vgb',val,0,'real');
        end
        function res = get.Vgb(obj)
            res = getParameter(obj,'Vgb');
        end
        function obj = set.Vgc(obj,val)
            obj = setParameter(obj,'Vgc',val,0,'real');
        end
        function res = get.Vgc(obj)
            res = getParameter(obj,'Vgc');
        end
        function obj = set.Vgj(obj,val)
            obj = setParameter(obj,'Vgj',val,0,'real');
        end
        function res = get.Vgj(obj)
            res = getParameter(obj,'Vgj');
        end
        function obj = set.Vi(obj,val)
            obj = setParameter(obj,'Vi',val,0,'real');
        end
        function res = get.Vi(obj)
            res = getParameter(obj,'Vi');
        end
        function obj = set.Na(obj,val)
            obj = setParameter(obj,'Na',val,0,'real');
        end
        function res = get.Na(obj)
            res = getParameter(obj,'Na');
        end
        function obj = set.Er(obj,val)
            obj = setParameter(obj,'Er',val,0,'real');
        end
        function res = get.Er(obj)
            res = getParameter(obj,'Er');
        end
        function obj = set.Ab(obj,val)
            obj = setParameter(obj,'Ab',val,0,'real');
        end
        function res = get.Ab(obj)
            res = getParameter(obj,'Ab');
        end
        function obj = set.Aepi(obj,val)
            obj = setParameter(obj,'Aepi',val,0,'real');
        end
        function res = get.Aepi(obj)
            res = getParameter(obj,'Aepi');
        end
        function obj = set.Aex(obj,val)
            obj = setParameter(obj,'Aex',val,0,'real');
        end
        function res = get.Aex(obj)
            res = getParameter(obj,'Aex');
        end
        function obj = set.Ac(obj,val)
            obj = setParameter(obj,'Ac',val,0,'real');
        end
        function res = get.Ac(obj)
            res = getParameter(obj,'Ac');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Kfn(obj,val)
            obj = setParameter(obj,'Kfn',val,0,'real');
        end
        function res = get.Kfn(obj)
            res = getParameter(obj,'Kfn');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Iss(obj,val)
            obj = setParameter(obj,'Iss',val,0,'real');
        end
        function res = get.Iss(obj)
            res = getParameter(obj,'Iss');
        end
        function obj = set.Iks(obj,val)
            obj = setParameter(obj,'Iks',val,0,'real');
        end
        function res = get.Iks(obj)
            res = getParameter(obj,'Iks');
        end
        function obj = set.Cjs(obj,val)
            obj = setParameter(obj,'Cjs',val,0,'real');
        end
        function res = get.Cjs(obj)
            res = getParameter(obj,'Cjs');
        end
        function obj = set.Vds(obj,val)
            obj = setParameter(obj,'Vds',val,0,'real');
        end
        function res = get.Vds(obj)
            res = getParameter(obj,'Vds');
        end
        function obj = set.Ps(obj,val)
            obj = setParameter(obj,'Ps',val,0,'real');
        end
        function res = get.Ps(obj)
            res = getParameter(obj,'Ps');
        end
        function obj = set.Vgs(obj,val)
            obj = setParameter(obj,'Vgs',val,0,'real');
        end
        function res = get.Vgs(obj)
            res = getParameter(obj,'Vgs');
        end
        function obj = set.As(obj,val)
            obj = setParameter(obj,'As',val,0,'real');
        end
        function res = get.As(obj)
            res = getParameter(obj,'As');
        end
        function obj = set.wVsubfwd(obj,val)
            obj = setParameter(obj,'wVsubfwd',val,0,'real');
        end
        function res = get.wVsubfwd(obj)
            res = getParameter(obj,'wVsubfwd');
        end
        function obj = set.wBvsub(obj,val)
            obj = setParameter(obj,'wBvsub',val,0,'real');
        end
        function res = get.wBvsub(obj)
            res = getParameter(obj,'wBvsub');
        end
        function obj = set.wBvbe(obj,val)
            obj = setParameter(obj,'wBvbe',val,0,'real');
        end
        function res = get.wBvbe(obj)
            res = getParameter(obj,'wBvbe');
        end
        function obj = set.wBvbc(obj,val)
            obj = setParameter(obj,'wBvbc',val,0,'real');
        end
        function res = get.wBvbc(obj)
            res = getParameter(obj,'wBvbc');
        end
        function obj = set.wVbcfwd(obj,val)
            obj = setParameter(obj,'wVbcfwd',val,0,'real');
        end
        function res = get.wVbcfwd(obj)
            res = getParameter(obj,'wVbcfwd');
        end
        function obj = set.wIbmax(obj,val)
            obj = setParameter(obj,'wIbmax',val,0,'real');
        end
        function res = get.wIbmax(obj)
            res = getParameter(obj,'wIbmax');
        end
        function obj = set.wIcmax(obj,val)
            obj = setParameter(obj,'wIcmax',val,0,'real');
        end
        function res = get.wIcmax(obj)
            res = getParameter(obj,'wIcmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
    end
end
