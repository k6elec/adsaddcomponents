classdef ADS_TX < ADScomponent
    % ADS_TX matlab representation for the ADS TX component
    % TX model for Channel Simulation.
    % TX [:Name] p ...
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % The tx model (s--rs) 
        Model
        % The bit rate (smorr) 
        BitRate
    end
    methods
        function obj = set.Model(obj,val)
            obj = setParameter(obj,'Model',val,0,'string');
        end
        function res = get.Model(obj)
            res = getParameter(obj,'Model');
        end
        function obj = set.BitRate(obj,val)
            obj = setParameter(obj,'BitRate',val,0,'real');
        end
        function res = get.BitRate(obj)
            res = getParameter(obj,'BitRate');
        end
    end
end
