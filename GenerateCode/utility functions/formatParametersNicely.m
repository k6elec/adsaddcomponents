function formatParametersNicely(fid,parameters)

% find the longest string in the parameter names
NAMELENGTH = findLongest('NAME',parameters);
UNITLENGTH = findLongest('UNIT',parameters);

if length(parameters)==1
    if isfield(parameters,'SMORR')
        fprintf(fid,'%%     %s %s %s %s\r\n',appendSpaces(parameters.NAME.Text,NAMELENGTH+1)...
            ,             parameters.SMORR.Text             ...
            ,appendSpaces(parameters.UNIT.Text,UNITLENGTH+1)...
            ,             parameters.DESCRIPTION.Text  );
    else
        fprintf(fid,'%%     %s %s %s\r\n',appendSpaces(parameters.NAME.Text,NAMELENGTH+1)...            ...
            ,appendSpaces(parameters.UNIT.Text,UNITLENGTH+1)...
            ,             parameters.DESCRIPTION.Text  );
    end
else
    for jj=1:length(parameters)
        if isfield(parameters{jj},'SMORR')
            fprintf(fid,'%%     %s %s %s %s\r\n',appendSpaces(parameters{jj}.NAME.Text,NAMELENGTH+1)...
                ,             parameters{jj}.SMORR.Text             ...
                ,appendSpaces(parameters{jj}.UNIT.Text,UNITLENGTH+1)...
                ,             parameters{jj}.DESCRIPTION.Text  );
        else
            fprintf(fid,'%%     %s %s %s\r\n',appendSpaces(parameters{jj}.NAME.Text,NAMELENGTH+1)...            ...
                ,appendSpaces(parameters{jj}.UNIT.Text,UNITLENGTH+1)...
                ,             parameters{jj}.DESCRIPTION.Text  );
        end
    end
end



end

    function res = findLongest(field,parameters)
        res = 0;
        if length(parameters)==1
            res=length(parameters.(field).Text);
        else
            for ii=1:length(parameters)
                if length(parameters{ii}.(field).Text)>res
                    res=length(parameters{ii}.(field).Text);
                end
            end
        end
    end

function res = appendSpaces(str,wantedlength)
    res = [str repmat(' ',1,wantedlength-length(str))];
end
