classdef ADS_Diode < ADScomponent
    % ADS_Diode matlab representation for the ADS Diode component
    % Junction Diode
    % ModelName [:Name] anode cathode
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Junction area factor (smorr) Unit: m^2
        Area
        % Junction periphery factor (smorr) Unit: m
        Periph
        % Junction periphery factor (smorr) Unit: m
        Pj
        % Geometry width (smorr) Unit: m
        Width
        % Geometry length (smorr) Unit: m
        Length
        % Scale factor (s--rr) 
        Scale
        % DC operating region, 0=off, 1=on (s---i) 
        Region
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Nonlinear spectral model on/off (s---b) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % Small signal conductance (---rr) Unit: S
        Gd
        % Small signal capacitance (---rr) Unit: F
        Cd
    end
    methods
        function obj = set.Area(obj,val)
            obj = setParameter(obj,'Area',val,0,'real');
        end
        function res = get.Area(obj)
            res = getParameter(obj,'Area');
        end
        function obj = set.Periph(obj,val)
            obj = setParameter(obj,'Periph',val,0,'real');
        end
        function res = get.Periph(obj)
            res = getParameter(obj,'Periph');
        end
        function obj = set.Pj(obj,val)
            obj = setParameter(obj,'Pj',val,0,'real');
        end
        function res = get.Pj(obj)
            res = getParameter(obj,'Pj');
        end
        function obj = set.Width(obj,val)
            obj = setParameter(obj,'Width',val,0,'real');
        end
        function res = get.Width(obj)
            res = getParameter(obj,'Width');
        end
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.Scale(obj,val)
            obj = setParameter(obj,'Scale',val,0,'real');
        end
        function res = get.Scale(obj)
            res = getParameter(obj,'Scale');
        end
        function obj = set.Region(obj,val)
            obj = setParameter(obj,'Region',val,0,'integer');
        end
        function res = get.Region(obj)
            res = getParameter(obj,'Region');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'boolean');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Gd(obj,val)
            obj = setParameter(obj,'Gd',val,0,'real');
        end
        function res = get.Gd(obj)
            res = getParameter(obj,'Gd');
        end
        function obj = set.Cd(obj,val)
            obj = setParameter(obj,'Cd',val,0,'real');
        end
        function res = get.Cd(obj)
            res = getParameter(obj,'Cd');
        end
    end
end
