classdef ADS_MSTAPER < ADScomponent
    % ADS_MSTAPER matlab representation for the ADS MSTAPER component
    % Microstrip taper transmission line
    % MSTAPER [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Width of port 1 of microstrip taper (Smorr) Unit: m
        W1
        % Width of port 2 of microstrip taper (Smorr) Unit: m
        W2
        % Length of microstrip taper (Smorr) Unit: m
        L
        % Substrate label (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.W1(obj,val)
            obj = setParameter(obj,'W1',val,0,'real');
        end
        function res = get.W1(obj)
            res = getParameter(obj,'W1');
        end
        function obj = set.W2(obj,val)
            obj = setParameter(obj,'W2',val,0,'real');
        end
        function res = get.W2(obj)
            res = getParameter(obj,'W2');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end
