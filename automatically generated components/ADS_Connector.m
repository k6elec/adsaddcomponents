classdef ADS_Connector < ADScomponent
    % ADS_Connector matlab representation for the ADS Connector component
    % User-defined model
    % Connector [:Name] n1 n2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % User device parameter (S--ri) Unit: unknown
        N
        % User device parameter (S--ri) Unit: unknown
        Row
        % User device parameter (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Row(obj,val)
            obj = setParameter(obj,'Row',val,0,'integer');
        end
        function res = get.Row(obj)
            res = getParameter(obj,'Row');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end
