classdef ADS_TQIND < ADScomponent
    % ADS_TQIND matlab representation for the ADS TQIND component
    % Triquint GaAs IC Discrete Inductor
    % TQIND [:Name] t1 t2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Inductor Value (s---r) Unit: H
        L
        % GaAs Substrate Height (s---r) Unit: m
        H
    end
    methods
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.H(obj,val)
            obj = setParameter(obj,'H',val,0,'real');
        end
        function res = get.H(obj)
            res = getParameter(obj,'H');
        end
    end
end
