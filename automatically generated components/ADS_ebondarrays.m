classdef ADS_ebondarrays < ADScomponent
    % ADS_ebondarrays matlab representation for the ADS ebondarrays component
    % User-defined model
    % ebondarrays [:Name] n1 n2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % User device parameter (S--ri) Unit: unknown
        NrOfArrays
        % User device parameter (Sm-rs) Unit: unknown
        Shape
        % User device parameter (Smorr) Unit: unknown
        N
        % User device parameter (Smorr) Unit: unknown
        Xoffset
        % User device parameter (Smorr) Unit: unknown
        Yoffset
        % User device parameter (Smorr) Unit: unknown
        Spacing
        % User device parameter (Smorr) Unit: unknown
        Length
        % User device parameter (smorr) Unit: unknown
        Wire_Angle
        % User device parameter (smorr) Unit: unknown
        Pin1Xoffset
        % User device parameter (smorr) Unit: unknown
        Pin1Yoffset
        % User device parameter (smorr) Unit: unknown
        Pin2Xoffset
        % User device parameter (smorr) Unit: unknown
        Pin2Yoffset
        % User device parameter (Sm-rs) Unit: unknown
        SpacingSeq
        % User device parameter (smorr) Unit: unknown
        Spacing1
        % User device parameter (sm-rs) Unit: unknown
        Spacing1Seq
        % User device parameter (smorr) Unit: unknown
        Spacing2
        % User device parameter (sm-rs) Unit: unknown
        Spacing2Seq
        % User device parameter (smorr) Unit: unknown
        Spacing3
        % User device parameter (sm-rs) Unit: unknown
        Spacing3Seq
        % User device parameter (smorr) Unit: unknown
        x
        % User device parameter (smorr) Unit: unknown
        y
        % User device parameter (smorr) Unit: unknown
        angle
        % User device parameter (sm-ri) Unit: unknown
        xMirror
        % User device parameter (sm-ri) Unit: unknown
        yMirror
    end
    methods
        function obj = set.NrOfArrays(obj,val)
            obj = setParameter(obj,'NrOfArrays',val,0,'integer');
        end
        function res = get.NrOfArrays(obj)
            res = getParameter(obj,'NrOfArrays');
        end
        function obj = set.Shape(obj,val)
            obj = setParameter(obj,'Shape',val,0,'string');
        end
        function res = get.Shape(obj)
            res = getParameter(obj,'Shape');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Xoffset(obj,val)
            obj = setParameter(obj,'Xoffset',val,0,'real');
        end
        function res = get.Xoffset(obj)
            res = getParameter(obj,'Xoffset');
        end
        function obj = set.Yoffset(obj,val)
            obj = setParameter(obj,'Yoffset',val,0,'real');
        end
        function res = get.Yoffset(obj)
            res = getParameter(obj,'Yoffset');
        end
        function obj = set.Spacing(obj,val)
            obj = setParameter(obj,'Spacing',val,0,'real');
        end
        function res = get.Spacing(obj)
            res = getParameter(obj,'Spacing');
        end
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.Wire_Angle(obj,val)
            obj = setParameter(obj,'Wire_Angle',val,0,'real');
        end
        function res = get.Wire_Angle(obj)
            res = getParameter(obj,'Wire_Angle');
        end
        function obj = set.Pin1Xoffset(obj,val)
            obj = setParameter(obj,'Pin1Xoffset',val,0,'real');
        end
        function res = get.Pin1Xoffset(obj)
            res = getParameter(obj,'Pin1Xoffset');
        end
        function obj = set.Pin1Yoffset(obj,val)
            obj = setParameter(obj,'Pin1Yoffset',val,0,'real');
        end
        function res = get.Pin1Yoffset(obj)
            res = getParameter(obj,'Pin1Yoffset');
        end
        function obj = set.Pin2Xoffset(obj,val)
            obj = setParameter(obj,'Pin2Xoffset',val,0,'real');
        end
        function res = get.Pin2Xoffset(obj)
            res = getParameter(obj,'Pin2Xoffset');
        end
        function obj = set.Pin2Yoffset(obj,val)
            obj = setParameter(obj,'Pin2Yoffset',val,0,'real');
        end
        function res = get.Pin2Yoffset(obj)
            res = getParameter(obj,'Pin2Yoffset');
        end
        function obj = set.SpacingSeq(obj,val)
            obj = setParameter(obj,'SpacingSeq',val,0,'string');
        end
        function res = get.SpacingSeq(obj)
            res = getParameter(obj,'SpacingSeq');
        end
        function obj = set.Spacing1(obj,val)
            obj = setParameter(obj,'Spacing1',val,0,'real');
        end
        function res = get.Spacing1(obj)
            res = getParameter(obj,'Spacing1');
        end
        function obj = set.Spacing1Seq(obj,val)
            obj = setParameter(obj,'Spacing1Seq',val,0,'string');
        end
        function res = get.Spacing1Seq(obj)
            res = getParameter(obj,'Spacing1Seq');
        end
        function obj = set.Spacing2(obj,val)
            obj = setParameter(obj,'Spacing2',val,0,'real');
        end
        function res = get.Spacing2(obj)
            res = getParameter(obj,'Spacing2');
        end
        function obj = set.Spacing2Seq(obj,val)
            obj = setParameter(obj,'Spacing2Seq',val,0,'string');
        end
        function res = get.Spacing2Seq(obj)
            res = getParameter(obj,'Spacing2Seq');
        end
        function obj = set.Spacing3(obj,val)
            obj = setParameter(obj,'Spacing3',val,0,'real');
        end
        function res = get.Spacing3(obj)
            res = getParameter(obj,'Spacing3');
        end
        function obj = set.Spacing3Seq(obj,val)
            obj = setParameter(obj,'Spacing3Seq',val,0,'string');
        end
        function res = get.Spacing3Seq(obj)
            res = getParameter(obj,'Spacing3Seq');
        end
        function obj = set.x(obj,val)
            obj = setParameter(obj,'x',val,0,'real');
        end
        function res = get.x(obj)
            res = getParameter(obj,'x');
        end
        function obj = set.y(obj,val)
            obj = setParameter(obj,'y',val,0,'real');
        end
        function res = get.y(obj)
            res = getParameter(obj,'y');
        end
        function obj = set.angle(obj,val)
            obj = setParameter(obj,'angle',val,0,'real');
        end
        function res = get.angle(obj)
            res = getParameter(obj,'angle');
        end
        function obj = set.xMirror(obj,val)
            obj = setParameter(obj,'xMirror',val,0,'integer');
        end
        function res = get.xMirror(obj)
            res = getParameter(obj,'xMirror');
        end
        function obj = set.yMirror(obj,val)
            obj = setParameter(obj,'yMirror',val,0,'integer');
        end
        function res = get.yMirror(obj)
            res = getParameter(obj,'yMirror');
        end
    end
end
