classdef ADS_Optim < ADSnodeless
    % ADS_Optim matlab representation for the ADS Optim component
    % Nominal optimization (using OptPak)
    % Optim [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Type of minimization method to use: gucker, hp_vmo, sga_sp... (s---s) 
        OptimType
        % Error function formulation: L1, L2, mm, neg_L2... (s---s) 
        ErrorForm
        % enable optimization cockpit (s---b) 
        EnableCockpit
        % normalize goal values (s---b) 
        NormalizeGoals
        % Maximum number of iterations/trials for the gradient/random optimizers respectively (s---i) 
        MaxIters
        % Initial Temperature for the SA optimizers (s---r) 
        InitialTemp
        % Maximum number of shoots inside a lot for the SA optimizers (s---i) 
        NumShootsPerIter
        % order of optimization norm (s---i) 
        P
        % maximum acceptable error function (s---r) 
        DesiredError
        % degree of annotation (s---i) 
        StatusLevel
        % Analysis to be called after optimization (s---s) 
        FinalAnalysis
        % Keep best values for parent optimization. (s---b) 
        SetBestValues
        % seed for random optimizer (s---i) 
        Seed
        % Flag to send nominal analysis solutions to dataset (s---b) 
        SaveNominal
        % Flag to send analysis solutions to dataset (s---b) 
        SaveSolns
        % Send opt var values to dataset (s---b) 
        SaveOptimVars
        % Send optgoal values to dataset (s---b) 
        SaveGoals
        % Send current error function value to dataset (s---b) 
        SaveCurrentEF
        % Update dataset on each iteration (s---b) 
        UpdateDataset
        % Save data for all iterations (s---b) 
        SaveAllIterations
        % Save data for all trials (s---b) 
        SaveAllTrials
        % Enable controller (sm--b) 
        Enable
        % Flag to indicate use of all optimization variables (s---b) 
        UseAllOptVars
        % Name of variable(s) to use in optimization (repeatable) (s---s) 
        OptVar
        % Flag to indicate use of all Goals (s---b) 
        UseAllGoals
        % Name of goal(s) to use in optimization (repeatable) (s---s) 
        GoalName
        % The limit for recovering simulation error (s---i) 
        RecoverErrorLimit
        % The debug level for optpak (s---i) 
        DebugLevel
        % sort optimization variables (s---b) 
        SortVars
        % Enable Advanced Termination Criteria (s---b) 
        UseAdvTermCriteria
        % Cost Relative Toerlance (s---r) 
        CostRelativeTol
        % Limit for the number of Continous Small Improvement (s---i) 
        LimitOfContSmallImprovement
        % Start Trial (s---i) 
        StartTrial
        % Stop Trial (s---i) 
        StopTrial
    end
    methods
        function obj = set.OptimType(obj,val)
            obj = setParameter(obj,'OptimType',val,0,'string');
        end
        function res = get.OptimType(obj)
            res = getParameter(obj,'OptimType');
        end
        function obj = set.ErrorForm(obj,val)
            obj = setParameter(obj,'ErrorForm',val,0,'string');
        end
        function res = get.ErrorForm(obj)
            res = getParameter(obj,'ErrorForm');
        end
        function obj = set.EnableCockpit(obj,val)
            obj = setParameter(obj,'EnableCockpit',val,0,'boolean');
        end
        function res = get.EnableCockpit(obj)
            res = getParameter(obj,'EnableCockpit');
        end
        function obj = set.NormalizeGoals(obj,val)
            obj = setParameter(obj,'NormalizeGoals',val,0,'boolean');
        end
        function res = get.NormalizeGoals(obj)
            res = getParameter(obj,'NormalizeGoals');
        end
        function obj = set.MaxIters(obj,val)
            obj = setParameter(obj,'MaxIters',val,0,'integer');
        end
        function res = get.MaxIters(obj)
            res = getParameter(obj,'MaxIters');
        end
        function obj = set.InitialTemp(obj,val)
            obj = setParameter(obj,'InitialTemp',val,0,'real');
        end
        function res = get.InitialTemp(obj)
            res = getParameter(obj,'InitialTemp');
        end
        function obj = set.NumShootsPerIter(obj,val)
            obj = setParameter(obj,'NumShootsPerIter',val,0,'integer');
        end
        function res = get.NumShootsPerIter(obj)
            res = getParameter(obj,'NumShootsPerIter');
        end
        function obj = set.P(obj,val)
            obj = setParameter(obj,'P',val,0,'integer');
        end
        function res = get.P(obj)
            res = getParameter(obj,'P');
        end
        function obj = set.DesiredError(obj,val)
            obj = setParameter(obj,'DesiredError',val,0,'real');
        end
        function res = get.DesiredError(obj)
            res = getParameter(obj,'DesiredError');
        end
        function obj = set.StatusLevel(obj,val)
            obj = setParameter(obj,'StatusLevel',val,0,'integer');
        end
        function res = get.StatusLevel(obj)
            res = getParameter(obj,'StatusLevel');
        end
        function obj = set.FinalAnalysis(obj,val)
            obj = setParameter(obj,'FinalAnalysis',val,0,'string');
        end
        function res = get.FinalAnalysis(obj)
            res = getParameter(obj,'FinalAnalysis');
        end
        function obj = set.SetBestValues(obj,val)
            obj = setParameter(obj,'SetBestValues',val,0,'boolean');
        end
        function res = get.SetBestValues(obj)
            res = getParameter(obj,'SetBestValues');
        end
        function obj = set.Seed(obj,val)
            obj = setParameter(obj,'Seed',val,0,'integer');
        end
        function res = get.Seed(obj)
            res = getParameter(obj,'Seed');
        end
        function obj = set.SaveNominal(obj,val)
            obj = setParameter(obj,'SaveNominal',val,0,'boolean');
        end
        function res = get.SaveNominal(obj)
            res = getParameter(obj,'SaveNominal');
        end
        function obj = set.SaveSolns(obj,val)
            obj = setParameter(obj,'SaveSolns',val,0,'boolean');
        end
        function res = get.SaveSolns(obj)
            res = getParameter(obj,'SaveSolns');
        end
        function obj = set.SaveOptimVars(obj,val)
            obj = setParameter(obj,'SaveOptimVars',val,0,'boolean');
        end
        function res = get.SaveOptimVars(obj)
            res = getParameter(obj,'SaveOptimVars');
        end
        function obj = set.SaveGoals(obj,val)
            obj = setParameter(obj,'SaveGoals',val,0,'boolean');
        end
        function res = get.SaveGoals(obj)
            res = getParameter(obj,'SaveGoals');
        end
        function obj = set.SaveCurrentEF(obj,val)
            obj = setParameter(obj,'SaveCurrentEF',val,0,'boolean');
        end
        function res = get.SaveCurrentEF(obj)
            res = getParameter(obj,'SaveCurrentEF');
        end
        function obj = set.UpdateDataset(obj,val)
            obj = setParameter(obj,'UpdateDataset',val,0,'boolean');
        end
        function res = get.UpdateDataset(obj)
            res = getParameter(obj,'UpdateDataset');
        end
        function obj = set.SaveAllIterations(obj,val)
            obj = setParameter(obj,'SaveAllIterations',val,0,'boolean');
        end
        function res = get.SaveAllIterations(obj)
            res = getParameter(obj,'SaveAllIterations');
        end
        function obj = set.SaveAllTrials(obj,val)
            obj = setParameter(obj,'SaveAllTrials',val,0,'boolean');
        end
        function res = get.SaveAllTrials(obj)
            res = getParameter(obj,'SaveAllTrials');
        end
        function obj = set.Enable(obj,val)
            obj = setParameter(obj,'Enable',val,0,'boolean');
        end
        function res = get.Enable(obj)
            res = getParameter(obj,'Enable');
        end
        function obj = set.UseAllOptVars(obj,val)
            obj = setParameter(obj,'UseAllOptVars',val,0,'boolean');
        end
        function res = get.UseAllOptVars(obj)
            res = getParameter(obj,'UseAllOptVars');
        end
        function obj = set.OptVar(obj,val)
            obj = setParameter(obj,'OptVar',val,1,'string');
        end
        function res = get.OptVar(obj)
            res = getParameter(obj,'OptVar');
        end
        function obj = set.UseAllGoals(obj,val)
            obj = setParameter(obj,'UseAllGoals',val,0,'boolean');
        end
        function res = get.UseAllGoals(obj)
            res = getParameter(obj,'UseAllGoals');
        end
        function obj = set.GoalName(obj,val)
            obj = setParameter(obj,'GoalName',val,1,'string');
        end
        function res = get.GoalName(obj)
            res = getParameter(obj,'GoalName');
        end
        function obj = set.RecoverErrorLimit(obj,val)
            obj = setParameter(obj,'RecoverErrorLimit',val,0,'integer');
        end
        function res = get.RecoverErrorLimit(obj)
            res = getParameter(obj,'RecoverErrorLimit');
        end
        function obj = set.DebugLevel(obj,val)
            obj = setParameter(obj,'DebugLevel',val,0,'integer');
        end
        function res = get.DebugLevel(obj)
            res = getParameter(obj,'DebugLevel');
        end
        function obj = set.SortVars(obj,val)
            obj = setParameter(obj,'SortVars',val,0,'boolean');
        end
        function res = get.SortVars(obj)
            res = getParameter(obj,'SortVars');
        end
        function obj = set.UseAdvTermCriteria(obj,val)
            obj = setParameter(obj,'UseAdvTermCriteria',val,0,'boolean');
        end
        function res = get.UseAdvTermCriteria(obj)
            res = getParameter(obj,'UseAdvTermCriteria');
        end
        function obj = set.CostRelativeTol(obj,val)
            obj = setParameter(obj,'CostRelativeTol',val,0,'real');
        end
        function res = get.CostRelativeTol(obj)
            res = getParameter(obj,'CostRelativeTol');
        end
        function obj = set.LimitOfContSmallImprovement(obj,val)
            obj = setParameter(obj,'LimitOfContSmallImprovement',val,0,'integer');
        end
        function res = get.LimitOfContSmallImprovement(obj)
            res = getParameter(obj,'LimitOfContSmallImprovement');
        end
        function obj = set.StartTrial(obj,val)
            obj = setParameter(obj,'StartTrial',val,0,'integer');
        end
        function res = get.StartTrial(obj)
            res = getParameter(obj,'StartTrial');
        end
        function obj = set.StopTrial(obj,val)
            obj = setParameter(obj,'StopTrial',val,0,'integer');
        end
        function res = get.StopTrial(obj)
            res = getParameter(obj,'StopTrial');
        end
    end
end
