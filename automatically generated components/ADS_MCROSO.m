classdef ADS_MCROSO < ADScomponent
    % ADS_MCROSO matlab representation for the ADS MCROSO component
    % Libra Microstrip Cross-Junction
    % MCROSO [:Name] n1 n2 n3 n4
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Conductor Width of Line at Pin 1 (Smorr) Unit: m
        W1
        % Conductor Width of Line at Pin 2 (Smorr) Unit: m
        W2
        % Conductor Width of Line at Pin 3 (Smorr) Unit: m
        W3
        % Conductor Width of Line at Pin 4 (Smorr) Unit: m
        W4
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W1(obj,val)
            obj = setParameter(obj,'W1',val,0,'real');
        end
        function res = get.W1(obj)
            res = getParameter(obj,'W1');
        end
        function obj = set.W2(obj,val)
            obj = setParameter(obj,'W2',val,0,'real');
        end
        function res = get.W2(obj)
            res = getParameter(obj,'W2');
        end
        function obj = set.W3(obj,val)
            obj = setParameter(obj,'W3',val,0,'real');
        end
        function res = get.W3(obj)
            res = getParameter(obj,'W3');
        end
        function obj = set.W4(obj,val)
            obj = setParameter(obj,'W4',val,0,'real');
        end
        function res = get.W4(obj)
            res = getParameter(obj,'W4');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end
