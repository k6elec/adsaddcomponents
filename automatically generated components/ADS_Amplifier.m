classdef ADS_Amplifier < ADScomponent
    % ADS_Amplifier matlab representation for the ADS Amplifier component
    % System simulator amplifier
    % Amplifier [:Name] input output
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Small signal gain term in dB (smorr) Unit: dB
        Gain
        % Input or output power in dBm at `GainComp' dB gain compression (smorr) Unit: dBm
        GainCompPower
        % Gain compression at `GainCompPower' power (smorr) 
        GainComp
        % Input or output second order intercept point in dBm (smorr) Unit: dBm
        SOI
        % Input or output third order intercept point in dBm (smorr) Unit: dBm
        TOI
        % Power Saturation Point in dBm (smorr) Unit: dBm
        Psat
        % Gain compression at `Psat' in dB (smorr) Unit: dB
        GainCompSat
        % Amplitude Modulation to Phase Modulation in degrees/dB (smorr) 
        AM2PM
        % Power Level at AM2PM in dBm (smorr) Unit: dBm
        PAM2PM
        % Forward Reflection Coefficient (smorc) 
        S11
        % Reverse Reflection Coefficient (smorc) 
        S22
        % Reverse Transmission Coefficient (smorc) 
        S12
        % Forward Transmission Coefficient (smorc) 
        S21
        % Power Levels refer to 1=INPUT or 0=OUTPUT (s--rb) 
        ReferToInput
        % Frequency at which Gain Compression is specified (smorr) Unit: Hz
        GainCompFreq
        % Reference Impedance for Scattering Parameters (smorc) Unit: Ohms
        Zref
        % Noise Figure in dB (smorr) 
        NF
        % Minimum Noise Figure at Sopt in dB (smorr) 
        NFmin
        % Equivalent Noise Resistance (smorr) Unit: Ohms
        Rn
        % Optimum Source Reflection for Minimum Noise Figure (smorc) 
        Sopt
        % Reference Impedance for Port1 (smorc) Unit: Ohms
        Z1
        % Reference Impedance for Port2 (smorc) Unit: Ohms
        Z2
        % Non-causal function impulse response order (sm--i) 
        ImpNoncausalLength
        % Convolution mode (sm--i) 
        ImpMode
        % Maximum Frequency to which device is evaluated (smo-r) Unit: Hz
        ImpMaxFreq
        % Sample spacing in frequency (smo-r) Unit: Hz
        ImpDeltaFreq
        % maximum allowed impulse response order (sm--i) 
        ImpMaxOrder
        % Smoothing Window (sm--i) 
        ImpWindow
        % Relative impulse Response truncation factor (smo-r) 
        ImpRelTol
        % Absolute impulse Response truncation factor (smo-r) 
        ImpAbsTol
        % Nonlinear Gain Compression Type: 0=polynomial, 1=file based (s---i) 
        GainCompType
        % Nonlinear Gain Compression File (s---d) 
        GainCompFile
        % SYM array pointer to all S parameters (smorc) 
        Sall
        % Clip Data File: 0=no, 1=yes (s---i) 
        ClipDataFile
    end
    methods
        function obj = set.Gain(obj,val)
            obj = setParameter(obj,'Gain',val,0,'real');
        end
        function res = get.Gain(obj)
            res = getParameter(obj,'Gain');
        end
        function obj = set.GainCompPower(obj,val)
            obj = setParameter(obj,'GainCompPower',val,0,'real');
        end
        function res = get.GainCompPower(obj)
            res = getParameter(obj,'GainCompPower');
        end
        function obj = set.GainComp(obj,val)
            obj = setParameter(obj,'GainComp',val,0,'real');
        end
        function res = get.GainComp(obj)
            res = getParameter(obj,'GainComp');
        end
        function obj = set.SOI(obj,val)
            obj = setParameter(obj,'SOI',val,0,'real');
        end
        function res = get.SOI(obj)
            res = getParameter(obj,'SOI');
        end
        function obj = set.TOI(obj,val)
            obj = setParameter(obj,'TOI',val,0,'real');
        end
        function res = get.TOI(obj)
            res = getParameter(obj,'TOI');
        end
        function obj = set.Psat(obj,val)
            obj = setParameter(obj,'Psat',val,0,'real');
        end
        function res = get.Psat(obj)
            res = getParameter(obj,'Psat');
        end
        function obj = set.GainCompSat(obj,val)
            obj = setParameter(obj,'GainCompSat',val,0,'real');
        end
        function res = get.GainCompSat(obj)
            res = getParameter(obj,'GainCompSat');
        end
        function obj = set.AM2PM(obj,val)
            obj = setParameter(obj,'AM2PM',val,0,'real');
        end
        function res = get.AM2PM(obj)
            res = getParameter(obj,'AM2PM');
        end
        function obj = set.PAM2PM(obj,val)
            obj = setParameter(obj,'PAM2PM',val,0,'real');
        end
        function res = get.PAM2PM(obj)
            res = getParameter(obj,'PAM2PM');
        end
        function obj = set.S11(obj,val)
            obj = setParameter(obj,'S11',val,0,'complex');
        end
        function res = get.S11(obj)
            res = getParameter(obj,'S11');
        end
        function obj = set.S22(obj,val)
            obj = setParameter(obj,'S22',val,0,'complex');
        end
        function res = get.S22(obj)
            res = getParameter(obj,'S22');
        end
        function obj = set.S12(obj,val)
            obj = setParameter(obj,'S12',val,0,'complex');
        end
        function res = get.S12(obj)
            res = getParameter(obj,'S12');
        end
        function obj = set.S21(obj,val)
            obj = setParameter(obj,'S21',val,0,'complex');
        end
        function res = get.S21(obj)
            res = getParameter(obj,'S21');
        end
        function obj = set.ReferToInput(obj,val)
            obj = setParameter(obj,'ReferToInput',val,0,'boolean');
        end
        function res = get.ReferToInput(obj)
            res = getParameter(obj,'ReferToInput');
        end
        function obj = set.GainCompFreq(obj,val)
            obj = setParameter(obj,'GainCompFreq',val,0,'real');
        end
        function res = get.GainCompFreq(obj)
            res = getParameter(obj,'GainCompFreq');
        end
        function obj = set.Zref(obj,val)
            obj = setParameter(obj,'Zref',val,0,'complex');
        end
        function res = get.Zref(obj)
            res = getParameter(obj,'Zref');
        end
        function obj = set.NF(obj,val)
            obj = setParameter(obj,'NF',val,0,'real');
        end
        function res = get.NF(obj)
            res = getParameter(obj,'NF');
        end
        function obj = set.NFmin(obj,val)
            obj = setParameter(obj,'NFmin',val,0,'real');
        end
        function res = get.NFmin(obj)
            res = getParameter(obj,'NFmin');
        end
        function obj = set.Rn(obj,val)
            obj = setParameter(obj,'Rn',val,0,'real');
        end
        function res = get.Rn(obj)
            res = getParameter(obj,'Rn');
        end
        function obj = set.Sopt(obj,val)
            obj = setParameter(obj,'Sopt',val,0,'complex');
        end
        function res = get.Sopt(obj)
            res = getParameter(obj,'Sopt');
        end
        function obj = set.Z1(obj,val)
            obj = setParameter(obj,'Z1',val,0,'complex');
        end
        function res = get.Z1(obj)
            res = getParameter(obj,'Z1');
        end
        function obj = set.Z2(obj,val)
            obj = setParameter(obj,'Z2',val,0,'complex');
        end
        function res = get.Z2(obj)
            res = getParameter(obj,'Z2');
        end
        function obj = set.ImpNoncausalLength(obj,val)
            obj = setParameter(obj,'ImpNoncausalLength',val,0,'integer');
        end
        function res = get.ImpNoncausalLength(obj)
            res = getParameter(obj,'ImpNoncausalLength');
        end
        function obj = set.ImpMode(obj,val)
            obj = setParameter(obj,'ImpMode',val,0,'integer');
        end
        function res = get.ImpMode(obj)
            res = getParameter(obj,'ImpMode');
        end
        function obj = set.ImpMaxFreq(obj,val)
            obj = setParameter(obj,'ImpMaxFreq',val,0,'real');
        end
        function res = get.ImpMaxFreq(obj)
            res = getParameter(obj,'ImpMaxFreq');
        end
        function obj = set.ImpDeltaFreq(obj,val)
            obj = setParameter(obj,'ImpDeltaFreq',val,0,'real');
        end
        function res = get.ImpDeltaFreq(obj)
            res = getParameter(obj,'ImpDeltaFreq');
        end
        function obj = set.ImpMaxOrder(obj,val)
            obj = setParameter(obj,'ImpMaxOrder',val,0,'integer');
        end
        function res = get.ImpMaxOrder(obj)
            res = getParameter(obj,'ImpMaxOrder');
        end
        function obj = set.ImpWindow(obj,val)
            obj = setParameter(obj,'ImpWindow',val,0,'integer');
        end
        function res = get.ImpWindow(obj)
            res = getParameter(obj,'ImpWindow');
        end
        function obj = set.ImpRelTol(obj,val)
            obj = setParameter(obj,'ImpRelTol',val,0,'real');
        end
        function res = get.ImpRelTol(obj)
            res = getParameter(obj,'ImpRelTol');
        end
        function obj = set.ImpAbsTol(obj,val)
            obj = setParameter(obj,'ImpAbsTol',val,0,'real');
        end
        function res = get.ImpAbsTol(obj)
            res = getParameter(obj,'ImpAbsTol');
        end
        function obj = set.GainCompType(obj,val)
            obj = setParameter(obj,'GainCompType',val,0,'integer');
        end
        function res = get.GainCompType(obj)
            res = getParameter(obj,'GainCompType');
        end
        function obj = set.GainCompFile(obj,val)
            obj = setParameter(obj,'GainCompFile',val,0,'string');
        end
        function res = get.GainCompFile(obj)
            res = getParameter(obj,'GainCompFile');
        end
        function obj = set.Sall(obj,val)
            obj = setParameter(obj,'Sall',val,0,'complex');
        end
        function res = get.Sall(obj)
            res = getParameter(obj,'Sall');
        end
        function obj = set.ClipDataFile(obj,val)
            obj = setParameter(obj,'ClipDataFile',val,0,'integer');
        end
        function res = get.ClipDataFile(obj)
            res = getParameter(obj,'ClipDataFile');
        end
    end
end
