classdef ADS_DOE < ADSnodeless
    % ADS_DOE matlab representation for the ADS DOE component
    % Design of Experiments
    % DOE [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Type of doe experiment to use. (s---s) 
        ExperimentType
        % Fractionalization element (s---i) 
        FracElem
        % degree of annotation (s---i) 
        StatusLevel
        % Flag to send analysis solutions to dataset (s---b) 
        SaveSolns
        % Send DOE variable values to dataset (s---b) 
        SaveDoeVars
        % Send doeGoal results to dataset (s---b) 
        SaveDoeGoals
        % Update dataset's DoeGoals for each doe iteration (s---b) 
        UpdateDataset
        % Save data for all iterations (s---b) 
        SaveAllIterations
        % Enable DOE Controller (sm--b) 
        Enable
        % Flag to indicate use of all DoeGoals (s---b) 
        UseAllDoeGoals
        % Name of DoeGoal(s) to use in experiment (repeatable) (s---s) 
        DoeGoalName
        % recover error limit (s---i) 
        RecoverErrorLimit
        % debug level (s---i) 
        DebugLevel
        % sort variables (s---b) 
        SortVars
    end
    methods
        function obj = set.ExperimentType(obj,val)
            obj = setParameter(obj,'ExperimentType',val,0,'string');
        end
        function res = get.ExperimentType(obj)
            res = getParameter(obj,'ExperimentType');
        end
        function obj = set.FracElem(obj,val)
            obj = setParameter(obj,'FracElem',val,0,'integer');
        end
        function res = get.FracElem(obj)
            res = getParameter(obj,'FracElem');
        end
        function obj = set.StatusLevel(obj,val)
            obj = setParameter(obj,'StatusLevel',val,0,'integer');
        end
        function res = get.StatusLevel(obj)
            res = getParameter(obj,'StatusLevel');
        end
        function obj = set.SaveSolns(obj,val)
            obj = setParameter(obj,'SaveSolns',val,0,'boolean');
        end
        function res = get.SaveSolns(obj)
            res = getParameter(obj,'SaveSolns');
        end
        function obj = set.SaveDoeVars(obj,val)
            obj = setParameter(obj,'SaveDoeVars',val,0,'boolean');
        end
        function res = get.SaveDoeVars(obj)
            res = getParameter(obj,'SaveDoeVars');
        end
        function obj = set.SaveDoeGoals(obj,val)
            obj = setParameter(obj,'SaveDoeGoals',val,0,'boolean');
        end
        function res = get.SaveDoeGoals(obj)
            res = getParameter(obj,'SaveDoeGoals');
        end
        function obj = set.UpdateDataset(obj,val)
            obj = setParameter(obj,'UpdateDataset',val,0,'boolean');
        end
        function res = get.UpdateDataset(obj)
            res = getParameter(obj,'UpdateDataset');
        end
        function obj = set.SaveAllIterations(obj,val)
            obj = setParameter(obj,'SaveAllIterations',val,0,'boolean');
        end
        function res = get.SaveAllIterations(obj)
            res = getParameter(obj,'SaveAllIterations');
        end
        function obj = set.Enable(obj,val)
            obj = setParameter(obj,'Enable',val,0,'boolean');
        end
        function res = get.Enable(obj)
            res = getParameter(obj,'Enable');
        end
        function obj = set.UseAllDoeGoals(obj,val)
            obj = setParameter(obj,'UseAllDoeGoals',val,0,'boolean');
        end
        function res = get.UseAllDoeGoals(obj)
            res = getParameter(obj,'UseAllDoeGoals');
        end
        function obj = set.DoeGoalName(obj,val)
            obj = setParameter(obj,'DoeGoalName',val,1,'string');
        end
        function res = get.DoeGoalName(obj)
            res = getParameter(obj,'DoeGoalName');
        end
        function obj = set.RecoverErrorLimit(obj,val)
            obj = setParameter(obj,'RecoverErrorLimit',val,0,'integer');
        end
        function res = get.RecoverErrorLimit(obj)
            res = getParameter(obj,'RecoverErrorLimit');
        end
        function obj = set.DebugLevel(obj,val)
            obj = setParameter(obj,'DebugLevel',val,0,'integer');
        end
        function res = get.DebugLevel(obj)
            res = getParameter(obj,'DebugLevel');
        end
        function obj = set.SortVars(obj,val)
            obj = setParameter(obj,'SortVars',val,0,'boolean');
        end
        function res = get.SortVars(obj)
            res = getParameter(obj,'SortVars');
        end
    end
end
