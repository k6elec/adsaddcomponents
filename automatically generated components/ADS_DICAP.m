classdef ADS_DICAP < ADScomponent
    % ADS_DICAP matlab representation for the ADS DICAP component
    % Dielectric Labs Di-cap Capacitor
    % DICAP [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Width of metal plates and dielectric (Smorr) Unit: m
        W
        % Length of metal plates and dielectric (Smorr) Unit: m
        L
        % Thickness of dielectric (Smorr) Unit: m
        T
        % Dielectric constant (Smorr) Unit: 1
        Er
        % Dielectrc loss tagent value at 1 MHz (Smorr) Unit: 1
        TanDeL
        % Series resistance at 1 GHz (Smorr) Unit: Ohm
        R0
        % Physical Temperature (smorr) Unit: C
        Temp
        % Non-causal function impulse response order (s--ri) Unit: (unknown units)
        Secured
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.T(obj,val)
            obj = setParameter(obj,'T',val,0,'real');
        end
        function res = get.T(obj)
            res = getParameter(obj,'T');
        end
        function obj = set.Er(obj,val)
            obj = setParameter(obj,'Er',val,0,'real');
        end
        function res = get.Er(obj)
            res = getParameter(obj,'Er');
        end
        function obj = set.TanDeL(obj,val)
            obj = setParameter(obj,'TanDeL',val,0,'real');
        end
        function res = get.TanDeL(obj)
            res = getParameter(obj,'TanDeL');
        end
        function obj = set.R0(obj,val)
            obj = setParameter(obj,'R0',val,0,'real');
        end
        function res = get.R0(obj)
            res = getParameter(obj,'R0');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'integer');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end
