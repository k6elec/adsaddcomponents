classdef ADS_ebond < ADScomponent
    % ADS_ebond matlab representation for the ADS ebond component
    % User-defined model
    % ebond [:Name] n1 n2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Shape profile string (S--rs) Unit: unknown
        Shape
        % Projected xy length bondwire (Smorr) Unit: unknown
        Length
        % Angle within bondwire component +/-180 degree (Smorr) Unit: unknown
        Wire_Angle
        % Low freq. inductance wire (smorr) Unit: unknown
        L
        % Layout x coordinate pin 1 of bondwire (smorr) Unit: unknown
        x
        % Layout y coordinate pin 1 of bondwire (smorr) Unit: unknown
        y
        % Layout angle of bondwire component (smorr) Unit: unknown
        angle
        % Mirrored around x axis (sm-ri) Unit: unknown
        xMirror
        % Mirrored around x axis (sm-ri) Unit: unknown
        yMirror
    end
    methods
        function obj = set.Shape(obj,val)
            obj = setParameter(obj,'Shape',val,0,'string');
        end
        function res = get.Shape(obj)
            res = getParameter(obj,'Shape');
        end
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.Wire_Angle(obj,val)
            obj = setParameter(obj,'Wire_Angle',val,0,'real');
        end
        function res = get.Wire_Angle(obj)
            res = getParameter(obj,'Wire_Angle');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.x(obj,val)
            obj = setParameter(obj,'x',val,0,'real');
        end
        function res = get.x(obj)
            res = getParameter(obj,'x');
        end
        function obj = set.y(obj,val)
            obj = setParameter(obj,'y',val,0,'real');
        end
        function res = get.y(obj)
            res = getParameter(obj,'y');
        end
        function obj = set.angle(obj,val)
            obj = setParameter(obj,'angle',val,0,'real');
        end
        function res = get.angle(obj)
            res = getParameter(obj,'angle');
        end
        function obj = set.xMirror(obj,val)
            obj = setParameter(obj,'xMirror',val,0,'integer');
        end
        function res = get.xMirror(obj)
            res = getParameter(obj,'xMirror');
        end
        function obj = set.yMirror(obj,val)
            obj = setParameter(obj,'yMirror',val,0,'integer');
        end
        function res = get.yMirror(obj)
            res = getParameter(obj,'yMirror');
        end
    end
end
