classdef ADS_DILABMLC < ADScomponent
    % ADS_DILABMLC matlab representation for the ADS DILABMLC component
    % Dielectric Labs Multi Layer Chip Capacitor
    % DILABMLC [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Nominal capacitance (Smorr) Unit: F
        C0
        % Dielectric loss tagent value at 1 MHz (Smorr) Unit: 1
        TanDeL
        % Bulk resistivity of termination at 1 MHz (Smorr) Unit: Ohm
        R0
        % Termination loss resistance at 1 MHz (Smorr) Unit: Ohm
        Rt
        % Electrode loss resistance per electrode at 1 GHz (Smorr) Unit: Ohm
        Re
        % Mounting orientation: flat or edge (Smorr) Unit: 1
        Mount
        % Physical Temperature (smorr) Unit: C
        Temp
        % Non-causal function impulse response order (s--ri) Unit: (unknown units)
        Secured
    end
    methods
        function obj = set.C0(obj,val)
            obj = setParameter(obj,'C0',val,0,'real');
        end
        function res = get.C0(obj)
            res = getParameter(obj,'C0');
        end
        function obj = set.TanDeL(obj,val)
            obj = setParameter(obj,'TanDeL',val,0,'real');
        end
        function res = get.TanDeL(obj)
            res = getParameter(obj,'TanDeL');
        end
        function obj = set.R0(obj,val)
            obj = setParameter(obj,'R0',val,0,'real');
        end
        function res = get.R0(obj)
            res = getParameter(obj,'R0');
        end
        function obj = set.Rt(obj,val)
            obj = setParameter(obj,'Rt',val,0,'real');
        end
        function res = get.Rt(obj)
            res = getParameter(obj,'Rt');
        end
        function obj = set.Re(obj,val)
            obj = setParameter(obj,'Re',val,0,'real');
        end
        function res = get.Re(obj)
            res = getParameter(obj,'Re');
        end
        function obj = set.Mount(obj,val)
            obj = setParameter(obj,'Mount',val,0,'real');
        end
        function res = get.Mount(obj)
            res = getParameter(obj,'Mount');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'integer');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end
