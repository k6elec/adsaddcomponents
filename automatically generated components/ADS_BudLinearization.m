classdef ADS_BudLinearization < ADSnodeless
    % ADS_BudLinearization matlab representation for the ADS BudLinearization component
    % Linear Budget HB Controller
    % BudLinearization [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Analysis instance/path name to control (repeatable) (s---s) 
        SimInstanceName
        % Component to linearize (repeatable) (s---s) 
        LinearizeComponent
    end
    methods
        function obj = set.SimInstanceName(obj,val)
            obj = setParameter(obj,'SimInstanceName',val,1,'string');
        end
        function res = get.SimInstanceName(obj)
            res = getParameter(obj,'SimInstanceName');
        end
        function obj = set.LinearizeComponent(obj,val)
            obj = setParameter(obj,'LinearizeComponent',val,0,'string');
        end
        function res = get.LinearizeComponent(obj)
            res = getParameter(obj,'LinearizeComponent');
        end
    end
end
