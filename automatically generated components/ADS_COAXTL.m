classdef ADS_COAXTL < ADScomponent
    % ADS_COAXTL matlab representation for the ADS COAXTL component
    % Ideal coax transmission line
    % COAXTL [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Center conductor radius of coax transmission line (Smorr) Unit: m
        A
        % Inner radius of outer conductor of coax transmission line (Smorr) Unit: m
        Ri
        % Outer radius of outer conductor of coax transmission line (Smorr) Unit: m
        Ro
        % Length of coax transmission line (Smorr) Unit: m
        L
        % Conductivity of plating metal of coax transmission line (Smorr) Unit: Sie/m
        Cond1
        % Conductivity of base metal of coax transmission line (Smorr) Unit: Sie/m
        Cond2
        % Relative permeability of coax transmission line (smorr) Unit: 1
        Mur
        % Plating thickness of coax transmission line (Smorr) Unit: m
        T
        % Dielectric constant of coax transmission line (Smorr) Unit: 1
        Er
        % Loss Tangent of coax transmission line (smorr) Unit: 1
        TanD
        % 0: Frequency independent, 1: Svensson/Djordjevic (default) (sm-ri) Unit: unknown
        DielectricLossModel
        % Frequency at which Er and TanD are measured (smorr) Unit: hz
        FreqForEpsrTand
        % Low roll-off frequency in the Svensson/Djordjevic model (smorr) Unit: hz
        LowFreqForTand
        % High roll-off frequency in the Svensson/Djordjevic model (smorr) Unit: hz
        HighFreqForTand
    end
    methods
        function obj = set.A(obj,val)
            obj = setParameter(obj,'A',val,0,'real');
        end
        function res = get.A(obj)
            res = getParameter(obj,'A');
        end
        function obj = set.Ri(obj,val)
            obj = setParameter(obj,'Ri',val,0,'real');
        end
        function res = get.Ri(obj)
            res = getParameter(obj,'Ri');
        end
        function obj = set.Ro(obj,val)
            obj = setParameter(obj,'Ro',val,0,'real');
        end
        function res = get.Ro(obj)
            res = getParameter(obj,'Ro');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Cond1(obj,val)
            obj = setParameter(obj,'Cond1',val,0,'real');
        end
        function res = get.Cond1(obj)
            res = getParameter(obj,'Cond1');
        end
        function obj = set.Cond2(obj,val)
            obj = setParameter(obj,'Cond2',val,0,'real');
        end
        function res = get.Cond2(obj)
            res = getParameter(obj,'Cond2');
        end
        function obj = set.Mur(obj,val)
            obj = setParameter(obj,'Mur',val,0,'real');
        end
        function res = get.Mur(obj)
            res = getParameter(obj,'Mur');
        end
        function obj = set.T(obj,val)
            obj = setParameter(obj,'T',val,0,'real');
        end
        function res = get.T(obj)
            res = getParameter(obj,'T');
        end
        function obj = set.Er(obj,val)
            obj = setParameter(obj,'Er',val,0,'real');
        end
        function res = get.Er(obj)
            res = getParameter(obj,'Er');
        end
        function obj = set.TanD(obj,val)
            obj = setParameter(obj,'TanD',val,0,'real');
        end
        function res = get.TanD(obj)
            res = getParameter(obj,'TanD');
        end
        function obj = set.DielectricLossModel(obj,val)
            obj = setParameter(obj,'DielectricLossModel',val,0,'integer');
        end
        function res = get.DielectricLossModel(obj)
            res = getParameter(obj,'DielectricLossModel');
        end
        function obj = set.FreqForEpsrTand(obj,val)
            obj = setParameter(obj,'FreqForEpsrTand',val,0,'real');
        end
        function res = get.FreqForEpsrTand(obj)
            res = getParameter(obj,'FreqForEpsrTand');
        end
        function obj = set.LowFreqForTand(obj,val)
            obj = setParameter(obj,'LowFreqForTand',val,0,'real');
        end
        function res = get.LowFreqForTand(obj)
            res = getParameter(obj,'LowFreqForTand');
        end
        function obj = set.HighFreqForTand(obj,val)
            obj = setParameter(obj,'HighFreqForTand',val,0,'real');
        end
        function res = get.HighFreqForTand(obj)
            res = getParameter(obj,'HighFreqForTand');
        end
    end
end
