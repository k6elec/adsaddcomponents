classdef ADS_TL < ADScomponent
    % ADS_TL matlab representation for the ADS TL component
    % Transmission Line
    % TL [:Name] inner1 outer1 inner2 outer2
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Z0 (smorr) Unit: Ohms
        Z
        % Length (smorr) Unit: m
        L
        % Velocity Factor (smorr) 
        V
        % Series Loss per meter (smorr) Unit: Ohms/m
        R
        % Reference Frequency (smorr) Unit: Hz
        Freq
        % Shunt Loss per meter (smorr) Unit: S/m
        G
    end
    methods
        function obj = set.Z(obj,val)
            obj = setParameter(obj,'Z',val,0,'real');
        end
        function res = get.Z(obj)
            res = getParameter(obj,'Z');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.V(obj,val)
            obj = setParameter(obj,'V',val,0,'real');
        end
        function res = get.V(obj)
            res = getParameter(obj,'V');
        end
        function obj = set.R(obj,val)
            obj = setParameter(obj,'R',val,0,'real');
        end
        function res = get.R(obj)
            res = getParameter(obj,'R');
        end
        function obj = set.Freq(obj,val)
            obj = setParameter(obj,'Freq',val,0,'real');
        end
        function res = get.Freq(obj)
            res = getParameter(obj,'Freq');
        end
        function obj = set.G(obj,val)
            obj = setParameter(obj,'G',val,0,'real');
        end
        function res = get.G(obj)
            res = getParameter(obj,'G');
        end
    end
end
