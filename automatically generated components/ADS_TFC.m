classdef ADS_TFC < ADScomponent
    % ADS_TFC matlab representation for the ADS TFC component
    % Thin Film Capacitor
    % TFC [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Conductor Width (Smorr) Unit: m
        W
        % Conductor Length (Smorr) Unit: m
        L
        % Dielectric Thickness (Smorr) Unit: m
        T
        % Relative Dielectric Constant (Smorr) Unit: unknown
        Er
        % Metal resistivity of Conductor (relative to Gold) (Smorr) Unit: unknown
        Rho
        % Dielectric Loss Tangent value (Smorr) Unit: unknown
        TanD
        % Physical Temperature (smorr) Unit: C
        Temp
        % 0: Frequency independent, 1: Svensson/Djordjevic (default) (sm-ri) Unit: unknown
        DielectricLossModel
        % Frequency at which Er and TanD are measured (smorr) Unit: hz
        FreqForEpsrTand
        % Low roll-off frequency in the Svensson/Djordjevic model (smorr) Unit: hz
        LowFreqForTand
        % High roll-off frequency in the Svensson/Djordjevic model (smorr) Unit: hz
        HighFreqForTand
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.T(obj,val)
            obj = setParameter(obj,'T',val,0,'real');
        end
        function res = get.T(obj)
            res = getParameter(obj,'T');
        end
        function obj = set.Er(obj,val)
            obj = setParameter(obj,'Er',val,0,'real');
        end
        function res = get.Er(obj)
            res = getParameter(obj,'Er');
        end
        function obj = set.Rho(obj,val)
            obj = setParameter(obj,'Rho',val,0,'real');
        end
        function res = get.Rho(obj)
            res = getParameter(obj,'Rho');
        end
        function obj = set.TanD(obj,val)
            obj = setParameter(obj,'TanD',val,0,'real');
        end
        function res = get.TanD(obj)
            res = getParameter(obj,'TanD');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.DielectricLossModel(obj,val)
            obj = setParameter(obj,'DielectricLossModel',val,0,'integer');
        end
        function res = get.DielectricLossModel(obj)
            res = getParameter(obj,'DielectricLossModel');
        end
        function obj = set.FreqForEpsrTand(obj,val)
            obj = setParameter(obj,'FreqForEpsrTand',val,0,'real');
        end
        function res = get.FreqForEpsrTand(obj)
            res = getParameter(obj,'FreqForEpsrTand');
        end
        function obj = set.LowFreqForTand(obj,val)
            obj = setParameter(obj,'LowFreqForTand',val,0,'real');
        end
        function res = get.LowFreqForTand(obj)
            res = getParameter(obj,'LowFreqForTand');
        end
        function obj = set.HighFreqForTand(obj,val)
            obj = setParameter(obj,'HighFreqForTand',val,0,'real');
        end
        function res = get.HighFreqForTand(obj)
            res = getParameter(obj,'HighFreqForTand');
        end
    end
end
