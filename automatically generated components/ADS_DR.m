classdef ADS_DR < ADScomponent
    % ADS_DR matlab representation for the ADS DR component
    % Dielectric-Resonator-Coupled Transmission Line
    % DR [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Characteristic Impedance of the TL being coupled (Smorr) Unit: ohms
        Z
        % Coupling Coefficient (Smorr) 
        K
        % Dielectric Constant of the Dielectric Resonator (Smorr) 
        Er
        % TE Mode (Sm-ri) 
        Mode
        % Q-factor of the Dielectric Resonator (Smorr) 
        Qdr
        % mRadius of the Dielectric Resonator (Smorr) 
        Rad
        % Thickness of the Dielectric Resonator (Smorr) Unit: m
        H
        % Dielectric Constant of the Substrate Underneath (Smorr) 
        ErL
        % Thickness of the Substrate Underneath (Smorr) Unit: m
        HL
        % Dielectric Constant of the Superstrate Above (Smorr) 
        ErU
        % Thickness of the Superstrate Above (Smorr) Unit: m
        HU
        % Conductivity of the upper and lower metal plate (Smorr) 
        Cond
        % Non-causal function impulse response order (s--ri) Unit: (unknown units)
        Secured
    end
    methods
        function obj = set.Z(obj,val)
            obj = setParameter(obj,'Z',val,0,'real');
        end
        function res = get.Z(obj)
            res = getParameter(obj,'Z');
        end
        function obj = set.K(obj,val)
            obj = setParameter(obj,'K',val,0,'real');
        end
        function res = get.K(obj)
            res = getParameter(obj,'K');
        end
        function obj = set.Er(obj,val)
            obj = setParameter(obj,'Er',val,0,'real');
        end
        function res = get.Er(obj)
            res = getParameter(obj,'Er');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Qdr(obj,val)
            obj = setParameter(obj,'Qdr',val,0,'real');
        end
        function res = get.Qdr(obj)
            res = getParameter(obj,'Qdr');
        end
        function obj = set.Rad(obj,val)
            obj = setParameter(obj,'Rad',val,0,'real');
        end
        function res = get.Rad(obj)
            res = getParameter(obj,'Rad');
        end
        function obj = set.H(obj,val)
            obj = setParameter(obj,'H',val,0,'real');
        end
        function res = get.H(obj)
            res = getParameter(obj,'H');
        end
        function obj = set.ErL(obj,val)
            obj = setParameter(obj,'ErL',val,0,'real');
        end
        function res = get.ErL(obj)
            res = getParameter(obj,'ErL');
        end
        function obj = set.HL(obj,val)
            obj = setParameter(obj,'HL',val,0,'real');
        end
        function res = get.HL(obj)
            res = getParameter(obj,'HL');
        end
        function obj = set.ErU(obj,val)
            obj = setParameter(obj,'ErU',val,0,'real');
        end
        function res = get.ErU(obj)
            res = getParameter(obj,'ErU');
        end
        function obj = set.HU(obj,val)
            obj = setParameter(obj,'HU',val,0,'real');
        end
        function res = get.HU(obj)
            res = getParameter(obj,'HU');
        end
        function obj = set.Cond(obj,val)
            obj = setParameter(obj,'Cond',val,0,'real');
        end
        function res = get.Cond(obj)
            res = getParameter(obj,'Cond');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'integer');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end
