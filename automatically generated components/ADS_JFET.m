classdef ADS_JFET < ADScomponent
    % ADS_JFET matlab representation for the ADS JFET component
    % Junction Field Effect Transistor
    % ModelName [:Name] drain gate source
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Junction area factor (smorr) 
        Area
        % DC operating region 0=off 1=on 2=rev 3=ohmic (s---i) 
        Region
        % N type JFET (s---b) 
        NFET
        % P type JFET (s---b) 
        PFET
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Temperature rise above ambient (smorr) Unit: deg C
        Trise
        % Nonlinear spectral model on/off (s---i) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % Small signal gate to source conductance (---rr) Unit: S
        Ggs
        % Small signal gate to drain conductance (---rr) Unit: S
        Ggd
        % Small signal drain to source conductance (---rr) Unit: S
        Gds
        % Small signal Vgs to Ids transconductance (---rr) Unit: S
        Gm
        % Small signal Vgd to Isd transconductance (---rr) Unit: S
        Gmr
        % Small signal Vgd to Igs transconductance (---rr) Unit: S
        Ggsgd
        % Small signal Vds to Igs transconductance (---rr) Unit: S
        Ggsds
        % Small signal Vgs to Igd transconductance (---rr) Unit: S
        Ggdgs
        % Small signal Vds to Igd transconductance (---rr) Unit: S
        Ggdds
        % Small signal gate to source capacitance (---rr) Unit: F
        Cgs
        % Small signal gate to drain capacitance (---rr) Unit: F
        Cgd
    end
    methods
        function obj = set.Area(obj,val)
            obj = setParameter(obj,'Area',val,0,'real');
        end
        function res = get.Area(obj)
            res = getParameter(obj,'Area');
        end
        function obj = set.Region(obj,val)
            obj = setParameter(obj,'Region',val,0,'integer');
        end
        function res = get.Region(obj)
            res = getParameter(obj,'Region');
        end
        function obj = set.NFET(obj,val)
            obj = setParameter(obj,'NFET',val,0,'boolean');
        end
        function res = get.NFET(obj)
            res = getParameter(obj,'NFET');
        end
        function obj = set.PFET(obj,val)
            obj = setParameter(obj,'PFET',val,0,'boolean');
        end
        function res = get.PFET(obj)
            res = getParameter(obj,'PFET');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Ggs(obj,val)
            obj = setParameter(obj,'Ggs',val,0,'real');
        end
        function res = get.Ggs(obj)
            res = getParameter(obj,'Ggs');
        end
        function obj = set.Ggd(obj,val)
            obj = setParameter(obj,'Ggd',val,0,'real');
        end
        function res = get.Ggd(obj)
            res = getParameter(obj,'Ggd');
        end
        function obj = set.Gds(obj,val)
            obj = setParameter(obj,'Gds',val,0,'real');
        end
        function res = get.Gds(obj)
            res = getParameter(obj,'Gds');
        end
        function obj = set.Gm(obj,val)
            obj = setParameter(obj,'Gm',val,0,'real');
        end
        function res = get.Gm(obj)
            res = getParameter(obj,'Gm');
        end
        function obj = set.Gmr(obj,val)
            obj = setParameter(obj,'Gmr',val,0,'real');
        end
        function res = get.Gmr(obj)
            res = getParameter(obj,'Gmr');
        end
        function obj = set.Ggsgd(obj,val)
            obj = setParameter(obj,'Ggsgd',val,0,'real');
        end
        function res = get.Ggsgd(obj)
            res = getParameter(obj,'Ggsgd');
        end
        function obj = set.Ggsds(obj,val)
            obj = setParameter(obj,'Ggsds',val,0,'real');
        end
        function res = get.Ggsds(obj)
            res = getParameter(obj,'Ggsds');
        end
        function obj = set.Ggdgs(obj,val)
            obj = setParameter(obj,'Ggdgs',val,0,'real');
        end
        function res = get.Ggdgs(obj)
            res = getParameter(obj,'Ggdgs');
        end
        function obj = set.Ggdds(obj,val)
            obj = setParameter(obj,'Ggdds',val,0,'real');
        end
        function res = get.Ggdds(obj)
            res = getParameter(obj,'Ggdds');
        end
        function obj = set.Cgs(obj,val)
            obj = setParameter(obj,'Cgs',val,0,'real');
        end
        function res = get.Cgs(obj)
            res = getParameter(obj,'Cgs');
        end
        function obj = set.Cgd(obj,val)
            obj = setParameter(obj,'Cgd',val,0,'real');
        end
        function res = get.Cgd(obj)
            res = getParameter(obj,'Cgd');
        end
    end
end
