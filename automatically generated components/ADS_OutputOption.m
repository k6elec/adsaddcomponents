classdef ADS_OutputOption < ADSnodeless
    % ADS_OutputOption matlab representation for the ADS OutputOption component
    % Output Option Definition
    % OutputOption [:Name]
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % A Vector of template names used by the DDS (s--rs) 
        DatasetTemplate
        % Attribute Name to be saved to the dataset (s--rs) 
        AttributeName
        % Attribute Value to be saved to the dataset (s--rs) 
        AttributeValue
    end
    methods
        function obj = set.DatasetTemplate(obj,val)
            obj = setParameter(obj,'DatasetTemplate',val,0,'string');
        end
        function res = get.DatasetTemplate(obj)
            res = getParameter(obj,'DatasetTemplate');
        end
        function obj = set.AttributeName(obj,val)
            obj = setParameter(obj,'AttributeName',val,0,'string');
        end
        function res = get.AttributeName(obj)
            res = getParameter(obj,'AttributeName');
        end
        function obj = set.AttributeValue(obj,val)
            obj = setParameter(obj,'AttributeValue',val,0,'string');
        end
        function res = get.AttributeValue(obj)
            res = getParameter(obj,'AttributeValue');
        end
    end
end
