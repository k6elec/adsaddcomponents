classdef ADS_TQAVIA < ADScomponent
    % ADS_TQAVIA matlab representation for the ADS TQAVIA component
    % Triquint AB to 1ME Via
    % TQAVIA [:Name] n1 n2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % AVIA  Width (smorr) Unit: m
        W
        % AVIA  Length (smorr) Unit: m
        L
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
    end
end
