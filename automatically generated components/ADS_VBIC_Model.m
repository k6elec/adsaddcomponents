classdef ADS_VBIC_Model < ADSmodel
    % ADS_VBIC_Model matlab representation for the ADS VBIC_Model component
    % VBIC Bipolar Junction Transistor model
    % model ModelName VBIC ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % NPN bipolar transistor (s---b) 
        NPN
        % PNP bipolar transistor (s---b) 
        PNP
        % Parameter measurement temperature (smorr) Unit: deg C
        Tnom
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Constant part of the collector resistance (smorr) Unit: Ohms/m^2
        Rcx
        % Intrinsic collector resistance (smorr) 
        Rci
        % Epi drift saturation voltage (smorr) 
        Vo
        % Epi doping parameter (smorr) 
        Gamm
        % High current RC factor (smorr) 
        Hrcf
        % Constant part of the base resistance (smorr) Unit: Ohms/m^2
        Rbx
        % Intrinsic base resistance (smorr) Unit: Ohms/m^2
        Rbi
        % Emitter series resistance (smorr) Unit: Ohms/m^2
        Re
        % Substrate series resistance (smorr) Unit: Ohms/m^2
        Rs
        % Parasitic base resistance (smorr) 
        Rbp
        % Transport saturation current (smorr) Unit: A
        Is
        % Forward emission coefficient (smorr) 
        Nf
        % Reverse emission coefficient (smorr) 
        Nr
        % Junction capacitor forward-bias threshold (smorr) 
        Fc
        % Small Signal Base Emmiter Capacitance (smorr) Unit: F
        Cbeo
        % Base-emitter zero-bias junction capacitance (smorr) Unit: F
        Cje
        % Emitter-base grading coefficient (smorr) 
        Pe
        % Base-emitter junction exponent (smorr) 
        Me
        % Base-emitter capacitance smoothing factor (smorr) 
        Aje
        % Small Signal Base Collector Capacitance (smorr) Unit: F
        Cbco
        % Base-collector zero-bias junction capacitance (smorr) Unit: F
        Cjc
        % Collector charge at zero bias (smorr) 
        Qco
        % Emitter-base zero-bias extrinsic capacitance (smorr) Unit: F
        Cjep
        % Collector-base grading coefficient (smorr) 
        Pc
        % Base-collector junction exponent (smorr) 
        Mc
        % Base-collector capacitance smoothing factor (smorr) 
        Ajc
        % Collector-base zero-bias extrinsic capacitance (smorr) Unit: F
        Cjcp
        % Collector-substrate grading coefficient (smorr) 
        Ps
        % Collector-substrate junction exponent (smorr) 
        Ms
        % Collector-substrate capacitance smoothing factor (smorr) 
        Ajs
        % Ideal base-emitter saturation current (smorr) Unit: A
        Ibei
        % Portion of Ibei from Vbei, (1-Wbe) from Vbex (smorr) 
        Wbe
        % Ideal base-emitter emission coefficient (smorr) 
        Nei
        % Non-ideal base-emitter saturation current (smorr) Unit: A
        Iben
        % Non-ideal base-emitter emission coefficient (smorr) 
        Nen
        % Ideal base-collector saturation current (smorr) Unit: A
        Ibci
        % Ideal base-collector emission coefficient (smorr) 
        Nci
        % Non-ideal base-collector saturation current (smorr) Unit: A
        Ibcn
        % Non-ideal base-collector emission coefficient (smorr) 
        Ncn
        % Parasitic transport saturation current (smorr) Unit: A
        Isp
        % Portion of Iccp from Vbep, (1-Wsp) from Vbci (smorr) 
        Wsp
        % Parasitic forward emission coefficient (smorr) 
        Nfp
        % Ideal parasitic base-emitter saturation current (smorr) Unit: A
        Ibeip
        % Non-ideal parasitic base-emitter saturation current (smorr) Unit: A
        Ibenp
        % Ideal parasitic base-collector saturation current (smorr) Unit: A
        Ibcip
        % Ideal parasitic base-collector emission coefficient (smorr) 
        Ncip
        % Non-ideal parasitic base-collector saturation current (smorr) Unit: A
        Ibcnp
        % Base-collector weak avalanche parameter 1 (smorr) 
        Avc1
        % Base-collector weak avalanche parameter 2 (smorr) 
        Avc2
        % Non-ideal parasitic base-collector emission coef (smorr) 
        Ncnp
        % Forward early voltage (zero means infinity) (smorr) Unit: V
        Vef
        % Reverse early voltage (zero means infinity) (smorr) Unit: V
        Ver
        % Forward high-injection knee current (smorr) Unit: A
        Ikf
        % Reverse high-injection knee current (smorr) Unit: A
        Ikr
        % Parasitic knee current (smorr) Unit: A
        Ikp
        % Ideal forward transit time (smorr) Unit: s
        Tf
        % Variation of Tf with base-width modulation (smorr) 
        Qtf
        % Coefficient of Tf bias dependence (smorr) 
        Xtf
        % Coefficient of Tf dependence on Vbc (smorr) 
        Vtf
        % Coefficient of Tf dependence on Icc (smorr) 
        Itf
        % Reverse transit time (smorr) Unit: s
        Tr
        % Forward excess-phase delay time (smorr) Unit: s
        Td
        % Flicker noise coefficient (smorr) 
        Kfn
        % Flicker noise exponent (smorr) 
        Afn
        % Flicker noise frequency exponent (smorr) 
        Bfn
        % Temperature exponent of emitter resistance (smorr) 
        Xre
        % Temperature exponent of base resistance (smorr) 
        Xrb
        % Temperature exponent of collector resistance (smorr) 
        Xrc
        % Temperature exponent of substrate resistance (smorr) 
        Xrs
        % Temperature exponent of Vo (smorr) 
        Xvo
        % Activation energy for Is (smorr) Unit: eV
        Ea
        % Activation energy for Ibei (smorr) Unit: eV
        Eaie
        % Activation energy for Ibci/Ibeip (smorr) Unit: eV
        Eaic
        % Activation energy for Ibcip (smorr) Unit: eV
        Eais
        % Activation energy for Iben (smorr) Unit: eV
        Eane
        % Activation energy for Ibcn/Ibenp (smorr) Unit: eV
        Eanc
        % Activation energy for Ibcnp (smorr) Unit: eV
        Eans
        % Temperature exponent of Is (smorr) 
        Xis
        % Temperature exponent of Ibei/Ibci/Ibeip/Ibcip (smorr) 
        Xii
        % Temperature exponent of Iben/Ibcn/Ibenp/Ibcnp (smorr) 
        Xin
        % Temperature coefficient of Nf (smorr) 
        Tnf
        % Temperature coefficient of Avc2 (smorr) 
        Tavc
        % Thermal resistance (smorr) Unit: Ohms
        Rth
        % Thermal capacitance (smorr) Unit: F
        Cth
        % Flag denoting self-heating (s--ri) 
        Selft
        % Maximum expected device temperature (smorr) Unit: deg C
        Dtmax
        % Explosion current (smorr) Unit: A
        Imax
        % Explosion current (smorr) Unit: A
        Imelt
        % Flag for limiting Kbci and Kbcx (s---b) 
        Imeltrci
        % Substrate junction forward bias (warning) (s--rr) Unit: V
        wVsubfwd
        % Substrate junction reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvsub
        % Base-emitter reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvbe
        % Base-collector reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvbc
        % Collector-emitter breakdown voltage (warning) (s--rr) Unit: V
        wBvce
        % Base-collector forward bias (warning) (s--rr) Unit: V
        wVbcfwd
        % Maximum base current (warning) (s--rr) Unit: A
        wIbmax
        % Maximum collector current (warning) (s--rr) Unit: A
        wIcmax
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
    end
    methods
        function obj = set.NPN(obj,val)
            obj = setParameter(obj,'NPN',val,0,'boolean');
        end
        function res = get.NPN(obj)
            res = getParameter(obj,'NPN');
        end
        function obj = set.PNP(obj,val)
            obj = setParameter(obj,'PNP',val,0,'boolean');
        end
        function res = get.PNP(obj)
            res = getParameter(obj,'PNP');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Rcx(obj,val)
            obj = setParameter(obj,'Rcx',val,0,'real');
        end
        function res = get.Rcx(obj)
            res = getParameter(obj,'Rcx');
        end
        function obj = set.Rci(obj,val)
            obj = setParameter(obj,'Rci',val,0,'real');
        end
        function res = get.Rci(obj)
            res = getParameter(obj,'Rci');
        end
        function obj = set.Vo(obj,val)
            obj = setParameter(obj,'Vo',val,0,'real');
        end
        function res = get.Vo(obj)
            res = getParameter(obj,'Vo');
        end
        function obj = set.Gamm(obj,val)
            obj = setParameter(obj,'Gamm',val,0,'real');
        end
        function res = get.Gamm(obj)
            res = getParameter(obj,'Gamm');
        end
        function obj = set.Hrcf(obj,val)
            obj = setParameter(obj,'Hrcf',val,0,'real');
        end
        function res = get.Hrcf(obj)
            res = getParameter(obj,'Hrcf');
        end
        function obj = set.Rbx(obj,val)
            obj = setParameter(obj,'Rbx',val,0,'real');
        end
        function res = get.Rbx(obj)
            res = getParameter(obj,'Rbx');
        end
        function obj = set.Rbi(obj,val)
            obj = setParameter(obj,'Rbi',val,0,'real');
        end
        function res = get.Rbi(obj)
            res = getParameter(obj,'Rbi');
        end
        function obj = set.Re(obj,val)
            obj = setParameter(obj,'Re',val,0,'real');
        end
        function res = get.Re(obj)
            res = getParameter(obj,'Re');
        end
        function obj = set.Rs(obj,val)
            obj = setParameter(obj,'Rs',val,0,'real');
        end
        function res = get.Rs(obj)
            res = getParameter(obj,'Rs');
        end
        function obj = set.Rbp(obj,val)
            obj = setParameter(obj,'Rbp',val,0,'real');
        end
        function res = get.Rbp(obj)
            res = getParameter(obj,'Rbp');
        end
        function obj = set.Is(obj,val)
            obj = setParameter(obj,'Is',val,0,'real');
        end
        function res = get.Is(obj)
            res = getParameter(obj,'Is');
        end
        function obj = set.Nf(obj,val)
            obj = setParameter(obj,'Nf',val,0,'real');
        end
        function res = get.Nf(obj)
            res = getParameter(obj,'Nf');
        end
        function obj = set.Nr(obj,val)
            obj = setParameter(obj,'Nr',val,0,'real');
        end
        function res = get.Nr(obj)
            res = getParameter(obj,'Nr');
        end
        function obj = set.Fc(obj,val)
            obj = setParameter(obj,'Fc',val,0,'real');
        end
        function res = get.Fc(obj)
            res = getParameter(obj,'Fc');
        end
        function obj = set.Cbeo(obj,val)
            obj = setParameter(obj,'Cbeo',val,0,'real');
        end
        function res = get.Cbeo(obj)
            res = getParameter(obj,'Cbeo');
        end
        function obj = set.Cje(obj,val)
            obj = setParameter(obj,'Cje',val,0,'real');
        end
        function res = get.Cje(obj)
            res = getParameter(obj,'Cje');
        end
        function obj = set.Pe(obj,val)
            obj = setParameter(obj,'Pe',val,0,'real');
        end
        function res = get.Pe(obj)
            res = getParameter(obj,'Pe');
        end
        function obj = set.Me(obj,val)
            obj = setParameter(obj,'Me',val,0,'real');
        end
        function res = get.Me(obj)
            res = getParameter(obj,'Me');
        end
        function obj = set.Aje(obj,val)
            obj = setParameter(obj,'Aje',val,0,'real');
        end
        function res = get.Aje(obj)
            res = getParameter(obj,'Aje');
        end
        function obj = set.Cbco(obj,val)
            obj = setParameter(obj,'Cbco',val,0,'real');
        end
        function res = get.Cbco(obj)
            res = getParameter(obj,'Cbco');
        end
        function obj = set.Cjc(obj,val)
            obj = setParameter(obj,'Cjc',val,0,'real');
        end
        function res = get.Cjc(obj)
            res = getParameter(obj,'Cjc');
        end
        function obj = set.Qco(obj,val)
            obj = setParameter(obj,'Qco',val,0,'real');
        end
        function res = get.Qco(obj)
            res = getParameter(obj,'Qco');
        end
        function obj = set.Cjep(obj,val)
            obj = setParameter(obj,'Cjep',val,0,'real');
        end
        function res = get.Cjep(obj)
            res = getParameter(obj,'Cjep');
        end
        function obj = set.Pc(obj,val)
            obj = setParameter(obj,'Pc',val,0,'real');
        end
        function res = get.Pc(obj)
            res = getParameter(obj,'Pc');
        end
        function obj = set.Mc(obj,val)
            obj = setParameter(obj,'Mc',val,0,'real');
        end
        function res = get.Mc(obj)
            res = getParameter(obj,'Mc');
        end
        function obj = set.Ajc(obj,val)
            obj = setParameter(obj,'Ajc',val,0,'real');
        end
        function res = get.Ajc(obj)
            res = getParameter(obj,'Ajc');
        end
        function obj = set.Cjcp(obj,val)
            obj = setParameter(obj,'Cjcp',val,0,'real');
        end
        function res = get.Cjcp(obj)
            res = getParameter(obj,'Cjcp');
        end
        function obj = set.Ps(obj,val)
            obj = setParameter(obj,'Ps',val,0,'real');
        end
        function res = get.Ps(obj)
            res = getParameter(obj,'Ps');
        end
        function obj = set.Ms(obj,val)
            obj = setParameter(obj,'Ms',val,0,'real');
        end
        function res = get.Ms(obj)
            res = getParameter(obj,'Ms');
        end
        function obj = set.Ajs(obj,val)
            obj = setParameter(obj,'Ajs',val,0,'real');
        end
        function res = get.Ajs(obj)
            res = getParameter(obj,'Ajs');
        end
        function obj = set.Ibei(obj,val)
            obj = setParameter(obj,'Ibei',val,0,'real');
        end
        function res = get.Ibei(obj)
            res = getParameter(obj,'Ibei');
        end
        function obj = set.Wbe(obj,val)
            obj = setParameter(obj,'Wbe',val,0,'real');
        end
        function res = get.Wbe(obj)
            res = getParameter(obj,'Wbe');
        end
        function obj = set.Nei(obj,val)
            obj = setParameter(obj,'Nei',val,0,'real');
        end
        function res = get.Nei(obj)
            res = getParameter(obj,'Nei');
        end
        function obj = set.Iben(obj,val)
            obj = setParameter(obj,'Iben',val,0,'real');
        end
        function res = get.Iben(obj)
            res = getParameter(obj,'Iben');
        end
        function obj = set.Nen(obj,val)
            obj = setParameter(obj,'Nen',val,0,'real');
        end
        function res = get.Nen(obj)
            res = getParameter(obj,'Nen');
        end
        function obj = set.Ibci(obj,val)
            obj = setParameter(obj,'Ibci',val,0,'real');
        end
        function res = get.Ibci(obj)
            res = getParameter(obj,'Ibci');
        end
        function obj = set.Nci(obj,val)
            obj = setParameter(obj,'Nci',val,0,'real');
        end
        function res = get.Nci(obj)
            res = getParameter(obj,'Nci');
        end
        function obj = set.Ibcn(obj,val)
            obj = setParameter(obj,'Ibcn',val,0,'real');
        end
        function res = get.Ibcn(obj)
            res = getParameter(obj,'Ibcn');
        end
        function obj = set.Ncn(obj,val)
            obj = setParameter(obj,'Ncn',val,0,'real');
        end
        function res = get.Ncn(obj)
            res = getParameter(obj,'Ncn');
        end
        function obj = set.Isp(obj,val)
            obj = setParameter(obj,'Isp',val,0,'real');
        end
        function res = get.Isp(obj)
            res = getParameter(obj,'Isp');
        end
        function obj = set.Wsp(obj,val)
            obj = setParameter(obj,'Wsp',val,0,'real');
        end
        function res = get.Wsp(obj)
            res = getParameter(obj,'Wsp');
        end
        function obj = set.Nfp(obj,val)
            obj = setParameter(obj,'Nfp',val,0,'real');
        end
        function res = get.Nfp(obj)
            res = getParameter(obj,'Nfp');
        end
        function obj = set.Ibeip(obj,val)
            obj = setParameter(obj,'Ibeip',val,0,'real');
        end
        function res = get.Ibeip(obj)
            res = getParameter(obj,'Ibeip');
        end
        function obj = set.Ibenp(obj,val)
            obj = setParameter(obj,'Ibenp',val,0,'real');
        end
        function res = get.Ibenp(obj)
            res = getParameter(obj,'Ibenp');
        end
        function obj = set.Ibcip(obj,val)
            obj = setParameter(obj,'Ibcip',val,0,'real');
        end
        function res = get.Ibcip(obj)
            res = getParameter(obj,'Ibcip');
        end
        function obj = set.Ncip(obj,val)
            obj = setParameter(obj,'Ncip',val,0,'real');
        end
        function res = get.Ncip(obj)
            res = getParameter(obj,'Ncip');
        end
        function obj = set.Ibcnp(obj,val)
            obj = setParameter(obj,'Ibcnp',val,0,'real');
        end
        function res = get.Ibcnp(obj)
            res = getParameter(obj,'Ibcnp');
        end
        function obj = set.Avc1(obj,val)
            obj = setParameter(obj,'Avc1',val,0,'real');
        end
        function res = get.Avc1(obj)
            res = getParameter(obj,'Avc1');
        end
        function obj = set.Avc2(obj,val)
            obj = setParameter(obj,'Avc2',val,0,'real');
        end
        function res = get.Avc2(obj)
            res = getParameter(obj,'Avc2');
        end
        function obj = set.Ncnp(obj,val)
            obj = setParameter(obj,'Ncnp',val,0,'real');
        end
        function res = get.Ncnp(obj)
            res = getParameter(obj,'Ncnp');
        end
        function obj = set.Vef(obj,val)
            obj = setParameter(obj,'Vef',val,0,'real');
        end
        function res = get.Vef(obj)
            res = getParameter(obj,'Vef');
        end
        function obj = set.Ver(obj,val)
            obj = setParameter(obj,'Ver',val,0,'real');
        end
        function res = get.Ver(obj)
            res = getParameter(obj,'Ver');
        end
        function obj = set.Ikf(obj,val)
            obj = setParameter(obj,'Ikf',val,0,'real');
        end
        function res = get.Ikf(obj)
            res = getParameter(obj,'Ikf');
        end
        function obj = set.Ikr(obj,val)
            obj = setParameter(obj,'Ikr',val,0,'real');
        end
        function res = get.Ikr(obj)
            res = getParameter(obj,'Ikr');
        end
        function obj = set.Ikp(obj,val)
            obj = setParameter(obj,'Ikp',val,0,'real');
        end
        function res = get.Ikp(obj)
            res = getParameter(obj,'Ikp');
        end
        function obj = set.Tf(obj,val)
            obj = setParameter(obj,'Tf',val,0,'real');
        end
        function res = get.Tf(obj)
            res = getParameter(obj,'Tf');
        end
        function obj = set.Qtf(obj,val)
            obj = setParameter(obj,'Qtf',val,0,'real');
        end
        function res = get.Qtf(obj)
            res = getParameter(obj,'Qtf');
        end
        function obj = set.Xtf(obj,val)
            obj = setParameter(obj,'Xtf',val,0,'real');
        end
        function res = get.Xtf(obj)
            res = getParameter(obj,'Xtf');
        end
        function obj = set.Vtf(obj,val)
            obj = setParameter(obj,'Vtf',val,0,'real');
        end
        function res = get.Vtf(obj)
            res = getParameter(obj,'Vtf');
        end
        function obj = set.Itf(obj,val)
            obj = setParameter(obj,'Itf',val,0,'real');
        end
        function res = get.Itf(obj)
            res = getParameter(obj,'Itf');
        end
        function obj = set.Tr(obj,val)
            obj = setParameter(obj,'Tr',val,0,'real');
        end
        function res = get.Tr(obj)
            res = getParameter(obj,'Tr');
        end
        function obj = set.Td(obj,val)
            obj = setParameter(obj,'Td',val,0,'real');
        end
        function res = get.Td(obj)
            res = getParameter(obj,'Td');
        end
        function obj = set.Kfn(obj,val)
            obj = setParameter(obj,'Kfn',val,0,'real');
        end
        function res = get.Kfn(obj)
            res = getParameter(obj,'Kfn');
        end
        function obj = set.Afn(obj,val)
            obj = setParameter(obj,'Afn',val,0,'real');
        end
        function res = get.Afn(obj)
            res = getParameter(obj,'Afn');
        end
        function obj = set.Bfn(obj,val)
            obj = setParameter(obj,'Bfn',val,0,'real');
        end
        function res = get.Bfn(obj)
            res = getParameter(obj,'Bfn');
        end
        function obj = set.Xre(obj,val)
            obj = setParameter(obj,'Xre',val,0,'real');
        end
        function res = get.Xre(obj)
            res = getParameter(obj,'Xre');
        end
        function obj = set.Xrb(obj,val)
            obj = setParameter(obj,'Xrb',val,0,'real');
        end
        function res = get.Xrb(obj)
            res = getParameter(obj,'Xrb');
        end
        function obj = set.Xrc(obj,val)
            obj = setParameter(obj,'Xrc',val,0,'real');
        end
        function res = get.Xrc(obj)
            res = getParameter(obj,'Xrc');
        end
        function obj = set.Xrs(obj,val)
            obj = setParameter(obj,'Xrs',val,0,'real');
        end
        function res = get.Xrs(obj)
            res = getParameter(obj,'Xrs');
        end
        function obj = set.Xvo(obj,val)
            obj = setParameter(obj,'Xvo',val,0,'real');
        end
        function res = get.Xvo(obj)
            res = getParameter(obj,'Xvo');
        end
        function obj = set.Ea(obj,val)
            obj = setParameter(obj,'Ea',val,0,'real');
        end
        function res = get.Ea(obj)
            res = getParameter(obj,'Ea');
        end
        function obj = set.Eaie(obj,val)
            obj = setParameter(obj,'Eaie',val,0,'real');
        end
        function res = get.Eaie(obj)
            res = getParameter(obj,'Eaie');
        end
        function obj = set.Eaic(obj,val)
            obj = setParameter(obj,'Eaic',val,0,'real');
        end
        function res = get.Eaic(obj)
            res = getParameter(obj,'Eaic');
        end
        function obj = set.Eais(obj,val)
            obj = setParameter(obj,'Eais',val,0,'real');
        end
        function res = get.Eais(obj)
            res = getParameter(obj,'Eais');
        end
        function obj = set.Eane(obj,val)
            obj = setParameter(obj,'Eane',val,0,'real');
        end
        function res = get.Eane(obj)
            res = getParameter(obj,'Eane');
        end
        function obj = set.Eanc(obj,val)
            obj = setParameter(obj,'Eanc',val,0,'real');
        end
        function res = get.Eanc(obj)
            res = getParameter(obj,'Eanc');
        end
        function obj = set.Eans(obj,val)
            obj = setParameter(obj,'Eans',val,0,'real');
        end
        function res = get.Eans(obj)
            res = getParameter(obj,'Eans');
        end
        function obj = set.Xis(obj,val)
            obj = setParameter(obj,'Xis',val,0,'real');
        end
        function res = get.Xis(obj)
            res = getParameter(obj,'Xis');
        end
        function obj = set.Xii(obj,val)
            obj = setParameter(obj,'Xii',val,0,'real');
        end
        function res = get.Xii(obj)
            res = getParameter(obj,'Xii');
        end
        function obj = set.Xin(obj,val)
            obj = setParameter(obj,'Xin',val,0,'real');
        end
        function res = get.Xin(obj)
            res = getParameter(obj,'Xin');
        end
        function obj = set.Tnf(obj,val)
            obj = setParameter(obj,'Tnf',val,0,'real');
        end
        function res = get.Tnf(obj)
            res = getParameter(obj,'Tnf');
        end
        function obj = set.Tavc(obj,val)
            obj = setParameter(obj,'Tavc',val,0,'real');
        end
        function res = get.Tavc(obj)
            res = getParameter(obj,'Tavc');
        end
        function obj = set.Rth(obj,val)
            obj = setParameter(obj,'Rth',val,0,'real');
        end
        function res = get.Rth(obj)
            res = getParameter(obj,'Rth');
        end
        function obj = set.Cth(obj,val)
            obj = setParameter(obj,'Cth',val,0,'real');
        end
        function res = get.Cth(obj)
            res = getParameter(obj,'Cth');
        end
        function obj = set.Selft(obj,val)
            obj = setParameter(obj,'Selft',val,0,'integer');
        end
        function res = get.Selft(obj)
            res = getParameter(obj,'Selft');
        end
        function obj = set.Dtmax(obj,val)
            obj = setParameter(obj,'Dtmax',val,0,'real');
        end
        function res = get.Dtmax(obj)
            res = getParameter(obj,'Dtmax');
        end
        function obj = set.Imax(obj,val)
            obj = setParameter(obj,'Imax',val,0,'real');
        end
        function res = get.Imax(obj)
            res = getParameter(obj,'Imax');
        end
        function obj = set.Imelt(obj,val)
            obj = setParameter(obj,'Imelt',val,0,'real');
        end
        function res = get.Imelt(obj)
            res = getParameter(obj,'Imelt');
        end
        function obj = set.Imeltrci(obj,val)
            obj = setParameter(obj,'Imeltrci',val,0,'boolean');
        end
        function res = get.Imeltrci(obj)
            res = getParameter(obj,'Imeltrci');
        end
        function obj = set.wVsubfwd(obj,val)
            obj = setParameter(obj,'wVsubfwd',val,0,'real');
        end
        function res = get.wVsubfwd(obj)
            res = getParameter(obj,'wVsubfwd');
        end
        function obj = set.wBvsub(obj,val)
            obj = setParameter(obj,'wBvsub',val,0,'real');
        end
        function res = get.wBvsub(obj)
            res = getParameter(obj,'wBvsub');
        end
        function obj = set.wBvbe(obj,val)
            obj = setParameter(obj,'wBvbe',val,0,'real');
        end
        function res = get.wBvbe(obj)
            res = getParameter(obj,'wBvbe');
        end
        function obj = set.wBvbc(obj,val)
            obj = setParameter(obj,'wBvbc',val,0,'real');
        end
        function res = get.wBvbc(obj)
            res = getParameter(obj,'wBvbc');
        end
        function obj = set.wBvce(obj,val)
            obj = setParameter(obj,'wBvce',val,0,'real');
        end
        function res = get.wBvce(obj)
            res = getParameter(obj,'wBvce');
        end
        function obj = set.wVbcfwd(obj,val)
            obj = setParameter(obj,'wVbcfwd',val,0,'real');
        end
        function res = get.wVbcfwd(obj)
            res = getParameter(obj,'wVbcfwd');
        end
        function obj = set.wIbmax(obj,val)
            obj = setParameter(obj,'wIbmax',val,0,'real');
        end
        function res = get.wIbmax(obj)
            res = getParameter(obj,'wIbmax');
        end
        function obj = set.wIcmax(obj,val)
            obj = setParameter(obj,'wIcmax',val,0,'real');
        end
        function res = get.wIcmax(obj)
            res = getParameter(obj,'wIcmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
    end
end
