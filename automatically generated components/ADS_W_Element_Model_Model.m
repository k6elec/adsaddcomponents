classdef ADS_W_Element_Model_Model < ADSmodel
    % ADS_W_Element_Model_Model matlab representation for the ADS W_Element_Model_Model component
    % Multi-conductor transmission line model
    % model ModelName W_Element_Model ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Number of signal lines (s--ri) 
        N
        % Model type: `RLGC' - static, `TABLE' - tabular (frequency dependent matrices) (s--rs) 
        Modeltype
        % Name of the file containing tabular frequency dependent L matrices (sm-rs) 
        Lfile
        % Name of the file containing tabular frequency dependent C matrices (sm-rs) 
        Cfile
        % Name of the file containing tabular frequency dependent R matrices (sm-rs) 
        Rfile
        % Name of the file containing tabular frequency dependent G matrices (sm-rs) 
        Gfile
        % Array of the lower triangular elements of static/tabular L matrix (smorr) Unit: H
        Ldata
        % Array of the lower triangular elements of static/tabular C matrix (smorr) Unit: F/m
        Cdata
        % Array of the lower triangular elements of static Rdc or tabular R matrix (smorr) Unit: Ohms/m
        Rdata
        % Array of the lower triangular elements of static Gdc or tabular G matrix (smorr) Unit: S/m
        Gdata
        % Array of the lower triangular elements of static Rs (skin-effect resistance) matrix (smorr) Unit: Ohms/m
        RSdata
        % Array of the lower triangular elements of static Gd matrix (smorr) Unit: S/m
        GDdata
        % Name of the SP model containing the L matrix tabular data (s--rs) 
        Lmodel
        % Name of the SP model containing the C matrix tabular data (s--rs) 
        Cmodel
        % Name of the SP model containing the R matrix tabular data (s--rs) 
        Rmodel
        % Name of the SP model containing the G matrix tabular data (s--rs) 
        Gmodel
        % Type of sweep in Ldata/Lfile: 0 - linear, 1,2,3 - log, 4 - points (sm-ri) 
        LfreqSweep
        % Frequency of the first data set in the Ldata/Lfile (LfreqSweep=0,1,2,3) (smorr) Unit: Hz
        LfreqStart
        % Frequency of the last data set in the Ldata/Lfile (LfreqSweep=0,1) (smorr) Unit: Hz
        LfreqStop
        % Number of frequency points per decade in Ldata/Lfile (LfreqSweep=2) (sm-ri) 
        LfreqsPerDec
        % Number of frequency points per octave in Ldata/Lfile (LfreqSweep=3) (sm-ri) 
        LfreqsPerOct
        % Type of sweep in Cdata/Cfile: 0 - linear, 1,2,3 - log, 4 - points (sm-ri) 
        CfreqSweep
        % Frequency of the first data set in the Cdata/Cfile (CfreqSweep=0,1,2,3) (smorr) Unit: Hz
        CfreqStart
        % Frequency of the last data set in the Cdata/Cfile (CfreqSweep=0,1) (smorr) Unit: Hz
        CfreqStop
        % Number of frequency points per decade in Cdata/Cfile (CfreqSweep=2) (sm-ri) 
        CfreqsPerDec
        % Number of frequency points per octave in Cdata/Cfile (CfreqSweep=3) (sm-ri) 
        CfreqsPerOct
        % Type of sweep in Rdata/Rfile: 0 - linear, 1,2,3 - log, 4 - points (sm-ri) 
        RfreqSweep
        % Frequency of the first data set in the Rdata/Rfile (RfreqSweep=0,1,2,3) (smorr) Unit: Hz
        RfreqStart
        % Frequency of the last data set in the Rdata/Rfile (RfreqSweep=0,1) (smorr) Unit: Hz
        RfreqStop
        % Number of frequency points per decade in Rdata/Rfile (RfreqSweep=2) (sm-ri) 
        RfreqsPerDec
        % Number of frequency points per octave in Rdata/Rfile (RfreqSweep=3) (sm-ri) 
        RfreqsPerOct
        % Type of sweep in Gdata/Gfile: 0 - linear, 1,2,3 - log, 4 - points (sm-ri) 
        GfreqSweep
        % Frequency of the first data set in the Gdata/Gfile (GfreqSweep=0,1,2,3) (smorr) Unit: Hz
        GfreqStart
        % Frequency of the last data set in the Gdata/Gfile (GfreqSweep=0,1) (smorr) Unit: Hz
        GfreqStop
        % Number of frequency points per decade in Gdata/Gfile (GfreqSweep=2) (sm-ri) 
        GfreqsPerDec
        % Number of frequency points per octave in Gdata/Gfile (GfreqSweep=3) (sm-ri) 
        GfreqsPerOct
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Modeltype(obj,val)
            obj = setParameter(obj,'Modeltype',val,0,'string');
        end
        function res = get.Modeltype(obj)
            res = getParameter(obj,'Modeltype');
        end
        function obj = set.Lfile(obj,val)
            obj = setParameter(obj,'Lfile',val,0,'string');
        end
        function res = get.Lfile(obj)
            res = getParameter(obj,'Lfile');
        end
        function obj = set.Cfile(obj,val)
            obj = setParameter(obj,'Cfile',val,0,'string');
        end
        function res = get.Cfile(obj)
            res = getParameter(obj,'Cfile');
        end
        function obj = set.Rfile(obj,val)
            obj = setParameter(obj,'Rfile',val,0,'string');
        end
        function res = get.Rfile(obj)
            res = getParameter(obj,'Rfile');
        end
        function obj = set.Gfile(obj,val)
            obj = setParameter(obj,'Gfile',val,0,'string');
        end
        function res = get.Gfile(obj)
            res = getParameter(obj,'Gfile');
        end
        function obj = set.Ldata(obj,val)
            obj = setParameter(obj,'Ldata',val,0,'real');
        end
        function res = get.Ldata(obj)
            res = getParameter(obj,'Ldata');
        end
        function obj = set.Cdata(obj,val)
            obj = setParameter(obj,'Cdata',val,0,'real');
        end
        function res = get.Cdata(obj)
            res = getParameter(obj,'Cdata');
        end
        function obj = set.Rdata(obj,val)
            obj = setParameter(obj,'Rdata',val,0,'real');
        end
        function res = get.Rdata(obj)
            res = getParameter(obj,'Rdata');
        end
        function obj = set.Gdata(obj,val)
            obj = setParameter(obj,'Gdata',val,0,'real');
        end
        function res = get.Gdata(obj)
            res = getParameter(obj,'Gdata');
        end
        function obj = set.RSdata(obj,val)
            obj = setParameter(obj,'RSdata',val,0,'real');
        end
        function res = get.RSdata(obj)
            res = getParameter(obj,'RSdata');
        end
        function obj = set.GDdata(obj,val)
            obj = setParameter(obj,'GDdata',val,0,'real');
        end
        function res = get.GDdata(obj)
            res = getParameter(obj,'GDdata');
        end
        function obj = set.Lmodel(obj,val)
            obj = setParameter(obj,'Lmodel',val,0,'string');
        end
        function res = get.Lmodel(obj)
            res = getParameter(obj,'Lmodel');
        end
        function obj = set.Cmodel(obj,val)
            obj = setParameter(obj,'Cmodel',val,0,'string');
        end
        function res = get.Cmodel(obj)
            res = getParameter(obj,'Cmodel');
        end
        function obj = set.Rmodel(obj,val)
            obj = setParameter(obj,'Rmodel',val,0,'string');
        end
        function res = get.Rmodel(obj)
            res = getParameter(obj,'Rmodel');
        end
        function obj = set.Gmodel(obj,val)
            obj = setParameter(obj,'Gmodel',val,0,'string');
        end
        function res = get.Gmodel(obj)
            res = getParameter(obj,'Gmodel');
        end
        function obj = set.LfreqSweep(obj,val)
            obj = setParameter(obj,'LfreqSweep',val,0,'integer');
        end
        function res = get.LfreqSweep(obj)
            res = getParameter(obj,'LfreqSweep');
        end
        function obj = set.LfreqStart(obj,val)
            obj = setParameter(obj,'LfreqStart',val,0,'real');
        end
        function res = get.LfreqStart(obj)
            res = getParameter(obj,'LfreqStart');
        end
        function obj = set.LfreqStop(obj,val)
            obj = setParameter(obj,'LfreqStop',val,0,'real');
        end
        function res = get.LfreqStop(obj)
            res = getParameter(obj,'LfreqStop');
        end
        function obj = set.LfreqsPerDec(obj,val)
            obj = setParameter(obj,'LfreqsPerDec',val,0,'integer');
        end
        function res = get.LfreqsPerDec(obj)
            res = getParameter(obj,'LfreqsPerDec');
        end
        function obj = set.LfreqsPerOct(obj,val)
            obj = setParameter(obj,'LfreqsPerOct',val,0,'integer');
        end
        function res = get.LfreqsPerOct(obj)
            res = getParameter(obj,'LfreqsPerOct');
        end
        function obj = set.CfreqSweep(obj,val)
            obj = setParameter(obj,'CfreqSweep',val,0,'integer');
        end
        function res = get.CfreqSweep(obj)
            res = getParameter(obj,'CfreqSweep');
        end
        function obj = set.CfreqStart(obj,val)
            obj = setParameter(obj,'CfreqStart',val,0,'real');
        end
        function res = get.CfreqStart(obj)
            res = getParameter(obj,'CfreqStart');
        end
        function obj = set.CfreqStop(obj,val)
            obj = setParameter(obj,'CfreqStop',val,0,'real');
        end
        function res = get.CfreqStop(obj)
            res = getParameter(obj,'CfreqStop');
        end
        function obj = set.CfreqsPerDec(obj,val)
            obj = setParameter(obj,'CfreqsPerDec',val,0,'integer');
        end
        function res = get.CfreqsPerDec(obj)
            res = getParameter(obj,'CfreqsPerDec');
        end
        function obj = set.CfreqsPerOct(obj,val)
            obj = setParameter(obj,'CfreqsPerOct',val,0,'integer');
        end
        function res = get.CfreqsPerOct(obj)
            res = getParameter(obj,'CfreqsPerOct');
        end
        function obj = set.RfreqSweep(obj,val)
            obj = setParameter(obj,'RfreqSweep',val,0,'integer');
        end
        function res = get.RfreqSweep(obj)
            res = getParameter(obj,'RfreqSweep');
        end
        function obj = set.RfreqStart(obj,val)
            obj = setParameter(obj,'RfreqStart',val,0,'real');
        end
        function res = get.RfreqStart(obj)
            res = getParameter(obj,'RfreqStart');
        end
        function obj = set.RfreqStop(obj,val)
            obj = setParameter(obj,'RfreqStop',val,0,'real');
        end
        function res = get.RfreqStop(obj)
            res = getParameter(obj,'RfreqStop');
        end
        function obj = set.RfreqsPerDec(obj,val)
            obj = setParameter(obj,'RfreqsPerDec',val,0,'integer');
        end
        function res = get.RfreqsPerDec(obj)
            res = getParameter(obj,'RfreqsPerDec');
        end
        function obj = set.RfreqsPerOct(obj,val)
            obj = setParameter(obj,'RfreqsPerOct',val,0,'integer');
        end
        function res = get.RfreqsPerOct(obj)
            res = getParameter(obj,'RfreqsPerOct');
        end
        function obj = set.GfreqSweep(obj,val)
            obj = setParameter(obj,'GfreqSweep',val,0,'integer');
        end
        function res = get.GfreqSweep(obj)
            res = getParameter(obj,'GfreqSweep');
        end
        function obj = set.GfreqStart(obj,val)
            obj = setParameter(obj,'GfreqStart',val,0,'real');
        end
        function res = get.GfreqStart(obj)
            res = getParameter(obj,'GfreqStart');
        end
        function obj = set.GfreqStop(obj,val)
            obj = setParameter(obj,'GfreqStop',val,0,'real');
        end
        function res = get.GfreqStop(obj)
            res = getParameter(obj,'GfreqStop');
        end
        function obj = set.GfreqsPerDec(obj,val)
            obj = setParameter(obj,'GfreqsPerDec',val,0,'integer');
        end
        function res = get.GfreqsPerDec(obj)
            res = getParameter(obj,'GfreqsPerDec');
        end
        function obj = set.GfreqsPerOct(obj,val)
            obj = setParameter(obj,'GfreqsPerOct',val,0,'integer');
        end
        function res = get.GfreqsPerOct(obj)
            res = getParameter(obj,'GfreqsPerOct');
        end
    end
end
